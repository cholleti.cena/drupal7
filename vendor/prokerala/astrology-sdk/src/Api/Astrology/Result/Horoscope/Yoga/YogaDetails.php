<?php

/*
 * This file is part of Prokerala Astrology API PHP SDK
 *
 * © Ennexa Technologies <info@ennexa.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Prokerala\Api\Astrology\Result\Horoscope\Yoga;

class YogaDetails
{

    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $description;

    /**
     * YogaDetails constructor.
     * @param string $name
     * @param string $description
     */
    public function __construct(
        $name,
        $description
    ) {

        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


}
