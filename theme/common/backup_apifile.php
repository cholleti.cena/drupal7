<?php
namespace App\Http\Controllers;

use App\Defaults;
use App\Eateries;
use App\UserFavouriteEateries;
use App\User;
use App\AppUsersReviewsEateries;
use App\Dishes;
use App\Menu;
use App\MenuSection;
use App\MenuSubSection;
use App\Nutrition;
use App\Cuisines;
use App\LifestyleChoices;
use App\Allergens;
use App\UserShoppingCart;
use App\AppUsersDelete;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Support\Facades\DB;
use App\AppCustomers;
use App\Ingredients;
use Illuminate\Support\Facades\Session;
use App\Recipe;
use App\AppUsersFavouriteEateries;
use App\AppUsersFavouriteRecipes;
use App\AppUsersRemovedEateries;
use App\AppUsersRemovedRecipes;
use App\AppuserLifeStyleChoices;
use App\AppUserRecipeReviews;
use App\AppuserProfileSettings;
use App\AppuserNutritionProfiles;
use App\AppuserCommunities;
use mysqli;
use PDO;
use Mail;
use App\Sharing;
use App\ShareOnUserCommunities;

ini_set('memory_limit', '5048M');
ini_set('max_execution_time', 5000);



	function v1_geteateries($latitude,$longitude,$cuisines_ids,$lifestyle_choices_ids,$search,$userId,$recipe_name)
	{
		 $defaults = Defaults::all();    
         $search_radius = $defaults[0]->search_radius;
         $filter_cuisine = '';
         $filter_lifestyle_choices = '';
        $filter_cuisineString = [];
        $filter_lifestyle_choicesString = [];
         if(!empty($cuisines_ids))
         {
             $cusinesIds = explode(",",$cuisines_ids);
             foreach($cusinesIds as $cuisine)
             {
                 $filter_cuisineString[] = "e.cuisines_ids in (".$cuisine.")";
             }
             $cuisineString = implode(" or ",$filter_cuisineString);
             $filter_cuisine = ' and ('.$cuisineString.')';
          }
          if(!empty($lifestyle_choices_ids))
         {
             $lifestyle_choices_Ids = explode(",",$lifestyle_choices_ids);
             foreach($lifestyle_choices_Ids as $lifestyle_choices)
             {
                 $filter_lifestyle_choicesString[] = "e.lifestyle_choices_ids in(".$lifestyle_choices.")";
             }
             $lifestyle_choices_String = implode(" or ",$filter_lifestyle_choicesString);
             $filter_lifestyle_choices = ' and ('.$lifestyle_choices_String.')';
          }
        if($search != ''){
            $search_result_limit = 100;
        }
        else{
            $search_result_limit = $defaults[0]->search_result_limit;
        }
        if($recipe_name != ''){
            // $recipe = explode(" ",$recipe_name);
            // $recepie_string = [];
            // $pattern = "[^$.|?*+()`~@#$%(+={\\//<\"\\',";

            $recepie_string = [];
            $pattern = "[^$.|?*+()`~@#$%(+={\\//<\"\\',";
            $prefect_string = str_replace('and','',str_replace('in','',str_replace('&','',$recipe_name)));
            $stripped = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $prefect_string);
            $recipe = explode(" ",$stripped);

            foreach($recipe as $recipeString){
                //$prefect_string = preg_replace($pattern,'',str_replace('and','',$recipeString));
                // $prefect_string = str_replace('and','',$recipeString);
                $recepie_string[] = "d.dish_name like '%".$recipeString."%'";
            }
            $recipeName = implode(" or ",$recepie_string);

            $associated_sql  = "select e.id, e.fhrsid, e.business_name, e.address, e.logo_path, e.is_associated , e.longitude, e.latitude,e.clicks_after_associated, e.foodadvisr_overall_rating,e.cuisines_ids,e.lifestyle_choices_ids,
                    round((6371*0.621371 * 2 * ASIN(SQRT( POWER(SIN(($latitude - abs(e.latitude)) * pi()/180 / 2),2)
                    +  COS($latitude * pi()/180 ) * COS(abs(e.latitude) * pi()/180) * POWER(SIN(($longitude - e.longitude)
                    * pi()/180 / 2), 2) ))),2) as distance
                    from eateries as e
                    inner join dishes as d
                    on d.eatery_id = e.id
                    where (".$recipeName." or d.dish_name = '".$recipe_name."') and e.is_associated = 1 and e.is_enabled = 1 ";
                
            if(isset($search) && !empty($search) && $search != NULL){
                $associated_sql .= " and (e.business_name like'%".$search."%' or e.locality like'%".$search."%' or e.address like'%".$search."%' or e.postal_code like'%".$search."%') ";
            }
            if(isset($userId) && !empty($userId) && $userId != NULL)
            {
                $associated_sql .= " and e.id not in (select eatery_id from appusers_removed_eateries where userid=".$userId." )";
            }
            if(isset($cuisines_ids) && !empty($cuisines_ids) && $cuisines_ids != NULL){
                //$associated_sql  .= " and e.cuisines_ids like'%".$cuisines_ids."%'";
                $associated_sql  .= $filter_cuisine;
            }
            if(isset($lifestyle_choices_ids) && !empty($lifestyle_choices_ids) && $lifestyle_choices_ids != NULL){
                //$associated_sql  .= " and e.lifestyle_choices_ids like'%".$lifestyle_choices_ids."%'";
                $associated_sql  .= $filter_lifestyle_choices;
            }
            $associated_sql  .= " group by e.id order by distance asc
                    limit $search_result_limit";
        }
        else{
            $associated_sql  = "select e.id, e.fhrsid, e.business_name, e.address, e.logo_path, e.is_associated , e.longitude, e.latitude,e.clicks_after_associated, e.foodadvisr_overall_rating,e.cuisines_ids,e.lifestyle_choices_ids,
                    round((6371*0.621371 * 2 * ASIN(SQRT( POWER(SIN(($latitude - abs(e.latitude)) * pi()/180 / 2),2)
                    +  COS($latitude * pi()/180 ) * COS(abs(e.latitude) * pi()/180) * POWER(SIN(($longitude - e.longitude)
                    * pi()/180 / 2), 2) ))),2) as distance
                    from eateries as e
                    where e.id in (select id from eateries
                    where e.is_associated = 1 and e.is_enabled = 1 )";

            if(isset($search) && !empty($search) && $search != NULL){
                $associated_sql .= " and (e.business_name like'%".$search."%' or e.locality like'%".$search."%' or e.address like'%".$search."%' or e.postal_code like'%".$search."%') ";
            }
            if(isset($userId) && !empty($userId) && $userId != NULL)
            {
                $associated_sql .= " and e.id not in (select eatery_id from appusers_removed_eateries where userid=".$userId." )";
            }
            if(isset($cuisines_ids) && !empty($cuisines_ids) && $cuisines_ids != NULL){
                //$associated_sql  .= " and e.cuisines_ids like'%".$cuisines_ids."%'";
                $associated_sql  .= $filter_cuisine;
            }
            if(isset($lifestyle_choices_ids) && !empty($lifestyle_choices_ids) && $lifestyle_choices_ids != NULL){
                //$associated_sql  .= " and e.lifestyle_choices_ids like'%".$lifestyle_choices_ids."%'";
                $associated_sql  .= $filter_lifestyle_choices;
            }
            $associated_sql  .= "  order by distance asc
                    limit $search_result_limit";
        }
         $eateries_associated = DB::select( DB::raw($associated_sql));
         
        if(!(!empty($cuisines_ids) || $cuisines_ids != "" || !empty($lifestyle_choices_ids) || $lifestyle_choices_ids != ""))
        {
            $unassociated_sql  = "select id, fhrsid, business_name, address, logo_path, is_associated, longitude, latitude,clicks_after_associated, foodadvisr_overall_rating,cuisines_ids,lifestyle_choices_ids,
                        round((6371*0.621371 * 2 * ASIN(SQRT( POWER(SIN(($latitude - abs(latitude)) * pi()/180 / 2),2)
                        +  COS($latitude * pi()/180 ) * COS(abs(latitude) * pi()/180) * POWER(SIN(($longitude - longitude)
                        * pi()/180 / 2), 2) ))),2) as distance
                        from eateries where id  in (select id from eateries
                        where ifnull(is_associated,0) = 0 and is_enabled = 1 
                        and (6371*0.621371 * 2 * ASIN(SQRT( POWER(SIN(($latitude - abs(latitude)) * pi()/180 / 2),2) +
                        COS($latitude * pi()/180 ) * COS(abs(latitude) * pi()/180) * POWER(SIN(($longitude - longitude) * pi()/180 / 2), 2) ))  <= $search_radius) )
                         order by distance asc
                        limit $search_result_limit";
            $eateries_unassociated = DB::select( DB::raw($unassociated_sql));
            $eateries = array_merge($eateries_associated,$eateries_unassociated);
        }
        else
            $eateries = $eateries_associated;

        return $eateries;
	}

    function v1_geteaterydetailsbyid($userid,$id,$recipe_name)
    {
        $result = Eateries::find($id);

        if($result <> null)
        {
            //$result->rating  = getRatingEateries($id);
            $result->distance = getDistanceById($id);
            $result->media = getImagesById($id);
            $result->recommendeddishes = getRecommendedDishes($userid,$id);
            $result->reciperelateddish_count = getreciperelateddish_count($userid,$id,$recipe_name);
            $result->menus = getmenubygroupid($result->group_id,$id);
            // $result->dishes = geteaterydishes($id);
        }
        return $result;     
    }


    function getreciperelateddish_count($userid,$eateryid,$recipe_name)
    {
        if($recipe_name != '')
        {
            
            $recepie_string = [];
            $pattern = "[^$.|?*+()`~@#$%(+={\\//<\"\\',";
            $prefect_string = str_replace('and','',str_replace('in','',str_replace('&','',$recipe_name)));
            $stripped = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $prefect_string);
            $recipe = explode(" ",$stripped);          
            
            foreach($recipe as $recipeString){
                // $prefect_string = preg_replace($pattern,'',str_replace('and','',$recipeString));
                // $prefect_string = str_replace('and','',$recipeString);
                $recepie_string[] = "dish_name like '%".$recipeString."%'";
            }
            $recipeName = implode(" or ",$recepie_string);
            $sql = "select count(*) as Total from dishes where is_visible=1 and eatery_id = ".$eateryid." and ".$recipeName."";
            $reciperelateddish_count = DB::select( DB::raw($sql));
            $result = $reciperelateddish_count[0]->Total;
        }
        else
        {
            $result = 0;
        }

        return $result;
    }
    function v1_getdishdetailsbyid($id,$userid)
    {
        $appusers = DB::table('appusers')
            ->where('id',$userid)->get();
        $profileId = $appusers[0]->profileid;


        if($profileId > 1000)
        {
            $login_result = DB::table('appusers')
                ->leftjoin('appuser_nutrition_profiles','appuser_nutrition_profiles.profileid','=','appusers.profileid')
                ->where('appusers.id',$userid)
                ->select(DB::raw('appusers.allergenids,appusers.allergens,appusers.includeids,appusers.excludeids,appusers.likeids,appusers.dislikeids,appuser_nutrition_profiles.*'))
                ->get();
        }
        elseif($profileId < 1000)
        {
            $login_result = DB::table('appusers')
                ->leftjoin('profiles','profiles.ref','=','appusers.profileid')
                ->where('appusers.id',$userid)
                ->select(DB::raw('appusers.allergenids,appusers.allergens,appusers.includeids,appusers.excludeids,appusers.likeids,appusers.dislikeids,profiles.*'))
                ->get();
        }
        else{
            $login_result = DB::table('appusers')
                ->where('appusers.id',$userid)
                ->select(DB::raw('appusers.allergenids,appusers.allergens,appusers.includeids,appusers.excludeids,appusers.likeids,appusers.dislikeids'))
                ->get();
        }
        $userAllergens = isset($login_result[0]->allergens)?$login_result[0]->allergens:null;
        $userProfileNutritionTotalFat = isset($login_result[0]->totalfat)?number_format($login_result[0]->totalfat,2):0;
        $userProfileNutritionCalories = isset($login_result[0]->calories)?number_format($login_result[0]->calories,2):0;
        $userProfileNutritionSaturatedfat = isset($login_result[0]->saturatedfat)?number_format($login_result[0]->saturatedfat,2):0;
        $userProfileNutritionSalt = isset($login_result[0]->salt)?number_format($login_result[0]->salt,2):0;
        $userProfileNutritionSugar = isset($login_result[0]->sugar)?number_format($login_result[0]->sugar,2):0;
        $userProfileNutritionProtien = isset($login_result[0]->protien)?number_format($login_result[0]->protien,2):0;
        $userProfileNutritionCarbs = isset($login_result[0]->carbs)?number_format($login_result[0]->carbs,2):0;
        $userProfileNutritionFibre = isset($login_result[0]->fibre)?number_format($login_result[0]->fibre,2):0;
        $userProfileNutritionCholesterol = isset($login_result[0]->cholesterol)?number_format($login_result[0]->cholesterol,2):0;


        $result = Dishes::find($id);

        if($result <> null)
        {
            $dish_allergens_string = getAllergensByAllergenIds($result->allergens_contain_ids);
            $dish_allergens_may_array = getAllergensByAllergenIds($result->allergens_may_contain);
            $dish_cuisine_array = getCuisinesById($result->cuisines_ids);
            $dish_lifestyle_array = getLifeStyleChoiceById($result->lifestyle_choices_ids);
            $dish_ingredients_string =getIngredientsByIngredientId($result->ingredients_ids);

            $dish_nutrition_fat = isset($result -> nutrition_fat)?number_format($result->nutrition_fat,2):0;
            $dish_nutrition_cholesterol = isset($result -> nutrition_cholesterol)?number_format($result->nutrition_cholesterol,2):0;
            $dish_nutrition_sugar = isset($result -> nutrition_sugar)?number_format($result->nutrition_sugar,2):0;
            $dish_nutrition_fibre = isset($result -> nutrition_fibre)?number_format($result->nutrition_fibre,2):0;
            $dish_nutrition_protein = isset($result -> nutrition_protein)?number_format($result->nutrition_protein,2):0;
            $dish_nutrition_saturated_fat = isset($result -> nutrition_saturated_fat)?number_format($result->nutrition_saturated_fat,2):0;
            $dish_nutrition_calories = isset($result -> nutrition_calories)?number_format($result->nutrition_calories,2):0;
            $dish_nutrition_carbohydrates = isset($result -> nutrition_carbohydrates)?number_format($result->nutrition_carbohydrates,2):0;
            $dish_nutrition_salt = isset($result -> nutrition_salt)?number_format($result->nutrition_salt,2):0;

            $result->allergens = $dish_allergens_string;
            $result->maycontains = $dish_allergens_may_array;
            $result->cuisines = $dish_cuisine_array;
            $result->lifestyle_choices = $dish_lifestyle_array;
            $result->ingredients = $dish_ingredients_string;
            $Nutritions[] = getNutrition('Fat',$dish_nutrition_fat,$userProfileNutritionTotalFat);
            $Nutritions[] = getNutrition('Cholesterol',$dish_nutrition_cholesterol,$userProfileNutritionCholesterol);
            $Nutritions[] = getNutrition('Sugar',$dish_nutrition_sugar,$userProfileNutritionSugar);
            $Nutritions[] = getNutrition('Fibre',$dish_nutrition_fibre,$userProfileNutritionFibre);
            $Nutritions[] = getNutrition('Protein',$dish_nutrition_protein,$userProfileNutritionProtien);
            $Nutritions[] = getNutrition('Saturated Fat',$dish_nutrition_saturated_fat,$userProfileNutritionSaturatedfat);
            $Nutritions[] = getNutrition('Calories',$dish_nutrition_calories,$userProfileNutritionCalories);
            $Nutritions[] = getNutrition('Carbs',$dish_nutrition_carbohydrates,$userProfileNutritionCarbs);
            $Nutritions[] = getNutrition('Salt',$dish_nutrition_salt,$userProfileNutritionSalt);
            $result->nutritions = $Nutritions;

        }
        return $result;     
    }
    function getNutrition($pDescription, $pContainsInDish, $pProfileSetting)
    {
        $nutrition = new Nutrition();
        $nutrition->description = $pDescription;
        $nutrition->contains_in_dish = $pContainsInDish;
        $nutrition->profile_setting = $pProfileSetting;
        return $nutrition;
    }

    function getLifeStyleChoiceById($lifestypeChoiceIds)
    {
        return implode(",",DB::table('lifestyle_choices')->whereIn('id',explode(",",$lifestypeChoiceIds))->pluck('description')->toArray());
    }

    function getCuisinesById($cuisineIds)
    {
        return implode(",",DB::table('cuisines')->whereIn('id',explode(",",$cuisineIds))->pluck('cuisine_name')->toArray());
    }

/**
 * User Recommended Dishes based on the profile settings
 * @param $userid
 * @param $eatery_id
 * @return array
 */
    function getRecommendedDishes($userid,$eatery_id)
    {
        // $servername = env('DB_HOST');
        // $username = env('DB_USERNAME');
        // $password = env('DB_PASSWORD');
        // $db = env('DB_DATABASE');

        $servername = "localhost";
        $username = "quicrdev";
        $password = "mysql";
        $db = "quicrdev";

        $pdo = new PDO('mysql:host='.$servername.';dbname='.$db.'',$username,$password);
        $res = $pdo->query('CALL getrecommendeddishes('.$userid.','.$eatery_id.')');
        $recommended_dish_array = array();        
        $i = 0;
        while ($row = $res->fetch()) {
           $recommended_dish_array[$i]['dish_id'] = $row['id'];
           $recommended_dish_array[$i]['dish_name'] = $row['dish_name'];
           $recommended_dish_array[$i]['description'] = $row['description'];
           $recommended_dish_array[$i]['img_url'] = $row['img_url'];
           $recommended_dish_array[$i]['default_price'] = $row['default_price'];
           $recommended_dish_array[$i]['lifestyle_choices_ids'] = $row['lifestyle_choices_ids'];
           $i++;
        }
            return $recommended_dish_array;
    }


/**
 * Get the Ingredients based on the Ingredient Ids
 * @param $ingredientIds
 * @return string
 */
    function getIngredientsByIngredientId($ingredientIds)
    {
        return implode(",",DB::table('ingredients')->whereIn('id',explode(",",$ingredientIds))->pluck('description')->toArray());
    }

/**
 * Get the Allergens based on the Allergens Ids
 * @param $allergenIds
 * @return string
 */
    function getAllergensByAllergenIds($allergenIds)
    {
        return implode(",",DB::table('allergens')->whereIn('ref',explode(",",$allergenIds))->pluck('title')->toArray());
    }

/**
 * Get the Intolerances based on the Intolerance Ids
 * @param $intoleranceIds
 * @return string
 */
    function getIntoleranceByIntoleranceIds($intoleranceIds)
    {
        return implode(",",DB::table('ingredients')->whereIn('id',explode(",",$intoleranceIds))->pluck('description')->toArray());
    }

/**
 * Get the Eatery Distance from the current location
 * @param $id
 * @return mixed
 */
    function getDistanceById($id)
    {
       $result = Eateries::find($id);
       $get_distance = DB::table('eateries')
                ->select(DB::raw('round((6371*0.621371 * 2 * ASIN(SQRT( POWER(SIN(("'.$result->Latitude.'" - abs(latitude)) * pi()/180 / 2),2) +  COS("'.$result->Latitude.'" * pi()/180 ) * COS(abs(latitude) * pi()/180) * POWER(SIN(("'.$result->Longitude.'" - longitude) * pi()/180 / 2), 2) ))),2) as distance'))
               ->where('id','=',$id)
               ->get();
       return $get_distance[0]->distance; 
    }

/**
 * Get the Slider Images for the Eatery
 * @param $id
 * @return array
 */
    function getImagesById($id)
    {
         $images_array = array();
         $sql  = "select * from eateriesmedia where eateriesmedia.eatery_id=" . $id ." ";
         $image_result = DB::select( DB::raw($sql));
         $mediaDir = 'content/eateryimages/'. $id . '/';
         $image_index = 0;
         foreach ($image_result as $image) {
            $images_array['images'][$image_index]['media_name'] = $mediaDir . $image->media_name ;
            $image_index++;
         }
         return $images_array;
    }

/**
 * Add the clicks before associated Eatery
 * @param $id
 */
    function v1_addclickbeforeassociated($id)
    {
        $clickbeforeassociated = 'UPDATE eateries SET clicks_before_associated = IFNULL(clicks_before_associated,0) + 1 WHERE id ='.$id;
        DB::select(DB::raw($clickbeforeassociated));        
        return 'View Added Successfully';
    }

/**
 * Add the clicks after associated Eatery
 * @param $id
 */
    function v1_addclickafterassociated($id)
    {
        $clickafterassociated = 'UPDATE eateries SET clicks_after_associated = IFNULL(clicks_after_associated,0) + 1 WHERE id ='.$id;
        DB::select(DB::raw($clickafterassociated));        
        return 'View Added Successfully';
    }

    function v1_gettop5eateriesBeforeAssociated()
    {
        $result  = DB::table('eateries')
                ->select(DB::raw('business_name,clicks_before_associated'))
                ->orwhereNull('is_associated')
                ->Where('is_associated', '=', 0)
                ->Where('clicks_before_associated', '>', 0)
                ->where('is_enabled','=', 1)
                ->orderby('clicks_before_associated','DESC')
                ->LIMIT(5)
                ->get();
        return $result;
    }

    function v1_gettop5eateriesAfterAssociated()
    {
       $result  = DB::table('eateries')
                ->select(DB::raw('business_name,clicks_after_associated'))
                ->Where('is_associated', '=', 1)
                ->where('is_enabled','=', 1)
                ->orderby('clicks_after_associated','DESC')
                ->LIMIT(5)
                ->get();
        return $result;
    }

    function v1_getcuisines()
    {
        $sql  = 'select * from cuisines where ifnull(is_enabled,0) = 1';
        $result = DB::select( DB::raw($sql));
        return $result;
    }

    function v1_lifestylechoices()
    {
        $sql  = 'select * from lifestyle_choices where ifnull(is_enabled,0) = 1 and is_enabled =1';
        $result = DB::select( DB::raw($sql));
        return $result;
    }

     function v1_getnutritions()
    {
        $sql  = 'select * from nutrition_types where ifnull(is_enabled,0) = 1';
        $result = DB::select( DB::raw($sql));
        return $result;
    }

     function v1_getallergens()
    {
        $sql  = 'select * from allergens where type = "A" and display = "Yes"';
        $result = DB::select( DB::raw($sql));
        return $result;
    }

/**
 * Add Eatery To User Removed List
 * @param $userid
 * @param $eatery_id
 * @return string
 */
    function v1_addeaterytouserremovedlist($userid,$eatery_id)
    {
     $user_remove_count = AppUsersRemovedEateries::where('userid',$userid)
        ->where('eatery_id',$eatery_id)->count();
      if($user_remove_count == 0)
      {
       $removefavouriteeateries = new AppUsersRemovedEateries();
       $removefavouriteeateries->userid = $userid;
       $removefavouriteeateries->eatery_id = $eatery_id;
       $removefavouriteeateries->deleted_on =  Carbon::now(new DateTimeZone('Europe/London'));
       $removefavouriteeateries->save();
       return 'Add Eatery To User Removed List Successfully';
       }
       else 
        return 'Already Added Into Removed List';

    }

    /**
 * Delete Eatery From User Removed List
 * @param $userid
 * @param $eatery_id
 * @return string
 */
    function v1_deleteeateryfromuserremovedlist($userid,$eatery_id)
    {
     
        $result = DB::table('appusers_removed_eateries')->where('userid', $userid)->where('eatery_id',$eatery_id)->delete();
        return "Deleted Eatery From User Removed List Successfully!";

    }

/**
 * Get User Removed Eateries
 * @param $userid
 * @return int
 */

   function v1_getuserremovedeateries($userid)
    {
      $sql = "select eateries.id eatery_id,eateries.business_name from appusers_removed_eateries join eateries on appusers_removed_eateries.eatery_id = eateries.id where userid=".$userid."";
      $v1_getuserremovedeateries = DB::select( DB::raw($sql));
      return $v1_getuserremovedeateries;
    }

/**
 * Eatery menu, section, sub section and dish details
 * @param $groupid
 * @param $eateryid
 * @return array
 */
    function getmenubygroupid($groupid,$eateryid){
        $menu_details = DB::table('menu')
            ->leftjoin('disheateries', 'disheateries.menuid', '=', 'menu.ref')
            ->select(DB::raw('distinct ref,menu,description,eatery_id'))
            ->where('company','=','FoodAdvisr')
            ->get();

        $menuDetails = [];

        foreach($menu_details as $menu){
            $menus = new Menu();
            $menus->id = $menu->ref;
            $menus->menu_name = $menu->menu;
            $menus->menu_description = $menu->description;
            $dish_details = DB::table('dishes')
                ->join('disheateries', 'disheateries.dishid', '=', 'dishes.id')
                ->where('disheateries.eateryid', '=', $eateryid)
                ->whereIn('dishes.menus_ids',array($menu->ref))
                ->where('dishes.is_visible', '=', '1')
                ->select(DB::raw('distinct dishes.id as dish_id,dishes.dish_name,dishes.description,dishes.img_url,dishes.cuisines_ids,dishes.lifestyle_choices_ids,dishes.allergens_contain_ids,dishes.ingredients_ids,dishes.default_price,dishes.menus_ids,dishes.sections_ids,dishes.subsections_ids'))
                ->get();
                
            $menu_dish_array = [];
            if(isset($dish_details) && !empty($dish_details))
            {
                foreach($dish_details as $dishes){
                    $dish = new Dishes();
                    $dish->dish_id = $dishes->dish_id;
                    $dish->dish_name = $dishes->dish_name;
                    $dish->description = $dishes->description;
                    $dish->img_url = $dishes->img_url;
                    $dish->default_price = $dishes->default_price;
                    $dish->lifestyle_choices_ids = $dishes->lifestyle_choices_ids;
                    $ingredients_ids = explode(",",$dishes->ingredients_ids);            
                    if(!empty($ingredients_ids))
                    {
                        $ingredient_array = [];
                        foreach($ingredients_ids as $ingredient){
                            $ingredients_data = Ingredients::where('id','=',$ingredient)->get();
                            $ingredients_count = count($ingredients_data);
                            if($ingredients_count > 0)
                            {
                                $ingredient_array[] = $ingredients_data[0]->description;
                            }
                            else
                            {
                                $ingredient_array[] = '';
                            }
                        }
                        $dish->ingredients = implode(",", $ingredient_array);
                    }
                    else{
                         $dish->ingredients = '';
                    }
                    $menu_dish_array[] = $dish;
                }
            }
            $menu_section_details = DB::table('menu_section')
                ->leftjoin('disheateries', 'disheateries.sectionid', '=', 'menu_section.id')
                ->select(DB::raw('distinct menu_section.id,section_name,description,eatery_id'))
                ->get();
                
            $section_array = [];
            foreach($menu_section_details as $section){
                $menu_section = [];
                $menu_section = new MenuSection();
                $menu_section->section_id = $section->id;
                $menu_section->section_name = $section->section_name;
                $menu_section->description = $section->description;
                $dish_details = DB::table('dishes')
                     ->join('disheateries', 'disheateries.dishid', '=', 'dishes.id')
                     ->where('disheateries.eateryid', '=', $eateryid)
                    ->whereIn('dishes.sections_ids',array($section->id))
                    ->where('dishes.is_visible', '=', '1')
                    ->select(DB::raw('distinct dishes.id as dish_id,dishes.dish_name,dishes.description,dishes.img_url,dishes.cuisines_ids,dishes.lifestyle_choices_ids,dishes.allergens_contain_ids,dishes.ingredients_ids,dishes.default_price,dishes.menus_ids,dishes.sections_ids,dishes.subsections_ids'))
                    ->get();
                $menusection_dish_array = [];
                if(isset($dish_details) && !empty($dish_details)) {
                    foreach ($dish_details as $dishes) {
                        $dish = new Dishes();
                        $dish->dish_id = $dishes->dish_id;
                        $dish->dish_name = $dishes->dish_name;
                        $dish->description = $dishes->description;
                        $dish->img_url = $dishes->img_url;
                        $dish->default_price = $dishes->default_price;
                        $dish->lifestyle_choices_ids = $dishes->lifestyle_choices_ids;
                        $ingredients_ids = explode(",",$dishes->ingredients_ids);            
                        if(!empty($ingredients_ids))
                        {
                            $ingredient_array = [];
                            foreach($ingredients_ids as $ingredient){
                                $ingredients_data = Ingredients::where('id','=',$ingredient)->get();
                                $ingredients_count = count($ingredients_data);
                                if($ingredients_count > 0)
                                {
                                    $ingredient_array[] = $ingredients_data[0]->description;
                                }
                                else
                                {
                                    $ingredient_array[] = '';
                                }
                            }
                            $dish->ingredients = implode(",", $ingredient_array);
                        }
                        else{
                             $dish->ingredients = '';
                        }
                        $menusection_dish_array[] = $dish;
                    }
                }

                $menu_sub_section_details = DB::table('menu_sub_section')
                    ->leftjoin('disheateries', 'disheateries.subsectionid', '=', 'menu_sub_section.id')
                    ->select(DB::raw('distinct menu_sub_section.id,sub_section_name,description,eatery_id'))
                    ->get();
                $sub_section_array = [];
                foreach($menu_sub_section_details as $sub_section){
                    $menu_sub_section = new MenuSubSection();
                    $menu_sub_section->sub_section_id = $sub_section->id;
                    $menu_sub_section->sub_section_name = $sub_section->sub_section_name;
                    $menu_sub_section->description = $sub_section->description;
                    $sub_section_dish_details = DB::table('dishes')
                        ->join('disheateries', 'disheateries.dishid', '=', 'dishes.id')
                        ->where('disheateries.eateryid', '=', $eateryid)
                        ->whereIn('dishes.subsections_ids',array($sub_section->id))
                        ->where('dishes.is_visible', '=', '1')
                        ->select(DB::raw('distinct dishes.id as dish_id,dishes.dish_name,dishes.description,dishes.img_url,dishes.cuisines_ids,dishes.lifestyle_choices_ids,dishes.allergens_contain_ids,dishes.ingredients_ids,dishes.default_price,dishes.menus_ids,dishes.sections_ids,dishes.subsections_ids'))
                        ->get();
                    $menusubsection_dish_array = [];
                    if(isset($sub_section_dish_details) && !empty($sub_section_dish_details)) {
                        foreach ($sub_section_dish_details as $dishes) {
                            $dish = new Dishes();
                            $dish->dish_id = $dishes->dish_id;
                            $dish->dish_name = $dishes->dish_name;
                            $dish->description = $dishes->description;
                            $dish->img_url = $dishes->img_url;
                            $dish->default_price = $dishes->default_price;
                            $dish->lifestyle_choices_ids = $dishes->lifestyle_choices_ids;
                            $ingredients_ids = explode(",",$dishes->ingredients_ids);            
                            if(!empty($ingredients_ids))
                            {
                                $ingredient_array = [];
                                foreach($ingredients_ids as $ingredient){
                                    $ingredients_data = Ingredients::where('id','=',$ingredient)->get();
                                    $ingredients_count = count($ingredients_data);
                                    if($ingredients_count > 0)
                                    {
                                        $ingredient_array[] = $ingredients_data[0]->description;
                                    }
                                    else
                                    {
                                        $ingredient_array[] = '';
                                    }
                                }
                                $dish->ingredients = implode(",", $ingredient_array);
                            }
                            else{
                                 $dish->ingredients = '';
                            }
                            $menusubsection_dish_array[] = $dish;
                        }
                    }

                    $menu_sub_section->dishes = $menusubsection_dish_array;
                    $sub_section_array[] = $menu_sub_section;
                }
                $menu_section->dishes = $menusection_dish_array;
                $menu_section['sub_sections'] = $sub_section_array ;
                $section_array[] = $menu_section;
            }
            $menus->dishes = $menu_dish_array;
            $menus['sections'] = $section_array;
            $menuDetails[] = $menus;
        }

       return $menuDetails;
    }

/**
 * Returns the Eatery dish without Menu, section and sub section
 * @param $eateryid
 * @return array
 */
    function geteaterydishes($eateryid)
    {
        $dish_details = DB::table('dishes')
            ->join('disheateries', 'disheateries.dishid', '=', 'dishes.id')
            ->where('disheateries.eateryid', '=', $eateryid)
            ->where('dishes.is_visible', '=', '1')
            ->select(DB::raw('distinct dishes.id as dish_id,dishes.dish_name,dishes.description,dishes.img_url,dishes.cuisines_ids,dishes.lifestyle_choices_ids,dishes.allergens_contain_ids,dishes.ingredients_ids,dishes.default_price,dishes.menus_ids,dishes.sections_ids,dishes.subsections_ids'))
            ->get();
           
        $dish_array = [];
        foreach($dish_details as $dishes){
            $dish = new Dishes();
            $dish->dish_id = $dishes->dish_id;
            $dish->dish_name = $dishes->dish_name;
            $dish->description = $dishes->description;
            $dish->img_url = $dishes->img_url;
            $dish->default_price = $dishes->default_price;
            $dish->menus_ids = $dishes->menus_ids;
            $dish->sections_ids = $dishes->sections_ids;
            $dish->subsections_ids = $dishes->subsections_ids;
            $dish->cuisines_ids = $dishes->cuisines_ids;
            $dish->lifestyle_choices_ids = $dishes->lifestyle_choices_ids;
            $dish->allergens_contain_ids = $dishes->allergens_contain_ids;
            // $dish->ingredients_ids = $dishes->ingredients_ids;
            $ingredients_ids = explode(",",$dishes->ingredients_ids);            
            if(!empty($ingredients_ids))
            {
                $ingredient_array = [];
                foreach($ingredients_ids as $ingredient){
                    $ingredients_data = Ingredients::where('id','=',$ingredient)->get();
                    $ingredients_count = count($ingredients_data);
                    if($ingredients_count > 0)
                    {
                        $ingredient_array[] = $ingredients_data[0]->description;
                    }
                    else
                    {
                        $ingredient_array[] = '';
                    }
                }
                $dish->ingredients = implode(",", $ingredient_array);
            }
            else{
                 $dish->ingredients = '';
            } 
            $dish_array[] = $dish;
        }


        return $dish_array;

    }

    function v1_getUserEmail($l)
    {
        //$l=clean::get('var');

        $passed=explode('/',$l);
        $command=$passed[0];
        $p4=h::safeArray($passed,4);
        $p3=h::safeArray($passed,3);
        $p2=h::safeArray($passed,2);
        $p1=h::safeArray($passed,1);
        $jwtEncoded=h::safeArray($l,1);


        //$key=settings::getSettings('JWT','key');
        $key = "dmWcv5cv8CcfK2xkBEEN7GrkMu52hYY6T8HEdMr747xPTDSYBy";
        //$decode=JWT::decode($l,$key);
        $decode=JWT::decode($jwtEncoded,$key);

        $email=$decode->email;

        /*$payload=array('email'=>$l,'logintime'=>Carbon::now(new DateTimeZone('Europe/London')));
        $encoded=JWT::encode($payload,$key);

        return $encoded;*/
        return $email;
    }

/**
 * Adding the review for the eatery based on the eatery id
 * @param $review
 * @return int|string
 */
    function v1_addrevieweateries($review,$communities)
    {
        $userid = $review['userid'];
        $eatery_id = $review['eatery_id'];
        $message = $review['message'];
        $rating = $review['rating'];

        $result = DB::table('appusers_reviews_eateries')->where('userid','=', $userid)->where('eatery_id','=', $eatery_id)->get()->count();
        if($result == 0)
        {
        if(isset($review) && !empty($review))
        {
            $reviews = new AppUsersReviewsEateries();
            $reviews->userid = $userid;
            $reviews->eatery_id = $eatery_id;
            $reviews->message = $message;
            $reviews->rating = $rating;
            $reviews->added_on = Carbon::now(new DateTimeZone('Europe/London'));
            $reviews->save();

            if(!($eatery_id == '' || $eatery_id == null))
            {
                if($eatery_id > 0)
                {
                    $affected = DB::update('update eateries set foodadvisr_overall_rating = (select avg(rating) from appusers_reviews_eateries where eatery_id =eateries.id) where id = ?', [$eatery_id]);
                }
            }
            $result =  'Added Eatery Review Successfully';
        }
        }
        else
        {
            $eatery_review = 'UPDATE appusers_reviews_eateries SET rating ="'.$rating.'",message ="'.$message.'"  WHERE userid ="'.$userid.'" and eatery_id ="'.$eatery_id.'"';
            DB::select(DB::raw($eatery_review));
            $affected = DB::update('update eateries set foodadvisr_overall_rating = (select avg(rating) from appusers_reviews_eateries where eatery_id =eateries.id) where id = ?', [$eatery_id]);
            $result =  'Updated Eatery Review Successfully';
        }
        $userdetails = DB::table('appusers')->where('id',$userid)->select(DB::raw('email'))->first();
        $useremail = $userdetails->email;
        $shareuserrecipereview = DB::table('sharing')
            ->where('sharing.userid',$userid)
            ->where('sharing.eateryorrecepieid',$eatery_id)
            ->where('sharing.type','E')
            ->get();

        if(count($shareuserrecipereview) == 0)
        {
            $usershares = new Sharing();
            $usershares->userid=$userid;
            $usershares->eateryorrecepieid = $eatery_id;
            $usershares->type = 'E';
            $usershares->rating = $rating;
            $usershares->is_visible = 1;
            $usershares->email = $useremail;
            $usershares->comments = $message;
            $usershares->groups = isset($communities)?$communities:'';
            $usershares->sharedate = Carbon::now(new DateTimeZone('Europe/London'));
            $usershares->modifieddate = Carbon::now(new DateTimeZone('Europe/London'));
            $usershares->save();
            $shareid = $usershares->id;

            if($communities != '')
            {
                $communitiesResult = DB::table('communities')->whereIn('cname',explode(",",$communities))->pluck('ref')->toArray();
                foreach($communitiesResult as $community)
                {
                    $usercommunities = new ShareOnUserCommunities();
                    $usercommunities->shareid = $shareid;
                    $usercommunities->userid = $userid;
                    $usercommunities->communityid = $community;
                    $usercommunities->save();
                }
            }
        }
        else
        {
            foreach($shareuserrecipereview as $share)
            {
                $shareId = $share->id;
                $usershares = Sharing::find($shareId);
                $usershares->is_visible = 0;
                $usershares->modifieddate = Carbon::now(new DateTimeZone('Europe/London'));
                $usershares->update();
                ShareOnUserCommunities::where('shareid',$shareId)->where('userid',$userid)->delete();
            }


            $usershares = new Sharing();
            $usershares->userid=$userid;
            $usershares->eateryorrecepieid = $eatery_id;
            $usershares->type = 'E';
            $usershares->rating = $rating;
            $usershares->is_visible = 1;
            $usershares->email = $useremail;
            $usershares->comments = $message;
            $usershares->groups = isset($communities)?$communities:'';
            $usershares->sharedate = Carbon::now(new DateTimeZone('Europe/London'));
            $usershares->modifieddate = Carbon::now(new DateTimeZone('Europe/London'));
            $usershares->save();
            $shareid = $usershares->id;
            if($communities != '')
            {
                $communitiesResult = DB::table('communities')->whereIn('cname', explode(",", $communities))->pluck('ref')->toArray();
                foreach ($communitiesResult as $community) {
                    $usercommunities = new ShareOnUserCommunities();
                    $usercommunities->shareid = $shareid;
                    $usercommunities->userid = $userid;
                    $usercommunities->communityid = $community;
                    $usercommunities->save();
                }
            }
        }
        return $result;
    }

/**
 * get the Eatery Rating details
 * @param $eateryId
 * @return mixed
 */
    function getRatingEateries($eateryId){
        //return $eateryId;
        /*$rating_query = "select sum(rating)/count(id) as TotalRating from appusers_reviews_eateries where eatery_id='+$eateryId+';";

        $rating = DB::select(DB::raw($rating_query));*/

        $rating_query = DB::table('appusers_reviews_eateries')
            ->where('eatery_id',$eateryId)->sum('rating');

        $rating_count = DB::table('appusers_reviews_eateries')
            ->where('eatery_id',$eateryId)->count();

       /* $TotalRating = "";
        foreach($rating_query as $review)
        {
            $TotalRating += $review->rating;
        }*/

        $total = $rating_query/$rating_count;

        return $total;
    }

/**
 * User information for validation and signup
 * @param $login_result
 * @return array
 */
    function userDetailedInformation($login_result)
    {
        $userSettingsExcludes = DB::table('appuser_profile_settings')
            ->where('appuserid',$login_result[0]->id)->where('type','I')->get();
        $Excludes = [];
        $excludesString="";
        if(isset($userSettingsExcludes) && !empty($userSettingsExcludes) && $userSettingsExcludes != '')
        {
            foreach($userSettingsExcludes as $userexclude){
                $Excludes[] = $userexclude->breakdown;
            }

            $excludesString = '{'.str_replace("'", "\"", implode(",",$Excludes)).'}';
        }

        $userSettingsLikes = DB::table('appuser_profile_settings')
            ->where('appuserid',$login_result[0]->id)->where('type','L')->get();
        $Likes = [];
        $likesString = "";
        if(isset($userSettingsLikes) && !empty($userSettingsLikes) && $userSettingsLikes != '')
        {
            foreach($userSettingsLikes as $userlikes){
                $Likes[] = $userlikes->breakdown;
            }

            $likesString = '{'.str_replace("'", "\"", implode(",",$Likes)).'}';
        }

        $userSettingsDislikes = DB::table('appuser_profile_settings')
            ->where('appuserid',$login_result[0]->id)->where('type','D')->get();
        $Dislikes = [];
        $dislikesString = "";
        if(isset($userSettingsDislikes) && !empty($userSettingsDislikes) && $userSettingsDislikes != ''){
            foreach($userSettingsDislikes as $userdislikes){
                $Dislikes[] = $userdislikes->breakdown;
            }

            $dislikesString = '{'.str_replace("'", "\"", implode(",",$Dislikes)).'}';
        }

        $allergen_ids = explode(",",$login_result[0]->allergenids);
        
        if(!empty($allergen_ids))
        {
            $allergens_array = [];
            foreach($allergen_ids as $allergen){
                $allergens_data = Allergens::where('ref','=',$allergen)->where('type','A')->get();
                $allergens_count = count($allergens_data);
                if($allergens_count > 0)
                {
                    $allergens_array[] = $allergens_data[0]->title;
                }
                else
                {
                    $allergens_array[] = '';
                }
            }
            $allergens = implode(",", $allergens_array);
        }
        else{
            $allergens = '';
        }

        $userLifeStyleChoices = DB::table('appuser_lifestylechoices')
            ->where('appuserid',$login_result[0]->id)->get();            
        $LifeStyleChoices = [];
        $LifeStyleChoicesString="";
        if(isset($userLifeStyleChoices) && !empty($userLifeStyleChoices) && $userLifeStyleChoices != '')
        {
            foreach($userLifeStyleChoices as $userchoices){
                $LifeStyleChoices[] = $userchoices->lifestylechoiceid;
            }

            $lifestylechoicesString = ''.str_replace("'", "\"", implode(",",$LifeStyleChoices)).'';
        }

        $userCommunities = DB::table('appusers_communities')
            ->where('appuserid',$login_result[0]->id)->get();            
        $Communities = [];
        $LifeStyleChoicesString="";
        if(isset($userCommunities) && !empty($userCommunities) && $userCommunities != '')
        {
            foreach($userCommunities as $usercommunity){
                // $Communities[] = $usercommunities->communityid;
                $communities_data = DB::table('communities')->where('ref','=',$usercommunity->communityid)->get();
                $communities_count = count($communities_data);
                if($communities_count > 0)
                {
                    $Communities[] = $communities_data[0]->cname;
                }
                else
                {
                    $Communities[] = '';
                }
            }
            $communitiesString = ''.str_replace("'", "\"", implode(",",$Communities)).'';
        }  

        $appuser = new AppCustomers();
        $appuser->id = $login_result[0]->id;
        $appuser->email = $login_result[0]->email;
        $appuser->allergens = $allergens;
        $appuser->lifestylechoices = $lifestylechoicesString;
        $appuser->communities = $communitiesString;
        $appuser->excludes = $excludesString;
        $appuser->likes = $likesString;
        $appuser->dislikes = $dislikesString;
        $appuser->profileid = $login_result[0]->profileid;
        if($login_result[0]->profileid != '')
        {
            $appuser->profilename = $login_result[0]->name;
            $appuser->profilecalories = $login_result[0]->calories;
            $appuser->profiletotalfat = $login_result[0]->totalfat;
            $appuser->profilesaturatedfat = $login_result[0]->saturatedfat;
            $appuser->profilesalt = $login_result[0]->salt;
            $appuser->profilesugar = $login_result[0]->sugar;
            $appuser->profileprotien = $login_result[0]->protien;
            $appuser->profilecarbs = $login_result[0]->carbs;
            $appuser->profilefibre = $login_result[0]->fibre;
            $appuser->profilecholesterol = $login_result[0]->cholesterol;
            $appuser->profileingredients = $login_result[0]->ingredients;
            $appuser->profilelikes = $login_result[0]->likes;
        }
        else{

            $appuser->profilename = '';
            $appuser->profilecalories = 0;
            $appuser->profiletotalfat = 0;
            $appuser->profilesaturatedfat = 0;
            $appuser->profilesalt = 0;
            $appuser->profilesugar = 0;
            $appuser->profileprotien = 0;
            $appuser->profilecarbs = 0;
            $appuser->profilefibre = 0;
            $appuser->profilecholesterol = 0;
            $appuser->profileingredients = 0;
            $appuser->profilelikes = 0;
        }

        $appuser_array[] = $appuser;

        return $appuser_array;
    }

/**
 * user login validation for signin
 * @param $email
 * @param $password
 * @return array
 */

    function v1_validateappuserlogin($email,$password)
    {
        $loginerr = DB::table('appusers')
            ->where('email',$email)->count();
        $register_data = DB::table('appusers')
            ->where('email',$email)->get();       
        if($loginerr > 0)
        {
            $passerr = DB::table('appusers')
            ->where('appusers.email','=', $email)
            ->where('appusers.password','=',!Hash::check($password, $register_data[0]->password))
            ->count(); 
            if($passerr > 0)
            {
                $login = DB::table('appusers')
                ->where('appusers.email','=', $email)
                ->where('appusers.password','=',!Hash::check($password, $register_data[0]->password))
                ->count();
                if($login == 1)
                {   
                    $profileId = $register_data[0]->profileid;
                    if($profileId > 1000)
                    {
                       $login_result = DB::table('appusers')
                        ->leftjoin('appuser_nutrition_profiles','appuser_nutrition_profiles.profileid','=','appusers.profileid')
                        ->where('appusers.email',$email)->where('appusers.password',!Hash::check($password, $register_data[0]->password))
                        ->get();
                    }
                    elseif($profileId < 1000)
                    {
                        $login_result = DB::table('appusers')
                            ->leftjoin('profiles','profiles.ref','=','appusers.profileid')
                            ->where('appusers.email',$email)->where('appusers.password',!Hash::check($password, $register_data[0]->password))
                            ->get();
                    }
                    else{
                        $login_result = DB::table('appusers')
                            ->where('email',$email)->where('password',!Hash::check($password, $register_data[0]->password))
                            ->get();
                    }
                    if(isset($login_result) && !empty($login_result) && $login_result != '')
                    {
                        $response = userDetailedInformation($login_result);
                    }
                    return $response;
                }
                else{
                    return -2006;
                   }
            }
            else
            {
                return -2005;
            }
        }
        else
        {
            return -2004;
        }
    }

/**
 * user signup
 * @param $email
 * @param $password
 * @param $ipaddress
 * @return array|int
 */
    function v1_appusersignup($p_storepeople)
    {
        $appuserdetails = DB::table('appusers')
            ->where('email',$p_storepeople->email)->count();
         if($appuserdetails == 0){
            if($p_storepeople->password == $p_storepeople->cfpassword)
            {
            $ref_array = array(Session::getId(),'user','email');
            $ref = md5(implode(",",$ref_array));
            $token = openssl_random_pseudo_bytes(17);
            $token = bin2hex($token);
            $appusers = new AppCustomers();
            $appusers->ref = $token;
            $appusers->email = $p_storepeople->email;
            $appusers->password = Hash::make($p_storepeople->password);
            $appusers->fullname = "";
            $appusers->handle = "";
            $appusers->date_entered = Carbon::now(new DateTimeZone('Europe/London'));
            $appusers->date_modified = Carbon::now(new DateTimeZone('Europe/London'));
            $appusers->created_by = "";
            $appusers->modified_user_id = "";
            $appusers->lastlogin = Carbon::now(new DateTimeZone('Europe/London'));
            $appusers->status = "Logged in";
            $appusers->browserstring = "";
            $appusers->browserhash = "";
            $appusers->ipaddress = "";
            $appusers->resethash = "";
            $appusers->appversion = "";
            $appusers->device = "0";
            $appusers->allergenids = "";
            $appusers->includeids = "";
            $appusers->excludeids = "";
            $appusers->likeids = "";
            $appusers->dislikeids = "";
            $appusers->profileid = "";
            $appusers->save();
            
            $userdetails['id'] = $appusers->id;
            $userdetails['email'] = $appusers->email;

            if($appusers->profileid > 1000)
            {
                $login_result = DB::table('appusers')
                    ->leftjoin('appuser_nutrition_profiles','appuser_nutrition_profiles.profileid','=','appusers.profileid')
                    ->where('appusers.id',$appusers->id)
                    ->get();
            }
            elseif($appusers->profileid < 1000)
            {
                $login_result = DB::table('appusers')
                    ->leftjoin('profiles','profiles.ref','=','appusers.profileid')
                    ->where('appusers.id',$appusers->id)
                    ->get();
            }
            else{
                $login_result = DB::table('appusers')
                    ->where('appusers.id',$appusers->id)
                    ->get();
            }
            $appuser_array = userDetailedInformation($login_result);

            return $appuser_array;
        }
        else
        {
            return -2002;
        }
     }
        else{
            return -2003;
        }
        
    }

/**
 * Save Dish By Id
 * @param $dish_id
 * @return string
 */
function v1_adddishviews($dish_id)
{
    $clicksfordish = 'UPDATE dishes SET views = IFNULL(views,0) + 1 WHERE id ="'.$dish_id.'"';
    DB::select(DB::raw($clicksfordish));
    return 'Success';
}

    function v1_getrecipes($search,$userId)
    {
        $app_id = env('recipe_app_id');
        $app_key = env('recipe_app_key');
        $primaryurl  = 'http://api.yummly.com/v1/api/recipes?q='.$search.'&_app_id='.$app_id.'&_app_key='.$app_key;
        $recipes  = json_decode(file_get_contents($primaryurl), true);        
        $recipesArry = [];
        foreach ((array)$recipes['matches'] as $recipeitem) {
            $result = DB::table('recipe')->where('recipe_id','=', $recipeitem['id'])->get()->count();
            $recipesArry[] = $recipeitem['id'];
            if($result == 0)
            {
                $newrecipe = new Recipe();
                $newrecipe->recipe_id = $recipeitem['id'];
                if(array_key_exists('sourceDisplayName',$recipeitem))
                    $newrecipe->sourceDisplayName = $recipeitem['sourceDisplayName'];
                if(array_key_exists('recipeName',$recipeitem))
                    $newrecipe->recipeName = $recipeitem['recipeName'];
                if(array_key_exists('totalTimeInSeconds',$recipeitem))
                    $newrecipe->totalTimeInSeconds = $recipeitem['totalTimeInSeconds'];
                if(array_key_exists('rating',$recipeitem))
                    $newrecipe->rating = $recipeitem['rating'];
                if(array_key_exists('ingredients',$recipeitem))
                    $newrecipe->ingredients = implode(",",$recipeitem['ingredients']);

                $attriburl  = 'http://api.yummly.com/v1/api/recipe/'.$newrecipe->recipe_id.'?_app_id='.$app_id.'&_app_key='.$app_key;
                $recipeattributes  =json_decode(file_get_contents($attriburl), true);
                if($recipeattributes <> null)
                {
                    if(array_key_exists('images',$recipeattributes))
                    {
                        $recipeimages = $recipeattributes['images'][0];
                        if(array_key_exists('hostedSmallUrl',$recipeimages))
                            $newrecipe->hostedSmallUrl = $recipeimages['hostedSmallUrl'];
                        if(array_key_exists('hostedMediumUrl',$recipeimages))
                            $newrecipe->hostedMediumUrl = $recipeimages['hostedMediumUrl'];
                        if(array_key_exists('hostedLargeUrl',$recipeimages))
                            $newrecipe->hostedLargeUrl = $recipeimages['hostedLargeUrl'];

                        if(array_key_exists('imageUrlsBySize',$recipeimages))
                        {
                            $recipesizedimages = $recipeimages['imageUrlsBySize'];
                            if(array_key_exists('90',$recipesizedimages))
                                $newrecipe->imageUrlsBySize90 = $recipesizedimages['90'];
                            if(array_key_exists('360',$recipesizedimages))
                                $newrecipe->imageUrlsBySize360 = $recipesizedimages['360'];
                        }
                    }
                    if(array_key_exists('prepTimeInSeconds',$recipeattributes))
                        $newrecipe->prepTimeInSeconds = intval($recipeattributes['prepTimeInSeconds']);
                    if(array_key_exists('totalTime',$recipeattributes))
                        $newrecipe->totalTime  = $recipeattributes['totalTime'];
                    if(array_key_exists('prepTime',$recipeattributes))
                        $newrecipe->prepTime = $recipeattributes['prepTime'];
                    if(array_key_exists('cookTime',$recipeattributes))
                        $newrecipe->cookTime = $recipeattributes['cookTime'];
                    if(array_key_exists('numberOfServings',$recipeattributes))
                        $newrecipe->numberOfServings = intval($recipeattributes['numberOfServings']);
                    if(array_key_exists('cookTimeInSeconds',$recipeattributes))
                        $newrecipe->cookTimeInSeconds = intval($recipeattributes['cookTimeInSeconds']);
                    if(array_key_exists('ingredientLines',$recipeattributes))
                        $newrecipe->ingredientLines = implode(",",$recipeattributes['ingredientLines']);
                }
                $newrecipe->save();
            }
        }    
        $result = DB::table('recipe')->whereIn('recipe_id', $recipesArry)->get();
        return $result;     
    }

/**
 * Save Recipe By Id
 * @param $recipe_id
 * @return string
 */
    function v1_addrecipeviews($recipe_id,$ingredients)
    {
        $app_id = env('recipe_app_id');
        $app_key = env('recipe_app_key');
        $primaryurl  = 'http://api.yummly.com/v1/api/recipe/'.$recipe_id.'?_app_id='.$app_id.'&_app_key='.$app_key;
        $recipeattributes  =json_decode(file_get_contents($primaryurl), true);
        $result = DB::table('recipe')->where('recipe_id','=', $recipe_id)->get()->count();        
        if($result == 0)
        {           
            addrecipes($recipeattributes,$ingredients);
            $clicksforrecipe = 'UPDATE recipe SET views = IFNULL(views,0) + 1 WHERE recipe_id ="'.$recipe_id.'"';
            DB::select(DB::raw($clicksforrecipe));
             $foodadvisroverall_rating = Recipe::where('recipe_id',$recipe_id)->select(DB::raw('foodadvisroverall_rating'))->get();
            return $foodadvisroverall_rating;
        }
        else
        {
          $clicksforrecipe = 'UPDATE recipe SET views = IFNULL(views,0) + 1 WHERE recipe_id ="'.$recipe_id.'"';
          DB::select(DB::raw($clicksforrecipe));
           $foodadvisroverall_rating = Recipe::where('recipe_id',$recipe_id)->select(DB::raw('foodadvisroverall_rating'))->get();
            return $foodadvisroverall_rating;
        }
    }


    /**
 * Adding the review for the Recipe based on the recipe id
 * @param $review
 * @return int|string
 */
    function v1_addreviewrecipe($review,$communities)
    {
        $userid = $review['userid'];
        $recipe_id = $review['recipe_id'];
        $message = $review['message'];
        $rating = $review['rating'];

        $result = DB::table('appusers_reviews_recipes')->where('userid','=', $userid)->where('recipe_id','=', $recipe_id)->get()->count();
        if($result == 0)
        {
            if(isset($review) && !empty($review))
            {
                $reviews = new AppUserRecipeReviews();
                $reviews->userid = $userid;
                $reviews->recipe_id = $recipe_id;
                $reviews->message = $message;
                $reviews->rating = $rating;
                $reviews->added_on = Carbon::now(new DateTimeZone('Europe/London'));
                $reviews->save();

                if(!empty($recipe_id))
                {
                   $affected = 'UPDATE recipe SET foodadvisroverall_rating = (select avg(rating) from appusers_reviews_recipes where recipe_id = recipe.recipe_id)  WHERE recipe_id ="'.$recipe_id.'"';
                    DB::select(DB::raw($affected));
                }
                $result =  'Added Recipe Review Successfully';
            }
        }
        else
       {
            $recipe_review = 'UPDATE appusers_reviews_recipes SET rating ="'.$rating.'",message ="'.$message.'"  WHERE userid ="'.$userid.'" and recipe_id ="'.$recipe_id.'"';
            DB::select(DB::raw($recipe_review));
           if(!empty($recipe_id))
            {
            $affected = 'UPDATE recipe SET foodadvisroverall_rating = (select avg(rating) from appusers_reviews_recipes where recipe_id = recipe.recipe_id)  WHERE recipe_id ="'.$recipe_id.'"';
            DB::select(DB::raw($affected));
            }
           $result =  'Updated Recipe Review Successfully';
       }
        $userdetails = DB::table('appusers')->where('id',$userid)->select(DB::raw('email'))->first();
        $useremail = $userdetails->email;
        $shareuserrecipereview = DB::table('sharing')
            ->where('sharing.userid',$userid)
            ->where('sharing.eateryorrecepieid',$recipe_id)
            ->where('sharing.type','R')
            ->get();
        
        if(count($shareuserrecipereview) == 0)
        {
            $usershares = new Sharing();
            $usershares->userid=$userid;
            $usershares->eateryorrecepieid = $recipe_id;
            $usershares->type = 'R';
            $usershares->rating = $rating;
            $usershares->is_visible = 1;
            $usershares->email = $useremail;
            $usershares->comments = $message;
            $usershares->groups = isset($communities)?$communities:'';
            $usershares->sharedate = Carbon::now(new DateTimeZone('Europe/London'));
            $usershares->modifieddate = Carbon::now(new DateTimeZone('Europe/London'));
            $usershares->save();
            $shareid = $usershares->id;

            if($communities != '')
            {
                $communitiesResult = DB::table('communities')->whereIn('cname',explode(",",$communities))->pluck('ref')->toArray();
                foreach($communitiesResult as $community)
                {
                    $usercommunities = new ShareOnUserCommunities();
                    $usercommunities->shareid = $shareid;
                    $usercommunities->userid = $userid;
                    $usercommunities->communityid = $community;
                    $usercommunities->save();
                }
            }
        }
        else
        {
            foreach($shareuserrecipereview as $share)
            {
                $shareId = $share->id;
                $usershares = Sharing::find($shareId);
                $usershares->is_visible = 0;
                $usershares->modifieddate = Carbon::now(new DateTimeZone('Europe/London'));
                $usershares->update();
                ShareOnUserCommunities::where('shareid',$shareId)->where('userid',$userid)->delete();
            }


            $usershares = new Sharing();
            $usershares->userid=$userid;
            $usershares->eateryorrecepieid = $recipe_id;
            $usershares->type = 'R';
            $usershares->rating = $rating;
            $usershares->is_visible = 1;
            $usershares->email = $useremail;
            $usershares->comments = $message;
            $usershares->groups = isset($communities)?$communities:'';
            $usershares->sharedate = Carbon::now(new DateTimeZone('Europe/London'));
            $usershares->modifieddate = Carbon::now(new DateTimeZone('Europe/London'));
            $usershares->save();
            $shareid = $usershares->id;
            if($communities != '')
            {
                $communitiesResult = DB::table('communities')->whereIn('cname', explode(",", $communities))->pluck('ref')->toArray();
                foreach ($communitiesResult as $community) {
                    $usercommunities = new ShareOnUserCommunities();
                    $usercommunities->shareid = $shareid;
                    $usercommunities->userid = $userid;
                    $usercommunities->communityid = $community;
                    $usercommunities->save();
                }
            }
        }
        return $result;
    }

    /**
 * Add User Shopping Cart
 * @param $userid
 * @param $ingredients
 * @return int|string
 */
    function v1_usershoppingcart($userid,$ingredients)
    {
        $user_shopping_count = UserShoppingCart::where('appuserid',$userid)
        ->where('ingredients',$ingredients)->count();
        if($user_shopping_count == 0)
        {
            $usershopingcart = new UserShoppingCart();
            $usershopingcart->appuserid = $userid;
            $usershopingcart->ingredients = $ingredients;
            $usershopingcart->date_entered = Carbon::now(new DateTimeZone('Europe/London'));
            $usershopingcart->save();

            $sql = 'select * from appusers where id="'.$userid.'"';
            $email = DB::select(DB::raw($sql));  
            $frommail = env('MAIL_USERNAME');          
            $data = array('name'=>$email[0]->fullname,'email'=> $email[0]->email,'ingredientslist'=> $usershopingcart->ingredients,'fromemail'=>$frommail );          
            Mail::send('layouts.ingredientslist', $data, function($m) use ($data) {
                $m->from('noreply@tiffin.com', 'FoodAdvisr');
                $m->to($data['email'])->subject('Your Ingredients List!');
            });
            return 'Added Shopping Cart Successfully';
        }
        else
            return -2007;             
    }

     /**
 * Get User Shopping Cart
 * @param $userid
 * @return int
 */
    function v1_getusershoppingcart($userid)
    {
        $getusershopping = UserShoppingCart::where('appuserid',$userid)->get();
        return $getusershopping;     
    }

/**
 * Adding the Favourite Eatery By User
 * @param $userid
 * @param $eatery_id
 * @return int|string
 */
    function v1_adduserfavouriteeatery($userid, $eatery_id)
    {
        $user_count = AppCustomers::where('id',$userid)->count();
        if($user_count == 0)
            return -2009;
        $eatery_count = Eateries::where('id',$eatery_id)->count();
        if($eatery_count == 0)
            return -2008;
            
        $user_insert_count = AppUsersFavouriteEateries::where('userid',$userid)
        ->where('eatery_id',$eatery_id)->count();
        if($user_insert_count == 0)
        {
            $appusersfavouriteeatery = new AppUsersFavouriteEateries();
            $appusersfavouriteeatery->userid = $userid;
            $appusersfavouriteeatery->eatery_id = $eatery_id;
            $appusersfavouriteeatery->save();
            return 'Added Favourite Eatery Successfully';
        }
        else
            return -2010;
    }

/**
 * Removing the Favourite Eatery by User
 * @param $userid
 * @param $eatery_id
 * @return int|string
 */
    function v1_removeuserfavouriteeatery($userid, $eatery_id)
    {
        $user_count = AppCustomers::where('id',$userid)->count();
        if($user_count == 0)
            return -2012;
        $eatery_count = Eateries::where('id',$eatery_id)->count();
        if($eatery_count == 0)
            return -2011;

        $user_remove_count = AppUsersFavouriteEateries::where('userid',$userid)
        ->where('eatery_id',$eatery_id)->count();
        if($user_remove_count == 0)
            return -2013;
        else
        {
           $result = DB::table('appusers_favourite_eateries')->where('userid', $userid)->where('eatery_id',$eatery_id)->delete();
            return 'Removed Favourite Eatery Successfully';
        }
    }

/**
 * Get User Favouite Eateries
 * @param $userid
 * @return mixed
 */
    function v1_getuserfavouriteeateries($userid)
    {
        $sql  = "select eatery_id from appusers_favourite_eateries where userid='". $userid."'";
        $result = DB::select( DB::raw($sql));
        return $result;
    }    
/**
 * Add User Favourite Recipe
 * @param $userid
 * @param $recipe_id
 * @return mixed
 */    
    function v1_adduserfavouriterecipe($userid, $recipe_id,$ingredients)
    {
        $user_count = AppCustomers::where('id',$userid)->count();
        if($user_count == 0)
            return -2014;

        $app_id = env('recipe_app_id');
        $app_key = env('recipe_app_key');
        $primaryurl  = 'http://api.yummly.com/v1/api/recipe/'.$recipe_id.'?_app_id='.$app_id.'&_app_key='.$app_key;
        $recipeattributes  = json_decode(file_get_contents($primaryurl), true);
        $recipe_count = Recipe::where('recipe_id',$recipe_id)->count();
        if($recipe_count == 0)
        {
                addrecipes($recipeattributes,$ingredients);
                $user_reciepe_insert_count = AppUsersFavouriteRecipes::where('userid',$userid)
                ->where('recipe_id',$recipe_id)->count();
                if($user_reciepe_insert_count == 0)
                {
                    $appusersfavouriterecipe = new AppUsersFavouriteRecipes();
                    $appusersfavouriterecipe->userid = $userid;
                    $appusersfavouriterecipe->recipe_id = $recipe_id;
                    $appusersfavouriterecipe->save();
                    return 'Added Favourite Recipe Successfully';
                }
                else
                {
                    return -2015;
                }
        }
        else
        {
         $user_reciepe_insert_count = AppUsersFavouriteRecipes::where('userid',$userid)
        ->where('recipe_id',$recipe_id)->count();
            if($user_reciepe_insert_count == 0)
            {
                $appusersfavouriterecipe = new AppUsersFavouriteRecipes();
                $appusersfavouriterecipe->userid = $userid;
                $appusersfavouriterecipe->recipe_id = $recipe_id;
                $appusersfavouriterecipe->save();
                return 'Added Favourite Recipe Successfully';
            }
            else
                {
                    return -2015;
                }
        }
           

            
        
    }

    /**
 * Removing the Favourite Recipe by User
 * @param $userid
 * @param $eatery_id
 * @return int|string
 */
    function v1_removeuserfavouriterecipe($userid, $recipe_id)
    {
        $user_count = AppCustomers::where('id',$userid)->count();
        if($user_count == 0)
            return -2017;
        $recipe_count = Recipe::where('recipe_id',$recipe_id)->count();
        if($recipe_count == 0)
            return -2016;

        $user_remove_count = AppUsersFavouriteRecipes::where('userid',$userid)
        ->where('recipe_id',$recipe_id)->count();
        if($user_remove_count == 0)
            return -2018;
        else
        {
           $result = DB::table('appusers_favourite_recipes')->where('userid', $userid)->where('recipe_id',$recipe_id)->delete();
            return 'Removed Favourite Eatery Successfully';
        }
    }

    /**
 * Get User Favouite Reciepes
 * @param $userid
 * @return mixed
 */
    function v1_getuserfavouriterecipes($userid)
    {
        $sql  = "select appusers_favourite_recipes.recipe_id,recipe.recipeName,hostedSmallUrl,rating,foodadvisroverall_rating,views,ingredients from appusers_favourite_recipes join recipe on recipe.recipe_id = appusers_favourite_recipes.recipe_id  where userid='". $userid."'";
        $result = DB::select( DB::raw($sql));
        return $result;
    }

    /**
 * Add Recipe To User Removed List
 * @param $userid
 * @param $recipe_id
 * @return string
 */
    function v1_addrecipetouserremovedlist($userid,$recipe_id)
    {
     $user_remove_count = AppUsersRemovedRecipes::where('userid',$userid)
        ->where('recipe_id',$recipe_id)->count();
      if($user_remove_count == 0)
      {
       $removefavouriteeateries = new AppUsersRemovedRecipes();
       $removefavouriteeateries->userid = $userid;
       $removefavouriteeateries->recipe_id = $recipe_id;
       $removefavouriteeateries->deleted_on =  Carbon::now(new DateTimeZone('Europe/London'));
       $removefavouriteeateries->save();
       return 'Added Recipe To User Removed List Successfully';
       }
       else 
        return 'Already Added Recipe To User Removed List';

    }

    /**
 * Delete Recipe From User Removed List
 * @param $userid
 * @param $recipe_id
 * @return int|string
 */
    function v1_deleterecipefromuserremovedlist($userid,$recipe_id)
    {
        $result = DB::table('appusers_removed_recipes')->where('userid', $userid)->where('recipe_id',$recipe_id)->delete();
        return "Deleted Recipe From User Removed List Successfully!";

    }

    /**
 * Get User Removed Recipe
 * @param $userid
 * @return int
 */

   function v1_getuserremovedrecipies($userid)
    {
      $sql = "select recipe.recipe_id,recipe.recipeName from appusers_removed_recipes join recipe on appusers_removed_recipes.recipe_id = recipe.recipe_id where userid=".$userid."";
      $v1_getuserremovedrecipes = DB::select( DB::raw($sql));
      return $v1_getuserremovedrecipes;
    }

/**
 * Getting App User Allergens from settings
 * @param $userid, $allergens
 * @return string
 */
    function v1_updateuserallergensbyuserid($userid,$allergens)
    {
        $allergens_ids = implode(",",DB::table('allergens')->where('type','A')->whereIn('title',explode(",",$allergens))->pluck('ref')->toArray());

        $userallergensUpdateQuery = 'update appusers set allergenids="'.$allergens_ids.'", allergens="'.$allergens.'" where id="'.$userid.'"';
        DB::select(DB::raw($userallergensUpdateQuery));

        return 'Allergens has been updated';
    }

/**
 * Gettinf the App User Includes Settings
 * @param $userid, $includes
 * @return string
 */

    function v1_updateuserincludesbyuserid($userid,$includes,$includesString)
    {

        $includeStringresult = implode(",",DB::table('ingredients')->whereIn('description',explode(",",$includes))->pluck('id')->toArray());

        $userincludesUpdateQuery = "update appusers set includeids='".$includeStringresult."' where id='".$userid."';";

        DB::select(DB::raw($userincludesUpdateQuery));



        $includesArray = explode("-",$includesString) ;
        AppuserProfileSettings::where('type','L')->where('appuserid',$userid)->delete();
        foreach($includesArray as $include){
            $inArray = explode(",",$include);

            $title = str_replace(",", "", str_replace("'", "", $inArray[0]));
            //$breakdown = str_replace("'", "", $inArray[1]);
            $breakdown = str_replace(",'", "'", $include);

            $appuserprofilesettings = new AppuserProfileSettings();
            $appuserprofilesettings->type='L';
            $appuserprofilesettings->appuserid=$userid;
            $appuserprofilesettings->title=$title;
            $appuserprofilesettings->breakdown=$breakdown;
            if($title != ''){
                $appuserprofilesettings->save();
            }
        }

        return 'Includes has been updated';
    }

/**
 * Gettinf the App User Excludes Settings
 * @param $userid, $includes
 * @return string
 */

    function v1_updateuserexcludesbyuserid($userid,$excludes,$excludesString)
    {

        $excludeStringresult = implode(",",DB::table('ingredients')->whereIn('description',explode(",",$excludes))->pluck('id')->toArray());
        //return $excludeStringresult;
        //return DB::table('ingredients')->whereIn('description',explode(",",$excludes))->pluck('description')->toArray();

        $userexcludesUpdateQuery = 'update appusers set excludeids="'.$excludeStringresult.'" where id="'.$userid.'"';


        DB::select(DB::raw($userexcludesUpdateQuery));

        AppuserProfileSettings::where('type','I')->where('appuserid',$userid)->delete();
        $excludesArray = explode("-",$excludesString) ;
        foreach($excludesArray as $exclude){
            $exArray = explode(":",$exclude);

            $title = str_replace(",", "", str_replace("'", "", $exArray[0]));
            //$breakdown = str_replace("'", "", $exArray[1]);
            $breakdown = str_replace(",'", "'", $exclude);

            $appuserprofilesettings = new AppuserProfileSettings();
            $appuserprofilesettings->type='I';
            $appuserprofilesettings->appuserid=$userid;
            $appuserprofilesettings->title=$title;
            $appuserprofilesettings->breakdown=$breakdown;
            if($title != ''){
                $appuserprofilesettings->save();
            }
        }


        return 'Excludes has been updated';
    }

/**
 * Gettinf the App User Likes Settings
 * @param $userid, $likes
 * @return string
 */

    function v1_updateuserlikesbyuserid($userid,$likes,$likesString)
    {

        $likeStringresult = implode(",",DB::table('ingredients')->whereIn('description',explode(",",$likes))->pluck('id')->toArray());
        $userlikesUpdateQuery = "update appusers set likeids='".$likeStringresult."' where id='".$userid."';";
        DB::select(DB::raw($userlikesUpdateQuery));

        $likesArray = explode("-",$likesString) ;
        AppuserProfileSettings::where('type','L')->where('appuserid',$userid)->delete();
        foreach($likesArray as $like){
            $inArray = explode(":",$like);

            $title = str_replace(",", "", str_replace("'", "", $inArray[0]));
            //$breakdown = str_replace("'", "", $inArray[1]);
            $breakdown = str_replace(",'", "'", $like);

            $appuserprofilesettings = new AppuserProfileSettings();
            $appuserprofilesettings->type='L';
            $appuserprofilesettings->appuserid=$userid;
            $appuserprofilesettings->title=$title;
            $appuserprofilesettings->breakdown=$breakdown;
            if($title != ''){
                $appuserprofilesettings->save();
            }
        }

        return 'Likes has been updated';
    }

    /**
 * Gettinf the App User DisLikes Settings
 * @param $userid, $dislikes
 * @return string
 */

    function v1_updateuserdislikesbyuserid($userid,$dislikes,$dislikesString)
    {

        $dislikesStringresult = implode(",",DB::table('ingredients')->whereIn('description',explode(",",$dislikes))->pluck('id')->toArray());

        $userdislikesUpdateQuery = 'update appusers set dislikeids="'.$dislikesStringresult.'" where id="'.$userid.'"';

        DB::select(DB::raw($userdislikesUpdateQuery));

        AppuserProfileSettings::where('type','D')->where('appuserid',$userid)->delete();
        $dislikesArray = explode("-",$dislikesString) ;
        foreach($dislikesArray as $dislike){
            $exArray = explode(":",$dislike);

            $title = str_replace(",", "", str_replace("'", "", $exArray[0]));
            //$breakdown = str_replace("'", "", $exArray[1]);
            $breakdown = str_replace(",'", "'", $dislike);

            $appuserprofilesettings = new AppuserProfileSettings();
            $appuserprofilesettings->type='D';
            $appuserprofilesettings->appuserid=$userid;
            $appuserprofilesettings->title=$title;
            $appuserprofilesettings->breakdown=$breakdown;
            if($title != ''){
                $appuserprofilesettings->save();
            }
        }

        return 'Dislikes has been updated';
    }

/**
 * Getting the App User Profiles
 * @param $userid, $profilename
 * @return string
 */
    function v1_updateuserprofilebyuserid($userid,$profile)
    {
        $profileSettings = DB::table('profiles')->where('name',$profile)->get();
        $profileId = $profileSettings[0]->ref;

        $updateAppuserProfile = 'update appusers set profileid="'.$profileId.'" where id='.$userid.';';
        DB::select(DB::raw($updateAppuserProfile));

        return 'Updated the User profile';
    }
/**
 * Getting the App User shares
 * @param $userid, $dishorrecipeId
 * @return array
 */
function v1_getusersharesbyuserid($userid)
{
    $usersharesdetails = DB::table('sharing')->where('userid',$userid)->where('is_visible','1')->get();
    $result = [];
    $i=0;
    foreach($usersharesdetails as $share)
    {
        if($share->type == 'E')
        {
            $getEateryInfo = DB::table('eateries')->where('id',$share->eateryorrecepieid)->first();
            $result[$i]['name'] = $getEateryInfo->business_name;
            $result[$i]['img'] = "http://qafoodadvisr.fire42.com/".$getEateryInfo->logo_path;
            $result[$i]['sharedate'] = $share->sharedate;
            $result[$i]['comments'] = $share->comments;
            $result[$i]['rating'] = $share->rating;
            $result[$i]['groups'] = $share->groups;
            $result[$i]['type'] = "E";
        }
        elseif($share->type == 'R')
        {
            $getRecipeInfo = DB::table('recipe')->where('recipe_id',$share->eateryorrecepieid)->first();
            $result[$i]['name'] = $getRecipeInfo->recipeName;
            $result[$i]['img'] = $getRecipeInfo->imageUrlsBySize90;
            $result[$i]['sharedate'] = $share->sharedate;
            $result[$i]['comments'] = $share->comments;
            $result[$i]['rating'] = $share->rating;
            $result[$i]['groups'] = $share->groups;
            $result[$i]['type'] = "R";
        }
        elseif($share->type == 'P')
        {
            $getProductInfo = DB::table('_products_import2')->where('barcode_number',$share->barcode)->first();
            $result[$i]['name'] = $getProductInfo->product_title;
            $result[$i]['img'] = md5($share->barcode.'.xfa8flP4K03LsT3jeya3O38FCMaE3p').'.jpg';
            $result[$i]['sharedate'] = $share->sharedate;
            $result[$i]['comments'] = $share->comments;
            $result[$i]['rating'] = $share->rating;
            $result[$i]['groups'] = $share->groups;
            $result[$i]['type'] = "P";
        }
        $i++;
    }
    $shareArray = $result;

    return $shareArray;
}

/**
 * Getting the Nutrition values
 * @param $userid,$nutrition values
 * @return string
 */
    function v1_addusernutritionbyuserid($userid,$nutrition)
    {
        $calories = $nutrition['calories'];
        $totalfat = $nutrition['totalfat'];
        $saturatedfat = $nutrition['saturatedfat'];
        $salt = $nutrition['salt'];
        $sugar = $nutrition['sugar'];
        $protien = $nutrition['protien'];
        $carbs = $nutrition['carbs'];
        $fibre = $nutrition['fibre'];
        $cholesterol = $nutrition['cholesterol'];

        $appuserNutrition = DB::table('appuser_nutrition_profiles')
            ->where('appuserid',$userid)->select('id')->first();
        if($appuserNutrition != '')
        {
            $nutritionId = $appuserNutrition->id;
            $usernutrition = AppuserNutritionProfiles::find($nutritionId);
            $usernutrition->appuserid = $userid;
            $usernutrition->profileid = '1001';
            $usernutrition->name = '';
            $usernutrition->calories = $calories;
            $usernutrition->totalfat = $totalfat;
            $usernutrition->saturatedfat = $saturatedfat;
            $usernutrition->salt = $salt;
            $usernutrition->sugar = $sugar;
            $usernutrition->protien = $protien;
            $usernutrition->carbs = $carbs;
            $usernutrition->fibre = $fibre;
            $usernutrition->cholesterol = $cholesterol;
            $usernutrition->ingredients = '';
            $usernutrition->likes = '';
            $usernutrition->display = 'yes';
            $usernutrition->update();

            $appusers = AppCustomers::find($userid);
            $appusers->profileid = 1001;
            $appusers->update();

            $result = 'Updated Nutritional information';
        }
        else
        {
            $usernutrition = new AppuserNutritionProfiles();
            $usernutrition->appuserid = $userid;
            $usernutrition->profileid = '1001';
            $usernutrition->name = '';
            $usernutrition->calories = $calories;
            $usernutrition->totalfat = $totalfat;
            $usernutrition->saturatedfat = $saturatedfat;
            $usernutrition->salt = $salt;
            $usernutrition->sugar = $sugar;
            $usernutrition->protien = $protien;
            $usernutrition->carbs = $carbs;
            $usernutrition->fibre = $fibre;
            $usernutrition->cholesterol = $cholesterol;
            $usernutrition->ingredients = '';
            $usernutrition->likes = '';
            $usernutrition->display = 'yes';
            $usernutrition->save();


            $appusers = AppCustomers::find($userid);
            $appusers->profileid = 1001;
            $appusers->update();

            $result = 'Added Nutritional information';
        }
        return $result;
    }
/**
 * Adding the Life style choices to the app users
 * @param $userid, $lifestylechoicesids
 * @return string
 */
    function v1_updateuserlifestylechoicesbyuserid($userid,$lifestylechoicesids)
    {
        $lifestylechoicesStringresult = implode(",",DB::table('lifestyle_choices')->whereIn('description',explode(",",$lifestylechoicesids))->pluck('id')->toArray());
        
        $appuser_lifestylechoices_count = AppuserLifeStyleChoices::where('appuserid','=',$userid)->delete();
        
            $inArraylifestylechoices = explode(',', $lifestylechoicesStringresult);
            foreach($inArraylifestylechoices as $lifestylechoices){               
                $appuser_lifestylechoices = new AppuserLifeStyleChoices();
                $appuser_lifestylechoices->appuserid = $userid;
                $appuser_lifestylechoices->lifestylechoiceid = $lifestylechoices;
                $appuser_lifestylechoices->is_visible = 1;
                $appuser_lifestylechoices->Save();           
            }
         return 'LifestyleChoices has been updated';
    }

/**
 * Adding the Communities to the app users
 * @param $userid, $communities
 * @return string
 */
    function v1_updateusercommunitiesbyuserid($userid,$communities)
    {
        $communitiesresult = DB::table('communities')->whereIn('cname',explode(",",$communities))->pluck('ref')->toArray();
        $appuser_communities_count = AppuserCommunities::where('appuserid','=',$userid)->count();

            if($appuser_communities_count == 0)
            {
                foreach($communitiesresult as $community) {
                    $appuser_community = new AppuserCommunities();
                    $appuser_community->appuserid = $userid;
                    $appuser_community->communityid = $community;
                    $appuser_community->is_visible = 1;
                    $appuser_community->Save();
                    $result = 'Added Your Communities Successfully!';
                }
            }
            else
            {
                AppuserCommunities::where('appuserid','=',$userid)->delete();
                foreach($communitiesresult as $community)
                {
                    $appuser_community = new AppuserCommunities();
                    $appuser_community->appuserid = $userid;
                    $appuser_community->communityid = $community;
                    $appuser_community->is_visible = 1;
                    $appuser_community->Save();
                    $result = 'Updated User LifestyleChoices Successfully!';
                }
            }
        return $result;
    }
?>