<?php

namespace App\Services\General;
use App\Repositories\Leads\LeadsInterface;

class LeadServices
{

 protected $newsRepo;
    public function __construct(LeadsInterface $newsRepo)
    {
        $this->newsRepo = $newsRepo;
    }

    public function leadList($data)
    {
        try
        {
            $result = $this->newsRepo->leadList($data);
            if(!empty($result)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Retrieved lead list');
                $res['data'] = $result;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please try again');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function leadDetails(array $data)
    {
        try
        {
            $addnews = $this->newsRepo->leadDetails($data);
            if($addnews === 201)
            {
                $res['status_code'] = 201;
                $res['message'] = trans('');
                $res['data'] = false;
                return $res;
            }
            elseif (!empty($addnews)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Retrieved lead details');
                $res['data'] = $addnews;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please fill the all manditory fileds');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function getMetaDataList($data)
    {
        try
        {
            $result = $this->newsRepo->getMetaDataList($data);
            if(!empty($result)) {
                $res['status_code'] = 200;
                $res['message'] = trans('data retrieved');
                $res['data'] = $result;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('data not retrieved');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }


    public function addCategory(array $data)
    {
        try
        {
            $addnews = $this->newsRepo->addCategory($data);
            if($addnews === 201)
            {
                $res['status_code'] = 201;
                $res['message'] = trans('Already added this category');
                $res['data'] = false;
                return $res;
            }
            elseif (!empty($addnews)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Category created successfully');
                $res['data'] = $addnews;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please fill the all manditory fileds');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function updateCategory(array $data)
    {
        try
        {
            $addnews = $this->newsRepo->updateCategory($data);
            if($addnews === 201)
            {
                $res['status_code'] = 201;
                $res['message'] = trans('Category details not found');
                $res['data'] = false;
                return $res;
            }
            elseif (!empty($addnews)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Category updated successfully');
                $res['data'] = $addnews;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please fill the all manditory fileds');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function deleteCategory(array $data)
    {
        try
        {
            $addnews = $this->newsRepo->deleteCategory($data);
            if($addnews === 201)
            {
                $res['status_code'] = 201;
                $res['message'] = trans('Category details not found');
                $res['data'] = false;
                return $res;
            }
            elseif (!empty($addnews)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Category deleted successfully');
                $res['data'] = $addnews;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please fill the all manditory fileds');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function categoryDetails($data)
    {
        try
        {
            $result = $this->newsRepo->categoryDetails($data);
            if($result === 201)
            {
                $res['status_code'] = 201;
                $res['message'] = trans('Category details not found');
                $res['data'] = false;
                return $res;
            }
            elseif(!empty($result)) {
                $res['status_code'] = 200;
                $res['message'] = trans('retrieved category data Successfully');
                $res['data'] = $result;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please try again');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function categoryList($data)
    {
        try
        {
            $result = $this->newsRepo->categoryList($data);
            if(!empty($result)) {
                $res['status_code'] = 200;
                $res['message'] = trans('retrieved category data Successfully');
                $res['data'] = $result;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please try again');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }
}
