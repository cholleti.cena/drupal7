<?php

namespace App\Services\Admin;

use App\Repositories\Category\CategoryInterface;

class CategoryServices
{
	protected $authRepo;
    public function __construct(CategoryInterface $authRepo)
    {
        $this->authRepo = $authRepo;
    }

    public function addCategory(array $data)
    {
        try
        {
            $addnews = $this->authRepo->addCategory($data);
            if($addnews === 201)
            {
                $res['status_code'] = 201;
                $res['message'] = trans('Already added this category');
                $res['data'] = false;
                return $res;
            }
            elseif (!empty($addnews)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Category created successfully');
                $res['data'] = $addnews;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please fill the all manditory fileds');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function updateCategory(array $data)
    {
        try
        {
            $addnews = $this->authRepo->updateCategory($data);
            if($addnews === 201)
            {
                $res['status_code'] = 201;
                $res['message'] = trans('Category details not found');
                $res['data'] = false;
                return $res;
            }
            elseif (!empty($addnews)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Category updated successfully');
                $res['data'] = $addnews;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please fill the all manditory fileds');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function deleteCategory(array $data)
    {
        try
        {
            $addnews = $this->authRepo->deleteCategory($data);
            if($addnews === 201)
            {
                $res['status_code'] = 201;
                $res['message'] = trans('Category details not found');
                $res['data'] = false;
                return $res;
            }
            elseif (!empty($addnews)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Category deleted successfully');
                $res['data'] = $addnews;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please fill the all manditory fileds');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function categoryDetails($data)
    {
        try
        {
            $result = $this->authRepo->categoryDetails($data);
            if($result === 201)
            {
                $res['status_code'] = 201;
                $res['message'] = trans('Category details not found');
                $res['data'] = false;
                return $res;
            }
            elseif(!empty($result)) {
                $res['status_code'] = 200;
                $res['message'] = trans('retrieved category data Successfully');
                $res['data'] = $result;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please try again');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function categoryList($data)
    {
        try
        {
            $result = $this->authRepo->categoryList($data);
            if(!empty($result)) {
                $res['status_code'] = 200;
                $res['message'] = trans('retrieved category data Successfully');
                $res['data'] = $result;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please try again');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

}