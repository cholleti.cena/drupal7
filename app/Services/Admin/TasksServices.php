<?php

namespace App\Services\Admin;
use App\Repositories\Tasks\TasksInterface;

class TasksServices
{

 protected $tasksRepo;
    public function __construct(TasksInterface $tasksRepo)
    {
        $this->tasksRepo = $tasksRepo;
    }

    public function tasksList($data)
    {
        try
        {
            $result = $this->tasksRepo->tasksList($data);
            if(!empty($result)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Retrieved task list');
                $res['data'] = $result;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please try again');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function tasksDetails(array $data)
    {
        try
        {
            $addnews = $this->tasksRepo->tasksDetails($data);
            if($addnews === 201)
            {
                $res['status_code'] = 201;
                $res['message'] = trans('');
                $res['data'] = false;
                return $res;
            }
            elseif (!empty($addnews)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Retrieved lead details');
                $res['data'] = $addnews;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please fill the all manditory fileds');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }
}
