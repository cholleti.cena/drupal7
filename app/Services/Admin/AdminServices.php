<?php

namespace App\Services\Admin;

use App\Repositories\Admin\AdminInterface;

class AdminServices
{
	protected $authRepo;
    public function __construct(AdminInterface $authRepo)
    {
        $this->authRepo = $authRepo;
    }

    public function adminLogin(array $data)
    {
        try
        {
            $updateProfile = $this->authRepo->adminLogin($data);
            if($updateProfile === 201)
            {
                $res['status_code'] = 201;
                $res['message'] = trans('login_fail');
                $res['data'] = false;
                return $res;
            }
            elseif (!empty($updateProfile)) {
                $res['status_code'] = 200;
                $res['message'] = trans('login_success');
                $res['data'] = $updateProfile;
                return $res;
            }
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function manageDashBoard()
    {
        try
        {
            if (!empty($updateProfile = $this->authRepo->manageDashBoard())) {
                $res['status_code'] = 200;
                $res['message'] = trans('Data retrieved');
                $res['data'] = $updateProfile;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Data not retrieved');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

}