<?php

namespace App\Services\Admin;
use App\Repositories\Leads\LeadsInterface;

class LeadServices
{

 protected $newsRepo;
    public function __construct(LeadsInterface $newsRepo)
    {
        $this->newsRepo = $newsRepo;
    }

    public function leadList($data)
    {
        try
        {
            $result = $this->newsRepo->leadList($data);
            if(!empty($result)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Retrieved lead list');
                $res['data'] = $result;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please try again');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function leadUpdate($data)
    {
        try
        {
            $result = $this->newsRepo->leadUpdate($data);
            if(!empty($result)) {
                $res['status_code'] = 200;
                $res['message'] = trans('lead updated');
                $res['data'] = $result;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please try again');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function leadDetails(array $data)
    {
        try
        {
            $addnews = $this->newsRepo->leadDetails($data);
            if($addnews === 201)
            {
                $res['status_code'] = 201;
                $res['message'] = trans('');
                $res['data'] = false;
                return $res;
            }
            elseif (!empty($addnews)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Retrieved lead details');
                $res['data'] = $addnews;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please fill the all manditory fileds');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function materialDetails(array $data)
    {
        try
        {
            $addnews = $this->newsRepo->materialDetails($data);
            if($addnews === 201)
            {
                $res['status_code'] = 201;
                $res['message'] = trans('');
                $res['data'] = false;
                return $res;
            }
            elseif (!empty($addnews)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Retrieved lead details');
                $res['data'] = $addnews;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please fill the all manditory fileds');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }

    public function schedulesList($data)
    {
        try
        {
            $result = $this->newsRepo->schedulesList($data);
            if(!empty($result)) {
                $res['status_code'] = 200;
                $res['message'] = trans('Retrieved schedules list');
                $res['data'] = $result;
                return $res;
            }
            $res['status_code'] = 201;
            $res['message'] = trans('Please try again');
            $res['data'] = false;
            return $res;
       }
       catch (\Exception $e) 
       {
            $res['status_code'] = 202;
            $res['message'] = $e->getMessage();
            $res['data'] = false;
            return $res;
       }
    }
}
