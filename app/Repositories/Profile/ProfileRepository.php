<?php
namespace App\Repositories\Profile;

use App\Repositories\Profile\ProfileInterface;
use Auth;
use App\Models\User;
use App\Helpers\CommonHelper;

class ProfileRepository implements ProfileInterface
{
   private $user;
   protected $helpers;
   protected $userBasicProfile;
   protected $userEducations;
   protected $userOrders;

   public function __construct(User $user,CommonHelper $helpers)
   {
     $this->user = $user;
     $this->helpers = $helpers;
   }

   public function updateProfile(array $data)
   {
      $user = Auth::user();
      if(!$user)
         return false;

      $email_validator = $this->user::where('email','=',$data['email'])->count();
      if($email_validator !== 1){
         $data['email'] = $data['email'];
         $data['is_email_verified'] = 0;
      }
      else if($user->email != $data['email']){
        return 201;
      }

      $phone_validator = $this->user::where('phone','=',$data['phone'])->count();
      if($phone_validator !== 1){
         $data['phone'] = $data['phone'];
         $data['is_phone_verify'] = 0;
      }
      else if($user->phone != $data['phone']){
        return false;
      }

      $user->update($data);
      return $user;
   }

   public function updateBasicProfile(array $data)
   {
      $user = Auth::user();
      if(!$user)
         return false;

      $data['user_id'] = $user->id;
      $basic = $this->userBasicProfile::where('user_id','=',$user->id)->first();
      if(is_null($basic))
      {
         $this->userBasicProfile->create($data);
         return true;
      }

      $basic->update($data);
      return true;
   }

   public function updateEducation(array $data)
   {
       $user = Auth::user();
       if(!$user)
         return false;

       $this->userEducations::where('user_id','=',$user->id)->delete();

       if(count($data['educations']) > 0)
       {
          foreach ($data['educations'] as $key => $value) 
          {
              $value['user_id'] = $user->id;
              $this->userEducations->create($value);
          }
          return true;
       }
       return false;
   }

   public function changePassword(array $data)
   {
      $user = Auth::user();
      if(!$user)
         return false;

      $oldpassword = $this->user::where('id','=',$user->id)
        ->where('password','=',!\Hash::check($data['current_password'], $user->password))->count();
      if($oldpassword == 0){
         return 201;
      }
      if($data['current_password'] === $data['new_password']){
         return 202;
      }
      $data['password'] = bcrypt($data['new_password']);
      $user->update($data);
      return true;
   }

   public function profilePic(array $data)
   {
      $user = Auth::user();
      if(!$user)
         return false;

      $data['profile_pic'] = $this->helpers->uploadFiles($data['profile_pic'],'users',$user->phone,0);
      $user->update($data);
      return true;
   }

   public function getUserDetails($data)
   {
      if(isset($data['user_id']))
      {
          $user = $this->user->find($data['user_id']);
          if(is_null($user))
          {
            return false;
          }
      }
      else
      {
        $user = Auth::user();
        if(!$user)
           return false;
      }
      $userbasic = $this->user::find($user->id)->first();
      return $user->userformat();
    }

   public function addAddress(array $data)
   {
      $user = Auth::user();
      if(!$user)
         return false;
       $data['user_id'] = $user->id;

       $validate = $this->userShipping::where('user_id','=',$user->id)
       ->where('phone','=',$data['phone'])
       ->where('pincode','=',$data['pincode'])
       ->where('address','=',$data['address'])
       ->where('city','=',$data['city'])
       ->where('state_id','=',$data['state_id'])
       ->where('address_type','=',$data['address_type'])->count();
       if($validate == 1)
            return 201;

       $this->userShipping->create($data);
       return true;
   }

   public function updateAddress(array $data)
   {
      $user = Auth::user();
      if(!$user)
         return false;
       $data['user_id'] = $user->id;

       $validate = $this->userShipping::where('user_id','=',$user->id)
       ->where('id','=',$data['address_id'])
       ->first();
       if(is_null($validate))
       {
         return 201;
       }

       $validate->update($data);
       return true;
   }

   public function deleteAddress(array $data)
   {
      $user = Auth::user();
      if(!$user)
         return false;
       $data['user_id'] = $user->id;

       $validate = $this->userShipping::where('user_id','=',$user->id)
       ->where('id','=',$data['address_id'])
       ->first();
       if(is_null($validate))
       {
         return 201;
       }

       $validate->delete();
       return true;
   }

   public function addressList(array $data)
   {
      $user = Auth::user();
      if(!$user)
         return false;

      $address_list = $this->userShipping::where('user_id','=',$user->id)
      ->get();
      return $address_list;
   }

   public function addressDetails(array $data)
   {
      $user = Auth::user();
      if(!$user)
         return false;

       $validate = $this->userShipping::where('user_id','=',$user->id)
       ->where('id','=',$data['address_id'])
       ->first();
       if(is_null($validate))
       {
         return false;
       }

       return $validate;
   }

   public function placeOrder(array $data)
   {
      if(isset($data['user_id']))
      {
          $user = $this->user->find($data['user_id']);
          if(is_null($user))
          {
            return false;
          }
      }
      else
      {
        $user = Auth::user();
        if(!$user)
           return false;
      }

       $data['user_id'] = $user->id;

       $validate_order = $this->userOrders::where('user_id','=',$user->id)
       ->whereNotIn('delivery_status',[4])
       ->count();
       if($validate_order == 1)
       {
         return 201;
       }

       $data['full_address'] = $validate->address.', '.$validate->city.', '.$validate->state_id->state.', '.$validate->land_mark.' - '.$validate->pincode;
       $data['city'] = $validate->city;
       $data['state_id'] = $validate->state_id->id;
       $details = $this->userOrders->create($data);

       $dat['transaction_id'] = $this->increment($details->id);
       $details->update($dat);

       return true;
   }

   public function orderDetails(array $data)
   {
      $user = Auth::user();
      if(!$user)
         return false;

       $validate_order = $this->userOrders::where('user_id','=',$user->id)
       ->with('address')
       ->where('id','=',$data['order_id'])
       ->first();
       if(is_null($validate_order))
       {
         return 201;
       }

       return $validate_order;
   }

   public function orderList(array $data)
   {
      $user = Auth::user();
      if(!$user)
         return false;

       $validate_order = $this->userOrders::where('user_id','=',$user->id)
       ->with('address')
       ->Orderby('created_at','desc')
       ->get();

       return $validate_order;
   }

   public function increment($po)
  {
      if(strlen($po) == 1):
        return '0000000'.$po;
      elseif(strlen($po) == 2):
        return '000000'.$po;
      elseif(strlen($po) == 3):
        return '00000'.$po;
      elseif(strlen($po) == 4):
        return '0000'.$po;
      elseif(strlen($po) == 5):
        return '000'.$po;
      elseif(strlen($po) == 6):
        return '00'.$po;
      elseif(strlen($po) == 7):
        return '0'.$po;
       elseif(strlen($po) == 8):
        return $po;
      else:
        return $po;
      endif;
  }

}