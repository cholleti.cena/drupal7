<?php
namespace App\Repositories\Profile;

interface ProfileInterface
{
	public function updateProfile(array $data);
	public function updateBasicProfile(array $data);
	public function updateEducation(array $data);
	public function changePassword(array $data);
	public function profilePic(array $data);
    public function getUserDetails(array $data);
}
