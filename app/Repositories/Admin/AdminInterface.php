<?php


namespace App\Repositories\Admin;


interface AdminInterface
{
    public function adminLogin(array $data);
    public function manageDashBoard();
}
