<?php

namespace App\Repositories\Admin;
use App\Repositories\Admin\AdminInterface;
use App\Models\User;
use App\Models\AdminUser;
use App\Models\Leads;
use App\Models\LeadsItems;
use App\Models\LeadsServices;
use App\Models\LeadsMaterials;
use App\Helpers\CommonHelper;
use Carbon\Carbon;
use Auth;
use Session;
use Hash;

class AdminRepository implements AdminInterface
{
	protected $adminUsers;
    protected $helpers;


    public function __construct(CommonHelper $helpers,AdminUser $adminUsers,Leads $leads,User $user)
    {
        $this->helpers = $helpers;
        $this->adminUsers = $adminUsers;
        $this->leads = $leads;
        $this->user = $user;
    }

	public function adminLogin(array $data)
    {
        $username = $data['email'];
        $user = $this->adminUsers::where(function($q)use ($username) {
                    $q->where('email', $username);
             })
        ->first();
        if ($user){
            if($user->status == 0){
                // $data['reset_code'] = $user->reset_code;
                // $subject = env('APP_NAME').'- You are almost there | Confirm your Activation';
                // \Mail::to($user->email)->send(new SendOTP($subject,$data));
                return 202;
            }
           if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
                $field = 'email';
            }

            $user = $this->adminUsers::where('email','=',$username)
            ->where('password','=',!Hash::check($data['password'], $user->password))
            ->first();
            $credentials = [$field => $username,'password' => $data['password']];
            if ($user) {
                $res['user_id'] = $user->id;
                $res['name'] = $user->name;
                $res['role_id'] = $user->role_id;
                $res['email'] = $user->email;
                $res['token'] = $user->createToken('AppName')->accessToken;
                return $res;
            }
            else{
                return false;
            }
        }
        return 201; 
    }

    public function manageDashBoard()
    {
        $now = date('2022-01-20');
        $from = date('2022-01-01');
        $to = date('2022-01-20');
        //CURDATE()
        //leads count
        $leads = $this->leads::selectRaw("leads.*")
        ->where($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), "=", $now)
        ->where('leads.lead_status','=','1')
        ->get();
        $finalData['leads_count'] = count($leads);

        //booked count
        $booked = $this->leads::selectRaw("leads.*")
        ->where($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), "=", $now)
        ->where('leads.lead_status','=','2')
        ->get();
        $finalData['booked_count'] = count($booked);

        //moves count
        $moves = $this->leads::selectRaw("leads.*")
        ->where($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), "=", $now)
        ->where('leads.lead_status','=','3')
        ->get();
        $finalData['moves_count'] = count($moves);

        //moves this month count 
        $first_day_month = $from;
        $last_day_month  = $to;
        //$first_day_month = date('Y-m-01');
        //$last_day_month  = date('Y-m-t');      
        $movesmonth = $this->leads::selectRaw("leads.*")
        ->whereBetween($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), [$first_day_month, $last_day_month])
         ->where('leads.lead_status','=','3')
        ->get();
        $finalData['moves_monthcount'] = count($movesmonth);

        //not moves this month count        
        $notmovesmonth = $this->leads::selectRaw("leads.*")
        ->whereBetween($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), [$first_day_month, $last_day_month])
          ->where('leads.lead_status','!=','3')
        ->get();
        $finalData['notmoves_monthcount'] = count($notmovesmonth);

        //avg moves this month count       
        $avgmovesmonth = $this->leads::selectRaw("leads.*")
        ->join('leads_items','leads_items.lead_id','leads.id')
        ->whereBetween($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), [$first_day_month, $last_day_month])
          ->where('leads.lead_status','=','3')
        ->sum('leads_items.price');        
        $finalData['avgmovesmonth'] = $avgmovesmonth;


        //sales leaders this month data       
        $salesleadersmonth = $this->leads::selectRaw("count(leads.id) as leadsCount,SUM(leads_items.price) as price,users.first_name")
        ->join('leads_items','leads_items.lead_id','leads.id')
        ->join('users','users.id','leads.user_id')
        ->whereBetween($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), [$first_day_month, $last_day_month])
        ->groupBy('leads.user_id')
        ->get();    
        $finalData['salesleadersmonth'] = $salesleadersmonth;

          //top referals  this month data       
          $topreferalsmonth = $this->leads::selectRaw("count(leads.id) as leadsCount,SUM(leads_items.price) as price,referrals.name")
          ->join('leads_items','leads_items.lead_id','leads.id')
          ->join('users','users.id','leads.user_id')
          ->join('referrals','referrals.id','users.referred_by')
          ->whereBetween($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), [$first_day_month, $last_day_month])
          //->sum('leads_items.price')    
          ->groupBy('users.referred_by')
          ->get();     

          
          $finalData['topreferalsmonth'] = $topreferalsmonth;

         
          //Yearly Revenue data     
          $yearStartDate=date('Y').'-01-01';
          $yearEndDate=date('Y').'-12-31';

         // $yearEndDate = date('Y-m-d');
          //$yearStartDate = date("Y-m-d",strtotime('-1 year , +1 month', strtotime($yearEndDate))) ;      

          $yearlyrevenue = $this->leads::selectRaw("SUM(leads_items.price) as price,DATE_FORMAT(leads.created_at,'%Y-%m') AS yearmonth,DATE_FORMAT(leads.created_at,'%m/%Y') AS monthyear,CASE WHEN leads.lead_status=1 THEN 'Estimated' ELSE CASE WHEN leads.lead_status=2 THEN 'Booked' ELSE CASE WHEN leads.lead_status=3 THEN 'Completed' ELSE 'Cancelled' END END END as 'type', leads.lead_status")
          ->join('leads_items','leads_items.lead_id','leads.id')
          ->whereBetween($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), [$yearStartDate, $yearEndDate])
          //->groupBy("DATE_FORMAT(leads.created_at,'%Y-%m')")
          ->groupBy("yearmonth")
          ->groupBy("leads.lead_status")
          ->get();     
          $yearStartMonth=array();
          for($i=1;$i<=12;$i++) {
              if($i<=9){
                  $i='0'.$i; 
              }
              $yearStartMonth[]= $i.'/'.date('Y');                                    
            }
           
            $yearlyfinaldata=array();
           for($j=1;$j<=4;$j++){   
			   if($j==1){
				   $name="Estimated";
			   }else if($j==2){
				   $name="Booked";
			   }else if($j==3){
				   $name="Completed";
			   }else if($j==4){
				   $name="Cancelled";
			   }
          // foreach($typesData as $type){            
            foreach($yearStartMonth as $months){               
                    $yearlyfinaldata[$name]['monthdata'][$months]=array("month"=>$months,'price'=>0,'type'=>$name);
                 
                foreach($yearlyrevenue as $revenue){
                    if($revenue['monthyear']==$months && $j==$revenue['lead_status']){                      
                         $yearlyfinaldata[$name]['monthdata'][$months]=array("month"=>$revenue['monthyear'],'price'=>$revenue['price'],'lead_status'=>$revenue['lead_status'],'type'=>$name);
                         
                    }
                }                
            }            
        }
          $finalData['yearlyrevenuedata'] = $yearlyfinaldata;
          
          $comprecntestleads = $this->leads::selectRaw("leads.*,users.first_name,leads_items.price,CASE WHEN leads.lead_status=1 THEN 'Estimated' ELSE CASE WHEN leads.lead_status=2 THEN 'Booked' ELSE CASE WHEN leads.lead_status=3 THEN 'Completed' ELSE 'Cancelled' END END END as 'type'")
           ->join('leads_items','leads_items.lead_id','leads.id')
         ->join('users','users.id','leads.user_id')
        ->where($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), "<=", $now)
        //->where('leads.lead_status','=','1')
        ->limit(5)
        ->get();   
      // echo "<pre>"; print_r($comprecntestleads);exit;     
        $finalData['comp_recntestleads'] = $comprecntestleads;
        
         $myrecntestleads = $this->leads::selectRaw("leads.*,users.first_name,leads_items.price,CASE WHEN leads.lead_status=1 THEN 'Estimated' ELSE CASE WHEN leads.lead_status=2 THEN 'Booked' ELSE CASE WHEN leads.lead_status=3 THEN 'Completed' ELSE 'Cancelled' END END END as 'type'")
         ->join('leads_items','leads_items.lead_id','leads.id')
         ->join('users','users.id','leads.user_id')
        ->where($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), "<=", $now)
        //->where('leads.lead_status','=','1')
        ->where('leads.user_id','=',Session::get("admin_id"))
        ->limit(5)
        ->get();        
        $finalData['my_recntestleads'] = $myrecntestleads;
          
         $comprecntleads = $this->leads::selectRaw("leads.*,users.first_name")
         ->join('users','users.id','leads.user_id')
        ->where($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), "<=", $now)
        ->where('leads.lead_status','=','2')
        ->limit(5)
        ->get();        
        $finalData['comp_recntleads'] = $comprecntleads;
        
         $myrecntleads = $this->leads::selectRaw("leads.*,users.first_name")
         ->join('users','users.id','leads.user_id')
        ->where($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), "<=", $now)
        ->where('leads.lead_status','=','2')
        ->where('leads.user_id','=',Session::get("admin_id"))
        ->limit(5)
        ->get();        
        $finalData['my_recntleads'] = $myrecntleads;
        
        
        $leadsweek = $this->leads::selectRaw("leads.*,leads_items.price")
       ->join('leads_items','leads_items.lead_id','leads.id')
        ->whereBetween($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), [$from, $to])
        ->get();
        
       $leadsmonth = $this->leads::selectRaw("leads.*,leads_items.price")
       ->join('leads_items','leads_items.lead_id','leads.id')
        ->whereBetween($this->leads::raw("(DATE_FORMAT(leads.created_at,'%Y-%m-%d'))"), [$first_day_month, $last_day_month])
        ->get();
        
           $companystatistics=array();
           for($j=1;$j<=4;$j++){   
			   if($j==1){
				   $name="Estimated";				  
			   }else if($j==2){
				   $name="Booked";
			   }else if($j==3){
				   $name="Completed";
			   }else if($j==4){
				   $name="Cancelled";
			   }
			    $i=0;
				$price=0;
		 $companystatistics[$name]['data']['today']=array("leads"=>$i,'price'=>$price);  
		   $wi=0;
			$wprice=0;
		  $companystatistics[$name]['data']['weekly']=array("leads"=>$wi,'price'=>$wprice);    
		   $mi=0;
			$mprice=0;
		  $companystatistics[$name]['data']['monthly']=array("leads"=>$mi,'price'=>$mprice);    
		foreach($comprecntestleads as $led){
			
			if($led['lead_status']==$j){	
				$i=$i+1;
			$price=$price+$led['price'];									
				 $companystatistics[$name]['data']['today']=array(
				 "leads"=>$i,
				 'price'=>$price);
			}			
		}	
		
		foreach($leadsweek as $weekled){			
			if($weekled['lead_status']==$j){	
				$wi=$wi+1;
			$wprice=$wprice+$weekled['price'];									
				 $companystatistics[$name]['data']['weekly']=array(
				 "leads"=>$wi,
				 'price'=>$wprice);
			}			
		}	
		
		foreach($leadsmonth as $monthled){
	
			if($monthled['lead_status']==$j){	
			$mi=$mi+1;
			$mprice=$mprice+$monthled['price'];									
				 $companystatistics[$name]['data']['monthly']=array(
				 "leads"=>$mi,
				 'price'=>$mprice);
			}			
		}			
			
			
		         
        }
         $finalData['companystatistics'] = $companystatistics;
         
         
              $mystatistics=array();
           for($j=1;$j<=4;$j++){   
			   if($j==1){
				   $name="Estimated";				  
			   }else if($j==2){
				   $name="Booked";
			   }else if($j==3){
				   $name="Completed";
			   }else if($j==4){
				   $name="Cancelled";
			   }
			    $i=0;
				$price=0;
		 $mystatistics[$name]['data']['today']=array("leads"=>$i,'price'=>$price);  
		   $wi=0;
			$wprice=0;
		  $mystatistics[$name]['data']['weekly']=array("leads"=>$wi,'price'=>$wprice);    
		   $mi=0;
			$mprice=0;
		  $mystatistics[$name]['data']['monthly']=array("leads"=>$mi,'price'=>$mprice);    
		foreach($comprecntestleads as $led){
			
			if($led['lead_status']==$j && $led['user_id']==Session::get("admin_id")){	
				$i=$i+1;
			$price=$price+$led['price'];									
				 $mystatistics[$name]['data']['today']=array(
				 "leads"=>$i,
				 'price'=>$price);
			}			
		}	
		
		foreach($leadsweek as $weekled){			
			if($weekled['lead_status']==$j && $weekled['user_id']==Session::get("admin_id")){	
				$wi=$wi+1;
			$wprice=$wprice+$weekled['price'];									
				 $mystatistics[$name]['data']['weekly']=array(
				 "leads"=>$wi,
				 'price'=>$wprice);
			}			
		}	
		
		foreach($leadsmonth as $monthled){
	
			if($monthled['lead_status']==$j && $monthled['user_id']==Session::get("admin_id")){	
			$mi=$mi+1;
			$mprice=$mprice+$monthled['price'];									
				 $mystatistics[$name]['data']['monthly']=array(
				 "leads"=>$mi,
				 'price'=>$mprice);
			}			
		}			
			
			
		         
        }
         $finalData['mystatistics'] = $mystatistics;
         
         

        return $finalData;
    }
}
