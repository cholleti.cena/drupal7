<?php

namespace App\Repositories\Category;

use App\Repositories\Category\CategoryInterface;
use App\Helpers\CommonHelper;
use App\Models\Categories;
use Auth;


class CategoryRepository implements CategoryInterface
{
	protected $categories;
    protected $helpers;


    public function __construct(CommonHelper $helpers,Categories $categories)
    {
        $this->helpers = $helpers;
        $this->categories = $categories;
    }


    public function addCategory(array $data)
    {
          $user = Auth::user();

          $data['created_by'] = $user->id;

          $validate_cat = $this->categories::where('name','=',$data['name'])->count();
          if($validate_cat > 0)
          {
            return 201;
          }
          $this->categories->create($data);
          return true;
    }

    public function updateCategory(array $data)
    {
        $user = Auth::user();

        $data['modified_by'] = $user->id;

        $news_categories = $this->categories::where('id','=',$data['category_id'])->first();
        if(is_null($news_categories))
        {
          return 201;
        }
        $news_categories->update($data);
        return true;
    }

    public function categoryDetails($data)
    {
        $newsCategory = $this->categories::where('id','=',$data['category_id'])->first();
        if(is_null($newsCategory))
        {
          return 201;
        }

        return $newsCategory;
    }

    public function deleteCategory($data)
    {
        $newsCategory = $this->categories::where('id','=',$data['category_id'])->first();
        if(is_null($newsCategory))
        {
          return 201;
        }

        $newsCategory->delete();
        return true;
    }

    public function categoryList($data)
    {
      $free_text = $data['free_text'];

      if ($data['per_page']) {
         $per_page = $data['per_page'];
      } else {
             $per_page = 10;
      }
      if ($data['page']) {
             $page = $data['page'];
      } else {
         $page = 1;
      }
      $offset = ($page - 1) * $per_page;

      $news_categories = $this->categories::selectRaw('categories.*');
      
      //$news = $news->where('news.is_active','=',1);

      if($free_text) :
       $news_categories = $news_categories->where(function ($query) use($free_text) {
          $query->orwhere('name', 'like', '%' . $free_text . '%');
          });
      endif;

      $news_categories = $news_categories->Orderby('categories.id','desc');
      $news_categories = $news_categories->skip($offset)->paginate($per_page);

      $fdata = $news_categories->map(function ($cat)
      {
          return ['id' => $cat->id , 'name' => $cat->name,'is_active' => $cat->is_active];
      });

      $datafilterInfo["total"] = $news_categories->total();
      $datafilterInfo["current_page"] = $page;
      $datafilterInfo["per_page"] = $per_page;
      $datafilterInfo["total_pages"] = ceil(($news_categories->total() / $per_page));

      $Finaldata = $datafilterInfo;
      $Finaldata['data'] = $fdata;

      return $Finaldata;
    }

}