<?php


namespace App\Repositories\Category;


interface CategoryInterface
{
    public function addCategory(array $data);
    public function updateCategory(array $data);
    public function categoryDetails(array $data);
    public function categoryList(array $data);
}
