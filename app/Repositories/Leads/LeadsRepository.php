<?php 
namespace App\Repositories\Leads;

use App\Repositories\Leads\LeadsInterface;
use App\Helpers\CommonHelper;
use Auth;
use App\Models\AdminUsers;
use App\Models\User;
use App\Models\Categories;
use App\Models\Role;
use App\Models\ServiceTypes;
use App\Models\Services;
use App\Models\Items;
use App\Models\Materials;
use App\Models\MoveSizes;
use App\Models\PhoneTypes;
use App\Models\Properties;
use App\Models\Referrals;
use App\Models\Leads;
use App\Models\LeadsItems;
use App\Models\MoveStatus;
use App\Models\LeadsMaterials;
use App\Models\LeadsServices;
use App\Models\Fleets;
use App\Models\EstimatedDetails;

class LeadsRepository implements LeadsInterface
{

 public function __construct(CommonHelper $helpers,AdminUsers $adminUsers,User $user,Categories $categories,Leads $leads,LeadsItems $leadsItems,Items $items,LeadsMaterials $leadsMaterials,LeadsServices $leadsServices,Fleets $fleets,EstimatedDetails $estimated)
   {
     $this->user = $user;
     $this->helpers = $helpers;
     $this->adminUsers = $adminUsers;
     $this->leads = $leads;
     $this->leadsItems = $leadsItems;
     $this->items = $items;
     $this->leadsMaterials = $leadsMaterials;
     $this->leadsServices = $leadsServices;
     $this->fleets = $fleets;
     $this->estimated = $estimated;
   }
  

    public function leadList($data)
    {
      $free_text = $data['free_text'];

      if ($data['per_page']) {
         $per_page = $data['per_page'];
      } else {
             $per_page = 10;
      }
      if ($data['page']) {
             $page = $data['page'];
      } else {
         $page = 1;
      }
      $offset = ($page - 1) * $per_page;

      $leads = $this->leads::selectRaw("leads.user_id,leads.id as lead_id,leads.approximate_move_date,CONCAT(users.first_name,' ',users.last_name) as name,users.email,phone_types.name as phone_type,users.phone,referrals.name as referred_by,move_status.name as lead_status")
      ->join('users','users.id','=','leads.user_id')
      ->leftjoin('phone_types','phone_types.id','=','users.phone_type')
      ->leftjoin('referrals','referrals.id','=','users.referred_by')
      ->leftjoin('move_status','move_status.id','=','leads.lead_status');
      

      if($free_text) :
       $leads = $leads->where(function ($query) use($free_text) {
          $query->orwhere('first_name', 'like', '%' . $free_text . '%')
              ->orwhere('last_name', 'like', '%' . $free_text . '%')
              ->orwhere('email', 'like', '%' . $free_text . '%')
              ->orwhere('phone', 'like', '%' . $free_text . '%');
          });
      endif;

      $leads = $leads->Groupby('leads.id','leads.user_id');
      $leads = $leads->skip($offset)->paginate($per_page);

      return $leads;
    }

    public function leadDetails($data)
    {
        $leads = $this->leads::selectRaw("CONCAT(users.first_name,' ',users.last_name) as name,users.email,phone_types.name as phone_type,users.phone,referrals.name as referred_by,move_status.name as lead_status,leads.*,leads.id as lead_id,leads.approximate_move_date")
        ->join('users','users.id','=','leads.user_id')
        ->leftjoin('phone_types','phone_types.id','=','users.phone_type')
        ->leftjoin('referrals','referrals.id','=','users.referred_by')
        ->leftjoin('move_status','move_status.id','=','leads.lead_status')
        ->where('leads.id','=',$data['lead_id'])
        ->with('leadsItems')
        ->with('leadsMaterials')
        ->first();
        if(is_null($leads))
        {
          return 201;
        }

        $categorywise = $this->leadsItems::selectRaw("categories.name,items.category_id")
        ->join('items','items.id','=','leads_items.item_id')
        ->join('categories','categories.id','=','items.category_id')
        ->where('leads_items.lead_id','=',$data['lead_id'])
        ->groupBy('items.category_id')
        ->get();

        $items = [];
        foreach ($categorywise as $key => $category) 
        {
            $items[$key]['category_id'] = $category->category_id;
            $items[$key]['category_name'] = $category->name;            
            $items[$key]['items'] = $this->catgegoryWiseItems($category->category_id);
        }

        $leads->category_wise_items=$items;

        $leads->distance = $this->helpers->distance($leads->origin_latitude, $leads->origin_longitude,$leads->destination_latitude, $leads->destination_longitude, "N");

        return $leads;
    }


    public function catgegoryWiseItems($category_id)
    {
        $items = $this->items::selectRaw("items.id,items.name,leads_items.floor,leads_items.quantity,leads_items.price,leads_items.weight,leads_items.cubic_foot")
        ->join('leads_items','leads_items.item_id','items.id')
        ->where('items.category_id','=',$category_id)
        ->Groupby('leads_items.item_id')
        ->get();
        return $items;
    }


    public function materialDetails($data)
    {
        return Materials::find($data['material_id']);
    }

    public function schedulesList($data)
    {
        $fleets = $this->fleets::selectRaw("concat(count(fleets.id),' jobs') as title,fleets.id,DATE_FORMAT(fleets.created_at, '%Y-%m-%d') as start,DATE_FORMAT(fleets.created_at, '%Y-%m-%d') as end")
        ->Groupby("fleets.created_at")
        ->get();
        return $fleets;
    }

    public function leadUpdate($data)
    {
        $leads = $this->leads::find($data['lead_id']);

        $this->Updatelead($leads,$data,$data['user_id']);

        return true;
    }

    public function Updatelead($lead,$data,$user_id)
    {
		//echo "<pre>";print_r($data);
		//echo "<pre>";print_r($lead);
		//echo "<pre>";print_r($user_id);
		//exit;
		
		unset($data['moving_information']['packing_requirement_id']);
        $data['moving_information']['user_id'] =  $user_id;
        $data['moving_information']['origin_floor'] = json_encode($data['moving_information']['origin_floor']);
        $data['moving_information']['destination_floor'] = json_encode($data['moving_information']['destination_floor']);
        
          $data['moving_information']['lead_status'] = $data['button'];
          $data['moving_information']['event_source_id'] = $data['event_source_id'];
         if($data['button']==4 || $data['button']==5){
            $data['moving_information']['is_active'] = '0';
	     }

        $ulead['moving_information'] = $data['moving_information'];
        $lead->update($ulead['moving_information']);

        $items['inventory_items'] = $data['moving_information']['inventory_items'];
        
      //  echo "<pre>";print_r($data['moving_information']);exit;
        $items['materials'] = $data['moving_information']['materials'];
       // $items['additional_services'] = $data['moving_information']['additional_services'];

        if(count($items['inventory_items']) > 0)
        {
            $this->leadsItems::where('user_id','=',$user_id)
            ->where('lead_id','=',$lead->id)->delete();

            /*foreach ($items['inventory_items'] as $key => $value) 
            {
                if($value)
                {
                    $uiteam['user_id'] = $user_id;
                    $uiteam['lead_id'] = $lead->id;
                    $uiteam['item_id'] = $value['item_id'];
                    $uiteam['floor'] = $value['floor'];
                    $uiteam['quantity'] = $value['quantity'];
                    $uiteam['price'] = $value['price'];
                    $uiteam['weight'] = $value['weight'];
                    $uiteam['cubic_foot'] = $value['cubic_foot'];
                    $this->leadsItems->create($uiteam);
                }
            }*/
            
                   $uiteam['user_id'] = $user_id;
                    $uiteam['lead_id'] = $lead->id;
                    $uiteam['item_id'] = $items['inventory_items']['item_id'][0];
                    $uiteam['floor'] = $items['inventory_items']['floor'][0];
                    $uiteam['quantity'] = $items['inventory_items']['quantity'][0];
                   // $uiteam['price'] = $value['price'];
                   // $uiteam['weight'] = $value['weight'];
                  //  $uiteam['cubic_foot'] = $value['cubic_foot'];
                    $this->leadsItems->create($uiteam);
        }

        if(count($items['materials']) > 0)
        {
            $this->leadsMaterials::where('user_id','=',$user_id)
            ->where('lead_id','=',$lead->id)->delete();
//print_r($items['materials']);exit;
           /* foreach ($items['materials'] as $key => $value) 
            {
                if($value)
                {
                    $umat['user_id'] = $user_id;
                    $umat['lead_id'] = $lead->id;
                    $umat['material_id'] = $value['material_id'];
                    $umat['quantity'] = $value['quantity'];
                    $umat['price'] = $value['price'];
                    $this->leadsMaterials->create($umat);
                }
            }*/
            
                    $umat['user_id'] = $user_id;
                    $umat['lead_id'] = $lead->id;
                    $umat['material_id'] = $items['materials']['packing_material_id'];
                    $umat['quantity'] = $items['materials']['pack_quantity'];
                  //  $umat['price'] = $value['price'];
                    $this->leadsMaterials->create($umat);
        }
        
        if(isset($data['trucks']) && $data['trucks']>0){
			$optradio=0;
			if(isset($data['optradio'])){ $optradio=$data['optradio'];}     
		$estdata['user_id'] = $user_id;
		$estdata['lead_id'] = $lead->id;
		$estdata['optradio'] = $optradio;
		$estdata['number_of_trucks'] = $data['trucks'];
		$estdata['crew_size'] = $data['crew_size'];
		$estdata['hourly_rate'] = $data['hourly_rate'];
		$estdata['min_hours'] = $data['min_hours'];
		$estdata['max_hours'] = $data['max_hours'];
		$estdata['est_hours'] = $data['est_hours'];
		$estdata['moving_hours'] = $data['moving_hours'];
		$estdata['travel_time'] = $data['travel_time'];
		$estdata['travel_fee'] = $data['travel_fee'];
		$estdata['moving_fee'] = $data['moving_fee'];
		$estdata['packing_material'] = $data['packing_material'];
		$estdata['packing_crew_size'] = $data['packing_crew_size'];
		$estdata['packing_hours'] = $data['packing_hours'];
		$estdata['packing_fee'] = $data['packing_fee'];
		$estdata['fuel_surcharge'] = $data['fuel_surcharge'];
		$estdata['total_price_range'] = $data['total_price_range'];
		$estdata['est_price_range_one'] = $data['est_price_range_one'];
		$estdata['est_price_range_two'] = $data['est_price_range_two'];
		$estdata['duration'] = ceil($data['moving_information']['final_duration']);
		$estdata['distance'] = $data['moving_information']['distance'];
		$estdata['created_at'] = date('Y-m-d H:i:s');
        $this->estimated->create($estdata);
	}

      /* if(count($items['additional_services']) > 0)
        {
            $this->leadsServices::where('user_id','=',$user_id)
            ->where('lead_id','=',$lead->id)->delete();

            foreach ($items['additional_services'] as $key => $value) 
            {
                $uservice['user_id'] = $user_id;
                $uservice['lead_id'] = $lead->id;
                $uservice['service_id'] = $value;
                $this->leadsServices->create($uservice);
            }
        }*/
  
        
        
    }

    public function schedulesDetails($data)
    {
        $fleets = $this->fleets::selectRaw("fleets.nickname as title,fleets.make,fleets.model,fleets.type,fleets.dimensions_l,fleets.dimensions_w,fleets.dimensions_h,fleets.hourly_rate,fleets.id,DATE_FORMAT(fleets.created_at, '%Y-%m-%d') as start")
        //->where('DATE_FORMAT(fleets.created_at, "%Y-%m-%d")','=',$data['startDate'])
        ->where($this->fleets::raw("(DATE_FORMAT(fleets.created_at,'%Y-%m-%d'))"), "=", $data['startDate'])
        ->get();
        return $fleets;
    }
    public function Updateleadstatus($data)
    {
       // echo "<pre>";print_r($data);exit;
        $leads = $this->leads::find($data['lead_id']);
         // echo $leads->id;exit;
         $sdata['moving_information']['lead_status'] = $data['button'];
         if($data['button']==4 || $data['button']==5){
         $sdata['moving_information']['is_active'] = '0';
	 }
        $leadupdate=$this->leads::where('id','=',$leads->id)
        ->update($sdata['moving_information']);
        return $leadupdate;
  

        
    }
}
