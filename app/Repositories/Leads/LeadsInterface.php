<?php
namespace App\Repositories\Leads;

interface LeadsInterface
{

    public function leadList(array $data);
    public function leadDetails(array $data);
    public function leadUpdate(array $data);
    public function materialDetails(array $data);
    public function schedulesList(array $data);
}