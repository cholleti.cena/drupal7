<?php


namespace App\Repositories\Tasks;
use Illuminate\Support\ServiceProvider;

class TasksServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    { }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Tasks\TasksInterface',
            'App\Repositories\Tasks\TasksRepository');
    }
}
