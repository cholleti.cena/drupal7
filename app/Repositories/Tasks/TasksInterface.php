<?php
namespace App\Repositories\Tasks;

interface TasksInterface
{
    public function tasksList(array $data);
}