<?php 
namespace App\Repositories\Tasks;

use App\Repositories\Tasks\TasksInterface;
use App\Helpers\CommonHelper;
use Auth;
use App\Models\AdminUsers;
use App\Models\User;
use App\Models\Categories;
use App\Models\Role;
use App\Models\ServiceTypes;
use App\Models\Services;
use App\Models\Items;
use App\Models\Materials;
use App\Models\MoveSizes;
use App\Models\PhoneTypes;
use App\Models\Properties;
use App\Models\Referrals;
use App\Models\Tasks;
use App\Models\Taskstypes;
use App\Models\MoveStatus;

class TasksRepository implements TasksInterface
{

   public function __construct(CommonHelper $helpers,AdminUsers $adminUsers,User $user,Categories $categories,Tasks $tasks,Items $items)
   {
     $this->user = $user;
     $this->helpers = $helpers;
     $this->adminUsers = $adminUsers;
     $this->tasks = $tasks;
     $this->items = $items;
   }
  
   public function tasksList($data)
   {
     $free_text = $data['free_text'];

     if ($data['per_page']) {
        $per_page = $data['per_page'];
     } else {
            $per_page = 10;
     }
     if ($data['page']) {
            $page = $data['page'];
     } else {
        $page = 1;
     }
     $offset = ($page - 1) * $per_page;

     $tasks = $this->tasks::selectRaw("*");
     $tasks = $tasks->skip($offset)->paginate($per_page);

      return $tasks;
   }
}
