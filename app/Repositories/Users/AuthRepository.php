<?php

namespace App\Repositories\Users;
use App\Repositories\Users\AuthInterface;
use App\Models\User;
use App\Helpers\CommonHelper;
use Carbon\Carbon;
use Auth;
use App\Models\Categories;
use App\Models\HomeBanners;
use App\Mail\SendOTP;
use App\Models\Faq;
use App\Models\Defaults;
use App\Models\Testimonial;
use Session;
use App\Mail\Subscribe;
use App\Mail\ContactUs;
use App\Models\UserContactUs;
use App\Models\Role;
use App\Models\ServiceTypes;
use App\Models\Services;
use App\Models\Items;
use App\Models\Materials;
use App\Models\MoveSizes;
use App\Models\PhoneTypes;
use App\Models\Properties;
use App\Models\Referrals;
use App\Models\Leads;
use App\Models\Tasks;
use App\Models\LeadsItems;
use App\Models\LeadsMaterials;
use App\Models\LeadsServices;
use App\Models\TasksServices;
use App\Models\MoveStatus;
use App\Models\PackingRequirements;
use App\Models\StorageRequirements;
use App\Models\PackingMaterials;
use App\Models\PackingQuotes;
use App\Models\PackingServices;
use App\Models\PricingElements;

class AuthRepository implements AuthInterface
{
    protected $user;
    protected $helpers;
    protected $categories;
    protected $homeBanners;
    protected $faqs;
    protected $defaults;
    protected $testimonial;
    protected $userContactUs;

    public function __construct(User $user,CommonHelper $helpers,Categories $categories,HomeBanners $homeBanners,Faq $faqs,Defaults $defaults,Testimonial $testimonial,UserContactUs $userContactUs,Leads $leads,LeadsMaterials $leadsMaterials,LeadsItems $leadsItems,LeadsServices $leadsServices,Tasks $tasks, TasksServices $tasksServices)
    {
        $this->user = $user;
        $this->helpers = $helpers;
        $this->categories = $categories;
        $this->homeBanners = $homeBanners;
        $this->faqs = $faqs;
        $this->defaults = $defaults;
        $this->testimonial = $testimonial;
        $this->userContactUs = $userContactUs;
        $this->leads = $leads;
        $this->tasks = $tasks;
        $this->leadsMaterials = $leadsMaterials;
        $this->leadsItems = $leadsItems;
        $this->leadsServices = $leadsServices;
        $this->tasksServices = $tasksServices;
    }

    public function getversion()
    {
        return $this->helpers->v1_getVersion();
    }

    public function signUp(array $data)
    {
        $data['reset_hash'] = $this->helpers->generateRandomHexString(50);
        $data['reset_code'] = $this->helpers->randomPin();
    	//$data['password'] = bcrypt($data['password']);
        do 
        {
          $referral_code = strtoupper($this->helpers->generateRandomHexString(6));
        }while($this->user::where('referral_code',$referral_code)->exists());

        $data['referral_code'] = $referral_code;
        $data['app_version'] = $this->helpers->v1_getVersion();

        if($data['is_registered'] == 1)
        {
            $subject = env('APP_NAME').'- You are almost there | Confirm your Activation';
            \Mail::to($data['email'])->send(new SendOTP($subject,$data));
        }

        $user = $this->user->create($data);
        
        $res['user_id'] = $user->id;
        $res['name'] = $user->first_name.' '.$user->last_name;
        $res['profile_pic'] = $user->profile_pic;
        $res['access_token'] = $user->createToken('AppName')->accessToken;
        $res['reset_hash'] = $user->reset_hash; 
        return $res;
    }

    public function postLead(array $data)
    {
        $data['reset_hash'] = $this->helpers->generateRandomHexString(50);
        $data['reset_code'] = $this->helpers->randomPin();
        do 
        {
          $referral_code = strtoupper($this->helpers->generateRandomHexString(6));
        }while($this->user::where('referral_code',$referral_code)->exists());

        $data['referral_code'] = $referral_code;
        $data['app_version'] = $this->helpers->v1_getVersion();

        $phone_validate = $this->user::where('phone','=',$data['phone'])->first();
        if($phone_validate)
        {
            $this->addLead($data,$phone_validate->id);
        } 
        else 
        {   
            $user = $this->user->create($data);

            $this->addLead($data,$user->id);
        }

        return true;
    }

    public function addLead($data,$user_id)
    {
        $data['moving_information']['user_id'] =  $user_id;
        $data['moving_information']['origin_floor'] = json_encode($data['moving_information']['origin_floor']);
        $data['moving_information']['destination_floor'] = json_encode($data['moving_information']['destination_floor']);

        $ulead['moving_information'] = $data['moving_information'];
        $lead = $this->leads->create($ulead['moving_information']);

        $items['inventory_items'] = $data['moving_information']['inventory_items'];
        $items['materials'] = $data['moving_information']['materials'];
        $items['additional_services'] = $data['moving_information']['additional_services'];

        if(count($items['inventory_items']) > 0)
        {
            foreach ($items['inventory_items'] as $key => $value) 
            {
                if($value)
                {
                    $uiteam['user_id'] = $user_id;
                    $uiteam['lead_id'] = $lead->id;
                    $uiteam['item_id'] = $value['item_id'];
                    $uiteam['floor'] = $value['floor'];
                    $uiteam['quantity'] = $value['quantity'];
                    $uiteam['price'] = $value['price'];
                    $uiteam['weight'] = $value['weight'];
                    $uiteam['cubic_foot'] = $value['cubic_foot'];
                    $this->leadsItems->create($uiteam);
                }
            }
        }

        if(count($items['materials']) > 0)
        {
            foreach ($items['materials'] as $key => $value) 
            {
                if($value)
                {
                    $umat['user_id'] = $user_id;
                    $umat['lead_id'] = $lead->id;
                    $umat['material_id'] = $value['material_id'];
                    $umat['quantity'] = $value['quantity'];
                    //$umat['price'] = $value['price'];
                    $this->leadsMaterials->create($umat);
                }
            }
        }

        if(count($items['additional_services']) > 0)
        {
            foreach ($items['additional_services'] as $key => $value) 
            {
                $uservice['user_id'] = $user_id;
                $uservice['lead_id'] = $lead->id;
                $uservice['service_id'] = $value;
                $this->leadsServices->create($uservice);
            }
        }
    }

    public function verifyPhone(array $data)
    {
        $user = $this->user::where('email','=',$data['email'])->first();
        if($user)
        {
            if($user->reset_code != $data['otp']){
              return false;
            }

            if(isset($data['newpassword']))
            {
                $data['password'] = bcrypt($data['newpassword']);
            }

            $data['is_email_verified'] = 1;
            $data['email_verified_at'] = now();
            $user->update($data);

            $res['user_id'] = $user->id;
            $res['name'] = $user->first_name.' '.$user->last_name;
            $res['profile_pic'] = $user->profile_pic;
            $res['access_token'] = $user->createToken('AppName')->accessToken;
            $res['reset_hash'] = $user->reset_hash; 
            return $res;
        }
        else
        {
            return false;
        }
    }

    public function resendOTP(array $data)
    {
        $user = $this->user::where('email','=',$data['email'])->first();
        if($user){
            $data['phone'] = $user->phone;
            $data['reset_hash'] = $this->helpers->generateRandomHexString(50);
            $data['reset_code'] = $this->helpers->randomPin();
            $subject = env('APP_NAME').'- You are almost there | Confirm your Activation';
            \Mail::to($data['email'])->send(new SendOTP($subject,$data));
            $user->update($data);
            $res['reset_hash'] = $data['reset_hash'];
            return $res;
        }
        else
        {
            return false;
        }
    }

    public function subScribe(array $data)
    {
        $userSubcribe = $this->userSubcribe::where('email','=',$data['email'])->first();
        if(is_null($userSubcribe))
        {
            $subject = env('APP_NAME').'- Thanks for subscribe with us';
            \Mail::to($data['email'])->send(new Subscribe($subject,$data));

            $this->userSubcribe->create($data);
            return true;
        }

        if($userSubcribe->is_active == 1)
        {
            return 201;
        }
        
        $sdata['is_active'] = 1;
        $userSubcribe->update($sdata);
        return true;
    }

    public function contactUs(array $data)
    {
        $email_val = $this->userContactUs::where('email','=',$data['email'])
        ->count();
        if($email_val > 0) 
        {
            return 201;  
        }

        $phone_val = $this->userContactUs::where('phone','=',$data['phone'])
        ->count();
        if($phone_val > 0) 
        {
            return 202;  
        }

        $subject = env('APP_NAME').'- Thank you for contact with us';
        \Mail::to($data['email'])->send(new ContactUs($subject,$data));

        $this->userContactUs->create($data);
        return true;
    }

    public function login(array $data)
    {
        $username = $data['username'];
        $user = $this->user::where(function($q)use ($username) {
                    $q->where('email', $username)
                    ->orWhere('phone', $username);
             })
        ->first();
        if ($user){
            // if($user->is_verify === "InActive"){
            //     $data['reset_code'] = $user->reset_code;
            //     $subject = env('APP_NAME').'- You are almost there | Confirm your Activation';
            //     \Mail::to($user->email)->send(new SendOTP($subject,$data));
            //     return 202;
            // }
            if(is_numeric($username)){
                $field = 'phone';
            } elseif (filter_var($username, FILTER_VALIDATE_EMAIL)) {
                $field = 'email';
            }
            $credentials = [$field => $username,'password' => $data['password']];
            if (Auth::attempt($credentials)) {
                $res['user_id'] = $user->id;
                $res['is_verify'] = $user->is_verify;
                $res['name'] = $user->first_name.' '.$user->last_name;
                $res['profile_pic'] = $user->profile_pic;
                $res['access_token'] = $user->createToken('AppName')->accessToken;
                $res['reset_hash'] = $user->reset_hash; 
                return $res;
            }
            else{
                return false;
            }
        }
        return 201; 
    }

    public function logOut()
    {
        $user = Auth::user();
        $token = Auth::user()->token();
        $token->delete();
        return true;
    }

    public function getCities()
    {
        return $this->cities::where('status','=',1)->Orderby('city','asc')->get(['id','city']);
    }

    public function getCategories()
    {
        $categories = $this->categories::where('is_active','=',1)
        ->Orderby('category_name','asc')->get(['id','category_name','category_image','parent_id']);
        return $this->helpers->buildTree($categories, $parent_id = 0);
    }

    public function getBanners()
    {
        return $this->homeBanners::where('is_active','=',1)
        ->Orderby('header_title','asc')->get(['id','header_title','hear_image']);
    }

    public function getFAQs()
    {
        return $this->faqs::where('is_active','=',1)->get(['id','question','answer']);
    }

    public function getAboutUsTerms()
    {
        return $this->defaults::first(['id','about_us','terms_conditions']);
    }

    public function getBlogs()
    {
        return $this->blogs::where('is_active','=',1)
        ->Orderby('id','desc')
        ->get();
    }

    public function getTestimonials()
    {
        return $this->testimonial::where('is_active','=',1)
        ->Orderby('id','desc')
        ->get();
    }

    public function getStatsCount()
    {
        $final['users_count'] = $this->user::count();
        $final['courses_count'] = $this->courses::count();

        return $final;
    }

    public function getItems($category_id)
    {
        $items = Items::where('category_id','=',$category_id)
        ->get();
        return $items;
    }

    public function getMetaDataList($data)
    {
        $totalList = array();
        $free_text = $data['free_text'];
        if(in_array('category', $data['metaData']))
        {
            $category = Categories::select('categories.id','categories.name')->with('items');
            if($free_text != ''):
              $category = $category->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $category = $category->orderBy('name','asc');
            $category = $category->get();

            $Cat = [];
            foreach ($category as $key => $cate) {
                $Cat[$key]['id'] = $cate->id;
                $Cat[$key]['name'] = $cate->name;
                $Cat[$key]['items'] = $this->getItems($cate->id);
            }

            $totalList['categories'] = $Cat;
        }

        if(in_array('roles', $data['metaData']))
        {
            $role = Role::select('id','name');
            if($free_text != ''):
              $role = $role->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $role = $role->orderBy('name','asc');
            $role = $role->get();
            $totalList['roles'] = $role;
        }

        if(in_array('servicetypes', $data['metaData']))
        {
            $servicetypes = ServiceTypes::select('id','name');
            if($free_text != ''):
              $servicetypes = $servicetypes->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $servicetypes = $servicetypes->orderBy('name','asc');
            $servicetypes = $servicetypes->get();
            $totalList['servicetypes'] = $servicetypes;
        }

        if(in_array('property', $data['metaData']))
        {
            $property = Properties::select('id','name');
            if($free_text != ''):
              $property = $property->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $property = $property->orderBy('name','asc');
            $property = $property->get();
            $totalList['properties'] = $property;
        }

        if(in_array('referrals', $data['metaData']))
        {
            $referrals = Referrals::select('id','name');
            if($free_text != ''):
              $referrals = $referrals->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $referrals = $referrals->orderBy('name','asc');
            $referrals = $referrals->get();
            $totalList['referrals'] = $referrals;
        }

        if(in_array('phonetypes', $data['metaData']))
        {
            $phonetypes = PhoneTypes::select('id','name');
            if($free_text != ''):
              $phonetypes = $phonetypes->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $phonetypes = $phonetypes->orderBy('name','asc');
            $phonetypes = $phonetypes->get();
            $totalList['phonetypes'] = $phonetypes;
        }

        if(in_array('movesizes', $data['metaData']))
        {
            $movesizes = MoveSizes::select('id','name');
            if($free_text != ''):
              $movesizes = $movesizes->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $movesizes = $movesizes->orderBy('name','asc');
            $movesizes = $movesizes->get();
            $totalList['movesizes'] = $movesizes;
        }

        if(in_array('materials', $data['metaData']))
        {
            $materials = Materials::select('id','name','price');
            if($free_text != ''):
              $materials = $materials->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $materials = $materials->orderBy('name','asc');
            $materials = $materials->get();
            
             $Mat = [];
            foreach ($materials as $key => $mate) {
                $Mat[$key]['id'] = $mate->id;
                $Mat[$key]['name'] = $mate->name;
                $Mat[$key]['price'] = $mate->price;
               // $Mat[$key]['items'] = $this->getItems($cate->id);
            }               
            $totalList['materials'] = $Mat;
        }

        if(in_array('services', $data['metaData']))
        {
            $services = Services::select('id','name');
            if($free_text != ''):
              $services = $services->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $services = $services->orderBy('name','asc');
            $services = $services->get();
            $totalList['services'] = $services;
        }

        if(in_array('movestatuses', $data['metaData']))
        {
            $movestatuses = MoveStatus::select('id','name');
            if($free_text != ''):
              $movestatuses = $movestatuses->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $movestatuses = $movestatuses->orderBy('name','asc');
            $movestatuses = $movestatuses->get();
            $totalList['movestatuses'] = $movestatuses;
        }

        if(in_array('packing_requirements', $data['metaData']))
        {
            $packing_requirements = PackingRequirements::select('id','name');
            if($free_text != ''):
              $packing_requirements = $packing_requirements->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $packing_requirements = $packing_requirements->orderBy('id','asc');
            $packing_requirements = $packing_requirements->get();
            $totalList['packing_requirements'] = $packing_requirements;
        }

        if(in_array('storage_requirements', $data['metaData']))
        {
            $storage_requirements = StorageRequirements::select('id','name');
            if($free_text != ''):
              $storage_requirements = $storage_requirements->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $storage_requirements = $storage_requirements->orderBy('id','asc');
            $storage_requirements = $storage_requirements->get();
            $totalList['storage_requirements'] = $storage_requirements;
        }

        if(in_array('packing_materials', $data['metaData']))
        {
            $packing_materials = PackingMaterials::select('id','name','pricing_elements');
            if($free_text != ''):
              $packing_materials = $packing_materials->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $packing_materials = $packing_materials->orderBy('id','asc');
            $packing_materials = $packing_materials->get();
            $totalList['packing_materials'] = $packing_materials;
        }

        if(in_array('packing_quotes', $data['metaData']))
        {
            $packing_quotes = PackingQuotes::select('id','name','pricing_elements');
            if($free_text != ''):
              $packing_quotes = $packing_quotes->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $packing_quotes = $packing_quotes->orderBy('id','asc');
            $packing_quotes = $packing_quotes->get();
            $totalList['packing_quotes'] = $packing_quotes;
        }

        if(in_array('packing_services', $data['metaData']))
        {
            $packing_services = PackingServices::select('id','name','pricing_elements');
            if($free_text != ''):
              $packing_services = $packing_services->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $packing_services = $packing_services->orderBy('id','asc');
            $packing_services = $packing_services->get();
            $totalList['packing_services'] = $packing_services;
        }

        if(in_array('pricing_elements', $data['metaData']))
        {
            $pricing_elements = PricingElements::select('id','name','price');
            if($free_text != ''):
              $pricing_elements = $pricing_elements->where(function($q)use ($free_text) {
                  $q->where('name', 'LIKE','%'.$free_text.'%');
              });
            endif;
            $pricing_elements = $pricing_elements->orderBy('id','asc');
            $pricing_elements = $pricing_elements->get();
            $totalList['pricing_elements'] = $pricing_elements;
        }

        return $totalList;
    }
}
