<?php


namespace App\Repositories\Users;


interface AuthInterface
{
    public function getversion();
    public function signUp(array $data);
    public function postLead(array $data);
    public function verifyPhone(array $data);
    public function resendOTP(array $data);
    public function login(array $data);
    public function logOut();
    public function getCategories();
    public function getBanners();
    public function getFAQs();
    public function getAboutUsTerms();
    public function getBlogs();
    public function getTestimonials();
    public function getStatsCount();
    public function subScribe(array $data);
    public function contactUs(array $data);
    public function getMetaDataList(array $data);
}
