<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemImages extends Model
{
    protected $table = 'items_images';  

    protected $fillable = [
        'item_id','image','created_at','updated_at'
    ];  
}
