<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Items extends Model
{
    protected $table = 'items';  

    protected $fillable = [
        'name','category_id','sulg','description','price','weight','cubic_foot','message','moving_time','over_size','taxable','is_active','created_at','updated_at'
    ];  

    public function getCategoryIdAttribute($value)
    {
        $items = \App\Models\Categories::find($value);
        if(is_null($items))
        {
            return array('id' =>0,'name' => '');
        }
        return $items;
    }

    public function setSulgAttribute($value)
    {
        $this->attributes['sulg'] = Str::slug($value);
    }
}
