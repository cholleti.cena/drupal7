<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayRollMiles extends Model
{
    protected $table = 'pay_roll_miles';

    protected $fillable = [
        'name','price', 'default','is_active','created_at','updated_at'
    ];

}
