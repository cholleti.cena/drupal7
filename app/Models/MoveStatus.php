<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MoveStatus extends Model
{
    protected $table = 'move_status';  

    protected $fillable = [
        'name','is_active','created_at','updated_at'
    ];  
}
