<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackingMaterials extends Model
{
    protected $table = 'packing_materials';  

    protected $fillable = [
        'name','price','default_quantity','pricing_elements','is_active','created_at','updated_at'
    ];  


    public function getPricingElementsAttribute($value)
    {
        if(is_null($value))
        {
            $pricing_elements = [];
        }
        $pricing_elements = json_decode($value);

        return PricingElements::whereIn('id',$pricing_elements)->get();
    }
}
