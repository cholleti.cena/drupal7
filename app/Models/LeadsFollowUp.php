<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadsFollowUp extends Model
{
    protected $table = 'leads_follow_up';

    protected $fillable = [
        'days_until_move','follow_up_hours','is_active','created_at','updated_at'
    ];

}
