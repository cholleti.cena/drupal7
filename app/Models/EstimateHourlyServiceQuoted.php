<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstimateHourlyServiceQuoted extends Model
{
   protected $table = 'estimate_hourly_service_quoted';

    protected $fillable = [
        'is_moving_services', 'is_traveling_services', 'is_packing_materials', 'is_packing_services', 'is_storage_services', 'is_fuel_surcharge', 'is_discount', 'hide_fields_from_estimate', 'is_show_total_est_price', 'is_show_hourly_notes', 'hourly_price_text', 'created_at','updated_at'
    ];
}
