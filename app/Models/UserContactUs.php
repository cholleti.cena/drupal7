<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContactUs extends Model
{
    protected $table = 'user_contact_us';

    protected $fillable = [
        'name','email', 'phone','message','created_at','updated_at'
    ];

}
