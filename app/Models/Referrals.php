<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Referrals extends Model
{
    protected $table = 'referrals';  

    protected $fillable = [
        'name','is_active','created_at','updated_at'
    ];  
}
