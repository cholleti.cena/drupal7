<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VisiblePayrollFields extends Model
{
    protected $table = 'visible_payroll_fields';

    protected $fillable = [
        'payroll_pdf_text','is_active','created_at','updated_at'
    ];

}
