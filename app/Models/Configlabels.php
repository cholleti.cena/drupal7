<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configlabels extends Model
{
    protected $table = 'config_labels';

    protected $fillable = [
        'configtype_id','name','description','is_active','created_by','modified_by','created_at','updated_at'
    ];
}
