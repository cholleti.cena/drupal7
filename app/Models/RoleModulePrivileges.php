<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleModulePrivileges extends Model
{
    public $timestamps = false;
    protected $table = 'rolemoduleprivileges';
}
