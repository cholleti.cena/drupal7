<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Facilities extends Model
{
    public $table = 'facilities';

    protected $fillable = [
        'name','street','state_id','zip','phone','fax','email','late_fee','default_terms','minimum_cost','terms','default_storage','is_active', 'created_at', 'updated_at'
    ];
}
