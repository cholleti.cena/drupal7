<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Properties extends Model
{
    protected $table = 'properties';  

    protected $fillable = [
        'name','is_active','created_at','updated_at'
    ];  
}
