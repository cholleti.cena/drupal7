<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Floors extends Model
{
    protected $table = 'floors';

    protected $fillable = [
        'floor','extra_time_Room', 'stairs_os_items','stairs_inventory','stairs_boxes','elevator_inventory','elevator_boxes','is_active','created_at','updated_at'
    ];

}
