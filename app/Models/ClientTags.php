<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientTags extends Model
{
    protected $table = 'client_tags';  

    protected $fillable = ['id','name','color','created_at','updated_at'];
}
