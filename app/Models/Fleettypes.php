<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fleettypes extends Model
{
    protected $table = 'fleettypes';  

    protected $fillable = ['id','name','created_by','updated_by', 'created_at','updated_at'];

     
}
