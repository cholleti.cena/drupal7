<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PricingElements extends Model
{
    protected $table = 'pricing_elments'; 

    protected $fillable = [
        'name','is_active','created_at','updated_at'
    ];  
}
