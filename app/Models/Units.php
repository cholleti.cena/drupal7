<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Units extends Model
{
    public $table = 'facility_units';

    protected $fillable = [
        'facility_id','unit_name','width','height','depth','location','price','quantity','cuft','total_occupied','is_active', 'created_at', 'updated_at'
    ];
}
