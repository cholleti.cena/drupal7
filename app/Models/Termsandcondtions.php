<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Termsandcondtions extends Model
{
    protected $table = 'terms_and_conditions';

    protected $fillable = [
        'heading', 'content','created_at' , 'updated_at'
    ];
}