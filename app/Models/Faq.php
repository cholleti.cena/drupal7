<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faqs';

    protected $fillable = [
        'question','answer', 'is_active','created_by','modified_by','created_at','updated_at'
    ];
}
