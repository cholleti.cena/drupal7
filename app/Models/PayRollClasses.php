<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayRollClasses extends Model
{
    protected $table = 'pay_roll_classes';

    protected $fillable = [
        'name','is_active','created_at','updated_at'
    ];

}
