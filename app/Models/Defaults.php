<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Defaults extends Model
{
    protected $table = 'defaults';

    protected $fillable = [
        'allow_create_logs','allow_edit_logs','logo','allow_delete_logs','log_max_days','version','about_us','about_us_image','who_we_are_image','vision_image','mission_image','objective_image','our_value_image','who_we_are','vision','mission','objective','our_value','terms_conditions','address'
    ];

    public function setAllowCreateLogsAttribute($value)
    {
        $this->attributes['allow_create_logs'] = ($value == ''  ? '0' : '1');
    }

    public function setAllowEditLogsAttribute($value)
    {
        $this->attributes['allow_edit_logs'] = ($value == ''  ? '0' : '1');
    }

    public function setAllowDeleteLogsAttribute($value)
    {
        $this->attributes['allow_delete_logs'] = ($value == ''  ? '0' : '1');
    }

    public function getAboutUsImageAttribute($value)
    {
        return !is_null($value) ? env('SITE_URL').'/'.$value : env('SITE_URL').'/'.env('NO_PROFILE_IMAGE');
    }

    public function getWhoWeAreImageAttribute($value)
    {
        return !is_null($value) ? env('SITE_URL').'/'.$value : env('SITE_URL').'/'.env('NO_PROFILE_IMAGE');
    }

    public function getVisionImageAttribute($value)
    {
        return !is_null($value) ? env('SITE_URL').'/'.$value : env('SITE_URL').'/'.env('NO_PROFILE_IMAGE');
    }

    public function getMissionImageAttribute($value)
    {
        return !is_null($value) ? env('SITE_URL').'/'.$value : env('SITE_URL').'/'.env('NO_PROFILE_IMAGE');
    }

    public function getObjectiveImageAttribute($value)
    {
        return !is_null($value) ? env('SITE_URL').'/'.$value : env('SITE_URL').'/'.env('NO_PROFILE_IMAGE');
    }

    public function getOurValueImageAttribute($value)
    {
        return !is_null($value) ? env('SITE_URL').'/'.$value : env('SITE_URL').'/'.env('NO_PROFILE_IMAGE');
    }

    public function getLogoAttribute($value)
    {
        return !is_null($value) ? env('SITE_URL').'/'.$value : "";
    }

    public function getAddressAttribute($value)
    {
        return !is_null($value) ? $value : "";
    }

}
