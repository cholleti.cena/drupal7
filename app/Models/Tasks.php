<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
    protected $table = 'tasks';  

    protected $fillable = ['id','task_id','task_type','assigned_user_by','assigned_user_to','client','contact_person','priority','status','when_date','send_emails','created_at','updated_at','active_yn','is_active'];  

    public function getTypeOfServiceAttribute($value)
    {
        $service_types = \App\Models\ServiceTypes::find($value);
        if(is_null($service_types))
        {
            return array('id' =>0,'task_id' => '');
        }
        return $service_types;
    }

    public function getOriginPropertyTypeIdAttribute($value)
    {
        $properties = \App\Models\Properties::find($value);
        if(is_null($properties))
        {
            return array('id' =>0,'task_id' => '');
        }
        return $properties;
    }

    public function getOriginFloorAttribute($value)
    {
        return !is_null($value) ? json_decode($value) : "";
    }

    public function getDestinationPropertyTypeIdAttribute($value)
    {
        $properties = \App\Models\Properties::find($value);
        if(is_null($properties))
        {
            return array('id' =>0,'task_id' => '');
        }
        return $properties;
    }

    public function getDestinationFloorAttribute($value)
    {
        return !is_null($value) ? json_decode($value) : "";
    }

    public function getMoveSizeIdAttribute($value)
    {
        $moveSizes = \App\Models\MoveSizes::find($value);
        if(is_null($moveSizes))
        {
            return array('id' =>0,'task_id' => '');
        }
        return $moveSizes;
    }
}