<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadsServices extends Model
{
    protected $table = 'leads_services';  

    protected $fillable = [
        'user_id','lead_id','service_id','created_at','updated_at'
    ];  
}
