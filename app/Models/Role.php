<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';  

    protected $fillable = [
        'name','role_type','created_by','modified_by','created_at','updated_at'
    ];  
}
