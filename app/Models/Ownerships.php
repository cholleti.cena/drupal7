<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ownerships extends Model
{
    protected $table = 'owner_ships';

    protected $fillable = [
        'name','is_active','created_at','updated_at'
    ];

}
