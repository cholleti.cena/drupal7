<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeBanners extends Model
{
    protected $table = 'home_banners';

    protected $fillable = [
        'header_title','hear_image', 'is_active','created_by','modified_by','created_at','updated_at'
    ];

    public function getHearImageAttribute($value)
    {
    	return !is_null($value) ? env('SITE_URL').'/'.$value : env('SITE_URL').'/'.env('NO_PROFILE_IMAGE');
    }
}
