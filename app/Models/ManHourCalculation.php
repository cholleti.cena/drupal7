<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ManHourCalculation extends Model
{
    protected $table = 'man_hour_calculation';

    protected $fillable = [
        'men_hours','men', 'stairs_men','elevator_men','is_active','created_at','updated_at'
    ];

}
