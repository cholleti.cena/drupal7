<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DistanceFromTruck extends Model
{
    protected $table = 'distance_from_truck';

    protected $fillable = [
        'distance_from_truck','room', 'o_s_items','inventory','boxes','is_active','created_at','updated_at'
    ];

}
