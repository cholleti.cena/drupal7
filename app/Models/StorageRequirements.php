<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StorageRequirements extends Model
{
    protected $table = 'storage_requirements'; 

    protected $fillable = [
        'name','is_active','created_at','updated_at'
    ];  
}
