<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaContent extends Model
{
    protected $table = 'media_content';

    protected $fillable = [
        'media_name','media_path','media_extension','created_by','modified_by','created_at','updated_at'
    ];

    public function getMediaPathAttribute($value)
    {
    	return !is_null($value) ? env('SITE_URL').'/'.$value : env('SITE_URL').'/'.env('NO_PROFILE_IMAGE');
    }

}
