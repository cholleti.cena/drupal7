<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstimateDaysCapacity extends Model
{
   protected $table = 'estimate_days_capacity';

    protected $fillable = [
        'number_of_jobs', 'number_of_men', 'number_of_trucks', 'recurring_days', 'created_at','updated_at'
    ];
}
