<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminUsers extends Model
{
    protected $table = 'admin_users';

    protected $fillable = [
        'name','email', 'password','mobileno','secret','reset_hash','role_id','status','created_by','modified_by','created_at','updated_at'
    ];

}
