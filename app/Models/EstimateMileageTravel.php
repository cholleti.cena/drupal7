<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstimateMileageTravel extends Model
{
    protected $table = 'estimate_mileage_travel';  

    protected $fillable = ['id','name','description','image','time_details','is_time_details','is_checked','created_at','updated_at'];
}
