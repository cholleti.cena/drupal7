<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstimateDefaultFloors extends Model
{
    protected $table = 'estimate_default_floors';

    protected $fillable = [
        'flooors','created_at','updated_at'
    ];
}
