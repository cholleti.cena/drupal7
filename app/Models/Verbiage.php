<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Verbiage extends Model
{
    protected $table = 'verbiage';

    protected $fillable = [
        'signature_name','required', 'hide','created_at' , 'updated_at'
    ];
}
