<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstimateDefaultEstimateNotes extends Model
{
    protected $table = 'estimate_default_estimate_notes';

    protected $fillable = [
        'estimate_hourly_service_quoted_id', 'description', 'created_at','updated_at'
    ];
}
