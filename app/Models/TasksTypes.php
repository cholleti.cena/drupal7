<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Taskstypes extends Model
{
    protected $table = 'task_types';  

    protected $fillable = ['id','name'];  

    public function getTypeOfServiceAttribute($value)
    {
        $service_types = \App\Models\ServiceTypes::find($value);
        if(is_null($service_types))
        {
            return array('id' =>0,'name' => '');
        }
        return $service_types;
    }

    public function getOriginPropertyTypeIdAttribute($value)
    {
        $properties = \App\Models\Properties::find($value);
        if(is_null($properties))
        {
            return array('id' =>0,'name' => '');
        }
        return $properties;
    }

    public function getOriginFloorAttribute($value)
    {
        return !is_null($value) ? json_decode($value) : "";
    }

    public function getDestinationPropertyTypeIdAttribute($value)
    {
        $properties = \App\Models\Properties::find($value);
        if(is_null($properties))
        {
            return array('id' =>0,'name' => '');
        }
        return $properties;
    }

    public function getDestinationFloorAttribute($value)
    {
        return !is_null($value) ? json_decode($value) : "";
    }

    public function getMoveSizeIdAttribute($value)
    {
        $moveSizes = \App\Models\MoveSizes::find($value);
        if(is_null($moveSizes))
        {
            return array('id' =>0,'name' => '');
        }
        return $moveSizes;
    }

}