<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneTypes extends Model
{
    protected $table = 'phone_types';  

    protected $fillable = [
        'name','is_active','created_at','updated_at'
    ];  
}
