<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';  

    protected $fillable = [
        'name','is_active','created_at','updated_at'
    ];

    public function items()
    {
        return $this->hasMany(Items::class,'category_id','id');
    }  
}
