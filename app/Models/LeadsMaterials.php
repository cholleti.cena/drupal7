<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadsMaterials extends Model
{
    protected $table = 'leads_materials';  

    protected $fillable = [
        'user_id','lead_id','material_id','quantity','price','pack_quantity','unpack_quantity','created_at','updated_at'
    ];

    public function getMaterialIdAttribute($value)
    {
        $items = \App\Models\Materials::find($value);
        if(is_null($items))
        {
            return array('id' =>0,'name' => '');
        }
        return $items;
    }  
}
