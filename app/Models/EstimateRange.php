<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstimateRange extends Model
{
    protected $table = 'estimate_range';

    protected $fillable = [
        'estimate_range_from','estimate_range_to', 'total_estimate_price','is_active','created_at','updated_at'
    ];

}
