<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceNotes extends Model
{
   protected $table = 'invoice_notes';  

    protected $fillable = [
        'notice_content','created_at','updated_at'
    ]; 
}
