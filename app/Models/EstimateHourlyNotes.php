<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstimateHourlyNotes extends Model
{
    protected $table = 'estimate_hourly_notes';

    protected $fillable = [
        'estimate_hourly_service_quoted_id', 'description', 'created_at','updated_at'
    ];
}
