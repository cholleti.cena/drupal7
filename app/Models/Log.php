<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'log';

    protected $fillable = ['module_id','created_on','user_id','action','category','description','log_type','created_at','updated_at'];
}
