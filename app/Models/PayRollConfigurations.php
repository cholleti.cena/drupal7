<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayRollConfigurations extends Model
{
    protected $table = 'pay_roll_configurations';

    protected $fillable = [
        'payroll_pdf_text','payroll_mile_text', 'visible_payroll_fields','is_active','created_at','updated_at'
    ];

}
