<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdditionalCharges extends Model
{
    protected $table = 'additional_charges';

    protected $fillable = [
        'name','price', 'default','is_active','created_at','updated_at'
    ];

}
