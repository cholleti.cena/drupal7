<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leads extends Model
{
    protected $table = 'leads';  

    protected $fillable = [
        'user_id','approximate_move_date','type_of_service','origin_address','origin_latitude','origin_longitude','origin_property_type_id','origin_floor','origin_elevator','destination_address','destination_latitude','destination_longitude','destination_property_type_id','destination_floor','destination_elevator','move_size_id','square_footage','estimated_boxes','total_weight','total_cubic_foot','is_active','lead_status','packing_requirement_id','comments','created_at','updated_at','event_source_id'
    ];  

    public function getTypeOfServiceAttribute($value)
    {
        $service_types = \App\Models\ServiceTypes::find($value);
        if(is_null($service_types))
        {
            return array('id' =>0,'name' => '');
        }
        return $service_types;
    }

    public function getOriginPropertyTypeIdAttribute($value)
    {
        $properties = \App\Models\Properties::find($value);
        if(is_null($properties))
        {
            return array('id' =>0,'name' => '');
        }
        return $properties;
    }

    public function getOriginFloorAttribute($value)
    {
        return !is_null($value) ? json_decode($value) : "";
    }

    public function getDestinationPropertyTypeIdAttribute($value)
    {
        $properties = \App\Models\Properties::find($value);
        if(is_null($properties))
        {
            return array('id' =>0,'name' => '');
        }
        return $properties;
    }

    public function getDestinationFloorAttribute($value)
    {
        return !is_null($value) ? json_decode($value) : "";
    }

    public function getMoveSizeIdAttribute($value)
    {
        $moveSizes = \App\Models\MoveSizes::find($value);
        if(is_null($moveSizes))
        {
            return array('id' =>0,'name' => '');
        }
        return $moveSizes;
    }

    public function leadsItems()
    {
        return $this->hasMany(LeadsItems::class,'lead_id','id');
    }

    public function leadsMaterials()
    {
        return $this->hasMany(LeadsMaterials::class,'lead_id','id');
    }
}
