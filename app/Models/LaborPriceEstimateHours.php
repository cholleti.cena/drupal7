<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LaborPriceEstimateHours extends Model
{
    protected $table = 'labor_price_estimate_hours';

    protected $fillable = [
        'minimum_hours', 'is_charge_after_minimum_hours', 'minimum_type', 'inventory_type', 'inventory_type_for_iframe', 'hourly_rate_type','is_iframe_auto_calculate', 'is_sort_inventory_vertically','is_default_type',
         'default_type', 'is_default_floors', 'default_floors', 'created_at' , 'updated_at'
    ];
}
