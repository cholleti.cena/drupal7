<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackingRequirements extends Model
{
    protected $table = 'packing_requirements'; 

    protected $fillable = [
        'name','is_active','created_at','updated_at'
    ];  
}
