<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TasksServices extends Model
{
    protected $table = 'tasks';  

    protected $fillable = [
        'id','task_id','created_at','updated_at'
    ];  
}
