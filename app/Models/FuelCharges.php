<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FuelCharges extends Model
{
    protected $table = 'fuel_charges';

    protected $fillable = [
        'min_range','max_range', 'min_price','price_per_mile','is_active','created_at','updated_at'
    ];

}
