<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TravelTime extends Model
{
    protected $table = 'travel_time';

    protected $fillable = [
        'min_mile_range','max_mile_range', 'min_travel_time','is_active','created_at','updated_at'
    ];

}
