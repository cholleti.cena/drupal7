<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FollowUpAutomation extends Model
{
    protected $table = 'follow_up_automation';

    protected $fillable = [
        'days_until_move','follow_up_hours','is_active','created_at','updated_at'
    ];

}
