<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Years extends Model
{
    
    protected $table = 'years';

    protected $fillable = [
        'year','created_at','updated_at'
    ];
}
