<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstimateJobNotes extends Model
{
    protected $table = 'estimate_job_notes';

    protected $fillable = [
        'estimate_hourly_service_quoted_id', 'description', 'created_at','updated_at'
    ];
}
