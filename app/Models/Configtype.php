<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configtype extends Model
{
    protected $table = 'config_types';

    protected $fillable = [
        'category_name','description','is_active','created_at','updated_at'
    ];
    
    public function configlables()
    {
        return $this->hasMany(Configlabels::class,'configtype_id','id');
    }
}
