<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fleets extends Model
{
    protected $table = 'fleets';

    protected $fillable = [
        'id','nickname','year', 'make','model', 'type', 'dimensions_l','dimensions_w','dimensions_h', 'cuft_capacity', 'VIN', 'tag_number','tag_country', 'tag_state', 'expires', 'insurance_company', 'policy_number', 'hourly_rate','vehicle_status','owner_ship','is_schedulable','created_at','updated_at'
    ];
}
