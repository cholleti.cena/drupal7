<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    public $timestamps = false;
    protected $table = 'testimonials';

    protected $fillable = [
        'image','name', 'message','is_active','created_by','modified_by','created_at','updated_at'
    ];

    public function getImageAttribute($value)
    {
    	return !is_null($value) ? env('SITE_URL').'/'.$value : env('SITE_URL').'/'.env('NO_PROFILE_IMAGE');
    }
}
