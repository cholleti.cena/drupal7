<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Addendums extends Model
{
    
    protected $table = 'addendums';

    protected $fillable = [
        'name','data', 'status','created_at','updated_at'
    ];
}
