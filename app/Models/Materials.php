<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Materials extends Model
{
    protected $table = 'materials';  

    protected $fillable = [
        'name','description','price','image','created_at','updated_at'
    ];  
}
