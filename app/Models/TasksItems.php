<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TasksItems extends Model
{
    protected $table = 'task_items';  

    protected $fillable = [
        'user_id','lead_id','item_id','floor','quantity','price','weight','cubic_foot','is_active','created_at','updated_at'
    ];  

    public function getItemIdAttribute($value)
    {
        $items = \App\Models\Items::find($value);
        if(is_null($items))
        {
            return array('id' =>0,'name' => '');
        }
        return $items;
    }
}