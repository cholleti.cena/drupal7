<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceTypes extends Model
{
    protected $table = 'service_types';  

    protected $fillable = [
        'name','is_active','created_at','updated_at'
    ];  
}
