<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackingServices extends Model
{
    protected $table = 'packing_services';  

    protected $fillable = [
        'name','price','pricing_elements','is_active','created_at','updated_at'
    ];  


    public function getPricingElementsAttribute($value)
    {
        if(is_null($value))
        {
            $pricing_elements = [];
        }
        $pricing_elements = json_decode($value);

        return PricingElements::whereIn('id',$pricing_elements)->get();
    }
}
