<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    
    protected $table = 'employees';  

    protected $fillable = ['id','first_name','created_by','updated_by', 'created_at','updated_at'];
}
