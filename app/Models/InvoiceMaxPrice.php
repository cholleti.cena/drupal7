<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceMaxPrice extends Model
{
    protected $table = 'invoicev2_maxprice';  

    protected $fillable = [
        'heading','content','created_at','updated_at'
    ]; 
}
