<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SquareFootage extends Model
{
    protected $table = 'square_footage';

    protected $fillable = [
        'min_range','max_range', 'minutes_per_room','cubic_feet_per_room','weight_per_room','people','trucks','price','is_active','created_at','updated_at'
    ];

}
