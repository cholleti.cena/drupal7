<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayRolls extends Model
{
    protected $table = 'pay_rolls';

    protected $fillable = [
        'pay_roll_class_id','name','is_active','created_at','updated_at'
    ];

}
