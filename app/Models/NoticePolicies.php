<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoticePolicies extends Model
{
    protected $table = 'notice_and_policies';  

    protected $fillable = [
        'heading','content','created_at','updated_at'
    ]; 
}
