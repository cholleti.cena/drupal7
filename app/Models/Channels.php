<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channels extends Model
{
    protected $table = 'channel';  

    protected $fillable = [
        'user_id','channel_name','description','logo','is_active','is_approve','created_at','updated_at'
    ];  


    public function getLogoAttribute($value)
    {
        if($value):
            return getenv('APP_URL').$value;
        else:
            return getenv('APP_URL').'/'.getenv('NO_PROFILE_IMAGE');
        endif;
    }

    public function getDescriptionAttribute($value)
    {
        return !is_null($value) ? $value : "";
    }

}
