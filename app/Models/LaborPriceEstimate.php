<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LaborPriceEstimate extends Model
{
    protected $table = 'labor_price_estimate';

    protected $fillable = [
        'people', 'price_per_hour', 'base_price', 'pack_per_hour', 'days', 'created_at' , 'updated_at'
    ];
}
