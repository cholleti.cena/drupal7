<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MoveSizes extends Model
{
    protected $table = 'move_sizes';  

    protected $fillable = [
        'name','is_active','created_at','updated_at'
    ];  
}
