<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\AdminUser as Authenticatable;
use Illuminate\Notifications\Notifiable;

class AdminUser extends Authenticatable
{
    use HasFactory,HasApiTokens, Notifiable;

    protected $table = 'admin_users';

    protected $fillable = [
        'name','email', 'password','mobileno','secret','reset_hash','role_id','status','created_by','modified_by','created_at','updated_at'
    ];

    protected $hidden = [
        'password'
    ];

}
