<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstimatedDetails extends Model
{
    protected $table = 'estimated_details';

    protected $fillable = [
        'id','lead_id','number_of_trucks','optradio','crew_size','hourly_rate','min_hours','max_hours','est_hours','moving_hours','travel_time','travel_fee','moving_fee','packing_material','packing_crew_size','packing_hours','packing_fee','fuel_surcharge','total_price_range','est_price_range_one','est_price_range_two','user_id','updated_at','created_at','duration','distance'
    ];

}
