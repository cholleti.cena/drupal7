<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Boltext extends Model
{
    protected $table = 'boltext';

    protected $fillable = [
        'bol','subtotal', 'tip','cc_processing_fee', 'cc_fee', 'shipment_text', 'storage_access', 'notice', 'days_in_advance','article_per_pound_amount', 'overtime_rate', 'overtime_charge_after', 'disable_for_foreman', 'crew_review_popup', 'rating_default_text', 'show_tip_crew_popup', 'tip_for_crew_default', 'created_at','updated_at'
    ];
}
