<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstimateDefaultType extends Model
{
    protected $table = 'estimate_default_type';

    protected $fillable = [
        'name','created_at','updated_at'
    ];
}
