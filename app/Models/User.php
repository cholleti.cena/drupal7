<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory,HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','first_name','middle_name','last_name','email','gender','dob','phone_type','phone','alter_phone','reset_hash','profile_pic','reset_code','referral_code','referred_by','refferal_link','provider','is_registered','is_phone_verify','phone_verified_at','is_email_verified','email_verified_at','last_login','privacy_policy','is_accept_tc','app_version','is_approve','is_active','created_at','updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getProfilePicAttribute($value)
    {
        if($value):
            return getenv('APP_URL').$value;
        else:
            return getenv('APP_URL').'/'.getenv('NO_PROFILE_IMAGE');
        endif;
    }

    public function getIsVerifyAttribute($value)
    {
        return !($value) ? 'InActive' : 'Active';
    }

    public function getPhoneAttribute($value)
    {
        return (int)$value;
    }
}
