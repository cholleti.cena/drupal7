<?php


namespace App\Http\Requests\Auth;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException as HttpResponse;

class VerifyPhoneRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'action' => 'required',
            'email' => 'required',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $res = [
            "status_code" => 202,
            "message" => $validator->errors()->first(),
            "data" => (object)[]
        ];
        throw new HttpResponse(response()->json($res, 202));
    }
}
