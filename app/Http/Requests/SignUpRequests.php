<?php


namespace App\Http\Requests;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException as HttpResponse;

class SignUpRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|max:128|email|email|regex:/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/',
            'phone_type' => 'required',
            'phone' => 'required|min:10|max:11',
            'referred_by' => 'required',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $res = [
            "status_code" => 202,
            "message" => $validator->errors()->first(),
            "data" => (object)[]
        ];
        throw new HttpResponse(response()->json($res, 202));
    }
}
