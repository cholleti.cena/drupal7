<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Auth\AuthServices;
use App\Http\Requests\Auth\SignUpRequests;
use App\Http\Requests\Auth\VerifyPhoneRequests;

class AuthController extends Controller
{
  private $service;

  public function __construct(AuthServices $service)
  {
    $this->service = $service;
  }
    
  public function Getversion()
  {
    $res = $this->service->getversion();
    return response()->json($res, 200);
  }

  public function SignUp(SignUpRequests $request)
  {
    $res = $this->service->signUp($request->all());
    return response()->json($res, 200);
  }

  public function PostLead(Request $request)
  {
     $res = $this->service->postLead($request->all());
     return response()->json($res, 200);
  }

  public function VerifyPhone(Request $request)
  {
    if($request->action == "verify"):
      $res = $this->service->verifyPhone($request->all());
    elseif($request->action == "sendotp"):
      $res = $this->service->resendOTP($request->all());
    elseif($request->action == "subscribe"):
      $res = $this->service->subScribe($request->all());
    elseif($request->action == "contact_us"):
      $res = $this->service->contactUs($request->all());
    else:
      $res['status_code'] = 201;
      $res['message'] = 'Input not found';
      $res['data'] = false;
      return $res;
    endif;
    return response()->json($res, 200);
  }

  public function Login(Request $request)
  {
    $res = $this->service->login($request->all());
    return response()->json($res, 200);
  }

  public function Logout(Request $request)
  {
    $res = $this->service->logOut();
    return response()->json($res, 200);
  }

  public function ForgotPassword(Request $request)
  {
     $res = $this->service->forgotPassword($request->all());
     return response()->json($res, 200);
  }

  public function GetStateWiseCities()
  {
   try{
    $res = $this->service->getStateWiseCities();
     return response()->json($res, 200);
     }
    catch (\Exception $e) 
    {
      $res['status_code'] = 202;
      $res['message'] =  $e->getMessage();
      $res['data'] = false;
      return $res;
    }
  }

  public function GetCities()
  {
   try{
    $res = $this->service->getCities();
     return response()->json($res, 200);
     }
    catch (\Exception $e) 
    {
      $res['status_code'] = 202;
      $res['message'] = $e->getMessage();
      $res['data'] = false;
      return $res;
    }
  }

  public function GetCategories()
  {
   try{
    $res = $this->service->getCategories();
     return response()->json($res, 200);
     }
    catch (\Exception $e) 
    {
      $res['status_code'] = 202;
      $res['message'] = $e->getMessage();
      $res['data'] = false;
      return $res;
    }
  }

  public function GetBanners()
  {
   try{
    $res = $this->service->getBanners();
     return response()->json($res, 200);
     }
    catch (\Exception $e) 
    {
      $res['status_code'] = 202;
      $res['message'] = $e->getMessage();
      $res['data'] = false;
      return $res;
    }
  }

  public function GetFAQs()
  {
   try{
    $res = $this->service->getFAQs();
     return response()->json($res, 200);
     }
    catch (\Exception $e) 
    {
      $res['status_code'] = 202;
      $res['message'] = $e->getMessage();
      $res['data'] = false;
      return $res;
    }
  }

  public function GetAboutUsTerms()
  {
   try{
    $res = $this->service->getAboutUsTerms();
     return response()->json($res, 200);
     }
    catch (\Exception $e) 
    {
      $res['status_code'] = 202;
      $res['message'] = $e->getMessage();
      $res['data'] = false;
      return $res;
    }
  }

  public function GetBlogs()
  {
   try{
    $res = $this->service->getBlogs();
     return response()->json($res, 200);
     }
    catch (\Exception $e) 
    {
      $res['status_code'] = 202;
      $res['error'] = array('message' => $e->getMessage());
      $res['data'] = (object) [];
      return $res;
    }
  }

  public function GetTestimonials()
  {
   try{
    $res = $this->service->getTestimonials();
     return response()->json($res, 200);
     }
    catch (\Exception $e) 
    {
      $res['status_code'] = 202;
      $res['error'] = array('message' => $e->getMessage());
      $res['data'] = (object) [];
      return $res;
    }
  }

  public function GetStatsCount()
  {
     $res = $this->service->getStatsCount();
     return response()->json($res, 200);
  }

  public function GetMetaDataList(Request $request)
  {
      $res = $this->service->getMetaDataList($request->all());
      return response()->json($res, 200);
  }
}
