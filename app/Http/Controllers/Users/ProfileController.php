<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Auth\ProfileServices;
use App\Http\Requests\Auth\SignUpRequests;
use App\Http\Requests\Auth\EventRequests;

class ProfileController extends Controller
{
	private $service;

	public function __construct(ProfileServices $service)
	{
	  $this->service = $service;
	}

    public function GetUserDetails(Request $request)
    {
       $res = $this->service->getUserDetails($request->all());
       return response()->json($res, 200);
    }

    public function UpdateProfile(Request $request)
    {
       if($request->type == "personal"):
       	  $res = $this->service->updateProfile($request->all());
       	  return response()->json($res, 200);
       elseif($request->type == "basic"):
          $res = $this->service->updateBasicProfile($request->all());
          return response()->json($res, 200);
       elseif($request->type == "educations"):
          $res = $this->service->updateEducation($request->all());
          return response()->json($res, 200);
       elseif($request->type == "change_password"):
       	  $res = $this->service->changePassword($request->all());
       	  return response()->json($res, 200);
       elseif($request->type == "profile_pic"):
       	  $res = $this->service->profilePic($request->all());
       	  return response()->json($res, 200);
       elseif($request->type == "place_order"):
          $res = $this->service->placeOrder($request->all());
          return response()->json($res, 200);
       else:
          $res['status_code'] = 201;
          $res['message'] = 'Input not found';
          $res['data'] = false;
          return $res;
       endif;
    }

    public function CouponsList(Request $request)
    {
      $res = $this->service->couponsList($request->all());
      return response()->json($res, 200);
    }

    public function CouponDetails(Request $request)
    {
       $res = $this->service->couponDetails($request->all());
       return response()->json($res, 200);
    }

    public function RedeemCoupon(Request $request)
    {
       $res = $this->service->redeemCoupon($request->all());
       return response()->json($res, 200);
    }

    public function ManageAddress(Request $request)
    {
       if($request->type == "add_address"):
          $res = $this->service->addAddress($request->all());
          return response()->json($res, 200);
       elseif($request->type == "update_address"):
          $res = $this->service->updateAddress($request->all());
          return response()->json($res, 200);
       elseif($request->type == "delete_address"):
          $res = $this->service->deleteAddress($request->all());
          return response()->json($res, 200);
       elseif($request->type == "address_list"):
          $res = $this->service->addressList($request->all());
          return response()->json($res, 200);
       elseif($request->type == "address_details"):
          $res = $this->service->addressDetails($request->all());
          return response()->json($res, 200);
       elseif($request->type == "order_details"):
          $res = $this->service->orderDetails($request->all());
          return response()->json($res, 200);
       elseif($request->type == "order_list"):
          $res = $this->service->orderList($request->all());
          return response()->json($res, 200);
       else:
          $res['status_code'] = 201;
          $res['message'] = 'Input not found';
          $res['data'] = false;
          return $res;
       endif;
    }
}
