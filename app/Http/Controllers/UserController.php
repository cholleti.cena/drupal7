<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SignUpRequests;
use App\Services\Auth\AuthServices;
use App\Models\User;
use App\Helpers\CommonHelper;
use Carbon\Carbon;
use Auth;

class UserController extends Controller
{
    private $service;
    protected $user;
    protected $helpers;

    public function __construct(AuthServices $service,User $user,CommonHelper $helpers)
    {
        $this->user = $user;
        $this->helpers = $helpers;    
    	$this->service = $service;
	}

    public function Getversion()
    {
     $res = $this->service->getversion();
     return response()->json($res, 200);
    }

    public function SignUp(SignUpRequests $request)
    {
     $res = $this->service->signUp($request->all());
     return response()->json($res, 200);
    }

    public function PostLead(SignUpRequests $request)
    {
     $res = $this->service->postLead($request->all());
     return response()->json($res, 200);
    }


	public function AdminLogin(Request $request)
	{
		$res = $this->service->adminLogin($request->all());
	    return response()->json($res, 200);
	}
}
