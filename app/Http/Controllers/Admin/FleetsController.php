<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use Input;
use Session;
use Carbon\Carbon;
use DateTimeZone;
use App\Helpers\CommonHelper;
use App\Repositories\Users\AuthRepository;
use App\Models\Fleets;
use App\Models\Years;
use App\Models\Fleettypes;
use App\Models\Countries;
use App\Models\States;
use App\Models\Ownerships;
use App\Models\VehicleStatus;

class FleetsController extends Controller
{
    private $helper;
    private $service;

    public function __construct(CommonHelper $helper,AuthRepository $authRepo)
    {
        $this->helper = $helper;
        
        $this->authRepo = $authRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getPrivileges()
     {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,7,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,7,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,7,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,7,4);        
        return $privileges;
     }

    
    public function index()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');
          
        $privileges = $this->getPrivileges();
        $fleets = Fleets::selectRaw('fleets.id,nickname,years.year,make,model,type,cuft_capacity,VIN,tag_number,tag_country,tag_state,expires,insurance_company,policy_number,hourly_rate')
        ->join('years','years.id','=','fleets.year')
        ->orderBy('fleets.created_at','desc')
        ->paginate(5);
        return View::make('admin.fleets.index')   
        ->with('fleets',$fleets)      
        ->with('privileges',$privileges);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

       
        $years = Years::get();
        $fleettypes = Fleettypes::get();
        $country = new Countries();
        $countries = $country::selectRaw("*")
        ->orderBy('name','ASC')
        ->where('status', '=','1')
        ->get();

        return View::make('admin.fleets.create')
        ->with('years',$years)
        ->with('countries',$countries)  
        ->with('fleettypes',$fleettypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); 
        $this->validate($request, []);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.fleets.create')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');

            $fleets = Fleets::create($input);     

            //$log = new Log();
            //$log->module_id=13;
            //$log->action='create';      
            //$log->description='Additional Charges ' . $category->name . ' Created Successfully!';
            //$log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            //$log->user_id=Session::get('admin_id'); 
            //$log->category=1;    
            //$log->log_type=1;
            //$this->helper->createLog($log);

            return Redirect::route('fleets.index')->with('success',"Fleet Created Successfully");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

       
        $years = Years::get();
        $fleettypes = Fleettypes::get();
        $country = new Countries();
        $countries = $country::selectRaw("*")
        ->orderBy('name','ASC')
        ->where('status', '=','1')
        ->get();

        $vehicle_statues = VehicleStatus::get();
        $ownships = Ownerships::get();

        $fleet = Fleets::find($id);

        return View::make('admin.fleets.edit')
        ->with('years',$years)
        ->with('countries',$countries)  
        ->with('fleettypes',$fleettypes)
        ->with('fleet',$fleet)
        ->with('vehicle_statues',$vehicle_statues)
        ->with('ownships',$ownships);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all(); 

        $this->validate($request, []);
        $rules = array('');
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) 
        {
            return Redirect::route('admin.fleets.edit',$id)
                ->withInput()
                ->withErrors($validator)
                ->with('warning', 'There were validation errors');
        }
        else
        {   
            $input['modified_by'] = Session::get('admin_id');


            if(isset($input['is_schedulable']))
            {
                if($input['is_schedulable'] === 'on')
                {
                    $input['is_schedulable'] = 1;
                }
                else 
                {
                    $input['is_schedulable'] = 0;
                }
            }
            else 
            {
                $input['is_schedulable'] = 0;
            }
            
            $category = Fleets::find($id);
            $category->update($input);

            return Redirect::route('fleets.index')->with('success',"Fleet Updated Successfully");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
