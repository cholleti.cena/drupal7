<?php

namespace App\Http\Controllers\Admin;
use View;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;

use App\Models\Fleettypes;

class FleettypesController extends Controller
{
    //
    public function index(){
        $fleet = new Fleettypes();
        $data = array();
        $where = array();
        $where['status'] = 1;
        $result =  $fleet::selectRaw(" *")
        ->orderBy('id','DESC')
        ->where('status', '=', 1)
        ->get();
        $data['fleettypes'] = $result;
        return view('admin.application.fleettypes')->with($data);
    }

    /**
     * Show the form for creating a new resource. and update the same
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $input = $request->all();
        $this->validate($request, []);   
 
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.application.fleettypes')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
         else
        {   
            $fleet = new Fleettypes();
            $fleet->name = $input['flletName'];
            $fleet->status = 1;
            $message = 'created';
             
            if($input['record_id'] && $input['record_id'] != ''){ 
                // $updateFleet = array();
                // // $updateFleet["updated_by"] = Session::has('admin_id');
                // $updateFleet["name"] = $input['flletName'];
                // $faq = $fleet->find($input['record_id']);
                // $result  = $faq->update($updateFleet);
                $result = DB::update('update fleettypes set name = ?, updated_by=? where id = ?',[$input['flletName'],Session::has('admin_id'), $input['record_id']]);
                $message = 'updated';
            } else {
                $fleet->status = 1;
                $fleet->created_by = Session::has('admin_id');
                $result = $fleet->save();
            }
            if($result){
                return Redirect::to('/admin/fleettypes')->with('success','Fleet type '.$message.' successfully');
            } else {
                return ["result"=>'Fleet type not screated'];
            }
        }
    }
    
    /** delete records
     * @fieldName column name
     * $value  searching column value
     */
    public function delete($id)
    {
         
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');
            DB::update('update fleettypes set status = ?, updated_by=? where id = ?',[0,Session::has('admin_id'), $id]);
            
            $message = 'Deleted successfully';
            
            return Redirect::to('/admin/fleettypes')->with('success', $message);
    }

    /** fetch records
     * $value  searching id column
     */
    public function edit($value)
    {
         
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

            // $privileges = $this->getPrivileges();
        $records = Fleettypes::selectRaw(" * ")
        ->where('id', '=', $value)
        ->get();
        
        return response()->json([
            'status' => 200,
            'data' => $records,
        ]);  
    }
}
