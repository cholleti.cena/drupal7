<?php

namespace App\Http\Controllers\Admin;
use View;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\ReferredBySource;
use Session;

class ReferredBySourceController extends Controller
{
    public function index(){
        $data = array();
        $where = array();
        $where['status'] = 1;
        $result =  ReferredBySource::selectRaw("*")
        ->orderBy('id','DESC')
        ->where('status', '=', 1)
        ->get();
        $data['sources'] = $result;
        return view('admin.application.referredbysource')->with($data);
    }
    /**
     * Show the form for creating a new resource. and update the same
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $input = $request->all();
        $this->validate($request, []);   
 
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.application.referredbysource')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $referredSource = new ReferredBySource();
            $referredSource->name = $input['referredName'];
            $referredSource->cost = $input['referredCost'];
           
            $message = 'Created';
            if($input['record_id'] && $input['record_id'] != ''){ 
                $result = DB::update('update referred_by_sources set name = ?, cost = ?, updated_by=? where id = ?',[$input['referredName'],$input['referredCost'],Session::has('admin_id'), $input['record_id']]);
                $message = 'Updated';
            } else {
                $referredSource->status = 1;
                $referredSource->created_by = Session::has('admin_id');
                $result = $referredSource->save();
            }
            if($result){
                return Redirect::to('/admin/referredbysources')->with('success', $message.' successfully');
            } else {
                return ["result"=>'Fleet type not screated'];
            }
        }
    }

    /** delete records
     * @fieldName column name
     * $value  searching column value
     */
    public function delete($id)
    {
         
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

            DB::update('update referred_by_sources set status = ?, updated_by=? where id = ?',[0,Session::has('admin_id'), $id]);
            
            $message = 'Deleted successfully';
            
        return Redirect::to('/admin/referredbysources')->with('success', $message);
    }
    
    /** fetch records
     * $value  searching id column
     */
    public function edit($value)
    {
         
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $records = ReferredBySource::selectRaw(" * ")
        ->where('id', '=', $value)
        ->get();
        
        return response()->json([
            'status' => 200,
            'data' => $records,
        ]);  
    }
}
