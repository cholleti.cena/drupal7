<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use Input;
use Session;
use Carbon\Carbon;
use DateTimeZone;
use App\Helpers\CommonHelper;
use App\Repositories\Users\AuthRepository;
use App\Models\Addendums;
use App\Models\Verbiage;
use App\Models\Boltext;
use App\Models\Termsandcondtions;
use App\Models\NoticePolicies;
use App\Models\InvoiceNotes;
use App\Models\InvoiceMaxPrice;

class BolinvoiceController extends Controller
{
    private $helper;
    private $service;

    public function __construct(CommonHelper $helper,AuthRepository $authRepo)
    {
        $this->helper = $helper;
        
        $this->authRepo = $authRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getPrivileges()
     {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,7,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,7,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,7,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,7,4);        
        return $privileges;
     }

    public function index()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
            
        $addendums = Addendums::selectRaw('id,name,data,if(ifnull(status,1)=1,"Active","Inactive") as status')
        ->orderBy('created_at','desc')
        ->paginate(5);

        return View::make('admin.bolinvoice.index')   
        ->with('addendums',$addendums)      
        ->with('privileges',$privileges);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

       

        return View::make('admin.bolinvoice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $input = $request->all(); 
        $this->validate($request, []);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.bolinvoice.create')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');

            $endums = Addendums::create($input);       

            //$log = new Log();
            //$log->module_id=13;
            //$log->action='create';      
            //$log->description='Additional Charges ' . $category->name . ' Created Successfully!';
            //$log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            //$log->user_id=Session::get('admin_id'); 
            //$log->category=1;    
            //$log->log_type=1;
            //$this->helper->createLog($log);

            return Redirect::route('admin.bolinvoice.index')->with('success',$log->description);
        
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin'); 

        $endums = Addendums::find($id);
 
        return View::make('admin.bolinvoice.edit', compact('endums'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all(); 

         $this->validate($request, [
            'name'  => 'required']);
        $rules = array('');
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) 
        {
            return Redirect::route('admin.bolinvoice.edit',$id)
                ->withInput()
                ->withErrors($validator)
                ->with('warning', 'There were validation errors');
        }
        else
        {   
            $input['modified_by'] = Session::get('admin_id');
            
            $endums = Addendums::find($id);
            $endums->update($input);

            
        return Redirect::route('bolinvoice.index')->with('success',"Addendums updated Successfully");
        
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $endums = Addendums::find($id);       
       
        if (is_null($endums))
        {
         return Redirect::back()->with('warning','Additional Charges Details Are Not Found!');
        }
        else
        {
        
            $endums->delete();

            
           return Redirect::back()->with('success',$log->description);
        }
    }
    public function getVerbiage()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        //$privileges = $this->getPrivileges();
            
        $addendums = Verbiage::selectRaw('id,signature_text,signature_name,required,hide')
        ->paginate(5);

        return View::make('admin.bolinvoice.step_three')   
        ->with('addendums',$addendums);
    }
    public function updateVerbiage(Request $request)
    {

       if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');
            
      return $input = $request->all();  

       for ($i=0; $i < count($input['sign_id']); $i++) 
       { 
            $data['signature_name'] = $input['signature_name'][$i];
            $data['required'] = $input['required'][$i]; 
            $data['hide'] = $input['hide'][$i]; 
            $endums = Verbiage::find($input['sign_id'][$i]);
            $endums->update($data);
       }

       return Redirect::route('bolinvoice.index')->with('success',"Verbiage updated Successfully");
    }
    public function getBoltext  ()
    {
        
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        //$privileges = $this->getPrivileges();
            
        $addendums = Boltext::selectRaw('id,bol,subtotal,tip,cc_processing_fee,cc_fee,shipment_text,storage_access,notice,days_in_advance,article_per_pound_amount,overtime_rate,disable_for_foreman,crew_review_popup,rating_default_text,show_tip_crew_popup,tip_for_crew_default')
        ->paginate(5);

        
        return View::make('admin.bolinvoice.step_one')   
        ->with('addendums',$addendums);
    }
    public function updateBolText(Request $request)
    {

       if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');
            
       $input = $request->all(); 
       
       $data['bol'] = $input['bol'];
       $data['subtotal'] = $input['subtotal']; 
       $data['tip'] = $input['tip']; 
       $data['cc_processing_fee'] = $input['cc_processing_fee'];
       $data['cc_fee'] = $input['cc_fee'];
       $data['shipment_text'] = $input['shipment_text']; 
       $data['storage_access'] = $input['storage_access'];
       $data['notice'] = $input['notice'];
       $data['days_in_advance'] = $input['days_in_advance']; 
       $data['article_per_pound_amount'] = $input['article_per_pound_amount'];
       $data['overtime_rate'] = $input['overtime_rate']; 
       $data['overtime_charge_after'] = $input['overtime_charge_after'];
       $data['disable_for_foreman'] = $input['disable_for_foreman']; 
       $data['crew_review_popup'] = $input['crew_review_popup']; 
       $data['rating_default_text'] = $input['rating_deaddTermsandConditionsfault_text']; 
       $data['show_tip_crew_popup'] = $input['show_tip_crew_popup']; 
       $data['tip_for_crew_default'] = $input['tip_for_crew_default'];

       $endums = Boltext::find($input['id']);

       $endums->update($data);
       

       return Redirect::route('bolinvoice.index')->with('success',"Boltext updated Successfully");
    }
    public function getTerms()
    {

        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        //$privileges = $this->getPrivileges();
            
         $addendums = Termsandcondtions::selectRaw('id,heading,content')
         ->paginate(5);
        
        return View::make('admin.bolinvoice.step_terms_condition')  
        ->with('addendums',$addendums);
    }

    public function addTermsandconditions(Request $request)
    {
        
        $input = $request->all(); 
        $this->validate($request, []);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.bolinvoice.index')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');
             
            $terms = Termsandcondtions::create($input);       

            //$log = new Log();
            //$log->module_id=13;
            //$log->action='create';      
            //$log->description='Additional Charges ' . $category->name . ' Created Successfully!';
            //$log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            //$log->user_id=Session::get('admin_id'); 
            //$log->category=1;    
            //$log->log_type=1;
            //$this->helper->createLog($log);

            return Redirect::route('bolinvoice.index')->with('success',"Terms and Conditions Added Successfully");
        
        }

    }
     public function editTerms($id)
    {
        
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin'); 

         $records = Termsandcondtions::selectRaw(" * ")
        ->where('id', '=', $id)
        ->get();
        
        return response()->json([
            'status' => 200,
            'data' => $records,
        ]);  
    }
    public function updateTerms(Request $request)
    {
       $input = $request->all(); 
        
       $data['id'] = $input['id'];
       $data['heading'] = $input['heading']; 
       $data['content'] = $input['content']; 
      

        $endums = Termsandcondtions::find($input['id']);
        $endums->update($data);

            
        return Redirect::route('bolinvoice.index')->with('success',"Termsandcondtions updated Successfully");

    }

    public function deleteTerms($id)
    {
        $endums = Termsandcondtions::find($id);       
       
        if (is_null($endums))
        {
         return Redirect::back()->with('warning','Additional Charges Details Are Not Found!');
        }
        else
        {
        
            $endums->delete();

            
           return Redirect::back()->with('success',"Deleted Successfully");
        }
    }

    public function getNoticeandpolicies()
    {

        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        //$privileges = $this->getPrivileges();
            
        $endums = NoticePolicies::selectRaw('id,heading,content')
        ->paginate(11);
        
        return View::make('admin.bolinvoice.step_notice_policies')   
        ->with('endums',$endums);

    }

    public function editNoticeandpolicies($id)
    {
        
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin'); 

         $records = NoticePolicies::selectRaw(" * ")
        ->where('id', '=', $id)
        ->get();
        
        return response()->json([
            'status' => 200,
            'data' => $records,
        ]);  
    }
    public function updateNoticeandpolicies(Request $request)
    {
       $input = $request->all(); 
        
       $data['id'] = $input['id'];
       //$data['heading'] = $input['heading']; 
       $data['content'] = $input['content']; 
      

        $endums = NoticePolicies::find($input['id']);
        $endums->update($data);

            
        return Redirect::route('bolinvoice.index')->with('success',"NoticePolicies updated Successfully");

    }

    
     public function getInvoiceNotes()
    {

        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        //$privileges = $this->getPrivileges();
            
        
        $addendums = InvoiceNotes::selectRaw('id,notice_content')
        ->paginate(5);
        
        return View::make('admin.bolinvoice.step_invoice_notes')   
        ->with('addendums',$addendums);
    }
    public function editInvoiceNotes($id)
    {
        
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin'); 

         $records = InvoiceNotes::selectRaw(" * ")
        ->where('id', '=', $id)
        ->get();
        
        return response()->json([
            'status' => 200,
            'data' => $records,
        ]);  
    }
    public function updateInvoiceNotes(Request $request)
    {
       $input = $request->all(); 
        
       $data['id'] = $input['id']; 
       $data['notice_content'] = $input['notice_content']; 
      

        $endums = InvoiceNotes::find($input['id']);
        $endums->update($data);

            
        return Redirect::route('bolinvoice.index')->with('success',"InvoiceNotes updated Successfully");

    }
    public function getInvoiceMaxPrice()
    {
         
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        //$privileges = $this->getPrivileges();
            
        $endums = InvoiceMaxPrice::selectRaw('id,heading,content')
        ->paginate(5);
        
        return View::make('admin.bolinvoice.step_invoice_maxprice')   
        ->with('endums',$endums);

    }
    public function editInvoiceMaxPrice($id)
    {
        
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin'); 

         $records = InvoiceMaxPrice::selectRaw(" * ")
        ->where('id', '=', $id)
        ->get();
        
        return response()->json([
            'status' => 200,
            'data' => $records,
        ]);  
    }
    public function updateInvoiceMaxPrice(Request $request)
    {
       $input = $request->all(); 
        
       $data['id'] = $input['id']; 
       $data['content'] = $input['content']; 
      

        $endums = InvoiceMaxPrice::find($input['id']);
        $endums->update($data);

            
        return Redirect::route('bolinvoice.index')->with('success',"Termsandcondtions updated Successfully");

    }
}
