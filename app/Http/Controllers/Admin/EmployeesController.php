<?php

namespace App\Http\Controllers\Admin;
use View;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
use App\Models\Log;
use App\Models\Employees;
use App\Models\EmployeesPhoneNumbers;
use App\Models\EmployeesEmailIds;
use App\Models\EmployeesAddresses;
use App\Models\EmployeesClientReview;
use App\Models\EmployeesDisciplinaryNotes;
use App\Models\EmployeesPayroll;
use App\Models\EmployeesAuditTrail;
use App\Models\EmployeesProspects;
use App\Models\EmployeesEvents;
use App\Models\User;
use App\Models\Timezones;
use App\Models\Countries;
use App\Models\States;
use App\Repositories\Users\AuthRepository;
use App\Helpers\CommonHelper;
use Illuminate\Support\Facades\Storage;
 
class EmployeesController extends Controller
{
    private $helper;
    private $service;

    public function __construct(CommonHelper $helper, AuthRepository $authRepo)
    {
        $this->helper = $helper;
        $this->authRepo = $authRepo;
    }
    
    public function __invoke()
    {
         
    }

    public function index(){
        $employees = new Employees();
        $data = array();
        $where = array();
        $where['status'] = 1;
        $result = array();
        $data['results'] = array();
        // if($fieldName && $fieldName != "" && $value > 0 && count($value) < 10 ){
        //     $result =  $employees::selectRaw(" *")
        //     ->orderBy('id','DESC')
        //     ->where('is_active', '=', $value)
        //     ->get();
        //     return response()->json([
        //         'status' => 200,
        //         'data' => $result,
        //     ]); 
        // } else {
            
        // }
        $result =  $employees::selectRaw(" *")
            ->orderBy('id','DESC')
            ->where('is_active', '=', 1)
            ->get();
            $data['results'] = $result;
        return view('admin.hr.employees')->with($data);
    }

    public function createform(){
        $data = array();
        $tz = new Timezones();
        $country = new Countries();
        $tzs =  $tz::selectRaw("*")
            ->orderBy('label','ASC')
            ->get();
        
        $countries = $country::selectRaw("*")
        ->orderBy('name','ASC')
        ->where('status', '=','1')
        ->get();
//  echo "Naresh";  exit;
        return View::make('admin.hr.employees_createform', compact('tzs'), compact('countries'))->with($data);
    }
    
    public function update(Request $request){
       $statusMsg = 'Updated ';
        $u = $request->all();
        if($u['tabName'] == 'Identity'){
            $request->validate([
                'first_name'=>'required',
                'last_name'=>'required',
                'user_name'=>'required',
                'ssn'=>'required',
                'designation'=>'required',
                'gender'=>'required',
                'relationship'=>'required',
            ]);
            $dob = explode("/", $u['date_of_birth']);
            DB::update('update employees set first_name = ?, middle_name = ?, last_name=?, nick_name=?, user_name=?, designation=?, time_zone=?, ssn=?, date_of_birth=?, gender=?, relationship=?,  get_to_know=?, biography=?, updated_by=?  where id = ?',[$u['first_name'],$u['middle_name'],$u['last_name'],$u['nick_name'],$u['user_name'],$u['designation'],$u['time_zone'],$u['ssn'],$dob[2]."-".$dob[0]."-".$dob[1],$u['gender'],$u['relationship'], $u['get_to_know'],$u['biography'],Session::has('admin_id'), $u['record_id']]);
            
        } else if($u['tabName'] == 'Login_Permissions'){
            $groupAccess = $u['groups_access'];
            $grpAccess = implode(",", $groupAccess);
             
            DB::update('update employees set user_name = ?, email = ?, driver_license=?, password=?, two_factor_authentication=?, chat_prmsn=?, incoming_call_prmsn=?, customer_chat_prmsn=?, chat_admin_prmsn=?, call_center_contact_prmsn=?, billing_user_mgmt_prmsn=?,  prop_app_user_prmsn=?, foreman_prmsn=?, groups_access=?, is_active=?, updated_by=? where id = ?',[$u['username'],$u['email'],$u['driving_license'],$u['password'],$u['two_factor_authentication'],$u['chat_prmsn'],$u['incoming_call_prmsn'],$u['customer_chat_prmsn'],$u['chat_admin_prmsn'] ,$u['call_center_contact_prmsn'],$u['billing_user_mgmt_prmsn'], $u['prop_app_user_prmsn'],$u['foreman_prmsn'],$grpAccess,$u['is_active'],Session::has('admin_id'), $u['record_id']]);
            
        }
        
        else if($u['tabName'] == 'Contact_Information'){
            
            $phoneTables = new EmployeesPhoneNumbers();
            $update = array();
            $update["is_active"] = 0;
            // $phone = $phoneTables->find(array('emp_id'=>$u['record_id']));
            // $result  = $phone->update($update);
            EmployeesPhoneNumbers::where('emp_id', '=', $u['record_id'])->update($update);

            $emailids = new EmployeesEmailIds();
            // $update["is_active"] = 0;
            EmployeesEmailIds::where('emp_id', '=', $u['record_id'])->update($update);
            // $emails = $emailids->find(array('emp_id'=>$u['record_id']));
            // $result  = $emails->update($update);

            $empAddress = new EmployeesAddresses();
            // $update["is_active"] = 0;
            EmployeesAddresses::where('emp_id', '=', $u['record_id'])->update($update);
            // $address = $phoneTables->find(array('emp_id'=>$u['record_id']));
            // $result  = $address->update($update);
            $emp_id = $u['record_id'];
            foreach($u['phone_type'] as $phoneKey => $phones){
                $phone = new EmployeesPhoneNumbers();
                $phoneType = $u['phone_type'][$phoneKey];
                $phoneNumber = $u['phone_number'][$phoneKey];
                $phone->emp_id = $emp_id;
                $phone->phone_type = $phoneType;
                $phone->phone_number = $phoneNumber;
                $phone->is_active = 1;
                $phone->created_by = 1;
                $phone->updated_by = 1;
                $phone->save();
                // Save Phone
            }
            // Email ids
            foreach($u['email_ids'] as $emailKey => $email){
                $emailids = new EmployeesEmailIds();
                $emailids->emp_id = $emp_id;
                $emailids->email_type = $input['email_type'][$emailKey];
                $emailids->email_id = $email;
                $emailids->is_active = 1;
                $emailids->created_by = 1;
                $emailids->updated_by = 1;
                $emailids->save();
                // Save Email Id
            }
    
            // Address ids
            foreach($u['street'] as $addressKey => $address){
                $empAddress = new EmployeesAddresses();
                $empAddress->emp_id = $emp_id;
                $empAddress->street = $u['street'][$addressKey];
                $empAddress->suite = $u['suite'][$addressKey];
                $empAddress->country = $u['country'][$addressKey];
                $empAddress->state = $u['states'][$addressKey];
                $empAddress->city = $u['city'][$addressKey];
                $empAddress->zip = $u['zip'][$addressKey];
                $empAddress->is_active = 1;
                $empAddress->created_by = 1;
                $empAddress->updated_by = 1;
                $empAddress->save();
                // Save Email Id
            }
            // return Redirect::to('/admin/employees')->with('success','Employee contact information updated successfully');
        }  else if($u['tabName'] == 'Employment_Details'){ 
            $hire_date = explode("/", $u['hire_date']);
            $termination_date = explode("/", $u['termination_date']);
            $hDate = $hire_date[2]."-".$hire_date[0]."-".$hire_date[1];
            $tDate = $termination_date[2]."-".$termination_date[0]."-".$termination_date[1];
            DB::update('update employees set employee_status = ?, employee_type = ?, hire_date=?, termination_date=?, hourly_rate=?, commission_rate=?, updated_by=?  where id = ?',[$u['employee_status'],$u['employee_type'],$hDate,$tDate,$u['hourly_rate'],$u['commission_rate'], Session::has('admin_id'), $u['record_id']]);
        } else if($u['tabName'] == 'Payroll'){
            $hire_date = explode("/", $u['payrollLog_entry_date']);
            $hDate = $hire_date[2]."-".$hire_date[0]."-".$hire_date[1];
            $hourlyRate = '0.0';
            $hourlyRate = $u['payrollLog_hourly_rate'];
            if($u['payrollLog_hourly_rate'] == '--'){
                $hourlyRate = '0.0';
            }
            $amount = 0;
            if($u['payrollLog_tip_amount'] == '--'){
                $amount = '0';
            }
            $desc = $u['payrollLog_description'];
            $payroll = new EmployeesPayroll();
            $payroll->emp_id = $u['record_id'];
            $payroll->class_select = $u['payrollLog_class_select'];
            $payroll->company_branch = $u['payrollLog_company_branch'];
            $payroll->entry_date = $hDate;
            $payroll->class_name = $u['payrollLog_class'];
            $payroll->customer = $u['payrollLog_customer'];
            $payroll->hourly_rate = $hourlyRate;
            $payroll->hours = $u['payrollLog_hours'];
            $payroll->travel = $u['payrollLog_travel'];
            $payroll->commission = $u['payrollLog_commission'];
            $payroll->amount = $amount;
            $payroll->description = $desc;
            $payroll->is_active = 1;
            $payroll->updated_by = Session::has('admin_id');
            $payroll->created_by = Session::has('admin_id');
            $payroll->save();
            // DB::update('update employees_payrolls set emp_id = ?, class_select = ?, company_branch=?, entry_date=?,class_name=?,customer=?, hourly_rate=?, hours=?, travel=?,commission=?,amount=? ,description=?, is_active=?, updated_by=?, created_by=?', [$u['record_id'],$u['payrollLog_class_select'], $u['payrollLog_company_branch'], $hDate, $u['payrollLog_class'], $u['payrollLog_customer'], $hourlyRate, $u['payrollLog_hours'], $u['payrollLog_travel'], $u['payrollLog_commission'], $amount, $desc, 1, Session::has('admin_id'), Session::has('admin_id')]);
             
            $message = 'Created ' . $u['tabName'].' - '.$u['payrollLog_class_select']. ' successfully';
        } else if($u['tabName'] == 'Audit_Trail'){
            $fromDate = explode("/", $u['fromDate']);
            $toDate = explode("/", $u['toDate']);
            $fDate = $fromDate[2]."-".$fromDate[0]."-".$fromDate[1];
            $tDate = $toDate[2]."-".$toDate[0]."-".$toDate[1];

            $auditTrailModel = new EmployeesAuditTrail();

            $auditTrail = $auditTrailModel::selectRaw("*")
                        ->orderBy('created_at','DESC')
                        ->where('created_at', '>=', $fDate)
                        ->where('created_at', '<=', $tDate)
                        ->get();
            $result = ['Audit_Trail' => $auditTrail];
            return response()->json([
                'status' => 200,
                'data' => $result,
            ]);
        } else if($u['tabName'] == 'Disciplinary'){
            $discipnary_date = explode("/", $u['discipnary_date']);
            $discipnary_date = $discipnary_date[2]."-".$discipnary_date[0]."-".$discipnary_date[1];
            $DisciplinaryNotes = new EmployeesDisciplinaryNotes();
            $DisciplinaryNotes->date  = $discipnary_date;
            $DisciplinaryNotes->customer_name = $u["discipnary_customer"];
            $DisciplinaryNotes->category = $u["discipnary_category"];
            $DisciplinaryNotes->issues = $u["discipnary_issue"];
            $DisciplinaryNotes->note = $u["discipnary_notes"];
            $DisciplinaryNotes->emp_id = $u["emp_id"];
            $DisciplinaryNotes->created_by = Session::has('admin_id');
            $DisciplinaryNotes->is_active = 1;
             
            $dispRecord = $u["discipnary_record_id"];
            if($dispRecord == ''){
                $statusMsg = 'Created '; 
                $DisciplinaryNotes->save();
            } else {
                DB::update('update employees_disciplinary_notes set emp_id = ?, date = ?, customer_name=?, category=?, issues=?, updated_by=?  where id = ?',[$u['emp_id'],$discipnary_date,$u["discipnary_customer"],$u['discipnary_category'],$u['discipnary_issue'], Session::has('admin_id'), $u['discipnary_record_id']]);
            }
            
            // $hire_date = explode("/", $u['hire_date']);
            // $termination_date = explode("/", $u['termination_date']);
            // $hDate = $hire_date[2]."-".$hire_date[0]."-".$hire_date[1];
            // $tDate = $termination_date[2]."-".$termination_date[0]."-".$termination_date[1];
            // DB::update('update employees set employee_status = ?, employee_type = ?, hire_date=?, termination_date=?, hourly_rate=?, commission_rate=?, updated_by=?  where id = ?',[$u['employee_status'],$u['employee_type'],$hDate,$tDate,$u['hourly_rate'],$u['commission_rate'], Session::has('admin_id'), $u['record_id']]);
        }

        $message = $statusMsg . $u['tabName'].' successfully';
        return response()->json([
            'status' => 200,
            'data' => $message,
        ]); 

        

        // return view('admin.hr.employees_create', compact('tzs'), compact('countries'))->with($data);
    }
    //Display File Name
    // echo 'File Name: '.$file->getClientOriginalName();
    
    // //Display File Extension
    // echo 'File Extension: '.$file->getClientOriginalExtension();
    
    // //Display File Real Path
    // echo 'File Real Path: '.$file->getRealPath();
    
    // //Display File Size
    // echo 'File Size: '.$file->getSize();
    
    // //Display File Mime Type
    // echo 'File Mime Type: '.$file->getMimeType();
    public function create(Request $request){
         
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'user_name'=>'required',
            'ssn'=>'required',
            'designation'=>'required',
            'gender'=>'required',
            'relationship'=>'required',
            'employee_photo'=>'required',
            'employee_signature'=>'required',
        ]);
        
        $input = $request->all();
        // Upload pic
        $picPath = 'employees/profile/picture';
        $photo = $request->file('employee_photo');
     
        //Move Uploaded File
        // $destinationPath = 'uploads/';
        $photo->move($picPath, $photo->getClientOriginalName());
        // $photo->store($picPath);
        // print_r($img1);

        $signature = $request->file('employee_signature');
        $destinationPath = 'employees/profile/signatures';
        $signature->move($destinationPath,$signature->getClientOriginalName());
        // $signature->store($signPath);
        // $photoPath = $picPath . $photo->getClientOriginalName();
        // $signaturePath = $signPath . $signature->getClientOriginalName();
        
        // print_r($img2);
        $dob = explode("/", $input['date_of_birth']);
        $employee = new Employees();
        $employee->emp_no = date("mYdHis");
        $employee->first_name = $input['first_name'];
        $employee->middle_name = $input['middle_name'];
        $employee->last_name = $input['last_name'];
        $employee->user_name = $input['user_name'];
        $employee->nick_name = $input['nick_name'];
        $employee->designation = $input['designation'];
        $employee->time_zone = $input['time_zone'];
        $employee->ssn = $input['ssn'];
        $employee->date_of_birth = $dob[2]."-".$dob[0]."-".$dob[1];
        $employee->gender = $input['gender'];
        $employee->relationship = $input['relationship'];
        $employee->profile_pic = '/employees/profile/picture/'.$photo->getClientOriginalName();
        $employee->signature_pic = '/employees/profile/signatures/'.$signature->getClientOriginalName();
        $employee->is_active = 1;
        $employee->join_date = date('Y-m-d');
        $employee->end_date = date('Y-m-d');
        $employee->created_by = Session::has('admin_id');
        $employee->save();
        $emp_id = $employee->id;
        
        
        foreach($input['phone_type'] as $phoneKey => $phones){
            $phone = new EmployeesPhoneNumbers();
            $phoneType = $input['phone_type'][$phoneKey];
            $phoneNumber = $input['phone_number'][$phoneKey];
            $phone->emp_id = $emp_id;
            $phone->phone_type = $phoneType;
            $phone->phone_number = $phoneNumber;
            $phone->is_active = 1;
            $phone->created_by = 1;
            $phone->updated_by = 1;
            $phone->save();
            // Save Phone
        }
        // Email ids
        foreach($input['email'] as $emailKey => $email){
            $emailids = new EmployeesEmailIds();
            $emailids->email_type = $input['email_type'][$emailKey];
            $emailids->emp_id = $emp_id;
            $emailids->email_id = $email;
            $emailids->is_active = 1;
            $emailids->created_by = 1;
            $emailids->updated_by = 1;
            $emailids->save();
            // Save Email Id
        }

        // Address ids
        foreach($input['street'] as $addressKey => $address){
            $empAddress = new EmployeesAddresses();
            $empAddress->emp_id = $emp_id;
            $empAddress->street = $input['street'][$addressKey];
            $empAddress->suite = $input['suite'][$addressKey];
            $empAddress->country = $input['country'][$addressKey];
            $empAddress->state = $input['states'][$addressKey];
            $empAddress->city = $input['city'][$addressKey];
            $empAddress->zip = $input['zip'][$addressKey];
            $empAddress->is_active = 1;
            $empAddress->created_by = 1;
            $empAddress->updated_by = 1;
            $empAddress->save();
            // Save Email Id
        }

        return Redirect::to('/admin/employees')->with('success','Employee created successfully');

         
        // $employee->status = 1;
        // $message = 'created';
            
        // if($input['record_id'] && $input['record_id'] != ''){ 
        //     // $updateFleet = array();
        //     // // $updateFleet["updated_by"] = Session::has('admin_id');
        //     // $updateFleet["name"] = $input['flletName'];
        //     // $faq = $employee->find($input['record_id']);
        //     // $result  = $faq->update($updateFleet);
        //     $result = DB::update('update fleettypes set name = ?, updated_by=? where id = ?',[$input['flletName'],Session::has('admin_id'), $input['record_id']]);
        //     $message = 'updated';
        // } else {
        //     $employee->status = 1;
        //     $employee->created_by = Session::has('admin_id');
        //     $result = $employee->save();
        // }

        // return view('admin.hr.employees_createform', compact('tzs'), compact('countries'))->with($data);
         
    }
    public function createemployee(Request $req){
         
        $data = array();
        $tz = new Timezones();
        $country = new Countries();
        $states = new States();
        $tzs =  $tz::selectRaw("*")
            ->orderBy('label','ASC')
            ->get();
        
        $countries = $country::selectRaw("*")
        ->orderBy('name','ASC')
        ->where('status', '=','1')
        ->get();

        return view('admin.hr.employees_create', compact('tzs'), compact('countries'))->with($data);
         
    }

    public function edit($empid, $section=''){
        // // echo $empid;
        $result = [];
        $data = array();
        $tz = new Timezones();
        $country = new Countries();
        $empAddress = new EmployeesAddresses();
        $states = new States();
        $tzs =  $tz::selectRaw("*")
            ->orderBy('label','ASC')
            ->get();
        
        $countries = $country::selectRaw("*")
        ->orderBy('name','ASC')
        ->where('status', '=','1')
        ->get();
        
        $employees = new Employees();
        $emp = $employees::selectRaw(" * ")
        ->where('id', '=', $empid)
        ->get();

        //  echo $emp; exit;
        $data['emp'] = $emp[0];
        // echo $id;
        if($section == ''){
            return view('admin.hr.employees_edit', compact('tzs'), compact('countries'))->with($data);
        } else {
            $qry = '';
            if($section == 'Contact_Information'){
                $phoneNumbers = new EmployeesPhoneNumbers();
                
                $phoneNumbers = $phoneNumbers::selectRaw(' phone_type, phone_number ')
                ->orderBy('id','ASC')
                ->where('is_active', '=','1')
                ->where('emp_id', '=',$empid)
                ->get();

                $emailTable = new EmployeesEmailIds();
                $emails = $emailTable::selectRaw(' email_id, email_type ')
                ->orderBy('id','ASC')
                ->where('is_active', '=','1')
                ->where('emp_id', '=',$empid)
                ->get();
                
               
                $addressTable = new EmployeesAddresses();
                $address = $addressTable::selectRaw(' emp_id, street, suite, country, state, city,  zip ')
                ->orderBy('id','ASC')
                ->where('is_active', '=','1')
                ->where('emp_id', '=',$empid)
                ->get();
                 
                $states = new States();
                $states = $states::selectRaw("states.name as name, states.id as id, states.country_id as cid")
                ->join('countries','countries.id','=','states.country_id')
                ->join('employees_addresses','employees_addresses.country','=','countries.id')
                ->orderBy('name','ASC')
                ->where('employees_addresses.is_active', '=','1')
                ->where('employees_addresses.emp_id', '=',$empid)
                ->where('states.status', '=','1')
                ->groupBy('states.id')
                ->get();

                $result = ['phoneNumbers' => $phoneNumbers, 'emails' => $emails, 'address' => $address, 'states' => $states];

            } else if($section == 'Employment_Details'){

                $employees = new Employees();
                $empdata = $employees::selectRaw(" id,emp_no,user_name, get_to_know, biography, employee_status, employee_type, hire_date, termination_date, hourly_rate, commission_rate, join_date, end_date  ")
                ->where('id', '=', $empid)
                ->get();

                $result = ['employeement_details' => $empdata[0]];
            } else if($section == 'Review'){

                $empClientReview = new EmployeesClientReview();
                $empReviewData = $empClientReview::selectRaw("id, emp_id, client_id, customer_id, customer_name, estimate_id, crew_member_id, rating_number, comments")
                ->where('emp_id', '=', $empid)
                ->get();

                $result = ['empReviewData' => $empReviewData];
            } else if($section == 'Disciplinary'){
                $empDisciplinary = new EmployeesDisciplinaryNotes();
                $empDisciplinaryData = $empDisciplinary::selectRaw("id, date, emp_id, customer_id, customer_name, estimate_id, category, issues, comments")
                ->where('emp_id', '=', $empid)
                ->get();

                $result = ['empDisciplinaryData' => $empDisciplinaryData];
            } else if($section == 'Payroll'){
                $payroll = new EmployeesPayroll();

                $moving = $payroll::selectRaw("*")
                ->orderBy('created_at','DESC')
                ->where('class_select', '=','moving')
                ->get();
                $miscHours = $payroll::selectRaw("*")
                ->orderBy('created_at','DESC')
                ->where('class_select', '=','miscHours')
                ->get();
                $tip = $payroll::selectRaw("*")
                ->orderBy('created_at','DESC')
                ->where('class_select', '=','tip')
                ->get();
                $bonus = $payroll::selectRaw("*")
                ->orderBy('created_at','DESC')
                ->where('class_select', '=','bonus')
                ->get();
                $deduction = $payroll::selectRaw("*")
                ->orderBy('created_at','DESC')
                ->where('class_select', '=','deduction')
                ->get();

                $paroll = array(
                    'realdata' => array(),
                    'moving_packing' => $moving,
                    'misc_hours' => $miscHours,
                    'manual_entry' => array(),
                    'tips' => $tip,
                    'bonuses_perdiem' => $bonus,
                    'deductions' => $deduction,
                );

                $result = ['Payroll' => $paroll];
            } else if($section == 'Expertise'){
                $result = ['Expertise' => array()];
            } else if($section == 'sEvents'){
                $employeesEvents = new EmployeesEvents();
                $result = ['Events' => array()];
            } else if($section == 'Audit_Trail'){
                $test = array( 0 => [ ]);
                $result = ['Audit_Trail' => array()];
            } else if($section == 'Assigned_Prospects'){

                $employeesPros = new EmployeesProspects();

                $hotProspect = $employeesPros::selectRaw(" * ")
                ->where('emp_id', '=', $empid)
                ->where('type', '=', 'hot')
                ->get();
                
                $warmProspect = $employeesPros::selectRaw(" * ")
                ->where('emp_id', '=', $empid)
                ->where('type', '=', 'warm')
                ->get();
                
                $coldProspect = $employeesPros::selectRaw(" * ")
                ->where('emp_id', '=', $empid)
                ->where('type', '=', 'cold')
                ->get();

                $result = ['hotProspect' => $hotProspect, 'warmProspect' => $warmProspect, 'coldProspect' => $coldProspect];

            } else if($section == 'Assigned_Customers'){
                $user = new User();

                $userResult = $user::selectRaw(" concat(first_name,' ', last_name) as name, referred_by, is_active ")
                ->get();

                $result = ['users' => $userResult];

            } else if($section == 'Events'){
                $employeesEvents = new EmployeesEvents();
                $empEvents = $employeesEvents::selectRaw("*")
                ->orderBy('created_at','DESC')
                ->where('emp_id', '=',$empid)
                ->where('is_active', '=','1')
                ->get();
                $result = ['events' => $empEvents];
            } else if($section == 'Login_Permissions'){
                $employees = new Employees();
                $empdata = $employees::selectRaw(" id, emp_no, user_name,driver_license, email, two_factor_authentication, chat_prmsn, incoming_call_prmsn, customer_chat_prmsn,  chat_admin_prmsn, call_center_contact_prmsn, billing_user_mgmt_prmsn, prop_app_user_prmsn, foreman_prmsn, groups_access, is_active ")
                ->where('id', '=', $empid)
                ->get();

                $result = ['Login_Permissions' => $empdata];
            }
            

            return response()->json([
                'status' => 200,
                'data' => ($result),
                'section' => $section
            ]);
        }
    }
    
    public function delete(){

    }

    public function displinary($dispid){
        $empDisciplinary = new EmployeesDisciplinaryNotes();
        $empDisciplinaryData = $empDisciplinary::selectRaw("id, date, emp_id, customer_id, customer_name, estimate_id, category, note, issues, comments")
        ->where('id', '=', $dispid)
        ->get();

        $result = ['empDisciplinaryData' => $empDisciplinaryData];
        return response()->json([
            'status' => 200,
            'data' => ($result),
             
        ]);
    }
     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $extension = File::extension($name);
        $path = public_path('storage/' . $name);
        if (!File::exists($path)) {abort(404);}
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

}
