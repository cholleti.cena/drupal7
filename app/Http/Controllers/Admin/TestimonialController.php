<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use View;
use App\Models\Testimonial;
use Session;
use App\Models\Log;
use File;
use Image;
use Carbon\Carbon;
use DateTimeZone;
use App\Helpers\CommonHelper;
use DB;

class TestimonialController extends Controller
{
    private $helper;

    public function __construct(CommonHelper $helper)
    {
        $this->helper = $helper;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getPrivileges()
    {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,6,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,6,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,6,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,6,4);        
        return $privileges;
    }

    public function index()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();

        $testimonials = Testimonial::select(DB::raw('id,image,name,message,if(ifnull(is_active,1)=1,"Active","Inactive") as status'))
            ->get();

        return View::make('admin.testimonials.index', compact('testimonials'))         
        ->with('privileges',$privileges);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Add'] !='true')    
            return Redirect::back()->with('warning','Do not have permission to add!'); 

        return View::make('admin.testimonials.create')
        ->with('privileges',$privileges);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); 
        $this->validate($request, [
            'name'  => 'required|unique:testimonials']);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {
            return Redirect::route('testimonials.create')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');

            $testimonials = Testimonial::create($input);

            $testimonial_destinationDir = env('CONTENT_TESTIMONIAL_PATH');
             if($files=$request->file('logo')){
                $name=$files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $filename = $testimonials->id . '.' . $extension;
                $testimonials->image =  $testimonial_destinationDir.'/'. $filename;
                $testimonials->Update();
                $files->move($testimonial_destinationDir,$filename);
             }          

            $log = new Log();
            $log->module_id=6;
            $log->action='create';      
            $log->description='Testimonial ' . $testimonials->name . ' Created Successfully!';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get('admin_id'); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

        return Redirect::route('testimonials.index')->with('success',$log->description);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Edit'] !='true')
            return Redirect::to('/');

        $testimonials = Testimonial::find($id);
 
        return View::make('admin.testimonials.edit', compact('testimonials'))
        ->with('privileges',$privileges);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

         $this->validate($request, [
            'name'  => 'required']);
        $rules = array('');
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) 
        {
            return Redirect::route('testimonials.edit',$id)
                ->withInput()
                ->withErrors($validator)
                ->with('warning', 'There were validation errors');
        }
        else
        {   
            $input['modified_by'] = Session::get('admin_id');

            $testimonials = Testimonial::find($id);
            $testimonials->update($input);

            if(!empty($request->file('logo')))
            {
                $image = $testimonials->image;
                if(file_exists($image))
                {
                    unlink($image);
                }
            }
            $testimonial_destinationDir = env('CONTENT_TESTIMONIAL_PATH');
             if($files=$request->file('logo')){
                $name = $files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $filename = $id . '.' . $extension;
                $testimonials->image = $testimonial_destinationDir.'/'. $filename;
                $testimonials->Update();
                $files->move($testimonial_destinationDir,$filename);
             }

            $log = new Log();
            $log->module_id=6;
            $log->action='update';      
            $log->description='Testimonial ' . $testimonials->name . ' Updated Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
        return Redirect::route('testimonials.index')->with('success',$log->description);
        
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonials = Testimonial::find($id);       
       
        if (is_null($testimonials))
        {
         return Redirect::back()->with('warning','Testimonial Details Are Not Found!');
        }
        else
        {

           if(file_exists($testimonials->image))
            {
                unlink($testimonials->image);
            }
            
            $testimonials->delete();

            $log = new Log();
            $log->module_id=6;
            $log->action='delete';      
            $log->description='Testimonial '. $testimonials->name . ' Deleted Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
           return Redirect::back()->with('success',$log->description);
        }
    }
}
