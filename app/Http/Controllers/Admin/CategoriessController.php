<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use App\Models\Categories;
use Input;
use Session;
use App\Models\Log;
use File;
use Image;
use Carbon\Carbon;
use DateTimeZone;
use Hash;
use App\Helpers\CommonHelper;
use App\Services\Admin\CategoryServices;

class CategoriessController extends Controller
{
    private $helper;
    private $service;

    public function __construct(CommonHelper $helper,CategoryServices $service)
    {
        $this->helper = $helper;
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */   

    private function getPrivileges()
     {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,9,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,9,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,9,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,9,4);        
        return $privileges;
     }

    public function index()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        $categories = Categories::select(DB::raw('id,name as category_name,if(ifnull(is_active,1)=1,"Active","Inactive") as status'))
            ->orderBy('created_at','desc')
            ->paginate(5);
        
        return View::make('admin.categories.index', compact('categories'))         
        ->with('privileges',$privileges);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Add'] !='true')    
            return Redirect::back()->with('warning','Do not have permission to add!');    

        return View::make('admin.categories.create')
        ->with('privileges',$privileges);
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); 
        $this->validate($request, [
            'name'  => 'required|unique:categories']);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.categories.create')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');

            $category = Categories::create($input);       

            $log = new Log();
            $log->module_id=9;
            $log->action='create';      
            $log->description='Category ' . $category->category_name . ' Created Successfully!';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get('admin_id'); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

        return Redirect::route('categories.index')->with('success',$log->description);
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Edit'] !='true')            
            return Redirect::back()->with('warning','Do not have permission to add!'); 

        $category = Categories::find($id);
 
        return View::make('admin.categories.edit', compact('category'))
        ->with('privileges',$privileges);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $input = $request->all(); 

         $this->validate($request, [
            'name'  => 'required']);
        $rules = array('');
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) 
        {
            return Redirect::route('admin.categories.edit',$id)
                ->withInput()
                ->withErrors($validator)
                ->with('warning', 'There were validation errors');
        }
        else
        {   
            $input['modified_by'] = Session::get('admin_id');
            
            $category = Categories::find($id);
            $category->update($input);

            $log = new Log();
            $log->module_id=9;
            $log->action='update';      
            $log->description='Category ' . $category->name . ' Updated Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
        return Redirect::route('categories.index')->with('success',$log->description);
        
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Categories::find($id);       
       
        if (is_null($category))
        {
         return Redirect::back()->with('warning','Category Details Are Not Found!');
        }
        else
        {
        
            $category->delete();

            $log = new Log();
            $log->module_id=9;
            $log->action='delete';      
            $log->description='Category '. $category->name . ' Deleted Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
           return Redirect::back()->with('success',$log->description);
        }
    }

    public function ManageCategory(Request $request)
    {
        if($request->type == "add_category"):
              $res = $this->service->addCategory($request->all());
              return response()->json($res, 200);
        elseif($request->type == "update_category"):
              $res = $this->service->updateCategory($request->all());
              return response()->json($res, 200);
        elseif($request->type == "delete_category"):
              $res = $this->service->deleteCategory($request->all());
              return response()->json($res, 200);
        elseif($request->type == "category_details"):
              $res = $this->service->categoryDetails($request->all());
              return response()->json($res, 200);
        elseif($request->type == "category_list"):
              $res = $this->service->categoryList($request->all());
              return response()->json($res, 200);
        else:
              $res['status_code'] = 201;
              $res['message'] = 'Input not found';
              $res['data'] = false;
              return $res;
        endif;
    }
}
