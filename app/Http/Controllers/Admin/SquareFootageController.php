<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use App\Models\SquareFootage;
use Input;
use Session;
use App\Models\Log;
use File;
use Image;
use Carbon\Carbon;
use DateTimeZone;
use Hash;
use App\Helpers\CommonHelper;

class SquareFootageController extends Controller
{
    private $helper;
    private $service;

    public function __construct(CommonHelper $helper)
    {
        $this->helper = $helper;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */   

    private function getPrivileges()
     {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,18,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,18,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,18,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,18,4);        
        return $privileges;
     }

    public function index()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        $squarefootage = SquareFootage::select(DB::raw('id,min_range,max_range,minutes_per_room,cubic_feet_per_room,weight_per_room,people,trucks,price,if(ifnull(is_active,1)=1,"Active","Inactive") as status'))
            ->orderBy('created_at','desc')
            ->paginate(5);
        
        return View::make('admin.squarefootage.index', compact('squarefootage'))         
        ->with('privileges',$privileges);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Add'] !='true')    
            return Redirect::back()->with('warning','Do not have permission to add!');    

        return View::make('admin.squarefootage.create')
        ->with('privileges',$privileges);
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); 
        $this->validate($request, []);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.squarefootage.create')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');

            $category = SquareFootage::create($input);       

            $log = new Log();
            $log->module_id=18;
            $log->action='create';      
            $log->description='Fuel Charges - Min ' . $category->min_range . ' - Max ' .$category->max_range. ' Created Successfully!';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get('admin_id'); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

        return Redirect::route('squarefootage.index')->with('success',$log->description);
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Edit'] !='true')            
            return Redirect::back()->with('warning','Do not have permission to add!'); 

        $charge = SquareFootage::find($id);
 
        return View::make('admin.squarefootage.edit', compact('charge'))
        ->with('privileges',$privileges);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $input = $request->all(); 

         $this->validate($request, []);
        $rules = array('');
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) 
        {
            return Redirect::route('admin.squarefootage.edit',$id)
                ->withInput()
                ->withErrors($validator)
                ->with('warning', 'There were validation errors');
        }
        else
        {   
            $input['modified_by'] = Session::get('admin_id');
            
            $category = SquareFootage::find($id);
            $category->update($input);

            $log = new Log();
            $log->module_id=18;
            $log->action='update';      
            $log->description='Square Footage - Min ' . $category->min_range . ' - Max ' .$category->max_range. ' Updated Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
        return Redirect::route('squarefootage.index')->with('success',$log->description);
        
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = SquareFootage::find($id);       
       
        if (is_null($category))
        {
         return Redirect::back()->with('warning','Square Footage Details Are Not Found!');
        }
        else
        {
        
            $category->delete();

            $log = new Log();
            $log->module_id=18;
            $log->action='delete';      
            $log->description='Square Footage - Min ' . $category->min_range . ' - Max ' .$category->max_range. ' Deleted Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
           return Redirect::back()->with('success',$log->description);
        }
    }
}
