<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use View;
use App\Repositories\Admin\AdminRepository;
use App\Helpers\CommonHelper;

class DashboardController extends Controller
{
    private $helper;
    private $adminRepo;

    public function __construct(CommonHelper $helper,AdminRepository $adminRepo)
    {
        $this->helper = $helper;
        $this->adminRepo = $adminRepo;
    }

    public function index()
    {
    	if(!Session::has('admin_id') || Session::get('admin_id') == '')
        	return Redirect::to('/admin');

        $data = $this->adminRepo->manageDashBoard();

        return View::make('admin.dashboard.index')->with('data',$data);
    }
}
