<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use App\Models\HomeBanners;
use Input;
use Session;
use App\Models\Log;
use File;
use Image;
use Carbon\Carbon;
use DateTimeZone;
use Hash;
use App\Helpers\CommonHelper;

class BannersController extends Controller
{
    private $helper;

    public function __construct(CommonHelper $helper)
    {
        $this->helper = $helper;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */   

    private function getPrivileges()
    {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,2,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,2,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,2,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,2,4);        
        return $privileges;
    }

    public function index()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();

        $banners = HomeBanners::select(DB::raw('id,header_title,hear_image,if(ifnull(is_active,1)=1,"Active","Inactive") as status'))
            ->get();

         return View::make('admin.banners.index', compact('banners'))         
        ->with('privileges',$privileges);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Add'] !='true')    
            return Redirect::back()->with('warning','Do not have permission to add!'); 

        return View::make('admin.banners.create')
        ->with('privileges',$privileges);
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); 
        $this->validate($request, [
            'header_title'  => 'required|unique:home_banners']);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {
            return Redirect::route('admin.banners.create')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            
            $input['created_by'] = Session::get('admin_id');

            $banner = HomeBanners::create($input);

            $hear_destinationDir = env('CONTENT_BANNER_PATH');
             if($files=$request->file('logo')){
                $name=$files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $filename = $banner->id . '.' . $extension;
                $banner->hear_image = $hear_destinationDir.'/'. $filename;
                $banner->Update();
                $files->move($hear_destinationDir,$filename);
             }          

            $log = new Log();
            $log->module_id=2;
            $log->action='create';      
            $log->description='Banner ' . $banner->header_title . ' Created Successfully!';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get('admin_id'); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

        return Redirect::route('banners.index')->with('success',$log->description);
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Edit'] !='true')
            return Redirect::back()->with('warning','Do not have permission to update!'); 

        $banner = HomeBanners::find($id);
 
        return View::make('admin.banners.edit', compact('banner'))
        ->with('privileges',$privileges);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $input = $request->all(); 

         $this->validate($request, [
            'header_title'  => 'required']);
        $rules = array('');
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) 
        {
            return Redirect::route('admin.banners.edit',$id)
                ->withInput()
                ->withErrors($validator)
                ->with('warning', 'There were validation errors');
        }
        else
        {   
            
            $input['modified_by'] = Session::get('admin_id');

            $banner = HomeBanners::find($id);
            $banner->update($input);

            if(!empty($request->file('logo')))
            {
                $hear_image = $banner->hear_image;
                if(file_exists($hear_image))
                {
                    unlink($hear_image);
                }
            }
            
            $banner_destinationDir = env('CONTENT_BANNER_PATH');
             if($files=$request->file('logo')){
                $name = $files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $filename = $id . '.' . $extension;
                $banner->hear_image = $banner_destinationDir.'/'. $filename;
                $banner->Update();
                $files->move($banner_destinationDir,$filename);
             }

            $log = new Log();
            $log->module_id=2;
            $log->action='update';      
            $log->description='Banner ' . $banner->header_title . ' Updated Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

        return Redirect::route('banners.index')->with('success',$log->description);
        
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = HomeBanners::find($id);       
       
        if (is_null($banner))
        {
         return Redirect::back()->with('warning','Banner Details Are Not Found!');
        }
        else
        {
            $hear_image = $banner->hear_image;
            if(file_exists($hear_image))
            {
                unlink($hear_image);
            }

            $banner->delete();

            $log = new Log();
            $log->module_id=2;
            $log->action='delete';      
            $log->description='Banner '. $banner->header_title . ' Deleted Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

           return Redirect::back()->with('success',$log->description);
        }
    }
}
