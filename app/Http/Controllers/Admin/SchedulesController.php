<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use Input;
use Session;
use App\Models\Log;
use File;
use Image;
use Carbon\Carbon;
use DateTimeZone;
use App\Helpers\CommonHelper;
use App\Repositories\Leads\LeadsRepository;


class SchedulesController extends Controller
{
    private $helper;

    public function __construct(CommonHelper $helper,LeadsRepository $leadRepo)
    {
        $this->helper = $helper;
        $this->leadRepo = $leadRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */   

    private function getPrivileges()
     {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,8,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,8,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,8,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,8,4);        
        return $privileges;
     }

    public function index()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();

        $data['from_date'] = date('Y-m-d');

         $leadRepo = $this->leadRepo->schedulesList($data);

         return View::make('admin.schedules.index')
        ->with('leadRepo',$leadRepo)        
        ->with('privileges',$privileges);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
