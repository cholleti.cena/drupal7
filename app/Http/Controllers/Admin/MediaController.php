<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Models\MediaContent;
use View;
use DB;
use Session;
use App\Models\Log;
use File;
use Image;
use Carbon\Carbon;
use DateTimeZone;
use App\Helpers\CommonHelper;

class MediaController extends Controller
{
    private $helper;

    public function __construct(CommonHelper $helper)
    {
        $this->helper = $helper;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getPrivileges()
    {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,9,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,9,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,9,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,9,4);        
        return $privileges;
    }

    public function index(Request $request)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $perPage = $request->per_page ?? 10;

        $privileges = $this->getPrivileges();

        $medias = MediaContent::select(DB::raw('id,media_name,media_path,media_extension'));
        if($request->search):
            $medias = $medias->where('media_name', 'like', '%'. $request->search . '%');
        endif;
        $medias = $medias->paginate($perPage);
        $medias = $medias->appends(['search' => $request->search, 'per_page' => $request->per_page]);


        return View::make('admin.medias.index', compact('medias'))         
        ->with('privileges',$privileges);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Add'] !='true')    
            return Redirect::back()->with('warning','Do not have permission to add!'); 

        return View::make('admin.medias.create')
        ->with('privileges',$privileges);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); 
        
        $this->validate($request, [
            'media_name'  => 'required|unique:media_content']);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {
            return Redirect::route('admin.medias.create')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            
            $input['created_by'] = Session::get('admin_id');

            $media = MediaContent::create($input);
            
            $hear_destinationDir = env('CONTENT_MEDIA_PATH');
             if($files=$request->file('media_path')){
                $name=$files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $filename = $media->id . '.' . $extension;
                $media->media_path = $hear_destinationDir.'/'. $filename;
                $media->media_extension = $extension;
                $media->Update();
                $files->move($hear_destinationDir,$filename);
            } 

            $log = new Log();
            $log->module_id=9;
            $log->action='create';      
            $log->description='Media ' . $media->media_name . ' Created Successfully!';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get('admin_id'); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

        return Redirect::route('medias.index')->with('success',$log->description);
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = MediaContent::find($id);       
       
        if (is_null($media))
        {
         return Redirect::back()->with('warning','Media Details Are Not Found!');
        }
        else
        {
            $path = env('CONTENT_BANNER_PATH');
            $myfile = env('SITE_URL').'/'.$media->media_path;            
            if (File::exists($myfile))
            {
                File::delete($myfile);
            }

            $media->delete();

            $log = new Log();
            $log->module_id=9;
            $log->action='delete';      
            $log->description='Media '. $media->media_name . ' Deleted Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

           return Redirect::back()->with('success',$log->description);
        }
    }
}
