<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use App\Models\Tasks;
use Input;
use Session;
use App\Models\Log;
use File;
use Image;
use Carbon\Carbon;
use DateTimeZone;
use App\Helpers\CommonHelper;
use App\Services\Admin\TasksServices;
use App\Repositories\Tasks\TasksRepository;
use App\Repositories\Users\AuthRepository;

class TasksController extends Controller
{
    private $helper;
    private $service;

    public function __construct(CommonHelper $helper,TasksServices $service,Tasks $tasks,   TasksRepository $taskRepo,AuthRepository $authRepo)
    {
        $this->helper = $helper;
        $this->service = $service;
        $this->tasks = $tasks;
        $this->taskRepo = $taskRepo;
        $this->authRepo = $authRepo;
    }

    public function __invoke()
    {
         
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */   

    private function getPrivileges()
     {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,8,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,8,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,8,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,8,4);        
        return $privileges;
     }

    public function index($fieldName = "", $value = "")
    {
         
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');
       
            $privileges = $this->getPrivileges();
        if($fieldName && $fieldName != "" && $value > 0 && $value < 10 ){
            $tasks = $this->tasks::selectRaw(" tasks.id as id,
            tasks.task_id as task_id,task_types.name as tasktypeName, tasks.assigned_user_by as assigned_user_by,tasks.assigned_user_to as assigned_user_to,
            tasks.client as client,tasks.contact_person as contact_person,tasks.priority as priority,tasks.when_date as when_date, task_status.name as taskstatusName ")
            ->join('task_types','task_types.id','=','tasks.task_type')
            ->join('task_status','task_status.id','=','tasks.status')
            ->orderBy('tasks.id','DESC')
            ->where('tasks.'.$fieldName, '=', $value)
            ->get();
            return response()->json([
                'status' => 200,
                'data' => $tasks,
            ]); 
        } else {
            $tasks = $this->tasks::selectRaw(" tasks.id as id,
            tasks.task_id as task_id,task_types.name as tasktypeName,tasks.assigned_user_by as assigned_user_by,tasks.assigned_user_to as assigned_user_to,
            tasks.client as client,tasks.contact_person as contact_person,tasks.priority as priority,tasks.when_date as when_date, task_status.name as taskstatusName ")
            ->join('task_types','task_types.id','=','tasks.task_type')
            ->join('task_status','task_status.id','=','tasks.status')
            ->orderBy('tasks.id','DESC')
            ->get();

            $tasktypes = DB::table('task_types')->get();  
            $taskStatus = DB::table('task_status')->get();  
            if($value == "all"){
                return response()->json([
                    'status' => 200,
                    'data' => $tasks,
                ]); 
            } else {
                return View::make('admin.tasks.index', compact('tasks'), compact('tasktypes'))         
                ->with('privileges',$privileges)
                ->with('taskStatus',$taskStatus);
            }
        }
    }
    public function status($value = "")
    {
        
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');
       
            $privileges = $this->getPrivileges();
        if($value > 0 && $value < 10 ){
            $tasks = $this->tasks::selectRaw(" tasks.id as id,
            tasks.task_id as task_id,task_types.name as tasktypeName, tasks.assigned_user_by as assigned_user_by,tasks.assigned_user_to as assigned_user_to,
            tasks.client as client,tasks.contact_person as contact_person,tasks.priority as priority,tasks.when_date as when_date, task_status.name as taskstatusName ")
            ->join('task_types','task_types.id','=','tasks.task_type')
            ->join('task_status','task_status.id','=','tasks.status')
            ->orderBy('tasks.id','DESC')
            ->where('tasks.status', '=', $value)
            ->get();
            return response()->json([
                'status' => 200,
                'data' => $tasks,
            ]); 
        } else {
            $tasks = $this->tasks::selectRaw(" tasks.id as id,
            tasks.task_id as task_id,task_types.name as tasktypeName,tasks.assigned_user_by as assigned_user_by,tasks.assigned_user_to as assigned_user_to,
            tasks.client as client,tasks.contact_person as contact_person,tasks.priority as priority,tasks.when_date as when_date, task_status.name as taskstatusName ")
            ->join('task_types','task_types.id','=','tasks.task_type')
            ->join('task_status','task_status.id','=','tasks.status')
            ->orderBy('tasks.id','DESC')
            ->get();

            $tasktypes = DB::table('task_types')->get();  
            $taskStatus = DB::table('task_status')->get();  
            if($value == "all"){
                return response()->json([
                    'status' => 200,
                    'data' => $tasks,
                ]); 
            } else {
                return View::make('admin.tasks.index', compact('tasks'), compact('tasktypes'))         
                ->with('privileges',$privileges)
                ->with('taskStatus',$taskStatus);
            }
        }
    }
    
    /**
     * Show the form for creating a new resource. and update the same
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
       if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');
             
        $task = new Tasks;
        $whenDate = explode("/", $req->when_date);
        $task->task_id = time() ;
        $task->assigned_user_to = $req->assigned_user_to;
        $task->when_date = $whenDate[2]."-".$whenDate[0]."-".$whenDate[1];
        $task->task_type = $req->task_type;
        $task->assigned_user_by = $req->assigned_user_by;
        $task->priority = $req->priority;
        $task->status = $req->status;
        $task->send_emails = 0;
        $task->active_yn = 1;
        $task->comments = $req->comments;
        $message = 'created';
        if($req->record_id && $req->record_id != ''){ 
            $updateTask = array();
            $updateTask["task_id"] = time() ;
            $updateTask["assigned_user_to"] = $req->assigned_user_to;
            $updateTask["when_date"] = $whenDate[2]."-".$whenDate[0]."-".$whenDate[1];
            $updateTask["task_type"] = $req->task_type;
            $updateTask["assigned_user_by"] = $req->assigned_user_by;
            $updateTask["priority"] = $req->priority;
            $updateTask["status"] = $req->status;
            $updateTask["send_emails"] = 0;
            $updateTask["active_yn"] = 1;
            $updateTask["comments"] = $req->comments;

            $faq = $task->find($req->record_id);
            $result  = $faq->update($updateTask);
            $message = 'updated';
        } else {
            $result = $task->save();
        }
        if($result){
            return Redirect::to('/admin/tasks')->with('success','Task '.$message.' successfully');
        } else {
            return ["result"=>'Task not screated'];
        }
    }

    /** search records
     * @fieldName column name
     * $value  searching column value
     */
    public function search($fieldName, $value)
    {
         
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

            $privileges = $this->getPrivileges();
        $tasks = $this->tasks::selectRaw(" * ")
        ->where($fieldName, '=', $value)
        ->get();
        
        return response()->json([
            'status' => 200,
            'data' => $tasks,
        ]);  
    }
    /** delete records
     * @fieldName column name
     * $value  searching column value
     */
    public function delete($value)
    {
         
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

            $task = new Tasks;
            $updateTask = array();
            $updateTask["status"] = 0;
            $faq = $task->find($value);
            $result  = $faq->update($updateTask);
            $message = 'Deleted successfully';
        
            return response()->json([
                'status' => 200,
                'data' => $message ,
            ]); 
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
}
