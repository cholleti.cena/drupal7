<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use Input;
use Session;
use App\Log;
use Carbon\Carbon;
use App\Models\Defaults;
use DB;
use DateTimeZone;
class ConfigurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         
        $defaults = Defaults::select(DB::raw('defaults.*')) 
                ->where('defaults.id','=',1)          
                ->get();
      
        if (is_null($defaults))
        {
            return Redirect::route('admin.configuration.index')
                ->with('message','There were validation errors');
        }
        else 
         return View::make('admin.configuration.index')
                ->with('defaults',$defaults[0]);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all(); 
        $this->validate($request, [
            'version'=>'required','log_max_days'=>'required|numeric']);

        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {
            return Redirect::route('admin.configuration.index')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {
            $input['created_by'] = Session::get('admin_id');
            $input['modified_by'] = Session::get('admin_id');

            $defaults = Defaults::find($id);
            $defaults->update($input);

            if(!empty($request->file('logou')))
            {
                $logo = $defaults->logo;
                if(file_exists($logo))
                {
                    unlink($logo);
                }
            }

            if(!empty($request->file('aboutusimage')))
            {
                $about_us_image = $defaults->about_us_image;
                if(file_exists($about_us_image))
                {
                    unlink($about_us_image);
                }
            }

            if(!empty($request->file('whoweareimage')))
            {
                $who_we_are_image = $defaults->who_we_are_image;
                if(file_exists($who_we_are_image))
                {
                    unlink($who_we_are_image);
                }
            }

            if(!empty($request->file('visionimage')))
            {
                $vision_image = $defaults->vision_image;
                if(file_exists($vision_image))
                {
                    unlink($vision_image);
                }
            }

            if(!empty($request->file('missionimage')))
            {
                $mission_image = $defaults->mission_image;
                if(file_exists($mission_image))
                {
                    unlink($mission_image);
                }
            }

            if(!empty($request->file('objectiveimage')))
            {
                $objective_image = $defaults->objective_image;
                if(file_exists($objective_image))
                {
                    unlink($objective_image);
                }
            }
            
            if(!empty($request->file('ourvalueimage')))
            {
                $our_value_image = $defaults->our_value_image;
                if(file_exists($our_value_image))
                {
                    unlink($our_value_image);
                }
            }
            

             $banner_destinationDir = env('CONTENT_MEDIA_PATH');
             if($files=$request->file('logou'))
             {
                $name = $files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $filename = 'logo_'.$id . '.' . $extension;
                $defaults->logo = $banner_destinationDir.'/'. $filename;
                $defaults->Update();
                $files->move($banner_destinationDir,$filename);
             }

             if($files=$request->file('aboutusimage'))
             {
                $name = $files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $filename = 'about_us_'.$id . '.' . $extension;
                $defaults->about_us_image = $banner_destinationDir.'/'. $filename;
                $defaults->Update();
                $files->move($banner_destinationDir,$filename);
             }

             if($files=$request->file('whoweareimage'))
             {
                $name = $files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $filename = 'who_we_are_'.$id . '.' . $extension;
                $defaults->who_we_are_image = $banner_destinationDir.'/'. $filename;
                $defaults->Update();
                $files->move($banner_destinationDir,$filename);
             }

             if($files=$request->file('visionimage'))
             {
                $name = $files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $filename = 'vision_'.$id . '.' . $extension;
                $defaults->vision_image = $banner_destinationDir.'/'. $filename;
                $defaults->Update();
                $files->move($banner_destinationDir,$filename);
             }

             if($files=$request->file('missionimage'))
             {
                $name = $files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $filename = 'mission_'.$id . '.' . $extension;
                $defaults->mission_image = $banner_destinationDir.'/'. $filename;
                $defaults->Update();
                $files->move($banner_destinationDir,$filename);
             }

             if($files=$request->file('objectiveimage'))
             {
                $name = $files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $filename = 'objective_'.$id . '.' . $extension;
                $defaults->objective_image = $banner_destinationDir.'/'. $filename;
                $defaults->Update();
                $files->move($banner_destinationDir,$filename);
             }

             if($files=$request->file('ourvalueimage'))
             {
                $name = $files->getClientOriginalName();
                $extension = $files->getClientOriginalExtension();
                $filename = 'our_value_'.$id . '.' . $extension;
                $defaults->our_value_image = $banner_destinationDir.'/'. $filename;
                $defaults->Update();
                $files->move($banner_destinationDir,$filename);
             }
            
            return Redirect::back()->with('success','Configuration details are updated successfully!');           
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
