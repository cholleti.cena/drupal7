<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use App\Models\Leads;
use Input;
use Session;
use App\Models\Log;
use File;
use Image;
use Carbon\Carbon;
use DateTimeZone;
use App\Helpers\CommonHelper;
use App\Services\Admin\LeadServices;
use App\Repositories\Leads\LeadsRepository;
use App\Repositories\Users\AuthRepository;

class LeadsController extends Controller
{
    private $helper;
    private $service;

    public function __construct(CommonHelper $helper,LeadServices $service,Leads $leads,LeadsRepository $leadRepo,AuthRepository $authRepo)
    {
        $this->helper = $helper;
        $this->service = $service;
        $this->leads = $leads;
        $this->leadRepo = $leadRepo;
        $this->authRepo = $authRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */   

    private function getPrivileges()
     {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,7,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,7,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,7,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,7,4);        
        return $privileges;
     }

    public function index()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();

        $leads = $this->leads::selectRaw("leads.user_id,leads.id as lead_id,leads.approximate_move_date,CONCAT(users.first_name,' ',users.last_name) as name,users.email,phone_types.name as phone_type,users.phone,'' as alter_phone,referrals.name as referred_by,move_status.name as lead_status,leads.created_at")
          ->join('users','users.id','=','leads.user_id')
          ->leftjoin('phone_types','phone_types.id','=','users.phone_type')
          ->leftjoin('referrals','referrals.id','=','users.referred_by')
          ->leftjoin('move_status','move_status.id','=','leads.lead_status')
          ->where('leads.lead_status','=',1)
          ->Groupby('leads.id','leads.user_id')
          ->orderBy('leads.created_at','desc')
          ->paginate(5);

         return View::make('admin.leads.index', compact('leads'))         
        ->with('privileges',$privileges);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Add'] !='true')    
            return Redirect::back()->with('warning','Do not have permission to add!');  

        return View::make('admin.leads.create')
        ->with('privileges',$privileges);
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Edit'] !='true')
            return Redirect::back()->with('warning','Do not have permission to update!');  

        $data['lead_id'] = $id;
        $lead = $this->leadRepo->leadDetails($data);

        $mdata['free_text'] = "";
        $mdata['metaData'] = array("servicetypes","property","movesizes","category","materials","packing_requirements","storage_requirements","packing_services","packing_materials",
        "packing_quotes");
        $metadata = $this->authRepo->getMetaDataList($mdata);

        $floors = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
 
        return View::make('admin.leads.edit', compact('lead'))
        ->with('metadata',$metadata)
        ->with('floors',$floors)
        ->with('privileges',$privileges);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all(); 
        
       // print_r($input);exit;

        $this->validate($request, []);
        $rules = array('');
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) 
        {
            return Redirect::route('faq.edit',$id)
                ->withInput()
                ->withErrors($validator)
                ->with('warning', 'There were validation errors');
        }
        else
        {   
            $input['modified_by'] = Session::get('admin_id');

            if(!empty($input['leadtype'])){
                $lead = $this->leadRepo->Updateleadstatus($input);
            }else{
                $lead = $this->leadRepo->leadUpdate($input);
            }

            // return $input;
            // 

            $log = new Log();
            $log->module_id=7;
            $log->action='delete';      
            $log->description='Lead update Updated Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

            return Redirect::route('leads.index')->with('success',$log->description);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq = Faq::find($id);       
       
        if (is_null($faq))
        {
         return Redirect::back()->with('warning','Faq Details Are Not Found!');
        }
        else
        {
            $faq->delete();

            $log = new Log();
            $log->module_id=5;
            $log->action='delete';      
            $log->description='Faq '. $faq->question . ' Deleted Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
            
           return Redirect::back()->with('success',$log->description);
        }
    }

    public function ManageLeads(Request $request)
    {
        if($request->type == "lead_list"):
              $res = $this->service->leadList($request->all());
              return response()->json($res, 200);
        elseif($request->type == "lead_details"):
              $res = $this->service->leadDetails($request->all());
              return response()->json($res, 200);
        elseif($request->type == "material_details"):
              $res = $this->service->materialDetails($request->all());
              return response()->json($res, 200);
        elseif($request->type == "lead_update"):
              $res = $this->service->leadUpdate($request->all());
              return response()->json($res, 200);
        elseif($request->type == "schedules_list"):
              $res = $this->service->schedulesList($request->all());
              return response()->json($res, 200);
        else:
              $res['status_code'] = 201;
              $res['message'] = 'Input not found';
              $res['data'] = false;
              return $res;
        endif;
    }

    public function MaterialDetails(Request $request)
    {
        $res = $this->service->materialDetails($request->all());
        return response()->json($res, 200);
    }
}
