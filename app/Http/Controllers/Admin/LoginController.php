<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Redirect;
use Input;
use DB;
use Session;
use DateTimeZone;
use Mail;
use Hash;
use App\Models\AdminUsers;
use Carbon\Carbon;
use App\Helpers\CommonHelper;
use App\googleauthenticator;

class LoginController extends Controller
{
    private $helper;

    public function __construct(CommonHelper $helper)
    {
        $this->helper = $helper;
    }

    public function index()
    {
       //Session::flush();
	   if(!Session::has('admin_id') || Session::get('admin_id') == '')
	   {
		    return View::make('admin.login.login');
	   }
	   
	   return Redirect::to('/admin/dashboard');
    }

    public function validateuser(Request $request)
    {
        $input = $request->all();
        $this->validate($request, [ ]);
        $rules = array('');
        
        $validator = \Validator::make($input, $rules);

        if($validator->fails())
        {
          return View::make('admin.login.login')
            ->withInput()
            ->withErrors($validator)
            ->with('errors', 'There were validation errors.');
        }

        $email_validate = AdminUsers::where('email','=',$input['email'])
        ->first();
        if(is_null($email_validate))
        {
            return \Redirect::back()->withErrors( 'Please Enter Correct Email')
                ->withInput();
        }

        $login_details = AdminUsers::where('email','=', $input['email'])
        //->where('password','=',!Hash::check($input['password'], $email_validate->password))
        ->where('password','=',$input['password'])
        ->first();
        if(is_null($login_details))
        {
            return \Redirect::back()->withErrors( 'Please Enter Correct Password')
            ->withInput();
        }

        if($login_details->status == 2)
        {
            return \Redirect::back()->withErrors( 'In active user')
            ->withInput();
        }

        $reset_hash=$this->helper->generateRandomHexString(50);
        $login_details->reset_hash = $reset_hash;
        $login_details->Update();

        // Session::put("adminid",$login_details->id);
        // return View::make('admin.login.auth');

        Session::put("admin_id",$login_details->id);
        Session::put("role_id",$login_details->role_id);
        Session::put("name",$login_details->name);
        Session::put("admin_email",$login_details->email);
        Session::put("status",$login_details->status);
        Session::put("mobileno",$login_details->mobileno);
        return Redirect::to('/admin/dashboard');
    }


    public function auth(Request $request)
    {   
        if(!Session::has('adminid') || Session::get('adminid') == '')
        {
            return View::make('admin.login.login');
        }
        $user_id = Session::get('adminid');
        $login_details = AdminUsers::find($user_id);
        $secret_key = $login_details->secret;
        $currentcode = $request['secretkey'];
        $ga = new googleauthenticator();
        if($ga->verifyCode($secret_key, $currentcode, 2)){
            Session::put("admin_id",$login_details->id);
            Session::put("role_id",$login_details->role_id);
            Session::put("name",$login_details->name);
            Session::put("admin_email",$login_details->email);
            Session::put("status",$login_details->status);
            Session::put("mobileno",$login_details->mobileno);
            return Redirect::to('/admin/dashboard');
        }
        else
        {
           return redirect('admin/auth')
           ->withErrors('Login Failed')
           ->withInput();  
        }
    }

    public function logout(Request $request) 
    {
          Session::forget('admin_id');
          Session::forget('role_id');
          Session::forget('name');
          Session::forget('admin_email');
          Session::forget('status');
          Session::forget('mobileno');
          return redirect('/admin');
    }

    public function forgot(Request $request)
    {
        $input = Input::all();
       
         $this->validate($request, [
            'email'  => 'required|email'
        ]);
        $rules = array('');
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) 
        {
            return View::make('login.forgot')
            ->withInput()
            ->withErrors($validator)
            ->with('errors', 'There were validation errors.');
        }
        $login_details = DB::table('person')
            ->where('person.email','=', Input::get("email")) 
            ->get();
        $login = DB::table('person')
            ->where('person.email','=', Input::get("email"))           
            ->count();
        if($login == 1)
        {
            $frommail = env('MAIL_USERNAME'); 
            $data = array('hash' => $login_details[0]->ref ,'name'=>$login_details[0]->firstnames,'email'=> $login_details[0]->email,'fromemail'=>$frommail);          
            Mail::send('layouts.forgotpassword', $data, function($m) use ($data) {
                $m->from('info@advisrtech.com', 'FoodAdvisr');
                $m->to($data['email'])->subject('Forgot Password!');
            });
           return redirect('/login')->with('success','We Have Mailed Your Reset Password Link!');
        }
        else{
             return \Redirect::back()->withErrors( 'Invalid Email')
                ->withInput();  
        }   
    }

    public function resetpassword(Request $request)
    {
        $input = Input::all();
        $this->validate($request, [
            'newpassword'  => 'required','cfnewpassword' => 'required'
        ]);
        $rules = array('');
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) 
        {
            return View::make('login.resetpassword')
            ->withInput()
            ->withErrors($validator)
            ->with('errors', 'There were validation errors.');
        }
        else
        {
            if($input['newpassword'] == $input['cfnewpassword'])
            {
                $email = $input['email'];
                $hash = $input['hash'];
                $password = Hash::make($input['newpassword']);
                $resetpassword = "UPDATE person SET password ='".$password."' WHERE email ='".$email."' and ref='".$hash."'";
                DB::select(DB::raw($resetpassword));
              return redirect('/login')->with('success','Updated Password!');  
            }
            else
            {
                return \Redirect::back()->withErrors( 'Password And Confirm Password Are Not Match')
                ->withInput();
            }
        }

    }
}
