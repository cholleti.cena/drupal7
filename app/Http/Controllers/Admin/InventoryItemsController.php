<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use App\Models\Items;
use Input;
use Session;
use App\Models\Log;
use File;
use Image;
use Carbon\Carbon;
use DateTimeZone;
use Hash;
use App\Helpers\CommonHelper;
use App\Repositories\Users\AuthRepository;

class InventoryItemsController extends Controller
{
    private $helper;
    private $service;

    public function __construct(CommonHelper $helper,AuthRepository $authRepo)
    {
        $this->helper = $helper;
        $this->authRepo = $authRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */   

    private function getPrivileges()
     {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,12,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,12,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,12,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,12,4);        
        return $privileges;
     }

    public function index()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        $items = Items::select(DB::raw('id,name,price,weight,cubic_foot,if(ifnull(is_active,1)=1,"Active","Inactive") as status'))
            ->orderBy('created_at','desc')
            ->paginate(5);
        
        return View::make('admin.inventoryitems.index', compact('items'))         
        ->with('privileges',$privileges);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Add'] !='true')    
            return Redirect::back()->with('warning','Do not have permission to add!');    

        $mdata['free_text'] = "";
        $mdata['metaData'] = array("category");
        $metadata = $this->authRepo->getMetaDataList($mdata);

        return View::make('admin.inventoryitems.create')
        ->with('metadata',$metadata)
        ->with('privileges',$privileges);
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); 
        $this->validate($request, []);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.inventoryitems.create')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');

            if(isset($input['taxable']))
            {
                if($input['taxable'] === "on")
                {
                    $input['taxable'] = 1;
                }
            }

            if(isset($input['over_size']))
            {
                if($input['over_size'] === "on")
                {
                    $input['over_size'] = 1;
                }
            }

            $input['description'] = $input['name'];

            $category = Items::create($input);       

            $log = new Log();
            $log->module_id=12;
            $log->action='create';      
            $log->description='Inventory Item ' . $category->name . ' Created Successfully!';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get('admin_id'); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

        return Redirect::route('inventoryitems.index')->with('success',$log->description);
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Edit'] !='true')            
            return Redirect::back()->with('warning','Do not have permission to add!'); 

        $item = Items::find($id);

        $mdata['free_text'] = "";
        $mdata['metaData'] = array("category");
        $metadata = $this->authRepo->getMetaDataList($mdata);
 
        return View::make('admin.inventoryitems.edit', compact('item'))
        ->with('metadata',$metadata)
        ->with('privileges',$privileges);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all(); 

         $this->validate($request, [
            'name'  => 'required']);
        $rules = array('');
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) 
        {
            return Redirect::route('admin.rooms.edit',$id)
                ->withInput()
                ->withErrors($validator)
                ->with('warning', 'There were validation errors');
        }
        else
        {   
            $input['modified_by'] = Session::get('admin_id');

            if(isset($input['taxable']))
            {
                if($input['taxable'] === "on")
                {
                    $input['taxable'] = 1;
                }
            }

            if(isset($input['over_size']))
            {
                if($input['over_size'] === "on")
                {
                    $input['over_size'] = 1;
                }
            }

            $input['description'] = $input['name'];
            
            $category = Items::find($id);
            $category->update($input);

            $log = new Log();
            $log->module_id=12;
            $log->action='update';      
            $log->description='Inventory Item ' . $category->name . ' Updated Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
        return Redirect::route('inventoryitems.index')->with('success',$log->description);
        
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Items::find($id);       
       
        if (is_null($category))
        {
         return Redirect::back()->with('warning','Room Details Are Not Found!');
        }
        else
        {
        
            $category->delete();

            $log = new Log();
            $log->module_id=12;
            $log->action='delete';      
            $log->description='Inventory Item '. $category->name . ' Deleted Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
           return Redirect::back()->with('success',$log->description);
        }
    }
}
