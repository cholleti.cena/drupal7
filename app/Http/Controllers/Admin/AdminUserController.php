<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use View;
use DB;
use App\Models\AdminUsers;
use App\Models\Role;
use App\googleauthenticator;
use Input;
use Session;
use App\Models\Log;
use File;
use Image;
use Carbon\Carbon;
use DateTimeZone;
use Hash;
use App\Helpers\CommonHelper;
use App\Services\Admin\AdminServices;

class AdminUserController extends Controller
{
	private $helper;
    private $service;

    public function __construct(CommonHelper $helper,AdminServices $service)
    {
        $this->helper = $helper;
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */   

    private function getPrivileges()
    {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,1,1);//role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,1,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,1,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,1,4);        
        return $privileges;
    }

    public function index()
    {
       if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();

        $users = AdminUsers::join('role', 'role.id', '=', 'admin_users.role_id')
                ->select(DB::raw('admin_users.*,role.name as role_name,if(ifnull(status,1)=1,"Active","Inactive") as status'))
                ->orderBy('admin_users.created_at','desc')
                ->paginate(5);
        return View::make('admin.user.index', compact('users'))         
        ->with('privileges',$privileges);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       if ( !Session::has('admin_id') || Session::get('admin_id') == '' )
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Add'] !='true')    
            return Redirect::back()->with('warning','Do not have permission to add!');    

        $role = Role::all()->pluck('name','id');  
        $ga = new googleauthenticator();
        $newSecret=$ga->createSecret();   
        
        return View::make('admin.user.create')
        ->with('role',$role)   
        ->with('newSecret',$newSecret)       
        ->with('privileges',$privileges);
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); 
         
        $this->validate($request, [
            'email'  => 'required|unique:admin_users','password'  => 'required','mobileno'=> 'required|unique:admin_users|min:10|max:15|regex:/^(?=.*[0-9])[- +()0-9]+$/']);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {
            return Redirect::route('user.create')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');
            $input['password'] =  Hash::make($input['password']);

            $admin = AdminUsers::create($input);

            $log = new Log();
            $log->module_id=1;
            $log->action='create';      
            $log->description='User ' . $admin->name . ' is created';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get('admin_id'); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

        return Redirect::route('user.index')->with('success','User Created Successfully!');
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( !Session::has('admin_id') || Session::get('admin_id') == '' )
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Edit'] !='true')
            return Redirect::back()->with('warning','Do not have permission to update!');  

        $user = AdminUsers::find($id);
        $role = Role::all()->pluck('name','id');       
        $ga = new googleauthenticator();
        $newSecret=$ga->createSecret();  
        return View::make('admin.user.edit', compact('user'))
        ->with('role',$role)
        ->with('newSecret',$newSecret)
        ->with('privileges',$privileges);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $input = $request->all();  

         $this->validate($request, [
            'email'  => 'required|email']);
        $rules = array('');
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) 
        {
            return Redirect::route('admin.user.edit',$id)
                ->withInput()
                ->withErrors($validator)
                ->with('warning', 'There were validation errors');
        }
        else
        {   
            
            $input['modified_by'] = Session::get('admin_id');

            $admin = AdminUsers::find($id);
            $admin->update($input);

            $log = new Log();
            $log->module_id=1;
            $log->action='update';      
            $log->description='User ' . $admin->name . ' is updated';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
            return Redirect::route('user.index')->with('success','User Updated Successfully!');        
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = AdminUsers::find($id);
        if (is_null($user))
        {
         return Redirect::back()->with('warning','User Details Are Not Found!');
        }
        else
        {
            $user->delete();
            $log = new Log();
            $log->module_id=1;
            $log->action='delete';      
            $log->description='User '. $user->name . ' is Deleted';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get('admin_id'); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
           return Redirect::back()->with('warning','User Deleted Successfully!');
        }
    }

    public function AdminLogin(Request $request)
    {
        $res = $this->service->adminLogin($request->all());
        return response()->json($res, 200);
    }

    public function ManageDashBoard(Request $request)
    {
        $res = $this->service->manageDashBoard();
        return response()->json($res, 200);
    }
}
