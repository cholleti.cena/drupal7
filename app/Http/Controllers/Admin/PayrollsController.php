<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use App\Models\PayRollClasses;
use App\Models\PayRolls;
use App\Models\VisiblePayrollFields;
use App\Models\PayRollConfigurations;
use App\Models\PayRollMiles;
use Input;
use Session;
use App\Models\Log;
use File;
use Image;
use Carbon\Carbon;
use DateTimeZone;
use Hash;
use App\Helpers\CommonHelper;

class PayrollsController extends Controller
{
    private $helper;
    private $service;

    public function __construct(CommonHelper $helper)
    {
        $this->helper = $helper;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */   

    private function getPrivileges()
     {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,23,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,23,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,23,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,23,4);        
        return $privileges;
     }

    public function index()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        $payrolls = PayRolls::select(DB::raw('pay_rolls.id,pay_roll_classes.name as pay_roll_class,pay_rolls.name as pay_roll_name,if(ifnull(pay_rolls.is_active,1)=1,"Active","Inactive") as status'))
        ->join('pay_roll_classes','pay_roll_classes.id','=','pay_rolls.pay_roll_class_id')
        ->orderBy('pay_rolls.created_at','desc')
        ->paginate(5);

        $visible_payroll_fields = VisiblePayrollFields::get();
        $payrollmiles = PayRollMiles::get();

        $payrollcofig = PayRollConfigurations::first();

        return View::make('admin.payrolls.index', compact('payrolls'))
        ->with('visible_payroll_fields',$visible_payroll_fields)      
        ->with('payrollcofig',$payrollcofig)
        ->with('payrollmiles',$payrollmiles)       
        ->with('privileges',$privileges);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Add'] !='true')    
            return Redirect::back()->with('warning','Do not have permission to add!');   


        $categories = PayRollClasses::get(); 

        return View::make('admin.payrolls.create')
        ->with('categories',$categories)
        ->with('privileges',$privileges);
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); 
        $this->validate($request, []);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.payrolls.create')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');

            $category = PayRolls::create($input);       

            $log = new Log();
            $log->module_id=23;
            $log->action='create';      
            $log->description='PayRolls ' . $category->name .' Created Successfully!';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get('admin_id'); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

        return Redirect::route('payrolls.index')->with('success',$log->description);
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Edit'] !='true')            
            return Redirect::back()->with('warning','Do not have permission to add!'); 

        $charge = PayRolls::find($id);

        $categories = PayRollClasses::get(); 
 
        return View::make('admin.payrolls.edit', compact('charge'))
        ->with('categories',$categories)
        ->with('privileges',$privileges);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $input = $request->all(); 

         $this->validate($request, []);
        $rules = array('');
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) 
        {
            return Redirect::route('admin.payrolls.edit',$id)
                ->withInput()
                ->withErrors($validator)
                ->with('warning', 'There were validation errors');
        }
        else
        {   
            $input['modified_by'] = Session::get('admin_id');
            
            $category = PayRolls::find($id);
            $category->update($input);

            $log = new Log();
            $log->module_id=23;
            $log->action='update';      
            $log->description='Pay Rolls ' . $category->name.' Updated Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
        return Redirect::route('payrolls.index')->with('success',$log->description);
        
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = PayRolls::find($id);       
       
        if (is_null($category))
        {
         return Redirect::back()->with('warning','Pay Rolls Details Are Not Found!');
        }
        else
        {
        
            $category->delete();

            $log = new Log();
            $log->module_id=23;
            $log->action='delete';      
            $log->description='Pay Rolls ' . $category->name .' Deleted Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
           return Redirect::back()->with('success',$log->description);
        }
    }

    public function payrollsVerbige(Request $request)
    {   
        $payrollcofig = PayRollConfigurations::where('id','=',1)->first();
        if(is_null($payrollcofig))
        {   
            $data['payroll_pdf_text'] = $request['payroll_pdf_text'];
            $data['payroll_mile_text'] = $request['payroll_mile_text'];
            $data['visible_payroll_fields'] = json_encode($request['visible_payroll_fields']);
            PayRollConfigurations::create($data);
        }
        else
        {
            $data['payroll_pdf_text'] = $request['payroll_pdf_text'];
            $data['payroll_mile_text'] = $request['payroll_mile_text'];
            $data['visible_payroll_fields'] = json_encode($request['visible_payroll_fields']);
            $payrollcofig->update($data);
        }

        $request->session()->flash('alert-success', '');
        return Redirect::back()->with('success','Pay roll configurations Successfully updated!');
        
    }
}
