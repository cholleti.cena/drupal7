<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;
use DB;
use Session;
use App\Repositories\Users\AuthRepository;
use App\Helpers\CommonHelper;
use App\Models\States;

class StatesController extends Controller
{
    private $helper;
    private $service;

    public function __construct(CommonHelper $helper, AuthRepository $authRepo)
    {
        $this->helper = $helper;
        $this->authRepo = $authRepo;
    }

    public function __invoke(Request $request)
    {
         
    }

    public function index($country = ""){
        echo "Test"; exit;
        // $states = new States();
        // $result =  $states::selectRaw(" *")
        //     ->orderBy('id','DESC')
        //     ->where('country_id', '=', $country)
        //     ->where('status', '=', 1)
        //     ->get();

        //     print_r($result);
        //     exit;
        // return response()->json([
        //     'status' => 200,
        //     'data' => [],
        // ]); 
    }
    public function get($country = ""){
        $states = new States();
        $result =  $states::selectRaw("*")
            ->orderBy('name','ASC')
            ->where('country_id', '=', $country)
            ->where('status', '=', 1)
            ->get();

          
        return response()->json([
            'status' => 200,
            'data' => $result,
        ]); 
    }
}

