<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use View;
use DB;
use App\Models\PackingMaterials;
use Input;
use Session;
use App\Models\Log;
use Carbon\Carbon;
use DateTimeZone;
use App\Helpers\CommonHelper;

class PackingMaterialsController extends Controller
{
	private $helper;

    public function __construct(CommonHelper $helper)
    {
        $this->helper = $helper;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */   

    private function getPrivileges()
    {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,10,1);//role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,10,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,10,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,10,4);        
        return $privileges;
    }

    public function index()
    {
       if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();

        $packingmaterials = PackingMaterials::select(DB::raw('id,name,price,default_quantity,if(ifnull(is_active,1)=1,"Active","Inactive") as status'))
                ->orderBy('created_at','desc')
                ->paginate(5);
        return View::make('admin.packingmaterials.index', compact('packingmaterials'))         
        ->with('privileges',$privileges);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       if ( !Session::has('admin_id') || Session::get('admin_id') == '' )
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Add'] !='true')    
            return Redirect::back()->with('warning','Do not have permission to add!');    

        
        return View::make('admin.packingmaterials.create')
        ->with('privileges',$privileges);
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); 
         
        $this->validate($request, []);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {
            return Redirect::route('packingmaterials.create')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');

            $admin = PackingMaterials::create($input);

            $log = new Log();
            $log->module_id=10;
            $log->action='create';      
            $log->description='Packing Materials ' . $admin->name . ' is created';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get('admin_id'); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

        return Redirect::route('packingmaterials.index')->with('success','Packing Materials Created Successfully!');
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ( !Session::has('admin_id') || Session::get('admin_id') == '' )
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Edit'] !='true')
            return Redirect::back()->with('warning','Do not have permission to update!');  

        $packingmaterials = PackingMaterials::find($id);
         
        return View::make('admin.packingmaterials.edit', compact('packingmaterials'))
        ->with('privileges',$privileges);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $input = $request->all();  

         $this->validate($request, []);
        $rules = array('');
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) 
        {
            return Redirect::route('admin.packingmaterials.edit',$id)
                ->withInput()
                ->withErrors($validator)
                ->with('warning', 'There were validation errors');
        }
        else
        {   
            
            $input['modified_by'] = Session::get('admin_id');

            $admin = PackingMaterials::find($id);
            $admin->update($input);

            $log = new Log();
            $log->module_id=10;
            $log->action='update';      
            $log->description='Packing Materials ' . $admin->name . ' is updated';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
            return Redirect::route('packingmaterials.index')->with('success','Packing Materials Updated Successfully!');        
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = PackingMaterials::find($id);
        if (is_null($user))
        {
         return Redirect::back()->with('warning','Packing Materials Details Are Not Found!');
        }
        else
        {
            $user->delete();
            $log = new Log();
            $log->module_id=1;
            $log->action='delete';      
            $log->description='User '. $user->name . ' is Deleted';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get('admin_id'); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
           return Redirect::back()->with('warning','Packing Materials Deleted Successfully!');
        }
    }

    public function AdminLogin(Request $request)
    {
        $res = $this->service->adminLogin($request->all());
        return response()->json($res, 200);
    }

    public function ManageDashBoard(Request $request)
    {
        $res = $this->service->manageDashBoard();
        return response()->json($res, 200);
    }
}
