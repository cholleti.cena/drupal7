<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use App\Models\Units;
use Input;
use Session;
use App\Models\Log;
use File;
use Image;
use Carbon\Carbon;
use DateTimeZone;
use Hash;
use App\Helpers\CommonHelper;

class UnitsController extends Controller
{
    private $helper;
    private $service;

    public function __construct(CommonHelper $helper)
    {
        $this->helper = $helper;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */   

    private function getPrivileges()
     {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,24,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,24,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,24,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,24,4);        
        return $privileges;
     }

    public function index(Request $request)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();

        $facility_id = $request['facility_id'];

        $units = Units::select(DB::raw('id,facility_id,unit_name,width,height,depth,cuft,total_occupied,location,price,quantity'))
        ->where('facility_id','=',$facility_id)
        ->orderBy('created_at','desc')
        ->paginate(5);

        return View::make('admin.units.index', compact('units'))
        ->with('facility_id',$facility_id)
        ->with('privileges',$privileges);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
        if($privileges['Add'] !='true')    
            return Redirect::back()->with('warning','Do not have permission to add!');   
        
        $facility_id = $request['facility_id'];

        return View::make('admin.units.create')
        ->with('facility_id',$facility_id)
        ->with('privileges',$privileges);
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all(); 
        $this->validate($request, []);

        $facility_id = $request['facility_id'];        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.units.create')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');
            $input['facility_id'] = $facility_id;

            $category = Units::create($input);       

            $log = new Log();
            $log->module_id=24;
            $log->action='create';      
            $log->description='Units ' . $category->unit_name .' Created Successfully!';
            $log->created_on=  Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get('admin_id'); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);

        return Redirect::route('units.index',array('facility_id'=>$facility_id))->with('success',$log->description);
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $facility_id = $request['facility_id'];  

        $privileges = $this->getPrivileges();
        if($privileges['Edit'] !='true')            
            return Redirect::back()->with('warning','Do not have permission to add!'); 

        $charge = Units::find($id);
 
        return View::make('admin.units.edit',array('facility_id'=>$facility_id), compact('charge'))
        ->with('facility_id',$facility_id)
        ->with('privileges',$privileges);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $input = $request->all(); 
         $facility_id = $input['facility_id']; 
         $this->validate($request, []);
        $rules = array('');
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) 
        {
            return Redirect::route('admin.units.edit',$id)
                ->withInput()
                ->withErrors($validator)
                ->with('warning', 'There were validation errors');
        }
        else
        {   
            $input['modified_by'] = Session::get('admin_id');
            
            $category = Units::find($id);
            $category->update($input);

            $log = new Log();
            $log->module_id=24;
            $log->action='update';      
            $log->description='Units ' . $category->unit_name.' Updated Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
        return Redirect::route('units.index',array('facility_id'=>$facility_id))->with('success',$log->description);
        
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Units::find($id);       
       
        if (is_null($category))
        {
         return Redirect::back()->with('warning','Units Details Are Not Found!');
        }
        else
        {
        
            $category->delete();

            $log = new Log();
            $log->module_id=24;
            $log->action='delete';      
            $log->description='Units ' . $category->name .' Deleted Successfully!';
            $log->created_on= Carbon::now(new DateTimeZone('Asia/Kolkata'));
            $log->user_id=Session::get("admin_id"); 
            $log->category=1;    
            $log->log_type=1;
            $this->helper->createLog($log);
           return Redirect::back()->with('success',$log->description);
        }
    }
}
