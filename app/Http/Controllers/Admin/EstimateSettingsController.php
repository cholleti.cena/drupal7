<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use Input;
use Session;
use Carbon\Carbon;
use DateTimeZone;
use App\Helpers\CommonHelper;
use App\Repositories\Users\AuthRepository;
use App\Models\LaborPriceEstimate;
use App\Models\LaborPriceEstimateHours;
use App\Models\EstimateDefaultType;
use App\Models\EstimateDefaultFloors;
use App\Models\EstimateDaysCapacity;
use App\Models\WeekDays;
use App\Models\EstimateHourlyServiceQuoted;
use App\Models\EstimateDefaultEstimateNotes;
use App\Models\EstimateJobNotes;
use App\Models\EstimateHourlyNotes;
use App\Models\EstimateMileageTravel;


class EstimateSettingsController extends Controller
{
    private $helper;
    private $service;

    public function __construct(CommonHelper $helper,AuthRepository $authRepo)
    {
        $this->helper = $helper;
        
        $this->authRepo = $authRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getPrivileges()
     {
        $roleid = Session::get("role_id");
        $privileges['View']  = $this->helper->ValidateUserPrivileges($roleid,7,1);  //role, module, privilege
        $privileges['Add']  = $this->helper->ValidateUserPrivileges($roleid,7,2);
        $privileges['Edit']  = $this->helper->ValidateUserPrivileges($roleid,7,3);
        $privileges['Delete']  = $this->helper->ValidateUserPrivileges($roleid,7,4);        
        return $privileges;
     }

    public function index()
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $privileges = $this->getPrivileges();
            
        $laborprices = LaborPriceEstimate::selectRaw('id,people,price_per_hour,base_price,pack_per_hour,days')
        ->orderBy('created_at','desc')
        ->paginate(5);

        $laborhours = LaborPriceEstimateHours::selectRaw('labor_price_estimate_id,minimum_hours,is_charge_after_minimum_hours,minimum_type,inventory_type,inventory_type_for_iframe,hourly_rate_type,is_iframe_auto_calculate,is_sort_inventory_vertically,is_default_type,default_type,is_default_floors,default_floors')
        ->orderBy('created_at','desc')
        ->paginate(5);

         $types = EstimateDefaultType::selectRaw('id,name')
        ->orderBy('created_at','desc')
        ->paginate(10);

        $floors = EstimateDefaultFloors::selectRaw('id,flooors')
        ->orderBy('created_at','desc')
        ->paginate(5);

        $days = EstimateDaysCapacity::selectRaw('id,number_of_jobs,number_of_men,number_of_trucks,recurring_days')
        ->orderBy('created_at','desc')
        ->paginate(5);

        $recurringdays = WeekDays::selectRaw('id,day')
        ->orderBy('created_at','desc')
        ->paginate(5);

        $hourlyservices = EstimateHourlyServiceQuoted::selectRaw('id,is_moving_services,is_traveling_services,is_packing_materials,is_packing_services,is_storage_services,is_fuel_surcharge,is_discount,hide_fields_from_estimate,is_show_total_est_price,is_show_hourly_notes,hourly_price_text')
        ->orderBy('created_at','desc')
        ->paginate(5);

         $defaultnotes = EstimateDefaultEstimateNotes::selectRaw('id,description')
        ->orderBy('created_at','desc')
        ->paginate(5);

         $defaultjobs = EstimateJobNotes::selectRaw('id,description')
        ->orderBy('created_at','desc')
        ->paginate(10);

        $hourlynotes = EstimateHourlyNotes::selectRaw('id,description')
        ->orderBy('created_at','desc')
        ->paginate(10);

        return View::make('admin.estimatesettings.index')   
        ->with('laborprices',$laborprices)      
        ->with('laborhours',$laborhours)        
        ->with('types',$types)        
        ->with('floors',$floors)       
        ->with('days',$days)       
        ->with('recurringdays',$recurringdays)         
        ->with('hourlyservices',$hourlyservices)          
        ->with('defaultnotes',$defaultnotes)          
        ->with('defaultjobs',$defaultjobs)            
        ->with('hourlynotes',$hourlynotes)                               
        ->with('privileges',$privileges);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $input = $request->all(); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
    public function addEstimateLaborPrice(Request $request)
    {
        
        $input = $request->all(); 
        $this->validate($request, []);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.estimatesettings.index')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');
                
            $terms = LaborPriceEstimate::create($input);       


            return Redirect::route('estimatesettings.index')->with('success',"Labor Rate
             Added Successfully");
        
        }

    }

    public function updateEstimateLaborPrice(Request $request)
    {
       $input = $request->all(); 
        
       $data['id'] = $input['id'];
       $data['people'] = $input['people']; 
       $data['price_per_hour'] = $input['price_per_hour'];
       $data['base_price'] = $input['base_price']; 
       $data['pack_per_hour'] = $input['pack_per_hour']; 
       //$data['days'] = $input['days'];
      
        $laborprices = LaborPriceEstimate::find($input['id']);
        $laborprices->update($data);

            
        return Redirect::route('estimatesettings.index')->with('success',"Labor price updated Successfully");

    }

    public function deleteEstimateLaborPrice($id)
    {
        $laborprices = LaborPriceEstimate::find($id);       
       
        if (is_null($laborprices))
        {
         return Redirect::back()->with('warning','Additional Charges Details Are Not Found!');
        }
        else
        {
        
            $laborprices->delete();

            
           return Redirect::back()->with('success',"Deleted Successfully");
        }
    }

    public function updateEstimateLaborHour(Request $request)
    {
       $input = $request->all(); 
        
       $data['id'] = $input['labor_price_estimate_id'];
       $data['minimum_hours'] = $input['minimum_hours']; 
       $data['is_charge_after_minimum_hours'] = $input['is_charge_after_minimum_hours'];
       $data['minimum_type'] = $input['minimum_type']; 
       $data['inventory_type'] = $input['inventory_type']; 
       $data['inventory_type_for_iframe'] = $input['inventory_type_for_iframe']; 
       $data['hourly_rate_type'] = $input['hourly_rate_type'];
       $data['is_iframe_auto_calculate'] = $input['is_iframe_auto_calculate']; 
       $data['is_sort_inventory_vertically'] = $input['is_sort_inventory_vertically'];  
       $data['is_default_type'] = $input['is_default_type'];
       $data['default_type'] = $input['default_type']; 
       $data['is_default_floors'] = $input['is_default_floors'];
       $data['default_floors'] = $input['default_floors'];
      
        

        $laborprices = LaborPriceEstimateHours::find($input['labor_price_estimate_id']);
        $laborprices->update($data);

            
        return Redirect::route('estimatesettings.index')->with('success',"Labor price updated Successfully");

    }

    public function updateEstimateDaysCapacity(Request $request)
    {
       $input = $request->all(); 
        
       $data['id'] = $input['id'];
       $data['number_of_jobs'] = $input['number_of_jobs']; 
       $data['number_of_men'] = $input['number_of_men'];
       $data['number_of_trucks'] = $input['number_of_trucks'];
      
        

        $laborprices = EstimateDaysCapacity::find($input['id']);
        $laborprices->update($data);

            
        return Redirect::route('estimatesettings.index')->with('success',"Days Capacity updated Successfully");

    }

     public function updateHourlyServiceQuotedDefaults(Request $request)
    {
       $input = $request->all(); 
        
       $data['id'] = $input['id'];
       $data['is_moving_services'] = $input['is_moving_services']; 
       $data['is_traveling_services'] = $input['is_traveling_services'];
       $data['is_packing_materials'] = $input['is_packing_materials']; 
       $data['is_packing_services'] = $input['is_packing_services']; 
       $data['is_storage_services'] = $input['is_storage_services']; 
       $data['is_fuel_surcharge'] = $input['is_fuel_surcharge'];
       $data['is_discount'] = $input['is_discount']; 
       //$data['hide_fields_from_estimate'] = $input['hide_fields_from_estimate'];
       $data['is_show_total_est_price'] = $input['is_show_total_est_price'];    
       $data['is_show_hourly_notes'] = $input['is_show_hourly_notes'];
      
        

        $laborprices = EstimateHourlyServiceQuoted::find($input['id']);
        $laborprices->update($data);

            
        return Redirect::route('estimatesettings.index')->with('success',"Hourly Service Quoted Defaults updated Successfully");

    }

    public function UpdateHourlyPriceText(Request $request)
    {
       $input = $request->all(); 
        
       $data['id'] = $input['id'];
       $data['hourly_price_text'] = $input['hourly_price_text'];
      
        

        $laborprices = EstimateHourlyServiceQuoted::find($input['id']);
        $laborprices->update($data);

            
        return Redirect::route('estimatesettings.index')->with('success',"Hourly Price Text updated Successfully");

    }

    public function AddDefaultEstimateNotes(Request $request)
    {
        
        $input = $request->all(); 
        $this->validate($request, []);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.estimatesettings.index')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');
                
            $notes = EstimateDefaultEstimateNotes::create($input);       


            return Redirect::route('estimatesettings.index')->with('success',"Default Estimate Notes
             Added Successfully");
        
        }

    }
     public function editDefaultEstimateNotes($id)
    {
        
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin'); 

         $records = EstimateDefaultEstimateNotes::selectRaw(" * ")
        ->where('id', '=', $id)
        ->get();
        
        return response()->json([
            'status' => 200,
            'data' => $records,
        ]);  
    }
    public function UpdateDefaultEstimateNotes(Request $request)
    {
       $input = $request->all(); 
        
       $data['id'] = $input['id'];
       $data['description'] = $input['description'];
      
        

        $defaultnotes = EstimateDefaultEstimateNotes::find($input['id']);
        $defaultnotes->update($data);

            
        return Redirect::route('estimatesettings.index')->with('success',"Default Estimate Notes updated Successfully");

    }
    public function deleteDefaultEstimateNotes($id)
    {
        $notes = EstimateDefaultEstimateNotes::find($id);       
       
        if (is_null($notes))
        {
         return Redirect::back()->with('warning','Additional Charges Details Are Not Found!');
        }
        else
        {
        
            $notes->delete();

            
           return Redirect::back()->with('success',"Deleted Successfully");
        }
    }

    public function AddDefaultEstimateJob(Request $request)
    {
        
        $input = $request->all(); 
        $this->validate($request, []);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.estimatesettings.index')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');
                
            $notes = EstimateJobNotes::create($input);       


            return Redirect::route('estimatesettings.index')->with('success',"Default Estimate Job
             Added Successfully");
        
        }

    }
    public function editDefaultEstimateJob($id)
    {
        
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin'); 

         $records = EstimateJobNotes::selectRaw(" * ")
        ->where('id', '=', $id)
        ->get();
        
        return response()->json([
            'status' => 200,
            'data' => $records,
        ]);  
    }
    public function UpdateDefaultEstimateJob(Request $request)
    {
       $input = $request->all(); 
        
       $data['id'] = $input['id'];
       $data['description'] = $input['description'];
      

        $defaultjob = EstimateJobNotes::find($input['id']);
        $defaultjob->update($data);

            
        return Redirect::route('estimatesettings.index')->with('success',"Default Estimate Job updated Successfully");

    }
    public function deleteDefaultEstimateJob($id)
    {   

        $notes = EstimateJobNotes::find($id);       
       
        if (is_null($notes))
        {
         return Redirect::back()->with('warning','Additional Charges Details Are Not Found!');
        }
        else
        {
        
            $notes->delete();

            
           return Redirect::back()->with('success',"Deleted Successfully");
        }
    }
     public function AddHourlyNotes(Request $request)
    {
        
        $input = $request->all(); 
        $this->validate($request, []);        
        
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.estimatesettings.index')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $input['created_by'] = Session::get('admin_id');
                
            $notes = EstimateHourlyNotes::create($input);       


            return Redirect::route('estimatesettings.index')->with('success',"Hourly Notes
             Added Successfully");
        
        }

    }
    public function editHourlyNotes($id)
    {
        
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin'); 

         $records = EstimateHourlyNotes::selectRaw(" * ")
        ->where('id', '=', $id)
        ->get();

        return response()->json([
            'status' => 200,
            'data' => $records,
        ]);  
    }
    public function UpdateHourlyNotes(Request $request)
    {
       $input = $request->all(); 
        
       $data['id'] = $input['id'];
       $data['description'] = $input['description'];
      

        $hourlynote = EstimateHourlyNotes::find($input['id']);
        $hourlynote->update($data);

            
        return Redirect::route('estimatesettings.index')->with('success',"Hourly Notes updated Successfully");

    }
    public function deleteHourlyNotes($id)
    {   

        $notes = EstimateHourlyNotes::find($id);       
       
        if (is_null($notes))
        {
         return Redirect::back()->with('warning','Additional Charges Details Are Not Found!');
        }
        else
        {
        
            $notes->delete();

            
           return Redirect::back()->with('success',"Deleted Successfully");
        }
    }


}
