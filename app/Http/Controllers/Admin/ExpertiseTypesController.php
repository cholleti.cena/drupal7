<?php

namespace App\Http\Controllers\Admin;
use View;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;

use App\Models\ExpertiseTypes;

class ExpertiseTypesController extends Controller
{
    //
    public function index(){
        $data = array();
        $where = array();
        $where['status'] = 1;
        $result =  ExpertiseTypes::selectRaw("*")
        ->orderBy('id','DESC')
        ->where('status', '=', 1)
        ->get();
        $data['expertises'] = $result;
        return view('admin.application.expertisetypes')->with($data);
    }

    /**
     * Show the form for creating a new resource. and update the same
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');
        $input = $request->all();
        $this->validate($request, []);   
 
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.application.expertisetypes')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $expertiseTypes = new ExpertiseTypes();
            $expertiseTypes->name = $input['expertiseName'];
            $message = 'Created';
            
            if($input['record_id'] && $input['record_id'] != ''){ 
                $result = DB::update('update expertise_types set name = ?, updated_by=? where id = ?',[$input['expertiseName'],Session::has('admin_id'), $input['record_id']]);
                $message = 'Updated';
            } else {

                $expertiseTypes->status = 1;
                $expertiseTypes->created_by = Session::has('admin_id');
                $result = $expertiseTypes->save();
            }
            if($result){
                return Redirect::to('/admin/expertisetypes')->with('success', $message.' successfully');
            } else {
                return ["result"=>'Fleet type not screated'];
            }
        }
    }
    
    /** delete records
     * @fieldName column name
     * $value  searching column value
     */
    public function delete($id)
    {
         
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');
            DB::update('update expertise_types set status = ?, updated_by=? where id = ?',[0,Session::has('admin_id'), $id]);
            
            $message = 'Deleted successfully';
            
            return Redirect::to('/admin/expertisetypes')->with('success', $message);
    }

    /** fetch records
     * $value  searching id column
     */
    public function edit($value)
    {
         
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

        $records = ExpertiseTypes::selectRaw(" * ")
        ->where('id', '=', $value)
        ->get();
        
        return response()->json([
            'status' => 200,
            'data' => $records,
        ]);  
    }
}
