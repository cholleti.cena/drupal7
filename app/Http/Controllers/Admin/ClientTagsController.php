<?php

namespace App\Http\Controllers\Admin;
use View;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;

use App\Models\ClientTags;

class ClientTagsController extends Controller
{
    public function index(){
        $data = array();
        $where = array();
        $where['status'] = 1;
        $result =  ClientTags::selectRaw("*")
        ->orderBy('id','DESC')
        ->where('status', '=', 1)
        ->get();
        $data['clienttags'] = $result;
        return view('admin.application.clienttags')->with($data);
    }
    /**
     * Show the form for creating a new resource. and update the same
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');
        $input = $request->all();
        $this->validate($request, []);   
 
        $rules = array('');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) 
        {            
            return Redirect::route('admin.application.clienttags')
                ->withInput()
                ->withErrors($validator)
                ->with('errors', 'There were validation errors');
        }
        else
        {   
            $insert = new ClientTags();
            $insert->name = $input['clientTagName'];
            $insert->color = $input['clientTagColor'];
            $message = 'created';
            // print_r($insert);
            // exit;
            if($input['record_id'] && $input['record_id'] != ''){ 
                $result = DB::update('update client_tags set name = ?,color = ?, updated_by=? where id = ?',[$input['clientTagName'],$input['clientTagColor'],Session::has('admin_id'), $input['record_id']]);
                $message = 'Updated';
            } else {
                $insert->status = 1;
                $insert->created_by = Session::has('admin_id');
                $result = $insert->save();
            }
            if($result){
                return Redirect::to('/admin/clienttags')->with('success', $message.' successfully');
            } else {
                return ["result"=>'Fleet type not screated'];
            }
        }
    }
    
    /** delete records
     * @fieldName column name
     * $value  searching column value
     */
    public function delete($id)
    {
         
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');
            DB::update('update client_tags set status = ?, updated_by=? where id = ?',[0,Session::has('admin_id'), $id]);
            
            $message = 'Deleted successfully';
            
            return Redirect::to('/admin/clienttags')->with('success', $message);
    }
    /** fetch records
     * $value  searching id column
     */
    public function edit($value)
    {
         
        if(!Session::has('admin_id') || Session::get('admin_id') == '')
            return Redirect::to('/admin');

            // $privileges = $this->getPrivileges();
        $records = ClientTags::selectRaw(" * ")
        ->where('id', '=', $value)
        ->get();
        
        return response()->json([
            'status' => 200,
            'data' => $records,
        ]);  
    }
}


