<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;
use App\Models\Defaults;
use Illuminate\Support\Facades\Redirect;
use App\Repositories\Users\AuthRepository;
use Session;
use App\Models\User;
use App\Repositories\Profile\ProfileRepository;
use App\Helpers\CommonHelper;
Use App\Models\AuthAccessTokens;

class HomeController extends Controller
{
    private $authRepo;
    private $profileRepo;
    private $helpers;

    public function __construct(AuthRepository $authRepo,ProfileRepository $profileRepo,CommonHelper $helpers)
    {
      $this->authRepo = $authRepo;
      $this->profileRepo = $profileRepo;
      $this->helpers = $helpers;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $default = Defaults::first();
        if(is_null($default))
        {
            return Redirect::back()->with('warning','Do not have permission to add!'); 
        }

        $mdata['free_text'] = "";
        $mdata['metaData'] = array("servicetypes","property","movesizes","category","materials","packing_requirements","storage_requirements","referrals","phonetypes","services");
        $metadata = $this->authRepo->getMetaDataList($mdata);

        $floors = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];

        return View::make('web.home.index')
        ->with('default',$default)
        ->with('metadata',$metadata)
        ->with('floors',$floors);
    }

    public function aboutUs()
    {
        $default = Defaults::first();
        if(is_null($default))
        {
            return Redirect::back()->with('warning','Do not have permission to add!'); 
        }
        return View::make('web.home.about')        
        ->with('default',$default);
    }

    public function contactUs()
    {
        $default = Defaults::first();
        if(is_null($default))
        {
            return Redirect::back()->with('warning','Do not have permission to add!'); 
        }
        return View::make('web.home.contact')        
        ->with('default',$default);
    }

    public function Authentication(Request $request)
    {
        if(isset($request['access_token']))
        {
            $user = User::where('reset_hash','=',$request['access_token'])->first();
            if($user)
            {
                $user_id = $user->id;
                Session::put("user_id",$user->id);
                Session::put("user_name",$user->first_name);
                Session::put("profile_pic",$user->first_name);

                if(isset($request['type']))
                {
                    if (strpos($request['type'], 'course-details') !== false) 
                    {
                        return Redirect::to($request['type']);
                    }
                    elseif (strpos($request['type'], 'job-details') !== false) 
                    {
                        return Redirect::to($request['type']);
                    }
                    else
                    {
                        return Redirect::to('/courses');
                    }
                }
                else
                {
                    return Redirect::to('/courses');
                }
            }
            return \Redirect::back();
        }
        else
        {
            return \Redirect::back();
        }
    }

    public function Dashboard(Request $request)
    {
        if(!Session::has('user_id') || Session::get('user_id') == '')
        {
            return redirect('/');
        }
        else
        {
            $user_id = Session::get('user_id');
        }

        return View::make('web.home.dashboard');
    }


    public function MyProfile(Request $request)
    {
        if(!Session::has('user_id') || Session::get('user_id') == '')
        {
            return redirect('/');
        }
        else
        {
            $user_id = Session::get('user_id');
        }
        $data['user_id'] = $user_id;
        $userdetails = $this->profileRepo->getUserDetails($data);
        $gdata['free_text'] = "";
        $gdata['metaData'] = array('educations','states','cities','years','configtypes');
        $general = $this->authRepo->getMetaDataList($gdata);
        return View::make('web.home.myprofile')
        ->with('userdetails',$userdetails)
        ->with('general',$general);
    }

    public function Faqs(Request $request)
    {
        $getfaqs = $this->authRepo->getFAQs();

        return View::make('web.home.faqs')
        ->with('getfaqs',$getfaqs);
    }

    public function logout(Request $request) 
    {   

          Session::forget('user_id');
          Session::forget('user_name');
          Session::forget('profile_pic');
          echo '<script type="text/JavaScript">localStorage.removeItem("user_data");</script>';
          return redirect('/');
    }
    public function resetpassword(Request $request) 
    {
        if(isset($request['authentication']))
        {
            $user = User::where('reset_hash','=',$request['authentication'])->first();
            if($user)
            {
                return View::make('web.login.resetpassword')
                ->with('email',$user->email);
            }
            return \Redirect::back();
        }
        else
        {
           return \Redirect::back();
        }
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function PostLead(Request $request)
    {
        $data = $request->all();
        
        //echo "<pre>";print_r($data);exit;

        $postLead = $this->authRepo->postLead($data);
        return View::make('web.home.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
