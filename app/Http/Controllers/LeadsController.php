<?php

namespace App\Http\Controllers
;
use App\Http\Controllers\Controller;
use App\Services\General\LeadServices;
use Illuminate\Http\Request;

class LeadsController extends Controller
{

 private $service;

  public function __construct(LeadServices $service)
  {
    $this->service = $service;
  }

  public function ManageLeads(Request $request)
  {
    if($request->type == "lead_list"):
          $res = $this->service->leadList($request->all());
          return response()->json($res, 200);
    elseif($request->type == "lead_details"):
          $res = $this->service->leadDetails($request->all());
          return response()->json($res, 200);
    else:
          $res['status_code'] = 201;
          $res['message'] = 'Input not found';
          $res['data'] = false;
          return $res;
    endif;
  }

  public function GetMetaDataList(Request $request)
  {
      $res = $this->service->getMetaDataList($request->all());
      return response()->json($res, 200);
  }

  public function ManageCategory(Request $request)
  {
    if($request->type == "add_category"):
          $res = $this->service->addCategory($request->all());
          return response()->json($res, 200);
    elseif($request->type == "update_category"):
          $res = $this->service->updateCategory($request->all());
          return response()->json($res, 200);
    elseif($request->type == "delete_category"):
          $res = $this->service->deleteCategory($request->all());
          return response()->json($res, 200);
    elseif($request->type == "category_details"):
          $res = $this->service->categoryDetails($request->all());
          return response()->json($res, 200);
    elseif($request->type == "category_list"):
          $res = $this->service->categoryList($request->all());
          return response()->json($res, 200);
    else:
          $res['status_code'] = 201;
          $res['message'] = 'Input not found';
          $res['data'] = false;
          return $res;
    endif;
  }
}
