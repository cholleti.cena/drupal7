<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

//Admin
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\AdminUserController;
use App\Http\Controllers\Admin\CategoriessController;
use App\Http\Controllers\Admin\BannersController;
use App\Http\Controllers\Admin\PrivilegesController;
use App\Http\Controllers\Admin\TestimonialController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\LogsController;
use App\Http\Controllers\Admin\ConfigurationController;
use App\Http\Controllers\Admin\LeadsController;
use App\Http\Controllers\Admin\SchedulesController;
use App\Http\Controllers\Admin\TasksController;
use App\Http\Controllers\Admin\PackingMaterialsController;
use App\Http\Controllers\Admin\RoomsController;
use App\Http\Controllers\Admin\AdditionalChargesController;
use App\Http\Controllers\Admin\InventoryItemsController;
use App\Http\Controllers\Admin\FollowUpAutomationController;
use App\Http\Controllers\Admin\LeadsFollowUpController;
use App\Http\Controllers\Admin\TravelTimeController;
use App\Http\Controllers\Admin\FuelChargeController;
use App\Http\Controllers\Admin\SquareFootageController;
use App\Http\Controllers\Admin\FloorsController;
use App\Http\Controllers\Admin\EstimateRangeController;
use App\Http\Controllers\Admin\DistanceFromTruckController;
use App\Http\Controllers\Admin\ManHourCalculationsController;
use App\Http\Controllers\Admin\PayrollsController;
use App\Http\Controllers\Admin\FacilitiesController;
use App\Http\Controllers\Admin\UnitsController;
use App\Http\Controllers\Admin\BolinvoiceController;
use App\Http\Controllers\Admin\FleetsController;
use App\Http\Controllers\Admin\FleetsCalenderController;
use App\Http\Controllers\Admin\EstimateSettingsController;

use App\Http\Controllers\Admin\FleettypesController;
use App\Http\Controllers\Admin\ProspectInterestStatusesController;
use App\Http\Controllers\Admin\ReferredBySourceController;
use App\Http\Controllers\Admin\ExpertiseTypesController;
use App\Http\Controllers\Admin\ClientTagsController;

use App\Http\Controllers\Admin\EmployeesController;
use App\Http\Controllers\Admin\StatesController;

//Web
use App\Http\Controllers\Web\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/migrate', function () {
    Artisan::call('migrate:fresh');
    Artisan::call('db:seed');
    return "Migrated and seed";
});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    return "Cache is cleared";
});
Route::get('/passport-install', function () {
    Artisan::call('passport:install --force');
    return "passport-install";
});

Route::resource('/', HomeController::class);
Route::get('/sign-up', function () {
    return view('web.login.signup');
});
Route::get('/home', function () {
  return view('home');
});
Route::get('/login', function () {
    return view('web.login.login');
});
Route::get('/forgot', function () {
    return view('web.login.forgot');
});
Route::get('/zoom-meeting', function () {
    return view('web.login.zoommeeting');
});
Route::put('/paymentresponse', function () {
    return view('web.home.paymentresponse');
});
Route::get('logOut', [HomeController::class,'logOut']);
Route::get('/about-us', [HomeController::class,'aboutUs']);
Route::get('/contact-us', [HomeController::class,'contactUs']);
Route::get('/dashboard', [HomeController::class,'Dashboard']);
Route::get('/my-profile', [HomeController::class,'MyProfile']);
Route::get('/reset-password', [HomeController::class,'Resetpassword']);
Route::get('/authentication', [HomeController::class,'Authentication']);
Route::get('/faqs', [HomeController::class,'Faqs']);
Route::post('postLead', [HomeController::class,'PostLead']);

Route::group(['prefix' => 'admin'], function() 
{
  Route::resource('/', LoginController::class);
  Route::get('/forgot', function () {
    return view('admin.login.forgot');
  });
  Route::get('/resetpassword', function () {
    return view('admin.login.resetpassword');
  });
  Route::get('/auth', function () {
    return view('admin.login.auth');
  });

  Route::get('logout', [LoginController::class,'logout']);

  Route::post('validateuser', [LoginController::class,'validateuser']);
  Route::post('auth', [LoginController::class,'auth']);
  Route::resource('dashboard', DashboardController::class);
  Route::resource('user', AdminUserController::class);
  Route::resource('privileges', PrivilegesController::class);
  Route::post('allowprivileges/{role_id}/{module_id}/{privilege_id}', [PrivilegesController::class,'allowprivileges'])->name('allowprivileges');
  Route::post('denyprivileges/{role_id}/{module_id}/{privilege_id}', [PrivilegesController::class,'denyprivileges'])->name('denyprivileges');
  Route::resource('banners', BannersController::class);
  Route::resource('logs', LogsController::class);
  Route::get('getlogsDownload', [LogsController::class,'getlogsDownload'])->name('getlogsDownload');
  Route::resource('configuration', ConfigurationController::class);
  Route::resource('categories', CategoriessController::class);
  Route::resource('faq', FaqController::class);
  Route::resource('testimonials', TestimonialController::class);
  Route::resource('role', RoleController::class);
  Route::resource('profile', ProfileController::class);
  Route::resource('leads', LeadsController::class);
  Route::resource('schedules', SchedulesController::class);  
  Route::resource('packingmaterials', PackingMaterialsController::class);
  Route::resource('rooms', RoomsController::class);
  Route::resource('inventoryitems', InventoryItemsController::class);
  Route::resource('additionalcharges', AdditionalChargesController::class);
  Route::resource('followupautomation', FollowUpAutomationController::class);
  Route::resource('leadsfollowup', LeadsFollowUpController::class);
  Route::resource('traveltime', TravelTimeController::class);
  Route::resource('fuelcharges', FuelChargeController::class);
  Route::resource('squarefootage', SquareFootageController::class);
  Route::resource('floors', FloorsController::class);  
  Route::resource('estimaterange', EstimateRangeController::class);
  Route::resource('distancefromtruck', DistanceFromTruckController::class);
  Route::resource('manhourcalculation', ManHourCalculationsController::class);
  Route::resource('payrolls', PayrollsController::class);
  Route::resource('facilities', FacilitiesController::class);
  Route::resource('units', UnitsController::class);
  Route::post('payrollsVerbige', [PayrollsController::class,'payrollsVerbige'])->name('payrollsVerbige');
  Route::resource('bolinvoice', BolinvoiceController::class);
  Route::post('updateVerbiage', [BolinvoiceController::class,'updateVerbiage'])->name('updateVerbiage');
  Route::post('updateBolText', [BolinvoiceController::class,'updateBolText'])->name('updateBolText');

  Route::post('addTermsandconditions', [BolinvoiceController::class,'addTermsandconditions'])->name('addTermsandconditions');
  Route::post('updateTerms', [BolinvoiceController::class,'updateTerms'])->name('updateTerms');
  Route::get('bolinvoice/editTerms/{id}', [BolinvoiceController::class,'editTerms']);
  Route::get('bolinvoice/deleteTerms/{id}', [BolinvoiceController::class,'deleteTerms']);

  Route::get('bolinvoice/editNoticeandpolicies/{id}', [BolinvoiceController::class,'editNoticeandpolicies']);
  Route::post('updateNoticeandpolicies', [BolinvoiceController::class,'updateNoticeandpolicies'])->name('updateNoticeandpolicies');
  
  Route::get('bolinvoice/editInvoiceNotes/{id}', [BolinvoiceController::class,'editInvoiceNotes']);
  Route::post('updateInvoiceNotes', [BolinvoiceController::class,'updateInvoiceNotes'])->name('updateInvoiceNotes');

  Route::get('bolinvoice/editInvoiceMaxPrice/{id}', [BolinvoiceController::class,'editInvoiceMaxPrice']);
  Route::post('updateInvoiceMaxPrice', [BolinvoiceController::class,'updateInvoiceMaxPrice'])->name('updateInvoiceMaxPrice');

Route::resource('fleets', FleetsController::class);
Route::resource('fleetscalender', FleetsCalenderController::class);
Route::post('fleetscalenderDetails', [FleetsCalenderController::class,'fleetscalenderDetails'])->name('fleetscalenderDetails');

Route::resource('estimatesettings', EstimateSettingsController::class);
Route::post('addEstimateLaborPrice', [EstimateSettingsController::class,'addEstimateLaborPrice'])->name('addEstimateLaborPrice');
Route::post('updateEstimateLaborPrice', [EstimateSettingsController::class,'updateEstimateLaborPrice'])->name('updateEstimateLaborPrice');
Route::get('deleteEstimateLaborPrice/{id}', [EstimateSettingsController::class,'deleteEstimateLaborPrice'])->name('deleteEstimateLaborPrice');
Route::post('updateEstimateLaborHour', [EstimateSettingsController::class,'updateEstimateLaborHour'])->name('updateEstimateLaborHour');
Route::post('updateEstimateDaysCapacity', [EstimateSettingsController::class,'updateEstimateDaysCapacity'])->name('updateEstimateDaysCapacity');
Route::post('updateHourlyServiceQuotedDefaults', [EstimateSettingsController::class,'updateHourlyServiceQuotedDefaults'])->name('updateHourlyServiceQuotedDefaults');
Route::post('UpdateHourlyPriceText', [EstimateSettingsController::class,'UpdateHourlyPriceText'])->name('UpdateHourlyPriceText');
Route::post('AddDefaultEstimateNotes', [EstimateSettingsController::class,'AddDefaultEstimateNotes'])->name('AddDefaultEstimateNotes');
Route::get('editDefaultEstimateNotes/{id}', [EstimateSettingsController::class,'editDefaultEstimateNotes'])->name('editDefaultEstimateNotes');
Route::post('UpdateDefaultEstimateNotes', [EstimateSettingsController::class,'UpdateDefaultEstimateNotes'])->name('UpdateDefaultEstimateNotes');
Route::get('deleteDefaultEstimateNotes/{id}', [EstimateSettingsController::class,'deleteDefaultEstimateNotes'])->name('deleteDefaultEstimateNotes');
Route::post('AddDefaultEstimateJob', [EstimateSettingsController::class,'AddDefaultEstimateJob'])->name('AddDefaultEstimateJob');
Route::get('editDefaultEstimateJob/{id}', [EstimateSettingsController::class,'editDefaultEstimateJob'])->name('editDefaultEstimateJob');
Route::post('UpdateDefaultEstimateJob', [EstimateSettingsController::class,'UpdateDefaultEstimateJob'])->name('UpdateDefaultEstimateJob');
Route::get('deleteDefaultEstimateJob/{id}', [EstimateSettingsController::class,'deleteDefaultEstimateJob'])->name('deleteDefaultEstimateJob');
Route::post('AddHourlyNotes', [EstimateSettingsController::class,'AddHourlyNotes'])->name('AddHourlyNotes');
Route::get('editHourlyNotes/{id}', [EstimateSettingsController::class,'editHourlyNotes'])->name('editHourlyNotes');
Route::post('UpdateHourlyNotes', [EstimateSettingsController::class,'UpdateHourlyNotes'])->name('UpdateHourlyNotes');
Route::get('deleteHourlyNotes/{id}', [EstimateSettingsController::class,'deleteHourlyNotes'])->name('deleteHourlyNotes');






  Route::resource('tasks', TasksController::class);
  Route::resource('fleettypes', FleettypesController::class);
  Route::post('fleettypes/create', [FleettypesController::class,'create']);
  Route::get('fleettypes/delete/{id}', [FleettypesController::class,'delete']);
  Route::get('fleettypes/edit/{id}', [FleettypesController::class,'edit']);
  
  Route::resource('prospectintereststatuses', ProspectInterestStatusesController::class);
  Route::post('prospectintereststatuses/create', [ProspectInterestStatusesController::class,'create']);
  Route::get('prospectintereststatuses/delete/{id}', [ProspectInterestStatusesController::class,'delete']);
  Route::get('prospectintereststatuses/edit/{id}', [ProspectInterestStatusesController::class,'edit']);
  
  Route::resource('referredbysources', ReferredBySourceController::class);
  Route::post('referredbysources/create', [ReferredBySourceController::class,'create']);
  Route::get('referredbysources/delete/{id}', [ReferredBySourceController::class,'delete']);
  Route::get('referredbysources/edit/{id}', [ReferredBySourceController::class,'edit']);
  
  Route::resource('expertisetypes', ExpertiseTypesController::class);
  Route::post('expertisetypes/create', [ExpertiseTypesController::class,'create']);
  Route::get('expertisetypes/delete/{id}', [ExpertiseTypesController::class,'delete']);
  Route::get('expertisetypes/edit/{id}', [ExpertiseTypesController::class,'edit']);
  
  Route::resource('clienttags', ClientTagsController::class);
  Route::post('clienttags/create', [ClientTagsController::class,'create']);
  Route::get('clienttags/delete/{id}', [ClientTagsController::class,'delete']);
  Route::get('clienttags/edit/{id}', [ClientTagsController::class,'edit']);
  
  Route::resource('states', StatesController::class);
  Route::get('states/get/{country}', [StatesController::class, 'get']);
  
  Route::resource('employees', EmployeesController::class);
  Route::post('/employees/create', [EmployeesController::class, 'create']);
  Route::post('/employees/update', [EmployeesController::class, 'update']);
  Route::get('employees/images/{empid}', [EmployeesController::class, 'show']);
  Route::get('employees/edit/{empid}', [EmployeesController::class, 'edit']);
  Route::get('employees/displinary/{dispid}', [EmployeesController::class, 'displinary']);
  Route::get('employees/edit/{empid}/{section}', [EmployeesController::class, 'edit']);
  Route::get('employees_createform', [EmployeesController::class, 'createform']);
  Route::post('employees/delete/{id}', [EmployeesController::class, 'delete']);
  // Route::post('employees/{fieldName}/{value}', EmployeesController::class);

  Route::get('tasks/search/{fieldName}/{value}', [TasksController::class, 'search'] );
  Route::get('tasks/delete/{value}', [TasksController::class, 'delete'] );
  Route::get('/tasks/{fieldName}/{value}', TasksController::class);
  Route::post('tasks/create', [TasksController::class,'create']);
  Route::get('tasks/index', TasksController::class);
  Route::get('tasks/status/{value}', [TasksController::class, 'status']);

});
// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
