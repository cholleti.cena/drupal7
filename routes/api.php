<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users\AuthController;
use App\Http\Controllers\Users\ProfileController;


use App\Http\Controllers\Admin\AdminUserController;
use App\Http\Controllers\Admin\LeadsController;
use App\Http\Controllers\Admin\CategoriessController;
use App\Http\Controllers\admin\TasksController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('getversion', [AuthController::class, 'Getversion']);

Route::post('signUp', [AuthController::class,'SignUp']);
Route::get('getBanners', [AuthController::class, 'GetBanners']);
Route::get('getFaqs', [AuthController::class, 'GetFAQs']);
Route::get('getBlogs', [AuthController::class, 'GetBlogs']);
Route::get('getTestimonials', [AuthController::class, 'GetTestimonials']);
Route::get('getStatsCount', [AuthController::class, 'GetStatsCount']);
Route::post('metaData', [AuthController::class, 'GetMetaDataList']);

Route::post('verify', [AuthController::class, 'VerifyPhone']);
Route::post('login', [AuthController::class, 'Login']);

Route::post('postLead', [AuthController::class, 'PostLead']);

Route::middleware('auth:api')->group(function () 
{
    Route::post('logOut', [AuthController::class, 'Logout']);
    Route::post('updateProfile', [ProfileController::class, 'UpdateProfile']);
    Route::post('getUserDetails', [ProfileController::class, 'GetUserDetails']);
});

Route::post('materialDetails', [LeadsController::class, 'MaterialDetails']);

//Admin
Route::post('adminLogin', [AdminUserController::class, 'AdminLogin']);
Route::post('forgotPassword', [AdminUserController::class, 'ForgotPassword']);  
Route::middleware('auth:adminapi')->group(function () 
{
    Route::post('manageDashBoard', [AdminUserController::class, 'ManageDashBoard']);
    Route::post('manageLeads', [LeadsController::class, 'ManageLeads']);
    Route::post('manageCategory', [CategoriessController::class, 'ManageCategory']);    
});