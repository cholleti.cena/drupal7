@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Estimate Range
@endsection
@section('module')
Estimate Range
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSwitch', 'admin.components.form.switch', ['name', 'labeltext'=>null, 'value' => null, 'checkstatus' => false, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return CategoryValidate();','route' => 'facilities.store','files'=>true)) }}

<div class="card mb-4">
   <div class="card-body after-loading-icon">
     <div class="container-fluid">
      	<div class="col-md-6">
        
				{{ Form::ahText('name','Name :','',array())  }}				
				{{ Form::ahText('street','Street :','',array()) }}
				<div class="form-group" style="margin:5px">
							<label for="state_id" class="control-label col-sm-4">State :</label>
							<div class="col-sm-8">
								 <select name="state_id" data-live-search='true' class="form-control select">
                    @foreach($states as $state)
                    <option value="{{$state->id}}">{{$state->state}}</option>
                  @endforeach
                </select>
							</div>
					</div>
				{{ Form::ahText('zip','Zip :','',array()) }}
				{{ Form::ahText('fax','Fax :','',array()) }}
				{{ Form::ahText('email','Email :','',array()) }}
				{{ Form::ahText('phone','Phone :','',array()) }}
				{{ Form::ahNumber('late_fee','Late Fee :','0.0',array()) }}
				{{ Form::ahText('default_terms','Default Terms :','',array()) }}
				{{ Form::ahNumber('minimum_cost','Minimum Cost :','0.0',array()) }}
				{{ Form::ahText('terms','Terms :','',array()) }}
				{{ Form::ahSwitch('default_storage','Default Storage :',null,"") }}
		    {{ Form::ahSelect('is_active','Status :','1',array('1' => 'Active', '0' => 'Inactive')) }}
	    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('facilities.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	</div>
  </div>
</div>
@endsection