@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Estimate Range
@endsection
@section('module')
Estimate Range
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSwitch', 'admin.components.form.switch', ['name', 'labeltext'=>null, 'value' => null, 'checkstatus' => false, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return UserEditValidate();','method' => 'PUT', 'route' => array('facilities.update',$charge->id),'files'=>true)) }}
<div class="card mb-4">
	<a href="{{URL::to('admin/units/')}}?facility_id={{$charge->id}}" class="btn btn-info waves-effect waves-float waves-light"><i class="fa fa-edit"></i>Add Unit</a>
   <div class="card-body after-loading-icon">
     <div class="container-fluid">
      	<div class="col-md-6">
			<div class="col-md-6">
            {{ Form::ahText('name','Name :',$charge->name,array())  }}				
				{{ Form::ahText('street','Street :',$charge->street,array()) }}
				<div class="form-group" style="margin:5px">
							<label for="state_id" class="control-label col-sm-4">State :</label>
							<div class="col-sm-8">
								 <select name="state_id" data-live-search='true' class="form-control select">
                    @foreach($states as $state)
                    <option value="{{$state->id}}" <?php 
							                      $res = $state->id;
							                      $db_res = $charge->state_id;
							                      if($res == $db_res) echo 'selected="selected"' ?>>{{$state->state}}</option>
                  @endforeach
                </select>
							</div>
					</div>
				{{ Form::ahText('zip','Zip :',$charge->zip,array()) }}
				{{ Form::ahText('fax','Fax :',$charge->fax,array()) }}
				{{ Form::ahText('email','Email :',$charge->email,array()) }}
				{{ Form::ahText('phone','Phone :',$charge->phone,array()) }}
				{{ Form::ahNumber('late_fee','Late Fee :',$charge->late_fee,array()) }}
				{{ Form::ahText('default_terms','Default Terms :',$charge->default_terms,array()) }}
				{{ Form::ahNumber('minimum_cost','Minimum Cost :',$charge->minimum_cost,array()) }}
				{{ Form::ahText('terms','Terms :',$charge->terms,array()) }}
				{{ Form::ahSwitch('default_storage','Default Storage :',null,$charge->default_storage) }}
		        {{ Form::ahSelect('is_active','Status :',$charge->is_active,array('1' => 'Active', '0' => 'Inactive')) }}
                </br>
		    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('facilities.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	</div>
  </div>
</div>
@endsection