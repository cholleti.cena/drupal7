@extends('admin.layouts.master')
@section('title')
{{env('APP_NAME')}}-Faq
@endsection
@section('module')
Faq
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return UserEditValidate();','method' => 'PUT', 'route' => array('faq.update',$faq->id),'files'=>true)) }}
<div class="form-group form-horizontal">
        <div class="panel panel-default">
        </br>
            <div class="col-md-6">
                {{ Form::ahTextarea('question','Question :',$faq->question,array('size' => '20x3')) }}               
                {{ Form::ahTextarea('answer','Answers:',$faq->answer,array('size' => '20x3')) }}
                {{ Form::ahSelect('is_active','Status :',$faq->is_active,array('1' => 'Active', '0' => 'Inactive')) }}
                </br>
            </div>
          
        <div class="form-group">
            <div class="panel-footer">
                <div class="col-md-6 col-md-offset-3">
                    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                    {{ link_to_route('faq.index','Cancel',null, array('class' => 'btn btn-danger')) }}
                </div>
            </div>
        </div>
     </div>
 </div>
@endsection