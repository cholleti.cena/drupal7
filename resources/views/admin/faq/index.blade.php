@extends('admin.layouts.master')
@section('title')
{{env('APP_NAME')}}- Faq
@endsection
@section('module')
Faq
@endsection

@section('content')
@include('admin.components.message')	
<div class="row">
    <div class="col-md-12">
		<div class="panel panel-default">
                                <div class="panel-heading">          
                                    <div class="btn-group pull-left">
                                    @if($privileges['Add']=='true') 
                                        <a href="{{URL::to('/admin/faq/create')}}" class="btn btn-info"><i class="fa fa-edit"></i>Add Faq</a>
                                        @endif
                                    </div>
                                </div>
                                <div class="panel-body table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>Question</th>
                                                <th>Answer</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                                    
                                        @foreach($faq as $faqs)
                                    <tr>
                                        <td width="25%">
                                            {{$faqs->question}}
                                        </td>  
                                        <td>
                                            {{$faqs->answer}}
                                        </td>                                       
                                        <td>
                                            {{$faqs->status}}
                                        </td>                                       
                                        <td width="25%">
                                            <div >
                                                <div style="float:left;padding:0 10px 10px 0;">
                                                 @if($privileges['Edit']=='true')
                                                {{ link_to_route('faq.edit','Edit',array($faqs->id), array('class' => 'btn btn-info')) }}
                                                @endif 
                                                </div>
                                                <div style="float:left;padding-right:10px;">
                                                   @if($privileges['Delete']=='true')
                                                    {{ Form::open(array('onsubmit' => 'return confirm("Are you sure you want to delete?")','method' => 'DELETE', 'route' => array('faq.destroy', $faqs->id))) }}
                                                    <button type="submit" class="btn btn-danger btn-xs pull-right" style="font-size: 11px;padding: 4px 12px;">Delete</button>
                                                    {{ Form::close() }}
                                                   @endif
                                                </div>
                                            </div>
                                        </td>
                                         </tr>
                                    @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
    					</div>
    				</div> 

@endsection