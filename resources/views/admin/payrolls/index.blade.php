@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Payrolls
@endsection
@section('module')
Rooms
@endsection

@section('content')
@include('admin.components.message')	

{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}

<div class="content-wrapper container-xxl p-0">
        <div class="content-header row"></div>
        <div class="content-body">
          <section id="dashboard-ecommerce">
            <div class="">
                                <div class="btn-group pull-right">
                                    @if($privileges['Add']=='true') 
                                    <a href="{{URL::to('admin/payrolls/create')}}" class="btn btn-info"><i class="fa fa-edit"></i>Add Payrolls</a>
                                        @endif
                                </div>
              
                                <div class="table-bar2">
                                    <table id="example" class="table table-striped" style="width:100%">
                                    <thead class="thead">
                                            <tr>
                                                <th>Pay Roll Class</th>
                                                <th>Pay Roll Name</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($payrolls as $item)
                                    <tr>
                                        <td>
                                            {{$item->pay_roll_class}}
                                        </td>    
                                        <td>
                                            {{$item->pay_roll_name}}
                                        </td>                                  
                                        <td>
                                            {{$item->status}}
                                        </td>                                       
                                        <td width="25%">
                                            <div >
                                                <div style="float:left;padding:0 10px 10px 0;">
                                                 @if($privileges['Edit']=='true')
                                                {{ link_to_route('payrolls.edit','Edit',array($item->id), array('class' => 'btn btn-info')) }}
                                                @endif 
                                                </div>
                                                <div style="float:left;padding-right:10px;">
                                                   @if($privileges['Delete']=='true')
                                                    {{ Form::open(array('onsubmit' => 'return confirm("Are you sure you want to delete?")','method' => 'DELETE', 'route' => array('payrolls.destroy', $item->id))) }}
                                                    <button type="submit" class="btn btn-danger btn-xs pull-right" style="font-size: 11px;padding: 11px 12px;">Delete</button>
                                                    {{ Form::close() }}
                                                   @endif
                                                </div>
                                            </div>
                                        </td>
                                         </tr>
                                    @endforeach
                                        </tbody>
                                    </table>
                                    {!! $payrolls->render() !!}
                                </div>
                                
   
                
            </div>
          </section>
        </div>  

        <?php if(is_null($payrollcofig)) {
            $payroll_pdf_text = "";
            $payroll_mile_text = 0;
            $visible_payrollfields = [];
        }else { 
            $payroll_pdf_text = $payrollcofig->payroll_pdf_text;
            $payroll_mile_text = $payrollcofig->payroll_mile_text;
            $visible_payrollfields = json_decode($payrollcofig->visible_payroll_fields);
        }
        ?>

        <form class="auth-login-form mt-2" action="{{URL::to('admin/payrollsVerbige')}}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="card mb-4">
   <div class="card-body after-loading-icon">
     <div class="container-fluid">
        <div class="col-md-6">
                     {{ Form::ahText('payroll_pdf_text','Payroll Pdf Text :',$payroll_pdf_text,array()) }}
                    <div class="form-group" style="margin:5px">
                            <label for="payroll_mile_text" class="control-label col-sm-4">Payroll Mile Text :</label>
                            <div class="col-sm-8">
                               <select name="payroll_mile_text" data-live-search='true' class="form-control select">
                                    @foreach($payrollmiles as $payrollmile)
                                     <option value="{{$payrollmile->id}}" 
                                        <?php $res = $payrollmile->id;
                                              $db_res = $payroll_mile_text;
                                              if($res == $db_res) echo 'selected="selected"' ?>>
                                              {{$payrollmile->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                    </div>
                    <div class="form-group" style="margin:5px">
                            <label for="pay_roll_class_id" class="control-label col-sm-4">VISIBLE PAYROLL FIELDS :</label>
                            <div class="col-sm-8">
                                 <select multiple name="visible_payroll_fields[]" data-live-search='true' class="form-control select">
                                    @foreach($visible_payroll_fields as $payrollfield)
                                    <option value="{{$payrollfield->id}}" @if(isset($visible_payrollfields)) @if(in_array($payrollfield->id,$visible_payrollfields)) selected="selected" @endif @endif>{{$payrollfield->payroll_pdf_text}}</option>
                                  @endforeach
                                </select>
                            </div>
                    </div>
        </div>
        <div class="form-group">
            <div class="panel-footer">
                <div class="col-md-6 col-md-offset-3">
                    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<style type="text/css">
    .form-control 
    {
        height: 0% !important;
    }
</style>

@endsection