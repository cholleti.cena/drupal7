@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Inventory Items
@endsection
@section('module')
Inventory Items
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSwitch', 'admin.components.form.switch', ['name', 'labeltext'=>null, 'value' => null, 'checkstatus' => false, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return UserEditValidate();','method' => 'PUT', 'route' => array('payrolls.update',$charge->id),'files'=>true)) }}
<div class="card mb-4">
   <div class="card-body after-loading-icon">
     <div class="container-fluid">
      	<div class="col-md-6">
			<div class="col-md-6">
            
						<div class="form-group" style="margin:5px">
								<label for="pay_roll_class_id" class="control-label col-sm-4">Payroll Class :</label>
								<div class="col-sm-8">
									 <select name="pay_roll_class_id" data-live-search='true' class="form-control select">
	                    @foreach($categories as $category)
	                    <option value="{{$category->id}}" <?php 
							                      $res = $category->id;
							                      $db_res = $charge->id;
							                      if($res == $db_res) echo 'selected="selected"' ?>>{{$category->name}}</option>
	                  @endforeach
	                </select>
								</div>
						</div>
						{{ Form::ahText('name','Name :',$charge->name,array('maxlength' => '100'))  }}	
		        {{ Form::ahSelect('is_active','Status :',$charge->is_active,array('1' => 'Active', '0' => 'Inactive')) }}
            </br>
		    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('payrolls.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	</div>
  </div>
</div>
@endsection