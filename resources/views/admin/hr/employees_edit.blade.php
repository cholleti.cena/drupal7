@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}} - Employees Create
@endsection

@section('content')
<!-- {{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}} -->

<!-- {{ Form::open(array('onsubmit' => '','route' => 'employees.create','files'=>true)) }} -->
<style>
  input.error {
      border: solid 0.5px red;
  }
  select.error {
      border: solid 0.5px red;
  }

    a:hover,
    a:focus{
        text-decoration: none;
        outline: none;
    }

    /* Style the tab */
.tab {
  float: left;
  border: 1px solid #ccc;
  background-color: #fff;
  width: 100%;
  /* height: 300px; */
}

/* Style the buttons inside the tab */
.tab a {
  display: block;
  background-color: inherit;
  color: black;
  padding: 12px 8px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 14px;
  border-top: 1px solid #ccc;
}
.tab a:first-child {
  border-top: 0px solid #ccc;
}

/* Change background color of buttons on hover */
.tab a:hover {
  background-color: #277cbb;
  color: white
}

/* Create an active/current "tab button" class */
.tab a.active {
    background-color: #277cbb;
    color: white
}

/* Style the tab content */
.tabcontent {
  float: left;
  padding: 0px 12px;
  /* border: 1px solid #ccc; */
  width: 100%;
  border-left: none;
  /* height: 300px; */
}
</style>
<!-- Start Chat Related styles -->
<style>
    *{
        box-sizing:border-box;
    }
    body{
        background-color:#abd9e9;
        font-family:Arial;
    }
    #container{
        /* width:750px; */
        height:800px;
        background:#eff3f7;
        margin:0 auto;
        font-size:0;
        border-radius:5px;
        overflow:hidden;
    }
    aside{
        /* width:300px; */
        height:800px;
        background-color:#3b3e49;
        display:inline-block;
        font-size:15px;
        vertical-align:top;
    }
    main{
        /* width:100%; */
        height:800px;
        display:inline-block;
        font-size:15px;
        vertical-align:top;
    }
    .smsMain{
        width:100%;
    }
    aside header{
        padding:30px 20px;
    }
    aside input{
        width:100%;
        height:50px;
        line-height:50px;
        padding:0 50px 0 20px;
        background-color:#5e616a;
        border:none;
        border-radius:3px;
        color:#fff;
        background-image:url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/ico_search.png);
        background-repeat:no-repeat;
        background-position:170px;
        background-size:40px;
    }
    aside input::placeholder{
        color:#fff;
    }
    aside ul{
        padding-left:0;
        margin:0;
        list-style-type:none;
        overflow-y:scroll;
        height:690px;
    }
    aside li{
        padding:10px 0;
    }
    aside li:hover{
        background-color:#5e616a;
    }
    h2,h3{
        margin:0;
    }
    aside li img{
        border-radius:50%;
        margin-left:20px;
        margin-right:8px;
    }
    aside li div{
        display:inline-block;
        vertical-align:top;
        margin-top:12px;
    }
    aside li h2{
        font-size:14px;
        color:#fff;
        font-weight:normal;
        margin-bottom:5px;
    }
    aside li h3{
        font-size:12px;
        color:#7e818a;
        font-weight:normal;
    }

    .status{
        width:8px;
        height:8px;
        border-radius:50%;
        display:inline-block;
        margin-right:7px;
    }
    .green{
        background-color:#58b666;
    }
    .orange{
        background-color:#ff725d;
    }
    .blue{
        background-color:#6fbced;
        margin-right:0;
        margin-left:7px;
    }

    main header{
        height:110px;
        padding:30px 20px 30px 40px;
    }
    main header > *{
        display:inline-block;
        vertical-align:top;
    }
    main header img:first-child{
        border-radius:50%;
    }
    main header img:last-child{
        width:24px;
        margin-top:8px;
    }
    main header div{
        margin-left:10px;
        margin-right:145px;
    }
    main header h2{
        font-size:16px;
        margin-bottom:5px;
    }
    main header h3{
        font-size:14px;
        font-weight:normal;
        color:#7e818a;
    }

    #chat{
        padding-left:0;
        margin:0;
        list-style-type:none;
        overflow-y:scroll;
        height:535px;
        border-top:2px solid #fff;
        border-bottom:2px solid #fff;
    }
    #chat li{
        padding:10px 30px;
    }
    #chat h2,#chat h3{
        display:inline-block;
        font-size:13px;
        font-weight:normal;
    }
    #chat h3{
        color:#bbb;
    }
    #chat .entete{
        margin-bottom:5px;
    }
    #chat .message{
        padding:20px;
        color:#fff;
        line-height:25px;
        max-width:90%;
        display:inline-block;
        text-align:left;
        border-radius:5px;
    }
    #chat .me{
        text-align:right;
    }
    #chat .you .message{
        background-color:#58b666;
    }
    #chat .me .message{
        background-color:#6fbced;
    }
    #chat .triangle{
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 0 10px 10px 10px;
    }
    #chat .you .triangle{
            border-color: transparent transparent #58b666 transparent;
            margin-left:15px;
    }
    #chat .me .triangle{
            border-color: transparent transparent #6fbced transparent;
            margin-left:95%;
    }

    main footer{
        height:155px;
        padding:20px 30px 10px 20px;
    }
    main footer textarea{
        resize:none;
        border:none;
        display:block;
        width:100%;
        height:80px;
        border-radius:3px;
        padding:20px;
        font-size:13px;
        margin-bottom:13px;
    }
    main footer textarea::placeholder{
        color:#ddd;
    }
    main footer img{
        height:30px;
        cursor:pointer;
    }
    main footer a{
        text-decoration:none;
        text-transform:uppercase;
        font-weight:bold;
        color:#6fbced;
        vertical-align:top;
        margin-left:333px;
        margin-top:5px;
        display:inline-block;
    }
</style>
<!-- End Chat Related styles -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js"  ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>

<!-- Create Emp Page -->
<div class="content-wrapper container-xxl p-0">
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
           
            
            <div class="table-bar2 p-0  ">
                <strong class="p-2 pt-2">Admin / HR / Edit Employees</strong>   
            <!-- </div>
            
                <div class="table-bar2"> -->
                    @if($errors->any())
                        @foreach($errors->all() as $err)
                            <li>{{$err}}</li>
                        @endforeach
                    @endif
                    @include('admin.components.message')
                    <!--  Starts here  -->
                     
                      <!-- <form name="createEmpForm" id="createEmpForm" method="post" action="admin/employees/store"> -->
                      <!-- <form name="createEmpForm" id="createEmpForm" method="post" action="admin/employees/store"> -->
                          <input id='record_id' name="record_id" type="hidden" value="{{$emp->id}}" />
                          <input id='tabName' name="tabName" type="hidden" />

                          <div class="containers mt-1">
                              <div class="row m-0">
                                    <div class='col-12' style='margin-left: -4px;'>
                                        <div class="containers">
                                            <div class="row">
                                                <div class='col-sm-2 p-0'>
                                                    <div class="tab">
                                                        <a class="tablinks active" onclick="openEmployeeTabs(event, 'Identity')" id="defaultOpen">Identity</a>
                                                        <a class="tablinks" onclick="openEmployeeTabs(event, 'Contact_Information')">Contact Information</a>
                                                        <a class="tablinks" onclick="openEmployeeTabs(event, 'Employment_Details')">Employment Details</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Review')">Review</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Disciplinary')">Disciplinary</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Scheduling')">Scheduling</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Payroll')">Payroll</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Login_Permissions')">Login &amp; Permissions</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Expertise')">Expertise</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Audit_Trail')">Audit Trail</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Assigned_Prospects')">Assigned Prospects</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Assigned_Customers')">Assigned Customers</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Outstanding_Tasks')">Outstanding Tasks</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Assigned_Tasks')">Assigned Tasks</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Completed_Tasks')">Completed Tasks</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Events')">Events</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'Chat_History')">Chat History</a>
                                                        <a class=" tablinks" onclick="openEmployeeTabs(event, 'SMS_Messaging')">SMS Messaging</a>

                                                    </div>
                                                </div>
                                                <div class='col-sm-10 p-0'>
                                                    <div id="Identity" class="tabcontent col-sm-9">
                                                        <div class="row">
                                                            <h3>Identity</h3>
                                                            <div class="row col-sm-8">
                                                                <div class="row">
                                                                    <div class="col">First Name </div>
                                                                    <div class="col"> <input class="form-control ui-autocomplete-input required" value="{{$emp->first_name}}" type="text" name="first_name" id="first_name" autocomplete="off">
                                                                        <label for="first_name"></label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <label class="control-label">Middle Name</label>
                                                                    </div>
                                                                    <div class="col">
                                                                        <input class="form-control ui-autocomplete-input required" value="{{$emp->middle_name}}"  type="text" name="middle_name" id="middle_name" autocomplete="off">
                                                                        <label for="middle_name"></label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <label class="control-label">Last Name</label>
                                                                    </div>
                                                                    <div class="col">
                                                                        <input class="form-control ui-autocomplete-input required"  value="{{$emp->last_name}}"  type="text" name="last_name" id="last_name" autocomplete="off">
                                                                        <label for="last_name"></label>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <label class="control-label">Nick Name</label>
                                                                    </div>
                                                                    <div class="col">
                                                                        <input class="form-control ui-autocomplete-input required"  value="{{$emp->nick_name}}"  type="text" name="nick_name" id="nick_name" autocomplete="off">
                                                                        <label for="nick_name"></label>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <label class="control-label">User Name</label>
                                                                    </div>
                                                                    <div class="col">
                                                                        <input class="form-control ui-autocomplete-input required"  value="{{$emp->user_name}}"  type="text" name="user_name" id="user_name" autocomplete="off">
                                                                        <label for="user_name"></label>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col">
                                                                        <label class="control-label">Employee Designation</label>
                                                                    </div>
                                                                    <div class="col"> 
                                                                        <select name='designation' id="designation"  class="form-select ui-autocomplete-input required">
                                                                            <option value=''>Select</option>
                                                                            <option {{$emp->designation=='Driver'? 'selected': '' }} value='Office' >Office</option>
                                                                            <option {{$emp->designation=='Driver'? 'selected': '' }} value='Driver' >Driver</option>
                                                                            <option {{$emp->designation=='Helper'? 'selected': '' }} value='Helper' >Helper</option>
                                                                            <option {{$emp->designation=='Hide'? 'selected': '' }} value='Hide' >Hide</option>
                                                                            <option {{$emp->designation=='Crew_Lead'? 'selected': '' }} value='Crew_Lead' >Crew Lead</option>
                                                                        </select>
                                                                        <label for="designation"></label>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col">
                                                                        <label class="control-label">Time Zone</label>
                                                                    </div>
                                                                    <div class="col">
                                                                        <select name='time_zone' id="time_zone"  class="form-select ui-autocomplete-input required">
                                                                            <option value=''>Select Timezone</option>
                                                                            @foreach($tzs as $tz)
                                                                            <option  {{$emp->time_zone==$tz->value? 'selected': '' }} value="{{$tz->value}}">{{$tz->label}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        <label for="time_zone"></label>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col">
                                                                        <label class="control-label">S.S.N</label>
                                                                    </div>
                                                                    <div class="col">
                                                                        <input class="form-control ui-autocomplete-input required" value="{{$emp->ssn}}"  type="text" name="ssn" id="ssn" autocomplete="off">
                                                                        <label for="ssn"></label>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col">
                                                                        <label class="control-label">Date of Birth</label>
                                                                    </div>
                                                                    <div class="col">
                                                                        <?php $date_dob = explode('-',$emp->date_of_birth);  ?>
                                                                            <input class="form-control ui-autocomplete-input datepicker required" value="{{$date_dob[1].'/'.$date_dob[2].'/'.$date_dob[0]}}"  type="text" name="date_of_birth" id="date_of_birth" autocomplete="off">
                                                                            <label for="date_of_birth"></label>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col">
                                                                        <label class="control-label">Gender</label>
                                                                    </div>
                                                                    <div class="col">
                                                                        <select name='gender' id="gender"  class="form-select ui-autocomplete-input required">
                                                                            <option value=''>Select</option>
                                                                            <option {{$emp->gender=='Male'? 'selected': '' }} value='Male'>Male</option>
                                                                            <option {{$emp->gender=='Female'? 'selected': '' }} value='Female'>Female</option>
                                                                        </select>
                                                                        <label for="gender"></label>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col">
                                                                        <label class="control-label">Relationship</label>
                                                                    </div>
                                                                    <div class="col">
                                                                        <select name='relationship' id="relationship"  class="form-select ui-autocomplete-input required">
                                                                            <option value=''>Select</option>
                                                                            <option {{$emp->relationship=='Single'? 'selected': '' }} value='Single'>Single</option>
                                                                            <option {{$emp->relationship=='Married'? 'selected': '' }} value='Married'>Married</option>
                                                                            <option {{$emp->relationship=='Divorced'? 'selected': '' }} value='Divorced'>Divorced</option>
                                                                            <option {{$emp->relationship=='Widowed'? 'selected': '' }} value='Widowed'>Widowed</option>
                                                                        </select>
                                                                        <label for="relationship"></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div>
                                                                    <label class="control-label">Upload Photo</label>
                                                                    <img src="{{$emp->profile_pic}}"  style="width:50%; height: 50%"/>
                                                                    <input class="form-control ui-autocomplete-input required" type="file" name="employee_photo" id="employee_photo" autocomplete="off">
                                                                    <label for="employee_photo"></label>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <label class="control-label">Signature</label>
                                                                        <img src="{{$emp->signature_pic}}" style="width:150px; height: 100" />
                                                                        <input class="form-control ui-autocomplete-input required" type="file" name="employee_signature" id="employee_signature" autocomplete="off">
                                                                        <label for="employee_signature"></label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <label class="control-label">Get to know</label>
                                                                        <textarea class="form-control h-50" rows="7" cols="" name="get_to_know" id="get_to_know">{{$emp->get_to_know}}</textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <label class="control-label">Bio Graphy</label>
                                                                        <textarea class="form-control h-50" rows="7" cols="" name="biography" id="biography">{{$emp->biography}}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="Contact_Information" class="tabcontent row col-sm-9">
                                                        <div class="row">
                                                            <h3>Contact Information</h3>
                                                            <div class="col-sm-8 result">&nbsp;</div>
                                                            <div class="col-sm-4">
                                                                    <div>
                                                                        <label class="control-label">Upload Photo</label>
                                                                        <img src="{{$emp->profile_pic}}"  style="width:50%; height: 50%"/>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            <label class="control-label">Signature</label>
                                                                            <img src="{{$emp->signature_pic}}" style="width:150px; height: 100" />
                                                                        </div>
                                                                    </div>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div id="Employment_Details" class="tabcontent row col-sm-9">
                                                        <h3>Employment Details</h3>
                                                        <div class="row col-sm-12 result"></div> 
                                                    </div>
                                                    <div id="Review" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">Client Review</h3>
                                                            <span  class="col-sm-5" id="overallrating"></span>
                                                        </div>
                                                        <div class="row col-sm-12 result"></div> 
                                                    </div>
                                                    <div id="Disciplinary" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">Disciplinary</h3>
                                                            <span  class="col-sm-5 btn btn-primary" id="newDisciplinary" onClick="loadModal('displynryNote', 'newDisciplinary', 'closeNewDisp')">Create New Note </span>
                                                        </div>
                                                        <div class="row col-sm-12 result"></div> 
                                                    </div>
                                                    <div id="Payroll" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">Payroll</h3>
                                                        </div>
                                                        <div class="row col-sm-12 result"></div> 
                                                    </div>
                                                    <div id="Login_Permissions" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">Login & Permissions</h3>
                                                        </div>
                                                        <div class="row col-sm-12 result"></div> 
                                                    </div>
                                                    <div id="Assigned_Tasks" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">Assigned Tasks</h3>
                                                        </div>
                                                        <div class="row col-sm-12 result">
                                                            <div class="tableGrid">
                                                                <table id="assignedTasks" class="table table-striped datatable">
                                                                </table>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div id="Completed_Tasks" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">Completed Tasks</h3>
                                                        </div>
                                                        <div class="row col-sm-12 result">
                                                            <div class="tableGrid">
                                                                <table id="completedTasks" class="table table-striped datatable">
                                                                </table>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div id="Outstanding_Tasks" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">Outstanding Tasks</h3>
                                                        </div>
                                                        <div class="row col-sm-12 result">
                                                            <div class="tableGrid">
                                                                <table id="outstandingTasks" class="table table-striped datatable">
                                                                </table>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div id="Expertise" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">Expertise</h3>
                                                        </div>
                                                        <div class="row col-sm-12 result">
                                                             
                                                        </div> 
                                                    </div>
                                                    
                                                    <div id="Audit_Trail" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">Audit Trail</h3>
                                                        </div>
                                                        <div class=" row col-sm-9">
                                                            <div class="row">
                                                                <input type='text' class="form-control col-sm-2 " name="fromdate_audit" id="fromdate_audit"/><label class='col-sm-2'>To</label>
                                                                <input type='text' class=" form-control col-sm-2"  name="todate_audit" id="todate_audit"/>
                                                                <input type='button'  class="col-sm-2 btn btn-primary"  value="View" id="viewAudit" name="viewAudit"/>
                                                            </div>
                                                        </div>
                                                        <div class="row col-sm-12 result">
                                                             
                                                        </div> 
                                                    </div>
                                                    <div id="Assigned_Prospects" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">Assigned Prospects</h3>
                                                        </div>
                                                        <div class="row col-sm-12 result">
                                                             
                                                        </div> 
                                                    </div>
                                                    <div id="Assigned_Customers" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">Assigned Customers</h3>
                                                        </div>
                                                        <div class="row col-sm-12 result">
                                                             
                                                        </div> 
                                                    </div>
                                                    <div id="Events" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">Events</h3>
                                                        </div>
                                                        <div class="row col-sm-12 result">
                                                             
                                                        </div> 
                                                    </div>
                                                    <div id="Chat_History" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">Chat History</h3>
                                                        </div>
                                                        <div class="row col-sm-12 result">
                                                             
                                                        </div> 
                                                    </div>
                                                    <div id="SMS_Messaging" class="tabcontent row col-sm-9">
                                                        <div class=" row col-sm-9">
                                                            <h3 class="col-sm-7">SMS Messaging</h3>
                                                        </div>
                                                        <div class="row col-sm-12 result">
                                                             
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        

                                    
                                    </div>
                                  <!-- HTML Upload -->
                                  <!-- <div class=' col-3'>
                                        
                                  </div> -->
                                  <div class="row form-group mb-5"  id="saveEdit">
                                      <div class="col col-md text-center">
                                      <input type="button" id="resetpassword" value="Reset Password " class="pull-right btn btn-default" />
                                      <input type="submit" id="employeeCreate" value="Save " class="pull-right btn btn-primary" />
                                      <input type="button" value="Cancel" onClick="hidePopUp()" class="pull-left btn btn-danger" />
                                      </div>
                                  </div>
                              </div>
                          </div>
                       
                  <!-- </form> -->
                    <!--  ends here  -->
            </div>
                 
            
        </section>
    </div>
</div>

<!-- Ends Emp Page -->
<!-- Starts Modal content -->
<div id="displynryNote" class="modal">
    <div class="modal-content" style="display:block">
        <!-- <div class="row"></div> -->
        <div>
            <strong id="popuptitle">Employee Notes</strong>
            <span class="closeNewDisp close pull-right" onclick="hideModa('displynryNote')">&times;</span>
        </div>
        <hr/>
        <div class="row">
                <input id='discipnary_record_id' name="discipnary_record_id" type="hidden" />
                <div class="container">
                    <div class="col">
                        <div class="row form-group">
                            <div class="col col-sm-4">
                                <label class="control-label" for="task_contact_person_id">Date</label>
                            </div>
                            <div class="col col-sm-4">
                                <input class="form-control ui-autocomplete-input  col-sm-4 required" type="text" name="discipnary_date" id="discipnary_date" autocomplete="off">
                                <label for="discipnary_date"></label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-sm-4">
                                <label class="control-label" for="task_contact_person_id">Customer</label>
                            </div>
                            <div class="col col-sm-4">
                                <input class="form-control ui-autocomplete-input  col-sm-4 required" type="text" name="discipnary_customer" id="discipnary_customer" autocomplete="off">
                                <label for="discipnary_customer"></label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-sm-4">
                                <label class="control-label" for="task_contact_person_id">Category</label>
                            </div>
                            <div class="col col-sm-4">
                                <select class="form-select form-control ui-autocomplete-input  col-sm-4 required" name="discipnary_category" id="discipnary_category" ><option value="Attendance">Attendance</option>
                                    <option value="Attitude">Attitude</option>
                                    <option value="Complaint">Complaint</option>
                                    <option value="Uniform">Uniform</option>
                                    <option value="Misconduct">Misconduct</option>
                                    <option value="Other">Other</option>
                                </select>
                                <label for="discipnary_category"></label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-sm-4">
                                <label class="control-label" for="task_contact_person_id">Issue</label>
                            </div>
                            <div class="col col-sm-4">
                                <input class="form-control ui-autocomplete-input  col-sm-4 required" type="text" name="discipnary_issue" id="discipnary_issue" autocomplete="off">
                                <label for="discipnary_issue"></label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-sm-4">
                                <label class="control-label" for="task_contact_person_id">Notes</label>
                            </div>
                            <div class="col col-sm-4">
                                <input class="form-control ui-autocomplete-input  col-sm-4 required" type="text" name="discipnary_notes" id="discipnary_notes" autocomplete="off">
                                <label for="discipnary_notes"></label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md text-center">
                            <input type="button" id="createDisplinary" value="Create" class="pull-right btn btn-primary" />
                            <!-- </div>
                            <div class="col col-md"> -->
                            <input type="button" value="Cancel" onClick="hidePopUp()" class="pull-left btn btn-danger" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="payrollLog" class="modal">
    <div class="modal-content" style="display:block">
        <!-- <div class="row"></div> -->
        <div>
            <strong id="popuptitle">Log Event</strong>
            <span class="closePayroll close pull-right" onclick="hideModa('payrollLog')">&times;</span>
        </div>
        <hr/>
        <div class="row">
                <input id='payrollLog_record_id' name="payrollLog_record_id" type="hidden" />
                <div class="container">
                    <div class="col">
                        <div class="row form-group">
                            <div class="col col-sm-4">
                                <label class="control-label" for="payrollLog_class_select_label">Payroll Calss</label>
                            </div>
                            <div class="col col-sm-4">
                                <select class="form-select form-control ui-autocomplete-input  col-sm-4 required" onchange="showPayrollOptions()" name="payrollLog_class_select" id="payrollLog_class_select" >
                                    <option value="">Select</option>
                                    <option value="moving">Moving/Packing</option>
                                    <option value="miscHours">Misc Hours</option>
                                    <option value="tip">Tip</option>
                                    <option value="bonus">Bonus</option>
                                    <option value="deduction">Deduction</option>
                                    <option value="perdiem">Perdiem</option>
                                </select>
                                <label for="payrollLog_class_select"></label>
                            </div>
                        </div>
                        <div id="payrollLogCLassView" style="display:none">
                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label class="control-label" for="label_payrollLog_company_branch">Company Branch</label>
                                </div>
                                <div class="col col-sm-4">
                                    <select class="form-select form-control ui-autocomplete-input  col-sm-4 required" name="payrollLog_company_branch" id="payrollLog_company_branch" >
                                        <option value="">Select</option>
                                        <option value="BigHillMovers">Big Hill Movers</option>
                                    </select>
                                    <label for="payrollLog_company_branch"></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label class="control-label" for="label_payrollLog_entry_date">Entry Date</label>
                                </div>
                                <div class="col col-sm-4">
                                    <input class="form-control ui-autocomplete-input  col-sm-4 required" readonly type="text" name="payrollLog_entry_date" id="payrollLog_entry_date" autocomplete="off">
                                    <label for="payrollLog_entry_date"></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label class="control-label" for="label_payrollLog_class">Class</label>
                                </div>
                                <div class="col col-sm-4">
                                    <select class="form-select form-control ui-autocomplete-input  col-sm-4 required" name="payrollLog_class" id="payrollLog_class" >
                                        <option value="">Select</option>
                                    </select>
                                    <label for="payrollLog_class"></label>
                                </div>
                            </div>
                            <div class="row form-group" id="label_payroll_customer_div" style="display:none">
                                <div class="col col-sm-4">
                                    <label class="control-label" for="label_payrollLog_customer">Customer</label>
                                </div>
                                <div class="col col-sm-4">
                                    <input class="form-control ui-autocomplete-input  col-sm-4 required" type="text" name="payrollLog_customer" id="payrollLog_customer" autocomplete="off">
                                    <label for="payrollLog_customer"></label>
                                </div>
                            </div>
                            <div class="row form-group" id="class_hourly_rate_div" style="display:none;">
                                <div class="col col-sm-4">
                                    <label class="control-label" for="label_payrollLog_hourly_rate">Hourly Rate</label>
                                </div>
                                <div class="col col-sm-4">
                                    <input class="form-control ui-autocomplete-input  col-sm-4 required" type="text" name="payrollLog_hourly_rate" id="payrollLog_hourly_rate" autocomplete="off">
                                    <label for="payrollLog_hourly_rate"></label>
                                </div>
                            </div>
                            <div class="row form-group" id="class_hours_div" style="display:none;">
                                <div class="col col-sm-4">
                                    <label class="control-label" for="label_payrollLog_hours">Hours</label>
                                </div>
                                <div class="col col-sm-4">
                                    <input class="form-control ui-autocomplete-input  col-sm-4 required" type="text" name="payrollLog_hours" id="payrollLog_hours" autocomplete="off">
                                    <label for="payrollLog_hours"></label>
                                </div>
                            </div>
                            <div class="row form-group" id="class_travel_div" style="display:none;">
                                <div class="col col-sm-4">
                                    <label class="control-label" for="label_payrollLog_travel">Travel</label>
                                </div>
                                <div class="col col-sm-4">
                                    <input class="form-control ui-autocomplete-input  col-sm-4 required" type="text" name="payrollLog_travel" id="payrollLog_travel" autocomplete="off">
                                    <label for="payrollLog_travel"></label>
                                </div>
                            </div>
                            <div class="row form-group" id="class_commission_div" style="display:none;">
                                <div class="col col-sm-4">
                                    <label class="control-label" for="label_payrollLog_commission">Commission</label>
                                </div>
                                <div class="col col-sm-4">
                                    <input class="form-control ui-autocomplete-input  col-sm-4 required" type="text" name="payrollLog_commission" id="payrollLog_commission" autocomplete="off">
                                    <label for="payrollLog_commission"></label>
                                </div>
                            </div>
                            <div class="row form-group" id="class_amount_div" style="display:none;">
                                <div class="col col-sm-4">
                                    <label class="control-label" id="class_amount" for="label_payrollLog_tip_amount">Tip Amount</label>
                                </div>
                                <div class="col col-sm-4">
                                    <input class="form-control ui-autocomplete-input  col-sm-4 required" type="text" name="payrollLog_tip_amount" id="payrollLog_tip_amount" autocomplete="off">
                                    <label for="payrollLog_tip_amount"></label>
                                </div>
                            </div>
                            <div class="row form-group" id="class_desc_div" style="display:none;">
                                <div class="col col-sm-4">
                                    <label class="control-label" for="label_payrollLog_description">Description</label>
                                </div>
                                <div class="col col-sm-4">
                                    <input class="form-control ui-autocomplete-input  col-sm-4 required" type="text" name="payrollLog_description" id="payrollLog_description" autocomplete="off">
                                    <label for="payrollLog_description"></label>
                                </div>
                            </div>
                            
                            <!-- <select class="form-select form-control ui-autocomplete-input  col-sm-4 required" name="payrollLog_category" id="payrollLog_category" ><option value="Attendance">Attendance</option>
                                <option value="Attitude">Attitude</option>
                                <option value="Complaint">Complaint</option>
                                <option value="Uniform">Uniform</option>
                                <option value="Misconduct">Misconduct</option>
                                <option value="Other">Other</option>
                            </select> -->
                        </div>
                        
                        
                        <div class="row form-group">
                            <div class="col col-md text-center">
                            <input type="button" id="createPayroll" value="Create" class="pull-right btn btn-primary" />
                            <!-- </div>
                            <div class="col col-md"> -->
                            <input type="button" value="Cancel" onClick="hidePopUp()" class="pull-left btn btn-danger" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Ends Modal -->
<script>

/**
 * 
 * Modal Pop Up code
 */
// Get the modal
function loadModal(mdalName, buttonId, className){
    $("#"+ mdalName).show();
    $('#discipnary_date').datepicker({
        format: 'mm/dd/yyyy',
        startDate: 'd',
        maxDate: '120',
        changeMonth: true,
        changeYear: true,
    });
    $('#payrollLog_entry_date').datepicker({
        format: 'mm/dd/yyyy',
        startDate: 'd',
        maxDate: '120',
        changeMonth: true,
        changeYear: true,
    });
    
} 

 function hideModa(mdalName){
     $("#"+mdalName).hide();
 }

$("#createDisplinary").click(function(){
    var tabName = $("#tabName").val();
    updateObj = {
            discipnary_date: $("#discipnary_date").val(),
            discipnary_customer: $("#discipnary_customer").val(),
            discipnary_category: $("#discipnary_category").val(),
            discipnary_issue: $("#discipnary_issue").val(),
            discipnary_notes: $("#discipnary_notes").val(),
            discipnary_record_id: $("#discipnary_record_id").val(),
            emp_id: $("#record_id").val(),
            "_token": "{{ csrf_token() }}",
            tabName
        }
        $.post( '/admin/employees/update', updateObj)
            .done(function( result ) {
                alert(result.data)
                hideModa('Disciplinary');
                getEmployeeInfo('Disciplinary')
        });
 });

 $("#viewAudit").click(function(){
     var updateObj;

     updateObj = {
         fromDate: $("#fromdate_audit").val(),
         toDate: $('#todate_audit').val(),
         "_token": "{{ csrf_token() }}",
            tabName: $("#tabName").val()
     }

    $.post( '/admin/employees/update', updateObj)
            .done(function( result ) {
                renderAuditTrailDetails(result.data)
                // alert(result.data)
                // hideModa('Disciplinary');
                // getEmployeeInfo('Disciplinary')
        });
 })
/**
 * End Pop up modal pop up
 */

function showPayrollOptions(){

    $("#payrollLogCLassView").show();
    var pc = $("#payrollLog_class_select").val();
    var classHtml = "<option value=''>Select</option>";

    if(pc == "moving"){
        classHtml += "<option value='packing'>Packing</option>";
        classHtml += "<option value='moving'>Moving</option>";
        $("#class_amount_div").hide();
        $("#class_desc_div").hide();
        $("#class_commission_div").show();
        $("#payrollLog_commission").val('');
        $("#payrollLog_description").val('');
        $("#payrollLog_hours").val('');
         $("#class_hours_div").show();
         $("#class_travel_div").show();
        $("#payrollLog_travel").val('');
        $("#label_payroll_customer_div").show();
        $("#payrollLog_customer").val('');
        $("#class_amount_div").hide();
        $("#payrollLog_tip_amount").val('');
    } else if(pc == "miscHours"){
        classHtml += "<option value='warhouse'>Warehouse</option>";
        classHtml += "<option value='delivery'>Delivery</option>";
        classHtml += "<option value='office'>Office</option>";
        $("#class_amount").html('Tip Amount');
        $("#class_amount_div").show();
        $("#class_desc_div").show();
        $("#class_hourly_rate_div").show();
        $("#class_commission_div").hide();
        $("#label_payroll_customer_div").hide();
        $("#payrollLog_customer").val('');
        $("#payrollLog_hours").val('');
        $("#payrollLog_hourly_rate").val('');
        $("#class_hours_div").show();
        $("#class_travel_div").hide();
        $("#payrollLog_travel").val('');
        $("#class_amount_div").hide();
        $("#payrollLog_tip_amount").val('');
    } else if(pc == "tip"){
        classHtml += "<option value='tips'>Tips</option>";
        $("#class_amount_div").show();
        $("#class_amount").html('Tip Amount');
        $("#class_commission_div").hide();
        $("#class_hours_div").hide();
        $("#class_desc_div").hide();
        $("#class_travel_div").hide();
        $("#class_hourly_rate_div").hide();
        $("#payrollLog_description").val('');
        $("#payrollLog_hourly_rate").val('');
        $("#payrollLog_hours").val('');
        $("#payrollLog_travel").val('');
        $("#label_payroll_customer_div").show();
        $("#payrollLog_customer").val('');
        
    } else if(pc == "bonus"){
        classHtml += "<option value='bonus'>Bonus</option>";
        $("#class_amount").html('Bonus Amount');
        $("#class_amount_div").show();
        $("#class_desc_div").show();
        $("#class_commission_div").hide();
        $("#class_hours_div").hide();
        $("#class_travel_div").hide();
        $("#payrollLog_travel").val('');
    } else if(pc == "deduction"){
        classHtml += "<option value='latetojob'>Late to Job</option>";
        classHtml += "<option value='uniforms'>Uniforms</option>";
        classHtml += "<option value='servicefee'>Service Fee</option>";
        $("#class_amount").html('Deduction Amount');
        $("#class_amount_div").show();
        $("#class_commission_div").hide();
        $("#class_desc_div").show();
        $("#payrollLog_hours").val('');
        $("#class_hours_div").hide();
        $("#class_travel_div").hide();
        $("#payrollLog_travel").val('');
    } else if(pc == "perdiem"){
        classHtml += "<option value='perdiem'>PerDiem</option>";
        $("#class_amount").html('PerDiem Amount');
        $("#class_amount_div").show();
        $("#class_desc_div").show();
        $("#class_commission_div").hide();
        $("#payrollLog_hours").val('');
        $("#class_hours_div").hide();
        $("#class_travel_div").hide();
        $("#payrollLog_travel").val('');
    }

    $("#payrollLog_class").html(classHtml);

}

var selectedEmpTab = 'Identity';
function openEmployeeTabs(evt, selectedEmpTab) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(selectedEmpTab).style.display = "block";
  evt.currentTarget.className += " active";
  $("#tabName").val(selectedEmpTab);
  if(selectedEmpTab != 'Identity')
    getEmployeeInfo(selectedEmpTab);
    var notRequiredSaveButton = ['Payroll', 'Audit_Trail', 'Assigned_Prospects', 'Assigned_Customers', 'Events', 'Chat_History', 'SMS_Messaging']
    if(notRequiredSaveButton.indexOf(selectedEmpTab) > -1){
        $("#saveEdit").hide();
    } else {
        $("#saveEdit").show();
    }
}

function getEmployeeInfo(selectedEmpTab){
    var rId = $("#record_id").val();
    console.log(selectedEmpTab)
    if(selectedEmpTab != "Assigned_Tasks" && selectedEmpTab != "Completed_Tasks"  && selectedEmpTab != "Outstanding_Tasks"   && selectedEmpTab != "Audit_Trail"){
        $.get(`/admin/employees/edit/${rId}/${selectedEmpTab}`, function(res) {
            if(res && res.data){
                var renderHTML = '';
                switch(selectedEmpTab){
                    case 'Contact_Information':
                        renderHTML =   renderPhoneNumbers(res.data.phoneNumbers);
                        renderHTML +=   renderEmailIds(res.data.emails);
                        renderHTML +=  renderAddress(res.data.address, res.data.states);
                        break;
                    case 'Employment_Details':
                        renderHTML =  renderEmpDetails(res.data);
                        break;
                    case 'Review':
                        renderHTML =  renderEmpClientReviewDetails(res.data);
                        break;
                    case "Disciplinary":
                        renderHTML =  renderDisciplinaryDetails(res.data);
                        break;
                    case 'Login_Permissions':
                        renderHTML =  renderLogin_Permissions(res.data.Login_Permissions[0]);
                        break;
                    case 'Payroll':
                        renderHTML =  renderPayroll(res.data);
                        break;
                    case 'Expertise':
                        renderHTML =  renderExpertise(res.data);
                        break;
                    case 'Assigned_Prospects':
                        renderHTML =  renderAssigned_Prospects(res.data);
                        break;
                    case 'Assigned_Customers':
                        renderHTML =  renderAssigned_Customers(res.data);
                        break;
                    case 'Events':
                        renderHTML =  renderEvents(res.data);
                        break;
                    case 'Chat_History':
                        renderHTML =  renderChat_History(res.data);
                        break;
                    case 'SMS_Messaging':
                        renderHTML =  renderSMS_Messaging(res.data);
                        break;
                    default :

                    break;
                }
                $("#tabName").val(selectedEmpTab);
                $("#"+selectedEmpTab+'  div.result').html(renderHTML);

                if(selectedEmpTab == 'Login_Permissions'){
                    $('#groups_status').multiselect({
                        columns: 1,
                        placeholder: 'Select Group'
                    });
                }
                if(selectedEmpTab == 'Employment_Details'){
                    $('#hire_date, #termination_date').datepicker({
                        format: 'mm/dd/yyyy',
                        startDate: 'd',
                        maxDate: '0',
                        changeMonth: true,
                        changeYear: true,
                    });
                }
            }
        })
    } else if(selectedEmpTab == 'Audit_Trail') {
        $('#fromdate_audit').datepicker({
        format: 'mm/dd/yyyy',
        startDate: 'd',
        maxDate: '120',
        changeMonth: true,
        changeYear: true,
        });
        $('#todate_audit').datepicker({
            format: 'mm/dd/yyyy',
            startDate: 'd',
            maxDate: '120',
            changeMonth: true,
            changeYear: true,
        });
    } else {
        switch(selectedEmpTab){
            case 'Assigned_Tasks':
                renderAssigned_Tasks(selectedEmpTab);
            break;
            case 'Completed_Tasks':
                renderCompleted_Tasks(selectedEmpTab);
            break;
            case 'Outstanding_Tasks':
                renderOutstanding_Tasks(selectedEmpTab);
            break;
            
            default :

            break;
        }
    }
     
}

function renderPhoneNumbers(items){
    var htmlNumbers = ``;
    htmlNumbers +=    `<div class="row col"><div class="addmem">`
    items.map((item, i)=>{
        htmlNumbers +=    `
        <div class="row addrow">
        <div class="form-group row">
            <select name="phone_type" id="phone_type" class="form-select inputName ui-autocomplete-select required">
            <option value="">Select</option>
            <option value="Mobile_Phone" ${item.phone_type == "Mobile_Phone" && "selected"}>Mobile Phone</option>
            <option value="Business_Phone" ${item.phone_type == "Business_Phone" && "selected"} >Business Phone</option>
            <option value="Home_Phone"  ${item.phone_type == "Home_Phone" && "selected"}>Home Phone</option>
            <option value="Fax"  ${item.phone_type == "Fax" && "selected"}>Fax</option>
            <option value="Call_Center_Contact"  ${item.phone_type == "Call_Center_Contact" && "selected"}>Call Center Contact</option>
            </select>
            <input type="text" class="form-control inputMobile required"  id="phone_number" placeholder="Mobile Number" name="phone_number[]" value="${item.phone_number}" />
            <i class="fa fa-trash deletebatchmember cursorPoint"  onclick="removePhoneNumber(this)" style="display: ${items.length > 1 ? "block" : "none"}; text-align:right;"></i>
        </div>
    </div>`
    })
    htmlNumbers +=  `</div><input type="button" class="btn btn-success btn-align-right-add-update" value="Add Phone Number" onclick="addPhoneNumber()" id="addButton" /></div>`;
    console.log(htmlNumbers)
    return htmlNumbers;
}

function renderExpertise(item){
    var htmlNumbers = ``;
    htmlNumbers +=    `<div class="row col"><div class="addmem">`
    
        htmlNumbers +=    `
        <div class="row addrow col-sm-6">
            <div class="form-group row">
            Expertise:
            <select name="form_expertise" id="form_expertise" class="form-select inputName ui-autocomplete-select required">
                <option value="">Select</option>
                <option value="driver" ${item?.phone_type == "driver" && "selected"}>Driver</option>
                <option value="mover" ${item?.phone_type == "mover" && "selected"}>Mover</option>
                <option value="painosbabygrand" ${item?.phone_type == "painosbabygrand" && "selected"}>Piano's Baby Grand</option>
                <option value="pianosregular" ${item?.phone_type == "pianosregular" && "selected"} >Piano's Regular</option>
                <option value="packing"  ${item?.phone_type == "packing" && "selected"}>Packing</option>
            </select>
            </div>
            <div class="form-group row">
                <button>Save</button>
                <button>Cancel</button>
            </div>
            <hr/>
            <div class="form-group row col">
                <table>
                    <tr>
                        <td>Driver</td>
                        <td></td>
                    </tr>
                </table>
            </div>
    </div>`
    
    htmlNumbers +=  `</div></div>`;
    console.log(htmlNumbers)
    return htmlNumbers;
}

function renderEmailIds(items){
    var htmlEmails = '';
    htmlEmails +=    `<div class="row col"><div class="emailMem">`
    items.map((item, i)=>{
        htmlEmails +=    `<div class="row addemailrow">
                            <div class="form-group row">
                            <select name="phone_type" id="phone_type" class="form-select inputName ui-autocomplete-select required">
            <option value="">Select</option>
            <option value="Corporate" ${item.email_type == "Corporate" && "selected"}>Corporate</option>
            <option value="Personal" ${item.email_type == "Personal" && "selected"}>Personal</option>
            </select>
                                <input type="text" class="form-control  inputEmail required" placeholder="" name="email[]"  value="${item.email_id}"/>
                                <i class="fa fa-trash deletebatchemail cursorPoint"  onclick="removeEmail(this)"  style="display: none; text-align:right;"></i>
                            </div>
                        </div>`
    });
    htmlEmails +=  `</div><input type="button" class="btn btn-success btn-align-right-add-update" value='Add Email' onclick="addEmailNumber()" id='addEmail' /></div>
           `;
    return htmlEmails;
}

function renderEmpDetails(items){
    var htmlEmails = '';
    var item = items.employeement_details;
    var hireDate = '';
    if(item.hire_date == ''){
        hireDate = item.hire_date.split("/");
        empHireDate = hireDate[1]+'/'+ hireDate[2]+'/'+ hireDate[0];

    }
    var terminationDate = '';
    if(item.termination_date == ''){
        terminationDate = item.termination_date.split("/");
        terminationDate = terminationDate[1]+'/'+ terminationDate[2]+'/'+ terminationDate[0];
    }
   
     
    htmlEmails +=    `<div class="empDetailsMem">`
   
        htmlEmails +=    `<div class="row empDetailsRow">
                            <div class="form-group row">
                            <div class="row col-sm-12">
                                    <div id="address_div" class=" col-sm-12">
                                        <div class="col col-sm-12 row jsonData">
                                            <div class="col col-sm-4">
                                                <label class="control-label">Employee Status</label>
                                            </div>
                                            <div class="col col-sm-8">
                                            <select name="emp_status" id="emp_status" class="form-select empStatus ui-autocomplete-select required">
                                                <option value="">Select</option>
                                                <option value="Employed" ${item.employee_status == "Employed" && "selected"}>Employed</option>
                                                <option value="On-Leave" ${item.employee_status == "On-Leave" && "selected"}>On-Leave</option>
                                                <option value="Terminated" ${item.employee_status == "Terminated" && "selected"}>Terminated</option>
                                            </select>
                                                 
                                                <label for="email"></label>
                                            </div>
                                        </div>
                                        <div class="col col-sm-12 row jsonData">
                                            <div class="col col-sm-4">
                                                <label class="control-label">Employee Type</label>
                                            </div>
                                            <div class="col col-sm-8">
                                            <select name="emp_type" id="emp_type" class="form-select empType ui-autocomplete-select required">
                                                <option value="">Select</option>
                                                <option value="1" ${item.employee_type == "1" && "selected"}>Full-time</option>
                                                <option value="2" ${item.employee_type == "2" && "selected"}>Part-time</option>
                                            </select>
                                                <label for="email"></label>
                                            </div>
                                        </div>
                                        <div class="col col-sm-12 row jsonData">
                                            <div class="col col-sm-4">
                                                <label class="control-label">Hire Date</label>
                                            </div>
                                            <div class="col col-sm-8"> 
                                                <input class="form-control ui-autocomplete-input required inputCity" type="text" id="hire_date" name="hire_date" value="${hireDate}" autocomplete="off">  
                                                <label for="hire_date"></label>
                                            </div>
                                        </div>
                                        <div class="col col-sm-12 row jsonData">
                                            <div class="col col-sm-4">
                                                <label class="control-label">Termination Date</label>
                                            </div>
                                            <div class="col col-sm-8">
                                                    <input class="form-control ui-autocomplete-input required terminationDate" type="text" id="termination_date" name="termination_date" value="${terminationDate}" autocomplete="off"> 
                                                <label for="termination_date"></label>
                                            </div>
                                        </div>
                                        <div class="col col-sm-12 row jsonData">
                                            <div class="col col-sm-4">
                                                <label class="control-label">Hourly Rate</label>
                                            </div>
                                            <div class="col col-sm-8">
                                                <input class="form-control ui-autocomplete-input required hourlyRate" type="text" name="hourly_rate" id="hourly_rate" value="${item.hourly_rate != null ? item.hourly_rate : ''}" autocomplete="off">
                                                <label for="hourly_rate"></label>
                                            </div>
                                        </div>
                                        <div class="col col-sm-12 row jsonData">
                                            <div class="col col-sm-4">
                                                <label class="control-label">Commission Rate</label>
                                            </div>
                                            <div class="col col-sm-8">
                                                <input class="form-control ui-autocomplete-input required commissionRate" type="text" name="commission_rate" id="commission_rate" value="${item.commission_rate != null ? item.commission_rate : ''}" autocomplete="off">
                                                <label for="commission_rate"></label>
                                            </div>
                                        </div>
                                    </div>
                                 
                            </div>
                        </div>`
   
    htmlEmails +=  `</div>
           `;
    return htmlEmails;
}

function renderEmpClientReviewDetails(items){
    var htmlClientReview = ` <div class="table-bar2 tableGrid">
    <table  id="customers2" class="table table-striped datatable">
        <thead>
            <tr>
                <th>Date</th>
                <th>Customer Name</th>
                <th>Estimate</th>
                <th>Crew Members</th>
                <th>Rating</th>
                <th>Comments</th>
            </tr>
        </thead>`;

    var rat = 0;
    if(items.empReviewData && items.empReviewData.length > 0){
        items.empReviewData.map((item, i) => {
            htmlClientReview +=    `<tr>
            <td>${item.dates}</td>
            <td>${item.customer_name}</td>
            <td>${item.estimate_id}</td>
            <td>${item.crew_members}</td>
            <td>${item.rating_number}</td>
            <td>${item.comments}</td>
            </tr>`
            rat += item.rating_number;
        });
    } else {
        htmlClientReview += `<tr><td colspan="6" class="text-center">No Records Found</td></tr>`;
    }
    $("#overallrating").html("Overall Rating " + rat)
    htmlClientReview += `</table></div>`;

    return htmlClientReview;

}
function renderAssigned_Prospects(items){
    var thead = `<thead>
            <tr>
                <th>Name</th>
                <th>Referred By</th>
                <th>Rep</th>
                <th>Status</th>
                <th>L.C</th>
                <th>N.C</th>
            </tr>
        </thead>`;
    
    var htmlAssignedHotProspects = ` <div class="table-bar2 tableGrid">
    <h3>Hot Prospects</h3>
    <table  id="customers2" class="table table-striped datatable">`
        htmlAssignedHotProspects += thead;
    if(items.hotProspect && items.hotProspect.length > 0){
        items.hotProspect.map((item, i) => {
            htmlAssignedHotProspects +=    `<tr>
                <td>${item.name}</td>
                <td>${item.referred_by}</td>
                <td>${item.rep}</td>
                <td>${item.is_active == 1 ? `Active` : `In-Active` }</td>
                <td>${item.lc}</td>
                <td>${item.nc}</td>
            </tr>`
        });
    } else {
        htmlAssignedHotProspects += `<tr><td colspan="6" class="text-center">No Records Found</td></tr>`;
    }
    htmlAssignedHotProspects += `</table></div>`;

    var htmlAssignedWarmProspects = ` <div class="table-bar2 tableGrid">
    <h3>Warm Prospects</h3>
    <table  id="customers2" class="table table-striped datatable">`
        htmlAssignedWarmProspects += thead;
    if(items.warmProspect && items.warmProspect.length > 0){
        items.warmProspect.map((item, i) => {
            htmlAssignedWarmProspects +=    `<tr>
                <td>${item.name}</td>
                <td>${item.referred_by}</td>
                <td>${item.rep}</td>
                <td>${item.is_active == 1 ? `Active` : `In-Active` }</td>
                <td>${item.lc}</td>
                <td>${item.nc}</td>
            </tr>`
        });
    } else {
        htmlAssignedWarmProspects += `<tr><td colspan="6" class="text-center">No Records Found</td></tr>`;
    }
    htmlAssignedWarmProspects += `</table></div>`;

    var htmlAssignedColdProspects = ` <div class="table-bar2 tableGrid">
    <h3>Cold Prospects</h3>
    <table  id="customers2" class="table table-striped datatable">`
        htmlAssignedColdProspects += thead;
    if(items.coldProspect && items.coldProspect.length > 0){
        items.coldProspect.map((item, i) => {
            htmlAssignedColdProspects +=    `<tr>
                <td>${item.name}</td>
                <td>${item.referred_by}</td>
                <td>${item.rep}</td>
                <td>${item.is_active == 1 ? `Active` : `In-Active` }</td>
                <td>${item.lc}</td>
                <td>${item.nc}</td>
            </tr>`
        });
    } else {
        htmlAssignedColdProspects += `<tr><td colspan="6" class="text-center">No Records Found</td></tr>`;
    }
    htmlAssignedColdProspects += `</table></div>`;
    // $("#Assigned_Prospects div.result").html("Overall Rating " + rat)
    return htmlAssignedHotProspects + htmlAssignedWarmProspects  + htmlAssignedColdProspects ;
}

function renderAssigned_Customers(items){
    var thead = `<thead>
            <tr>
                <th>Name</th>
                <th>Referred By</th>
                <th>Rep</th>
                <th>Status</th>
                <th>L.C</th>
                <th>N.C</th>
            </tr>
        </thead>`;
    
    var htmlAssignedCustomers = ` <div class="table-bar2 tableGrid">
    <table  id="customers2" class="table table-striped datatable">`
        htmlAssignedCustomers += thead;
    if(items.users && items.users.length > 0){
        items.users.map((item, i) => {
            htmlAssignedCustomers +=    `<tr>
                <td>${item.name}</td>
                <td>${item.referred_by}</td>
                <td>${'rep'}</td>
                <td>${item.is_active == 1 ? `Active` : `In-Active` }</td>
                <td>${''}</td>
                <td>${''}</td>
            </tr>`
        });
    } else {
        htmlAssignedCustomers += `<tr><td colspan="6" class="text-center">No Records Found</td></tr>`;
    }
    htmlAssignedCustomers += `</table></div>`;
    return htmlAssignedCustomers;
}

function renderEvents(items){
    var thead = `<thead>
            <tr>
                <th>At</th>
                <th>Rep</th>
                <th>Source</th>
                <th>Person</th>
                <th>Estimate</th>
                <th>Comments</th>
            </tr>
        </thead>`;
    
    var htmlEvents = ` <div class="table-bar2 tableGrid">
    <table  id="customers2" class="table table-striped datatable">`
        htmlEvents += thead;
    if(items.events && items.events.length > 0){
        items.events.map((item, i) => {
            htmlEvents +=    `<tr>
                <td>${item.event_date}</td>
                <td>${item.rep}</td>
                <td>${item.source}</td>
                <td>${item.person}</td>
                <td>${item.estimate_id}</td>
                <td>${item.comments}</td>
            </tr>`
        });
    } else {
        htmlEvents += `<tr><td colspan="6" class="text-center">No Records Found</td></tr>`;
    }
    htmlEvents += `</table></div>`;
    return htmlEvents;
}
 
function renderChat_History(items){
    var htmlEvents = `<div id="container" class="mb-2">
	<aside>
		<header>
			<input type="text" placeholder="search">
		</header>
		<ul>
			<li>
				<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_01.jpg" alt="">
				<div>
					<h2>Prénom Nom</h2>
					<h3>
						<span class="status orange"></span>
						offline
					</h3>
				</div>
			</li>
			<li>
				<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_02.jpg" alt="">
				<div>
					<h2>Prénom Nom</h2>
					<h3>
						<span class="status green"></span>
						online
					</h3>
				</div>
			</li>
			<li>
				<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_03.jpg" alt="">
				<div>
					<h2>Prénom Nom</h2>
					<h3>
						<span class="status orange"></span>
						offline
					</h3>
				</div>
			</li>
			<li>
				<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_04.jpg" alt="">
				<div>
					<h2>Prénom Nom</h2>
					<h3>
						<span class="status green"></span>
						online
					</h3>
				</div>
			</li>
			<li>
				<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_05.jpg" alt="">
				<div>
					<h2>Prénom Nom</h2>
					<h3>
						<span class="status orange"></span>
						offline
					</h3>
				</div>
			</li>
			<li>
				<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_06.jpg" alt="">
				<div>
					<h2>Prénom Nom</h2>
					<h3>
						<span class="status green"></span>
						online
					</h3>
				</div>
			</li>
			<li>
				<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_07.jpg" alt="">
				<div>
					<h2>Prénom Nom</h2>
					<h3>
						<span class="status green"></span>
						online
					</h3>
				</div>
			</li>
			<li>
				<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_08.jpg" alt="">
				<div>
					<h2>Prénom Nom</h2>
					<h3>
						<span class="status green"></span>
						online
					</h3>
				</div>
			</li>
			<li>
				<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_09.jpg" alt="">
				<div>
					<h2>Prénom Nom</h2>
					<h3>
						<span class="status green"></span>
						online
					</h3>
				</div>
			</li>
			<li>
				<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_10.jpg" alt="">
				<div>
					<h2>Prénom Nom</h2>
					<h3>
						<span class="status orange"></span>
						offline
					</h3>
				</div>
			</li>
		</ul>
	</aside>
	<main>
		<header>
			<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_01.jpg" alt="">
			<div>
				<h2>Chat with Vincent Porter</h2>
				<h3>already 1902 messages</h3>
			</div>
			<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/ico_star.png" alt="">
		</header>
		<ul id="chat">
			<li class="you">
				<div class="entete">
					<span class="status green"></span>
					<h2>Vincent</h2>
					<h3>10:12AM, Today</h3>
				</div>
				<div class="triangle"></div>
				<div class="message">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
				</div>
			</li>
			<li class="me">
				<div class="entete">
					<h3>10:12AM, Today</h3>
					<h2>Vincent</h2>
					<span class="status blue"></span>
				</div>
				<div class="triangle"></div>
				<div class="message">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
				</div>
			</li>
			<li class="me">
				<div class="entete">
					<h3>10:12AM, Today</h3>
					<h2>Vincent</h2>
					<span class="status blue"></span>
				</div>
				<div class="triangle"></div>
				<div class="message">
					OK
				</div>
			</li>
			<li class="you">
				<div class="entete">
					<span class="status green"></span>
					<h2>Vincent</h2>
					<h3>10:12AM, Today</h3>
				</div>
				<div class="triangle"></div>
				<div class="message">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
				</div>
			</li>
			<li class="me">
				<div class="entete">
					<h3>10:12AM, Today</h3>
					<h2>Vincent</h2>
					<span class="status blue"></span>
				</div>
				<div class="triangle"></div>
				<div class="message">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
				</div>
			</li>
			<li class="me">
				<div class="entete">
					<h3>10:12AM, Today</h3>
					<h2>Vincent</h2>
					<span class="status blue"></span>
				</div>
				<div class="triangle"></div>
				<div class="message">
					OK
				</div>
			</li>
		</ul>
		<footer>
			<textarea placeholder="Type your message"></textarea>
			<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/ico_picture.png" alt="">
			<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/ico_file.png" alt="">
			<a href="#">Send</a>
		</footer>
	</main>
</div>`;
    return htmlEvents;
}
function renderSMS_Messaging(items){
    var htmlClientReview = ``;
    htmlClientReview = `<div id="container" class="mb-2">
	<main class="smsMain">
		<header>
			<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/chat_avatar_01.jpg" alt="">
			<div>
				<h2>Chat with Vincent Porter</h2>
				<h3>already 1902 messages</h3>
			</div>
			<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/ico_star.png" alt="">
		</header>
		<ul id="chat">
			<li class="you">
				<div class="entete">
					<span class="status green"></span>
					<h2>Vincent</h2>
					<h3>10:12AM, Today</h3>
				</div>
				<div class="triangle"></div>
				<div class="message">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
				</div>
			</li>
			<li class="me">
				<div class="entete">
					<h3>10:12AM, Today</h3>
					<h2>Vincent</h2>
					<span class="status blue"></span>
				</div>
				<div class="triangle"></div>
				<div class="message">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
				</div>
			</li>
			<li class="me">
				<div class="entete">
					<h3>10:12AM, Today</h3>
					<h2>Vincent</h2>
					<span class="status blue"></span>
				</div>
				<div class="triangle"></div>
				<div class="message">
					OK
				</div>
			</li>
			<li class="you">
				<div class="entete">
					<span class="status green"></span>
					<h2>Vincent</h2>
					<h3>10:12AM, Today</h3>
				</div>
				<div class="triangle"></div>
				<div class="message">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
				</div>
			</li>
			<li class="me">
				<div class="entete">
					<h3>10:12AM, Today</h3>
					<h2>Vincent</h2>
					<span class="status blue"></span>
				</div>
				<div class="triangle"></div>
				<div class="message">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
				</div>
			</li>
			<li class="me">
				<div class="entete">
					<h3>10:12AM, Today</h3>
					<h2>Vincent</h2>
					<span class="status blue"></span>
				</div>
				<div class="triangle"></div>
				<div class="message">
					OK
				</div>
			</li>
		</ul>
		<footer>
			<textarea placeholder="Type your message"></textarea>
			<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/ico_picture.png" alt="">
			<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1940306/ico_file.png" alt="">
			<a href="#">Send</a>
		</footer>
	</main>
</div>`;

    return htmlClientReview;

}

function renderPayroll(items){

    var htmlClientReview = ``;
    var addButtonsHtml = `<div>
    <div class='row'>
        <div class='col-sm-1'>Start Date</div>
        <div class='col-sm-1'><input type="text"/></div>
        <div class='col-sm-1'>End Date</div>
        <div class='col-sm-1'><input type="text"/></div>
        <div class='col-sm-1'>Company Branch</div>
        <div class='col-sm-1'><select>
            <option>Select</option>
            <option>Big Hill Movers</option>
            </select>
        </div>
        <div class='col-sm-1'><button>View</button></div>
        <div class='col-sm-1'><button>Export to PDF</button></div>
    </div>
    <button onclick="loadModal('payrollLog', 'newDisciplinary', 'closePayroll');">Add Manual Entry</button>
    </div>`;
    // starts real data
    var realDataHtml = '';
    realDataHtml += `
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Hours</th>
                    <th>Hours Pay</th>
                    <th>Travel</th>
                    <th>Miles</th>
                    <th>Commission</th>
                    <th>Bonuses</th>
                    <th>Per Diem</th>
                    <th>Tips</th>
                    <th>Deductions</th>
                    <th>Total</th>
                </tr>
            </thead>
         
    `;
    if(items.Payroll.realdata.length > 0){
        items.Payroll.realdata.map((rd, ri)=>{
            realDataHtml += `
            <tr>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
            </tr>
            `;
        })
    } else {
        realDataHtml += `
            <tr>
                <td colspan='10'>No Data Available</td>
            </tr>
            `;
    }
    realDataHtml += `</table>`;
    // ends real data

    // starts moving/packing
    var moving_packingHtml = '<h3>Moving / Packing</h3>';
    moving_packingHtml += `
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Day</th>
                    <th>Date</th>
                    <th>Class</th>
                    <th>Description</th>
                    <th>Customer</th>
                    <th>Rate</th>
                    <th>Hours</th>
                    <th>Travel</th>
                    <th>Commission</th>
                    <th>Total</th>
                </tr>
            </thead>
         
    `;
    if(items.Payroll.moving_packing.length > 0){
        items.Payroll.moving_packing.map((rd, ri)=>{
            moving_packingHtml += `
            <tr>
                <td>${rd.entry_date}</td>
                <td>${rd.entry_date}</td>
                <td>${rd.class_select}</td>
                <td>${rd.description}</td>
                <td>${rd.customer}</td>
                <td>${rd.hourly_rate}</td>
                <td>${rd.hours}</td>
                <td>${rd.travel}</td>
                <td>${rd.commission}</td>
                <td>${rd.entry_date}</td>
            </tr>
            `;
        })
    } else {
        moving_packingHtml += `
            <tr>
                <td colspan='10'>No Data Available</td>
            </tr>
            `;
    }
    moving_packingHtml += `</table>`;
    // ends moving packing

    // starts Manual Entry
    var manual_entryHtml = '<h3>Manual Entry</h3>';
    manual_entryHtml += `
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Day</th>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Rate</th>
                    <th>Hours</th>
                    <th>Commission</th>
                    <th>Bonuses</th>
                    <th>Per Diem</th>
                    <th>Tips</th>
                    <th>Deductions</th>
                    <th>Total</th>
                </tr>
            </thead>
         
    `;
    if(items.Payroll.manual_entry.length > 0){
        items.Payroll.manual_entry.map((rd, ri)=>{
            manual_entryHtml += `
            <tr>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
                <td>${''}</td>
            </tr>
            `;
        })
    } else {
        manual_entryHtml += `
            <tr>
                <td colspan='11'>No Data Available</td>
            </tr>
            `;
    }
    manual_entryHtml += `</table>`;
    // ends manual Entry

    // starts misc Hours
    var misc_hoursHtml = '<h3>Misc Hours</h3>';
    misc_hoursHtml += `
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Day</th>
                    <th>Date</th>
                    <th>Class</th>
                    <th>Description</th>
                    <th>Rate</th>
                    <th>Hours</th>
                    <th>Total</th>
                </tr>
            </thead>
         
    `;
    if(items.Payroll.misc_hours.length > 0){
        items.Payroll.misc_hours.map((rd, ri)=>{
            misc_hoursHtml += `
            <tr>
                <td>${rd.entry_date}</td>
                <td>${rd.entry_date}</td>
                <td>${rd.class_select}</td>
                <td>${rd.description}</td>
                <td>${rd.hourly_rate}</td>
                <td>${rd.hours}</td>
                <td>${'?'}</td>
            </tr>
            `;
        })
    } else {
        misc_hoursHtml += `
            <tr>
                <td colspan='7'>No Data Available</td>
            </tr>
            `;
    }
    misc_hoursHtml += `</table>`;
    // ends misc hours

    // starts tips Hours
    var tipsHtml = '<h3>Tips</h3>';
    tipsHtml += `
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Class</th>
                    <th>Customer</th>
                    <th>Description</th>
                    <th>Amount</th>
                </tr>
            </thead>
         
    `;
    if(items.Payroll.tips.length > 0){
        items.Payroll.tips.map((rd, ri)=>{
            tipsHtml += `
            <tr>
                <td>${rd.entry_date}</td>
                <td>${rd.class_select}</td>
                <td>${rd.customer}</td>
                <td>${rd.description}</td>
                <td>${rd.amount}</td>
            </tr>
            `;
        })
    } else {
        tipsHtml += `
            <tr>
                <td colspan='5'>No Data Available</td>
            </tr>
            `;
    }
    tipsHtml += `</table>`;
    // ends tips hours

    // starts bonuses/perdiem Hours
    var bonuses_perdiemHtml = '<h3>Bonuses / Per Diem</h3>';
    bonuses_perdiemHtml += `
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Class</th>
                    <th>Customer</th>
                    <th>Description</th>
                    <th>Amount</th>
                </tr>
            </thead>
         
    `;
    if(items.Payroll.bonuses_perdiem.length > 0){
        items.Payroll.bonuses_perdiem.map((rd, ri)=>{
            bonuses_perdiemHtml += `
            <tr>
                <td>${rd.entry_date}</td>
                <td>${rd.class_select}</td>
                <td>${rd.customer}</td>
                <td>${rd.description}</td>
                <td>${rd.amount}</td>
            </tr>
            `;
        })
    } else {
        bonuses_perdiemHtml += `
            <tr>
                <td colspan='5'>No Data Available</td>
            </tr>
            `;
    }
    bonuses_perdiemHtml += `</table>`;
    // ends bonuses/perdiem hours

    // starts deductions Hours
    var deductionsHtml = '<h3>Deductions</h3>';
    deductionsHtml += `
        <table class="table table-striped datatable">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Class</th>
                    <th>Customer</th>
                    <th>Description</th>
                    <th>Amount</th>
                </tr>
            </thead>
         
    `;
    if(items.Payroll.deductions.length > 0){
        items.Payroll.deductions.map((rd, ri)=>{
            deductionsHtml += `
            <tr>
                <td>${rd.entry_date}</td>
                <td>${rd.class_select}</td>
                <td>${rd.customer}</td>
                <td>${rd.description}</td>
                <td>${rd.amount}</td>
            </tr>
            `;
        })
    } else {
        deductionsHtml += `
            <tr>
                <td colspan='5'>No Data Available</td>
            </tr>
            `;
    }
    deductionsHtml += `</table>`;
    // ends deductions hours
    return htmlClientReview = addButtonsHtml + realDataHtml + moving_packingHtml + manual_entryHtml + misc_hoursHtml + misc_hoursHtml + tipsHtml + bonuses_perdiemHtml + deductionsHtml;

}

function renderAssigned_Tasks(selectedEmpTab){
   getBasedStatus('1', selectedEmpTab)
}
function renderCompleted_Tasks(selectedEmpTab){
   getBasedStatus('3', selectedEmpTab)
}
function renderOutstanding_Tasks(selectedEmpTab){
   getBasedStatus('2', selectedEmpTab)
}

function getBasedStatus(id, selectedEmpTab){
    $.get("/admin/tasks/status/"+id, function(res) {
        console.log(selectedEmpTab);
        $("#tabName").val(selectedEmpTab);
        var htmlTask = '';
        // $('#completedTasks, #assignedTasks').dataTable().fnDestroy();
        htmlTask += `<thead>
            <tr>
              <th>Task Id</th>
              <th>Task type</th>
              <th>Assigned By</th>
              <th>Assigned To</th>
              <th>Client</th>
              <th>Contact Person</th>
              <th>Priority</th>
              <th>Status</th>
              <th>When</th>
              <th>Actions</th>
            </tr>
          </thead><tbody>`;
        if(res && res.data && res.data.length > 0 ){
          res.data.map((j, i)=>{
          htmlTask += `<tr id="statuswise">
              <td>
                <a href="#" class="taskedit"  data-id="${j.id}">${j.task_id}</a>
              </td>
              <td>
                ${j.tasktypeName}
              </td>
              <td>
                ${j.assigned_user_by}
              </td>
              <td>
              ${j.assigned_user_to}
              </td>
              <td>
              ${j.client==null ? 'N/A' :j.client}
              </td>
              <td>
              ${j.contact_person==null ? 'N/A' : j.contact_person}
              </td>
              <td>
              ${j.priority}
              </td>
              <td>
              ${j.taskstatusName}
              </td>
              <td>
              ${j.when_date}
              </td>
              <td width="15%">
                <div >
                  <div style="float:left;padding:0 10px 10px 0;">
                        <i class="fa fa-edit cursorPoint taskedit" onclick="editTask(this)" data-id="${j.id}"></i>
                    </div>
                    <div style="float:left;padding-right:10px;">
                      <i class="fa fa-trash cursorPoint taskdelete"  onclick="deleteTask(this)" data-id="${j.id}"></i>
                    </div>
                </div>
              </td>
            </tr>`
          });
          htmlTask += `</tbody>`;
          // $("#customers2").html(htmlTask);
          // $('#customers2').addClass('table table-striped data')
          // $('#customers2').dataTable();
        } else {
          htmlTask += `<tr>
              <td colspan="10">No data available</td>
            </tr>`
        }
        console.log(htmlTask)
        console.log(selectedEmpTab)
        if(selectedEmpTab == 'Completed_Tasks'){
            $("#completedTasks").html(htmlTask);
        } else if(selectedEmpTab == 'Outstanding_Tasks'){
            $("#outstandingTasks").html(htmlTask);
        } else {
            $("#assignedTasks").html(htmlTask);
        }
            
        //   return htmlTask
      }).fail(function() {
        console.log("fail")
      }).done(function() {
        console.log("completed");
      })
}

function renderLogin_Permissions(item){
    var htmlEmails = '';
    
    htmlEmails +=    `<div class="loginPermissions">`
        let groupAccess = item.groups_access != null && (item.groups_access).split(",");
        htmlEmails +=    `<div class="row empDetailsRow">
                            <div class="form-group row">
                            <div class="row col-sm-12">
                                <div id="address_div" class=" col-sm-12">
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">User Name:</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input type="text" name="username" id="username" value="${item.user_name}" />
                                            <label for="username"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Email:</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input type="text" name="email" id="email" value="${item.email == null ? '' : item.email}" />
                                            <label for="email"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Driving License:</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input type="text" name="driving_license" id="driving_license" value="${item.driver_license == null ? '' : item.driver_license}" />
                                            <label for="driving_license"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Password</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input type="password" name="password" id="password" value=""  />
                                            <label for="password"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Two-Factor Authentication</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            Add an extra layer of protection to your account, with a verification code sent via SMS or voice call</br>
                                            <input type="radio" name="two_factor_auth" id="two_factor_auth_disabled" value="disabled" ${item.two_factor_authentication == "disabled" && "checked=true"}/> Disabled (Do not require a verification code) </br>
                                            <input type="radio" name="two_factor_auth" id="two_factor_auth_once_per_comp" value="once_per_computer" ${item.two_factor_authentication == "once_per_computer" && "checked=true"}/> Once Per Computer (Trust computers and only ask for verification code every 30 days)</br>
                                            <input type="radio" name="two_factor_auth" id="two_factor_auth_every_login" value="every_login" ${item.two_factor_authentication == "every_login" && "checked=true"}/> Every Log-in (We'll always ask for a verification code)</br>
                                            <label for="two_factor_auth"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Chat:</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input type="checkbox" name="chat_status" id="chat_status" value="yes" ${item.chat_prmsn == "1" && "checked"}/>
                                            <label for="chat_status"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Incoming Call Status:</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input type="checkbox" name="incoming_call_status" id="incoming_call_status" value="yes" ${item.incoming_call_prmsn == "1" && "checked"}/>
                                            <label for="incoming_call_status"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Customer Chat:</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input type="checkbox" name="customer_chat_status" id="customer_chat_status" value="yes" ${item.customer_chat_prmsn == "1" && "checked"}/>
                                            <label for="customer_chat_status"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Chat Admin:</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input type="checkbox" name="chat_admin_status" id="chat_admin_status" value="yes" ${item.chat_admin_prmsn == "1" && "checked"}/>
                                            <label for="chat_admin_status"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Call Center Contact:</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input type="checkbox" name="call_center_contact_status" id="call_center_contact_status" value="yes" ${item.call_center_contact_prmsn == "1" && "checked"}/>
                                            <label for="call_center_contact_status"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Billing User Management:</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input type="checkbox" name="billing_user_mgmt_status" id="billing_user_mgmt_status" value="yes" ${item.billing_user_mgmt_prmsn == "1" && "checked"}/>
                                            <label for="billing_user_mgmt_status"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Pro App User:</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input type="checkbox" name="pro_app_user_status" id="pro_app_user_status" value="yes" ${item.prop_app_user_prmsn == "1" && "checked"}/>
                                            <label for="pro_app_user_status"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Foreman:</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input type="checkbox" name="foreman_status" id="foreman_status" value="yes" ${item.foreman_prmsn == "1" && "checked"}/>
                                            <label for="foreman_status"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Groups</label>
                                        </div>
                                        <div class="col col-sm-8"> 
                                            <select name="groups_status" id="groups_status"  class="select" multiple>
                                                <option value="1" ${groupAccess.length > 0 && groupAccess.indexOf("1") > -1 && "selected"}>Group 1</option> 
                                                <option value="2" ${groupAccess.length > 0 && groupAccess.indexOf("2") > -1 && "selected"}>Group 2</option> 
                                                <option value="3" ${groupAccess.length > 0 && groupAccess.indexOf("3") > -1 && "selected"}>Group 3</option> 
                                                <option value="4" ${groupAccess.length > 0 && groupAccess.indexOf("4") > -1 && "selected"}>Group 4</option> 
                                            </select> 
                                            <label for="groups_status"></label>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12 row jsonData">
                                        <div class="col col-sm-4">
                                            <label class="control-label">Status</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input type="radio" name="status" id="status_active" value="1" ${item.is_active && "checked"}/> Active
                                            <input type="radio" name="status" id="status_inactive" value="0" ${!item.is_active && "checked"}/> Inactive
                                            <label for="status"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`
    htmlEmails +=  `</div>`;

    return htmlEmails;
}

function renderAuditTrailDetails(items){
    var htmlAuditTrail = `<div class="table-bar2 tableGrid">`;

    var rat = 0;
    if(items.Audit_Trail && items.Audit_Trail.length > 0){
        items.Audit_Trail.map((item, i) => {
            htmlAuditTrail +=    `<div class=" row col m-2">${item.description}</div>`
        });
    }  
    htmlAuditTrail += `</div>`
    $("#Audit_Trail div.result").html(htmlAuditTrail)
}

function countryDetails(cid){
    // console.log(cid)
    var htmlCountries = '<option>Select Country</option>';
    // console.log(idCountry)
    @foreach($countries as $country)
    var idCountry = `{{$country->id}}`;
    htmlCountries += `<option value="{{$country->id}}" `+ (idCountry == cid ? 'selected' : null  ) + `   >{{$country->name}}</option>`;
    @endforeach
    // console.log(htmlCountries)

    return htmlCountries;
}

function statesHandler(cid, sid, states){
    var y = states.filter((state, i) => state.cid == cid ? state: null )
    console.log(y)
    var statesHtml = '<option value="">Select State</option>';
    y.map((item, i)=>{
        statesHtml +=    `<option value="${item.id}" `+ (item.id == sid ? 'selected' : null  ) + `>${item.name}</option>`
    });
     
    return statesHtml;
    // $.get( "/admin/states/get/"+cid, function(result) {
    //     var data = result.data;
    //     if(data && data.length > 0){
    //         data.map(function(d, i){
    //             statesHtml += `<option value='${d.id}' >${d.name}</option>`;
    //         })
    //     }
    //     console.log(statesHtml)
    //     return statesHtml;
    //     // $(that).parent().parent().next().find('select.statesNames').html(statesHtml)
    // });
} 
// console.log(countryDetails())

function renderAddress(items, states){
    var htmlAddress = ``;
    htmlAddress +=    `<div class="row col"><div class="addressMem">`
    items.map((item, i)=>{

        

        htmlAddress +=    `<div class="row addAddressrow">
                                <div class="form-group row">
                                    <div class="addressDetails">
                                        <div class="row col-sm-12">
                                        <div id="address_div" class=" col-sm-12">
                                        <div class="col col-sm-12 row jsonData">
                                            <div class="col col-sm-4">
                                                <label class="control-label">Street</label>
                                            </div>
                                            <div class="col col-sm-8">
                                                <input class="form-control ui-autocomplete-input required inputStreet" type="text" name="street[]" value="${item.street}" id="street" autocomplete="off">
                                                <label for="email"></label>
                                            </div>
                                        </div>
                                        <div class="col col-sm-12 row jsonData">
                                            <div class="col col-sm-4">
                                                <label class="control-label">Suite</label>
                                            </div>
                                            <div class="col col-sm-8">
                                                <input class="form-control ui-autocomplete-input required inputSuite" type="text" name="suite[]" value="${item.suite}" id="suite" autocomplete="off">
                                                <label for="email"></label>
                                            </div>
                                        </div>
                                        <div class="col col-sm-12 row jsonData">
                                            <div class="col col-sm-4">
                                                <label class="control-label">Country</label>
                                            </div>
                                            <div class="col col-sm-8"> 
                                                    <select name="country" onchange='getStates(this)' class="form-select inputName countryselection ui-autocomplete-select required">
                                                    ` + countryDetails(parseInt(item.country)) + `
                                                        </select>    
                                                <label for="country"></label>
                                            </div>
                                        </div>
                                        <div class="col col-sm-12 row jsonData">
                                            <div class="col col-sm-4">
                                                <label class="control-label">State</label>
                                            </div>
                                            <div class="col col-sm-8">
                                                    <select name="states" class="form-select statesNames inputName ui-autocomplete-select required">
                                                        `+  statesHandler(item.country, item.state, states) + `
                                                    </select> 
                                                <label for="state"></label>
                                            </div>
                                        </div>
                                        <div class="col col-sm-12 row jsonData">
                                            <div class="col col-sm-4">
                                                <label class="control-label">City</label>
                                            </div>
                                            <div class="col col-sm-8">
                                                <input class="form-control ui-autocomplete-input required inputCity" type="text" name="city[]" value="${item.city}" autocomplete="off">
                                                <label for="email"></label>
                                            </div>
                                        </div>
                                        <div class="col col-sm-12 row jsonData">
                                            <div class="col col-sm-4">
                                                <label class="control-label">Zip</label>
                                            </div>
                                            <div class="col col-sm-8">
                                                <input class="form-control ui-autocomplete-input required inputZip" type="text" name="zip[]" value="${item.zip}" autocomplete="off">
                                                <label for="email"></label>
                                            </div>
                                        </div>
                                        </div>
                                    </div></div>
                                    <i class="fa fa-trash deleteaddressMem cursorPoint" onclick="removeAddress(this)"  style="display: none; text-align:right;"></i>
                                    <hr/>
                                </div>
                            </div>`
    })
    htmlAddress +=  `</div><input type="button" class="btn btn-align-right-add-update btn-success text-right" onclick='addAddress()' value='Add Address' id='addaddressMem' /></div>`;
    return htmlAddress;
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();

$( function() {
    $('#date_of_birth').datepicker({
        format: 'mm/dd/yyyy',
        startDate: 'd',
        maxDate: '0',
        changeMonth: true,
        changeYear: true,
    });
    
    // });
});

function editTask(that){
    // $(".taskedit").on("click", function(){
      var id = $(that).attr('data-id');
      $.get("/admin/tasks/search/id/"+id, function(res) {
        if(res && res.data && res.data.length > 0 ){
          var data = res.data[0];
          var dateWhen = data.when_date.split("-");
          modal.style.display = "block";
          $("#assigned_user_to").val(data.assigned_user_to);
          $("#when_date").val(dateWhen[1]+"/"+dateWhen[2]+"/"+dateWhen[0]);
          $("#task_type").val(data.task_type);
          $("#priority").val(data.priority);
          $("#status").val(data.status);
          $("#comments").val(data.comments);
          $("#record_id").val(data.id);
          $("#create").val("Update");
          $("#popuptitle").text("Update Task");
        } else {
          alert("data not found")
        }
        }).fail(function() {
            console.log("fail")
        }).done(function() {
            console.log("completed");
        })
        }
    // })
    function deleteTask(that){
    // $(".taskdelete").on("click", function(){
      var id = $(that).attr('data-id');
      $.get("/admin/tasks/delete/"+id, function(res) {
        if(res && res.data && res.data.length > 0 ){
          var data = res.data;
          alert(data);
          var id = $(".ui-tabs-active").attr('data-id');
          getBasedStatus(id)
        } else {
          alert("data not found")
        }
      }).fail(function() {
        console.log("fail")
      }).done(function() {
        console.log("completed");
      })
    }
function editDispRec(that){
    var rId = $(that).attr("data-id");
    $.get(`/admin/employees/displinary/${rId}`, function(res) {
        debugger;
        if(res && res.data){
            var disp = res.data.empDisciplinaryData[0];
                loadModal('displynryNote', 'newDisciplinary', 'closeNewDisp');
                dDate = disp.date.split("-");
                var dispDate = dDate[1]+'/'+ dDate[2]+'/'+ dDate[0];
                $('#discipnary_record_id').val(disp.id)
                $('#discipnary_date').val(dispDate)
                $('#discipnary_customer').val(disp.customer_name)
                $('#discipnary_category').val(disp.category)
                $('#discipnary_issue').val(disp.issues)
                $('#discipnary_notes').val(disp.note)
            } else {
                alert("invalid access");
            }
        });
}

// emmployee update

$(document).ready(function(){

    $("#employeeCreate").click(function(){
        var updateObj;
        var tabName = $("#tabName").val();
        console.log(tabName)
        if(tabName == 'Identity'){
            updateObj = {
                first_name: $("#first_name").val(),
                middle_name: $("#middle_name").val(),
                last_name: $("#last_name").val(),
                nick_name: $("#nick_name").val(),
                user_name: $("#user_name").val(),
                designation: $("#designation").val(),
                time_zone: $("#time_zone").val(),
                ssn: $("#ssn").val(),
                date_of_birth: $("#date_of_birth").val(),
                gender: $("#gender").val(),
                relationship: $("#relationship").val(),
                record_id: $("#record_id").val(),
                get_to_know: $("#get_to_know").val(),
                biography: $("#biography").val(),
                "_token": "{{ csrf_token() }}",
                tabName
            }
        } else if(tabName == 'Contact_Information'){

            var phone_nu = [];
            var phone_ty = [];
            var email_ids = [];
            var email_type = [];
            var street = [];
            var suite = [];
            var country = [];
            var states = [];
            var city = [];
            var zip = [];
            $("select[name=phone_type]").each(function(index, vals){
                phone_ty.push($(vals).val());
            })

            $("input.inputMobile").each(function(index, vals){
                phone_nu.push($(vals).val());
            })

            $("input.inputEmail ").each(function(index, vals){
                email_ids.push($(vals).val());
            })
            $("select[name=email_type] ").each(function(index, vals){
                email_type.push($(vals).val());
            })
            
            $("input.inputStreet ").each(function(index, vals){
                suite.push($(vals).val());
            })
            $("input.inputSuite ").each(function(index, vals){
                street.push($(vals).val());
            })
            $("select[name=country] ").each(function(index, vals){
                country.push($(vals).val());
            })
            $("select[name=states] ").each(function(index, vals){
                states.push($(vals).val());
            })
            $("input.inputCity ").each(function(index, vals){
                city.push($(vals).val());
            })
            $("input.inputZip ").each(function(index, vals){
                zip.push($(vals).val());
            })

            updateObj = {
                phone_type: phone_ty,
                phone_number: phone_nu,
                email_ids: email_ids,
                suite: suite,
                street: street,
                country: country,
                states: states,
                city: city,
                zip: zip,
                record_id: $("#record_id").val(),
                "_token": "{{ csrf_token() }}",
                tabName
            }
        } else if(tabName == 'Employment_Details'){
            updateObj = {
                employee_status: $("#emp_status").val(),
                employee_type: $("#emp_type").val(),
                hire_date: $("#hire_date").val(),
                termination_date: $("#termination_date").val(),
                hourly_rate: $("#hourly_rate").val(),
                commission_rate: $("#commission_rate").val(),
                record_id: $("#record_id").val(),
                "_token": "{{ csrf_token() }}",
                tabName
            }
        } else if(tabName == 'Login_Permissions'){
            var tfa = ''; var isActive = 1;

            if($("#status_inactive").prop('checked')){
                isActive = 0;
            }
            if($("#two_factor_auth_disabled").prop('checked')){
                tfa = $("#two_factor_auth_disabled").val();
            } else if($("#two_factor_auth_once_per_comp").prop('checked')){
                tfa = $("#two_factor_auth_once_per_comp").val();
            } else if($("#two_factor_auth_every_login").prop('checked')){
                tfa = $("#two_factor_auth_every_login").val();
            }
            updateObj = {
                username: $("#username").val(),
                email: $("#email").val(),
                driving_license: $("#driving_license").val(),
                password: $("#password").val(),
                two_factor_authentication: tfa,
                chat_prmsn: $("#chat_status").prop("checked") ? 1 : 0 ,
                incoming_call_prmsn: $("#incoming_call_status").prop('checked') ? 1 : 0,
                customer_chat_prmsn: $("#customer_chat_status").prop('checked') ? 1 : 0,
                chat_admin_prmsn: $("#chat_admin_status").prop('checked') ? 1 : 0,
                call_center_contact_prmsn: $("#call_center_contact_status").prop('checked') ? 1 : 0,
                billing_user_mgmt_prmsn: $("#billing_user_mgmt_status").prop('checked') ? 1 : 0,
                prop_app_user_prmsn: $("#pro_app_user_status").prop('checked') ? 1 : 0,
                foreman_prmsn: $("#foreman_status").prop('checked') ? 1 : 0,
                groups_access: $("#groups_status").val(),
                is_active: isActive,
                "_token": "{{ csrf_token() }}",
                record_id: $("#record_id").val(),
                tabName
            }
        } else if(tabName == 'Payroll'){

        }
        
        $.post( '/admin/employees/update', updateObj)
        .done(function( result ) {
            alert(result.data)
        });
    });

    $("#createPayroll").click(function(){
        var updateObj;
        var pcs = $("#payrollLog_class_select").val();
        var tabName = $("#tabName").val();
        if(pcs == 'moving') {
            updateObj = {
                "payrollLog_class_select" : $("#payrollLog_class_select").val(),
                "payrollLog_company_branch" : $("#payrollLog_company_branch").val(),
                "payrollLog_entry_date" : $("#payrollLog_entry_date").val(),
                "payrollLog_class" : $("#payrollLog_class").val(),
                "payrollLog_customer" : $("#payrollLog_customer").val(),
                "payrollLog_hours" : $("#payrollLog_hours").val(),
                "payrollLog_travel" : $("#payrollLog_travel").val(),
                "payrollLog_commission" : $("#payrollLog_commission").val(),
                "payrollLog_tip_amount" : '--',
                "payrollLog_description": '--',
                "payrollLog_hourly_rate" : '--',
                "_token": "{{ csrf_token() }}",
                record_id: $("#record_id").val(),
                tabName
            }
        } else if(pcs == 'miscHours'){
            updateObj = {
                "payrollLog_class_select" : $("#payrollLog_class_select").val(),
                "payrollLog_company_branch" : $("#payrollLog_company_branch").val(),
                "payrollLog_entry_date" : $("#payrollLog_entry_date").val(),
                "payrollLog_class" : $("#payrollLog_class").val(),
                "payrollLog_hourly_rate" : $("#payrollLog_hourly_rate").val(),
                "payrollLog_hours" : $("#payrollLog_hours").val(),
                "payrollLog_description" : $("#payrollLog_description").val(),
                "payrollLog_customer" : '--',
                "payrollLog_travel" : '--',
                "payrollLog_commission" : '--',
                "payrollLog_tip_amount" : '--',
                "_token": "{{ csrf_token() }}",
                record_id: $("#record_id").val(),
                tabName
            }
        } else if(pcs == 'tip'){
            updateObj = {
                "payrollLog_class_select" : $("#payrollLog_class_select").val(),
                "payrollLog_company_branch" : $("#payrollLog_company_branch").val(),
                "payrollLog_entry_date" : $("#payrollLog_entry_date").val(),
                "payrollLog_class" : $("#payrollLog_class").val(),
                "payrollLog_customer" : $("#payrollLog_customer").val(),
                "payrollLog_hourly_rate" : '--',
                "payrollLog_hours" : '--',
                "payrollLog_travel" : '--',
                "payrollLog_commission" : '--',
                "payrollLog_tip_amount" : $("#payrollLog_tip_amount").val(),
                "payrollLog_description" : '--',
                "_token": "{{ csrf_token() }}",
                record_id: $("#record_id").val(),
                tabName
            }
        } else if(pcs == 'bonus'){
            updateObj = {
                "payrollLog_class_select" : $("#payrollLog_class_select").val(),
                "payrollLog_company_branch" : $("#payrollLog_company_branch").val(),
                "payrollLog_entry_date" : $("#payrollLog_entry_date").val(),
                "payrollLog_class" : $("#payrollLog_class").val(),
                "payrollLog_customer" : $("#payrollLog_customer").val(),
                "payrollLog_hourly_rate" : '--',
                "payrollLog_hours" : '--',
                "payrollLog_travel" : '--',
                "payrollLog_commission" : '--',
                "payrollLog_tip_amount" : $("#payrollLog_tip_amount").val(),
                "payrollLog_description" : $("#payrollLog_description").val(),
                "_token": "{{ csrf_token() }}",
                record_id: $("#record_id").val(),
                tabName
            }
        } else if(pcs == 'deduction'){
            updateObj = {
                "payrollLog_class_select" : $("#payrollLog_class_select").val(),
                "payrollLog_company_branch" : $("#payrollLog_company_branch").val(),
                "payrollLog_entry_date" : $("#payrollLog_entry_date").val(),
                "payrollLog_class" : $("#payrollLog_class").val(),
                "payrollLog_customer" : $("#payrollLog_customer").val(),
                "payrollLog_hourly_rate" : '--',
                "payrollLog_hours" : '--',
                "payrollLog_travel" : '--',
                "payrollLog_commission" : '--',
                "payrollLog_tip_amount" : $("#payrollLog_tip_amount").val(),
                "payrollLog_description" : $("#payrollLog_description").val(),
                "_token": "{{ csrf_token() }}",
                record_id: $("#record_id").val(),
                tabName
            }
        } else if(pcs == 'perdiem'){
            updateObj = {
                "payrollLog_class_select" : $("#payrollLog_class_select").val(),
                "payrollLog_company_branch" : $("#payrollLog_company_branch").val(),
                "payrollLog_entry_date" : $("#payrollLog_entry_date").val(),
                "payrollLog_class" : $("#payrollLog_class").val(),
                "payrollLog_customer" : $("#payrollLog_customer").val(),
                "payrollLog_hourly_rate" : '--',
                "payrollLog_hours" : '--',
                "payrollLog_travel" : '--',
                "payrollLog_commission" : '--',
                "payrollLog_tip_amount" : $("#payrollLog_tip_amount").val(),
                "payrollLog_description" : $("#payrollLog_description").val(),
                "_token": "{{ csrf_token() }}",
                record_id: $("#record_id").val(),
                tabName
            }
        }
        // updateObj = {
        //     "payrollLog_class_select" : $("#payrollLog_class_select").val(),
        //     "payrollLog_company_branch" : $("#payrollLog_company_branch").val(),
        //     "payrollLog_entry_date" : $("#payrollLog_entry_date").val(),
        //     "payrollLog_class" : $("#payrollLog_class").val(),
        //     "payrollLog_customer" : $("#payrollLog_customer").val(),
        //     "payrollLog_hourly_rate" : $("#payrollLog_hourly_rate").val(),
        //     "payrollLog_hours" : $("#payrollLog_hours").val(),
        //     "payrollLog_travel" : $("#payrollLog_travel").val(),
        //     "payrollLog_commission" : $("#payrollLog_commission").val(),
        //     "payrollLog_tip_amount" : $("#payrollLog_tip_amount").val(),
        //     "payrollLog_description" : $("#payrollLog_description").val(),
        //     "_token": "{{ csrf_token() }}",
        //     record_id: $("#record_id").val(),
        //     tabName
        // }
        $.post( '/admin/employees/update', updateObj)
        .done(function( result ) {
             alert(result.data)
        });
    });
    
});


// Phone Additions

// Add/Remove phone Numbers


// $('#addButton').on('click', function () {


function addPhoneNumber(){
    var $container = $('.addmem');
    var $row = $('.addrow').last();
    var $add = $('#addButton');
    var $remove = $('.deletebatchmember');
    var $focused;

    $container.on('click', 'input', function () {
        $focused = $(this);
    });
    var isValidatedRow;
    $( "div.addrow").each(function(){
        console.log($(this).html());
        console.log($(this).children().find("select.inputName").val())
        console.log($(this).children().find("input.inputMobile").val())
        // console.log($(this).children().find("input.inputEmail").val())
        var inputName = $(this).find("select.inputName").val();
        var inputMobile  = $(this).find("input.inputMobile").val();
        // var inputEmail = $(this).find("input.inputEmail").val();
        console.log(inputName, inputMobile);
        if(inputName==""){
            $(this).find("select.inputName").addClass('error');
        } else {
            $(this).find("select.inputName").removeClass('error');
        }
        if(inputMobile==""){
            $(this).find("input.inputMobile").addClass('error');
        } else {
            $(this).find("input.inputMobile").removeClass('error');
        }
        // if(inputEmail==""){
        //     $(this).find("input.inputEmail").addClass('error')
        // } else {
        //     $(this).find("input.inputEmail").removeClass('error')
        // }
        
        if(inputMobile && inputMobile != "" && inputName && inputName != ""){
            isValidatedRow = true;
        } else {
            isValidatedRow = false;
        }
    });
    if(isValidatedRow){
        var $newRow = $row.clone().insertAfter('.addrow:last');
        $newRow.find('input').each(function () {
            this.value = '';
        });
        console.log($('.addrow:last i.deletebatchmember').html())
        $('.addrow:last i.deletebatchmember').on('click', function(){
            updateDeleteEvents(this)
        })
        trashButtonUpdates('.addrow', '.deletebatchmember');
    }
}
// });

// $remove.on('click', 
function removePhoneNumber(that) {
    // var $container = $('.addmem');
    // var $row = $('.addrow').last();
    // var $add = $('#addButton');
    // var $remove = $('.deletebatchmember');
    var $focused = false;
    if($('.addrow').length > 1){
        $focused = true;
    }
    // $container.on('click', 'input', function () {
    //     $focused = $(this);
    // });
    if (!$focused) {
        alert('Select a row to delete (click en input with it)');
        return;
    }
    var $currentRow = $(that).closest('.addrow');
    if ($('.addrow').length === 1) {
        alert("Atlease one batch member required");
    } else {
        $(that).parent().parent().remove();
    }
    trashButtonUpdates('.addrow', '.deletebatchmember');
}
// );



function updateDeleteEvents(that, clsName){
    if ($(that).length === 1) {
        alert("Atlease one batch member required");
    } else {
        $(that).parent().parent().parent().remove();
    }
    trashButtonUpdates(that, clsName)
}

function trashButtonUpdates(row, deleterow){
    if($(row).length > 1){
        $(deleterow).show();
    } else {
        $(deleterow).hide();
    }
}
// End Phone Number Addition


// Email Id


function addEmailNumber(){
    var $containerEmail = $('.emailMem');
    var $rowEmail = $('.addemailrow');
    var $addEmail = $('#addEmail');
    var $removeEmail = $('.deletebatchemail');
    var $focusedEmail;

    $containerEmail.on('click', 'input', function () {
        $focusedEmail = $(this);
    });
    var isValidatedRow;
    $( " div.addemailrow").each(function(){
        console.log($(this).children().find("input.inputEmail").val())
        var inputEmail = $(this).find("input.inputEmail").val();
        
        if(inputEmail==""){
            $(this).find("input.inputEmail").addClass('error')
        } else {
            $(this).find("input.inputEmail").removeClass('error')
        }
        
        if(inputEmail && inputEmail != ""){
            isValidatedRow = true;
        } else {
            isValidatedRow = false;
        }
    });
    if(isValidatedRow){
        var $newRowEmail = $rowEmail.clone().insertAfter('.addemailrow:last');
        $newRowEmail.find('input').each(function () {
            this.value = '';
        });
        console.log($('.addemailrow:last i.deletebatchemail').html())
        $('.addemailrow:last i.deletebatchemail').on('click', function(){
            updateDeleteEvents(this)
        })
        trashButtonUpdates('.addemailrow', '.deletebatchemail');
    }
}
// });

function removeEmail(that){
    // $removeEmail.on('click', function () {
    var $focusedEmail = false;
    if($('.addemailrow').length > 1){
        $focusedEmail = true;
    }
    if (!$focusedEmail) {
        alert('Select a row to delete (click en input with it)');
        return;
    }
    var $currentRow = $focusedEmail.closest('.addemailrow');
    if ($('.addemailrow').length === 1) {
        alert("Atlease one batch member required");
    } else {
        $(that).parent().parent().remove();
    }
    trashButtonUpdates('.addemailrow', '.deletebatchemail');
// });
}



// Address
// Address Id


// $('#addaddressMem').on('click', function () {
function addAddress(){
    var $containerAddress = $('.addressMem');
    var $rowAddress= $('.addAddressrow');
    var $addAddress= $('#addaddressMem');
    var $removeAddress= $('.deleteaddressMem');
    var $focusedAddress;

    $containerAddress.on('click', 'input', function () {
        $focusedAddress= $(this);
    });
    var isValidatedRow;
    $( " div.addAddressrow").each(function(){
        var addressDetails = $(this).find(".addressDetails").text();
        
        // if(addressDetails==""){
        //     $(this).find("input.addressDetails").addClass('error')
        // } else {
        //     $(this).find("input.addressDetails").removeClass('error')
        // }
        
        if(addressDetails && addressDetails != ""){
            isValidatedRow = true;
        } else {
            isValidatedRow = false;
        }
    });
    if(isValidatedRow){
        var $newRowAddress = $rowAddress.clone().insertAfter('.addAddressrow:last');
        $newRowAddress.find('input').each(function () {
            this.value = '';
        });
        $newRowAddress.find('select.statesNames').html('<option value="">Select State</option>')
        console.log($('.addAddressrow:last i.deleteaddressMem').html())
        $('.addAddressrow:last i.deleteaddressMem').on('click', function(){
            updateDeleteEvents(this)
        })
        trashButtonUpdates('.addAddressrow', '.deleteaddressMem');
    }
}

// $removeAddress.on('click', function () {
function removeAddress(that) {
    var $focusedAddress = false;
    if($('.addAddressrow').length > 1){
        $focusedAddress = true;
    }
    if (!$focusedAddress) {
        alert('Select a row to delete (click en input with it)');
        return;
    }
    var $currentRow = $(that).closest('.addAddressrow');
    if ($('.addAddressrow').length === 1) {
        alert("Atlease one batch member required");
    } else {
        $(that).parent().parent().remove();
    }
    trashButtonUpdates('.addAddressrow', '.deleteaddressMem');
}
//);

function getStates(that){
// $('.countryselection').on('change',function(){
    var id = $(that).val()
    $.get( "/admin/states/get/"+id, function(result) {
        var data = result.data;
        var statesHtml = '<option value="">Select State</option>';
        if(data && data.length > 0){
            data.map(function(d, i){
                statesHtml += `<option value='${d.id}'>${d.name}</option>`;
            })
        }
        $(that).parent().parent().next().find('select.statesNames').html(statesHtml)
    })
    // })
} 
</script>
@endsection
 