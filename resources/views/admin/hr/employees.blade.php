@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Employees
@endsection

@section('content')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{ Form::open(array('onsubmit' => '','route' => 'employees.create','files'=>true)) }}

<!-- Starts List Page -->
<div class="content-wrapper container-xxl p-0">
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            <div class="">
                        
                <div class="table-bar2">
                    <strong>Admin / HR / Employees</strong>   
                    @include('admin.components.message')
                </div>
                <div class="table-bar2">
                    <div class="btn-group btn-align-right-add-update">
                        <div class="btn btn-primary cursorPoint" onclick="window.location='employees_createform'" id="myBtn">New Employee</div>
                    </div>
                </div>
                    
                    <!-- <div class="btn-group pull-right">
                        <a href="{{URL::to('admin/rooms/create')}}" class="btn btn-info">
                            <i class="fa fa-add"></i>Add Fleet Type</a>
                    </div> -->
                    <div class="table-bar2">
                    <div id="tabsneW" style="border: 0px; width:50%">
                            <ul>
                                <li class="navTabs" data-id="active" ><a href="#active">Active</a></li>
                                <li class="navTabs" data-id="inactive" ><a href="#inactive">In active</a></li>
                            </ul>
                        </div>
                        <!-- <div class="btn-group pull-right">
                            <button class="btn btn-primary" id="myBtn">New Fleet Types</button>
                        </div> -->
                        <table id="data-table" class="table table-striped" style="width:100%">
                            <thead class="thead">
                                <tr>
                                    <th>Emp No</th>
                                    <th>Name</th>
                                    <th>Phone Numbers</th>
                                    <th>Designation</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($results as $result)
                                <tr>
                                    <td>{{ $result->emp_no }}</td>
                                    <td>{{ $result->first_name }}</td>
                                    <td>{{ $result->phone_numbers }}</td>
                                    <td>{{ $result->designation }}</td>
                                    <td><a href="employees/edit/{{$result->id}}"><i class="fa fa-edit cursorPoint editRec" data-id="{{$result->id}}"></i></a> <a href="employees/delete/{{$result->id}}"><i class="fa fa-trash cursorPoint objDelete" data-id="{{$result->id}}"></i></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                <!-- </div> -->
                <!--  -->

            </div>
        </section>
    </div>
</div>
<!-- Ends List Page -->
<script>
    $( function() {
    $( "#tabsneW" ).tabs();
     
    // $('#customers2').dataTable();
  });
    $(".navTabs").click(function(){
      var id = $(this).attr('data-id');
      getBasedStatus(id)
    });
    
    function getBasedStatus(id){
        var status = id === 'active' ? 1 : 0;
    $.get("employees/status/"+status, function(res) {
        console.log(res);
        $('#data-table').dataTable().fnDestroy();
        if(res && res.data && res.data.length > 0 ){
          var data = res.data[0];
          var htmlTask = `<thead>
            <tr>
                <th>Emp No</th>
                <th>Name</th>
                <th>Phone Numbers</th>
                <th>Designation</th>
            </tr>
          </thead><tbody>`;
          res.data.map((j, i)=>{
          htmlTask += `<tr id="statuswise">
              <td>
              ${j.emp_no}
              </td>
              <td>
              ${j.name}
              </td>
              <td>
              ${j.phone_numbers}
              </td>
              <td>
              ${j.desgination}
              </td>
              <td width="15%">
                <div >
                  <div style="float:left;padding:0 10px 10px 0;">
                        <i class="fa fa-edit cursorPoint taskedit" data-id="${j.id}"></i>
                    </div>
                    <div style="float:left;padding-right:10px;">
                      <i class="fa fa-trash cursorPoint taskdelete" data-id="${j.id}"></i>
                    </div>
                </div>
              </td>
            </tr>`
          });
          htmlTask += `</tbody>`;
          $("#data-table").html(htmlTask);
          $('#data-table').addClass('table table-striped data')
          // $('#customers2').dataTable();
        } else {
          htmlTask += `<tr>
              <td colspan="10">No data available</td>
              </tr>`

          }
      }).fail(function() {
        console.log("fail")
      }).done(function() {
        console.log("completed");
      })
  }
</script>
@endsection