@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}} - Employees Create
@endsection

@section('content')
<!-- {{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}} -->

{{ Form::open(array('onsubmit' => '','route' => 'employees.create','files'=>true)) }}
<style>
  input.error {
      border: solid 0.5px red;
  }
  select.error {
      border: solid 0.5px red;
  }
</style>
<!-- Create Emp Page -->
<div class="content-wrapper container-xxl p-0">
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            <div class="">
            
            <div class="table-bar2">
                <strong>Admin / HR / Create Employees</strong>   
            </div>
            
            @include('admin.components.message')
                <div class="table-bar2">
                    <!--  Starts here  -->
                    <div class=""> 
                      <form name="createEmpForm" id="createEmpForm" method="post">
                          <input id='record_id' name="record_id" type="hidden" />
                          <div class="container">
                              <div class="row">
                                  <div class='col col-6'>
                                    <div class="row col-sm-12">
                                        <div class="col col-sm-4">
                                            <label class="control-label">First Name</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input class="form-control ui-autocomplete-input required" type="text" name="first_name" id="first_name" autocomplete="off">
                                            <label for="first_name"></label>
                                        </div>
                                    </div>
                                    <div class="row col-sm-12">
                                        <div class="col col-sm-4">
                                          <label class="control-label">Middle Name</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input class="form-control ui-autocomplete-input required" type="text" name="middle_name" id="middle_name" autocomplete="off">
                                            <label for="middle_name"></label>
                                        </div>
                                    </div>
                                    <div class="row col-sm-12">
                                        <div class="col col-sm-4">
                                          <label class="control-label">Last Name</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input class="form-control ui-autocomplete-input required" type="text" name="last_name" id="last_name" autocomplete="off">
                                            <label for="last_name"></label>
                                        </div>
                                    </div>
                                    <div class="row col-sm-12">
                                        <div class="col col-sm-4">
                                          <label class="control-label">Nick Name</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input class="form-control ui-autocomplete-input required" type="text" name="nick_name" id="nick_name" autocomplete="off">
                                            <label for="nick_name"></label>
                                        </div>
                                    </div>
                                    <div class="row col-sm-12">
                                        <div class="col col-sm-4">
                                          <label class="control-label">User Name</label>
                                        </div>
                                        <div class="col col-sm-8">
                                            <input class="form-control ui-autocomplete-input required" type="text" name="user_name" id="user_name" autocomplete="off">
                                            <label for="user_name"></label>
                                        </div>
                                    </div>
                                    <div class="row col-sm-12">
                                      <div class="col col-sm-4">
                                        <label class="control-label">Employee Designation</label>
                                      </div>
                                      <div class="col col-sm-8">
                                          <select name='designation' id="designation"  class="form-select ui-autocomplete-input required">
                                            <option value=''>Select</option>
                                            <option value='Office'>Office</option>
                                            <option value='Driver'>Driver</option>
                                            <option value='Helper'>Helper</option>
                                            <option value='Hide'>Hide</option>
                                            <option value='Crew_Lead'>Crew Lead</option>
                                          </select>
                                          <label for="designation"></label>
                                      </div>
                                    </div>
                                    <div class="row col-sm-12">
                                      <div class="col col-sm-4">
                                        <label class="control-label">Time Zone</label>
                                      </div>
                                      <div class="col col-sm-8">
                                          <select name='time_zone' id="time_zone"  class="form-select ui-autocomplete-input required">
                                            <option value=''>Select Timezone</option>
                                            @foreach($tzs as $tz)
                                            <option value='Office'>{{$tz->label}}</option>
                                            @endforeach
                                          </select>
                                          <label for="time_zone"></label>
                                      </div>
                                    </div>
                                    <div class="row col-sm-12">
                                      <div class="col col-sm-4">
                                        <label class="control-label">S.S.N</label>
                                      </div>
                                      <div class="col col-sm-8">
                                            <input class="form-control ui-autocomplete-input required" type="text" name="ssn" id="ssn" autocomplete="off">
                                            <label for="ssn"></label>
                                      </div>
                                    </div>
                                    <div class="row col-sm-12">
                                      <div class="col col-sm-4">
                                        <label class="control-label">Date of Birth</label>
                                      </div>
                                      <div class="col col-sm-8">
                                            <input class="form-control ui-autocomplete-input datepicker required" type="text" name="date_of_birth" id="date_of_birth" autocomplete="off">
                                            <label for="date_of_birth"></label>
                                      </div>
                                    </div>
                                    <div class="row col-sm-12">
                                      <div class="col col-sm-4">
                                        <label class="control-label">Gender</label>
                                      </div>
                                      <div class="col col-sm-8">
                                          <select name='gender' id="gender"  class="form-select ui-autocomplete-input required">
                                            <option value=''>Select</option>
                                            <option value='Male'>Male</option>
                                            <option value='Female'>Female</option>
                                          </select>
                                          <label for="gender"></label>
                                      </div>
                                    </div>
                                    <div class="row col-sm-12">
                                      <div class="col col-sm-4">
                                        <label class="control-label">Relationship</label>
                                      </div>
                                      <div class="col col-sm-8">
                                          <select name='relationship' id="relationship"  class="form-select ui-autocomplete-input required">
                                            <option value=''>Select</option>
                                            <option value='Single'>Single</option>
                                            <option value='Married'>Married</option>
                                            <option value='Divorced'>Divorced</option>
                                            <option value='Widowed'>Widowed</option>
                                          </select>
                                          <label for="relationship"></label>
                                      </div>
                                    </div>
                                     <!-- Phone Number Addition -->
                                    <div class="col row">
                                      <div class="col col-sm-4" >
                                        <label class="control-label">Phone Number</label>
                                      </div>
                                      <div  class="col col-sm-8"  style="max-width:70%">
                                        <div class="addmem">
                                            <!-- <label class="contact-textfield-label" for="">Phone Number</label> class="col col-sm-4" -->
                                            <div class="row addrow">
                                                <div class="form-group row">
                                                  <select name="phone_type[]"   class="form-select inputName ui-autocomplete-select required">
                                                    <option value=''>Select</option>
                                                    <option value='Mobile_Phone'>Mobile Phone</option>
                                                    <option value='Business_Phone'>Business Phone</option>
                                                    <option value='Home_Phone'>Home Phone</option>
                                                    <option value='Fax'>Fax</option>
                                                    <option value='Call_Center_Contact'>Call Center Contact</option>
                                                  </select>
                                                  <input type="text" class="form-control  inputMobile required"  id="phone_number"  placeholder="Mobile Number" name="phone_number[]" />
                                                  <i class="fa fa-trash deletebatchmember cursorPoint" style="display: none; text-align:right;"></i>
                                                </div>
                                            </div>
                                          </div>
                                          <input type="button" class="btn btn-success btn-align-right-add-update" value='Add Phone Number' id='addButton' />
                                      </div>
                                    </div>
                                    <!-- End Number Addition -->

                                    <!-- Email Id Number Addition -->
                                    <div class="col row">
                                      <div class="col col-sm-4" >
                                        <label class="control-label">Email</label>
                                      </div>
                                      <div  class="col col-sm-8"  style="max-width:70%">
                                        <div class="emailMem">
                                            <!-- <label class="contact-textfield-label" for="">Phone Number</label> class="col col-sm-4" -->
                                            <div class="row addemailrow">
                                                <div class="form-group row">
                                                <select name="email_type[]" class="form-select inputEmailType ui-autocomplete-select required">
                                                    <option value=''>Select</option>
                                                    <option value='Corporate'>Corporate</option>
                                                    <option value='Personal'>Personal</option>
                                                </select>
                                                  <input type="text" class="form-control  inputEmail required" placeholder="" name="email[]" />
                                                  <i class="fa fa-trash deletebatchemail cursorPoint" style="display: none; text-align:right;"></i>
                                                </div>
                                            </div>
                                          </div>
                                          <input type="button" class="btn btn-success btn-align-right-add-update" value='Add Email' id='addEmail' />
                                      </div>
                                    </div>
                                    <!-- End Email Addition -->

                                   <!-- Email Id Number Addition -->
                                   <div class="col row"><strong>Address</strong></div>
                                   <div class="col row">
                                      <!-- <div class="col col-sm-4" >
                                        <label class="control-label">Address</label>
                                      </div> -->
                                      <div  class="col col-sm-12"  style="max-width:100%">
                                        <div class="addressMem">
                                            <!-- <label class="contact-textfield-label" for="">Phone Number</label> class="col col-sm-4" -->
                                            <div class="row addAddressrow">
                                                <div class="form-group row">
                                                   <div class="addressDetails">
                                                     <div class="row col-sm-12">
                                                      <div id="address_div" class=" col-sm-12">
                                                        <div class="col col-sm-12 row jsonData">
                                                            <div class="col col-sm-4">
                                                                <label class="control-label">Street</label>
                                                            </div>
                                                            <div class="col col-sm-8">
                                                                <input class="form-control ui-autocomplete-input required" type="text" name="street[]" id="street" autocomplete="off">
                                                                <label for="email"></label>
                                                            </div>
                                                        </div>
                                                        <div class="col col-sm-12 row jsonData">
                                                            <div class="col col-sm-4">
                                                                <label class="control-label">Suite</label>
                                                            </div>
                                                            <div class="col col-sm-8">
                                                                <input class="form-control ui-autocomplete-input required" type="text" name="suite[]" id="suite" autocomplete="off">
                                                                <label for="email"></label>
                                                            </div>
                                                        </div>
                                                        <div class="col col-sm-12 row jsonData">
                                                            <div class="col col-sm-4">
                                                                <label class="control-label">Country</label>
                                                            </div>
                                                            <div class="col col-sm-8">
                                                                    <select name="country[]" onchange='getStates(this)' class="form-select inputName countryselection ui-autocomplete-select required">
                                                                      <option value=''>Select Country</option>
                                                                      @foreach($countries as $country)
                                                                        <option value='{{$country->id}}'>{{$country->name}}</option>
                                                                        @endforeach
                                                                    </select>    
                                                                <label for="country"></label>
                                                            </div>
                                                        </div>
                                                        <div class="col col-sm-12 row jsonData">
                                                            <div class="col col-sm-4">
                                                                <label class="control-label">State</label>
                                                            </div>
                                                            <div class="col col-sm-8">
                                                                  <select name="states[]" class="form-select statesNames inputName ui-autocomplete-select required">
                                                                      <option value=''>Select State</option>
                                                                  </select> 
                                                                <label for="state"></label>
                                                            </div>
                                                        </div>
                                                        <div class="col col-sm-12 row jsonData">
                                                            <div class="col col-sm-4">
                                                                <label class="control-label">City</label>
                                                            </div>
                                                            <div class="col col-sm-8">
                                                                <input class="form-control ui-autocomplete-input required" type="text" name="city[]" autocomplete="off">
                                                                <label for="email"></label>
                                                            </div>
                                                        </div>
                                                        <div class="col col-sm-12 row jsonData">
                                                            <div class="col col-sm-4">
                                                                <label class="control-label">Zip</label>
                                                            </div>
                                                            <div class="col col-sm-8">
                                                                <input class="form-control ui-autocomplete-input required" type="text" name="zip[]" autocomplete="off">
                                                                <label for="email"></label>
                                                            </div>
                                                        </div>
                                                      </div>
                                                    </div></div>
                                                  <i class="fa fa-trash deleteaddressMem cursorPoint" style="display: none; text-align:right;"></i>
                                                  <hr/>
                                                </div>
                                            </div>
                                          </div>
                                          <input type="button" class="btn btn-align-right-add-update btn-success text-right" value='Add Address' id='addaddressMem' />
                                      </div>
                                    </div>
                                    <!-- End Email Addition -->
                                    
                                    
                                  </div>
                                  <!-- HTML Upload -->
                                  <div class='col col-6'>
                                    <div class="row col-sm-12">
                                          <div class="col col-sm-12">
                                              <label class="control-label">Upload Photo</label>
                                              <input class="form-control ui-autocomplete-input required" type="file" name="employee_photo" id="employee_photo" autocomplete="off">
                                              <label for="employee_photo"></label>
                                          </div>
                                    </div>
                                    <div class="row col-sm-12">
                                          <div class="col col-sm-12">
                                              <label class="control-label">Signature</label>
                                              <input class="form-control ui-autocomplete-input required" type="file" name="employee_signature" id="employee_signature" autocomplete="off">
                                              <label for="employee_signature"></label>
                                          </div>
                                    </div>
                                     
                                  </div>
                                  <div class="row form-group">
                                      <div class="col col-md text-center">
                                      <input type="submit" id="employeeCreate" value="Create " class="pull-right btn btn-primary" />
                                      <input type="button" value="Cancel" onClick="hidePopUp()" class="pull-left btn btn-danger" />
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </form>
                    <!--  ends here  -->
                </div>
                 
            </div>
        </section>
    </div>
</div>
<!-- Ends Emp Page -->
<script>
$( function() {
    $('#date_of_birth').datepicker({
        format: 'mm/dd/yyyy',
        startDate: 'd',
        maxDate: '0',
        changeMonth: true,
        changeYear: true,
    });
});
// Phone Additions

// Add/Remove phone Numbers
var $container = $('.addmem');
var $row = $('.addrow');
var $add = $('#addButton');
var $remove = $('.deletebatchmember');
var $focused;

$container.on('click', 'input', function () {
    $focused = $(this);
});

$('#addButton').on('click', function () {
    var isValidatedRow;
    $( " div.addrow").each(function(){
        // console.log($(this).children().find("input.inputEmail").val())
        var inputName = $(this).find("select.inputName").val();
        var inputMobile  = $(this).find("input.inputMobile").val();
        // var inputEmail = $(this).find("input.inputEmail").val();
        console.log(inputName, inputMobile);
        if(inputName==""){
            $(this).find("select.inputName").addClass('error');
        } else {
            $(this).find("select.inputName").removeClass('error');
        }
        if(inputMobile==""){
            $(this).find("input.inputMobile").addClass('error');
        } else {
            $(this).find("input.inputMobile").removeClass('error');
        }
        // if(inputEmail==""){
        //     $(this).find("input.inputEmail").addClass('error')
        // } else {
        //     $(this).find("input.inputEmail").removeClass('error')
        // }
        
        if(inputMobile && inputMobile != "" && inputName && inputName != ""){
            isValidatedRow = true;
        } else {
            isValidatedRow = false;
        }
    });
    if(isValidatedRow){
        var $newRow = $row.clone().insertAfter('.addrow:last');
        $newRow.find('input').each(function () {
            this.value = '';
        });
        console.log($('.addrow:last i.deletebatchmember').html())
        $('.addrow:last i.deletebatchmember').on('click', function(){
            updateDeleteEvents(this)
        })
        trashButtonUpdates('.addrow', '.deletebatchmember');
    }
});

$remove.on('click', function () {
    if (!$focused) {
        alert('Select a row to delete (click en input with it)');
        return;
    }
    var $currentRow = $focused.closest('.addrow');
    if ($('.addrow').length === 1) {
        alert("Atlease one batch member required");
    } else {
        $(this).parent().parent().remove();
    }
    trashButtonUpdates('.addrow', '.deletebatchmember');
});



function updateDeleteEvents(that, clsName){
    if ($(that).length === 1) {
        alert("Atlease one batch member required");
    } else {
        $(that).parent().parent().parent().remove();
    }
    trashButtonUpdates(that, clsName)
}

function trashButtonUpdates(row, deleterow){
    if($(row).length > 1){
        $(deleterow).show();
    } else {
        $(deleterow).hide();
    }
}
// End Phone Number Addition


// Email Id
var $containerEmail = $('.emailMem');
var $rowEmail = $('.addemailrow');
var $addEmail = $('#addEmail');
var $removeEmail = $('.deletebatchemail');
var $focusedEmail;

$containerEmail.on('click', 'input', function () {
    $focusedEmail = $(this);
});

$('#addEmail').on('click', function () {
    var isValidatedRow;
    $( " div.addemailrow").each(function(){
        var inputEmailType = $(this).find("select.inputEmailType").val();
        var inputEmail = $(this).find("input.inputEmail").val();
        
        if(inputEmailType==""){
            $(this).find("select.inputName").addClass('error');
        } else {
            $(this).find("select.inputName").removeClass('error');
        }
        if(inputEmail==""){
            $(this).find("input.inputEmail").addClass('error')
        } else {
            $(this).find("input.inputEmail").removeClass('error')
        }
        
        if(inputEmail && inputEmail != ""){
            isValidatedRow = true;
        } else {
            isValidatedRow = false;
        }
    });
    if(isValidatedRow){
        var $newRowEmail = $rowEmail.clone().insertAfter('.addemailrow:last');
        $newRowEmail.find('input').each(function () {
            this.value = '';
        });
        console.log($('.addemailrow:last i.deletebatchemail').html())
        $('.addemailrow:last i.deletebatchemail').on('click', function(){
            updateDeleteEvents(this)
        })
        trashButtonUpdates('.addemailrow', '.deletebatchemail');
    }
});

$removeEmail.on('click', function () {
    if (!$focusedEmail) {
        alert('Select a row to delete (click en input with it)');
        return;
    }
    var $currentRow = $focusedEmail.closest('.addemailrow');
    if ($('.addemailrow').length === 1) {
        alert("Atlease one batch member required");
    } else {
        $(this).parent().parent().remove();
    }
    trashButtonUpdates('.addemailrow', '.deletebatchemail');
});


// Address
// Address Id
var $containerAddress = $('.addressMem');
var $rowAddress= $('.addAddressrow');
var $addAddress= $('#addaddressMem');
var $removeAddress= $('.deleteaddressMem');
var $focusedAddress;

$containerAddress.on('click', 'input', function () {
    $focusedAddress= $(this);
});

$('#addaddressMem').on('click', function () {
    var isValidatedRow;
    $( " div.addAddressrow").each(function(){
        console.log($(this).children().find(".addressDetails").text())
        
        var addressDetails = $(this).find(".addressDetails").text();
        
        // if(addressDetails==""){
        //     $(this).find("input.addressDetails").addClass('error')
        // } else {
        //     $(this).find("input.addressDetails").removeClass('error')
        // }
        
        if(addressDetails && addressDetails != ""){
            isValidatedRow = true;
        } else {
            isValidatedRow = false;
        }
    });
    if(isValidatedRow){
        var $newRowAddress = $rowAddress.clone().insertAfter('.addAddressrow:last');
        $newRowAddress.find('input').each(function () {
            this.value = '';
        });
        $newRowAddress.find('select.statesNames').html('<option value="">Select State</option>')
        console.log($('.addAddressrow:last i.deleteaddressMem').html())
        $('.addAddressrow:last i.deleteaddressMem').on('click', function(){
            updateDeleteEvents(this)
        })
        trashButtonUpdates('.addAddressrow', '.deleteaddressMem');
    }
});

$removeAddress.on('click', function () {
    if (!$focusedAddress) {
        alert('Select a row to delete (click en input with it)');
        return;
    }
    var $currentRow = $focusedAddress.closest('.addAddressrow');
    if ($('.addAddressrow').length === 1) {
        alert("Atlease one batch member required");
    } else {
        $(this).parent().parent().remove();
    }
    trashButtonUpdates('.addAddressrow', '.deleteaddressMem');
});

function getStates(that){
// $('.countryselection').on('change',function(){
    var id = $(that).val()
    $.get( "/admin/states/get/"+id, function(result) {
        var data = result.data;
        var statesHtml = '<option value="">Select State</option>';
        if(data && data.length > 0){
            data.map(function(d, i){
                statesHtml += `<option value='${d.id}'>${d.name}</option>`;
            })
        }
        $(that).parent().parent().next().find('select.statesNames').html(statesHtml)
    })
    // })
} 
</script>
@endsection
 