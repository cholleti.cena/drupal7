@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Estimate Range
@endsection
@section('module')
Estimate Range
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSwitch', 'admin.components.form.switch', ['name', 'labeltext'=>null, 'value' => null, 'checkstatus' => false, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return UserEditValidate();','method' => 'PUT', 'route' => array('units.update',$charge->id),'files'=>true)) }}
<div class="card mb-4">
	 <div class="card-body after-loading-icon">
     <div class="container-fluid">
      	<div class="col-md-6">
			<div class="col-md-6">
            <input type="hidden" name="facility_id" value="{{$facility_id}}">
						{{ Form::ahText('unit_name','Unit Name :',$charge->unit_name,array())  }}	
						{{ Form::ahNumber('width','Width :',$charge->width,array()) }}
						{{ Form::ahNumber('height','Height :',$charge->height,array()) }}
						{{ Form::ahNumber('depth','Depth :',$charge->depth,array()) }}
						{{ Form::ahText('location','Location :',$charge->location,array()) }}
						{{ Form::ahNumber('price','Price :',$charge->price,array()) }}
						{{ Form::ahNumber('quantity','Quantity :',$charge->quantity,array()) }}	
						{{ Form::ahNumber('cuft','Cuft :',$charge->cuft,array())  }}				
						{{ Form::ahNumber('total_occupied','Total Occupied :',$charge->total_occupied,array()) }}
            </br>
		    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('units.index','Cancel',array('facility_id'=>$facility_id), array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	</div>
  </div>
</div>
@endsection