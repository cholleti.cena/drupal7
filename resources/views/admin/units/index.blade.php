@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Facility Units
@endsection

@section('content')
@include('admin.components.message')	

<div class="content-wrapper container-xxl p-0">
        <div class="content-header row"></div>
        <div class="content-body">
          <section id="dashboard-ecommerce">
            <div class="">
                                <div class="btn-group pull-right">
                                    @if($privileges['Add']=='true') 
                                    <a href="{{URL::to('admin/units/create')}}?facility_id={{$facility_id}}" class="btn btn-info"><i class="fa fa-edit"></i>Add Facility Units</a>
                                        @endif
                                </div>
              
                                <div class="table-bar2">
                                    <table id="example" class="table table-striped" style="width:100%">
                                    <thead class="thead">
                                            <tr>
                                                <th>Unit Name</th>
                                                <th>Width</th>
                                                <th>Height</th>
                                                <th>Depth</th>
                                                <th>Cuft</th>
                                                <th>Quantity</th>
                                                <th>Total Occupied</th>
                                                <th>Location</th>
                                                <th>Price</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($units as $unit)
                                    <tr>
                                        <td>
                                            {{$unit->unit_name}}
                                        </td>  
                                        <td>
                                            {{$unit->width}}
                                        </td> 
                                        <td>
                                            {{$unit->height}}
                                        </td>
                                        <td>
                                            {{$unit->depth}}
                                        </td>
                                        <td>
                                            {{$unit->cuft}}
                                        </td>
                                        <td>
                                            {{$unit->quantity}}
                                        </td>  
                                        <td>
                                            {{$unit->total_occupied}}
                                        </td>  
                                            
                                        <td>
                                            {{$unit->location}}
                                        </td>  
                                        <td>
                                            {{$unit->price}}
                                        </td>                 
                                                                             
                                        <td width="20%">
                                            <div >
                                                <div style="float:left;padding:0 10px 10px 0;">
                                                 @if($privileges['Edit']=='true')
                                                {{ link_to_route('units.edit','Edit',array($unit->id,'facility_id'=>$facility_id), array('class' => 'btn btn-info')) }}
                                                @endif 
                                                </div>
                                                <div style="float:left;padding-right:10px;">
                                                   @if($privileges['Delete']=='true')
                                                    {{ Form::open(array('onsubmit' => 'return confirm("Are you sure you want to delete?")','method' => 'DELETE', 'route' => array('units.destroy', $unit->id))) }}
                                                    <button type="submit" class="btn btn-danger btn-xs pull-right" style="font-size: 11px;padding: 11px 12px;">Delete</button>
                                                    {{ Form::close() }}
                                                   @endif
                                                </div>
                                            </div>
                                        </td>
                                         </tr>
                                    @endforeach
                                        </tbody>
                                    </table>
                                    {!! $units->render() !!}
                                </div>
                                
   
                
            </div>
          </section>
        </div>  
        <div class="btn-group pull-right">
            <a href="{{URL::to('admin/facilities')}}" class="btn btn-info"><i class="fa fa-back"></i>Back to Facility</a>
        </div>
      </div>

@endsection