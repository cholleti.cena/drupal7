<head>
  <!--<link rel="stylesheet" 
    href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" 
    crossorigin="anonymous">-->
</head>  
<h1>Labor Price Estimate</h1>
 <div class="col-sm" style="padding-bottom: 20px;">
   <a class="btn btn-info" href="#" data-toggle="modal" data-target="#addlaborModal">Add Labor Rate</a>
 </div>
 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th>People</th>
        <th>Price Per Hour</th>
        <th>Base Price</th>
        <th>Pack Per Hour</th>
        <th>Days</th>
        <th>Action</th>
    </thead>
    <tbody>
      @foreach($laborprices as $laborprice)
        <tr>
            <form method="post" action="{{URL::to('admin/updateEstimateLaborPrice')}}"> 
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{$laborprice->id}}">
            <input type="hidden" name="people" value="{{$laborprice->people}}">
            <td>{{$laborprice->people}}</td>
            <td>$<input type="text" name="price_per_hour" value="{{$laborprice->price_per_hour}}"></td>
            <td>$<input type="text" name="base_price" value="{{$laborprice->base_price}}"></td>
            <td>$<input type="text" name="pack_per_hour" value="{{$laborprice->pack_per_hour}}"></td>
            <td><button class="btn btn-success">Select</button></td>
            <td>
                <div class="row">
                    <button class="col-sm btn btn-success">Update</button>
                     <a class="col-sm" href="deleteEstimateLaborPrice/{{$laborprice->id}}"><i class="fa fa-trash cursorPoint objDelete" data-id="{{$laborprice->id}}"></i></a>
                </div>
            </td>
            </form>
        </tr>
        @endforeach
    </tbody>
 </table>

 <div class="row" style="width:60%">
     <form method="post" action="{{URL::to('admin/updateEstimateLaborHour')}}"> 
     <input type="hidden" name="_token" value="{{ csrf_token() }}">
      @foreach($laborhours as $laborhour)
      <input type="hidden" name="labor_price_estimate_id" value="{{$laborhour->labor_price_estimate_id}}">
     <div class="row" >
         <div class="col-sm">
             <label>Minimum hours</label>
             <input type="text" name="minimum_hours" value="{{$laborhour->minimum_hours}}">
         </div>
         <?php if($laborhour->is_charge_after_minimum_hours)
          {
            $is_charge_after_minimum_hours='checked'; 
          }else {
            $is_charge_after_minimum_hours='';
          }
          ?>
         <div class="col-sm">
             <label>Charge after minimum hours:</label>
             <input type="hidden" name="is_charge_after_minimum_hours" id="is_charge_after_minimum_hours" value="{{$laborhour->is_charge_after_minimum_hours}}">
             <input type="checkbox" onchange="is_charge()" name="is_charge_after_minimum_hours"  value="{{$laborhour->is_charge_after_minimum_hours}}" {{$is_charge_after_minimum_hours}}>
         </div>
     </div>

     <div class="row">
         <div class="col-auto pt-1" >
             <label>Minimum Type:</label>
         </div>
         
          <div class="col-auto" >   
             <input type="radio" name="minimum_type" value="0" {{ ($laborhour->minimum_type=="0")? "checked" : "" }} ><label>Minimum Including Travel Time</label>
            <div>
              <input type="radio" name="minimum_type" value="1" {{ ($laborhour->minimum_type=="1")? "checked" : "" }}><label>Minimum plus Travel Time</label>
            </div>
         </div>         
     </div>

     <div class="row">
         <div class="col-auto pt-1" >
             <label>Inventory Type:</label>
         </div>
          <div class="col-auto" >   
             <input type="radio" name="inventory_type" value="0" {{ ($laborhour->inventory_type=="0")? "checked" : "" }}><label>Percentage Inventory</label>
            <div>
              <input type="radio" name="inventory_type" value="1"  {{ ($laborhour->inventory_type=="1")? "checked" : "" }}><label>Itemized Inventory</label>
            </div>
         </div>         
     </div>

     <div class="row">
         <div class="col-auto pt-1" >
             <label>Inventory Type for Iframe:</label>
         </div>
          <div class="col-auto" >   
             <input type="radio" name="inventory_type_for_iframe" value="0" {{ ($laborhour->inventory_type_for_iframe=="0")? "checked" : "" }}><label>Percentage Inventory</label>
            <div>
              <input type="radio" name="inventory_type_for_iframe" value="1" {{ ($laborhour->inventory_type_for_iframe=="1")? "checked" : "" }}><label> Itemized Inventory</label>
            </div>
         </div>         
     </div>

     <div class="row">
         <div class="col-auto pt-1" >
             <label>Hourly Rate Type:</label>
         </div>
          <div class="col-auto" >   
             <input type="radio" name="hourly_rate_type" value="0" {{ ($laborhour->hourly_rate_type=="0")? "checked" : "" }}><label>Hourly Rate per Day</label>
            <div>
              <input type="radio" name="hourly_rate_type" value="1" {{ ($laborhour->hourly_rate_type=="1")? "checked" : "" }}><label>Hourly Rate per Labour</label>
            </div>
         </div>         
     </div>

     <div class="row">
         <div class="col-auto" >
             <label>Iframe auto-calculate</label>
         </div>
         <?php if($laborhour->is_iframe_auto_calculate)
          {
            $is_iframe_auto_calculate='checked'; 
          }else {
            $is_iframe_auto_calculate='';
          }
          ?>
          <div class="col-auto" > 
             <input type="hidden" name="is_iframe_auto_calculate" id="is_iframe_auto_calculate" value="{{$laborhour->is_iframe_auto_calculate}}">  
             <input type="checkbox" onchange="is_iframe()" name="is_iframe_auto_calculate"  value="{{$laborhour->is_iframe_auto_calculate}}" {{$is_iframe_auto_calculate}}>
         </div>         
     </div>

     <div class="row">
         <div class="col-auto" >
             <label>Sort inventory vertically</label>
         </div>
         <?php if($laborhour->is_sort_inventory_vertically)
          {
            $is_sort_inventory_vertically='checked'; 
          }else {
            $is_sort_inventory_vertically='';
          }
          ?>
          <div class="col-auto" >
             <input type="hidden" name="is_sort_inventory_vertically" id="is_sort_inventory_vertically" value="{{$laborhour->is_sort_inventory_vertically}}">   
             <input type="checkbox" onchange="is_sort()" name="is_sort_inventory_vertically"  value="{{$laborhour->is_sort_inventory_vertically}}"  {{$is_sort_inventory_vertically}}>
         </div>         
     </div>

     <div class="row">
         <div class="col-auto" >
             <label>Default type:</label>
         </div>
         <?php if($laborhour->is_default_type)
          {
            $is_default_type='checked'; 
          }else {
            $is_default_type='';
          }
          ?>
          <div class="col-auto" >
             <input type="hidden" name="is_default_type" id="is_default_type" value="{{$laborhour->is_default_type}}">
             <input type="checkbox" onchange="is_typ()" name="is_default_type"  value="{{$laborhour->is_default_type}}"  {{$is_default_type}}>
              <input type="hidden" name="default_type" id="default_type" value="{{$laborhour->default_type}}">
             <select name="default_type" value="{{$laborhour->default_type}}">
              @foreach($types as $type)
                 <option>{{$type->name}}</option>
              @endforeach 
             </select>
         </div>         
     </div>

     <div class="row">
         <div class="col-auto" >
             <label>Default floors:</label>
         </div>
         <?php if($laborhour->is_default_floors)
          {
            $is_default_floors='checked'; 
          }else {
            $is_default_floors='';
          }
          ?>
          <div class="col-auto" >   
             <input type="hidden" name="is_default_floors" id="is_default_floors" value="{{$laborhour->is_default_floors}}">
             <input type="checkbox" onchange="is_def()" name="is_default_floors"  value="{{$laborhour->is_default_floors}}"  {{$is_default_floors}}>
              <input type="hidden" name="default_floors" value="{{$laborhour->default_floors}}">
             <select name="default_floors" value="{{$laborhour->default_floors}}">
                 @foreach($floors as $floor)
                 <option>{{$floor->flooors}}</option>
              @endforeach 
             </select>
         </div>         
     </div>
     {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
     {{ link_to_route('estimatesettings.index','Cancel',null, array('class' => 'btn btn-danger')) }}
     @endforeach
   </form>
     
 </div> 
 <!-- The Modal -->
  <div class="modal fade" id="addlaborModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#fff;">
          <h4 class="modal-title">Add Labor Rate</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form method="post" action="{{URL::to('admin/addEstimateLaborPrice')}}">  
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row col-sm-12">
              <label class="col-sm-4">People::</label>
              <input type="text" class="col-sm-8" name="people" id="people">
            </div>
            <div class="row col-sm-12">
              <label class="col-sm-4">Price Per Hour:</label>
              <input type="text" class="col-sm-8" name="price_per_hour" id="price_per_hour">
            </div>
            <div class="container-fluid text-center">
                <button class="btn btn-info">Add</button>
                {{ link_to_route('estimatesettings.index','Cancel',null, array('class' => 'btn btn-danger')) }}
             </div>
          </form>
         </div>
      </div>
    </div>
  </div>
   

  <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->



<script type="text/javascript">

  function is_charge() {
    var charge= $('#is_charge_after_minimum_hours').val();
    if (charge==1){
          $('#is_charge_after_minimum_hours').val("0");
        } else{
          $('#is_charge_after_minimum_hours').val("1");
        }
  }

  function is_iframe() {
    var frame = $('#is_iframe_auto_calculate').val();
    if (frame==1){
          $('#is_iframe_auto_calculate').val("0");
        } else{
          $('#is_iframe_auto_calculate').val("1");
        }
  }

 function is_sort() {
    var sort= $('#is_sort_inventory_vertically').val();
    if (sort==1){
          $('#is_sort_inventory_vertically').val("0");
        } else{
          $('#is_sort_inventory_vertically').val("1");
        }
  }

  function is_typ() {
    var sot= $('#is_default_type').val();
    if (sot==1){
          $('#is_default_type').val("0");
        } else{
          $('#is_default_type').val("1");
        }
  }
function is_def() {
    var floors = $('#is_default_floors').val();
    if (floors==1){
          $('#is_default_floors').val("0");
        } else{
          $('#is_default_floors').val("1");
        }
  }
  

</script>