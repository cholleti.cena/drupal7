<h1 class="pt-1 pb-4">Tariff Defaults</h1>


<table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th>TARIFF DEFAULTS PRICE TEXT</th>
        <th>Action</th>
    </thead>
    <tbody>
        <tr>
            <td></td>
            <td><i class="fas fa-pencil-alt icon-large"></i></td>
            
        </tr>
    </tbody>
 </table>


 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th style="width:80%">Default Estimate Notes</th>
        <th><button class="btn btn-success">Add Note</button></th>
    </thead>
    <tbody>
        <tr>
        	<td></td>
        	<td><i class="fas fa-pencil-alt icon-large"> </i><i class="fas fa-trash-alt icon-large"></i></td>
        </tr>
    </tbody>
 </table>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th style="width:80%">Default Job Notes</th>
        <th><button class="btn btn-success">Add Note</button></th>
    </thead>
    <tbody>
        <tr>
        	<td></td>
        	<td><i class="fas fa-pencil-alt icon-large"> </i><i class="fas fa-trash-alt icon-large"></i></td>
        </tr>
    </tbody>
 </table>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th style="width:80%">Tariff Defaults Notes</th>
        <th><button class="btn btn-success">Add Note</button></th>
    </thead>
    <tbody>
        <tr>
        	<td></td>
        	<td><i class="fas fa-pencil-alt icon-large"> </i><i class="fas fa-trash-alt icon-large"></i></td>
        </tr>
    </tbody>
 </table>