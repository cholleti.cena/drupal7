@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Fleets
@endsection

@section('content')
@include('admin.components.message')	
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $(".nav-link").click(function(){
    $('.div-cont').hide();
    $('.active').removeClass('active');
    $('#div'+$(this).attr('target')).show();
    $('#link'+$(this).attr('target')).addClass('active');
  });
});
</script>
<div class="content-wrapper container-xxl p-0">
    <div class="content-header row"></div>
    <div class="content-body">
      <section id="dashboard-ecommerce">
        <div class="">
            <div class="table-bar">
                <div class="row">
                    <div class="col-sm">
                        <h1>Estimate Settings</h1>
                    </div>
                    
                 </div>
                 <div class="col-xl-12">
                     <div class="nav-tabs-top">
                         <ul class="nav nav-tabs">
                             <li class="list-item"><a class="nav-link active" target="1" id="link1" data-toggle="tab" href="#">Mileage And Travel</a></li>
                             <li class="list-item"><a class="nav-link" target="2" id="link2" data-toggle="tab" href="#">Labor Price Estimate</a></li>
                             <li class="list-item"><a class="nav-link" target="3" id="link3" data-toggle="tab" href="#">Days Capacity</a></li>
                             <li class="list-item"><a class="nav-link" target="4" id="link4" data-toggle="tab" href="#">Hourly Service Quoted Defaults</a></li>
                             <li class="list-item"><a class="nav-link" target="5" id="link5" data-toggle="tab" href="#">Guaranteed Quote Service Quoted Defaults</a></li>
                             <li class="list-item"><a class="nav-link" target="6" id="link6" data-toggle="tab" href="#">Not To Exceed Service Quoted Defaults</a></li>
                             <li class="list-item"><a class="nav-link" target="7" id="link7" data-toggle="tab" href="#">Edit Verbiage</a></li>
                             <li class="list-item"><a class="nav-link" target="8" id="link8" data-toggle="tab" href="#">Customer Portal</a></li>
                             <li class="list-item"><a class="nav-link" target="9" id="link9" data-toggle="tab" href="#">Tariff Defaults</a></li>
                             <li class="list-item"><a class="nav-link" target="10" id="link10" data-toggle="tab" href="#">Company FAQ / General Info</a></li>
                         </ul>
                     </div>
                 </div>



                 <div class="container mt-2" style="border:1px solid #867d7d;">
                    <div class="div-cont p-2" id="div1">
                       @include('admin.estimatesettings.step_one')
                    </div>

                    <div class="div-cont p-2" id="div2">
                        @include('admin.estimatesettings.step_two')
                    </div>


                    <div class="div-cont" id="div3">
                        @include('admin.estimatesettings.step_three')
                    </div>

                    <div class="div-cont" id="div4">
                          @include('admin.estimatesettings.step_four')
                    </div>
                    <div class="div-cont" id="div5">
                        @include('admin.estimatesettings.step_five')
                    </div>

                    <div class="div-cont" id="div6">
                        @include('admin.estimatesettings.step_six')
                    </div>
                    <div class="div-cont" id="div7">
                        @include('admin.estimatesettings.step_seven')
                    </div>

                    <div class="div-cont" id="div8">
                        @include('admin.estimatesettings.step_eight')
                    </div>
                    <div class="div-cont" id="div9">
                        @include('admin.estimatesettings.step_nine')
                    </div>

                    <div class="div-cont" id="div10">
                        @include('admin.estimatesettings.step_ten')
                    </div>



                 </div>
                 
               
            </div>
        </div>
      </section>
    </div>  
  </div>
  <style type="text/css">
      ul{
        margin-block-start: 1em;
        margin-block-end: 1em;
        margin-inline-start: 0px;
        margin-inline-end: 0px;
        padding-inline-start: 40px;
      }
      .nav-tabs .nav-link {
       border: none; 
       }
      .list-item:hover{
        border-top:1px solid #d2c2c2;
        border-right:1px solid #d2c2c2;
        border-left:1px solid #d2c2c2;
      }
      .nav-tabs{
          border-bottom:none;
      }
      #div2,#div3,#div4,#div5,#div6,#div7,#div8,#div9,#div10{display:none;}
      
  </style>

@endsection