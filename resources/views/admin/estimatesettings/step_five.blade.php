
<div class="row">
	<h1>Guaranteed Quote Service Quoted Defaults</h1>
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Moving Services</label>
	    <div class="col-auto">
	      <input type="text" class="form-control "  value="0%">
	    </div>
	</div>
	<div class="row">
		<label class="col-auto">Include With:</label>
	    <div class="col-auto">
	      <select class="form-control ">
	      	<option>None Seleected</option>
	      </select>
	    </div>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Traveling Services</label>
		<div class="col-auto">
	      <input type="text" class="form-control "  value="0%">
	    </div>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Packing Materials</label>
		<div class="col-auto">
	      <input type="text" class="form-control "  value="0%">
	    </div>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Packing Services</label>
		<div class="col-auto">
	      <input type="text" class="form-control "  value="0%">
	    </div>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Storage Services</label>
		<div class="col-auto">
	      <input type="text" class="form-control "  value="0%">
	    </div>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Fuel Surcharge</label>
		<div class="col-auto">
	      <input type="text" class="form-control "  value="0%">
	    </div>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Discount</label>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">0.0%</label>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">$0.0 Valuation $0.75 </label>
	</div>


	<div class="row" style="padding-left:1px;">
		<label class="col-auto">SHOW GUARANTEED NOTES:</label>
		<input type="checkbox" value="1" class="col-auto mt-1">
	</div>	
	<div class="row" style="padding-left:1px;">
		<label class="col-auto">SHOW ESTIMATED BEFORE CREW: </label>
		<input type="checkbox" value="1" class="col-auto mt-1">
	</div>		
	
</div>


 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th>GUARANTEED PRICE TEXTT</th>
        <th>Action</th>
    </thead>
    <tbody>
        <tr>
            <td>This ESTIMATED PRICE is based on the info you provided, and that your locations are packed and ready to move. If additional packing, additional items, or variables happen the price will be adjusted accordingly. PLEASE KEEP IN MIND THIS IS AN ESTIMATE.</td>
            <td><i class="fas fa-pencil-alt icon-large"></i></td>
            
        </tr>
    </tbody>
 </table>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th style="width:80%">Default Estimate Notes</th>
        <th><button class="btn btn-success">Add Note</button></th>
    </thead>
    <tbody>
        <tr>
        	<td></td>
        	<td><i class="fas fa-pencil-alt icon-large"> </i><i class="fas fa-trash-alt icon-large"></i></td>
        </tr>
    </tbody>
 </table>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th style="width:80%">Default Job Notes</th>
        <th><button class="btn btn-success">Add Note</button></th>
    </thead>
    <tbody>
        <tr>
        	<td></td>
        	<td><i class="fas fa-pencil-alt icon-large"> </i><i class="fas fa-trash-alt icon-large"></i></td>
        </tr>
    </tbody>
 </table>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th style="width:80%">Guaranteed Notes</th>
        <th><button class="btn btn-success">Add Note</button></th>
    </thead>
    <tbody>
        <tr>
        	<td></td>
        	<td><i class="fas fa-pencil-alt icon-large"> </i><i class="fas fa-trash-alt icon-large"></i></td>
        </tr>
    </tbody>
 </table>