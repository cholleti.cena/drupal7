<div class="row">
	<div class="col-sm">
		<div class="row">
			<label class="col-auto">Esign for customer portal:</label>
			<input type="checkbox" value="1" class="col-auto mt-1">
	   </div>
	   <div class="row">
			<label class="col-auto">Require esign for booking:</label>
			<input type="checkbox" value="1" class="col-auto mt-1">
	   </div>
	   <div class="row">
			<label class="col-auto">Hide Measurement Section:</label>
			<input type="checkbox" value="1" class="col-auto mt-1">
	   </div>
	   <div class="row">
			<label class="col-auto">Hide trucks and men:</label>
			<input type="checkbox" value="1" class="col-auto mt-1">
	   </div>
	   <div class="row">
			<label class="col-auto">Hide arrival time-frame:</label>
			<input type="checkbox" value="1" class="col-auto mt-1">
	   </div>	
	   <div class="row">
			<label class="col-auto">Hide dwelling and floors:</label>
			<input type="checkbox" value="1" class="col-auto mt-1">
	   </div>
	   <div class="row">
			<label class="col-auto">Hide boxes/totes:</label>
			<input type="checkbox" value="1" class="col-auto mt-1">
	   </div>
	   <div class="form-group row">
		    <label for="declined_estimate_url" class="col-sm col-form-label ">Declined estimate url:</label>
		    <div class="col-sm">
		      <input type="text" class="form-control " id="declined_estimate_url" name="declined_estimate_url" value="www.bighillmovers.com">
		    </div>
        </div>
        <div class="row">
			<label class="col-auto">Popup in customer portal::</label>
			<input type="checkbox" value="1" class="col-auto mt-1">
	   </div>
	   <div class="form-group row">
		    <label for="estimate_for_services" class="col-sm col-form-label ">Estimate for services:</label>
		    <div class="col-sm">
		      <input type="text" class="form-control " id="estimate_for_services" name="estimate_for_services" value="Estimate for services">
		    </div>
        </div>
        <div class="form-group row">
		    <label for="companys_website" class="col-sm col-form-label ">Company's website:</label>
		    <div class="col-sm">
		      <input type="text" class="form-control " id="companys_website" name="companys_website" value="www.bighillmovers.com">
		    </div>
        </div>
        <div class="form-group row">
		    <label for="usdot" class="col-sm col-form-label ">Usdot::</label>
		    <div class="col-sm">
		      <input type="text" class="form-control " id="usdot" name="usdot" value="">
		    </div>
        </div>
        <div class="form-group row">
		    <label for="state_authority" class="col-sm col-form-label ">State authority:</label>
		    <div class="col-sm">
		      <input type="text" class="form-control " id="state_authority" name="state_authority" value=" ">
		    </div>
        </div>

	</div>

	<div class="col-sm"></div>
</div>


<table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th>Book This Move Text</th>
        <th>Action</th>
    </thead>
    <tbody>
        <tr>
            <td>This does not confirm your booking. The booking is only confirmed after we contact you back letting you know that the date you selected is available.</td>
            <td><i class="fas fa-pencil-alt icon-large"></i></td>
            
        </tr>
    </tbody>
 </table>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th>Portal Guaranteed Text</th>
        <th>Action</th>
    </thead>
    <tbody>
        <tr>
            <td>All jobs require a credit card on file to reserve  your date with a three hour deposit fully refundable as long as we are able to book a move in it's place. By signing you agree and understand that all quotes unless noted are an estimate only, not a guaranteed price or flat rate. I have read and understand all attachments that were sent with my estimate. LET'S GET MOVING!</td>
            <td><i class="fas fa-pencil-alt icon-large"></i></td>
            
        </tr>
    </tbody>
 </table>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th>Portal Hourly Text</th>
        <th>Action</th>
    </thead>
    <tbody>
        <tr>
            <td>All jobs require a credit card on file to reserve  your date with a three hour deposit fully refundable as long as we are able to book a move in it's place. By signing you agree and understand that all quotes unless noted are an estimate only, not a guaranteed price or flat rate. I have read and understand all attachments that were sent with my estimate. LET'S GET MOVING!</td>
            <td><i class="fas fa-pencil-alt icon-large"></i></td>
            
        </tr>
    </tbody>
 </table>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th>Portal NotToExceed Text</th>
        <th>Action</th>
    </thead>
    <tbody>
        <tr>
            <td>All jobs require a credit card on file to reserve  your date with a three hour deposit fully refundable as long as we are able to book a job in it's place. By signing you agree and understand that all quotes unless noted are an estimate only, not a guaranteed price or flat rate. I have read and understand all attachments that were sent with my estimate. LET'S GET MOVING!</td>
            <td><i class="fas fa-pencil-alt icon-large"></i></td>
            
        </tr>
    </tbody>
 </table>