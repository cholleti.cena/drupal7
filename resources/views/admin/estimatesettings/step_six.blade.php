<div class="row">
	<h1>Not To Exceed Service Quoted Defaults</h1>
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Moving Services</label>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Traveling Services</label>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Packing Materials</label>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Packing Services</label>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Storage Services</label>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Fuel Surcharge</label>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">Discount</label>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">0.0%</label>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">$0.0 Valuation $0.75 </label>
	</div>
    <div class="row">
		<label class="col-auto">Hide Fields from Estimate:</label>
	    <div class="col-auto">
	      <select class="form-control ">
	      	<option>Seledt all</option>
	      </select>
	    </div>
	</div>	

	<div class="row" style="padding-left:1px;">
		<label class="col-auto">SHOW TOTAL EST PRICE:</label>
		<input type="checkbox" value="1" class="col-auto mt-1">
	</div>	
	<div class="row" style="padding-left:1px;">
		<label class="col-auto">SHOW NOT-TO-EXCEED AMOUNT: </label>
		<input type="checkbox" value="1" class="col-auto mt-1">
	</div>
	<div class="row" style="padding-left:1px;">
		<label class="col-auto">SHOW NOT-TO-EXCEED NOTES: </label>
		<input type="checkbox" value="1" class="col-auto mt-1">
	</div>		
	
</div>


 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th>NOT TO EXCEED PRICE TEXT</th>
        <th>Action</th>
    </thead>
    <tbody>
        <tr>
            <td>This estimate is based on the information you provide, and on the assumption that your house or business is packed and ready to move upon arrival, unless packing services are noted above. Estimates are given to inform you of approximate costs and allow us to schedule movers appropriately. Actual charges reflect the time spent on your move, plus boxes, packing supplies, travel time, fuel, and any other extra charges as noted on your estimate. In addition all moves have a 7% surcharge which covers the consumables that are not charged individually, for example moving blankets, rubber bands, shrink wrap, etc. Billing begins when the movers arrive to your location and ends when they finish the move, plus the travel time which is the time to get to and from your locations. The estimate above reflects the minimum valuation of .60 per item per pound included. Additional valuation options are available upon request. All jobs require a $100.00 deposit upon reserving your date. Deposit is non-refundable if cancelled or postponed within 24 hours of move date. Deposit will be applied directly to your moving bill.</td>
            <td><i class="fas fa-pencil-alt icon-large"></i></td>
            
        </tr>
    </tbody>
 </table>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th style="width:80%">Default Estimate Notes</th>
        <th><button class="btn btn-success">Add Note</button></th>
    </thead>
    <tbody>
        <tr>
        	<td></td>
        	<td><i class="fas fa-pencil-alt icon-large"> </i><i class="fas fa-trash-alt icon-large"></i></td>
        </tr>
    </tbody>
 </table>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th style="width:80%">Default Job Notes</th>
        <th><button class="btn btn-success">Add Note</button></th>
    </thead>
    <tbody>
        <tr>
        	<td></td>
        	<td><i class="fas fa-pencil-alt icon-large"> </i><i class="fas fa-trash-alt icon-large"></i></td>
        </tr>
    </tbody>
 </table>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th style="width:80%">Not To Exceed Notes</th>
        <th><button class="btn btn-success">Add Note</button></th>
    </thead>
    <tbody>
        <tr>
        	<td></td>
        	<td><i class="fas fa-pencil-alt icon-large"> </i><i class="fas fa-trash-alt icon-large"></i></td>
        </tr>
    </tbody>
 </table>