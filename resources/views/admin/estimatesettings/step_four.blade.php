<div class="row ml-2">
	<h1>Hourly Service Quoted Defaults</h1>

	<form method="post" action="{{URL::to('admin/updateHourlyServiceQuotedDefaults')}}"> 
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @foreach($hourlyservices as $hourlyservice)
       <input type="hidden" name="id" value="{{$hourlyservice->id}}"> 
	<div class="row">
		<?php if($hourlyservice->is_moving_services)
          {
            $is_moving_services='checked'; 
          }else {
            $is_moving_services='';
          }
          ?>
        <input type="hidden" name="is_moving_services" value="{{$hourlyservice->is_moving_services}}" id="is_moving_services">
		
		<input type="checkbox" class="col-auto mt-2" onchange="is_moving()" name="ismoving_services"  value="{{$hourlyservice->is_moving_services}}" {{$is_moving_services}}>
		<label class="col-auto">Moving Services</label>
	</div>	
	<div class="row">
		<?php if($hourlyservice->is_traveling_services)
          {
            $is_traveling_services='checked'; 
          }else {
            $is_traveling_services='';
          }
          ?>
		 <input type="hidden" name="is_traveling_services" id="is_traveling_services" value="{{$hourlyservice->is_traveling_services}}">
		<input type="checkbox" class="col-auto mt-2" onchange="is_traveling()" name="is_traveling_services"  value="{{$hourlyservice->is_traveling_services}}" {{$is_traveling_services}}>
		<label class="col-auto">Traveling Services</label>
	</div>	
	<div class="row">
		<?php if($hourlyservice->is_packing_materials)
          {
            $is_packing_materials='checked'; 
          }else {
            $is_packing_materials='';
          }
          ?>
        <input type="hidden" name="is_packing_materials" id="is_packing_materials" value="{{$hourlyservice->is_packing_materials}}">
		<input type="checkbox" class="col-auto mt-2" onchange="is_packing_mat()" name="is_packing_materials"  value="{{$hourlyservice->is_packing_materials}}" {{$is_packing_materials}}>
		<label class="col-auto">Packing Materials</label>
	</div>	
	<div class="row">
		<?php if($hourlyservice->is_packing_services)
          {
            $is_packing_services='checked'; 
          }else {
            $is_packing_services='';
          }
          ?>
        <input type="hidden" name="is_packing_services" id="is_packing_services" value="{{$hourlyservice->is_packing_services}}">
		<input type="checkbox" class="col-auto mt-2" onchange="is_packing_serv()" name="is_packing_services"  value="{{$hourlyservice->is_packing_services}}" {{$is_packing_services}}>
		<label class="col-auto">Packing Services</label>
	</div>	
	<div class="row">
		<?php if($hourlyservice->is_storage_services)
          {
            $is_storage_services='checked'; 
          }else {
            $is_storage_services='';
          }
          ?>
        <input type="hidden" name="is_storage_services" id="is_storage_services" value="{{$hourlyservice->is_storage_services}}">
		<input type="checkbox" class="col-auto mt-2" onchange="is_storage()" name="is_storage_services"  value="{{$hourlyservice->is_storage_services}}" {{$is_storage_services}}>
		<label class="col-auto">Storage Services</label>
	</div>	
	<div class="row">
		<?php if($hourlyservice->is_fuel_surcharge)
          {
            $is_fuel_surcharge='checked'; 
          }else {
            $is_fuel_surcharge='';
          }
          ?>
        <input type="hidden" name="is_fuel_surcharge" id="is_fuel_surcharge" value="{{$hourlyservice->is_fuel_surcharge}}">
		<input type="checkbox" class="col-auto mt-2" onchange="is_fuel()" name="is_fuel_surcharge"  value="{{$hourlyservice->is_fuel_surcharge}}" {{$is_fuel_surcharge}}>
		<label class="col-auto">Fuel Surcharge</label>
	</div>	
	<div class="row">
		<?php if($hourlyservice->is_discount)
          {
            $is_discount='checked'; 
          }else {
            $is_discount='';
          }
          ?>
        <input type="hidden" name="is_discount" id="is_discount" value="{{$hourlyservice->is_discount}}">
		<input type="checkbox" class="col-auto mt-2" onchange="is_dis()" name="is_moving_services"  value="{{$hourlyservice->is_discount}}" {{$is_discount}}>
		<label class="col-auto">Discount</label>
	</div>	
	<!--<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">0.0%</label>
	</div>	
	<div class="row">
		<input type="checkbox" value="1" class="col-auto mt-1">
		<label class="col-auto">$0.0 Valuation $0.75 </label>
	</div>-->


	<div class="row" style="padding-left:1px;">
		<label class="col-auto">SHOW TOTAL EST PRICE:</label>
		<?php if($hourlyservice->is_show_total_est_price)
          {
            $is_show_total_est_price='checked'; 
          }else {
            $is_show_total_est_price='';
          }
          ?>
        <input type="hidden" name="is_show_total_est_price" id="is_show_total_est_price" value="{{$hourlyservice->is_show_total_est_price}}">
		<input type="checkbox" class="col-auto mt-2" onchange="is_show_total()" name="is_show_total_est_price"  value="{{$hourlyservice->is_show_total_est_price}}" {{$is_show_total_est_price}}>
	</div>	
	<div class="row" style="padding-left:1px;">
		<label class="col-auto">SHOW HOURLY NOTES: </label>
		<?php if($hourlyservice->is_show_hourly_notes)
          {
            $is_show_hourly_notes='checked'; 
          }else {
            $is_show_hourly_notes='';
          }
          ?>
        <input type="hidden" name="is_show_hourly_notes" value="{{$hourlyservice->is_show_hourly_notes}}" id="is_show_hourly_notes">
		<input type="checkbox" class="col-auto mt-2" onchange="is_show_hourly()" name="is_show_hourly_notes"  value="{{$hourlyservice->is_show_hourly_notes}}"{{$is_show_hourly_notes}}>
	</div>	
	{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
     {{ link_to_route('estimatesettings.index','Cancel',null, array('class' => 'btn btn-danger')) }}
     	
	
</div>


 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th>HOURLY PRICE TEXT</th>
        <th>Action</th>
    </thead>
    <tbody>
        <tr>
            <td>{{$hourlyservice->hourly_price_text}}</td>
            <td><a href="#" data-toggle="modal" data-target="#EditHourlyModal"><i class="fas fa-pencil-alt icon-large"></i></a></td>
            
        </tr>
    </tbody>
 </table>

  @endforeach
   </form>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th style="width:80%">Default Estimate Notes</th>
        <th><button class="btn btn-success" data-toggle="modal" data-target="#AddDefaultNotesModal">Add Note</button></th>
    </thead>
    <tbody>
    	@foreach($defaultnotes as $defaultnote)
        <tr>
        	<td>{{$defaultnote->description}}</td>
        	<td>
        		<a href="#"class="DefaultNotesModal" data-id="{{$defaultnote->id}}"><i class="fas fa-pencil-alt icon-large"> </i></a>
        		<a href="deleteDefaultEstimateNotes/{{$defaultnote->id}}"><i class="fa fa-trash cursorPoint objDelete" data-id="{{$defaultnote->id}}"></i></a>
        		
        	</td>
        </tr>
         @endforeach
    </tbody>
 </table>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th style="width:80%">Default Job Notes</th>
        <th><button class="btn btn-success" data-toggle="modal" data-target="#AddDefaultJobModal">Add Note</button></th>
    </thead>
    <tbody>
    	@foreach($defaultjobs as $defaultjob)
        <tr>
        	<td>{{$defaultjob->description}}</td>
        	<td>
        		<a href="#" class="DefaultJobModal" data-id="{{$defaultjob->id}}"><i class="fas fa-pencil-alt icon-large"> </i></a>
        		<a href="deleteDefaultEstimateJob/{{$defaultjob->id}}"><i class="fa fa-trash cursorPoint objDelete" data-id="{{$defaultjob->id}}"></i></a>
        		
        	</td>
        </tr>
         @endforeach
    </tbody>
 </table>

 <table class="table table-bordered table-striped" style="width:100%">
    <thead>
        <th style="width:80%">Hourly Notes</th>
        <th><button class="btn btn-success" data-toggle="modal" data-target="#AddHourlyNotesModal">Add Note</button></th>
    </thead>
    <tbody>
    	@foreach($hourlynotes as $hourlynote)
        <tr>
        	<td>{{$hourlynote->description}}</td>
        	<td>
        	<a href="#" class="HourlyNotesModal" data-id="{{$hourlynote->id}}"><i class="fas fa-pencil-alt icon-large"> </i></a>
        		<a href="deleteHourlyNotes/{{$hourlynote->id}}"><i class="fa fa-trash cursorPoint objDelete" data-id="{{$hourlynote->id}}"></i></a>
        	</td>
        </tr>
        @endforeach
    </tbody>
 </table>



<!-- The Modal -->
  <div class="modal fade" id="EditHourlyModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#fff;">
          <h4 class="modal-title">Edit Hourly Price Text</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form method="post" action="{{URL::to('admin/UpdateHourlyPriceText')}}">  
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row col-sm-12">
              <label class="col-sm">Hourly Price Text::</label>
              <input type="hidden" name="id" id="id" value="{{$hourlyservice->id}}">
              <textarea type="text" class="col-sm" cols="10" rows="10" name="description" id="description" >{{$hourlyservice->description}}</textarea>
            </div>
            
            <div class="container-fluid text-center">
                <button class="btn btn-info">Update</button>
                {{ link_to_route('estimatesettings.index','Cancel',null, array('class' => 'btn btn-danger')) }}
             </div>
          </form>
         </div>
      </div>
    </div>
  </div>


  <!-- The Modal -->
  <div class="modal fade" id="AddDefaultNotesModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#fff;">
          <h4 class="modal-title">Add Default Notes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form method="post" action="{{URL::to('admin/AddDefaultEstimateNotes')}}">  
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row col-sm-12">
              <label class="col-sm">Default Notes:</label>
              <textarea type="text" class="col-sm" cols="10" rows="10" name="description" id="description" ></textarea>
            </div>
            
            <div class="container-fluid text-center">
                <button class="btn btn-info">Save</button>
                {{ link_to_route('estimatesettings.index','Cancel',null, array('class' => 'btn btn-danger')) }}
             </div>
          </form>
         </div>
      </div>
    </div>
  </div>

   <!-- The Modal -->
  <div class="modal" id="EditDefaultNotesModal">
    <div class="modal-dialog">
      <div class="modal-content" style="display:block">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#fff;">
          <h4 class="modal-title">Edit Default Notes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" id="editBody">
          <form method="post" action="{{URL::to('admin/UpdateDefaultEstimateNotes')}}">	
          	<input type="hidden" name="_token" value="{{ csrf_token() }}">
          	<input name="id" id="notes_id" value="{{$defaultnote->id}}" type="hidden" />
	          <div class="row col-sm-12">
	          	<label class="col-sm-4">Defaulit Notes:</label>
	          	<textarea class="col-sm-8" name="description" id="notes_description" value="{{$defaultnote->description}}"></textarea>
	          </div>
	          
	          <div class="container-fluid text-center">
	              <button class="btn btn-info">Update</button>
	              {{ link_to_route('estimatesettings.index','Cancel',null, array('class' => 'btn btn-danger')) }}
	           </div>
	        </form>
         </div>
      </div>
    </div>
  </div>
   

   <!-- The Modal -->
  <div class="modal fade" id="AddDefaultJobModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#fff;">
          <h4 class="modal-title">Add Default Job</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form method="post" action="{{URL::to('admin/AddDefaultEstimateJob')}}">  
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row col-sm-12">
              <label class="col-sm">Default Job:</label>
              <textarea type="text" class="col-sm" cols="10" rows="10" name="description" id="description" ></textarea>
            </div>
            
            <div class="container-fluid text-center">
                <button class="btn btn-info">Save</button>
                {{ link_to_route('estimatesettings.index','Cancel',null, array('class' => 'btn btn-danger')) }}
             </div>
          </form>
         </div>
      </div>
    </div>
  </div>

  <!-- The Modal -->
  <div class="modal" id="EditDefaultJobModal">
    <div class="modal-dialog">
      <div class="modal-content" style="display:block">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#fff;">
          <h4 class="modal-title">Edit Default Job</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" id="editBody">
          <form method="post" action="{{URL::to('admin/UpdateDefaultEstimateJob')}}">	
          	<input type="hidden" name="_token" value="{{ csrf_token() }}">
          	<input name="id" id="job_id" value="{{$defaultjob->id}}" type="hidden" />
	          <div class="row col-sm-12">
	          	<label class="col-sm-4">Default Job:</label>
	          	<textarea class="col-sm-8" name="description" id="job_description" value="{{$defaultjob->description}}"></textarea>
	          </div>
	          
	          <div class="container-fluid text-center">
	              <button class="btn btn-info">Update</button>
	              {{ link_to_route('estimatesettings.index','Cancel',null, array('class' => 'btn btn-danger')) }}
	           </div>
	        </form>
         </div>
      </div>
    </div>
  </div>


   <!-- The Modal -->
  <div class="modal fade" id="AddHourlyNotesModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#fff;">
          <h4 class="modal-title">Add Hourly Notes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form method="post" action="{{URL::to('admin/AddHourlyNotes')}}">  
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row col-sm-12">
              <label class="col-sm">Hourly Notes:</label>
              <textarea type="text" class="col-sm" cols="10" rows="10" name="description" id="description" ></textarea>
            </div>
            
            <div class="container-fluid text-center">
                <button class="btn btn-info">Save</button>
                {{ link_to_route('estimatesettings.index','Cancel',null, array('class' => 'btn btn-danger')) }}
             </div>
          </form>
         </div>
      </div>
    </div>
  </div>

  <!-- The Modal -->
  <div class="modal" id="EditHourlyNotes">
    <div class="modal-dialog">
      <div class="modal-content" style="display:block">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#fff;">
          <h4 class="modal-title">Edit Hourly Notes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" id="editBody">
          <form method="post" action="{{URL::to('admin/UpdateHourlyNotes')}}">	
          	<input type="hidden" name="_token" value="{{ csrf_token() }}">
          	<input name="id" id="hourly_id" value="{{$hourlynote->id}}" type="hidden" />
	          <div class="row col-sm-12">
	          	<label class="col-sm-4">Hourly Notes:</label>
	          	<textarea class="col-sm-8" name="description" id="hourly_description" value="{{$hourlynote->description}}"></textarea>
	          </div>
	          
	          <div class="container-fluid text-center">
	              <button class="btn btn-info">Update</button>
	              {{ link_to_route('estimatesettings.index','Cancel',null, array('class' => 'btn btn-danger')) }}
	           </div>
	        </form>
         </div>
      </div>
    </div>
  </div>
 



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script>
  	 var notesmodal = document.getElementById("EditDefaultNotesModal");
    
     $(".DefaultNotesModal").click(function(){

      var notesid = $(this).attr('data-id');
      $.get("editDefaultEstimateNotes/"+notesid, function(res) {
        if(res && res.data && res.data.length > 0 ){
          var data = res.data[0];
          notesmodal.style.display = "block";
          $("#notes_id").val(data.id);
          $("#notes_description").val(data.description);
        } else {
          alert("data not found")
        }
      }).fail(function() {
        console.log("fail")
      }).done(function() {
        console.log("completed");
      })
    })

    </script>
    <script>
  	 var jobmodal = document.getElementById("EditDefaultJobModal");
    
     $(".DefaultJobModal").click(function(){

      var jobid = $(this).attr('data-id');
      $.get("editDefaultEstimateJob/"+jobid, function(res) {
        if(res && res.data && res.data.length > 0 ){
          var data = res.data[0];
          jobmodal.style.display = "block";
          $("#job_id").val(data.id);
          $("#job_description").val(data.description);
        } else {
          alert("data not found")
        }
      }).fail(function() {
        console.log("fail")
      }).done(function() {
        console.log("completed");
      })
    })

    </script>
    <script>
  	 var hourlymodal = document.getElementById("EditHourlyNotes");
    
     $(".HourlyNotesModal").click(function(){

      var hourlyid = $(this).attr('data-id');
      $.get("editHourlyNotes/"+hourlyid, function(res) {
        if(res && res.data && res.data.length > 0 ){
          var data = res.data[0];
          hourlymodal.style.display = "block";
          $("#hourly_id").val(data.id);
          $("#hourly_description").val(data.description);
        } else {
          alert("data not found")
        }
      }).fail(function() {
        console.log("fail")
      }).done(function() {
        console.log("completed");
      })
    })

    </script>
 <script type="text/javascript">

  function is_moving() {
    var charge= $('#is_moving_services').val();
    if (charge==1){
          $('#is_moving_services').val("0");
        } else{
          $('#is_moving_services').val("1");
        }
  }

  function is_traveling() {
    var travel = $('#is_traveling_services').val();
    if (travel==1){
          $('#is_traveling_services').val("0");
        } else{
          $('#is_traveling_services').val("1");
        }
  }

 function is_packing_mat() {
    var material= $('#is_packing_materials').val();
    if (material==1){
          $('#is_packing_materials').val("0");
        } else{
          $('#is_packing_materials').val("1");
        }
  }

  function is_packing_serv() {
    var service= $('#is_packing_services').val();
    if (service==1){
          $('#is_packing_services').val("0");
        } else{
          $('#is_packing_services').val("1");
        }
  }
function is_storage() {
    var storage = $('#is_storage_services').val();
    if (storage==1){
          $('#is_storage_services').val("0");
        } else{
          $('#is_storage_services').val("1");
        }
  }
   function is_fuel() {
    var fuel= $('#is_fuel_surcharge').val();
    if (fuel==1){
          $('#is_fuel_surcharge').val("0");
        } else{
          $('#is_fuel_surcharge').val("1");
        }
  }

  function is_dis() {
    var discount = $('#is_discount').val();
    if (discount==1){
          $('#is_discount').val("0");
        } else{
          $('#is_discount').val("1");
        }
  }

 function is_show_total() {
    var total= $('#is_show_total_est_price').val();
    if (total==1){
          $('#is_show_total_est_price').val("0");
        } else{
          $('#is_show_total_est_price').val("1");
        }
  }

  function is_show_hourly() {
    var hourly= $('#is_show_hourly_notes').val();
    if (hourly==1){
          $('#is_show_hourly_notes').val("0");
        } else{
          $('#is_show_hourly_notes').val("1");
        }
  }
  

</script>