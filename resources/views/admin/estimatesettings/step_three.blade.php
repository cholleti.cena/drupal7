
<div class="row">
	<div class="pb-4" style="width:60%">
       <h1>Days Capacity For Company Branch</h1>
       <form method="post" action="{{URL::to('admin/updateEstimateDaysCapacity')}}"> 
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @foreach($days as $day)
        <input type="hidden" name="id" value="{{$day->id}}">
		<div class="row">
			<div class="form-group row">
			    <label for="jobs" class="col-sm col-form-label ">Number of jobs:</label>
			    <div class="col-sm">
			      <input type="text" class="form-control " id="number_of_jobs" name="number_of_jobs" value="{{$day->number_of_jobs}}">
			    </div>
            </div>
		</div>
		<div class="row">
		    <div class="form-group row">
			    <label for="men" class="col-sm col-form-label ">Number of men:</label>
			    <div class="col-sm">
			      <input type="text" class="form-control " id="number_of_men" name="number_of_men" value="{{$day->number_of_men}}">
			    </div>
            </div>
		</div>
		<div class="row">
			<div class="form-group row">
			    <label for="trucks" class="col-sm col-form-label ">Number of trucks:</label>
			    <div class="col-sm">
			      <input type="text" class="form-control " id="number_of_trucks" name="number_of_trucks" value="{{$day->number_of_trucks}}">
			    </div>
            </div>
		</div>
		{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
     {{ link_to_route('estimatesettings.index','Cancel',null, array('class' => 'btn btn-danger')) }}
     @endforeach
   </form>
	</div>
	<div class="" style="width:35%">
		<div class="row">
	     <h4>DO NOT BOOK, Recurring Days</h4>
        </div>
        <div class="row">
        	<form method="post" action="{{URL::to('admin/updateEstimateRecurringDays')}}"> 
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="col-sm">
				<input type="hidden" name="recurring_days" value="{{$day->recurring_days}}">
				<select class="form-control" name="recurring_days" value="{{$day->recurring_days}}">
					 @foreach($recurringdays as $recurringday)
					<option>{{$recurringday->day}}</option>
					 @endforeach
				</select>
			</div>
			<div class="col-sm">
				<button class="form-control btn btn-success">Update Day</button>
			</div>
		</div>
	</form>
	</div>
</div>
