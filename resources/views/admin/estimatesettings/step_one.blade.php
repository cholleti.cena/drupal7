<h1>Mileage Calculation Method</h1>
                       
 <div class="row" style="margin:10px;">
     <table class="table table-bordered table-striped" style="width:100%">
        <tr>
            <td><input type="radio" value="1" ></td>
            <td><img src="{{env('SITE_URL')}}/theme/assets/worst_case_image.png"></td>
            <td>
                <h5>Worst Case Calc</h5>
                <p>Calculates the mileage of a job as the distance from the office to through each stop and back to the office.</p>
            </td>
            <td></td>
        </tr>
        <tr>
            <td><input type="radio" value="1" ></td>
            <td><img src="{{env('SITE_URL')}}/theme/assets/point_to_point_image.png"></td>
            <td>
                <h5>Point To Point</h5>
                <p>Calculates the mileage from the Origin through Extra Stops and ends at the destination.</p>
            </td>
            <td><input type="checkbox" value="1" checked>Double Drive Time</td>
        </tr>
     </table>
     <div class="row">
       <div class="col-auto">
           <label>Allow miles override</label>
       </div>
       <div class="col-auto alignment">
            <input type="checkbox" name="" value="1">
       </div>
     </div>
 </div>