<div class="row">	      
     
        <div>
      	<div class="panel-body">
      		@foreach($endums as $endum)
      	  <table class="table table-bordered table-striped">
      	  	
            <thead>
			  <tr>
			   	<th>{{$endum->heading}}</th>
			   	<th>Action</th>
			  </tr>
			</thead>
            <tbody>
		        <tr>
		            <td>{{$endum->content}}</td>
		             <td><a href="#"><i class="fa fa-edit cursorPoint editInvoice" id ="editInvoice" data-id="{{$endum->id}}"></i></a></td>
		             
		         </tr>
		         <br>
		    </tbody>

		   
        </table>
         @endforeach
       </div>	
       					       
        </div>
       
	 </div>






<style>
table th {
	background-color: #CCCCCC !important;
}
</style>

<div class="modal" id="2">
    <div class="modal-dialog">
      <div class="modal-content" style="display:block">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#fff;">
          <h4 class="modal-title">Invoice And MaxPrice</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" id="invoiceBody">
          <form method="post" action="{{URL::to('admin/updateInvoiceMaxPrice')}}">	
          	<input type="hidden" name="_token" value="{{ csrf_token() }}">
          	<input name="id" id="id" value="{{$endum->id}}" type="hidden" />
	          <div class="row col-sm-12">
	          	<label class="col-sm-4">Content:</label>
	          	<textarea class="col-sm-8" name="content" rows="5"  id="invoice-content" value="{{$endum->content}}"></textarea>
	          </div>
	          <div class="container-fluid text-center">
	              <button class="btn btn-info">Update</button>
	              {{ link_to_route('bolinvoice.index','Cancel',null, array('class' => 'btn btn-danger')) }}
	           </div>
	        </form>
         </div>
      </div>
    </div>
  </div>


  
 


<script>
  	 var imodal = document.getElementById("2");
    
     $("#editInvoice").click(function(){
      var id = $(this).attr('data-id');
      $.get("bolinvoice/editInvoiceMaxPrice/"+id, function(res) {
        if(res && res.data && res.data.length > 0 ){
          var data = res.data[0];
          imodal.style.display = "block";
          $("#id").val(data.id);
          $("#invoice-content").val(data.content);
          //$("#create").val("Update");
         // $("#popuptitle").text("Update");
        } else {
          alert("data not found")
        }
      }).fail(function() {
        console.log("fail")
      }).done(function() {
        console.log("completed");
      })
    })

    </script>