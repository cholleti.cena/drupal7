<div class="row">	      
     
        <div>
      	<div class="panel-body">
      		@foreach($endums as $endum)
      	  <table class="table table-bordered table-striped">
      	  	
            <thead>
			  <tr>
			   	<th>{{$endum->heading}}</th>
			   	<th>Action</th>
			  </tr>
			</thead>
            <tbody>
		        <tr>
		            <td>{{$endum->content}}</td>
		             <td><a href="#"><i class="fa fa-edit cursorPoint notice_policies" data-id="{{$endum->id}}"></i></a></td>
		             
		         </tr>
		         <br>
		    </tbody>

		   
        </table>
         @endforeach
       </div>	
       					       
        </div>
       
	 </div>

<style>
table th {
	background-color: #CCCCCC !important;
}
</style>




 <div class="modal" id="PolicyModal">
    <div class="modal-dialog">
      <div class="modal-content" style="display:block">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#fff;">
          <h4 class="modal-title">Notice And Policies</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" id="editBody">
          <form method="post" action="{{URL::to('admin/updateNoticeandpolicies')}}">	
          	<input type="hidden" name="_token" value="{{ csrf_token() }}">
          	<input name="id" id="id" value="{{$endum->id}}" type="hidden" />
	         <!-- <div class="row col-sm-12">
	          	<label class="col-sm-4">Heading:</label>
	          	<textarea class="col-sm-8" name="heading" id="term-heading"value="{{$endum->heading}}"></textarea>
	          </div>-->
	          <div class="row col-sm-12">
	          	<label class="col-sm-4">Content:</label>
	          	<textarea class="col-sm-8" name="content" rows="5"  id="notice-content" value="{{$endum->content}}"></textarea>
	          </div>
	          <div class="container-fluid text-center">
	              <button class="btn btn-info">Update</button>
	              {{ link_to_route('bolinvoice.index','Cancel',null, array('class' => 'btn btn-danger')) }}
	           </div>
	        </form>
         </div>
      </div>
    </div>
  </div>


  <script>
  	 var npmodal = document.getElementById("PolicyModal");
    
     $(".notice_policies").click(function(){
      var npid = $(this).attr('data-id');
      $.get("bolinvoice/editNoticeandpolicies/"+npid, function(res) {
        if(res && res.data && res.data.length > 0 ){
          var data = res.data[0];
          npmodal.style.display = "block";
          $("#id").val(data.id);
         // $("#term-heading").val(data.heading);
          $("#notice-content").val(data.content);
          //$("#create").val("Update");
         // $("#popuptitle").text("Update");
        } else {
          alert("data not found")
        }
      }).fail(function() {
        console.log("fail")
      }).done(function() {
        console.log("completed");
      })
    })

    </script>
    </script>
 



