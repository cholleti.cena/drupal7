<div class="row">
							  
							     <div class=row>
							     	<div class="col-sm">
							     		<h1>Addendums</h1>
							     	</div>
							     	<div class="col-sm">
							     		<a class="btn btn-info float-right" href="{{URL::to('admin/bolinvoice/create')}}">New Addendum</a>
							     	</div>
							     </div>
							      <div>
							      	<div class="panel-body">
							      	  <table class="table table-bordered table-striped">
							            <thead>
										  <tr>
										   	<th>Name</th>
										   	<th>Data</th>
										   	<th>Status</th>
										   	<th>Actions</th>
										  </tr>
										</thead>
							            <tbody>
                                            @foreach($addendums as $addendum)
                                    <tr>
                                        <td>
                                            {{$addendum->name}}
                                        </td> 
                                        <td>
                                            {{$addendum->data}}
                                        </td> 

                                        <td>
                                            {{$addendum->status}}
                                        </td>                                       
                                        <td width="25%">
                                            <div >
                                                <div style="float:left;padding:0 10px 10px 0;">
                                                {{ link_to_route('bolinvoice.edit','Edit',array($addendum->id), array('class' => 'btn btn-info')) }}
                                                </div>
                                                <div style="float:left;padding-right:10px;">
                                                    {{ Form::open(array('onsubmit' => 'return confirm("Are you sure you want to delete?")','method' => 'DELETE', 'route' => array('bolinvoice.destroy', $addendum->id))) }}
                                                    <button type="submit" class="btn btn-danger btn-xs pull-right" style="font-size: 11px;padding: 11px 12px;">Delete</button>
                                                    {{ Form::close() }}
                                                </div>
                                            </div>
                                        </td>
                                         </tr>
                                    @endforeach
                                        </tbody>
							        </table>
							       </div>	
							       					       
							  </div>
						 </div>