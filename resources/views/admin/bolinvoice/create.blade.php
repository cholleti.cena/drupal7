@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Categories
@endsection
@section('module')
Category
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return CategoryValidate();','route' => 'bolinvoice.store','files'=>true)) }}

<div class="card mb-4">
   <div class="card-body after-loading-icon">
     <div class="container-fluid">
      	<div class="col-md-6">
        
				{{ Form::ahText('name','Name :','',array('maxlength' => '100'))  }}
				<div class="form-group" style="margin:5px">
		        <label for="redeem_limit" class="control-label col-sm-4">Data :</label>
		            <div class="col-sm-12">
		                <textarea rows="4" cols="50" name="data" id="data" class="summernote"></textarea>
		            </div>
		        </div>				
		    	{{ Form::ahSelect('status','Status :','1',array('1' => 'Active', '0' => 'Inactive')) }}
	    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('bolinvoice.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	</div>
  </div>
</div>
@endsection