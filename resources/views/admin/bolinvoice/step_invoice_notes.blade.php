<div class="row">	      
     
        <div>
      	<div class="panel-body">
      		
      	  <table class="table table-bordered table-striped">
      	  	
            <thead>
			  <tr>
			   	<th>INVOICE NOTES</th>
			   	<th>ADD</th>
			  </tr>
			</thead>
            <tbody>
            	@foreach($addendums as $addendum)
		        <tr>
		            <td>{{$addendum->notice_content}}</td>
		             <td><a href="#"><i class="fa fa-edit cursorPoint invoice_notes" data-id="{{$addendum->id}}"></i></a></td>
		             
		         </tr>
		          @endforeach
		 
		    </tbody>

		   
        </table>
        
       </div>	
       					       
        </div>
       
	 </div>




<style>
table th {
	background-color: #CCCCCC !important;
}
</style>

 <!-- The Modal -->
  <div class="modal" id="notesModal">
    <div class="modal-dialog">
      <div class="modal-content" style="display:block">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#fff;">
          <h4 class="modal-title">Invoice Notes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" id="editBody">
          <form method="post" action="{{URL::to('admin/updateInvoiceNotes')}}">	
          	<input type="hidden" name="_token" value="{{ csrf_token() }}">
          	<input name="id" id="id" value="{{$addendum->id}}" type="hidden" />
	          <!--<div class="row col-sm-12">
	          	<label class="col-sm-4">Heading:</label>
	          	<textarea class="col-sm-8" name="heading" id="term-heading"value="{{$addendum->heading}}"></textarea>
	          </div>-->
	          <div class="row col-sm-12">
	          	<label class="col-sm-4">Content:</label>
	          	<textarea class="col-sm-8" name="notice_content" rows="5"  id="notes-content" value="">{{$addendum->notice_content}}</textarea>
	          </div>
	          <div class="container-fluid text-center">
	              <button class="btn btn-info">Update</button>
	              {{ link_to_route('bolinvoice.index','Cancel',null, array('class' => 'btn btn-danger')) }}
	           </div>
	        </form>
         </div>
      </div>
    </div>
  </div>


  <script>
  	 var ntmodal = document.getElementById("notesModal");
    
     $(".invoice_notes").click(function(){
      var ntid = $(this).attr('data-id');
      $.get("bolinvoice/editInvoiceNotes/"+ntid, function(res) {
        if(res && res.data && res.data.length > 0 ){
          var data = res.data[0];
          ntmodal.style.display = "block";
          $("#id").val(data.id);
          //$("#term-heading").val(data.heading);
          $("#notes-content").val(data.notice_content);
          //$("#create").val("Update");
         // $("#popuptitle").text("Update");
        } else {
          alert("data not found")
        }
      }).fail(function() {
        console.log("fail")
      }).done(function() {
        console.log("completed");
      })
    })

    </script>
    </script>




