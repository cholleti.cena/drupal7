<div class="row">
							  
							      <div class="panel-heading ui-draggable-handle">
							      	<h1>Edit Verbiage</h1>
							                                        
							      </div>
							      <form method="post" action="{{URL::to('admin/updateVerbiage')}}">
							      	<input type="hidden" name="_token" value="{{ csrf_token() }}">
							      <div>
							      	<div class="panel-body">
							      	  <table class="table table-bordered table-striped">
							            <thead>
										  <tr>
										   	<th>Signature</th>
										   	<th>Required</th>
										   	<th>Hide</th>
										  </tr>
										</thead>
							            <tbody>
                                            @foreach($addendums as $key=>$addendum)
                                    <tr>
                                        <td>
                                            <label>{{$addendum->signature_text}} :</label> 
                                            <input type="hidden" name="sign_id[]" value="{{$addendum->id}}">
                                            <input type="text-align" name="signature_name[]" value="{{$addendum->signature_name}}">
                                        </td>
                                         
                                        <td>
                                        	<?php if($addendum->required)
                                        	{
                                        		$is_required='checked'; 
                                        	}else {
                                        		$is_required='';
                                        	}
                                        	?>
                                        	<input type="hidden" name="required[]" id="required{{$key}}" value="{{$addendum->required}}">
                                            <input type="checkbox" onchange="check({{$key}})" value="{{$addendum->required}}" {{$is_required}}>
                                        </td> 

                                        <td>
                                        	<?php if($addendum->hide)
                                        	{
                                        		$is_hide='checked'; 
                                        	}else {
                                        		$is_hide='';
                                        	}
                                        	?>
                                        	<input type="hidden" name="hide[]" id="hide{{$key}}" value="{{$addendum->hide}}">
                                            <input type="checkbox" value="{{$addendum->hide}}" {{$is_hide}} onchange="Ischeck({{$key}})">
                                        </td>                                       
                                        
                                         </tr>
                                    @endforeach
                                        <tr style="border: none">
                                        	<td colspan="3" style="border: none">
                                            <div class="container-fluid text-center">
                                                <button class="btn btn-info">Update</button>
                                                <a href="#" class="btn btn-danger">Cancel</a>
                                            </div>
                                           </td>
                                        </tr>
                                        </tbody>
							        </table>
							       </div>	
							       					       
							  </div>
							</form>
						 </div>
<style>
table th {
	background-color: #CCCCCC !important;
}
</style>
<script type="text/javascript">

	function check(id) {
		var required = $('#required'+id).val();
		if (required==1){
        	$('#required'+id).val("0");
        } else{
        	$('#required'+id).val("1");
        }
	}

	function Ischeck(id) {
		var hide = $('#hide'+id).val();
		if (hide==1){
        	$('#hide'+id).val("0");
        } else{
        	$('#hide'+id).val("1");
        }
	}

</script>