<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
<div class="container">

	<div class="row pb-4" style="border: 1px solid #dee2e6;">
		<div class="col-lg-7">
			<div class="row">
				<form class="form-inline" method="post" action="{{URL::to('admin/updateBolText')}}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				@foreach($addendums as $addendum)
				<input type="hidden" name="id" value="{{$addendum->id}}">
				<input type="hidden" name="bol" id="bol" value="{{$addendum->bol}}">
			    <label for="bol_type" name="bol">BOL:</label>
			    <select onchange="selectChange()">
			    	<option value="Default BOL">Default BOL</option>
			    	<option value="Texas BOL">Texas BOL</option>
			    	<option value="BOL, V2">BOL, V2</option>
			    </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    <?php if($addendum->subtotal)
            	{
            		$is_subtotal='checked'; 
            	}else {
            		$is_subtotal='';
            	}
            	?>
			    <div class="form-check">
			      <label class="form-check-label">
			      	<span>Subtotal:</span>
			      	<input type="hidden" name="subtotal" id="subtotal" value="{{$addendum->subtotal}}">
			        <input class="form-check-input" type="checkbox" onchange="Issubtotal()" value="{{$addendum->subtotal}}" {{$is_subtotal}}>
			      </label>
			    </div>&nbsp;&nbsp;&nbsp;&nbsp;
			    <?php if($addendum->tip)
            	{
            		$is_tip='checked'; 
            	}else {
            		$is_tip='';
            	}
            	?>
			    <div class="form-check">
			      <label class="form-check-label">
			      	<span>Tip:</span>
			        <input type="hidden" name="tip" id="tip" value="{{$addendum->tip}}">
			        <input class="form-check-input" type="checkbox" onchange="Istip()" value="{{$addendum->tip}}" {{$is_tip}}>
			      </label>
			    </div>
			   
			  
			</div>

			<div class="row">
                <?php if($addendum->cc_fee)
            	{
            		$is_cc_fee='checked'; 
            	}else {
            		$is_cc_fee='';
            	}
            	?>
				<div class="form-check">
			      <label class="form-check-label">
			      	<span>CC Processing Fee:</span>
			      	<input type="text" name="cc_processing_fee" id="cc_processing_fee" value="{{$addendum->cc_processing_fee}} ">
			      	<span> % </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			        <input type="hidden" name="cc_fee" id="cc_fee" value="{{$addendum->cc_fee}}">
			        <input class="form-check-input" type="checkbox" onchange="Iscc_fee()" value="{{$addendum->cc_fee}}" {{$is_cc_fee}}>
			      </label>
			    </div>
			</div>
            <br>

			<div class="row form-inline">
				<?php if($addendum->shipment_text)
            	{
            		$is_shipment_text='checked'; 
            	}else {
            		$is_shipment_text='';
            	}
            	?>
			    <div class="form-check">
			      <label class="form-check-label">
			      	<span>Shipment Text:</span>
			        <input type="hidden" name="shipment_text" id="shipment_text" value="{{$addendum->shipment_text}}">
			        <input class="form-check-input" type="checkbox" onchange="Isshipment_text()"  value="{{$addendum->shipment_text}}" {{$is_shipment_text}}>
			      </label>
			    </div>&nbsp;&nbsp;&nbsp;&nbsp;
			    <?php if($addendum->storage_access)
            	{
            		$is_storage_access='checked'; 
            	}else {
            		$is_storage_access='';
            	}
            	?>
			    <div class="form-check">
			      <label class="form-check-label">
			      	<span>Storage Accounts Access:</span>
			        <input type="hidden" name="storage_access" id="storage_access" value="{{$addendum->storage_access}}">
			        <input class="form-check-input" type="checkbox" onchange="Isstorage_access()" value="{{$addendum->storage_access}}" {{$is_storage_access}}>
			      </label>
			    </div>&nbsp;&nbsp;&nbsp;&nbsp;
			    <?php if($addendum->notice)
            	{
            		$is_notice='checked'; 
            	}else {
            		$is_notice='';
            	}
            	?>
			    <div class="form-check">
			      <label class="form-check-label">
			      	<span>Notice:</span>
			        <input type="hidden" name="notice" id="notice" value="{{$addendum->notice}}">
			        <input class="form-check-input" type="checkbox" onchange="Isnotice()" value="{{$addendum->notice}}" {{$is_notice}}>
			      </label>
			    </div>
			  
			</div>
			<br>

			<div class="form-group row">
			    <label for="days" class="col-sm-6 col-form-label ">How Many Days in Advance:</label>
			    <div class="col-sm-6">
			      <input type="text" class="form-control " id="days" name="days_in_advance" value="{{$addendum->days_in_advance}}">
			    </div>
            </div>
            <div class="form-group row">
			    <label for="amount" class="col-sm-6 col-form-label">Article Per Pound amount:</label>
			    <div class="col-sm-6">
			      <input type="text" class="form-control " id="amount" name="article_per_pound_amount" value="{{$addendum->article_per_pound_amount}}">
			    </div>
            </div>
            <div class="form-group row">
			    <label for="time-rate" class="col-sm-6 col-form-label" >Overtime Rate:	</label>
			    <div class="col-sm-6">
			      <input type="text" class="form-control " id="overtime_rate" name="overtime_rate" value="{{$addendum->overtime_rate}}">
			    </div>
            </div>
            <div class="form-group row">
			    <label for="time-charge" class="col-sm-2 col-form-label" >Overtime Charge After:	</label>
			    <div class="col-sm-8">
			      <input type="text" class="form-control " id="vertime_charge_after" name="overtime_charge_after" value="{{$addendum->overtime_charge_after}}">
			    </div>
			    <span class="col-sm-2">hours.</span>
            </div>
            <?php if($addendum->disable_for_foreman)
            	{
            		$is_disable_for_foreman='checked'; 
            	}else {
            		$is_disable_for_foreman='';
            	}
            	?>
            <div class="form-group row">
			    <label for="Disable" class="col-sm-6 col-form-label" >Disable For Foreman:</label>
			    <div class="col-sm-6">&nbsp;&nbsp;&nbsp;&nbsp;
			      <input type="hidden" name="disable_for_foreman" id="disable_for_foreman" value="{{$addendum->disable_for_foreman}}">
			      <input class="form-check-input" type="checkbox" onchange="Isdisable_for_foreman()"  value="{{$addendum->disable_for_foreman}}" {{$is_disable_for_foreman}}>
			    </div>
            </div>
            <?php if($addendum->crew_review_popup)
            	{
            		$is_crew_review_popup='checked'; 
            	}else {
            		$is_crew_review_popup='';
            	}
            	?>
            <div class="form-group row">
			    <label for="Review" class="col-sm-6 col-form-label" >Crew Review Pop-up:</label>
			    <div class="col-sm-6">&nbsp;&nbsp;&nbsp;&nbsp;
			      <input type="hidden" name="crew_review_popup" id="crew_review_popup" value="{{$addendum->crew_review_popup}}">
			      <input class="form-check-input" type="checkbox" onchange="Iscrew_review_popup()" value="{{$addendum->crew_review_popup}}" {{$is_crew_review_popup}}>
			    </div>
            </div>
            <div class="form-group row">
			    <label for="Rating" class="col-sm-6 col-form-label" >Rating default text:</label>
			    <div class="col-sm-6">
			      <textarea  class="md-textarea form-control" name="rating_default_text" rows="3" placeholder="We would love to get your feedback on how your service was today from our company." value="{{$addendum->rating_default_text}}"></textarea>
			     </div>
            </div>
            <?php if($addendum->show_tip_crew_popup)
            	{
            		$is_show_tip_crew_popup='checked'; 
            	}else {
            		$is_show_tip_crew_popup='';
            	}
            	?>
            <div class="form-group row">
			    <label for="Crew Pop-up" class="col-sm-6 col-form-label" >Show Tip For Crew Pop-up:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    <div class="col-sm-5">
			      <input type="hidden" name="show_tip_crew_popup" id="show_tip_crew_popup" value="{{$addendum->show_tip_crew_popup}}">
			      <input class="form-check-input" type="checkbox" onchange="Isshow_tip_crew_popup()" value="{{$addendum->show_tip_crew_popup}}" {{$is_show_tip_crew_popup}}>
			    </div>
            </div>
            <div class="form-group row">
			    <label for="crew default" class="col-sm-6 col-form-label" >Tip for crew default text:</label>
			    <div class="col-sm-6">
			      <textarea  class="md-textarea form-control" name="tip_for_crew_default" rows="3" placeholder="Would you like to tip your Crew today?"value="{{$addendum->tip_for_crew_default}}"></textarea>
			     </div>
            </div>
          @endforeach
		</div>
		<div class="container-fluid text-center">
            <button class="btn btn-info">Update</button>
        </div>
      </form>
	</div>
	
</div>
<script type="text/javascript">
    $( "select" )
  .change(function() {
    var str = "";
    //alert($( this ).val());
    $( "select option:selected" ).each(function() {
      str = $( this ).val();

    });
    $( "#bol" ).val( str );
  })

	function Issubtotal() {
		var sub = $('#subtotal').val();
		if (sub==1){
        	$('#subtotal').val("0");
        } else{
        	$('#subtotal').val("1");
        }
	}

	function Istip() {
		var tip = $('#tip').val();
		if (tip==1){
        	$('#tip').val("0");
        } else{
        	$('#tip').val("1");
        }
	}
	function Iscc_fee() {
		var cc_fee = $('#cc_fee').val();
		if (cc_fee==1){
        	$('#cc_fee').val("0");
        } else{
        	$('#cc_fee').val("1");
        }
	}
	function Isshipment_text() {
		var shipment_text = $('#shipment_text').val();
		if (shipment_text==1){
        	$('#shipment_text').val("0");
        } else{
        	$('#shipment_text').val("1");
        }
	}
	function Isstorage_access() {
		var storage_access = $('#storage_access').val();
		if (storage_access==1){
        	$('#storage_access').val("0");
        } else{
        	$('#storage_access').val("1");
        }
	}
	function Isnotice() {
		var notice = $('#notice').val();
		if (notice==1){
        	$('#notice').val("0");
        } else{
        	$('#notice').val("1");
        }
	}
	function Isdisable_for_foreman() {
		var disable = $('#disable_for_foreman').val();
		if (disable==1){
        	$('#disable_for_foreman').val("0");
        } else{
        	$('#disable_for_foreman').val("1");
        }
	}
	function Iscrew_review_popup() {
		var crew = $('#crew_review_popup').val();
		if (crew==1){
        	$('#crew_review_popup').val("0");
        } else{
        	$('#crew_review_popup').val("1");
        }
	}
	function Isshow_tip_crew_popup() {
		var show = $('#show_tip_crew_popup').val();
		if (show==1){
        	$('#show_tip_crew_popup').val("0");
        } else{
        	$('#show_tip_crew_popup').val("1");
        }
	}
	



</script>
