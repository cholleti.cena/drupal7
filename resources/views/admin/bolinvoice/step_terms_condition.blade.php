  
<div class=row>
  <div class="col-sm">
    <h5>TERMS AND CONDITIONS</h5>
  </div>
  <div class="col-sm">
    <a class="btn btn-info float-right" href="#" data-toggle="modal" data-target="#myModal">Add Terms and Conditions</a>
  </div>
 </div>

  <div class="row">        
     
        <div>
        <div class="panel-body">

 <table class="table table-bordered">
    <thead>
	   
	  <tr>
	  	<th>Heading</th>
	  	<th>Content</th>
	  	<th>Action</th>
	  </tr>
	</thead>
	<tbody>
		 @foreach($addendums as $addendum)

		 <tr>
		 	<td>{{$addendum->heading}}</td>
		 	<td>{{$addendum->content}}</td>
		 	<td>
		 		<div>
		 			<a href="#"><i class="fa fa-edit cursorPoint term_cond" data-id="{{$addendum->id}}"></i></a>
		 			 <a href="bolinvoice/deleteTerms/{{$addendum->id}}"><i class="fa fa-trash cursorPoint objDelete" data-id="{{$addendum->id}}"></i></a>
		 		</div>
		 	           
		 	</td>
		 </tr>
		 @endforeach


	</tbody>

</table>
</div>



 
  <!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#fff;">
          <h4 class="modal-title">New Terms and Conditions</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form method="post" action="{{URL::to('admin/addTermsandconditions')}}">	
          	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	          <div class="row col-sm-12">
	          	<label class="col-sm-4">Heading:</label>
	          	<textarea class="col-sm-8" name="heading"></textarea>
	          </div>
	          <div class="row col-sm-12">
	          	<label class="col-sm-4">Content:</label>
	          	<textarea class="col-sm-8" name="content" rows="5"></textarea>
	          </div>
	          <div class="container-fluid text-center">
	              <button class="btn btn-info">Add</button>
	              {{ link_to_route('bolinvoice.index','Cancel',null, array('class' => 'btn btn-danger')) }}
	           </div>
	        </form>
         </div>
      </div>
    </div>
  </div>
  
   <!-- The Modal -->
  <div class="modal" id="TermModal">
    <div class="modal-dialog">
      <div class="modal-content" style="display:block">
      
        <!-- Modal Header -->
        <div class="modal-header" style="background-color:#fff;">
          <h4 class="modal-title">Terms and Conditions</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" id="editBody">
          <form method="post" action="{{URL::to('admin/updateTerms')}}">	
          	<input type="hidden" name="_token" value="{{ csrf_token() }}">
          	<input name="id" id="id" value="{{$addendum->id}}" type="hidden" />
	          <div class="row col-sm-12">
	          	<label class="col-sm-4">Heading:</label>
	          	<textarea class="col-sm-8" name="heading" id="term-heading"value="{{$addendum->heading}}"></textarea>
	          </div>
	          <div class="row col-sm-12">
	          	<label class="col-sm-4">Content:</label>
	          	<textarea class="col-sm-8" name="content" rows="5"  id="term-content" value="{{$addendum->content}}"></textarea>
	          </div>
	          <div class="container-fluid text-center">
	              <button class="btn btn-info">Update</button>
	              {{ link_to_route('bolinvoice.index','Cancel',null, array('class' => 'btn btn-danger')) }}
	           </div>
	        </form>
         </div>
      </div>
    </div>
  </div>


  <script>
  	 var tsmodal = document.getElementById("TermModal");
    
     $(".term_cond").click(function(){
      var tsid = $(this).attr('data-id');
      $.get("bolinvoice/editTerms/"+tsid, function(res) {
        if(res && res.data && res.data.length > 0 ){
          var data = res.data[0];
          tsmodal.style.display = "block";
          $("#id").val(data.id);
          $("#term-heading").val(data.heading);
          $("#term-content").val(data.content);
          //$("#create").val("Update");
         // $("#popuptitle").text("Update");
        } else {
          alert("data not found")
        }
      }).fail(function() {
        console.log("fail")
      }).done(function() {
        console.log("completed");
      })
    })

    </script>