@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Invoice
@endsection
@section('module')
Invoice
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $(".showSingle").click(function(){
    $('.cnt').hide();
    $('.active').removeClass('active');
    $('.circle-active').removeClass('circle-active');
    $('#div'+$(this).attr('target')).show();
    $('#mar-left'+$(this).attr('target')).addClass('active');
    $('#circle'+$(this).attr('target')).addClass('circle-active');
  });
});
</script>
</head>
<style>
  #div2,#div3{display:none;}
  #menu{margin-left:20px;}
  .circle {
    color: darkgrey;
    display: inline;
    font-size: 19px;
    font-weight: 400;
    line-height: 12px;
    padding: 7px 13px;
    border: 2px solid rgba(24,28,33,0.06);
    border-radius: 50%;
    line-height: normal;
    position: absolute;
    text-align: center;
    z-index: 2;
    -webkit-transition: all .1s linear 0s;
    transition: all .1s linear 0s;
    margin-top: 17px;
      }
 .circle-active{
    color: #26B4FF;
    font-weight: 400;
    padding: 7px 13px;
    font-size: 19px;
    border-radius: 50%;
    border: 2px solid #26B4FF
 }     
 .mar-left {
    margin-left: 50px;
    color: darkgrey;
  }
  .active {
    color: #4E5155;
    font-weight: bold;
}
  .mar {
    margin-left: 50px;
    margin-top: 17px;
}
.text-muted {
    color: #a3a4a6 !important;
}
a:hover{text-decoration: none!important;}
</style>
@section('content')
<h3 style="margin-bottom:40px;">BOL / Invoice Settings</h3>
@include('admin.components.message')	

<div class="content-wrapper container-xxl p-0">
        <div class="content-header row"></div>
        <div class="content-body">
          <section id="dashboard-ecommerce">
            <div class="">
              
                <div class="table-bar2">

                <div class="container">
                  <div class="row ml-4" id="menu">
                        <div class="col-sm ">
                          <a href="#" class="showSingle" target="1">
                            <span class="circle mr-2 circle-active" id="circle1">1</span>
                            <div class="text-muted mar"> FIRST STEP</div>
                            <div class="mar-left active" id="mar-left1"> Bol Text </div>
                          </a>
                        </div>
                        <div class="col-sm">
                          <a href="#" class="showSingle" target="2">                
                            <span class="circle mr-2" id="circle2">2</span>
                            <div class="text-muted mar"> SECOND STEP</div>
                            <div class="mar-left" id="mar-left2"> Addendum </div>
                          </a>
                        </div>
                        <div class="col-sm">
                          <a href="#" class="showSingle" target="3">
                           <span class="circle mr-2" id="circle3">3</span>
                            <div class="text-muted mar"> THIRD STEP</div>
                            <div class="mar-left" id="mar-left3"> Verbiage </div>
                          </a>
                        </div>
                  </div>
                </div><br><br>
                <div class="container">
                <div class="cnt" id="div2">@include('admin.bolinvoice.step_two')</div>
                <div class="cnt" id="div3">
                  @php
                  echo App\Http\Controllers\Admin\BolinvoiceController::getVerbiage();
                 @endphp
               </div>  
                <div class="cnt" id="div1" >
                  <div class="row">
                    @php
                    echo App\Http\Controllers\Admin\BolinvoiceController::getBoltext();
                   @endphp
                 </div>
                 <div class="row">
                  @php
                        echo App\Http\Controllers\Admin\BolinvoiceController::getNoticeandpolicies();
                       @endphp
                </div>
                 <div class="row">
                    @php
                    echo App\Http\Controllers\Admin\BolinvoiceController::getInvoiceNotes();
                   @endphp
                 </div>
                 <div class="row">
                  @php
                        echo App\Http\Controllers\Admin\BolinvoiceController::getTerms();
                       @endphp
                </div>
                 <div class="row">
                  @php
                        echo App\Http\Controllers\Admin\BolinvoiceController::getInvoiceMaxPrice();
                       @endphp
                </div>
                 
                
                </div>
                <!--<div class="cnt" id="div1">@include('admin.bolinvoice.step_one')</div>-->
                
                <!--<div class="cnt" id="div3">@include('admin.bolinvoice.step_three')</div>-->
                </div>
            </div>
          </section>
        </div>  
      </div>

@endsection