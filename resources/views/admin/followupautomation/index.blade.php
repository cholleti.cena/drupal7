@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Packing Materials
@endsection

@section('content')
@include('admin.components.message')	

<div class="content-wrapper container-xxl p-0">
        <div class="content-header row"></div>
        <div class="content-body">
          <section id="dashboard-ecommerce">
            <div class="">
                                <div class="btn-group pull-right">
                                    @if($privileges['Add']=='true') 
                                    <a href="{{URL::to('admin/followupautomation/create')}}" class="btn btn-info"><i class="fa fa-edit"></i>Add Follow Up Automation</a>
                                        @endif
                                </div>
              
                                <div class="table-bar2">
                                    <table id="example" class="table table-striped" style="width:100%">
                                    <thead class="thead">
                                            <tr>
                                                <th>Days Until Move</th>
                                                <th>Follow-Up Hours</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($followupautomation as $followup)
                                    <tr>
                                        <td>
                                            {{$followup->days_until_move}}
                                        </td>  
                                        <td>
                                            {{$followup->follow_up_hours}}
                                        </td>                             
                                        <td>
                                            {{$followup->status}}
                                        </td>                                       
                                        <td width="25%">
                                            <div >
                                                <div style="float:left;padding:0 10px 10px 0;">
                                                 @if($privileges['Edit']=='true')
                                                {{ link_to_route('followupautomation.edit','Edit',array($followup->id), array('class' => 'btn btn-info')) }}
                                                @endif 
                                                </div>
                                                <div style="float:left;padding-right:10px;">
                                                   @if($privileges['Delete']=='true')
                                                    {{ Form::open(array('onsubmit' => 'return confirm("Are you sure you want to delete?")','method' => 'DELETE', 'route' => array('followupautomation.destroy', $followup->id))) }}
                                                    <button type="submit" class="btn btn-danger btn-xs pull-right" style="font-size: 11px;padding: 11px 12px;">Delete</button>
                                                    {{ Form::close() }}
                                                   @endif
                                                </div>
                                            </div>
                                        </td>
                                         </tr>
                                    @endforeach
                                        </tbody>
                                    </table>
                                    {!! $followupautomation->render() !!}
                                </div>
                                
   
                
            </div>
          </section>
        </div>  
      </div>

@endsection