@extends('admin.layouts.master')
@section('title')
{{env('APP_NAME')}}-Testimonials
@endsection
@section('module')
Testimonials
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return TestimonialsValidate();','route' => 'testimonials.store','files'=>true)) }}
<div class="form-group form-horizontal">
		<div class="panel panel-default">
		</br>
			<div class="col-md-6">
        {{ Form::ahTextarea('message','Message :','',array('size' => '20x3'))  }}
				{{ Form::ahText('name','Customer Name :','',array('maxlength' => '100'))  }}				
		        {{ Form::ahSelect('is_active','Status :','1',array('1' => 'Active', '0' => 'Inactive')) }}
				</br>
		    </div>
		    <div class="row">         
          <?php                
                $no_image=env('NO_PROFILE_IMAGE');
                ?>
            <div class="col-md-4">
                    <div class="form-group">            
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 130px; height: 120px;">
                                <img src="{{env('SITE_URL')}}/<?php echo $no_image ?>" alt="..." style="width: 130px; height: 120px;">
                            </div>
                             <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 130px; max-height: 120px;"></div>
                                <div>
                                   <span class="btn btn-primary btn-file"><span class="fileinput-new">Select Logo</span><span class="fileinput-exists">Change</span>
                                   <input type="file" name="logo" id="logo">
                                    </span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                 </div>
                        </div>
                    </div>
                </div>          
        </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('testimonials.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	 </div>
 </div>
@endsection