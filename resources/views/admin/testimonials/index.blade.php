@extends('admin.layouts.master')
@section('title')
{{env('APP_NAME')}}-Testimonials
@endsection
@section('module')
Testimonials
@endsection

@section('content')
@include('admin.components.message')	
<div class="row">
    <div class="col-md-12">
		<div class="panel panel-default">
                                <div class="panel-heading">          
                                    <div class="btn-group pull-left">
                                    @if($privileges['Add']=='true') 
                                        <a href="{{URL::to('/admin/testimonials/create')}}" class="btn btn-info"><i class="fa fa-edit"></i>Add Testimonial</a>
                                        @endif
                                    </div>
                                </div>
                                <div class="panel-body table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Message</th>
                                                <th>Image</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                                    @foreach($testimonials as $testimonial)
                                    <tr>
                                        <td>
                                            {{$testimonial->name}}
                                        </td>  
                                        <td width="50%">
                                            {{$testimonial->message}}
                                        </td>     
                                         <td>
                                              <?php
                                            $logo_path = '';
                                            $no_image=env('NO_PROFILE_IMAGE');
                                            if(!empty($testimonial->image))
                                            {
                                                $logo_path = $testimonial->image;
                                             ?>
                                             <img src="<?php echo $logo_path ?>" alt="..." style="width: 130px; height: 102px;">
                                             <?php } else { ?>
                                             <img src="../../<?php echo $no_image ?>" alt="..." style="width: 130px; height: 111px;">
                                             <?php } ?>
                                        </td>                                    
                                        <td>
                                            {{$testimonial->status}}
                                        </td>                                       
                                        <td width="25%">
                                            <div >
                                                <div style="float:left;padding:0 10px 10px 0;">
                                                 @if($privileges['Edit']=='true')
                                                {{ link_to_route('testimonials.edit','Edit',array($testimonial->id), array('class' => 'btn btn-info')) }}
                                                @endif 
                                                </div>
                                                <div style="float:left;padding-right:10px;">
                                                   @if($privileges['Delete']=='true')
                                                    {{ Form::open(array('onsubmit' => 'return confirm("Are you sure you want to delete?")','method' => 'DELETE', 'route' => array('testimonials.destroy', $testimonial->id))) }}
                                                    <button type="submit" class="btn btn-danger btn-xs pull-right" style="font-size: 11px;padding: 4px 12px;">Delete</button>
                                                    {{ Form::close() }}
                                                   @endif
                                                </div>
                                            </div>
                                        </td>
                                         </tr>
                                    @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
    					</div>
    				</div> 

@endsection