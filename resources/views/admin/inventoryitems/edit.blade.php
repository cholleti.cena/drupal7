@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Inventory Items
@endsection
@section('module')
Inventory Items
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSwitch', 'admin.components.form.switch', ['name', 'labeltext'=>null, 'value' => null, 'checkstatus' => false, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return UserEditValidate();','method' => 'PUT', 'route' => array('inventoryitems.update',$item->id),'files'=>true)) }}
<div class="card mb-4">
   <div class="card-body after-loading-icon">
     <div class="container-fluid">
      	<div class="col-md-6">
			<div class="col-md-6">
            {{ Form::ahText('name','Name :',$item->name,array('maxlength' => '100'))  }}			
						{{ Form::ahNumber('price','Price :',$item->price,array()) }}
						{{ Form::ahNumber('cubic_foot','CuFT :',$item->cubic_foot,array()) }}
						{{ Form::ahNumber('weight','Lbs :',$item->weight,array()) }}
						{{ Form::ahTextarea('message','Message :',$item->message,array("onchange"=>"getlatitudelongitude(this)",'size' => '30x5'))  }}
						<div class="form-group" style="margin:5px">
								<label for="category_id" class="control-label col-sm-4">Category :</label>
								<div class="col-sm-8">
									 <select name="category_id" data-live-search='true' class="form-control select">
	                    @foreach($metadata['categories'] as $category)
	                    <option value="{{$category['id']}}" <?php 
							                      $res = $category['id'];
							                      $db_res = $item->category_id->id;
							                      if($res == $db_res) echo 'selected="selected"' ?>>{{$category['name']}}</option>
	                  @endforeach
	                </select>
								</div>
						</div>
						{{ Form::ahNumber('moving_time','Moving Time :',$item->moving_time,array()) }}
						{{ Form::ahSwitch('over_size','Over Size :',null,$item->over_size) }}
						{{ Form::ahSwitch('taxable','Taxable :',null,$item->taxable) }}
		        {{ Form::ahSelect('is_active','Status :',$item->is_active,array('1' => 'Active', '0' => 'Inactive')) }}
            </br>
		    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('inventoryitems.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	</div>
  </div>
</div>
@endsection