<!DOCTYPE html>
<html>
<head>
	<title>Send OTP</title>
</head>
<body>
<h4>{{$data['reset_code']}} is the OTP for {{getenv('APP_NAME')}} email verification. Please don't share the OTP with anyone. Thanks {{getenv('APP_NAME')}}; 
</h4>
</body>
</html>