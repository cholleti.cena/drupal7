<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <title>{{env('APP_NAME')}}</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" /> 
        <meta name="csrf-token" content="{{ csrf_token() }}" />       
        <link rel="icon" type="image/png" sizes="32x32" href="{{env('SITE_URL')}}/theme/favicon-32x32.png">    
        <link rel="stylesheet" type="text/css" id="theme" href="{{env('SITE_URL')}}/theme/css/theme-blue.css"/>
    </head>
    <body>       
        <div class="login-container">        
            <div class="login-box animated fadeInDown">
                <div class="login-body">
                    <div class="login-title" style="text-align: center;"><strong>Authentication</strong> </div>
                    <div class="col-md-12">
                    @if ($errors->all())
                        <div class="alert alert-danger">
                        
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>        
                            @endforeach
                        </div>
                    @elseif( Session::has( 'success' ))
                        <div class="alert alert-success">  {{ Session::get( 'success' ) }}
                          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        </div>
                        @elseif( Session::has( 'warning' ))
                        <div class="alert alert-danger">{{ Session::get( 'warning' ) }}
                          <button type="button" class="close" data-dismiss="alert">×</button>
                        </div>
                    @endif
                    </div>
                    <form action="{{URL::to('admin/auth')}}" class="form-horizontal" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input autofocus type="text" id="secretkey" name="secretkey" class="form-control" placeholder="AUTH Key" required>
                        </div>
                    </div>                    
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="/admin" class="btn btn-link btn-block">Go To Login Page</a>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-info btn-block">Validate</button>
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="col-md-4">
                            
                        </div>
                        <div class="col-md-4">
                           
                        </div>
                        <div class="col-md-4">                            
                            
                        </div>
                    </div>
                                                          
                    </form>
                </div>
                <div class="login-footer" style="text-align: center;">                    
                        &copy; <?php echo date("Y"); ?> {{env('APP_NAME')}}                    
                </div>
            </div>
            
        </div>
    </body>
</html>