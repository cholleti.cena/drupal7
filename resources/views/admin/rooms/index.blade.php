@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Rooms
@endsection
@section('module')
Rooms
@endsection

@section('content')
@include('admin.components.message')	

<div class="content-wrapper container-xxl p-0">
        <div class="content-header row"></div>
        <div class="content-body">
          <section id="dashboard-ecommerce">
            <div class="">
                                <div class="btn-group pull-right">
                                    @if($privileges['Add']=='true') 
                                    <a href="{{URL::to('admin/rooms/create')}}" class="btn btn-info"><i class="fa fa-edit"></i>Add Room</a>
                                        @endif
                                </div>
              
                                <div class="table-bar2">
                                    <table id="example" class="table table-striped" style="width:100%">
                                    <thead class="thead">
                                            <tr>
                                                <th>Name</th>
                                                <th>Price</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($rooms as $room)
                                    <tr>
                                        <td>
                                            {{$room->name}}
                                        </td>    
                                        <td>
                                            {{$room->price}}
                                        </td>                                  
                                        <td>
                                            {{$room->status}}
                                        </td>                                       
                                        <td width="25%">
                                            <div >
                                                <div style="float:left;padding:0 10px 10px 0;">
                                                 @if($privileges['Edit']=='true')
                                                {{ link_to_route('rooms.edit','Edit',array($room->id), array('class' => 'btn btn-info')) }}
                                                @endif 
                                                </div>
                                                <div style="float:left;padding-right:10px;">
                                                   @if($privileges['Delete']=='true')
                                                    {{ Form::open(array('onsubmit' => 'return confirm("Are you sure you want to delete?")','method' => 'DELETE', 'route' => array('rooms.destroy', $room->id))) }}
                                                    <button type="submit" class="btn btn-danger btn-xs pull-right" style="font-size: 11px;padding: 11px 12px;">Delete</button>
                                                    {{ Form::close() }}
                                                   @endif
                                                </div>
                                            </div>
                                        </td>
                                         </tr>
                                    @endforeach
                                        </tbody>
                                    </table>
                                    {!! $rooms->render() !!}
                                </div>
                                
   
                
            </div>
          </section>
        </div>  
      </div>

@endsection