@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Travel Time
@endsection

@section('content')
@include('admin.components.message')	

<div class="content-wrapper container-xxl p-0">
        <div class="content-header row"></div>
        <div class="content-body">
          <section id="dashboard-ecommerce">
            <div class="">
                                <div class="btn-group pull-right">
                                    @if($privileges['Add']=='true') 
                                    <a href="{{URL::to('admin/fuelcharges/create')}}" class="btn btn-info"><i class="fa fa-edit"></i>Add Fuel Charges</a>
                                        @endif
                                </div>
              
                                <div class="table-bar2">
                                    <table id="example" class="table table-striped" style="width:100%">
                                    <thead class="thead">
                                            <tr>
                                                <th>Min Range</th>
                                                <th>Max Range</th>
                                                <th>Min Price</th>
                                                <th>Price/mile</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($fuelcharges as $charge)
                                    <tr>
                                        <td>
                                            {{$charge->min_range}}
                                        </td>  
                                        <td>
                                            {{$charge->max_range}}
                                        </td> 
                                        <td>
                                            {{$charge->min_price}}
                                        </td> 
                                        <td>
                                            {{$charge->price_per_mile}}
                                        </td>                             
                                        <td>
                                            {{$charge->status}}
                                        </td>                                       
                                        <td width="25%">
                                            <div >
                                                <div style="float:left;padding:0 10px 10px 0;">
                                                 @if($privileges['Edit']=='true')
                                                {{ link_to_route('fuelcharges.edit','Edit',array($charge->id), array('class' => 'btn btn-info')) }}
                                                @endif 
                                                </div>
                                                <div style="float:left;padding-right:10px;">
                                                   @if($privileges['Delete']=='true')
                                                    {{ Form::open(array('onsubmit' => 'return confirm("Are you sure you want to delete?")','method' => 'DELETE', 'route' => array('fuelcharges.destroy', $charge->id))) }}
                                                    <button type="submit" class="btn btn-danger btn-xs pull-right" style="font-size: 11px;padding: 11px 12px;">Delete</button>
                                                    {{ Form::close() }}
                                                   @endif
                                                </div>
                                            </div>
                                        </td>
                                         </tr>
                                    @endforeach
                                        </tbody>
                                    </table>
                                    {!! $fuelcharges->render() !!}
                                </div>
                                
   
                
            </div>
          </section>
        </div>  
      </div>

@endsection