@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Schedules
@endsection
@section('module')
Schedules
@endsection

@section('content')
@include('admin.components.message')

<link rel="stylesheet" href="{{env('SITE_URL')}}/theme/calendar/fonts/icomoon/style.css">
  
    <link href="{{env('SITE_URL')}}/theme/calendar/fullcalendar/packages/core/main.css" rel='stylesheet' />
    <link href="{{env('SITE_URL')}}/theme/calendar/fullcalendar/packages/daygrid/main.css" rel='stylesheet' />
    
    
   
    <!-- Style -->
    <link rel="stylesheet" href="{{env('SITE_URL')}}/theme/calendar/css/style.css">	

<div class="content-wrapper container-xxl p-0">
        <div class="content-header row"></div>
        <div class="content-body">
          <section id="dashboard-ecommerce">
            <div class="">
    <div id='calendar'></div>
   </div>
          </section>
        </div>  
      </div>

 <script src="{{env('SITE_URL')}}/theme/calendar/js/jquery-3.3.1.min.js"></script>
    <script src="{{env('SITE_URL')}}/theme/calendar/js/popper.min.js"></script>
    <script src="{{env('SITE_URL')}}/theme/calendar/js/bootstrap.min.js"></script>

    <script src="{{env('SITE_URL')}}/theme/calendar/fullcalendar/packages/core/main.js"></script>
    <script src="{{env('SITE_URL')}}/theme/calendar/fullcalendar/packages/interaction/main.js"></script>
    <script src="{{env('SITE_URL')}}/theme/calendar/fullcalendar/packages/daygrid/main.js"></script>



    <script>
      document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');



    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid' ],
      defaultDate: '<?php echo date('Y-m-d');?>',
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: <?php print_r(json_encode($leadRepo)); ?>
    });

    calendar.render();
  });

    </script>

    <script src="{{env('SITE_URL')}}/theme/calendar/js/main.js"></script>


@endsection