@extends('admin.layouts.master')
@section('title')
{{env('APP_NAME')}}-Logs
@endsection
@section('module')
Logs
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}

  <div class="form-group form-horizontal">
        <div class="panel panel-default">
        </br>
         <form action="{{URL::to('/admin/logs')}}" class="form-horizontal" method="get">
            <div class="col-md-6" style="margin-bottom: 16px;">    
                {{ Form::ahSelect('role','Role :',$selectedrole,$roles) }} 
            </div>
            <div class="col-md-1"> 
                {{ Form::submit('Show', array('class' => 'btn btn-primary')) }}
                </br>
            </div>
           </form>
    <div class="col-md-1">
        <form class="form-inline" action="getlogsDownload" method="GET">
        <a>
            <input type="hidden" id="role" name="role" value="{{$selectedrole}}"></input>
            <button type="submit" class="btn btn-primary"><i class="fa fa-file-excel-o fa-lg pull-center"></i>Export Excel</button>
        </a>
        </form>
     </div>
   </div>        
 </div>

  
 
<div class="row">
    <div class="col-md-12">
		<div class="panel panel-default">
                                
                                
                                <div class="panel-body table-responsive">
                                    <table id="customers2" class="table datatable">
                                    <thead>
                                            <tr>
                                                <th>Created On</th>
                                                <th>Module Name</th>
                                                <th>Action</th>
                                                <th>User Name</th>
                                                <th>Description</th>                           
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                     @foreach($logs as $log)
                                    <tr>
                                        <td>
                                            {{$log->created_on}}
                                        </td>
                                        <td>
                                            {{$log->module_name}}
                                        </td>                                        
                                        <td>
                                            {{$log->action}}
                                        </td>
                                        <td>{{$log->email}}</td>
                                        <td width="30%">{{$log->description}}</td>          
                                    </tr>
                                    @endforeach
                                    </tbody>
                                    </table>                                    
                                    
                                </div>
                               
                            </div>
    					</div>
    				</div>       
            
                                
@endsection