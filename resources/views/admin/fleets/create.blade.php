@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Categories
@endsection
@section('module')
Category
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return CategoryValidate();','route' => 'fleets.store','files'=>true)) }}

<div class="card mb-4">
   <div class="card-body after-loading-icon">
     <div class="container-fluid">
     	<div class="row ">
     		<h1>Vehicle Information</h1>
	      	<div class="col-sm">
	      		<div class="form-group row">
				    <label for="nickname" class="col-sm col-form-label ">Nickname:</label>
				    <div class="col-sm">
				      <input type="text" class="form-control " id="nickname" name="nickname">
				    </div>
                 </div>
                 <div class="form-group row">
				    <label for="make" class="col-sm col-form-label ">Make:</label>
				    <div class="col-sm">
				      <input type="text" class="form-control " id="make" name="make">
				    </div>
                 </div>
                 <div class="form-group row">
				    <label for="type" class="col-sm col-form-label ">Type:</label>
				    <div class="col-sm">
				      <select class="form-control" id="type" name="type">
				      	@foreach($fleettypes as $type)
				      	<option value='{{$type->id}}'>{{$type->name}}</option>
                		@endforeach
				      </select>
				    </div>
                 </div>
                 <div class="form-group row">
				    <label for="capacity" class="col-sm col-form-label ">CUFT Capacity:</label>
				    <div class="col-sm">
				      <input type="text" class="form-control " id="capacity" name="cuft_capacity">
				    </div>
                 </div>
                 <div class="form-group row">
				    <label for="tagnumber" class="col-sm col-form-label ">Tag Number:</label>
				    <div class="col-sm">
				      <input type="text" class="form-control " id="tagnumber" name="tag_number">
				    </div>
                 </div>
            
            <div class="form-group row">
				    <label for="insurance" class="col-sm col-form-label ">Insurance Company:</label>
				    <div class="col-sm">
				      <input type="text" class="form-control " id="insurance" name="insurance_company">
				    </div>
                 </div>
                 <div class="form-group row">
				    <label for="hourlyrate" class="col-sm col-form-label ">Hourly Rate:</label>
				    <div class="col-sm">
				      <input type="text" class="form-control " id="hourlyrate" name="hourly_rate" value="0.0">
				    </div>
                 </div>

	      	</div>
		    <div class="col-sm" style="margin-right:30px;">

		    	<div class="form-group row">
				    <label for="year" class="col-sm col-form-label ">Year:</label>
				    <div class="col-sm">
				      <select class="form-control" id="year" name="year">
				      	@foreach($years as $year)
				      	<option value='{{$year->id}}'>{{$year->year}}</option>
                		@endforeach
				      </select>
				    </div>
                 </div>
                <div class="form-group row">
				    <label for="model" class="col-sm col-form-label ">Model:</label>
				    <div class="col-sm">
				      <input type="text" class="form-control " id="model" name="model">
				    </div>
                 </div> 
                 <div class="form-group row">
				    <label for="dimensions" class="col-sm col-form-label ">Dimensions:</label>
				    <div class="col-sm">
				    	<label for="dimensions" class="col-sm col-form-label ">L:</label>
				      <input type="number" class="form-control " id="dimensions_l" name="dimensions_l">
				      <label for="dimensions" class="col-sm col-form-label ">W:</label>
				      <input type="number" class="form-control " id="dimensions_w" name="dimensions_w">
				      <label for="dimensions" class="col-sm col-form-label ">H:</label>
				      <input type="number" class="form-control " id="dimensions_h" name="dimensions_h">
				    </div>
                 </div>
                 <div class="form-group row">
				    <label for="vin" class="col-sm col-form-label ">VIN:</label>
				    <div class="col-sm">
				      <input type="text" class="form-control " id="vin" name="VIN">
				    </div>
                 </div>
             <div class="form-group row">
                 	
				    <label for="tagcountry" class="col-sm col-form-label ">Tag Country:</label>
				    <div class="col-sm">
				      <select name="tag_country" onchange='getStates(this)' class="form-select inputName countryselection ui-autocomplete-select required">
              <option value=''>Select Country</option>
              @foreach($countries as $country)
                <option value='{{$country->id}}'>{{$country->name}}</option>
                @endforeach
            </select> 
				    </div>
                 </div>
                 <div class="form-group row">
                 	
				    <label for="tagcountry" class="col-sm col-form-label ">Tag State:</label>
				    <div class="col-sm">
                      <select name="tag_state"   class="form-select statesNames inputName ui-autocomplete-select required">
                          <option value=''>Select State</option>
                      </select> 
                    <label for="state"></label>
            </div>
                 <div class="form-group row">
				    <label for="expires" class="col-sm col-form-label ">Expires:</label>
				    <div class="col-sm">
				      <input type="date" class="form-control " id="expires" name="expires">
				    </div>
                 </div>
                 <div class="form-group row">
				    <label for="policynumber" class="col-sm col-form-label ">Policy Number:</label>
				    <div class="col-sm">
				      <input type="text" class="form-control " id="policynumber" name="policy_number">
				    </div>
                 </div>

		    </div>
		    <!-- <div class='col col-6'>
          <div class="row col-sm-12">
                    <label class="control-label">Upload Image</label>
                    <input class="form-control ui-autocomplete-input required" type="file" name="employee_photo" id="employee_photo" autocomplete="off">
                    <label for="employee_photo"></label>
          </div>
          <div class="row col-sm-12">
                    <label class="control-label">Upload Registration</label>
                    <input class="form-control ui-autocomplete-input required" type="file" name="employee_signature" id="employee_signature" autocomplete="off">
                    <label for="employee_signature"></label>
          </div>
           
        </div> -->
	    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-offset-3 col-md-6 ">
		            {{ Form::submit('Save', array('class' => 'btn btn-info')) }}
		            {{ link_to_route('fleets.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	</div>
  </div>
</div>

<script>
	function getStates(that){
// $('.countryselection').on('change',function(){
    var id = $(that).val()
    $.get( "/admin/states/get/"+id, function(result) {
        var data = result.data;
        var statesHtml = '<option value="">Select State</option>';
        if(data && data.length > 0){
            data.map(function(d, i){
                statesHtml += `<option value='${d.id}'>${d.name}</option>`;
            })
        }
        $(that).parent().parent().next().find('select.statesNames').html(statesHtml)
    })
    // })
} 
</script>
@endsection