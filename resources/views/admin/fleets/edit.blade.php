@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Fleets
@endsection

@section('content')
@include('admin.components.message')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $(".list-group-item").click(function(){
    $('.info').hide();
    $('.active').removeClass('active');
    $('#data'+$(this).attr('target')).addClass('active');
    $('#div'+$(this).attr('target')).show();
  });
});
</script>




<div class="content-wrapper container-xxl p-0">
    <div class="content-header row"></div>
    <div class="content-body">
      <section id="dashboard-ecommerce">
        <div class="">
             <h3>International</h3>
            <div class="table-bar">
               <div class="row">
                   <div class="list-group" style="width:20%">
                       <a href="#" class="list-group-item active" id="data1" target="1">Vehicle Information</a>
                       <a href="#" class="list-group-item" id="data2" target="2">Vehicle Details</a>
                       <a href="#" class="list-group-item" id="data3" target="3">Maintenance</a>
                       <a href="#" class="list-group-item" id="data4" target="4">Inspections</a>
                       <a href="#" class="list-group-item" id="data5" target="5">Scheduling</a>
                       <a href="#" class="list-group-item" id="data6" target="6">Job log</a>

                   </div>
                   <div class="container" style="width:80%">
                       <div class="info" id="div1">
                        {{ Form::open(array('onsubmit' => 'return UserEditValidate();','method' => 'PUT', 'route' => array('fleets.update',$fleet->id),'files'=>true)) }}

                        <h1>Vehicle Information</h1>

                        <div class="row">
                            <div class="col-sm" >
                                <div class="form-group row">
                                    <label for="nickname" class="col-sm col-form-label ">Nickname:</label>
                                    <div class="col-sm">
                                      <input type="text" class="form-control " id="nickname" name="nickname" value="{{$fleet->nickname}}">
                                    </div>
                                 </div>
                            </div>
                            <div class="col-sm" >
                                <div class="form-group row" >
                                    <label for="year" class="col-sm col-form-label ">Year:</label>
                                    <div class="col-sm">
                                      <select class="form-control" id="year" name="year">
                                        @foreach($years as $year)
                                        <option value='{{$year->id}}' <?php 
                                                      $res = $year->id;
                                                      $db_res = $fleet->year;
                                                      if($res == $db_res) echo 'selected' ?>>{{$year->year}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                 </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm" >
                                <div class="form-group row">
                                    <label for="make" class="col-sm col-form-label ">Make:</label>
                                    <div class="col-sm">
                                      <input type="text" class="form-control " id="make" name="make" value="{{$fleet->make}}">
                                    </div>
                                 </div>
                            </div>
                            <div class="col-sm" >
                                <div class="form-group row">
                                    <label for="model" class="col-sm col-form-label ">Model:</label>
                                    <div class="col-sm">
                                      <input type="text" class="form-control " id="model" name="model" value="{{$fleet->model}}">
                                    </div>
                                 </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm" >
                               <div class="form-group row">
                                    <label for="type" class="col-sm col-form-label ">Type:</label>
                                    <div class="col-sm">
                                      <select class="form-control" id="type" name="type">
                                        @foreach($fleettypes as $type)
                                        <option value='{{$type->id}}' <?php 
                                                      $res = $type->id;
                                                      $db_res = $fleet->type;
                                                      if($res == $db_res) echo 'selected' ?>>{{$type->name}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                               </div> 
                             </div>
                             <div class="col-sm" >
                                 <div class="form-group row">
                                    <label for="dimensions" class="col-sm col-form-label ">Dimensions:</label>
                                    <div class="col-sm">
                                        <label for="dimensions" class="col-sm col-form-label ">L:</label>
                                      <input type="number" class="form-control " id="dimensions_l" name="dimensions_l" value="{{$fleet->dimensions_l}}">
                                      <label for="dimensions" class="col-sm col-form-label ">W:</label>
                                      <input type="number" class="form-control " id="dimensions_w" name="dimensions_w" value="{{$fleet->dimensions_w}}">
                                      <label for="dimensions" class="col-sm col-form-label ">H:</label>
                                      <input type="number" class="form-control " id="dimensions_h" name="dimensions_h" value="{{$fleet->dimensions_h}}">
                                    </div>
                                 </div>
                             </div>
                         </div>

                         <div class="row">
                            <div class="col-sm" >
                                <div class="form-group row">
                                    <label for="capacity" class="col-sm col-form-label ">CUFT Capacity:</label>
                                    <div class="col-sm">
                                      <input type="text" class="form-control " id="capacity" name="cuft_capacity" value="{{$fleet->cuft_capacity}}">
                                    </div>
                                 </div>
                            </div>
                            <div class="col-sm" >
                                <div class="form-group row">
                                    <label for="vin" class="col-sm col-form-label ">VIN:</label>
                                    <div class="col-sm">
                                      <input type="text" class="form-control " id="vin" name="VIN" value="{{$fleet->VIN}}">
                                    </div>
                                 </div>
                            </div>
                        </div>

                         <div class="row">
                            <div class="col-sm" >
                                <div class="form-group row">
                                    <label for="tagnumber" class="col-sm col-form-label ">Tag Number:</label>
                                    <div class="col-sm">
                                      <input type="text" class="form-control " id="tagnumber" name="tag_number" value="{{$fleet->tag_number}}">
                                    </div>
                                 </div>
                            </div>
                            <div class="form-group row">
                    
                                <label for="tagcountry" class="col-sm col-form-label ">Tag Country:</label>
                                <div class="col-sm">
                                  <select name="tag_country" onchange='getStates(this)' class="form-select inputName countryselection ui-autocomplete-select required">
                                  <option value=''>Select Country</option>
                                  @foreach($countries as $country)
                                    <option value='{{$country->id}}' 
                                        <?php 
                                          $res = $country->id;
                                          $db_res = $fleet->tag_country;
                                          if($res == $db_res) echo 'selected' ?>>{{$country->name}}</option>
                                    @endforeach
                                </select> 
                                </div>
                                     </div>
                                            </div>

                                             <div class="row">
                                                <div class="col-sm" >
                                                    <div class="form-group row">
                                                        <label for="tagstate" class="col-sm col-form-label ">Tag State:</label>
                                                        <div class="col-sm">
                                          <select name="tag_state"   class="form-select statesNames inputName ui-autocomplete-select required">
                                              <option value=''>Select State</option>
                                          </select> 
                                        <label for="state"></label>
                                </div>
                                 </div>
                            </div>
                            <div class="col-sm" >
                                <div class="form-group row">
                                    <label for="expires" class="col-sm col-form-label ">Expires:</label>
                                    <div class="col-sm">
                                      <input type="date" class="form-control " id="expires" name="expires" value="{{$fleet->expires}}">
                                    </div>
                                 </div>
                            </div>
                        </div>

                         <div class="row">
                            <div class="col-sm" >
                                <div class="form-group row">
                                    <label for="insurance" class="col-sm col-form-label ">Insurance Company:</label>
                                    <div class="col-sm">
                                      <input type="text" class="form-control " id="insurance" name="insurance_company" value="{{$fleet->insurance_company}}">
                                    </div>
                                 </div>
                            </div>
                            <div class="col-sm" >
                                <div class="form-group row">
                                    <label for="policynumber" class="col-sm col-form-label ">Policy Number:</label>
                                    <div class="col-sm">
                                      <input type="text" class="form-control " id="policynumber" name="policy_number" value="{{$fleet->policy_number}}">
                                    </div>
                                 </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm" >
                                <div class="form-group row">
                                    <label for="hourlyrate" class="col-sm col-form-label ">Hourly Rate:</label>
                                    <div class="col-sm">
                                      <input type="text" class="form-control " id="hourlyrate" name="hourly_rate" value="{{$fleet->hourly_rate}}">
                                    </div>
                                 </div>
                            </div>
                            <div class="col-sm" ></div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="panel-footer">
                                    <div class="col-md-offset-3 col-md-6 ">
                                        {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
                                        {{ link_to_route('fleets.index','Cancel',null, array('class' => 'btn btn-danger')) }}
                                    </div>
                                </div>
                            </div>
                        </div>


                       </div>

                       {{ Form::close() }}



                       <div class="info" id="div2">
                        <h1>Vehicle Details</h1>
                            {{ Form::open(array('onsubmit' => 'return UserEditValidate();','method' => 'PUT', 'route' => array('fleets.update',$fleet->id),'files'=>true)) }}
                            <div class="row">
                                <div class="form-group row">
                                    <label for="nickname" class="col-sm col-form-label ">Vehicle ID:</label>
                                    <div class="col-sm">
                                      {{$fleet->id}}
                                    </div>
                                 </div>
                            </div>
                            <div class="row">
                                <div class="form-group row" >
                                    <label for="year" class="col-sm col-form-label ">Vehicle Status:</label>
                                    <div class="col-sm">
                                      <select class="form-control" id="vehicle_status" name="vehicle_status">
                                        @foreach($vehicle_statues as $type)
                                        <option value='{{$type->id}}' <?php 
                                                      $res = $type->id;
                                                      $db_res = $fleet->vehicle_status;
                                                      if($res == $db_res) echo 'selected' ?>>{{$type->name}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                 </div>
                            </div>
                            <div class="row">
                                <div class="form-group row" >
                                    <label for="year" class="col-sm col-form-label ">Ownership:</label>
                                    <div class="col-sm">
                                      <select class="form-control" id="owner_ship" name="owner_ship">
                                        @foreach($ownships as $type)
                                        <option value='{{$type->id}}' <?php 
                                                      $res = $type->id;
                                                      $db_res = $fleet->owner_ship;
                                                      if($res == $db_res) echo 'selected' ?>>{{$type->name}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                 </div>
                            </div>
                            <div class="row">
                            <div class="form-group">
                                <div class="panel-footer">
                                    <div class="col-md-offset-3 col-md-6 ">
                                        {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
                                        {{ link_to_route('fleets.index','Cancel',null, array('class' => 'btn btn-danger')) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>

                         {{ Form::close() }}
                      
                       <div class="info" id="div3">
                          <h1>Vehicle Maintenance</h1>

                          <div class="row">
                              <h5>Upcoming maintenance</h5>
                              <table id="example" class="table table-bordered table-striped" style="width:100%">
                                    <thead class="thead">
                                        <tr>
                                            <th>Date</th>
                                            <th>Mileage</th>
                                            <th>Service center</th>
                                            <th>Mechanic</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                          </div>

                          <div class="row">
                              <h5>Completed maintenance</h5>
                              <table id="example" class="table table-bordered table-striped" style="width:100%">
                                    <thead class="thead">
                                        <tr>
                                            <th>Date</th>
                                            <th>Mileage</th>
                                            <th>Service center</th>
                                            <th>Mechanic</th>
                                            <th>Description</th>
                                            <th>Warrenty</th>
                                            <th>Cost</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                          </div>
                      </div>


                       <div class="info" id="div4">Inspection</div>

                       <div class="info" id="div5">
                        <?php 
                            if($fleet->is_schedulable) {
                                $is_schedulable = "checked";
                            }
                            else {
                                $is_schedulable = "";
                            }
                        ?>
                        {{ Form::open(array('onsubmit' => 'return UserEditValidate();','method' => 'PUT', 'route' => array('fleets.update',$fleet->id),'files'=>true)) }}
                          <div class="row" style="width:40%;">
                                <div class="form-check row">
                                    <label for="insurance" class="col-sm col-form-label ">Schedulable:</label>
                                    <div class="col-sm" style="padding-top:15px;">
                                      <input type="checkbox" class="form-check-input" id="is_schedulable" name="is_schedulable" {{$is_schedulable}}>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="form-group">
                                <div class="panel-footer">
                                    <div class="col-md-offset-3 col-md-6 ">
                                        {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
                                        {{ link_to_route('fleets.index','Cancel',null, array('class' => 'btn btn-danger')) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                       <div class="info" id="div6"><h1>Job log</h1></div>
                   </div>
               </div>
            </div>
        </div>
      </section>
    </div>  
  </div>




@endsection
<style type="text/css">
 #div2,#div3,#div4,#div5,#div6{display:none;}
 .list-group{
   
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    flex-direction: column;
    padding-left: 1px !important;
    margin-bottom: 0;
 } 
 .list-group-item{
    padding: 0.85rem 1.5rem;
    border-color: rgba(24,28,33,0.03) !important;

 } 
 .list-group-item:hover{
    z-index: 1;
    color: #4E5155;
    text-decoration: none;
    background-color: rgb(20 21 22 / 4%);
 }
 .list-group-item .active {
    z-index: 2;
    color: #4E5155 !important;
    background: transparent !important;
    font-weight: bold !important;
}
.list-group .list-group-item{
    border-right: 0;
    border-left: 0;
    border-radius: 0;

}
.list-group:first-child .list-group-item:first-child {
    border-top: 0;
}
.active {
    font-weight: bold;
}

</style>

<script>
    function getStates(that){
// $('.countryselection').on('change',function(){
    var id = $(that).val()
    $.get( "/admin/states/get/"+id, function(result) {
        var data = result.data;
        var statesHtml = '<option value="">Select State</option>';
        if(data && data.length > 0){
            data.map(function(d, i){
                statesHtml += `<option value='${d.id}'>${d.name}</option>`;
            })
        }
        $(that).parent().parent().next().find('select.statesNames').html(statesHtml)
    })
    // })
} 

$(function () {
        $("#chkPassport").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport").show();
            } else {
                $("#dvPassport").hide();
            }
        });
    });
</script>