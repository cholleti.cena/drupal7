@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Fleets
@endsection

@section('content')
@include('admin.components.message')	

<div class="content-wrapper container-xxl p-0">
    <div class="content-header row"></div>
    <div class="content-body">
      <section id="dashboard-ecommerce">
        <div class="">
            <div class="table-bar">
                <div class="row">
                    <div class="col-sm">
                        <h1>Fleets</h1>
                    </div>
                    <div class="col-sm" style="padding-bottom: 20px;">
                        <a class="btn btn-info" href="{{URL::to('admin/fleets/create')}}" style="float:right;" >New Fleet</a>
                    </div>
                 </div>
                <table id="example" class="table table-bordered table-striped" style="width:100%">
                <thead class="thead">
                        <tr>
                            <th>NickName</th>
                            <th>Year</th>
                            <th>Vehicle</th>
                            <th>Size</th>
                            <th>VIN</th>
                            <th>Tag</th>
                            <th>Maintenance</th>
                            <th>Inspection</th>
                            <th>Hourly rate ($)</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{ $fleets }}
                        @foreach($fleets as $fleet)
                        <tr>
                            <td>
                                <a href="{{ route('fleets.edit', $fleet->id) }}">{{$fleet->nickname}}</a>
                            </td> 
                            <td>
                                {{$fleet->year}}
                            </td> 

                            <td>
                                {{$fleet->vehicle}}
                            </td>                                      
                           <td>
                                {{$fleet->size}}
                            </td>
                            <td>
                                {{$fleet->VIN}}
                            </td>
                            <td>
                                {{$fleet->tag}}
                            </td>
                            <td>
                                {{$fleet->maintenance}}
                            </td>
                            <td>
                                {{$fleet->inspection}}
                            </td>
                            <td>
                                {{$fleet->hourly_rate}}
                            </td>
                       </tr>
                       @endforeach
                    </tbody>
                </table>
            </div>
        </div>
      </section>
    </div>  
  </div>

@endsection