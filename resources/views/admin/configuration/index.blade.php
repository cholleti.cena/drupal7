@extends('admin.layouts.master')
@section('title')
{{env('APP_NAME')}}-Configuration
@endsection
@section('module')
Configuration
@endsection

@section('content')
@include('admin.components.message')        

{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahDate', 'admin.components.form.date', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahCheckbox', 'admin.components.form.checkbox', ['name', 'labeltext'=>null, 'value' => null, 'checkstatus' => false, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}

{{ Form::model($defaults, array('method' => 'PATCH', 'route' => array('configuration.update', $defaults->id),'files'=>true)) }}

 <div class="form-group form-horizontal">
    <div class="panel panel-default">
    </br>
      <div class="col-md-6">
       {{ Form::ahCheckbox('allow_create_logs','Allow Create Logs :',null,$defaults->allow_create_logs ) }}
       {{ Form::ahCheckbox('allow_edit_logs','Allow Edit Logs :',null,$defaults->allow_edit_logs ) }}
       {{ Form::ahCheckbox('allow_delete_logs','Allow Delete Logs :',null,$defaults->allow_delete_logs ) }}      
       {{ Form::ahNumber('log_max_days','Log Max Days :',$defaults->log_max_days,array('maxlength' => '2','min'=>'0','max'=>'99')) }}
       {{ Form::ahText('version','Version :',$defaults->version,array('maxlength' => '255'))  }} 
      </br>
      </div>
      <div class="col-md-6">
         <div class="form-group">            
            <div class="fileinput fileinput-new" data-provides="fileinput">
            <?php
          $logo_path = '';
          $no_image=env('NO_PROFILE_IMAGE');
                if(!empty($defaults->logo))
                {
                    $logo_path = $defaults->logo;
                 ?>
            <div class="fileinput-new thumbnail" style="width: 130px; height: 111px;">
            <a>
                <img src="<?php echo $logo_path ?>" alt="..." style="width: 130px; height: 111px;">
                </a>
            </div>
            <?php } else { ?>
            <div class="fileinput-new thumbnail" style="width: 130px; height: 111px;">
            <a>
                <img src="{{env('SITE_URL')}}/<?php echo $no_image ?>" alt="..." style="width: 130px; height: 111px;">
                </a>
            </div>
             <?php } ?>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 130px; max-height: 111px;"></div>
                  <div>
                      <span class="btn btn-primary btn-file"><span class="fileinput-new">Change Logo</span><span class="fileinput-exists">Change Logo</span>
                      <input type="file" name="logou" id="logou">
                      </span>
                      <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                  </div>

              </div>
          </div>
      </br>
      </div>
      <div class="col-md-8">
      <h2>About Us</h2>
      <div class="form-group" style="margin:5px">
        <label for="redeem_limit" class="control-label col-sm-4"></label>
            <div class="col-sm-12">
                <textarea rows="4" cols="50" name="about_us" id="about_us" class="summernote">{{$defaults->about_us}}</textarea>
            </div>
        </div>
     </div>
     <div class="col-md-4">
      <h2>About Image</h2>
        <div class="form-group">            
            <div class="fileinput fileinput-new" data-provides="fileinput">
            <?php
          $logo_path = '';
          $no_image=env('NO_PROFILE_IMAGE');
                if(!empty($defaults->about_us_image))
                {
                    $logo_path = $defaults->about_us_image;
                 ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="<?php echo $logo_path ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
            <?php } else { ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="{{env('SITE_URL')}}/<?php echo $no_image ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
             <?php } ?>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px;"></div>
                  <div>
                      <span class="btn btn-primary btn-file"><span class="fileinput-new">Change About Us Logo</span><span class="fileinput-exists">Change About Us Logo</span>
                      <input type="file" name="aboutusimage" id="aboutusimage">
                      </span>
                      <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                  </div>

              </div>
          </div>
      </div>
      <div class="col-md-8">
      <h2>Who we are</h2>
      <div class="form-group" style="margin:5px">
        <label for="redeem_limit" class="control-label col-sm-4"></label>
            <div class="col-sm-12">
                <textarea rows="4" cols="50" name="who_we_are" id="who_we_are" class="summernote">{{$defaults->who_we_are}}</textarea>
            </div>
        </div>
      </div>
      <div class="col-md-4">
      <h2>Who we are</h2>
        <div class="form-group">            
            <div class="fileinput fileinput-new" data-provides="fileinput">
            <?php
          $logo_path = '';
          $no_image=env('NO_PROFILE_IMAGE');
                if(!empty($defaults->who_we_are_image))
                {
                    $logo_path = $defaults->who_we_are_image;
                 ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="<?php echo $logo_path ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
            <?php } else { ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="{{env('SITE_URL')}}/<?php echo $no_image ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
             <?php } ?>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px;"></div>
                  <div>
                      <span class="btn btn-primary btn-file"><span class="fileinput-new">Change Who we are Logo</span><span class="fileinput-exists">Change Who we are Logo</span>
                      <input type="file" name="whoweareimage" id="whoweareimage">
                      </span>
                      <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                  </div>

              </div>
        </div>
    </div>
<br>
<br>
      <div class="col-md-8">
      <h2>Vision</h2>
      <div class="form-group" style="margin:5px">
        <label for="redeem_limit" class="control-label col-sm-4"></label>
            <div class="col-sm-12">
                <textarea rows="4" cols="50" name="vision" id="vision" class="summernote">{{$defaults->vision}}</textarea>
            </div>
        </div>
      </div>
      <div class="col-md-4">
      <h2>Vision</h2>
        <div class="form-group">            
            <div class="fileinput fileinput-new" data-provides="fileinput">
            <?php
          $logo_path = '';
          $no_image=env('NO_PROFILE_IMAGE');
                if(!empty($defaults->vision_image))
                {
                    $logo_path = $defaults->vision_image;
                 ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="<?php echo $logo_path ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
            <?php } else { ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="{{env('SITE_URL')}}/<?php echo $no_image ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
             <?php } ?>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px;"></div>
                  <div>
                      <span class="btn btn-primary btn-file"><span class="fileinput-new">Change Vision Logo</span><span class="fileinput-exists">Change Vision Logo</span>
                      <input type="file" name="visionimage" id="visionimage">
                      </span>
                      <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                  </div>

              </div>
        </div>
    </div>
<br>
<br>
      <div class="col-md-8">
      <h2>Mission</h2>
      <div class="form-group" style="margin:5px">
        <label for="redeem_limit" class="control-label col-sm-4"></label>
            <div class="col-sm-12">
                <textarea rows="4" cols="50" name="mission" id="mission" class="summernote">{{$defaults->mission}}</textarea>
            </div>
        </div>
      </div>
      <div class="col-md-4">
      <h2>Mission</h2>
        <div class="form-group">            
            <div class="fileinput fileinput-new" data-provides="fileinput">
            <?php
          $logo_path = '';
          $no_image=env('NO_PROFILE_IMAGE');
                if(!empty($defaults->mission_image))
                {
                    $logo_path = $defaults->mission_image;
                 ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="<?php echo $logo_path ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
            <?php } else { ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="{{env('SITE_URL')}}/<?php echo $no_image ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
             <?php } ?>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px;"></div>
                  <div>
                      <span class="btn btn-primary btn-file"><span class="fileinput-new">Change Mission Logo</span><span class="fileinput-exists">Change Mission Logo</span>
                      <input type="file" name="missionimage" id="missionimage">
                      </span>
                      <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                  </div>

              </div>
        </div>
    </div>
  <br>
  <br>
      <div class="col-md-8">
      <h2>Objective</h2>
      <div class="form-group" style="margin:5px">
        <label for="redeem_limit" class="control-label col-sm-4"></label>
            <div class="col-sm-12">
                <textarea rows="4" cols="50" name="objective" id="objective" class="summernote">{{$defaults->objective}}</textarea>
            </div>
        </div>
      </div>
      <div class="col-md-4">
      <h2>Objective</h2>
        <div class="form-group">            
            <div class="fileinput fileinput-new" data-provides="fileinput">
            <?php
          $logo_path = '';
          $no_image=env('NO_PROFILE_IMAGE');
                if(!empty($defaults->objective_image))
                {
                    $logo_path = $defaults->objective_image;
                 ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="<?php echo $logo_path ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
            <?php } else { ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="{{env('SITE_URL')}}/<?php echo $no_image ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
             <?php } ?>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px;"></div>
                  <div>
                      <span class="btn btn-primary btn-file"><span class="fileinput-new">Change Objective Logo</span><span class="fileinput-exists">Change Objective Logo</span>
                      <input type="file" name="objectiveimage" id="objectiveimage">
                      </span>
                      <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                  </div>

              </div>
        </div>
    </div>
  <br>
  
  <br>
      <div class="col-md-8">
      <h2>OurValue</h2>
      <div class="form-group" style="margin:5px">
        <label for="redeem_limit" class="control-label col-sm-4"></label>
            <div class="col-sm-12">
                <textarea rows="4" cols="50" name="our_value" id="our_value" class="summernote">{{$defaults->our_value}}</textarea>
            </div>
        </div>
      </div>
       <div class="col-md-4">
      <h2>OurValue</h2>
        <div class="form-group">            
            <div class="fileinput fileinput-new" data-provides="fileinput">
            <?php
          $logo_path = '';
          $no_image=env('NO_PROFILE_IMAGE');
                if(!empty($defaults->our_value_image))
                {
                    $logo_path = $defaults->our_value_image;
                 ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="<?php echo $logo_path ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
            <?php } else { ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="{{env('SITE_URL')}}/<?php echo $no_image ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
             <?php } ?>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px;"></div>
                  <div>
                      <span class="btn btn-primary btn-file"><span class="fileinput-new">Change OurValue Logo</span><span class="fileinput-exists">Change OurValue Logo</span>
                      <input type="file" name="ourvalueimage" id="ourvalueimage">
                      </span>
                      <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                  </div>

              </div>
        </div>
    </div>
<br>
<br>

      <div class="col-md-8">
      <h2>Terms Conditions</h2>
      <div class="form-group" style="margin:5px">
        <label for="redeem_limit" class="control-label col-sm-4"></label>
            <div class="col-sm-12">
                <textarea rows="4" cols="50" name="terms_conditions" id="terms_conditions" class="summernote">{{$defaults->terms_conditions}}</textarea>
            </div>
        </div>
      </div>
      <div class="col-md-4">
      <h2>Terms Conditions</h2>
        <div class="form-group">            
            <div class="fileinput fileinput-new" data-provides="fileinput">
            <?php
          $logo_path = '';
          $no_image=env('NO_PROFILE_IMAGE');
                if(!empty($defaults->Terms_Conditions_image))
                {
                    $logo_path = $defaults->Terms_Conditions_image;
                 ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="<?php echo $logo_path ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
            <?php } else { ?>
            <div class="fileinput-new thumbnail" style="width: 300px; height: 300px;">
            <a>
                <img src="{{env('SITE_URL')}}/<?php echo $no_image ?>" alt="..." style="width: 300px; height: 300px;">
                </a>
            </div>
             <?php } ?>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px;"></div>
                  <div>
                      <span class="btn btn-primary btn-file"><span class="fileinput-new">Change Terms Conditions Logo</span><span class="fileinput-exists">Change Terms Conditions Logo</span>
                      <input type="file" name="aboutusimage" id="aboutusimage">
                      </span>
                      <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                  </div>

              </div>
        </div>
    </div>
  <br>
   <br>
  <div class="col-md-8">
      <h2>Company Information</h2>
      <div class="form-group" style="margin:5px">
        <label for="redeem_limit" class="control-label col-sm-4"></label>
            <div class="col-sm-12">
                <textarea rows="4" cols="50" name="address" id="address" class="summernote">{{$defaults->address}}</textarea>
            </div>
        </div>
      </div>
      <div class="form-group">
        <div class="panel-footer">
            <div class="col-md-6 col-md-offset-3">
                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
            </div>
        </div>
      </div>
   </div>
 </div>     
{{ Form::close() }}
@endsection
