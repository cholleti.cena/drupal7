@extends('admin.layouts.master')
@section('title')
{{env('APP_NAME')}}-Banners
@endsection
@section('module')
Banners
@endsection

@section('content')
@include('admin.components.message')	
<div class="row">
    <div class="col-md-12">
		<div class="panel panel-default">
                                <div class="panel-heading">          
                                    <div class="btn-group pull-left">
                                    @if($privileges['Add']=='true') 
                                        <a href="{{URL::to('/admin/banners/create')}}" class="btn btn-info"><i class="fa fa-edit"></i>Add Banner</a>
                                        @endif
                                    </div>
                                </div>
                                <div class="panel-body table-responsive">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>Banner</th>
                                                <th>Title</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                                    @foreach($banners as $banner)
                                    <tr>
                                        <td>
                                            <?php
                                            $logo_path = '';
                                            $no_image=env('NO_IMAGE');
                                            if(!empty($banner->hear_image))
                                            {
                                                $logo_path = $banner->hear_image;
                                             ?>
                                             <img src="<?php echo $logo_path ?>" alt="..." style="width: 109px;height: 71px;">
                                             <?php } else { ?>
                                             <img src="../../<?php echo $no_image ?>" alt="..." style="width: 130px; height: 111px;">
                                             <?php } ?>
                                        </td>
                                        <td>
                                            {{$banner->header_title}}
                                        </td>   
                                        <td>
                                            {{$banner->status}}
                                        </td>                                       
                                        <td width="25%">
                                            <div >
                                                <div style="float:left;padding:0 10px 10px 0;">
                                                 @if($privileges['Edit']=='true')
                                                {{ link_to_route('banners.edit','Edit',array($banner->id), array('class' => 'btn btn-info')) }}
                                                @endif 
                                                </div>
                                                <div style="float:left;padding-right:10px;">
                                                   @if($privileges['Delete']=='true')
                                                    {{ Form::open(array('onsubmit' => 'return confirm("Are you sure you want to delete?")','method' => 'DELETE', 'route' => array('banners.destroy', $banner->id))) }}
                                                    <button type="submit" class="btn btn-danger btn-xs pull-right" style="font-size: 11px;padding: 4px 12px;">Delete</button>
                                                    {{ Form::close() }}
                                                   @endif
                                                </div>
                                            </div>
                                        </td>
                                         </tr>
                                    @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
    					</div>
    				</div> 

@endsection