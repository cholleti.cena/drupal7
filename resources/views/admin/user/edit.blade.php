@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Admin Users
@endsection
@section('module')
Admin User
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}

{{ Form::open(array('method' => 'PUT', 'route' => array('user.update',$user->id),'files'=>true)) }}
<div class="card mb-4">
   <div class="card-body after-loading-icon">
     <div class="container-fluid">
      	<div class="col-md-6">
		        {{ Form::ahText('name','Name :',$user->name,array('maxlength' => '100'))  }}
		        {{ Form::ahText('email','Email :',$user->email,array('maxlength' => '100'))  }}
                {{ Form::ahNumber('mobileno','Mobile No :',$user->mobileno,array('min'=>'0','maxlength' => '11','max'=>'99999999999')) }}
		        {{ Form::ahSelect('role_id','Role :',$user->role_id,$role) }}		       
                {{ Form::ahSelect('status','Status :',$user->status,array('1' => 'Active', '2' => 'Inactive')) }}
                </br>
		    </div>
		    <div class="col-md-6">
		    	<?php 
		    	if($user->email!=''){
				 $ret='otpauth://totp/Epic:'.$user->email.'?secret='.$user->secret.'&issuer=Epic';	
				 echo DNS2D::getBarcodeHTML($ret, "QRCODE",5,5);
				 }
                ?>
               
		    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('user.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	</div>
  </div>
</div>
@endsection