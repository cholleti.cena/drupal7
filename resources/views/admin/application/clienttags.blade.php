@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Client Tags
@endsection

@section('content')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{ Form::open(array('onsubmit' => '','route' => 'clienttags.create','files'=>true)) }}

<!-- Starts List Page -->
<div class="content-wrapper container-xxl p-0">
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            <div class="">
                <div class="table-bar2">
                     <strong>Admin / Setting / Application / Client Tags</strong>   
                </div>
                <div class="table-bar2">
                    @include('admin.components.message')
                    <div id="tabsneW" style="border: 0px;">
                      <div class="btn-group btn-align-right-add-update">
                        <div class="btn btn-primary cursorPoint" id="myBtn">Add New</div>
                      </div>
                    </div>
                </div>
                <!-- <div class="btn-group pull-right">
                    <a href="{{URL::to('admin/rooms/create')}}" class="btn btn-info">
                        <i class="fa fa-add"></i>Add Fleet Type</a>
                </div> -->
                <div class="table-bar2">
                    <!-- <div class="btn-group pull-right">
                        <button class="btn btn-primary" id="myBtn">New Fleet Types</button>
                    </div> -->
                    <table id="example" class="table table-striped" style="width:100%">
                        <thead class="thead">
                            <tr>
                                <th>Tag Name</th>
                                <th>Tag Color</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($clienttags as $client)
                            <tr>
                                <td>{{ $client->name }}</td>
                                <td>{{ $client->color }}</td>
                                <th><a href="#"><i class="fa fa-edit cursorPoint editRec" data-id="{{$client->id}}"></i></a><a href="clienttags/delete/{{$client->id}}"><i class="fa fa-trash cursorPoint taskdelete" data-id="{{$client->id}}"></i></a></th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- Ends List Page -->

<!-- Starts Modal content -->
<div id="myModal" class="modal">
    <div class="modal-content" style="display:block">
        <!-- <div class="row"></div> -->
        <div>
            <strong id="popuptitle">Add New</strong>
            <span class="close pull-right">&times;</span>
        </div>
        <hr/>
        <div class="row">
            <form name="clientTagForm" id="clientTagForm" method="post" action="clienttags/create">
                <input id='record_id' name="record_id" type="hidden" />
                <div class="container">
                    <div class="col">
                        <div class="row form-group" id="prospect_field_div">
                            <div class="col col-sm-4">
                                <label class="control-label" for="task_contact_person_id">Tag Name</label>
                            <!-- </div>
                            <div class="col-form-label col-sm-4"> -->
                                <input class="form-control ui-autocomplete-input required" type="text" name="clientTagName" id="clientTagName" autocomplete="off">
                                <label for="clientTagName"></label>
                            </div>
                        </div>
                        <div class="row form-group" id="prospect_field_div">
                            <div class="col col-sm-4">
                                <label class="control-label" for="task_contact_person_id">Tag Color</label>
                            <!-- </div>
                            <div class="col-form-label col-sm-4"> -->
                                <!-- <input class="form-control ui-autocomplete-input required" type="text" name="clientTagColor" id="clientTagColor" autocomplete="off"> -->
                                <select class="form-control ui-autocomplete-input required" type="text" name="clientTagColor" id="clientTagColor">
                                    <<option value="">Select</option>
                                    <<option value="blue">Blue</option>
                                    <<option value="gray">Gray</option>
                                    <<option value="green">Green</option>
                                    <<option value="red">Red</option>
                                    <<option value="yellow">Yellow</option>
                                </select>
                                <label for="clientTagColor"></label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md text-center">
                            <input type="submit" id="create" value="Create" class="pull-right btn btn-primary" />
                            <!-- </div>
                            <div class="col col-md"> -->
                            <input type="submit" value="Cancel" onClick="hidePopUp()" class="pull-left btn btn-danger" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Ends Modal -->

<script>
// Get the modal
var modal = document.getElementById("myModal");
var btn = document.getElementById("myBtn");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
// When the user clicks the button, open the modal
btn.onclick = function() {
    $("#create").val("Create");
    $("#popuptitle").text("Add New");
    modal.style.display = "block";
}
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
    $("#create").val("Create");
    $("#popuptitle").text("Add New");
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

function hidePopUp(){
    modal.style.display = "none";
}

$(function() {
    $("#create").click(function() {
        $("#clientTagForm").validate({
            rules: {
                clientTagName: "required",
                clientTagColor: "required",
            },
            messages: {
                clientTagName: {
                required: "Please enter tag name"
            },
            clientTagColor: {
                required: "Please select tag color"
            },
            }
            
        });

        if($("#clientTagForm").validate()){
                console.log("test")
        }
    })
});
// Get Record
$(".editRec").click(function(){
    var id = $(this).attr('data-id');
    $.get("clienttags/edit/"+id, function(res) {
    if(res && res.data && res.data.length > 0 ){
        var data = res.data[0];
        modal.style.display = "block";
        $("#clientTagName").val(data.name);
        $("#clientTagColor").val(data.color);
        $("#record_id").val(data.id);
        $("#create").val("Update");
        $("#popuptitle").text("Update");
    } else {
        alert("data not found")
    }
    }).fail(function() {
    console.log("fail")
    }).done(function() {
    console.log("completed");
    })
})
</script>
@endsection