@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Referred By Source
@endsection

@section('content')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{ Form::open(array('onsubmit' => '','route' => 'referredbysources.create','files'=>true)) }}

<!-- Starts List Page -->
<div class="content-wrapper container-xxl p-0">
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            <div class="">
                <div class="table-bar2">
                </div>
                    <div class="table-bar2">
                    <strong>Admin / Setting / Application / Referred By Source</strong>   
                    @include('admin.components.message')
                    <div id="tabsneW" style="border: 0px;">
                      <div class="btn-group btn-align-right-add-update">
                        <div class="btn btn-primary cursorPoint" id="myBtn">Add New</div>
                      </div>
                    </div>
                </div>
                <!-- <div class="btn-group pull-right">
                    <a href="{{URL::to('admin/rooms/create')}}" class="btn btn-info">
                        <i class="fa fa-add"></i>Add Fleet Type</a>
                </div> -->
                <div class="table-bar2">
                    <!-- <div class="btn-group pull-right">
                        <button class="btn btn-primary" id="myBtn">New Fleet Types</button>
                    </div> -->
                    <table id="example" class="table table-striped" style="width:100%">
                        <thead class="thead">
                            <tr>
                                <th>Name</th>
                                <th>Cost</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sources as $source)
                            <tr>
                                <td>{{ $source->name }}</td>
                                <td>{{ $source->cost }}</td>
                                <th><a href="#"><i class="fa fa-edit cursorPoint editRec" data-id="{{$source->id}}"></i></a><a href="referredbysources/delete/{{$source->id}}"><i class="fa fa-trash cursorPoint taskdelete" data-id="{{$source->id}}"></i></a></th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- Ends List Page -->

<!-- Starts Modal content -->
<div id="myModal" class="modal">
    <div class="modal-content" style="display:block">
        <!-- <div class="row"></div> -->
        <div>
            <strong id="popuptitle">Add New</strong>
            <span class="close pull-right">&times;</span>
        </div>
        <hr/>
        <div class="row">
            <form name="fleetForm" id="fleetForm" method="post" action="referredbysources/create">
                <input id='record_id' name="record_id" type="hidden" />
                <div class="container">
                    <div class="col">
                        <div class="row form-group" id="prospect_field_div">
                            <div class="col col-sm-4">
                                <label class="control-label" for="task_contact_person_id">Title</label>
                            <!-- </div>
                            <div class="col-form-label col-sm-4"> -->
                                <input class="form-control ui-autocomplete-input required" type="text" name="referredName" id="referredName" autocomplete="off">
                                <label for="referredName"></label>
                            </div>
                        </div>
                        <div class="row form-group" id="prospect_field_div">
                            <div class="col col-sm-4">
                                <label class="control-label" for="task_contact_person_id">Cost</label>
                            <!-- </div>
                            <div class="col-form-label col-sm-4"> -->
                                <input class="form-control ui-autocomplete-input required" type="text" name="referredCost" id="referredCost" autocomplete="off">
                                <label for="referredCost"></label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md text-center">
                            <input type="submit" id="create" value="Create" class="pull-right btn btn-primary" />
                            <!-- </div>
                            <div class="col col-md"> -->
                            <input type="button" value="Cancel" onClick="hidePopUp()" class="pull-left btn btn-danger" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Ends Modal -->

<script>
// Get the modal
var modal = document.getElementById("myModal");
var btn = document.getElementById("myBtn");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
// When the user clicks the button, open the modal
btn.onclick = function() {
    $("#create").val("Create");
    $("#popuptitle").text("Add New");
    modal.style.display = "block";
}
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
    $("#create").val("Create");
    $("#popuptitle").text("Add New");
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

function hidePopUp(){
    modal.style.display = "none";
}

$(function() {
// function FleetValidate (){
    $("#create").click(function() {
    console.log("tester");
    // return false;
    console.log("tester")
      $("#fleetForm").validate({
        rules: {
            prospectsName: "required",
            referredCost: "required",
        },
        messages: {
            prospectsName: {
            required: "Please enter name"
          },
          referredCost: {
            required: "Please enter cost"
          },
        }
        
      });

      if($("#fleetForm").validate()){
            console.log("test")
      }
    })
    })
// }
// Get Record
$(".editRec").click(function(){
    var id = $(this).attr('data-id');
    $.get("referredbysources/edit/"+id, function(res) {
    if(res && res.data && res.data.length > 0 ){
        var data = res.data[0];
        modal.style.display = "block";
        $("#referredName").val(data.name);
        $("#referredCost").val(data.cost);
        $("#record_id").val(data.id);
        $("#create").val("Update");
        $("#popuptitle").text("Update");
    } else {
        alert("data not found")
    }
    }).fail(function() {
    console.log("fail")
    }).done(function() {
    console.log("completed");
    })
})
</script>
@endsection