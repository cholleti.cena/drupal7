@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Estimate Range
@endsection
@section('module')
Estimate Range
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return CategoryValidate();','route' => 'distancefromtruck.store','files'=>true)) }}

<div class="card mb-4">
   <div class="card-body after-loading-icon">
     <div class="container-fluid">
      	<div class="col-md-6">
        
				{{ Form::ahText('distance_from_truck','Distance From Truck :','',array())  }}				
				{{ Form::ahNumber('room','Room :','',array()) }}
				{{ Form::ahNumber('o_s_items','O/S Items :','',array()) }}
				{{ Form::ahNumber('inventory','Inventory :','',array()) }}
				{{ Form::ahNumber('boxes','Boxes :','',array()) }}
		    {{ Form::ahSelect('is_active','Status :','1',array('1' => 'Active', '0' => 'Inactive')) }}
	    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('distancefromtruck.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	</div>
  </div>
</div>
@endsection