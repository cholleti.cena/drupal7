<div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="/dashboard">{{env('APP_NAME')}}</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <h3 style="color: white;padding-top: 9px;">EA</h3>
                        </a>                                                                    
                    </li>
                    <li class="{{ Request::segment(2) === 'dashboard' ? 'active' : null }}"><a href="/admin/dashboard"><span class="fa fa-dashboard"></span> <span class="xn-text">Dashboard</span></a>     
                    </li>
                    <li class="{{ Request::segment(2) === 'user' ? 'active' : null }}"><a href="/admin/user"><span class="fa fa-users"></span> <span class="xn-text">Admin Users</span></a>
                    </li>
                    <li class="{{ Request::segment(2) === 'leads' ? 'active' : null }}"><a href="/admin/leads"><span class="fa fa-database"></span> <span class="xn-text">Leads</span></a>
                    </li>
                    <li class="{{ Request::segment(2) === 'schedules' ? 'active' : null }}"><a href="/admin/schedules"><span class="fa fa-database"></span> <span class="xn-text">Schedules</span></a>
                    </li>
                    <li class="{{ Request::segment(2) === 'categories' ? 'active' : null }}"><a href="/admin/categories"><span class="fa fa-database"></span> <span class="xn-text">Categories</span></a>
                    </li>
                    <li class="{{ Request::segment(2) === 'privileges' ? 'active' : null }}"><a href="/admin/privileges"><span class="fa fa-key"></span> <span class="xn-text">Privileges</span></a>
                    </li>                    
                     <li class="{{ Request::segment(2) === 'logs' ? 'active' : null }}"><a href="/admin/logs"><span class="fa fa-database"></span> <span class="xn-text">Logs</span></a>     
                    </li>
                </ul>
            </div>