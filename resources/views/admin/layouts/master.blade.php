<!DOCTYPE html>
<html lang="en">
    <head>        
        <title>@yield('title')</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" /> 
        <meta name="csrf-token" content="{{ csrf_token() }}" />         
        <link rel="icon" type="image/png" sizes="32x32" href="{{env('SITE_URL')}}/theme/faviconnew.png">      
        <link rel="stylesheet" type="text/css" id="theme" href="{{env('SITE_URL')}}/theme/css/theme-blue.css"/>
        <link href="{{env('SITE_URL')}}/theme/css/bootstrap-imageupload.css" rel="stylesheet">
        <link href="{{env('SITE_URL')}}/theme/css/jasny-bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <style>
    #save {
    visibility:hidden;
}
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url({{env('SITE_URL')}}/theme/img/ibt-process-42.gif) center no-repeat #fff;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script>
        $(window).load(function() { 
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
    <body class="preload">
    <div class="se-pre-con"></div>
    <?php   
  if(Session::get("role_id")!== null)
{
    
    ?>
        <div class="page-container">
            @include('admin.layouts.menu');
            <div class="page-content">
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    <!-- <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li> -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Logout"><span class="fa fa-sign-out"></span></a>                      
                    </li> 
                    <?php
                    if(!in_array(Session::get("role_id"),array(2,3)))
                    { ?>
                      <li class="{{ Request::segment(2) === 'profile' ? 'active' : null }} xn-icon-button pull-right">
                        <a href="/admin/profile" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Change Password"><span class="fa fa-user"></span></a>                        
                    </li>
                    <?php } ?>
                     <li class="xn-icon-button pull-right" style="margin-top: 17px;">
                        <h4 style="color: white;margin-left: -13px;"><?php echo Session::get("name"); ?></h4>                      
                    </li>  
                      <!-- <li class="xn-icon-button pull-right">
                        <a href="/profile"><span class="fa fa-user"></span></a>                        
                    </li>  -->                          
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a>@yield('module')</a></li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                    
                    @yield('content')
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="{{URL::to('admin/logout')}}" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="{{env('SITE_URL')}}/theme/audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="{{env('SITE_URL')}}/theme/audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                  
    <?php   
}
else
{ ?>
<script>
window.location.href="/admin";
</script>
<?php  }
  ?>               
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
         <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src="{{env('SITE_URL')}}/theme/js/plugins/icheck/icheck.min.js"></script>        
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
       <!--  <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/scrolltotop/scrolltopcontrol.js"></script> -->
        
        
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/tableexport/tableExport.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/tableexport/jquery.base64.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/tableexport/html2canvas.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/tableexport/jspdf/libs/sprintf.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/tableexport/jspdf/jspdf.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/tableexport/jspdf/libs/base64.js"></script>
          

        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/morris/raphael-min.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/morris/morris.min.js"></script>       
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/rickshaw/d3.v3.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/rickshaw/rickshaw.min.js"></script>
        <script type='text/javascript' src="{{env('SITE_URL')}}/theme/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script type='text/javascript' src="{{env('SITE_URL')}}/theme/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>                
        <script type='text/javascript' src="{{env('SITE_URL')}}/theme/js/plugins/bootstrap/bootstrap-datepicker.js"></script>                
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/owl/owl.carousel.min.js"></script>                 
        
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/moment.min.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- END THIS PAGE PLUGINS-->        
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/bootstrap/bootstrap-timepicker.min.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/bootstrap/bootstrap-colorpicker.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/bootstrap/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/bootstrap/bootstrap-timepicker.min.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/codemirror/codemirror.js"></script>        
        <script type='text/javascript' src="{{env('SITE_URL')}}/theme/js/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
        <script type='text/javascript' src="{{env('SITE_URL')}}/theme/js/plugins/codemirror/mode/xml/xml.js"></script>
        <script type='text/javascript' src="{{env('SITE_URL')}}/theme/js/plugins/codemirror/mode/javascript/javascript.js"></script>
        <script type='text/javascript' src="{{env('SITE_URL')}}/theme/js/plugins/codemirror/mode/css/css.js"></script>
        <script type='text/javascript' src="{{env('SITE_URL')}}/theme/js/plugins/codemirror/mode/clike/clike.js"></script>
        <script type='text/javascript' src="{{env('SITE_URL')}}/theme/js/plugins/codemirror/mode/php/php.js"></script>    

        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/summernote/summernote.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/highlight/jquery.highlight-4.js"></script>
        <!-- START TEMPLATE -->
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/settings.js"></script>
        
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins.js"></script>        
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/actions.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/demo_tables.js"></script>    
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/demo_dashboard.js"></script>
        <script src="{{env('SITE_URL')}}/theme/js/bootstrap-imageupload.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/jasny-bootstrap.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/smartwizard/jquery.smartWizard-2.0.min.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/jquery-validation/jquery.validate.js"></script>
        <script type='text/javascript' src="{{env('SITE_URL')}}/theme/js/plugins/icheck/icheck.min.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/rangeslider/jQAllRangeSliders-min.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/plugins/knob/jquery.knob.min.js"></script>
        <script type="text/javascript" src="{{env('SITE_URL')}}/theme/js/faq.js"></script>
        
       
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>