<div
      class="main-menu menu-fixed menu-light menu-accordion menu-shadow monrope_font"
      data-scroll-to-active="true"
    >
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item me-auto">
            <a class="navbar-brand" href="/admin/dashboard"
              ><span class="brand-logo">
                <img
                  src="{{env('SITE_URL')}}/theme/app-assets/images/logo/big_logo.png"
                  alt="big hill movers"
                />
                <!--<h2 class="brand-text">We Move Mountains for You</h2>	 -->
              </span>
            </a>
          </li>
        </ul>
      </div>
      <div class="shadow-bottom"></div>
      <div class="main-menu-content">
        <ul
          class="navigation navigation-main monrope_font"
          id="main-menu-navigation"
          data-menu="menu-navigation"
        >
          <li class="nav-item {{ Request::segment(2) === 'dashboard' ? 'active' : null }}">
            <a class="d-flex align-items-center" href="/admin/dashboard"
              ><i data-feather="home"></i
              ><span class="menu-title text-truncate monrope_font" data-i18n="Dashboards"
                >Dashboard</span></a>
          </li>

          <li class="nav-item {{ Request::segment(2) === 'tasks' ? 'active' : null }}" >
            <a class="d-flex align-items-center" href="#"
              ><i data-feather="check-square"></i
              ><span class="menu-title text-truncate" data-i18n="Invoice"
                >Tasks</span
              ></a
            >
            <ul class="menu-content">
              <li>
                <a class="d-flex align-items-center" href="/admin/tasks"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="List"
                    >All</span
                  ></a
                >
              </li>
              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="Preview"
                    >My Tasks</span
                  ></a
                >
              </li>
              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="Edit"
                    >Tasks for Others</span
                  ></a
                >
              </li>
            </ul>
          </li>
          <li class="nav-item {{ Request::segment(2) === 'leads' ? 'active' : null }}">
            <a class="d-flex align-items-center" href="/admin/leads"
              ><i class="fa fa-align-left"></i
              ><span class="menu-title text-truncate" data-i18n="Email"
                >Leads</span
              ></a
            >
          </li>
          <li class="nav-item {{ Request::segment(2) === 'user' ? 'active' : null }}">
            <a class="d-flex align-items-center" href="/admin/user"
              ><i class="fa fa-users"></i
              ><span class="menu-title text-truncate" data-i18n="Email"
                >Admin Users</span
              ></a
            >
          </li>
          <li class="nav-item {{ Request::segment(2) === 'privileges' ? 'active' : null }}">
            <a class="d-flex align-items-center" href="/admin/privileges">
              <!-- <i class="fa fa-exclamation" aria-hidden="true"></i> -->
              <i class="fa fa-universal-access" aria-hidden="true"></i>
              <span class="menu-title text-truncate" data-i18n="Todo"
                >Privileges</span
              ></a
            >
          </li>
          <li class="nav-item">
            <a class="d-flex align-items-center" href="#">
              <i class="fa fa-list" aria-hidden="true"></i
              ><span class="menu-title text-truncate" data-i18n="Email"
                >Follow Ups</span
              ></a
            >
          </li>
          <li class="nav-item {{ Request::segment(2) === 'schedules' ? 'active' : null }}">
            <a class="d-flex align-items-center" href="#"
              ><i data-feather="calendar"></i
              ><span class="menu-title text-truncate" data-i18n="Invoice"
                >Schedules</span
              ></a
            >
            <ul class="menu-content">
              <li>
                <a class="d-flex align-items-center" href="/admin/schedules"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="List"
                    >Booked Jobs</span
                  ></a
                >
              </li>
              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="Preview"
                    >Pending Jobs</span
                  ></a
                >
              </li>
              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="Edit"
                    >Office</span
                  ></a
                >
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="d-flex align-items-center" href="#">
              <!-- <i class="fa fa-user-tie-hair" aria-hidden="true"></i> -->
              <i class="fa fa-circle-user"></i>
              <span class="menu-title text-truncate" data-i18n="Invoice"
                >Clients</span
              ></a
            >
            <ul class="menu-content">
              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >Prospects</span
                  ></a
                >
                <ul class="menu-content">
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Active</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span
                        class="menu-item text-truncate"
                        data-i18n="Security"
                        >In-Active</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span
                        class="menu-item text-truncate"
                        data-i18n="Billing &amp; Plans"
                        >All</span
                      ></a
                    >
                  </li>
                </ul>
              </li>
              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >Customers</span
                  ></a
                >
                <ul class="menu-content">
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Active</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span
                        class="menu-item text-truncate"
                        data-i18n="Security"
                        >In-Active</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span
                        class="menu-item text-truncate"
                        data-i18n="Billing &amp; Plans"
                        >All</span
                      ></a
                    >
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="d-flex align-items-center" href="#">
            <i class="fa fa-boxes-stacked"></i>
              <span class="menu-title text-truncate" data-i18n="Chat"
                >Storage</span
              ></a
            >
          </li>
          <li class="nav-item">
            <a class="d-flex align-items-center" href="#">
              <!-- <i class="fa fa-exclamation" aria-hidden="true"></i> -->
              <i class="fa fa-file-invoice"></i>
              <span class="menu-title text-truncate" data-i18n="Todo"
                >Claims</span
              ></a
            >
          </li>

          <li class="nav-item">
            <a class="d-flex align-items-center" href="#"
              ><i class="fa fa-bar-chart" aria-hidden="true"></i
              ><span class="menu-title text-truncate" data-i18n="Invoice"
                >Reports</span
              ></a
            >
            <ul class="menu-content">
              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >Sales</span
                  ></a
                >
                <ul class="menu-content">
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Sales By Referral Source</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span
                        class="menu-item text-truncate"
                        data-i18n="Security"
                        >Sales By Sales Rep</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span
                        class="menu-item text-truncate"
                        data-i18n="Billing &amp; Plans"
                        >Sales By Customer</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Sales by Line Item</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span
                        class="menu-item text-truncate"
                        data-i18n="Security"
                        >Payments Received</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span
                        class="menu-item text-truncate"
                        data-i18n="Billing &amp; Plans"
                        >Sales By Booking Date</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span
                        class="menu-item text-truncate"
                        data-i18n="Billing &amp; Plans"
                        >Sales By Type Of Service</span
                      ></a
                    >
                  </li>
                </ul>
              </li>
              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >Estimates</span
                  ></a
                >
                <ul class="menu-content">
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Estimates By Referral Source</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span
                        class="menu-item text-truncate"
                        data-i18n="Security"
                        >Estimates By Customer</span
                      ></a
                    >
                  </li>
                </ul>
              </li>
              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >Payroll</span
                  ></a
                >
                <ul class="menu-content">
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Labor Payroll</span
                      ></a
                    >
                  </li>
                </ul>
              </li>

              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >Emails</span
                  ></a
                >
                <ul class="menu-content">
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Sent Emails</span
                      ></a
                    >
                  </li>
                </ul>
              </li>

              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >Booked</span
                  ></a
                >
                <ul class="menu-content">
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Booked By Sales Rep</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span
                        class="menu-item text-truncate"
                        data-i18n="Security"
                        >Booked By Move Date</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span
                        class="menu-item text-truncate"
                        data-i18n="Billing &amp; Plans"
                        >Booked By Customer</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Booked By Referral Source</span
                      ></a
                    >
                  </li>
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span
                        class="menu-item text-truncate"
                        data-i18n="Security"
                        >Booked by Type of Service</span
                      ></a
                    >
                  </li>
                </ul>
              </li>

              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="Security"
                    >Cancellations By Sales Rep</span
                  ></a
                >
              </li>

              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i class="fa fa-address-book"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >Leads</span
                  ></a
                >
                <ul class="menu-content">
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Leads By Referral Source</span
                      ></a
                    >
                  </li>

                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Lead ROI</span
                      ></a
                    >
                  </li>
                </ul>
              </li>

              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i class="fa fa-building-o"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >Storage</span
                  ></a
                >
                <ul class="menu-content">
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Storage Summary</span
                      ></a
                    >
                  </li>

                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Lead ROI</span
                      ></a
                    >
                  </li>
                </ul>
              </li>

              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="Security"
                    >Reviews By Customer</span
                  ></a
                >
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a class="d-flex align-items-center" href="#"
              ><i class="fa fa-usd" aria-hidden="true"></i
              ><span class="menu-title text-truncate" data-i18n="Invoice"
                >Accounting</span
              ></a
            >
            <ul class="menu-content">
              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >Close Out jobs</span
                  ></a
                >
              </li>
              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >Payroll</span
                  ></a
                >
                <ul class="menu-content">
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Manual Entry</span
                      ></a
                    >
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="d-flex align-items-center" href="#"
              ><i class="fa fa-cog" aria-hidden="true"></i
              ><span class="menu-title text-truncate" data-i18n="Invoice"
                >Manage</span
              ></a
            >
            <ul class="menu-content">
              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >Fleets</span
                  ></a
                >
                <ul class="menu-content">
                  <li class="{{ Request::segment(2) === 'fleets' ? 'active' : null }}">
                    <a class="d-flex align-items-center" href="/admin/fleets"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Fleet</span
                      ></a
                    >
                  </li>

                  <li class="{{ Request::segment(2) === 'fleetscalender' ? 'active' : null }}">
                    <a class="d-flex align-items-center" href="/admin/fleetscalender"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Fleet Calendar</span
                      ></a
                    >
                  </li>
                </ul>
              </li>

              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >HR</span
                  ></a>
                <ul class="menu-content">
                  <li class="{{ Request::segment(2) === 'employees' ? 'active' : null }}">
                    <a class="d-flex align-items-center" href="/admin/employees">
                      <span class="menu-item text-truncate" data-i18n="Account">Employees</span>
                    </a>
                  </li>
                </ul>
              </li>

              <li>
                <a class="d-flex align-items-center" href="#"
                  ><i data-feather="circle"></i
                  ><span class="menu-item text-truncate" data-i18n="View"
                    >Admin</span
                  ></a
                >
                <ul class="menu-content">
                  <li>
                    <a class="d-flex align-items-center" href="#"
                      ><span class="menu-item text-truncate" data-i18n="Account"
                        >Setting</span
                      ></a
                    >

                    <ul class="menu-content">
                      <li>
                        <a class="d-flex align-items-center" href="#"
                          ><span
                            class="menu-item text-truncate"
                            data-i18n="Account"
                            >Corporate</span
                          ></a
                        >
                        <ul class="menu-content">
                          <li>
                            <a class="d-flex align-items-center" href="#"
                              ><span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Identity</span
                              ></a
                            >
                          </li>
                        </ul>
                      </li>
                      <li>
                        <a class="d-flex align-items-center" href="#"
                          ><span
                            class="menu-item text-truncate"
                            data-i18n="Account"
                            >Automated Emails</span
                          ></a
                        >
                      </li>

                      <li>
                        <a class="d-flex align-items-center" href="#"
                          ><span
                            class="menu-item text-truncate"
                            data-i18n="Account"
                            >Application</span
                          ></a
                        >
                        <ul class="menu-content">
                          <li class="{{ Request::segment(2) === 'fleettypes' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/fleettypes">
                              <span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Fleet</span
                              ></a
                            >
                          </li>
                          <li class="{{ Request::segment(2) === 'prospectintereststatuses' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/prospectintereststatuses">
                            <span
                            class="menu-item text-truncate"
                            data-i18n="Account"
                            >Prospect Interest Statuses</span
                            ></a
                            >
                          </li>
                          <li class="{{ Request::segment(2) === 'referredbysources' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/referredbysources">
                              <span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Referred by Sources</span
                                ></a
                                >
                              </li>
                          <li class="{{ Request::segment(2) === 'expertisetypes' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/expertisetypes"
                              ><span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Expertise types</span
                              ></a
                            >
                          </li>
                          <li class="{{ Request::segment(2) === 'clienttags' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/clienttags">
                              <span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Client Tags</span
                              ></a
                            >
                          </li>
                        </ul>
                      </li>

                      <li>
                        <a class="d-flex align-items-center" href="#"
                          ><span
                            class="menu-item text-truncate"
                            data-i18n="Account"
                            >Estimates</span
                          ></a
                        >
                        <ul class="menu-content">
                          <li class="{{ Request::segment(2) === 'categories' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/categories"
                              ><span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Inventory Categories</span
                              ></a
                            >
                          </li>
                          <li class="{{ Request::segment(2) === 'inventoryitems' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/inventoryitems"
                              ><span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Inventories</span
                              ></a
                            >
                          </li>
                          <li class="{{ Request::segment(2) === 'rooms' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/rooms"
                              ><span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Rooms</span
                              ></a
                            >
                          </li>
                          <li class="{{ Request::segment(2) === 'packingmaterials' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/packingmaterials"
                              ><span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Packing Materials</span
                              ></a
                            >
                          </li>
                          <li>
                            <a class="d-flex align-items-center" href="#"
                              ><span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Moving Estimates</span
                              ></a
                            >
                            <ul class="menu-content">
                              <li class="{{ Request::segment(2) === 'traveltime' ? 'active' : null }}">
                                <a class="d-flex align-items-center" href="/admin/traveltime"
                                  ><span
                                    class="menu-item text-truncate"
                                    data-i18n="Account"
                                    >Travel Time</span
                                  ></a
                                >
                              </li>
                              <li class="{{ Request::segment(2) === 'fuelcharges' ? 'active' : null }}">
                                <a class="d-flex align-items-center" href="/admin/fuelcharges"
                                  ><span
                                    class="menu-item text-truncate"
                                    data-i18n="Account"
                                    >Fuel Charges</span
                                  ></a
                                >
                              </li>
                              <li class="{{ Request::segment(2) === 'squarefootage' ? 'active' : null }}">
                                <a class="d-flex align-items-center" href="/admin/squarefootage"
                                  ><span
                                    class="menu-item text-truncate"
                                    data-i18n="Account"
                                    >Square Footages</span
                                  ></a
                                >
                              </li>
                              <li class="{{ Request::segment(2) === 'floors' ? 'active' : null }}">
                                <a class="d-flex align-items-center" href="/admin/floors"
                                  ><span
                                    class="menu-item text-truncate"
                                    data-i18n="Account"
                                    >Floors</span
                                  ></a
                                >
                              </li>
                              <li class="{{ Request::segment(2) === 'estimaterange' ? 'active' : null }}">
                                <a class="d-flex align-items-center" href="/admin/estimaterange"
                                  ><span
                                    class="menu-item text-truncate"
                                    data-i18n="Account"
                                    >Estimate Range</span
                                  ></a
                                >
                              </li>
                              <li class="{{ Request::segment(2) === 'distancefromtruck' ? 'active' : null }}">
                                <a class="d-flex align-items-center" href="/admin/distancefromtruck"
                                  ><span
                                    class="menu-item text-truncate"
                                    data-i18n="Account"
                                    >Distance From Truck</span
                                  ></a
                                >
                              </li>
                              <li class="{{ Request::segment(2) === 'manhourcalculation' ? 'active' : null }}">
                                <a class="d-flex align-items-center" href="/admin/manhourcalculation"
                                  ><span
                                    class="menu-item text-truncate"
                                    data-i18n="Account"
                                    >Man Hour Calculations</span
                                  ></a
                                >
                              </li>
                            </ul>
                          </li>

                          <li class="{{ Request::segment(2) === 'additionalcharges' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/additionalcharges"
                              ><span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Additional Charges</span
                              ></a
                            >
                          </li>
                          <li class="{{ Request::segment(2) === 'followupautomation' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/followupautomation"
                              ><span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Follow-up Automation</span
                              ></a
                            >
                          </li>
                          <li>
                            <a class="d-flex align-items-center" href="/admin/estimatesettings"
                              ><span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Estimate Settings</span
                              ></a
                            >
                          </li>
                          <li class="{{ Request::segment(2) === 'leadsfollowup' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/leadsfollowup"
                              ><span
                                class="menu-item text-truncate"
                                data-i18n="Account"
                                >Leads Follow-up Automation</span
                              ></a
                            >
                          </li>
                        </ul>
                      </li>

                      <li class="{{ Request::segment(2) === 'bolinvoice' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/bolinvoice"
                          ><span
                            class="menu-item text-truncate"
                            data-i18n="Account"
                            >BOL / Invoice Text</span
                          ></a
                        >
                      </li>
                      <li class="{{ Request::segment(2) === 'facilities' ? 'active' : null }}">
                            <a class="d-flex align-items-center" href="/admin/facilities"
                          ><span
                            class="menu-item text-truncate"
                            data-i18n="Account"
                            >Storage</span
                          ></a
                        >
                      </li>
                      <li class="{{ Request::segment(2) === 'payrolls' ? 'active' : null }}">
                        <a class="d-flex align-items-center" href="/admin/payrolls">
                        <span class="menu-item text-truncate" data-i18n="Account">Payroll</span></a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>