<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta
      name="viewport"
      content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui"
    />

    <meta name="description" content="{{env('APP_NAME')}}" />
    <meta name="keywords" content="{{env('APP_NAME')}}" />
    <meta name="author" content="{{env('APP_NAME')}}" />
    <title>@yield('title')</title>
    <link rel="icon" type="image/png" sizes="32x32" href="{{env('SITE_URL')}}/theme/app-assets/images/ico/favicon.ico">      
    <link rel="apple-touch-icon" href="{{env('SITE_URL')}}/theme/app-assets/images/logo/big_logo.png" />

    <!-- <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
      rel="stylesheet"
    /> -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Manrope:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">
    <?php $colorOrange = '#cd7139'; ?>
    <?php $colorBlue = '#277cbb'; ?>
    <!-- BEGIN: Vendor CSS-->
    <link
      rel="stylesheet"
      type="text/css"
      href="{{env('SITE_URL')}}/theme/app-assets/vendors/css/vendors.min.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="{{env('SITE_URL')}}/theme/app-assets/vendors/css/charts/apexcharts.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="{{env('SITE_URL')}}/app-assets/vendors/css/extensions/toastr.min.css"
    />
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link
      rel="stylesheet"
      type="text/css"
      href="{{env('SITE_URL')}}/theme/app-assets/css/bootstrap.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="{{env('SITE_URL')}}/theme/app-assets/css/bootstrap-extended.css"
    />
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/app-assets/css/colors.css" />
    <link
      rel="stylesheet"
      type="text/css"
      href="{{env('SITE_URL')}}/theme/app-assets/css/components.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="{{env('SITE_URL')}}/theme/app-assets/css/themes/dark-layout.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="{{env('SITE_URL')}}/theme/app-assets/css/themes/bordered-layout.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="{{env('SITE_URL')}}/theme/app-assets/css/themes/semi-dark-layout.css"
    />

    <!-- BEGIN: Page CSS-->
    <link
      rel="stylesheet"
      type="text/css"
      href="{{env('SITE_URL')}}/theme/app-assets/css/core/menu/menu-types/vertical-menu.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="{{env('SITE_URL')}}/theme/app-assets/css/pages/dashboard-ecommerce.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="{{env('SITE_URL')}}/theme/app-assets/css/plugins/charts/chart-apex.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="{{env('SITE_URL')}}/theme/app-assets/css/plugins/extensions/ext-component-toastr.css"
    />
    <link
      rel="stylesheet"
      type="text/css"
      href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css"
    />
    
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/assets/css/style.css" />
    <link rel="stylesheet" href="{{env('SITE_URL')}}/theme/app-assets/css/styles.css" />
    <!-- END: Custom CSS-->
    <!-- Nav Tabs bar css -->
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/app-assets/css/jquery-ui.css"/>
    <!-- End Nav bar css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"/>
     
   <style>
    * {
      margin: 0;
      padding: 0;
    }

    a {
      /* color: #0d6efd; */
      text-decoration: none;
      /* background-color:#277cbb !important; */
      /* color: #277cbb; */
    }
    .navTabs.ui-tabs-active a {
      background-color:#277cbb !important;
    }
    .navTabs.ui-tabs-active{
      border: 0px solid #fff !important;

    }
    
    .row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -0.75rem;
    margin-left: -0.75rem;
}

 

  .mar, .mar-left{
    margin-left: 50px;
  }
    .nav-bar {
      background-color: white;

      padding: 10px 10px 0 10px;
    }

    .nav-tabs.nav-bar .nav-link::after {
      content: '';
      position: absolute;
      bottom: 0;
      left: 0;
      width: 100%;
      height: 3px;
      background: #fff !important;
    }

    .nav.nav-bar {
      border-radius: 0.25rem 0.25rem 0 0;
    }

    .nav-tabs.nav-bar {
      border-bottom: 1px solid #ddd;
    }

    .nav-tabs .nav-link.active,
    .nav-tabs .nav-item.show .nav-link {
      color: #277cbb;
      background-color: transparent;
      border-color: #dae1e7 #dae1e7 transparent;
    }

    .nav-tabs .nav-link.active {
      position: relative;
      color: #277cbb;
    }

    .nav-tabs.nav-bar>li>.nav-link.active {
      cursor: default;
      background-color: #fff;
      border: 1px solid #ddd;
      border-bottom-color: transparent;

      margin-bottom: -1px;
    }

    .table-bar {
      background-color: white;
      padding: 10px;
      align-items: center;
      border-radius: 0 0 0.25rem 0.25rem;

    }

    .table-bar2 {
      background-color: white;
      margin: 15px;
      margin-top: -20px;
      padding: 10px;
    }

    .table-bar .thead {
      background-color: #acb6bf;
    }

    .navbar.custnav {
      padding: 0;
    }

    

    .nav-tabs {
      border-bottom: 1px solid #ddd;
    }

    .progress{
      height: 25px;
      display: flex;
      font-size: 1rem;
      background-color: rgba(24,28,33,0.03);
      border-radius: 10rem;
      margin-bottom: 10px;
      
    }

    .card {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(24,28,33,0.06);
    border-radius: 1.25rem;
    /*padding: 25px;*/
}

li.active .step, li.active.complete .step {
    color: #26B4FF;
    font-weight: 400;
    padding: 7px 13px;
    font-size: 19px;
    border-radius: 50%;
    border: 2px solid #26B4FF;
}


a {
    color: #1e70cd;
    text-decoration: none;
    background-color: transparent;
}

li {
    text-align: -webkit-match-parent;
}

.nav {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    padding-left: 0;
    margin-bottom: 0;
    list-style: none;
}

ul {
    list-style-type: disc;
}

li .step {
    color: darkgrey;
    display: inline;
    font-size: 19px;
    font-weight: 400;
    line-height: 12px;
    padding: 7px 13px;
    border: 2px solid rgba(24,28,33,0.06);
    border-radius: 50%;
    line-height: normal;
    position: absolute;
    text-align: center;
    z-index: 2;
    -webkit-transition: all .1s linear 0s;
    transition: all .1s linear 0s;
    margin-top: 0px;
}

.container-fluid {
    width: 100%;
    padding-right: 0.75rem;
    padding-left: 0.75rem;
    margin-right: auto;
    margin-left: auto;
}
.top-bar-common {
    background-color: #2a3036;
    height: 35px;
    width: 100%;
    color: #ffffff;
    margin-bottom: 10px;
}

.dot {
    color: red;
    display: contents;
}
/* .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col, .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm, .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md, .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg, .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl, .col-xl-auto {
    position: relative;
    width: 100%;
    padding-right: 0.75rem;
    padding-left: 0.75rem;
} */

.col-auto{
  margin-bottom: 1rem;
}
.card-body {
    -webkit-box-flex: 1;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.5rem;
}
*, *::before, *::after {
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}

.col-lg-6 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
  }

   
  @media (min-width: 992px){
  .col-lg-6 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}}

@media (min-width: 576px){
     .col-sm-6 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 16%;
}
}
.col {
    -ms-flex-preferred-size: 0;
    flex-basis: 0;
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    max-width: 100%;
}

label {
    cursor: default;
}





.form-label, .col-form-label {
    margin-bottom: calc(0.438rem - 2px);
    font-weight: 500;
    font-size: 0.83125rem;
    padding-bottom: 0;
}

.col-form-label {
    padding-top: calc(0.438rem + 1px);
    padding-bottom: calc(0.438rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.54;
    display: inline-block;
    padding-top: 15px;
}


label {
    display: inline-block;
    margin-bottom: 0.5rem;
}
.form-control {
    display: block;
    width: 100%;
    height: calc(1.54em + 0.876rem + 2px);
    padding: 0.438rem 0.875rem;
    font-size: 0.894rem;
    font-weight: 400;
    line-height: 1.54;
    color: #4E5155;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid rgba(24,28,33,0.1);
    border-radius: 0.25rem;
    -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
    transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
}

input, button, select, optgroup, textarea {
    margin: 0;
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}

.nav-tabs-top {
    /* -webkit-box-orient: vertical; */
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
}

 
.bg-primary {
    --bs-bg-opacity: 1;
    background-color: #00a6ff !important;
}

.card i.fa {
  color: <?php echo $colorOrange; ?>;
}
.orangeColor{
  color: <?php echo $colorOrange; ?>;
}
.card div.counts {
  font-weight: 800;
  font-size:22px;
}
.card div.details-data{
  height: 70px;
}
.card div.card-description {
  font-weight: 700;
  height:40px;
}
.manrope_font {
  font-family: "manrope" !important
}
.topMenuNavBar {
  box-shadow: 0px 2px #acb6bf !important;
}
.text-truncate{
  font-weight: 500;
}
li.nav-item:hover, li.nav-item span:hover{ color: <?php echo $colorBlue; ?>}
li.nav-item.active span:hover{ color: #fff; }
.cursorPoint{
  cursor: pointer;
}
.btn-align-right-add-update{
  float: right;
  margin-bottom: 15px;
}
/* Starts Modal */
/* The Modal (background) */
.modal {
    display: none;
    /* Hidden by default */
    position: fixed;
    /* Stay in place */
    z-index: 21;
    /* Sit on top */
    padding-top: 100px;
    /* Location of the box */
    left: 0;
    top: 0;
    width: 100%;
    /* Full width */
    height: 100%;
    /* Full height */
    overflow: auto;
    /* Enable scroll if needed */
    background-color: rgb(0, 0, 0);
    /* Fallback color */
    background-color: rgba(0, 0, 0, 0.4);
    /* Black w/ opacity */
  }
  /* Modal Content */
  
  .modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 30%;
  }
  /* The Close Button */
  
  .close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
  }
  
  .close:hover,
  .close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
  }
  .ui-widget-header{
    background: #fff !important;
    border-top: 0px !important;
    border-left: 0px !important;
    border-right: 0px !important;
    border-bottom: 0px !important;
  }
  
  .ui-tabs-panel{
    padding: 0px !important;
  }
  .tableGrid{
    margin: 0px !important;
  }
  .tableGrid div div label{
    display: inline-flex;
  }
  .statuswise tr{
    border-bottom: 1px solid #ccc;
  }
/* Ends Modal */
</style>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->

  <body
    class="vertical-layout vertical-menu-modern navbar-floating footer-static manrope_font"
    data-open="click"
    data-menu="vertical-menu-modern"
    data-col=""
  >
    <!-- BEGIN: Header-->
    <nav
      class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow container-xxl topMenuNavBar"
    >
      <div class="navbar-container d-flex content">
        <div class="bookmark-wrapper d-flex align-items-center">
          <ul class="nav navbar-nav d-xl-none">
            <li class="nav-item">
              <a class="nav-link menu-toggle" href="#"
                ><i class="ficon" data-feather="menu"></i
              ></a>
            </li>
          </ul>
          <ul class="nav navbar-nav bookmark-icons">
            <li class="nav-item d-none d-lg-block">
              <a
                class="nav-link"
                href="#"
                data-bs-toggle="tooltip"
                data-bs-placement="bottom"
                title="Daily Schedule"
                ><i class="fa fa-truck fa-lg" aria-hidden="true"></i
              ></a>
            </li>
            <li class="nav-item d-none d-lg-block">
              <a
                class="nav-link"
                href="#"
                data-bs-toggle="tooltip"
                data-bs-placement="bottom"
                title="Jobs Calendar"
                ><i class="fa fa-calendar fa-lg" aria-hidden="true"></i
              ></a>
            </li>
            <li class="nav-item d-none d-lg-block">
              <a
                class="nav-link"
                href="#"
                data-bs-toggle="tooltip"
                data-bs-placement="bottom"
                title="New Task"
                ><i class="fa fa-check-square-o fa-lg" aria-hidden="true"></i
              ></a>
            </li>
            <li class="nav-item d-none d-lg-block">
              <a
                class="nav-link"
                href="#"
                data-bs-toggle="tooltip"
                data-bs-placement="bottom"
                title="New Event"
                ><i class="fa fa-calendar-o fa-lg" aria-hidden="true"></i
              ></a>
            </li>

            <li class="nav-item d-none d-lg-block">
              <a
                class="nav-link"
                href="#"
                data-bs-toggle="tooltip"
                data-bs-placement="bottom"
                title="New Prospect"
                ><i class="fa fa-user-plus fa-lg" aria-hidden="true"></i
              ></a>
            </li>
          </ul>
          <ul class="nav navbar-nav">
            <li class="nav-item d-none d-lg-block">
              <a class="nav-link bookmark-star"
                ><i class="fa fa-search" data-feather="search"></i
              ></a>
              <div class="bookmark-input search-input">
                <div class="bookmark-input-icon">
                  <i data-feather="search"></i>
                </div>
                <input
                  class="form-control input"
                  type="text"
                  placeholder="Search...."
                  tabindex="0"
                  data-search="search"
                />
                <ul class="search-list search-list-bookmark"></ul>
              </div>
            </li>
          </ul>
        </div>
        <ul class="nav navbar-nav align-items-center ms-auto">
          
          <!-- <li class="nav-item dropdown dropdown-notification me-25">
            <a class="nav-link" href="#" data-bs-toggle="dropdown"
              ><i class="fa fa-bell-o fa-lg" aria-hidden="true"></i
              ><span class="badge rounded-pill bg-danger badge-up">5</span></a
            >
            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
              <li class="dropdown-menu-header">
                <div class="dropdown-header d-flex">
                  <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                  <div class="badge rounded-pill badge-light-primary">
                    6 New
                  </div>
                </div>
              </li>
              <li class="scrollable-container media-list">
                <a class="d-flex" href="#">
                  <div class="list-item d-flex align-items-start">
                    <div class="me-1">
                      <div class="avatar">
                        <img
                          src="{{env('SITE_URL')}}/theme/app-assets/images/portrait/small/avatar-s-15.jpg"
                          alt="avatar"
                          width="32"
                          height="32"
                        />
                      </div>
                    </div>
                    <div class="list-item-body flex-grow-1">
                      <p class="media-heading">
                        <span class="fw-bolder">Congratulation Sam 🎉</span
                        >winner!
                      </p>
                      <small class="notification-text">
                        Won the monthly best seller badge.</small
                      >
                    </div>
                  </div> </a
                ><a class="d-flex" href="#">
                  <div class="list-item d-flex align-items-start">
                    <div class="me-1">
                      <div class="avatar">
                        <img
                          src="{{env('SITE_URL')}}/theme/app-assets/images/portrait/small/avatar-s-3.jpg"
                          alt="avatar"
                          width="32"
                          height="32"
                        />
                      </div>
                    </div>
                    <div class="list-item-body flex-grow-1">
                      <p class="media-heading">
                        <span class="fw-bolder">New message</span>&nbsp;received
                      </p>
                      <small class="notification-text">
                        You have 10 unread messages</small
                      >
                    </div>
                  </div> </a
                ><a class="d-flex" href="#">
                  <div class="list-item d-flex align-items-start">
                    <div class="me-1">
                      <div class="avatar bg-light-danger">
                        <div class="avatar-content">MD</div>
                      </div>
                    </div>
                    <div class="list-item-body flex-grow-1">
                      <p class="media-heading">
                        <span class="fw-bolder">Revised Order 👋</span
                        >&nbsp;checkout
                      </p>
                      <small class="notification-text">
                        MD Inc. order updated</small
                      >
                    </div>
                  </div>
                </a>
                <div class="list-item d-flex align-items-center">
                  <h6 class="fw-bolder me-auto mb-0">System Notifications</h6>
                  <div class="form-check form-check-primary form-switch">
                    <input
                      class="form-check-input"
                      id="systemNotification"
                      type="checkbox"
                      checked=""
                    />
                    <label
                      class="form-check-label"
                      for="systemNotification"
                    ></label>
                  </div>
                </div>
                <a class="d-flex" href="#">
                  <div class="list-item d-flex align-items-start">
                    <div class="me-1">
                      <div class="avatar bg-light-danger">
                        <div class="avatar-content">
                          <i class="avatar-icon" data-feather="x"></i>
                        </div>
                      </div>
                    </div>
                    <div class="list-item-body flex-grow-1">
                      <p class="media-heading">
                        <span class="fw-bolder">Server down</span
                        >&nbsp;registered
                      </p>
                      <small class="notification-text">
                        USA Server is down due to high CPU usage</small
                      >
                    </div>
                  </div> </a
                ><a class="d-flex" href="#">
                  <div class="list-item d-flex align-items-start">
                    <div class="me-1">
                      <div class="avatar bg-light-success">
                        <div class="avatar-content">
                          <i class="avatar-icon" data-feather="check"></i>
                        </div>
                      </div>
                    </div>
                    <div class="list-item-body flex-grow-1">
                      <p class="media-heading">
                        <span class="fw-bolder">Sales report</span
                        >&nbsp;generated
                      </p>
                      <small class="notification-text">
                        Last month sales report generated</small
                      >
                    </div>
                  </div> </a
                ><a class="d-flex" href="#">
                  <div class="list-item d-flex align-items-start">
                    <div class="me-1">
                      <div class="avatar bg-light-warning">
                        <div class="avatar-content">
                          <i
                            class="avatar-icon"
                            data-feather="alert-triangle"
                          ></i>
                        </div>
                      </div>
                    </div>
                    <div class="list-item-body flex-grow-1">
                      <p class="media-heading">
                        <span class="fw-bolder">High memory</span>&nbsp;usage
                      </p>
                      <small class="notification-text">
                        BLR Server using high memory</small
                      >
                    </div>
                  </div>
                </a>
              </li>
              <li class="dropdown-menu-footer">
                <a class="btn btn-primary w-100" href="#"
                  >Read all notifications</a
                >
              </li>
            </ul>
          </li> -->

          <!-- <li class="nav-item dropdown dropdown-notification me-25">
            <a class="nav-link" href="#" data-bs-toggle="dropdown"
              ><i class="fa fa-exclamation-circle fa-lg" aria-hidden="true"></i
              ><span class="badge rounded-pill bg-danger badge-up">5</span></a
            >
            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
              <li class="dropdown-menu-header">
                <div class="dropdown-header d-flex">
                  <h4 class="notification-title mb-0 me-auto">Notifications</h4>
                  <div class="badge rounded-pill badge-light-primary">
                    6 New
                  </div>
                </div>
              </li>
              <li class="scrollable-container media-list">
                <a class="d-flex" href="#">
                  <div class="list-item d-flex align-items-start">
                    <div class="me-1">
                      <div class="avatar">
                        <img
                          src="{{env('SITE_URL')}}/theme/app-assets/images/portrait/small/avatar-s-15.jpg"
                          alt="avatar"
                          width="32"
                          height="32"
                        />
                      </div>
                    </div>
                    <div class="list-item-body flex-grow-1">
                      <p class="media-heading">
                        <span class="fw-bolder">Congratulation Sam 🎉</span
                        >winner!
                      </p>
                      <small class="notification-text">
                        Won the monthly best seller badge.</small
                      >
                    </div>
                  </div> </a
                ><a class="d-flex" href="#">
                  <div class="list-item d-flex align-items-start">
                    <div class="me-1">
                      <div class="avatar">
                        <img
                          src="{{env('SITE_URL')}}/theme/app-assets/images/portrait/small/avatar-s-3.jpg"
                          alt="avatar"
                          width="32"
                          height="32"
                        />
                      </div>
                    </div>
                    <div class="list-item-body flex-grow-1">
                      <p class="media-heading">
                        <span class="fw-bolder">New message</span>&nbsp;received
                      </p>
                      <small class="notification-text">
                        You have 10 unread messages</small
                      >
                    </div>
                  </div> </a
                ><a class="d-flex" href="#">
                  <div class="list-item d-flex align-items-start">
                    <div class="me-1">
                      <div class="avatar bg-light-danger">
                        <div class="avatar-content">MD</div>
                      </div>
                    </div>
                    <div class="list-item-body flex-grow-1">
                      <p class="media-heading">
                        <span class="fw-bolder">Revised Order 👋</span
                        >&nbsp;checkout
                      </p>
                      <small class="notification-text">
                        MD Inc. order updated</small
                      >
                    </div>
                  </div>
                </a>
                <div class="list-item d-flex align-items-center">
                  <h6 class="fw-bolder me-auto mb-0">System Notifications</h6>
                  <div class="form-check form-check-primary form-switch">
                    <input
                      class="form-check-input"
                      id="systemNotification"
                      type="checkbox"
                      checked=""
                    />
                    <label
                      class="form-check-label"
                      for="systemNotification"
                    ></label>
                  </div>
                </div>
                <a class="d-flex" href="#">
                  <div class="list-item d-flex align-items-start">
                    <div class="me-1">
                      <div class="avatar bg-light-danger">
                        <div class="avatar-content">
                          <i class="avatar-icon" data-feather="x"></i>
                        </div>
                      </div>
                    </div>
                    <div class="list-item-body flex-grow-1">
                      <p class="media-heading">
                        <span class="fw-bolder">Server down</span
                        >&nbsp;registered
                      </p>
                      <small class="notification-text">
                        USA Server is down due to high CPU usage</small
                      >
                    </div>
                  </div> </a
                ><a class="d-flex" href="#">
                  <div class="list-item d-flex align-items-start">
                    <div class="me-1">
                      <div class="avatar bg-light-success">
                        <div class="avatar-content">
                          <i class="avatar-icon" data-feather="check"></i>
                        </div>
                      </div>
                    </div>
                    <div class="list-item-body flex-grow-1">
                      <p class="media-heading">
                        <span class="fw-bolder">Sales report</span
                        >&nbsp;generated
                      </p>
                      <small class="notification-text">
                        Last month sales report generated</small
                      >
                    </div>
                  </div> </a
                ><a class="d-flex" href="#">
                  <div class="list-item d-flex align-items-start">
                    <div class="me-1">
                      <div class="avatar bg-light-warning">
                        <div class="avatar-content">
                          <i
                            class="avatar-icon"
                            data-feather="alert-triangle"
                          ></i>
                        </div>
                      </div>
                    </div>
                    <div class="list-item-body flex-grow-1">
                      <p class="media-heading">
                        <span class="fw-bolder">High memory</span>&nbsp;usage
                      </p>
                      <small class="notification-text">
                        BLR Server using high memory</small
                      >
                    </div>
                  </div>
                </a>
              </li>
              <li class="dropdown-menu-footer">
                <a class="btn btn-primary w-100" href="#"
                  >Read all notifications</a
                >
              </li>
            </ul>
          </li> -->

          <li class="nav-item dropdown dropdown-user">
            <a
              class="nav-link dropdown-toggle dropdown-user-link"
              id="dropdown-user"
              href="#"
              data-bs-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <div class="user-nav d-sm-flex d-none">
                <span class="user-name fw-bolder"> {{date('l, d  F, Y ')}} &nbsp;<strong class="orangeColor font-size-lg" >|</strong>&nbsp; Welcome <?php echo Session::get("name"); ?>!</span
                >
              </div>
              <!-- <span class="avatar"
                ><img
                  class="round"
                  src="{{env('SITE_URL')}}/theme/app-assets/images/portrait/small/avatar-s-11.jpg"
                  alt="avatar"
                  height="40"
                  width="40" /><span class="avatar-status-online"></span
              ></span> -->
            </a>
            <div
              class="dropdown-menu dropdown-menu-end"
              aria-labelledby="dropdown-user"
            >
              <a class="dropdown-item" href="#"
                ><i class="me-50" data-feather="user"></i> Profile</a
              ><a class="dropdown-item" href="#"
                ><i class="me-50" data-feather="mail"></i> Inbox</a
              ><a class="dropdown-item" href="#"
                ><i class="me-50" data-feather="check-square"></i> Task</a
              ><a class="dropdown-item" href="#"
                ><i class="me-50" data-feather="message-square"></i> Chats</a
              >
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#"
                ><i class="me-50" data-feather="settings"></i> Settings</a
              ><a class="dropdown-item" href="#"
                ><i class="me-50" data-feather="credit-card"></i> Pricing</a
              ><a class="dropdown-item" href="#"
                ><i class="me-50" data-feather="help-circle"></i> FAQ</a
              ><a class="dropdown-item" href="{{URL::to('admin/logout')}}"
                ><i class="me-50" data-feather="power"></i> Logout</a
              >
            </div>
          </li>

          <li class="nav-item">
            <img
              src="{{env('SITE_URL')}}/theme/app-assets/images/logo/big_logo.png "
              width="100%"
              height="40px"
            />
          </li>
        </ul>
      </div>
    </nav>
    <ul class="main-search-list-defaultlist d-none">
      <li class="d-flex align-items-center">
        <a href="#">
          <h6 class="section-label mt-75 mb-0">Files</h6>
        </a>
      </li>
      <li class="auto-suggestion">
        <a
          class="d-flex align-items-center justify-content-between w-100"
          href="#"
        >
          <div class="d-flex">
            <div class="me-75">
              <img
                src="{{env('SITE_URL')}}/theme/app-assets/images/icons/xls.png"
                alt="png"
                height="32"
              />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">Two new item submitted</p>
              <small class="text-muted">Marketing Manager</small>
            </div>
          </div>
          <small class="search-data-size me-50 text-muted">&apos;17kb</small>
        </a>
      </li>
      <li class="auto-suggestion">
        <a
          class="d-flex align-items-center justify-content-between w-100"
          href="#"
        >
          <div class="d-flex">
            <div class="me-75">
              <img
                src="{{env('SITE_URL')}}/theme/app-assets/images/icons/jpg.png"
                alt="png"
                height="32"
              />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">52 JPG file Generated</p>
              <small class="text-muted">FontEnd Developer</small>
            </div>
          </div>
          <small class="search-data-size me-50 text-muted">&apos;11kb</small>
        </a>
      </li>
      <li class="auto-suggestion">
        <a
          class="d-flex align-items-center justify-content-between w-100"
          href="#"
        >
          <div class="d-flex">
            <div class="me-75">
              <img
                src="{{env('SITE_URL')}}/theme/app-assets/images/icons/pdf.png"
                alt="png"
                height="32"
              />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">25 PDF File Uploaded</p>
              <small class="text-muted">Digital Marketing Manager</small>
            </div>
          </div>
          <small class="search-data-size me-50 text-muted">&apos;150kb</small>
        </a>
      </li>
      <li class="auto-suggestion">
        <a
          class="d-flex align-items-center justify-content-between w-100"
          href="#"
        >
          <div class="d-flex">
            <div class="me-75">
              <img
                src="{{env('SITE_URL')}}/theme/app-assets/images/icons/doc.png"
                alt="png"
                height="32"
              />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">Anna_Strong.doc</p>
              <small class="text-muted">Web Designer</small>
            </div>
          </div>
          <small class="search-data-size me-50 text-muted">&apos;256kb</small>
        </a>
      </li>
      <li class="d-flex align-items-center">
        <a href="#">
          <h6 class="section-label mt-75 mb-0">Members</h6>
        </a>
      </li>
      <li class="auto-suggestion">
        <a
          class="d-flex align-items-center justify-content-between py-50 w-100"
          href="#"
        >
          <div class="d-flex align-items-center">
            <div class="avatar me-75">
              <img
                src="{{env('SITE_URL')}}/theme/app-assets/images/portrait/small/avatar-s-8.jpg"
                alt="png"
                height="32"
              />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">John Doe</p>
              <small class="text-muted">UI designer</small>
            </div>
          </div>
        </a>
      </li>
      <li class="auto-suggestion">
        <a
          class="d-flex align-items-center justify-content-between py-50 w-100"
          href="#"
        >
          <div class="d-flex align-items-center">
            <div class="avatar me-75">
              <img
                src="{{env('SITE_URL')}}/theme/app-assets/images/portrait/small/avatar-s-1.jpg"
                alt="png"
                height="32"
              />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">Michal Clark</p>
              <small class="text-muted">FontEnd Developer</small>
            </div>
          </div>
        </a>
      </li>
      <li class="auto-suggestion">
        <a
          class="d-flex align-items-center justify-content-between py-50 w-100"
          href="app-user-view-account.html"
        >
          <div class="d-flex align-items-center">
            <div class="avatar me-75">
              <img
                src="{{env('SITE_URL')}}/theme/app-assets/images/portrait/small/avatar-s-14.jpg"
                alt="png"
                height="32"
              />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">Milena Gibson</p>
              <small class="text-muted">Digital Marketing Manager</small>
            </div>
          </div>
        </a>
      </li>
      <li class="auto-suggestion">
        <a
          class="d-flex align-items-center justify-content-between py-50 w-100"
          href="#"
        >
          <div class="d-flex align-items-center">
            <div class="avatar me-75">
              <img
                src="{{env('SITE_URL')}}/theme/app-assets/images/portrait/small/avatar-s-6.jpg"
                alt="png"
                height="32"
              />
            </div>
            <div class="search-data">
              <p class="search-data-title mb-0">Anna Strong</p>
              <small class="text-muted">Web Designer</small>
            </div>
          </div>
        </a>
      </li>
    </ul>
    <ul class="main-search-list-defaultlist-other-list d-none">
      <li class="auto-suggestion justify-content-between">
        <a
          class="d-flex align-items-center justify-content-between w-100 py-50"
        >
          <div class="d-flex justify-content-start">
            <span class="me-75" data-feather="alert-circle"></span
            ><span>No results found.</span>
          </div>
        </a>
      </li>
    </ul>
    <!-- END: Header-->

    <!-- BEGIN: Main Menu-->
    @include('admin.layouts.thememenu')
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <!-- <div class="content-wrapper container-xxl p-0"> -->
      <div class="content-wrapper container-xxl">
        <div class="content-header row"></div>
        <div class="content-body">
          <!-- Dashboard Ecommerce Starts -->
            @yield('content')
          <!-- Dashboard Ecommerce ends -->
        </div>
      </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
    

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
      <p class="clearfix mb-0">
        
      </p>
    </footer>
    <button class="btn btn-primary btn-icon scroll-top" type="button">
      <i data-feather="arrow-up"></i>
    </button>
    <!-- END: Footer-->
    
    <!-- BEGIN: Vendor JS-->
    <script src="{{env('SITE_URL')}}/theme/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{env('SITE_URL')}}/theme/app-assets/vendors/js/charts/apexcharts.min.js"></script>
    <script src="{{env('SITE_URL')}}/theme/app-assets/vendors/js/extensions/toastr.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{env('SITE_URL')}}/theme/app-assets/js/core/app-menu.js"></script>
    <script src="{{env('SITE_URL')}}/theme/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->
    <script src="{{env('SITE_URL')}}/theme/js/plugins/summernote/summernote.js"></script>
    <!-- BEGIN: Page JS-->
    <!-- <script src="{{env('SITE_URL')}}/theme/app-assets/js/scripts/pages/dashboard-ecommerce.js"></script> -->
    <!-- END: Page JS-->

    <script>
      $(window).on("load", function () {
        if (feather) {
          feather.replace({
            width: 14,
            height: 14,
          });
        }
      });
    </script>
  </body>
  <!-- END: Body-->
</html>
