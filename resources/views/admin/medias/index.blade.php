@extends('admin.layouts.master')
@section('title')
{{env('APP_NAME')}}-Media Content
@endsection
@section('module')
Media Content
@endsection

@section('content')
@include('admin.components.message')

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">          
        <!-- <div class="btn-group pull-left">
          @if($privileges['Add']=='true') 
            <a href="{{URL::to('/admin/medias/create')}}" class="btn btn-info"><i class="fa fa-edit"></i>Add Media</a>
          @endif
        </div> -->
      </div>
      <div class="panel-body table-responsive">
        @foreach($medias as $media)
        <div class="col-md-6" style="padding-top: 24px;">
          <h3><?php 
            if(strlen($media->media_name) > 20) 
            {
              echo substr( $media->media_name, 0, 20 ) . '...';
            }
            else
            {
              echo $media->media_name;
            }
          ?></h3>
          <?php $no_image=env('NO_PROFILE_IMAGE');
            if($media->media_extension === "jpg" || $media->media_extension === "png" || $media->media_extension === "jpeg")
            {
          ?>
          <div class="form-group" style="margin:5px">           
              <div class="fileinput fileinput-new" data-provides="fileinput">
              <?php
                  $logo_path = '';
                  $no_image=env('NO_PROFILE_IMAGE');
                  if(!empty($media->media_path))
                  {
                      $logo_path = $media->media_path;
                   ?>
                  <div class="fileinput-new thumbnail" style="width: 450px; height: 350px;">
                  <a>
                      <img src="<?php echo $logo_path ?>" alt="..." style="width: 450px; height: 350px;">
                      </a>
                  </div>
                  <?php } else { ?>
                  <div class="fileinput-new thumbnail" style="width: 450px; height: 350px;">
                  <a>
                      <img src="{{env('SITE_URL')}}/<?php echo $no_image ?>" alt="..." style="width: 450px; height: 350px;">
                      </a>
                  </div>
                   <?php } ?>
              </div>
          </div>
        <?php } elseif($media->media_extension === "mp4" || $media->media_extension === "mov" || $media->media_extension === "3gp"){ ?>
          <video width="450" height="350" controls>
            <source src="{{$media->media_path}}" type="video/{{$media->media_extension}}">
          </video>
        <?php }?>
        <p></p>
        <!-- <div style="float:left;padding-right:10px;">
           @if($privileges['Delete']=='true')
            {{ Form::open(array('onsubmit' => 'return confirm("Are you sure you want to delete?")','method' => 'DELETE', 'route' => array('medias.destroy', $media->id))) }}
            <button type="submit" class="btn btn-danger btn-xs pull-right" style="font-size: 11px;padding: 4px 12px;">Delete</button>
            {{ Form::close() }}
           @endif
        </div> -->
      </div>
      @endforeach
    </div>
  </div>
 </div>
</div> 

@endsection