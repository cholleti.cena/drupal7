@extends('admin.layouts.master')
@section('title')
{{env('APP_NAME')}}-Media
@endsection
@section('module')
Media
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return CategoryValidate();','route' => 'medias.store','files'=>true)) }}
<div class="form-group form-horizontal">
		<div class="panel panel-default">
		</br>
			<div class="col-md-6">
				{{ Form::ahText('media_name','Media Name :','',array('maxlength' => '100'))  }}
        <div class="form-group" style="margin:5px">
          <label for="media_name" class="control-label col-sm-4">Media :</label>
            <div class="col-sm-8">
                <input class="form-control" maxlength="100" name="media_path" type="file" value="" id="media_path">
            </div>
        </div>
				</br>
		    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('medias.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	 </div>
 </div>
@endsection