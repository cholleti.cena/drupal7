@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Fuel Charges
@endsection
@section('module')
Fuel Charges
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return CategoryValidate();','route' => 'squarefootage.store','files'=>true)) }}

<div class="card mb-4">
   <div class="card-body after-loading-icon">
     <div class="container-fluid">
      	<div class="col-md-6">
        
				{{ Form::ahNumber('min_range','Min Range :','',array())  }}				
				{{ Form::ahNumber('max_range','Max Range :','',array()) }}
				{{ Form::ahNumber('minutes_per_room','Minutes per room :','',array()) }}
				{{ Form::ahNumber('cubic_feet_per_room','Cubic Feet per room :','',array()) }}
				{{ Form::ahNumber('weight_per_room','Weight per room :','',array()) }}
				{{ Form::ahNumber('people','People :','',array()) }}
				{{ Form::ahNumber('trucks','Trucks :','',array()) }}
				{{ Form::ahNumber('price','Price :','',array()) }}
		    {{ Form::ahSelect('is_active','Status :','1',array('1' => 'Active', '0' => 'Inactive')) }}
	    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('squarefootage.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	</div>
  </div>
</div>
@endsection