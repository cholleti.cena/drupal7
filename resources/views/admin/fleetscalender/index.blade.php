@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Fleets Calender
@endsection
@section('module')
Schedules
@endsection

@section('content')
@include('admin.components.message')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{env('SITE_URL')}}/theme/calendar/fonts/icomoon/style.css">
  
    <link href="{{env('SITE_URL')}}/theme/calendar/fullcalendar/packages/core/main.css" rel='stylesheet' />
    <link href="{{env('SITE_URL')}}/theme/calendar/fullcalendar/packages/daygrid/main.css" rel='stylesheet' />   
    <!-- Style -->
    <link rel="stylesheet" href="{{env('SITE_URL')}}/theme/calendar/css/style.css">	
    <style>  
   
  .modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 150%;
}
.modal-backdrop.show {
    opacity: 0.5;
}
.modal-backdrop.fade {
    opacity: 0;
}
.modal-backdrop {
  position: inherit !important;
    top: 0;
    left: 0;
    z-index: 1050;
    width: 100vw;
    height: 100vh;
    background-color: #22292f;
}

</style>

<div class="content-wrapper container-xxl p-0">
        <div class="content-header row"></div>
        <div class="content-body">
          <section id="dashboard-ecommerce">
            <div class="">
    <div id='calendar'></div>

   </div>

   <div id="calendarModal" class="modal fade">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Fleet Details</h4>
        </div>
        <div id="modalBody" class="modal-body">
        <span id="result" class="modal-title"></span>
               
        <div class="modal-footer">
            <button class="btn" id="cancelbtn" >Cancel</button>
        </div>
    </div>
</div>
</div>
          </section>
        </div>  
      </div>
      


      

 <script src="{{env('SITE_URL')}}/theme/calendar/js/jquery-3.3.1.min.js"></script>
    <script src="{{env('SITE_URL')}}/theme/calendar/js/popper.min.js"></script>
    <script src="{{env('SITE_URL')}}/theme/calendar/js/bootstrap.min.js"></script>

    <script src="{{env('SITE_URL')}}/theme/calendar/fullcalendar/packages/core/main.js"></script>
    <script src="{{env('SITE_URL')}}/theme/calendar/fullcalendar/packages/interaction/main.js"></script>
    <script src="{{env('SITE_URL')}}/theme/calendar/fullcalendar/packages/daygrid/main.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>



    <script>
      document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');



    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid' ],
      defaultDate: '<?php echo date('Y-m-d');?>',
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: <?php print_r(json_encode($leadRepo)); ?>,
      eventClick: function(info) {
              
        var dataString={};
            dataString['eventName']=info.event.title;
            dataString['startDate']=moment(info.event.start).format('YYYY-MM-DD');           
            $.ajax({
                type : 'POST',
                url : '/admin/fleetscalenderDetails',
                data: dataString,
                headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                  fleetsData=[];          
                    //alert("Data Insert SuccessFully");
                    //if(data.success)
                      //alert("Data Insert SuccessFully");
                      $("#result").empty();
                      fleetsData=data; 
                  $('#calendarModal').modal('show');



        $.each(fleetsData, function(index, value){
             $("#result").append("<table border='1' class='table table-bordered table-striped sortable'><tbody><tr><td style='line-height: 20px;padding-top: 10px;'> <b>Client : </b>" + value.title + '</b><br>'+"<b>Type of Service : </b>" + value.type + '<br>'
            +"<b>Total Cost : </b>" + value.hourly_rate + '<br>'+"<b>Scheduled Date : </b>" + value.start + '</b><br></td></tr></table>');
        });
                }
              });
             // calendar.render();
        
     
       
      }

    });
    
    calendar.render();
  });



   $("#cancelbtn").click(function(){
    //$("#result").remove("");
    $("#result").empty();
    $('#calendarModal').modal('hide');
    
  });

    </script>

    <script src="{{env('SITE_URL')}}/theme/calendar/js/main.js"></script>


@endsection
