@extends('admin.layouts.master')
@section('title')
{{env('APP_NAME')}}-Change Password
@endsection
@section('module')
Change Password
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahPassword', 'admin.components.form.password', ['name', 'labeltext'=>null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahFile', 'admin.components.form.file', ['name', 'labeltext'=>null,'value' =>null, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return validations();','route' => 'profile.store','files'=>true)) }}
<div class="form-group form-horizontal">
        <div class="panel panel-default">
        </br>
            <div class="col-md-6">
                <div class="form-group" style="margin:5px">
                        <label for="email" class="control-label col-sm-4">Email :</label>
                    <div class="col-sm-8">
                                <label for="email" class="control-label" style="text-align:left"><?php echo Session::get('admin_email')?></label>
                    </div>
                </div>
                {{ Form::ahPassword('currentpassword','Current Password :',array('maxlength' => '100')) }}
                {{ Form::ahPassword('password','New Password :',array('maxlength' => '100')) }}
                {{ Form::ahPassword('new_password','Confirm Password :',array('maxlength' => '100')) }}
                </br>
            </div>
             
        <div class="form-group">
            <div class="panel-footer">
                <div class="col-md-6 col-md-offset-3">
                    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                </div>
            </div>
        </div>
     </div>
 </div>
 <script type="text/javascript">

  function validations()
{
    if($('#currentpassword').val()=="")
    {
        alert('Please Enter Current Password');
        $('#currentpassword').focus();
        return false;
    }
    if($('#password').val()=="")
    {
        alert('Please Enter New Password');
        $('#password').focus();
        return false;
    }   
    if($('#new_password').val()=="")
    {
        alert('Please Enter Confirm Password');
        $('#new_password').focus();
        return false;
    } 
}
</script>
@endsection