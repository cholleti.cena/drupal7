@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Dashboard
@endsection


@section('content')

<section id="dashboard-ecommerce" class="manrope_font">
            <div class="match-height">
              <!-- Statistics Card -->

              <div class="card-statistics">
                <!-- <div class="card-header">
                  <h4
                    class="card-title"
                    style="color: #000;"
                  >
                    @section('module')
          						Dashboard
          					@endsection
                  </h4>
                  <div class="d-flex align-items-center">
                    <p
                      class="card-text font-small-2 mb-0"
                      style="color: #277cbb;font-weight: 900;"
                    >
                      Today is  {{date('l, d  F, Y ')}}
                    </p>
                  </div>
                </div> -->

                <div class="statistics-body">
                  <div class="row">

                    <div class="col-xl-2 col-sm-6 col-12 mb-2 mb-xl-0">
                      <div class="card">
                        <div class="card-body" style="padding: 0.5rem">
                          <div class="">
                            <div class="my-auto bg-light-primary me-12">
                              <div class="avatar-content">
                                <div class="text-large text-center"> 
                                  <i class="fa fa-users fa-lg  text-center" aria-hidden="true"></i>
                                </div>
                              </div>
                            </div>
                            <div class="my-auto details-data">
                              <div class="text-large counts text-center">{{$data['leads_count']}}</div>
                              <div class="small card-description text-center">Leads Today</div>
                              <br />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-2 col-sm-6 col-12 mb-2 mb-xl-0">
                      <div class="card">
                        <div class="card-body" style="padding: 0.5rem">
                          <div class="">
                            <div class="my-auto bg-light-primary me-12">
                              <div class="avatar-content">
                                <div class="text-large text-center"> 
                                  <i class="fa fa-address-card fa-lg  text-center" aria-hidden="true"></i>
                                </div>
                              </div>
                            </div>
                            <div class="my-auto details-data">
                              <div class="text-large counts text-center">{{$data['booked_count']}}</div>
                              <div class="small card-description text-center">Booked Today</div>
                              <br />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    
                    <div class="col-xl-2 col-sm-6 col-12 mb-2 mb-xl-0">
                      <div class="card">
                        <div class="card-body" style="padding: 0.5rem">
                          <div class="">
                            <div class="my-auto bg-light-primary me-12">
                              <div class="avatar-content">
                                <div class="text-large text-center"> 
                                  <!-- <i class="fa fa-rv    text-center" aria-hidden="true"></i> -->
                                  <i class="fa fa-truck fa-lg  text-center" aria-hidden="true"></i>
                                </div>
                              </div>
                            </div>
                            <div class="my-auto details-data">
                              <div class="text-large counts text-center">{{$data['moves_count']}}</div>
                              <div class="small card-description text-center">Moves Today</div>
                              <br />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-2 col-sm-6 col-12 mb-2 mb-xl-0">
                      <div class="card">
                        <div class="card-body" style="padding: 0.5rem">
                          <div class="">
                            <div class="my-auto bg-light-primary me-12">
                              <div class="avatar-content">
                                <div class="text-large text-center"> 
                                  <i class="fa fa-calendar fa-lg  text-center" aria-hidden="true"></i>
                                </div>
                              </div>
                            </div>
                            <div class="my-auto details-data">
                              <div class="text-large counts text-center">{{$data['moves_monthcount']}}</div>
                              <div class="small card-description text-center">Moves This Month</div>
                              <br />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-2 col-sm-6 col-12 mb-2 mb-xl-0">
                      <div class="card">
                        <div class="card-body" style="padding: 0.5rem">
                          <div class="">
                            <div class="my-auto bg-light-primary me-12">
                              <div class="avatar-content">
                                <div class="text-large text-center"> 
                                  <i class="fa fa-question-circle-o fa-lg  text-center" aria-hidden="true"></i>
                                </div>
                              </div>
                            </div>
                            <div class="my-auto details-data">
                              <div class="text-large counts text-center">{{$data['notmoves_monthcount']}}</div>
                              <div class="small card-description text-center">Moves Not Booked This Month</div>
                              <br />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-xl-2 col-sm-6 col-12 mb-2 mb-xl-0">
                      <div class="card">
                        <div class="card-body" style="padding: 0.5rem">
                          <div class="">
                            <div class="my-auto bg-light-primary me-12">
                              <div class="avatar-content">
                                <div class="text-large text-center"> 
                                  <i class="fa fa-usd fa-lg  text-center" aria-hidden="true"></i>
                                </div>
                              </div>
                            </div>
                            <div class="my-auto details-data">
                              <div class="text-large counts text-center">{{$data['avgmovesmonth']}}</div>
                              <div class="small card-description text-center">Avg. Move Value This Month</div>
                              <br />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                     
                  </div>
                </div>
              </div>

              <!--/ Statistics Card -->
            </div>

            <div class="row" style="margin-top: 20px">
              <!-- progress-bar-article1 -->
              <div class="col-lg-3 col-md-6 col-6">
                <div class="card card-browser-states">
                  <div class="card-header">
                    <div>
                      <h4
                        class="card-title"
                        style="color: #000;  font-weight: 900;"; 
                      >
                        Sales Leaders This Month
                      </h4>
                      <hr class="new1" />
                  
                      @foreach($data['salesleadersmonth'] as $key=>$salesusers) 
                     <div class="progress-bar-article">
                        <p>{{$salesusers['first_name']}}</p>
                        <div class="progress">
                          <div
                            class="progress-bar bg-primary"
                            role="progressbar"
                            style="width: 75%"
                            aria-valuenow="75"
                            aria-valuemin="0"
                            aria-valuemax="100"
                          ></div>
                        </div>
                        <p> {{$salesusers['leadsCount']}} moves/${{$salesusers['price']}}</p>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
              <!--/ progress-bar-article -->

              <!-- progress-bar-article1 -->
              <div class="col-lg-3 col-md-6 col-12">
                <div class="card card-browser-states">
                  <div class="card-header">
                    <div>
                      <h4
                        class="card-title"
                        style="color: #000;  font-weight: 900;"
                      >
                        Top Referral Sources This Month
                      </h4>
                      <hr class="new1" />
                      @foreach($data['topreferalsmonth'] as $key=>$referal)
                      <div class="progress-bar-article">
                        <p>{{$referal['name']}}</p>
                        <div class="progress">
                          <div
                            class="progress-bar bg-primary"
                            role="progressbar"
                            style="width: 75%"
                            aria-valuenow="75"
                            aria-valuemin="0"
                            aria-valuemax="100"
                          ></div>
                        </div>
                        <p>{{$referal['leadsCount']}} moves/${{$referal['price']}}</p>
                      </div>
                      @endforeach
                      <!--<div class="progress-bar-article">
                        <p>Facebook</p>
                        <div class="progress">
                          <div
                            class="progress-bar bg-primary"
                            role="progressbar"
                            style="width: 50%"
                            aria-valuenow="75"
                            aria-valuemin="0"
                            aria-valuemax="100"
                          ></div>
                        </div>
                        <p>3 moves/$3,775</p>
                      </div>-->

                      <!--<div class="progress-bar-article">
                        <p>Google Ad</p>
                        <div class="progress">
                          <div
                            class="progress-bar bg-primary"
                            role="progressbar"
                            style="width: 30%"
                            aria-valuenow="75"
                            aria-valuemin="0"
                            aria-valuemax="100"
                          ></div>
                        </div>
                        <p>5 moves/$2,358</p>
                      </div>-->

                     <!-- <div class="progress-bar-article">
                        <p>Prime Storage</p>
                        <div class="progress">
                          <div
                            class="progress-bar bg-primary"
                            role="progressbar"
                            style="width: 10%"
                            aria-valuenow="75"
                            aria-valuemin="0"
                            aria-valuemax="100"
                          ></div>
                        </div>
                        <p>1 moves/$1,650</p>
                      </div>-->
                    </div>
                  </div>
                </div>
              </div>
              <!--/ progress-bar-article -->
            </div>
            <!-- progreess row end-->

            <br />

            <div class="card mb-4">
              <h6 class="card-header card-elements">
                <div
                  class="card-header-title"
                  style="color: #000; "
                >
                  Foreman
                </div>
                <div class="card-header-elements ml-auto"></div>
              </h6>
              <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12">
                  <div class="card-body-tab">
                    <div id="" class="table-responsive">
                      <table class="table card-table small" id="">
                        <thead class="table-header">
                          <tr style="font-weight: bold; ">
                            <th>Estimate</th>
                            <th>Invoice</th>
                            <th>Customer</th>
                            <th>Price</th>
                            <th>Move Date</th>
                            <th>Arrival time</th>
                            <th>Est. Hours</th>
                            <th>Weight</th>
                            <th>Miles/Kms</th>
                            <th>Crew</th>
                            <th>Truck</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td
                              colspan="11"
                              style="font-weight: 500; "
                            >
                              No Job Assignments
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="card mb-4">
              <h6 class="card-header card-elements">
                <div class="card-header-title">
                  Company Yearly Revenue Statistics
                </div>
                <div class="card-header-elements ml-auto"></div>
              </h6>
              <div class="row no-gutters row-bordered">
                <div class="col-md-12 col-lg-12 col-xl-12">
                  <div class="card-body-tab">
                    <div class="table-responsive">
                      <table class="table card-table small">
                        <thead style="background-color: #277cbb">
                          <tr style="font-weight: bold">
                            <th style="width: 5%">Type</th>
                            <th style="width: 5%">01/22</th>
                            <th style="width: 5%">02/22</th>
                            <th style="width: 5%">03/22</th>
                            <th style="width: 5%">04/22</th>
                            <th style="width: 5%">05/22</th>
                            <th style="width: 5%">06/22</th>
                            <th style="width: 5%">07/22</th>
                            <th style="width: 5%">08/22</th>
                            <th style="width: 5%">09/22</th>
                            <th style="width: 5%">10/22</th>
                            <th style="width: 5%">11/22</th>
                            <th style="width: 5%">12/22</th>
                            <!--<th style="width: 5%">12 M</th>-->
                          </tr>  </thead>
                        
                        @foreach($data['yearlyrevenuedata'] as $key=>$yearsdt)               
                          <tr >
                            <td style="width: 5%"><strong>{{$key}}</strong></td>
                             @foreach($yearsdt['monthdata'] as $key=>$month)  
                            <td style="width: 5%">{{$month['price']}}</td>
                             @endforeach
                          </tr>
                           @endforeach
                      
                        <tbody></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6 col-lg-12 col-xl-6">
                <div class="card mb-4">
                  <h6 class="card-header card-elements">
                    <div class="card-header-title">Company Statistics</div>
                    <div class="card-header-elements ml-auto"></div>
                  </h6>
                  <div class="row no-gutters row-bordered">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                      <div class="card-body-tab">
                        <div class="table-responsive">
                          <table class="table card-table small">
                            <thead style="background-color: #277cbb !important">
                              <tr>
                                <th style="width: 5%">Type</th>
                                <th style="width: 9%">Today</th>
                                <th style="width: 9%">Weekly</th>
                                <th style="width: 9%">Monthly</th>
                              </tr>
                            </thead>
                            <tbody>
								 @foreach($data['companystatistics'] as $key=>$cmpstatic) 
                          <tr>
                            <td style="">
                              <strong>{{$key}}</strong>
                            </td>
                             @foreach($cmpstatic['data'] as $key=>$cmpstaticdays) 
                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >({{$cmpstaticdays['leads']}}) ${{$cmpstaticdays['price']}}</a
                              >
                            </td>
                            @endforeach
                          </tr>
                             @endforeach
                             <!-- <tr>
                                <td style="">
                                  <strong>Estimated</strong>
                                </td>
                                <td>
                                  <a
                                    href="#"
                                    data-toggle="modal"
                                    data-target="#exampleModalCenter"
                                    >(0) $0</a
                                  >
                                </td>
                                <td>
                                  <a
                                    href="#"
                                    data-toggle="modal"
                                    data-target="#exampleModalCenter"
                                    >(1) $500</a
                                  >
                                </td>

                                <td>
                                  <a
                                    href="#"
                                    data-toggle="modal"
                                    data-target="#exampleModalCenter"
                                    >(34) $63,273</a
                                  >
                                </td>
                              </tr>
                              <tr>
                                <td style="">
                                  <strong>Booked</strong>
                                </td>
                                <td>
                                  <a
                                    href="#"
                                    data-toggle="modal"
                                    data-target="#exampleModalCenter"
                                    >(0) $0</a
                                  >
                                </td>
                                <td>
                                  <a
                                    href="#"
                                    data-toggle="modal"
                                    data-target="#exampleModalCenter"
                                    >(2) $1,200</a
                                  >
                                </td>

                                <td>
                                  <a
                                    href="#"
                                    data-toggle="modal"
                                    data-target="#exampleModalCenter"
                                    >(19) $21,939</a
                                  >
                                </td>
                              </tr>
                              <tr>
                                <td style="">
                                  <strong>Completed</strong>
                                </td>
                                <td>
                                  <a
                                    href="#"
                                    data-toggle="modal"
                                    data-target="#exampleModalCenter"
                                    >(0) $0</a
                                  >
                                </td>
                                <td>
                                  <a
                                    href="#"
                                    data-toggle="modal"
                                    data-target="#exampleModalCenter"
                                    >(0) $0</a
                                  >
                                </td>

                                <td>
                                  <a
                                    href="#"
                                    data-toggle="modal"
                                    data-target="#exampleModalCenter"
                                    >(4) $20,997</a
                                  >
                                </td>
                              </tr>
                              <tr>
                                <td style=" color: #000">
                                  <strong>Cancelled</strong>
                                </td>
                                <td>
                                  <a
                                    href="#"
                                    data-toggle="modal"
                                    data-target="#exampleModalCenter"
                                    >(0) $0</a
                                  >
                                </td>
                                <td>
                                  <a
                                    href="#"
                                    data-toggle="modal"
                                    data-target="#exampleModalCenter"
                                    >(0) $0</a
                                  >
                                </td>

                                <td>
                                  <a
                                    href="#"
                                    data-toggle="modal"
                                    data-target="#exampleModalCenter"
                                    >(2) $1,550</a
                                  >
                                </td>
                              </tr>-->
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6 col-lg-12 col-xl-6">
                <div class="card mb-4">
                  <h6 class="card-header card-elements">
                    <div class="card-header-title">My Statistics</div>
                    <div class="card-header-elements ml-auto"></div>
                  </h6>
                  <div class="row no-gutters row-bordered">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                      <div class="card-body-tab">
                      <div class="table-responsive">
                        <table class="table card-table small">
                          <thead style="background-color: #277cbb">
                            <tr>
                              <th style="width: 5%">Type</th>
                              <th style="width: 9%">Today</th>
                              <th style="width: 9%">Weekly</th>
                              <th style="width: 9%">Monthly</th>
                            </tr>
                          </thead>
                          
                        
                           @foreach($data['mystatistics'] as $key=>$mystatic) 
                          <tr>
                            <td style="">
                              <strong>{{$key}}</strong>
                            </td>
                             @foreach($mystatic['data'] as $key=>$mystaticdays) 
                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >({{$mystaticdays['leads']}}) ${{$mystaticdays['price']}}</a
                              >
                            </td>
                            @endforeach
                          </tr>
                             @endforeach
                          
                          
                          <!--<tr>
                            <td style="">
                              <strong>Estimated</strong>
                            </td>

                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >(0) $0</a
                              >
                            </td>
                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >(1) $500</a
                              >
                            </td>

                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >(34) $63,273</a
                              >
                            </td>
                          </tr>
                          <tr>
                            <td style="">
                              <strong>Booked</strong>
                            </td>
                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >(0) $0</a
                              >
                            </td>
                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >(2) $1,200</a
                              >
                            </td>

                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >(19) $21,939</a
                              >
                            </td>
                          </tr>
                          <tr>
                            <td style="">
                              <strong>Completed</strong>
                            </td>
                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >(0) $0</a
                              >
                            </td>
                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >(0) $0</a
                              >
                            </td>

                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >(4) $20,997</a
                              >
                            </td>
                          </tr>
                          <tr>
                            <td style=" color: #000">
                              <strong>Cancelled</strong>
                            </td>
                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >(0) $0</a
                              >
                            </td>
                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >(0) $0</a
                              >
                            </td>

                            <td>
                              <a
                                href="#"
                                data-toggle="modal"
                                data-target="#exampleModalCenter"
                                >(2) $1,550</a
                              >
                            </td>
                          </tr>--->
                        </table>
                      </div>
                    </div>
                  </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6 col-lg-12 col-xl-6">
                <div class="card mb-4">
                  <h6 class="card-header with-elements">
                    <div class="card-header-title">
                      Company Recent Estimates
                    </div>
                    <div class="card-header-elements ml-auto"></div>
                  </h6>
                  <div class="row no-gutters row-bordered">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                      <div class="card-body-tab">
                      <div id="company_recent_estimates_div"
                        class="table-responsive">
                        <table class="table card-table small" id="estimates_table">
                          <thead style="background-color: #277cbb">
                            <tr>
                              <th>Estimate</th>
                              <th>Rep</th>
                              <th>Customer</th>
                              <th>Status</th>
                              <th>Price</th>
                            </tr>
                          </thead>
                          @foreach($data['comp_recntestleads'] as $key=>$comprcntest)       
                               <tr>
                                <td>{{$comprcntest['approximate_move_date']}}</td>
                                <td>{{$comprcntest['first_name']}}</td>
                                <td>{{$comprcntest['first_name']}}</td>
                                <td>{{$comprcntest['type']}}</td>
                                <td>{{$comprcntest['price']}}</td>
                              </tr>
                              @endforeach
                        </table>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6 col-lg-12 col-xl-6">
                <div class="card mb-4">
                  <h6 class="card-header with-elements">
                    <div class="card-header-title">My Recent Estimates</div>
                    <div class="card-header-elements ml-auto"></div>
                  </h6>
                  <div class="row no-gutters row-bordered">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                      <div class="card-body-tab">
                        <div id="my_recent_estimates_div" class="table-responsive">
                          <table class="table card-table small" id="estimates_table">
                            <thead style="background-color: #277cbb">
                              <tr>
                                <th>Estimate</th>
                                <th>Customer</th>
                                <th>Status</th>
                                <th>Price</th>
                              </tr>
                            </thead>
                              @foreach($data['my_recntestleads'] as $key=>$myrcntest)       
                               <tr>
                                <td>{{$myrcntest['approximate_move_date']}}</td>
                                <td>{{$myrcntest['first_name']}}</td>
                                <td>{{$myrcntest['type']}}</td>
                                <td>{{$myrcntest['price']}}</td>
                              </tr>
                              @endforeach
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6 col-lg-12 col-xl-6">
                <div class="card mb-4">
                  <h6 class="card-header with-elements">
                    <div class="card-header-title">Company Recent Event</div>

                    <div class="card-header-elements ml-auto"></div>
                  </h6>
                  <div class="row no-gutters row-bordered">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                      <div class="card-body-tab">
                        <div id="company_recent_events_div" class="table-responsive">
                          <table class="table card-table small" id="estimates_table">
                            <thead style="background-color: #277cbb">
                              <tr>
                                <th>Estimate</th>
                                <th>Source</th>
                                <th>Rep</th>
                                <th>Person</th>
                                <th>Comments</th>
                              </tr>
                                  </thead>                                
                             @foreach($data['comp_recntleads'] as $key=>$comprcnt)       
                               <tr>
                                <td>{{$comprcnt['approximate_move_date']}}</td>
                                <td>{{$comprcnt['origin_address']}}</td>
                                <td>{{$comprcnt['first_name']}}</td>
                                <td>{{$comprcnt['first_name']}}</td>
                                <td>{{$comprcnt['comments']}}</td>
                              </tr>
                              @endforeach
                        
                          </table>
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6 col-lg-12 col-xl-6">
                <div class="card mb-4">
                  <h6 class="card-header with-elements">
                    <div class="card-header-title">My Recent Event</div>
                    <div class="card-header-elements ml-auto"></div>
                  </h6>
                  <div class="row no-gutters row-bordered">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                      <div class="card-body-tab">
                        <div id="my_recent_events_div" class="table-responsive">
                          <table class="table card-table small" id="estimates_table">
                            <thead style="background-color: #e7e7e7">
                              <tr>
                                <th>Estimate</th>
                                <th>Source</th>
                                <th>Person</th>
                                <th>Comments</th>
                              </tr>
                            </thead>
                            @foreach($data['my_recntleads'] as $key=>$myrcnt)       
                               <tr>
                                <td>{{$myrcnt['approximate_move_date']}}</td>
                                <td>{{$myrcnt['origin_address']}}</td>
                                <td>{{$myrcnt['first_name']}}</td>
                                <td>{{$myrcnt['comments']}}</td>
                              </tr>
                              @endforeach
                          </table>
                         </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>

            <!--/ Transaction Card -->
          </section>
@endsection

