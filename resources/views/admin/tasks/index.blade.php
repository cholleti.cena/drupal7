@extends('admin.layouts.thememaster') @section('title') {{env('APP_NAME')}}-Tasks @endsection @section('module') Tasks @endsection @section('content') @include('admin.components.message')
{{ Form::open(array('onsubmit' => 'return CategoryValidate();','route' => 'tasks.create','files'=>true)) }}

<!-- Starts Modal content -->
<div id="myModal" class="modal">
  <div class="modal-content" style="display:block">
    <span class="close pull-right">&times;</span>
    <strong id="popuptitle">New Tasks</strong>
    <hr/>
    <div class="row">
      <form name="formName" id="formName" method="post" action="tasks/create">
        <input id='record_id' name="record_id" type="hidden" />
        <div class="container">
          <div class="col">
            <div class="row form-group" id="prospect_field_div" style="">
              <div class="col col-md">
                <label class="control-label" for="task_contact_person_id">Prospect/Customer or Person:</label>
              </div>
              <div class="col-form-label col-sm-4">
                <input class="form-control ui-autocomplete-input required" type="text" name="assigned_user_to" id="assigned_user_to" autocomplete="off">
              </div>
            </div>
            <div class="row form-group">
              <div class="col col-md">
                <label class="control-label" for="task_when">Due/When:</label>
              </div>
              <div class="col-form-label col-sm-4">
                <input type="hidden" name="hidden_when_date" id="hidden_when_date" value="m-d-Y">
                <input autocomplete="off" readonly="readonly" class="form-control flatpickr-input datepicker" type="text" name="when_date" id="when_date">
              </div>
            </div>
            <div class="row form-group">
              <div class="col col-md">
                <label class="control-label" for="task_task_type_id">Task Type:</label>
              </div>
              <div class="col-form-label col-sm-4">
                <!-- Remove survey option from drop down for creating a task as survey task will now create from estimate pag-1 .. task no. DB-1705 -->
                <select class="custom-select col-md" name="task_type" id="task_type">
                @foreach($tasktypes as $eachTaskType)
                  <option value="{{$eachTaskType->id}}" >{{$eachTaskType->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="row form-group">
              <div class="col col-md">
                <label class="control-label" for="task_employee_id">Assign To:</label>
              </div>
              <div class="col-form-label col-sm-4">
                <select class="custom-select col-sm-12" name="assigned_user_by" id="assigned_user_by">
                  <option value="7574">Anthony McWhinnie</option>
                  <option value="7588">Richard Dias</option>
                  <option value="7589">Shawn Aldrich</option>
                  <option value="7590">Chris Aldrich</option>
                  <option value="7591">Patrick Rico</option>
                  <option value="24062">Bud Carlson</option>
                  <option value="54672">Braden Michael Carlson</option>
                  <option value="61524">Paul Martin Aldrich</option>
                  <option value="61533">Brandon Patanao</option>
                  <option value="72728">Joseph Poirier</option>
                  <option value="73485">Robert Mullin</option>
                  <option value="74267">Veera Mogilicherla</option>
                  <option value="75439">Chris Ivory</option>
                  <option value="75890">Madeline Lawson</option>
                </select>
              </div>
            </div>
            <div class="row form-group">
              <div class="col col-md">
                <label class="control-label" for="task_priority">Priority:</label>
              </div>
              <div class="col-form-label col-sm-4">
                <select class="custom-select col-sm-12" name="priority" id="priority">
                  <option value="Low">Low</option>
                  <option value="Medium">Medium</option>
                  <option value="High">High</option>
                </select>
              </div>
            </div>
            <input type="hidden" name="customer_task_client_id" id="customer_task_client_id" value="">
            <input value="" type="hidden" name="task[client_id]" id="task_client_id">
            <input type="hidden" name="task[contact_person_id]" id="task_contact_person_id">
            <div class="row form-group">
              <div class="col col-md">
                <label class="control-label" for="task_status">Status:</label>
              </div>
              <div class="col-form-label col-sm-4">
                <select class="custom-select col-sm-12" name="status" id="status">
                  @foreach($taskStatus as $eachTaskStatus)
                  <option value="{{$eachTaskStatus->id}}" >{{$eachTaskStatus->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <!-- <div id="new_task_estimates" style="old_comments_title"  style="display:none"> </div>
            <div id="old_comments" style="display:none">
              <div class="col col-md">
                <label class="col-form-label" for="task_comments"></label>
              </div>
              <div class="col-auto col col-sm-4">
              </div>
            </div> -->
            <div class="row form-group">
              <div class="col col-md">
                <label class="control-label" for="task_comments">Comments:</label>
              </div>
              <div class="col-form-label col-sm-4">
                 
                  <textarea class="form-control  required" rows="5" cols="26" name="comments" id="comments" style="height: 85px;"></textarea>
                  <input value="74267" type="hidden" name="task[comments_attributes][0][employee_id]" id="task_comments_attributes_0_employee_id">
                 
              </div>
            </div>
            <div class="row form-group">
              <div class="col col-md">
                <input type="submit" id="create" value="Create" class="pull-right btn btn-primary" />
              </div>
              <div class="col col-md">
                <input type="button" value="Cancel" onClick="hidePopUp()" class="pull-left btn btn-danger" />
              </div>
            </div>
          </div>
        </div>
    </div>
    </form>
  </div>
</div>
<!-- Ends Modal  -->

<section class="dashboard-ecommerce">
<div class="match-height">
    <div class="panel panel-default">
    <div class="table-bar2 ">
        
        <div id="tabsneW" style="border: 0px;">
          <div class="btn-group pull-right">
            <button class="btn btn-primary" id="myBtn">New Task</button>
          </div>
          <ul>
            <li class="navTabs" data-id="all" ><a href="#all">All</a></li>
            @foreach($taskStatus as $eachTaskStatus)
            <li class="navTabs" data-id="{{$eachTaskStatus->id}}" ><a href="#tab{{$eachTaskStatus->id}}">{{$eachTaskStatus->name}}</a></li>
            @endforeach
          </ul>
          <div id="all">
            <!-- <p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p> -->
          </div>
          @foreach($taskStatus as $eachTaskStatus)
          <div id="tab{{$eachTaskStatus->id}}">
            <!-- <p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p> -->
          </div>
          @endforeach
           
          </div>
      
      <div class="table-bar2 tableGrid">
      <!-- <div class="panel-body table-responsive"> -->
        <table id="customers2" class="table table-striped datatable">
          <thead>
            <tr>
              <th>Task Id</th>
              <th>Task type</th>
              <th>Assigned By</th>
              <th>Assigned To</th>
              <th>Client</th>
              <th>Contact Person</th>
              <th>Priority</th>
              <th>Status</th>
              <th>When</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody id="statuswise">
            @foreach($tasks as $eachTask)
            <tr>
              <td><?php // print_r($eachTask);?>
                <a href="#" class="taskedit"  data-id="{{$eachTask->id}}">{{$eachTask->task_id}}</a>
              </td>
              <td>
                {{$eachTask->tasktypeName}}
              </td>
              <td>
                {{$eachTask->assigned_user_by}}
              </td>
              <td>
                {{$eachTask->assigned_user_to}}
              </td>
              <td>
                {{$eachTask->client ? $eachTask->client : 'N/A'}}
              </td>
              <td>
                {{$eachTask->contact_person ? $eachTask->contact_person : 'N/A'}}
              </td>
              <td>
                {{$eachTask->priority}}
              </td>
              <td>
                {{$eachTask->taskstatusName}}
              </td>
              <td>
                {{$eachTask->when_date}}
              </td>
              <td width="15%">
                <div >
                  <div style="float:left;padding:0 10px 10px 0;">
                    <!-- <button type="submit" class="btn btn-primary btn-xs pull-right fa fa-edit" style="font-size: 11px;padding: 4px 12px;"></button> -->
                    @if($privileges['Edit']=='true')
                        <i class="fa fa-edit cursorPoint taskedit" data-id="{{$eachTask->id}}"></i>
                    @endif 
                    </div>
                    <div style="float:left;padding-right:10px;">
                      @if($privileges['Delete']=='true')
                      @endif 
                      @if($privileges['Delete']=='true')
                      <i class="fa fa-trash cursorPoint taskdelete" data-id="{{$eachTask->id}}"></i>
                        {{ Form::close() }}
                        @endif
                    </div>
                </div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        
      </div>
    </div>
    </div>
  </div>
</section>
<script>
  // Get the modal
  var modal = document.getElementById("myModal");

  // Get the button that opens the modal
  var btn = document.getElementById("myBtn");

  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  // When the user clicks the button, open the modal
  btn.onclick = function() {
     $("#create").val("Create");
     $("#popuptitle").text("New Task");
    modal.style.display = "block";
  }

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
    $("#create").val("Create");
     $("#popuptitle").text("New Task");
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }

  function hidePopUp(){
    modal.style.display = "none";
  }
</script>
<script>
  $( function() {
    $( "#tabsneW" ).tabs();
    $('#when_date').datepicker({
      format: 'mm/dd/yyyy',
      startDate: 'd'
    });
    // $('#customers2').dataTable();
  });
  
  
  // pop up validation
  // js validate
  $(document).ready(function() {
    $("#create").click(function() {
      console.log('asdfsdf')
      $("#formName").validate({
        rules: {
          assigned_user_to: {
            required: true
          },
          when_date: {
            required: true
          },
          comments: {
            required: true
          }
        },
        messages: {
          assigned_user_to: {
            "required": "Please enter user"
          },
          when_date: {
            required: "Please select date"
          },
          comments: {
            required: "Please Enter Text"
          },
        },
        submitHandler: function() {
          submitTask();
          _token:'{{ csrf_token() }}'
        }
      })
    });

    $(".taskedit").click(function(){
      var id = $(this).attr('data-id');
      $.get("tasks/search/id/"+id, function(res) {
        if(res && res.data && res.data.length > 0 ){
          var data = res.data[0];
          var dateWhen = data.when_date.split("-");
          modal.style.display = "block";
          $("#assigned_user_to").val(data.assigned_user_to);
          $("#when_date").val(dateWhen[1]+"/"+dateWhen[2]+"/"+dateWhen[0]);
          $("#task_type").val(data.task_type);
          $("#priority").val(data.priority);
          $("#status").val(data.status);
          $("#comments").val(data.comments);
          $("#record_id").val(data.id);
          $("#create").val("Update");
          $("#popuptitle").text("Update Task");
        } else {
          alert("data not found")
        }
      }).fail(function() {
        console.log("fail")
      }).done(function() {
        console.log("completed");
      })
    })
    
    $(".taskdelete").click(function(){
      var id = $(this).attr('data-id');
      $.get("tasks/delete/"+id, function(res) {
        if(res && res.data && res.data.length > 0 ){
          var data = res.data;
          alert(data);
          var id = $(".ui-tabs-active").attr('data-id');
          getBasedStatus(id)
        } else {
          alert("data not found")
        }
      }).fail(function() {
        console.log("fail")
      }).done(function() {
        console.log("completed");
      })
    });

    $(".navTabs").click(function(){
      var id = $(this).attr('data-id');
      getBasedStatus(id)
    });
  });

  function submitTask() {
    $("#formName").submit()
  }

  function getBasedStatus(id){
    $.get("tasks/status/"+id, function(res) {
        console.log(res);
        $('#customers2').dataTable().fnDestroy();
        if(res && res.data && res.data.length > 0 ){
          var data = res.data[0];
          var htmlTask = `<thead>
            <tr>
              <th>Task Id</th>
              <th>Task type</th>
              <th>Assigned By</th>
              <th>Assigned To</th>
              <th>Client</th>
              <th>Contact Person</th>
              <th>Priority</th>
              <th>Status</th>
              <th>When</th>
              <th>Actions</th>
            </tr>
          </thead><tbody>`;
          res.data.map((j, i)=>{
          htmlTask += `<tr id="statuswise">
              <td>
                <a href="#" class="taskedit"  data-id="${j.id}">${j.task_id}</a>
              </td>
              <td>
                ${j.tasktypeName}
              </td>
              <td>
                ${j.assigned_user_by}
              </td>
              <td>
              ${j.assigned_user_to}
              </td>
              <td>
              ${j.client==null ? 'N/A' :j.client}
              </td>
              <td>
              ${j.contact_person==null ? 'N/A' : j.contact_person}
              </td>
              <td>
              ${j.priority}
              </td>
              <td>
              ${j.taskstatusName}
              </td>
              <td>
              ${j.when_date}
              </td>
              <td width="15%">
                <div >
                  <div style="float:left;padding:0 10px 10px 0;">
                        <i class="fa fa-edit cursorPoint taskedit" data-id="${j.id}"></i>
                    </div>
                    <div style="float:left;padding-right:10px;">
                      <i class="fa fa-trash cursorPoint taskdelete" data-id="${j.id}"></i>
                    </div>
                </div>
              </td>
            </tr>`
          });
          htmlTask += `</tbody>`;
          $("#customers2").html(htmlTask);
          $('#customers2').addClass('table table-striped data')
          // $('#customers2').dataTable();
        } else {
          htmlTask += `<tr>
              <td colspan="10">No data available</td>
              </tr>`

          }
      }).fail(function() {
        console.log("fail")
      }).done(function() {
        console.log("completed");
      })
  }
</script>

@endsection