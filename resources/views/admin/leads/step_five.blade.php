<div class="row">
	                             @foreach($metadata['storage_requirements'] as $kk=>$storage)
	                             	<div class="col-md-12">
									   <div class="form-group" style="margin:5px">
									        <label for="storage_requirements" class="control-label col-sm-4">{{$storage['name']}} : $</label>
									     <div class="col-sm-6">
									     	<input type="hidden" name="moving_information[storage_requirements_id][]" id="storage_requirements_id{{$kk}}">
									        <input class="form-control" min="0" maxlength="12" max="999999999999" name="storage_requirements_price[]" type="number" value="0.00" id="storage_requirements_price{{$kk}}">
									     </div>
									   </div>                           	            
									 </div>
	                             @endforeach
								 </div>