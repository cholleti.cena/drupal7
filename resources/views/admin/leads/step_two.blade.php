<div class="row">
		                            <div class="col-md-6">
		                            	<strong>{{ Form::ahNumber('moving_information[square_footage]','Total Square Footage:',$lead['square_footage'],array('min'=>'0','maxlength' => '12','max'=>'999999999999'))  }}</strong>
		                            	<strong>{{ Form::ahNumber('moving_information[estimated_boxes]','Total Estimated Boxes:',$lead['estimated_boxes'],array('min'=>'0','maxlength' => '12','max'=>'999999999999'))  }}</strong>
		                            	<div class="form-group" style="margin:5px">
							                <label for="move_size_id" class="control-label col-sm-4"><strong>Move Size:</strong></label>
							                <div class="col-sm-8">
							                    <select class="form-control select" data-live-search='true' id="move_size_id" name="moving_information[move_size_id]">
							                      @foreach($metadata['movesizes'] as $movesize)
							                      <option value="{{$movesize['id']}}" <?php 
							                      $res = $movesize['id'];
							                      $db_res = $lead['move_size_id']['id'];
							                      if($res == $db_res) echo 'selected="selected"' ?>>{{$movesize['name']}}</option>
							                    @endforeach
							                  </select>
							                </div>
							            </div>
							            <div class="form-group" style="margin:5px">
							                <label for="category_id" class="control-label col-sm-4"><strong>SELECT ITEMS:</strong></label>
							                <div class="col-sm-8">
							                    <select class="form-control" id="category_id" name="moving_information[category_id]">
							                     <option selected value="base">Please Select Category</option>
							                      @foreach($metadata['categories'] as $category)
							                      <option value="{{$category['id']}}">{{$category['name']}}</option>
							                    @endforeach
							                  </select>
							                </div>
							            </div>
							            <div class="form-group" style="margin:5px">
								          <label for="categories" class="control-label col-sm-4"><strong>Items:</strong></label>
								           <div class="col-sm-8">
								                  <select class="form-control" name="item_id" id="item_id" data-live-search='true'>
								                      <option>Please choose from above</option>		                      
								                  </select>
								              </div>              
								        </div>
		                            </div>
		                            <?php if(count($lead['category_wise_items']) > 0) {?>
		                            <div class="col-md-6">
		                            	<h3>INVENTORY:   Cu Ft: {{$lead['total_cubic_foot']}}, Weight: {{$lead['total_weight']}}</h3>
		                            	<table class="table table-striped table-bordered zero-configuration">
			                            <thead>
			                                <tr>
			                                  <th>ITEM</th>
			                                  <th>FLOOR</th>
			                                  <th>QTY</th>
			                                 <!--  <th></th> -->
			                                </tr>
			                            </thead>
			                            <tbody>                                                    			@foreach($lead['category_wise_items'] as $key=>$category)
			                            	<tr><td colspan="4" style="text-align:center;">{{$category['category_name']}}</td></tr>
			                            	@foreach($category['items'] as $ke=>$item)
											<tr class="pos-item_{{$item['id']}}">
		                                    	<td><span class="pos-cart-item">{{$item['name']}}</span></td>
		                                    	<td width="30%">
		                                    		<input type="hidden" name="moving_information[inventory_items][item_id][]" value="{{$item['id']}}">
		                                    		<input type="hidden" name="moving_information[item_name][]" value="{{$item['name']}}">
		                                    		<select class="form-control select" data-live-search='true' id="floor{{$key}}" name="moving_information[inventory_items][floor][]"><option value="0">Select floor</option>@foreach($floors as $floor)<option value="{{$floor}}" <?php 
							                      $res = $floor;
							                      $db_res = $item['floor'];
							                      if($res == $db_res) echo 'selected="selected"' ?>>{{$floor}}</option>@endforeach</select>
		                                    	</td>
		                                    	<td>
		                                    		<!-- <div class="btn-group pull-left"> 
											  	      <a href="#" class="btn btn-danger" id="item_minus_button{{$key}}"><i class="fa fa-minus"></i></a>
											        </div>  -->
											        <div class="col-sm-10">
		                                    		    <input type="number" name="moving_information[inventory_items][quantity][]" class="form-control" id="quantity" value="{{$item['quantity']}}" min="1" step="1">
		                                    	    </div>
		                                    	    <!-- <div class="btn-group pull-right"> 
											  	      <a href="#" class="btn btn-info" id="item_plus_button{{$key}}"><i class="fa fa-plus"></i></a>
											        </div> -->
		                                    	</td>
		                                      </tr>
		                                    	@endforeach
		                                    @endforeach
                                        </tbody>
                                    </table>
		                            </div>	
		                            <?php } ?>                           
		                          </div>