@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Lead Estimate
@endsection

<meta name="csrf-token" content="{{ csrf_token() }}">
<script>
	
$('#myModal').modal('show');


/*function submitPageSixForm(type,leadstatus) {

	if(document.getElementById("comment").value==""){
      alert("Please enter comments");
	  return false;

	}
	var dataString={};
	dataString['event_source_id']= document.getElementById("event_source_id").value;
	dataString['lead_id']= document.getElementById("lead_id").value;
	dataString['authenticity_token']= document.getElementById("authenticity_token").value;
	dataString['event_contact_person_id']= document.getElementById("event_contact_person_id").value;
	dataString['client_interest_status_id']= document.getElementById("client_interest_status_id").value;
	dataString['comment']= document.getElementById("comment").value;
	dataString['lead_status']=leadstatus;
	dataString['lead_type']=type;

	$.ajax({
                type : 'POST',
                url : 'admin/leadupdate',
                data: dataString,
                headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
					console.log(data);return false;
                  //fleetsData=[];          
                    //alert("Data Insert SuccessFully");
                    //if(data.success)
                      //alert("Data Insert SuccessFully");
                      //$("#result").empty();
                     // fleetsData=data; 

                }
  });
  };*/
	

</script>
<style>

.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 150% !important;
}
.modal-backdrop.show {
    opacity: 0.5;
}
.modal-backdrop.fade {
    opacity: 0;
}
.modal-backdrop {
  position: inherit !important;
    top: 0;
    left: 0;
    z-index: 1050;
    width: 100vw;
    height: 100vh;
    background-color: #22292f;
}
.modal .modal-header {
    background-color: #ffffff !important;
}
</style>
 
<div class="btn-group pull-right">
            <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Save
</button>
          </div>
		  <!-----Model Start ---->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Event            Next Follow-up Date: {{$lead['approximate_move_date']}}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <hr/>
      <div class="modal-body">
	  <form class="new_event" id="myform"  name="myform"  method="post" enctype='multipart/form-data'>
		  <input type="hidden" name="authenticity_token" id="authenticity_token" value="{{ csrf_token() }}">
		  <input type="hidden" name="lead_id" id="lead_id" value="{{$lead['id']}}">
		  <input type="hidden" name="leadtype" id="leadtype" value="leadupdate">
<fieldset>
<div class="form-group row">
<div class="col-sm-4">
<label class="col-form-label text-sm-right" for="event_source_id">Source:</label>
</div>
<div class="col-sm-6">
<select class="custom-select" name="event_source_id" id="event_source_id">
	<option value="1">Phone Inbound: Spoke With</option>
<option value="2">Phone Outbound: Spoke With</option>
<option value="3">Phone Outbound: Voicemail</option>
<option value="4">Email</option>
<option value="5">Fax</option>
<option value="6">Letter</option>
<option value="7">Walk In</option>
<option value="8">Scheduled Meeting</option>
<option value="9">Reviewing Only</option>
<option value="10">Damage Call</option>
<option value="11">Other</option></select>
</div>
</div>
<div class="form-group row">
<div class="col-sm-4">
<label class="col-form-label text-sm-right" for="event_contact_person_id">Person:</label>
</div>
<div class="col-sm-6">
<select class="custom-select" name="event_contact_person_id" id="event_contact_person_id"><option value="{{$lead['id']}}">{{$lead['name']}}</option></select>
</div>
</div>
<div class="form-group row">
<div class="col-sm-4">
<label class="col-form-label text-sm-right" for="interest_status">Interest Status:</label>
</div>
<div class="col-sm-6">
<select name="client_interest_status_id" id="client_interest_status_id" onchange="interest_status();" required="required" class="custom-select"><option value="1011">Hot</option>
<option value="1012">Warm</option>
<option selected="selected" value="1013">Cold</option>
<option value="1014">Not Interested</option></select>
</div>
</div>
<!--<div class="form-group row">
<div class="col-sm-4">
<label class="col-form-label text-sm-right" for="event_next_contact_date">Follow-up Override:</label>
</div>
<div class="col-sm-6 input">
<label class="custom-control custom-checkbox ">
<input type="checkbox" class="custom-control-input is-valid" id="next_contact_date_checkbox">
<span class="custom-control-label "><input class="form-control flatpickr-input" readonly="readonly" style="display:none" type="hidden" name="next_contact_date" id="next_contact_date"><input class="form-control flatpickr-input form-control input" placeholder="" tabindex="0" type="text" readonly="readonly"></span>
</label>
</div>
</div>-->

<div class="form-group row">
<div class="col-sm-4">
<label class="col-form-label text-sm-right" for="event_comment">Comment:</label>
</div>
<div class="col-sm-7">
<textarea class="form-control" required="required" rows="5" cols="60" name="comment" id="comment"></textarea>
</div>
</div>
</fieldset>
<br>
<div style="margin-left: 5%;">
<div class="row">

<div class="col-auto">
<button name="button" type="submit" id="scheduled_btn"  value="2" class="btn btn-success new-evt-button" >Schedule Job</button>
</div>

<div class="col-auto">

<button name="button" type="submit" id="pending_btn" value="3" class="btn btn btn-primary new-evt-button"  >Pending Job</button>
</div>

<div class="col-auto">
<button name="button" type="submit" id="not_interested" value="4" class="btn btn btn-danger new-evt-button" >Not Interested</button>
</div>
<div class="col-auto">
<button name="button" type="submit" id="dead_lead" value="5" class="btn btn btn-danger">Dead Lead</button>
</div>
<div class="col-auto"></div>
</div>
</div>
<!--<div id="serviceDatesDialog" title="Select Date for Change" style="display: none;">
<div id="processing-indicator" style="margin-left: 16%; margin-top: 10%; position: fixed;display: none;">
<img id="processing-image" src="/assets/processing_image-813199184b405177a49d7c73fc9f30a6b9fd2813edc519cc1e2a8660ecb324fd.gif">
</div>
<div class="container-fluid text-center">
<button name="button" type="submit" class="btn btn-success" onclick="changeDates();">save</button>
</div>
</div>-->

</form>
      </div>
     <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>-->
    </div>
  </div>
</div>
  <!-----Model End ---->
  
<div class="row">


	
			                           <table class="table table-bordered table-striped">
			                           	<thead>
			                           		<th>Last Edited</th>
			                           		<th>Move Date</th>
			                           		<th>Scheduled</th>
			                           		<th>Booking</th>
			                           		<th>Arrival Time</th>
			                           	</thead>
							            <tbody>
							            	<tr>
							            		<td>{{$lead['updated_at']}}</td>
							            		<td>{{$lead['approximate_move_date']}}</td>
							            		<td>N/A</td>
							            		<td>N/A</td>
							            		<td>N/A</td>
							                </tr>    
							                            	
							            </tbody>
							        </table>
								</div>
				
								<div class=endsection"row">
									<h3>LOGISTICS - {{$lead['type_of_service']['name']}}</h3>

									<table class="table table-bordered table-striped">
			                           	<tbody>
							            	<tr>
							            		<td style="background-color: #26B4FF; color: white;">ORIGIN</td>
							            		<td>{{$lead['origin_address']}} <br> TYPE: {{$lead['origin_property_type_id']['name']}} <br>
							            		<?php if(count($lead['origin_floor']) > 0) { $ofloors =
							            		implode(", ",$lead['origin_floor']) . ' STAIRS';  } else { $ofloors = 'N/A'; } ?>

							            		FLOOR: {{$ofloors}}
							            	</td>
							            	
							            		<td style="background-color: #26B4FF; color: white;">STOPS</td>
							            		<td>NO EXTRA STOPS.</td>
							            		<td style="background-color: #26B4FF;color: white;">DESTIN</td>
							            		<td>{{$lead['destination_address']}} <br> TYPE: {{$lead['destination_property_type_id']['name']}} <br>
							            		<?php if(count($lead['destination_floor']) > 0) { $dfloors =
							            		implode(", ",$lead['destination_floor']) . ' STAIRS';  } else { $dfloors = 'N/A'; } ?>

							            		FLOOR: {{$dfloors}}</td>
							                </tr>    
							                            	
							            </tbody>
							        </table>
								</div>
								<div class="row">
			                           <table class="table table-bordered table-striped">
			                           	<thead>
			                           		<th>SERVICES QUOTED</th>
			                           		<th>MOVING</th>
			                           		<th>PRICING</th>
			                           	</thead>
							            <tbody>
							            	<tr>
							            	   <td>SqFt : {{$lead['square_footage']}} <br> 
							            		Weight lbs : {{$lead['total_weight']}} <br>
							            		CuFt : {{$lead['total_cubic_foot']}} <br>
							            		Miles : {{$lead['distance']}}
							            	</td>
							            		<td>MOVE SIZE : {{$lead['move_size_id']['name']}}</td>
							            		<td>
							            		</td>
							            	</tr>
							            	<tr>
							            		<td>
							            			@foreach($metadata['packing_services'] as $kk=>$packingservices)
							            			<input type="checkbox" id="packing_services{{$packingservices['id']}}" name="moving_information[packing_services][]" value="{{$packingservices['id']}}">
          											<label for="packing_services{{$packingservices['id']}}">{{$packingservices['name']}} (<span>0.00</span>)</label><br>
												    @endforeach

												    <hr>
												    @foreach($metadata['packing_quotes'] as $kk=>$quote)
							            			<div class="form-check">
													  <input type="radio" class="form-check-input" id="radio{{$kk}}" name="optradio" value="{{$quote['id']}}"> {{$quote['name']}} (<span>0.00</span>)
													  <label class="form-check-label" for="radio{{$kk}}"></label>
													</div>
												    @endforeach

												    <hr>
												    @foreach($metadata['packing_materials'] as $kk=>$material)
							            			<input type="checkbox" id="packing_materials{{$material['id']}}" name="moving_information[packing_materials][]" value="{{$material['id']}}">
          											<label for="packing_materials{{$material['id']}}">{{$material['name']}} (<span>0.00</span>)</label><br>
												    @endforeach

												</td>
							            		<td>
							            		<?php if(count($lead['category_wise_items']) > 0){?>
							            			@foreach($lead['category_wise_items'] as $key=>$category)
						                            	<h4>{{$category['category_name']}}</h4>
						                            	@foreach($category['items'] as $ke=>$item)
						                            	<p>({{$item['quantity']}}) {{$item['name']}} <br></p>
						                            	@endforeach   
						                            @endforeach   
						                           <?php } ?> 
							            		</td>
							            		<td>
							            			{{ Form::ahNumber('trucks','TRUCKS :',1,array('min'=>'0','maxlength' => '12','max'=>'999999999999'))  }}
							            			{{ Form::ahNumber('crew_size','CREW SIZE :',4,array('min'=>'0','maxlength' => '12','max'=>'999999999999'))  }} 
							            			{{ Form::ahNumber('hourly_rate','HOURLY RATE :',400.00,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }}
							            			{{ Form::ahNumber('min_hours','MIN HOURS :',3.5,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }}
							            			{{ Form::ahNumber('max_hours','MAX HOURS :',3.5,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }} 
							            			{{ Form::ahNumber('est_hours','EST HOURS :',9.0,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }}
							            			{{ Form::ahNumber('moving_hours','MOVING HOURS :',8.0,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }} 
							            			{{ Form::ahNumber('travel_time','TRAVEL TIME :',1.0,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }}
							            			{{ Form::ahNumber('travel_time','TRAVEL TIME :',1.0,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }}
							            			{{ Form::ahNumber('travel_fee','TRAVEL FEE :',0.0,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }}
							            			{{ Form::ahNumber('moving_fee','MOVING FEE :',0.0,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }} 
							            			{{ Form::ahNumber('packing_material','PACKING MATERIALS :',0.0,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }} 
							            			{{ Form::ahNumber('packing_crew_size','PACKING CREW SIZE :',0.0,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }}
							            			{{ Form::ahNumber('packing_hours','PACKING HOURS :',0.0,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }} 
							            			{{ Form::ahNumber('packing_fee','PACKING FEE :',0.0,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }}
							            			{{ Form::ahNumber('fuel_surcharge','FUEL SURCHARGE :',0.0,array('min'=>'0','maxlength' => '12','max'=>'999999999999','step' => '0.01'))  }} 
							            		</td>
							                </tr>    
							                <tr>
							                	<td></td>
							                	<td>TOTAL ESTIMATED BOXES/TOTES : {{$lead['estimated_boxes']}}</td>
							                	<td>
							                		<div class="form-group" style="margin:5px">
													   <label for="total_price_range" class="control-label col-sm-4">TOTAL ESTIMATED PRICE :</label>
													   <div class="col-sm-8">
													    <input class="form-control" min="0" maxlength="12" max="999999999999" name="total_price_range" type="number" value="{{$lead['estimated_boxes']}}" id="total_price_range">
													   </div>
													</div>
							                	    <div class="form-group" style="margin:5px">
													   <label for="est_price_range" class="control-label col-sm-4">EST PRICE RANGE :</label>
													   <div class="col-sm-4">
													    <input class="form-control" min="0" maxlength="12" max="999999999999" name="est_price_range_one" type="number" value="0" id="est_price_range_one">    
													   </div>
													   <div class="col-sm-4">
													   	<input class="form-control" min="0" maxlength="12" max="999999999999" name="est_price_range_two" type="number" value="0" id="est_price_range_two">
													   </div>
													</div>
												</td>
							                </tr>            	
							            </tbody>
							        </table>
								</div>
								
