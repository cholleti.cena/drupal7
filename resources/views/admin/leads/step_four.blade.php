<div class="row">
			                           <table class="table table-bordered table-striped">
							            <tbody id='myForm'>
							            	<input type="hidden" name="moving_information[packing_requirement_id]" id="packing_requirement_id" value="{{$lead['packing_requirement_id']}}">
							            	<tr>
							            		@foreach($metadata['packing_requirements'] as $k=> $packing)
							                  	<td width="35%">
							                  	  	<div class="form-check">
													  <input type="radio" class="form-check-input" id="radio{{$k}}" name="optradio" value="{{$packing['id']}}" <?php 
								                      $res = $packing['id'];
								                      $db_res = $lead['packing_requirement_id'];
								                      if($res == $db_res) echo 'checked' ?>> {{$packing['name']}}
													  <label class="form-check-label" for="radio{{$k}}"></label>
													</div>
							                    </td>
							                    @endforeach   
							                </tr>    
							                            	
							            </tbody>
							        </table>
								</div>
								<?php if(count($lead['leadsMaterials']) > 0) {?>
								<div class="row packing_address_sec">
							        <div class="panel-body">
							      	  <table class="table table-bordered table-striped">
							            <thead>
										  <tr>
										   	<th>MATERIAL</th>
										   	<th>Total QTY</th>
										   	<th>Pack QTY</th>
										   	<th>Unpack QTY</th>
										  </tr>
										</thead>
							            <tbody>
							            	@foreach($lead['leadsMaterials'] as $ke=>$item)
							            	<tr>
							                  	<td width="35%">
							                  	  <div class="form-group" style="margin:5px">
							                  	  	<div class="col-sm-10">
							                  	  		<input type="hidden" name="moving_information[materials][packing_material_id]" value="{{$item['material_id']['id']}}">
							                  	  		<input type="hidden" name="moving_information[materials][packing_material_name]" value="{{$item['material_id']['name']}}">
							                  	  		{{$item['material_id']['name']}}
							                  	  	</div>
							                  	  </div>
							                    </td>
							                    <td>
							                      <div class="form-group" style="margin:5px">
							                      	<div class="col-sm-10">
												    	<input type="number" name="moving_information[materials][packing_quantity]" class="form-control" id="packing_requirement_quantity{{$ke}}" value="{{$item['quantity']}}" min="1" step="1" disabled>
													</div>
												  </div>
							                    </td>
							                    <td>
							                      <div class="form-group" style="margin:5px">
							                      	<div class="col-sm-10">
												    	<input type="number" name="moving_information[materials][pack_quantity]" class="form-control" id="packing_requirement_pack_quantity{{$ke}}" value="1" min="1" step="0.01">    	
													</div>
												  </div>
							                    </td>
							                    <td>
							                      <div class="form-group" style="margin:5px">
							                      	<div class="col-sm-10">
												    	<input type="number" name="moving_information[$ke][materials][unpack_quantity]" class="form-control" id="packing_requirement_unpack_quantity{{$ke}}" value="1" min="1" step="0.01">    	
													</div>
												  </div>
							                    </td>
							                </tr>    
							                @endforeach               	
							            </tbody>
							        </table>
							       </div>
						 		</div>
						 			<?php } ?> 