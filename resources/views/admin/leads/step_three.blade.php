<div class="row">
							  
							      <div class="panel-heading ui-draggable-handle">
							        <div class="btn-group pull-right"> 
							  	      <a href="#" class="btn btn-info" id="appendAddressRow"><i class="fa fa-edit"></i>Add Materials</a>
							        </div>                                  
							      </div> 
							      <div>
							      	<div class="panel-body">
							      	  <table class="table table-bordered table-striped">
							            <thead>
										  <tr>
										   	<th>MATERIAL</th>
										   	<th>QTY</th>
										   	<th>PRICE</th>
										  </tr>
										</thead>
							            <tbody id='TextBoxesGroup'>
							            	<?php if(count($lead['leadsMaterials']) > 0) {?>
							            	@foreach($lead['leadsMaterials'] as $ke=>$item)
							            	<tr class="address_sec_{{$ke}} suppliersaddress{{$ke}}">
							                  	<td width="35%">
							                  	  <div class="form-group" style="margin:5px"><div class="col-sm-10"><select class="form-control" data-live-search='true' id="material_id{{$ke}}" name="moving_information[material_id][]">
								                      @foreach($metadata['materials'] as $material)
								                      <option value="{{$material['id']}}" <?php 
								                      $res = $material['id'];
								                      $db_res = $item['material_id']['id'];
								                      if($res == $db_res) echo 'selected="selected"' ?>>{{$material['name']}} - {{$material['price']}}</option>
								                    @endforeach
								                  </select></div></div>
							                    </td>
							                    <td>
							                      <div class="form-group" style="margin:5px">
							                      	<div class="btn-group pull-left"> 
											  	      <a href="#" class="btn btn-danger" id="minus_button{{$ke}}"><i class="fa fa-minus"></i></a>
											        </div>  
							                      	<div class="col-sm-6">
												    	<input type="number" name="moving_information[quantity][]" class="form-control" id="quantityy{{$ke}}" value="{{$item['quantity']}}" min="1" step="1">
													</div>
													<div class="btn-group pull-right"> 
											  	      <a href="#" class="btn btn-info" id="plus_button{{$ke}}"><i class="fa fa-plus"></i></a>
											        </div>  
												  </div>
							                    </td>
							                    <td>
							                      <div class="form-group" style="margin:5px">
							                      	<input type="hidden" name="moving_information[original_price][]" id="original_price{{$ke}}" value="0">
													<div class="col-sm-10">
												    	<input type="number" name="moving_information[price][]" class="form-control" id="price{{$ke}}" value="{{$item['price']}}" min="1" step="0.01">
													</div>
												  </div>
							                    </td>
							                </tr>    
							                @endforeach    
							                <?php } ?>              	
							            </tbody>
							        </table>
							       </div>	
							       					       
							  </div>
						 </div>