@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Leads
@endsection
@section('module')
Leads
@endsection

@section('content')
@include('admin.components.message')	

<div class="content-wrapper container-xxl p-0">
        <div class="content-header row"></div>
        <div class="content-body">
          <section id="dashboard-ecommerce">
            <div class="">
              
                                <div class="table-bar2">
                                    <table id="example" class="table table-striped" style="width:100%">
                                    <thead class="thead">
                                            <tr>
                                                <th>Lead No</th>
                                                <th>Referred By</th>
                                                <th>Customer Name</th>
                                                <th>Phone No</th>
                                                <th>Alter Phone No</th>
                                                <th>Move Date</th>
                                                <th>Status</th>
                                                <th>Received Date</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            @foreach($leads as $lead)
                                    <tr>
                                        <td>
                                            {{$lead->lead_id}}
                                        </td> 
                                        <td>
                                            {{$lead->referred_by}}
                                        </td>
                                        <td>
                                            {{$lead->name}}
                                        </td> 
                                        <td>
                                            {{$lead->phone}}
                                        </td>
                                        <td>
                                            {{$lead->alter_phone}}
                                        </td> 
                                        <td>
                                            {{$lead->approximate_move_date}}
                                        </td>   
                                        <td>
                                            {{$lead->lead_status}}
                                        </td> 
                                        <td>
                                            {{$lead->created_at}}
                                        </td>                                        
                                        <td width="15%">
                                            <div >
                                                <div style="float:left;padding:0 10px 10px 0;">
                                                 @if($privileges['Edit']=='true')
                                                {{ link_to_route('leads.edit','Estimate',array($lead->lead_id), array('class' => 'btn btn-info')) }}
                                                @endif 
                                                </div>
                                                <div style="float:left;padding-right:10px;">
                                                   @if($privileges['Delete']=='true')
                                                    {{ Form::open(array('onsubmit' => 'return confirm("Are you sure you want to delete?")','method' => 'DELETE', 'route' => array('leads.destroy', $lead->lead_id))) }}
                                                    <button type="submit" class="btn btn-danger btn-xs pull-right" style="font-size: 11px;padding: 4px 12px;">Delete</button>
                                                    {{ Form::close() }}
                                                   @endif
                                                </div>
                                            </div>
                                        </td>
                                         </tr>
                                    @endforeach
                                        </tbody>
                                    </table>
                                    {!! $leads->render() !!}
                                </div>
                                
   
                
            </div>
          </section>
        </div>  
      </div>

@endsection