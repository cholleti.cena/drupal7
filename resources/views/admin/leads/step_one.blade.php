<div class="row" style="margin-top: 1%;">
                        <div class="col col-sm-6 col-lg-6">
                          <div class="form-group row">
                           
                            <label class="space col-form-label col "><strong>Service Date:</strong> <span class="dot">*</span></label>
                             <div class="col col-sm-12 col-lg-6">
                              <input class="form-control" readonly="" name="moving_information[approximate_move_date]" type="text" value="{{$lead['approximate_move_date']}}" id="approximate_move_date">
                            </div>
                          </div>
                          
                          <div class="form-group row">
                            
                            <label class=" space col-form-label col " for="estimate_type_of_service"><strong>Type of
                              Service:</strong><span class="dot">*</span></label>
                             <div class="col col-sm-12 col-lg-6">
                              <select class="form-control select" data-live-search='true' id="type_of_service" name="moving_information[type_of_service]">
							                      @foreach($metadata['servicetypes'] as $servicetype)
							                      <option value="{{$servicetype['id']}}" <?php 
							                      $res = $servicetype['id'];
							                      $db_res = $lead['type_of_service']['id'];
							                      if($res == $db_res) echo 'selected="selected"' ?>>{{$servicetype['name']}}</option>
							                    @endforeach
							                  </select>
                            </div>
                          </div>                          
                        </div>

                        <div class="col border-left col-sm-6 col-lg-6" style="height: 350px;">
                           <div id="floating-panel">
                              <b>Mode of Travel: </b>
                              <select id="mode">
                                <option value="DRIVING">Driving</option>
                                <option value="WALKING">Walking</option>
                                <option value="BICYCLING">Bicycling</option>
                                <option value="TRANSIT">Transit</option>
                              </select>
                            </div>
                            <div id="mappp"></div>

                        </div>
                      </div>
                      <hr class="btn-primary">
                      <div class="row">
                        <div class="col col-sm-12 col-lg-6">
                          <div class="form-group row">
                           
                            <label class="space col-form-label col " for="estimate_origin"><strong>Origin Address:</strong> <span class="dot">*</span></label>
                            <div class="col-lg-9 col-sm-9">
                              <input required="required" class="form-control address" type="text" value="{{$lead['origin_address']}}" name="moving_information[origin_address]"
                                id="pacinput">
                            </div>
                            <div id="map" style="display:none;"></div>
                                <div id="infowindow-content">
                                  <span id="place-name" class="title"></span><br />
                                  <span id="place-address"></span>
                                </div>
                          </div>

                          <input type="hidden" name="moving_information[origin_latitude]" id="origin_latitude" value="{{$lead['origin_latitude']}}">
                              <input type="hidden" name="moving_information[origin_longitude]" id="origin_longitude" value="{{$lead['origin_longitude']}}">
                          
                          <div class="form-group row">
                            
                            <label class="space col-form-label col col-sm-3 " for="estimate_origin_type"><strong>Origin:</strong><span class="dot">*</span></label>
                            <div class="form-group col-lg-3 col-sm-8 ml-2" style="padding: 0px;">
                              <select  style="margin-left: 10px; width: 97%;"  class="form-control select" data-live-search='true' id="origin_property_type_id" name="moving_information[origin_property_type_id]">
							                      @foreach($metadata['properties'] as $property)
							                      <option value="{{$property['id']}}" <?php 
							                      $res = $property['id'];
							                      $db_res = $lead['origin_property_type_id']['id'];
							                      if($res == $db_res) echo 'selected="selected"' ?>>{{$property['name']}}</option>
							                    @endforeach
							                  </select>
                            </div>
                            <?php 
                                if(count($lead['origin_floor']) > 0)
                                {
                                  $origin_floor = implode(',',$lead['origin_floor']);
                                }
                                else  
                                {
                                  $origin_floor = 0;
                                }
                            ?>
                            <div class="form-group col-auto" id="origin_floor">
                            <input type="text" class="form-control input" name="moving_information[origin_floor][]"  value="{{$origin_floor}}" id="origin_floor" />                              
                            </div>
                            {{ Form::ahSwitch('origin_elevator','Elevator :',null,$lead['origin_elevator']) }}
                          </div>
                          <div class="form-group row">
                            
                            <label class="space col-form-label col" for="estimate_origin_truck_distance"><strong>Origin Truck
                              Distance:</strong><span class="dot">*</span></label>
                            <div class="col-sm-9">
                              <select class="form-control custom-select"name="moving_information[origin_truck_distance_id]"
                                id="estimate_origin_truck_distance_id">
                                <option selected="selected" value="2621">Less than 50 Feet</option>
                                <option value="2622">51 - 100 Feet</option>
                                <option value="2623">101 - 200 Feet</option>
                                <option value="2624">201 - 300 Feet</option>
                                <option value="2625">301 - 400 Feet</option>
                                <option value="2626">401 - 500 Feet</option>
                                <option value="2627">501 - 600 Feet</option>
                                <option value="2628">601 - 700 Feet</option>
                                <option value="2629">701 - 800 Feet</option>
                                <option value="2630">801 - 900 Feet</option>
                                <option value="2631">901 - 1000 Feet</option>
                              </select>
                            </div>                                          
                            
                          </div>
                          
                          <div class="form-group row">
                             <label class="space col-form-label col" for="estimate_distance"><strong>
                              Distance:</strong><span class="dot">*</span></label>
                            <div class="col-sm-9">
                             <input type="text" class="form-control input" name="moving_information[distance]"  value="" id="distance" />
                            </div>                             
                          </div>
                          
                           <div class="form-group row">
                             <label class="space col-form-label col" for="estimate_duration"><strong>
                              Duration:</strong><span class="dot">*</span></label>
                            <div class="col-sm-9">
                             <input type="text" class="form-control input" name="moving_information[duration]"  value="" id="duration" />
                             <input type="hidden" class="form-control input" name="moving_information[final_duration]"  value="" id="final_duration" />
                            </div>                             
                          </div>
                          
                        </div>
                        <div class="col border-left col-sm-12 col-lg-6">
                          <div class="form-group row">
                            
                            <label class="space col-form-label col" for="estimate_destination"><strong>Destination
                              Address:</strong><span class="dot">*</span></label>
                            <div class="col-lg-9 col-sm-9">
                              <input required="required" class="form-control address" onchange="disableMoveItSizeBtn();"
                                type="text" value="{{$lead['destination_address']}}" name="moving_information[destination_address]"
                                id="paccinput">
                                <div id="mapp" style="display:none;"></div>
                                <div id="infowindoww-content">
                                  <span id="placee-name" class="title"></span><br />
                                  <span id="placee-address"></span>
                                </div>
                            </div>
                          </div>
                          <div class="form-group row">
                           
                            <label class="space col-form-label col col-sm-3 "
                              for="estimate_destination_type"><strong>Destination:</strong> <span class="dot">*</span></label>
                            <div class="form-group col-lg-3 col-sm-8 ml-2" style="padding: 0px; width:100%;">
                              <div class="col" style="padding:0px;"> 
                              	<select class="form-control select" style="margin-left: 10px;width: 97%;" data-live-search='true' id="destination_property_type_id" name="moving_information[destination_property_type_id]">
							                      @foreach($metadata['properties'] as $property)
							                      <option value="{{$property['id']}}" <?php 
							                      $res = $property['id'];
							                      $db_res = $lead['destination_property_type_id']['id'];
							                      if($res == $db_res) echo 'selected="selected"' ?>>{{$property['name']}}</option>
							                    @endforeach
							                  </select>
                            	</div>
                              <input type="hidden" name="moving_information[destination_latitude]" id="destination_latitude" value="{{$lead['destination_latitude']}}">
                              <input type="hidden" name="moving_information[destination_longitude]" id="destination_longitude" value="{{$lead['destination_longitude']}}">
                            </div>
                            <?php 
                                if(count($lead['destination_floor']) > 0)
                                {
                                  $destination_floor = implode(',',$lead['destination_floor']);
                                }
                                else  
                                {
                                  $destination_floor = 0;
                                }
                            ?>
                            <div class="floor_div form-group col-auto" id="destination_floor">
                              <div class="col" style="padding:0px;">
                                  <input type="text" class="form-control input" name="moving_information[destination_floor][]" value="{{$destination_floor}}" id="destination_floor" />
                              </div>
                            </div>
                            {{ Form::ahSwitch('destination_elevator','Elevator :',null,$lead['destination_elevator']) }}
                          </div>
                          <div class="form-group row">
                            
                            <label class="space col-form-label col "
                              for="estimate_destination_truck_distance"><strong>Destination Truck Distance:</strong><span class="dot">*</span></label>
                            <div class="col-sm-9" >
                              <select class="form-control custom-select" name="moving_information[destination_truck_distance_id]"
                                id="estimate_destination_truck_distance_id">
                                <option selected="selected" value="2621">Less than 50 Feet</option>
                                <option value="2622">51 - 100 Feet</option>
                                <option value="2623">101 - 200 Feet</option>
                                <option value="2624">201 - 300 Feet</option>
                                <option value="2625">301 - 400 Feet</option>
                                <option value="2626">401 - 500 Feet</option>
                                <option value="2627">501 - 600 Feet</option>
                                <option value="2628">601 - 700 Feet</option>
                                <option value="2629">701 - 800 Feet</option>
                                <option value="2630">801 - 900 Feet</option>
                                <option value="2631">901 - 1000 Feet</option>
                              </select>
                            </div>
                          </div>
                          
                        </div>
                      </div>


<style>
  /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
#map {
  height: 100%;
}

/* Optional: Makes the sample page fill the window. */
html,
body {
  height: 100%;
  margin: 0;
  padding: 0;
}

#description {
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
}

#infowindow-content .title {
  font-weight: bold;
}

#infowindoww-content .title {
  font-weight: bold;
}

#infowindow-content {
  display: none;
}

#infowindoww-content {
  display: none;
}

#map #infowindow-content {
  display: inline;
}

#mapp #infowindow-content {
  display: inline;
}

.pac-card {
  background-color: #fff;
  border: 0;
  border-radius: 2px;
  box-shadow: 0 1px 4px -1px rgba(0, 0, 0, 0.3);
  margin: 10px;
  padding: 0 0.5em;
  font: 400 18px Roboto, Arial, sans-serif;
  overflow: hidden;
  font-family: Roboto;
  padding: 0;
}

#pac-container {
  padding-bottom: 12px;
  margin-right: 12px;
}

.pac-controls {
  display: inline-block;
  padding: 5px 11px;
}

.pac-controls label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
}

#pacinput:focus {
  border-color: #4d90fe;
}

#paccinput:focus {
  border-color: #4d90fe;
}

#title {
  color: #fff;
  background-color: #4d90fe;
  font-size: 25px;
  font-weight: 500;
  padding: 6px 12px;
}

#floating-panel {
  position: absolute;
  top: 10px;
  left: 25%;
  z-index: 5;
  background-color: #fff;
  padding: 5px;
  border: 1px solid #999;
  text-align: center;
  font-family: "Roboto", "sans-serif";
  line-height: 30px;
  padding-left: 10px;
}

#mappp {
  height: 100%;
}


  </style>
<script>
function initMap() {
  const map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 40.749933, lng: -73.98633 },
    zoom: 13,
    mapTypeControl: false,
  });
  
  const input = document.getElementById("pacinput");
  const biasInputElement = document.getElementById("use-location-bias");
  const strictBoundsInputElement = document.getElementById("use-strict-bounds");
  const options = {
    fields: ["formatted_address", "geometry", "name"],
    strictBounds: false,
    types: ["establishment"],
  };

  const autocomplete = new google.maps.places.Autocomplete(input, options);
  autocomplete.bindTo("bounds", map);

  const infowindow = new google.maps.InfoWindow();
  const infowindowContent = document.getElementById("infowindow-content");
  infowindow.setContent(infowindowContent);
  const marker = new google.maps.Marker({
    map,
    anchorPoint: new google.maps.Point(0, -29),
  });
   placeChangeEvent(autocomplete, 'ORIG');
  /*autocomplete.addListener("place_changed", () => {
    infowindow.close();
    marker.setVisible(false);
    const place = autocomplete.getPlace();
    if (!place.geometry || !place.geometry.location) {
      window.alert("No details available for input: '" + place.name + "'");
      return;
    }
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);
    }
    //console.log(place.geometry.location.lat()+' - '+place.geometry.location.lng());
    $('#origin_latitude').val(place.geometry.location.lat());
    $('#origin_longitude').val(place.geometry.location.lng());
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);
    infowindowContent.children["place-name"].textContent = place.name;
    infowindowContent.children["place-address"].textContent =
      place.formatted_address;
    infowindow.open(map, marker);
  });*/

//desti
  const mapp = new google.maps.Map(document.getElementById("mapp"), {
    center: { lat: 40.749933, lng: -73.98633 },
    zoom: 13,
    mapTypeControl: false,
  });
  
  const inputp = document.getElementById("paccinput");
  const biasInputElementp = document.getElementById("use-location-bias");
  const strictBoundsInputElementp = document.getElementById("use-strict-bounds");
  const optionss = {
    fields: ["formatted_address", "geometry", "name"],
    strictBounds: false,
    types: ["establishment"],
  };

  const autocompletee = new google.maps.places.Autocomplete(inputp, options);
  autocompletee.bindTo("bounds", mapp);

  const infowindoww = new google.maps.InfoWindow();
  const infowindowwContent = document.getElementById("infowindoww-content");
  infowindoww.setContent(infowindowwContent);
  const markerr = new google.maps.Marker({
    mapp,
    anchorPoint: new google.maps.Point(0, -29),
  });
     placeChangeEvent(autocompletee, 'DEST');
  /*autocompletee.addListener("place_changed", () => {
    infowindoww.close();
    markerr.setVisible(false);
    const placee = autocompletee.getPlace();
    if (!placee.geometry || !placee.geometry.location) {
      window.alert("No details available for input: '" + placee.name + "'");
      return;
    }
    if (placee.geometry.viewport) {
      mapp.fitBounds(placee.geometry.viewport);
    } else {
      mapp.setCenter(placee.geometry.location);
      mapp.setZoom(17);
    }
    //console.log(placee.geometry.location.lat()+' - '+placee.geometry.location.lng());
    $('#destination_latitude').val(placee.geometry.location.lat());
    $('#destination_longitude').val(placee.geometry.location.lng());
    markerr.setPosition(placee.geometry.location);
    markerr.setVisible(true);
    infowindowwContent.children["placee-name"].textContent = placee.name;
    infowindowwContent.children["placee-address"].textContent =
      placee.formatted_address;
    infowindow.open(mapp, marker);
  });*/
  

//map
  //const directionsRenderer = new google.maps.DirectionsRenderer();
  //const directionsService = new google.maps.DirectionsService();
  const mappp = new google.maps.Map(document.getElementById("mappp"), {
    zoom: 14,
    center: { lat: <?php echo $lead['origin_latitude'];?>, lng: <?php echo $lead['origin_longitude'];?> },
  });

  
}

function placeChangeEvent(autocomplete, mode) {
    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        var palcetype = place.types;
        if(mode == 'ORIG'){
            var org_city_locality = ['locality'];
            var  org_check_locality = check_google_types(org_city_locality, palcetype);
            var origin_data=  updateAddressDetails(place, mode);
           // console.log(origin_data);
        }
        if(mode == 'DEST'){
            var des_city_locality = ['locality'];
            var des_check_locality = check_google_types(des_city_locality, palcetype);
            var dest_data=  updateAddressDetails(place, mode);
        }
    });    
}
var destinationPlaceDetails  = [];
function updateAddressDetails(place, mode) {
    var addressData = {
    place_id: '',    
    name: '',
    latitude: '',
    longitude: '',
    formattedAddress: ''
    }
    
    addressData.place_id = place.place_id;
    addressData.name = place.name;
    addressData.latitude = place.geometry.location.lat();
    addressData.longitude = place.geometry.location.lng();   
    //console.log(); 
    addressData.formattedAddress = place.formatted_address;
    if (mode === 'ORIG') {              
        if(addressData.place_id !='' && addressData.latitude !='' && addressData.longitude !=''){
            originPlaceDetails = addressData;            
            document.getElementById('pacinput').value = addressData.name;            
            document.getElementById('place-name').value = addressData.name;            
            document.getElementById('place-address').value = addressData.formattedAddress;
            document.getElementById('origin_latitude').value = addressData.latitude;
            document.getElementById('origin_longitude').value = addressData.longitude;
        
        }
    }else if (mode === 'DEST') {
        if(addressData.place_id !='' && addressData.latitude !='' && addressData.longitude !=''){
            destinationPlaceDetails = addressData;                         
            document.getElementById('paccinput').value = addressData.name;
            document.getElementById('placee-name').value = addressData.name;
            document.getElementById('placee-address').value = addressData.formattedAddress;
            document.getElementById('destination_latitude').value = addressData.latitude;
            document.getElementById('destination_longitude').value = addressData.longitude;
            
          if(document.getElementById("place-name").value!='' && document.getElementById("placee-name").value!=''){  
            getdistance();
		  }
        }
    }

}

function check_google_types(needles, haystack) {
    for (var i = 0, len = needles.length; i < len; i++) {
        if (needles[i].indexOf(haystack, -1))
            return true;
    }
    return false;
}

function getdistance() {  
           var source = document.getElementById("place-name").value;  
           var destination = document.getElementById("placee-name").value;  
         // console.log(source);
          //console.log(destination);
           var service = new google.maps.DistanceMatrixService();  
           service.getDistanceMatrix({  
               origins: [source],  
               destinations: [destination],  
               travelMode: google.maps.TravelMode.DRIVING,  
               unitSystem: google.maps.UnitSystem.METRIC,  
               avoidHighways: false,  
               avoidTolls: false  
           }, function (response, status) {  
               if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {  
				  console.log(response);
                   var distance = response.rows[0].elements[0].distance.text;  
                   var duration = response.rows[0].elements[0].duration.text;                     
                   if(duration){
						var rem=duration.replace(" hours", "");
						var mins=rem.replace(" ", ".");
						var finaldt=mins.replace(" mins", "");
					    document.getElementById('final_duration').value = finaldt;  
				   }                   
                   //var lbl_distance = "Distance: " + distance;  
                  // var lbl_duration = "Duration: " + duration;  
                   //document.getElementById('lbl_distance').innerHTML = distance;  
                   document.getElementById('distance').value = distance;  
                  // document.getElementById('lbl_duration').innerHTML = duration;                      
                   document.getElementById('duration').value = duration;                      
  
       }  
       else {  
                  alert("Unable to calculate distance.");  
               }  
           });  
} 


</script>

   
