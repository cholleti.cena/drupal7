@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Lead Estimate
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahReadonly', 'admin.components.form.readonly', ['name', 'labeltext'=>null, 'value' => null])}}
{{Form::component('ahSwitch', 'admin.components.form.switch', ['name', 'labeltext'=>null, 'value' => null, 'checkstatus' => false, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return UserEditValidate();','method' => 'PUT', 'route' => array('leads.update',$lead['id']),'files'=>true)) }}


<style rel="stylesheet" type="text/less">
/** Colors **/
@darkGreen: #4a6a28;
@lightGreen: #87ba51;
@greyState: #DDD;
@mainBlue: #277cbb;

ol {
  
  &.progress-track {
    display: table;
    list-style-type: none;
    margin: 0;
    padding: 2em 1em;
    table-layout: fixed;
    width: 100%;    
    
    li {
      display: table-cell;
      /*line-height: 3em;*/
      position: relative;
      text-align: center;
      
      .icon-wrap {
        border-radius: 50%;
        top: -1.5em;
        color: #fff;
        display: block;
        height: 2.0em;
        margin: 0 auto -2em;
        left: 0;
        right: 0;        
        position: absolute;
        width: 2.0em;
      }
      
      .icon-check-mark, .icon-down-arrow {
        height: 25px;
        width: 15px;
        display: inline-block;
        fill: currentColor;        
      }         
      
      .progress-text {
         position: relative;
         top: 20px;
         font-size: 18px;
         color: #277cbb;
      }
      
      &.progress-done {
        border-top: 7px solid @lightGreen;
        transition: border-color 1s ease-in-out;
        -webkit-transition: border-color 1s ease-in-out;
        -moz-transition: border-color 1s ease-in-out;
        
        .icon-down-arrow {
          display: none;
        }        
        
          &.progress-current {

            .icon-wrap {
              background-color: @mainBlue;

              .icon-check-mark {
               display: none;
              }
              
              .icon-down-arrow {
                display: block;
              }               
            }        
          }                

        .icon-wrap {
          background-color: @darkGreen;
          border: 5px solid @lightGreen;     
        }                
      }
      
      &.progress-todo {
          border-top: 7px solid @greyState;
          color: black;        
        
        .icon-wrap {
          background-color: #FFF;
          border: 5px solid @greyState;
          border-radius: 50%;
          bottom: 1.5em;
          color: #fff;
          display: block;
          height: 2.0em;
          margin: 0 auto -2em;
          position: relative;
          width: 2.0em;

          .icon-check-mark, .icon-down-arrow {
              display: none;
          }      
        }        
      }            
    }    
  }
}
</style>

<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.2/less.js'></script>
<div class="content-wrapper container-xxl p-0">
      <div class="content-header row"></div>
      <div class="content-body">
        <section id="dashboard-ecommerce">

          <div class="match-height">
            <!-- Statistics Card -->

            <div class="card-statistics">
              <div class="card-header">
                  <!-- <h4 class="card-title" style="color: #000; font-family: inherit">
                    Leads
                  </h4> -->
              </div>

            </div>

            <!--/ Statistics Card -->
          </div>
          <div class="">
            <div class="" style="margin-top: 20px">
              <!--replace the content from here-->

              <div class="row">
                <div class="page-header col-6">
                  <div class="row">
                    <h3> New Estimate - {{$lead['id']}} Move Info</h3>
                  </div>

                </div>
              </div>

              <div class="title-header">
                <div id="estimate_viewers1">

                  <div id="currently_viewing_1394311" class="employee_74267" style="margin-right: 10px">

                  </div>
                </div>
                <h4 style="margin-bottom: 13px;">
                  <span>{{$lead['name']}}</span> - <span>{{$lead['created_at']}}</span>
                  <span style="margin-left: 1%"><i class="fas fa-pencil-alt icon-medium" style="color: #ffa500"
                      onclick="editInfo()"></i></span>

                </h4>
              </div>

              <div class="row">
                <div id="contact_info_div">
                  <style>
                    span.phone-number:hover {
                      text-decoration: underline;
                    }
                  </style>
                  <label><span title="Call" class="phone-number"> {{$lead['phone']}}
                      <span class="calling-text" id="calling-text_1401494"
                        style="color:rgb(2, 152, 252); display: none">Calling...</span></span> {{$lead['email']}}
                  </label>
                  <script>
                    function show_calling_text(id) {
                      $('#calling-text_' + id).show()
                    }
                  </script>

                </div>
                <div style="margin-top: -10px">
                </div>
              </div>

              <div id="add_client_tag" class="row" style="margin-left:1%; margin-bottom: 0.5%">
              </div>

              <!-- <div class="progress" style="margin-top: 0.1%;">
                <div class="progress-bar progress-bar bg-primary" style="width:30%">30% of estimate completed</div>
              </div> -->

              <div class="card mb-4">
                    <!-- Project Status will starts here -->
                <ol class="progress-track" id="progressTab">

                    <li class="pliTags progress-done progress-current" id="pid1">
                    <center>
                        <div class="icon-wrap">
                            <svg class="icon-state icon-check-mark">
                            <use xlink:href="#icon-check-mark"></use>
                            </svg>

                            <svg class="icon-state icon-down-arrow">
                            <use xlink:href="#icon-down-arrow"></use>
                            </svg>
                        </div>
                        <span class="progress-text" data-id="1"><a href="#">Move Info</a></span> 
                    </center>
                    </li>

                    <li  class="plitags progress-todo">
                    <center>
                        <div class="icon-wrap">
                            <svg class="icon-state icon-check-mark">
                            <use xlink:href="#icon-check-mark"></use>
                            </svg>

                            <svg class="icon-state icon-down-arrow">
                            <use xlink:href="#icon-down-arrow"></use>
                            </svg>
                        </div>
                        <span class="progress-text" data-id="2"><a href="#">Inventory</a></span>
                    </center>
                    </li>

                    <li  class="plitags progress-todo" id="pid3">
                    <center>
                        <div class="icon-wrap">
                            <svg class="icon-state icon-check-mark">
                            <use xlink:href="#icon-check-mark"></use>
                            </svg>

                            <svg class="icon-state icon-down-arrow">
                            <use xlink:href="#icon-down-arrow"></use>
                            </svg>
                        </div>
                        <span class="progress-text"  data-id="3"><a href="#">Materials</a></span>
                    </center>
                    </li>

                    <li  class="plitags progress-todo" id="pid4">
                    <center>
                        <div class="icon-wrap">
                            <svg class="icon-state icon-check-mark">
                            <use xlink:href="#icon-check-mark"></use>
                            </svg>

                            <svg class="icon-state icon-down-arrow">
                            <use xlink:href="#icon-down-arrow"></use>
                            </svg>
                        </div>
                        <span class="progress-text" data-id="4"><a href="#">Pack Labor</a></span>
                    </center>
                    </li>

                    <li  class="plitags progress-todo" id="pid5">
                    <center>
                        <div class="icon-wrap">
                            <svg class="icon-state icon-check-mark">
                            <use xlink:href="#icon-check-mark"></use>
                            </svg>

                            <svg class="icon-state icon-down-arrow">
                            <use xlink:href="#icon-down-arrow"></use>
                            </svg>
                        </div>
                        <span class="progress-text" data-id="5"><a href="#">Storage</a></span>
                    </center>
                    </li>
                    <li class="plitags progress-todo" id="pid6">
                    <center>
                        <div class="icon-wrap">
                            <svg class="icon-state icon-check-mark">
                            <use xlink:href="#icon-check-mark"></use>
                            </svg>

                            <svg class="icon-state icon-down-arrow">
                            <use xlink:href="#icon-down-arrow"></use>
                            </svg>
                        </div>
                        <span class="progress-text" data-id="6"><a href="#">Quote</a></span>
                    </center>
                    </li>
                </ol>
                    <!-- Ends Project Status -->
                <!-- <div class="container-fluid">
                  <div class="row form-group">
                    <ul class="nav nav-pills nav-justified thumbnail setup-panel" style="width: 100%">
                      <li class="active col">
                        <a href="#" class="mb-3 estimate_navigation_link">
                          <span class="step">1</span>
                          <li class="active"><a href="#step1" data-toggle="tab">Move Info</a></li>
                        </a>
                      </li>
                      <li class=" col">
                        <a href="#" class="mb-3 estimate_navigation_link">
                          <span class="step">2</span>
                          <li><a href="#step2" data-toggle="tab">Inventory</a></li>
                        </a>
                      </li>
                      <li class=" col">
                        <a href="#" class="mb-3 estimate_navigation_link">
                          <span class="step">3</span>
                          <li><a href="#step3" data-toggle="tab">Materials</a></li>
                        </a>
                      </li>
                      <li class=" col">
                        <a href="#" class="mb-3 estimate_navigation_link">
                          <span class="step">4</span>
                          <li><a href="#step4" data-toggle="tab">Pack Labor</a></li>
                        </a>
                      </li>
                      <li class=" col">
                        <a href="#" class="mb-3 estimate_navigation_link">
                          <span class="step">5</span>
                          <li><a href="#step5" data-toggle="tab">Storage</a></li>
                        </a>
                      </li>
                      <li class=" col">
                        <a href="#" class="mb-3 estimate_navigation_link">
                          <span class="step">6</span>
                          <li><a href="#step6" data-toggle="tab">Quote</a></li>
                        </a>
                      </li>
                    </ul>

                    <ul class="nav nav-tabs nav-justified">
		                    <li class="active"><a href="#step1" data-toggle="tab">Step 1</a></li>
		                    <li><a href="#step2" data-toggle="tab">Step 2</a></li>
		                    <li><a href="#step3" data-toggle="tab">Step 3</a></li>
		                    <li><a href="#step4" data-toggle="tab">Step 4</a></li>
		                    <li><a href="#step5" data-toggle="tab">Step 5</a></li>
		                    <li><a href="#step6" data-toggle="tab">Step 6</a></li>
		                </ul>  
                  </div>
                </div> -->

              </div>

              <nav class="navbar navbar-expand-lg top-bar-common">
                <div class="collapse navbar-collapse " id="navbar-example-8">
                  <div class="navbar-nav mr-auto">
                    <a class="nav-item nav-link active" href="javascript:void(0)">Referred By: {{$lead['referred_by']}}</a>
                    <a class="nav-item nav-link" href="javascript:void(0)">Move Date: {{$lead['approximate_move_date']}}</a>

                    <a class="nav-item nav-link" href="javascript:void(0)">Estimate: #{{$lead['id']}}</a>
                  </div>
                </div>
              </nav>

              <div class="card mb-4">
                <div class="card-body after-loading-icon">
                  <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
                  <style>
                    #map_canvas {
                      height: 100%;
                      width: 100%;
                      position: absolute !important;
                    }

                    .extra_stop {
                      opacity: 0.65;
                    }

                    /* .space {
                      padding: 3px;
                    } */

                    @media (min-width: 300px) and (max-width: 750px) {
                      .cal {
                        margin-top: 3%;
                      }

                      #destination_floor,
                      #origin_floor {
                        width: 400px;
                      }
                    }

                    button {
                      -webkit-appearance: none;
                      -moz-appearance: none;
                      appearance: none;
                    }
                  </style>
                  <div class="container-fluid">

                  	<div class="panel-body tab-content">
	                        <div class="tab-pane active" id="step1">
	                        	@include('admin.leads.step_one')
	                        </div>	                        
	                        <div class="tab-pane" id="step2">
	                            @include('admin.leads.step_two')
	                        </div> 
	                        <div class="tab-pane" id="step3">
		                     	@include('admin.leads.step_three')
	                        </div> 
	                        <div class="tab-pane" id="step4">
		                        @include('admin.leads.step_four')
	                        </div> 
	                        <div class="tab-pane" id="step5">
	                          	@include('admin.leads.step_five')
	                        </div>
	                        <div class="tab-pane" id="step6">
	                            @include('admin.leads.step_six')
	                        </div>                                                     
	                    </div>
                                   
                  </div>

                </div>
              </div>









          </div><!-- class="col-lg-12 col-md-12 col-12" ends--->





          </div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
 <style type="text/css">
 	/* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
#map {
  height: 100%;
}

#floating-panel {
  position: absolute;
  top: 10px;
  left: 25%;
  z-index: 5;
  background-color: #fff;
  padding: 5px;
  border: 1px solid #999;
  text-align: center;
  font-family: "Roboto", "sans-serif";
  line-height: 30px;
  padding-left: 10px;
}
 </style>
 <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfUl7G2CIfkJdCRwakYUQeen2o5cCzcVE&callback=initMap&libraries=places&v=weekly"
      async
    ></script>

 <script type="text/javascript">
var data = 
        <?php 
          $rows = array();
            foreach($metadata['categories'] as $casereferences){
                $Companyname = $casereferences['id'];
                $CaseRefence = $casereferences['items'];
                $CaseRefence_count = count($CaseRefence);
                for ($i=0; $i < $CaseRefence_count; $i++) { 
                  $rows[$Companyname][$i]= $CaseRefence[$i];
                }
                  
            }
            //$rows['base'] = array(array('name' => 'Please choose from above'));
            $d = json_encode($rows,JSON_PARTIAL_OUTPUT_ON_ERROR);
           echo $d;
        ?>;

 $("#category_id").change(function() {
 		var first = $(this),
            second = $("#item_id"),
            key = first.val(),
            // instead of the original switch code
            vals = data[key] == undefined ? data.base : data[key],
            html = [];
           // console.log(key);
         // create insert html before adding
         $.each(vals,function(i,val){
              html.push('<option value="'+val.id+'">'+val.name+'</option>')
         });
         // no need to empty the element before adding the new content
         second.html(html.join());        
});

var URL = '<?php echo env('SITE_URL');?>';
materials = '<?php echo count($metadata['materials']); ?>';
aRowCount=1;
increment = 0;
$(function() {
	$('#appendAddressRow').on('click', function( e ) {
        e.preventDefault();
        var op ='<tr class="address_sec suppliersaddress'+aRowCount+' active"><td width="35%"><div class="form-group" style="margin:5px"><div class="col-sm-10"><select class="form-control" id="material_id'+aRowCount+'" name="moving_information[material_id][]"><option value="0">Please select material</option>@foreach($metadata['materials'] as $material)<option value="{{$material['id']}}">{{$material['name']}} - {{$material['price']}}</option>@endforeach</select></div></div></td><td><div class="form-group" style="margin:5px"><div class="btn-group pull-left"><a href="#" class="btn btn-danger" id="minus_button'+aRowCount+'"><i class="fa fa-minus"></i></a></div><div class="col-sm-6"><input type="number" name="moving_information[quantity][]" class="form-control" id="quantityy'+aRowCount+'" value="1" min="1" step="1"></div><div class="btn-group pull-right"><a href="#" class="btn btn-info" id="plus_button'+aRowCount+'"><i class="fa fa-plus"></i></a></div></div></div></td><td><div class="form-group" style="margin:5px"><div class="col-sm-10"><input type="hidden" name="moving_information[original_price][]" id="original_price'+aRowCount+'" value="0"><input type="number" name="moving_information[price][]" class="form-control inputview" id="price'+aRowCount+'" value="" min="0" step="0.01"><button type="button" class="btn spinner-down btn-danger" id="remove-address" style="float: right;position: absolute;top: 0;right:-53px;"><i class="fa fa-minus"></i></button></div></div></td></tr>';
    		$('#TextBoxesGroup').append(op);
			aRowCount++;
		if(aRowCount == materials)
		{
			$("#appendAddressRow").hide();
		}
    });

    $(document).on('click', '#remove-address', function( e ) {
        e.preventDefault();
        $(this).closest( '.address_sec' ).remove();
     	aRowCount--;
        if(aRowCount < materials)
        {
        	$("#appendAddressRow").show();
        }
    });
});

$(document).ready(function(){
  var totalSteps = $("#progressTab > li").length;
  $(".progress-text").click(function(){
    var pID = $(this).attr('data-id');
    var stepID = 'step' + pID;
    $("#"+stepID).siblings().removeClass().addClass('tab-pane');
    $("#"+stepID).addClass('tab-pane active');
    
    $("#progressTab li").each(function(liIndx){
      if((liIndx + 1) < parseInt(pID)) {
        $(this).removeClass().addClass("plitags progress-done");  
      } else if((liIndx + 1) === parseInt(pID)){
        $(this).removeClass().addClass("plitags progress-done progress-current");  
      } else if((liIndx + 1) < parseInt(pID)) {
        $(this).removeClass().addClass("plitags progress-todo");  
      }
    })
  });
})


$(document).on('change', '#material_id'+aRowCount, function( e ) {
	var inc_val = aRowCount-1;
	var quantity = $('#quantityy'+inc_val).val();
	$.ajax(URL+'/api/materialDetails', {
		type: 'POST',  // http method
		data: {material_id : $('#material_id'+inc_val).val()},  // data to submit
		success: function (data, status, xhr) {
			if(data.status_code == 200)
	  		{
	  			var price = data.data.price;
	  			$('#original_price'+inc_val).val(price);	
	  			addTotalPrice(inc_val,quantity,price);
	  		}
		},
		error: function (jqXhr, textStatus, errorMessage) {
				alert('Error: ' + errorMessage);
				return false;
			}
	});
});

$(document).on('click', '#plus_button'+aRowCount, function( e ) {
e.preventDefault();
var inc_val = aRowCount-1;
var quantity = $('#quantityy'+inc_val).val();
quantity++;
	$('#quantityy'+inc_val).val(quantity);    
	var original_price = $('#original_price'+inc_val).val();
	addTotalPrice(inc_val,quantity,original_price);    	
});

$(document).on('click', '#minus_button'+aRowCount, function( e ) {
e.preventDefault();
var inc_val = aRowCount-1;
var quantity = $('#quantityy'+inc_val).val();
 if (quantity == 1) return;
  quantity--;
	$('#quantityy'+inc_val).val(quantity);    
	var original_price = $('#original_price'+inc_val).val();
	addTotalPrice(inc_val,quantity,original_price);    	
});


////

$.ajax(URL+'/api/materialDetails', {
type: 'POST',  // http method
data: {material_id : $('#material_id'+increment).val()},  // data to submit
success: function (data, status, xhr) {
	if(data.status_code == 200)
		{
			var price = data.data.price;
			$('#original_price'+increment).val(price);
			$('#price'+increment).val(price);
		}
},
error: function (jqXhr, textStatus, errorMessage) {
		alert('Error: ' + errorMessage);
		return false;
	}
});


$("#material_id"+increment).change(function() {			
	var quantity = $('#quantityy'+increment).val();
	$.ajax(URL+'/api/materialDetails', {
		type: 'POST',  // http method
		data: {material_id : $('#material_id'+increment).val()},  // data to submit
		success: function (data, status, xhr) {
			if(data.status_code == 200)
	  		{
	  			var price = data.data.price;
	  			$('#original_price'+increment).val(price);	          			
	  			addTotalPrice(increment,quantity,price);
	  		}
		},
		error: function (jqXhr, textStatus, errorMessage) {
				alert('Error: ' + errorMessage);
				return false;
			}
	});
});



$('#plus_button'+increment).on('click', function( e ) {
e.preventDefault();
var quantity = $('#quantityy'+increment).val();
quantity++;
	$('#quantityy'+increment).val(quantity);    
	var original_price = $('#original_price'+increment).val();
	addTotalPrice(increment,quantity,original_price);    	
});

$('#minus_button'+increment).on('click', function( e ) {
 e.preventDefault();
 var quantity = $('#quantityy'+increment).val();
 if (quantity == 1) return;
  quantity--;
  $('#quantityy'+increment).val(quantity); 
  var original_price = $('#original_price'+increment).val();
  addTotalPrice(increment,quantity,original_price);    
});

$(document).on('click', '#remove-address'+increment, function( e ) {
        e.preventDefault();
        $(this).closest( '.address_sec_'+increment).remove();
});


function addTotalPrice(increment,quantity,original_price)
{
	var total_price = original_price * quantity;
	$('#price'+increment).val(total_price);
} 

if($("input[type='radio'].form-check-input").is(':checked')) {
	    var card_type = $("input[type='radio'].form-check-input:checked").val();
	    $('#packing_requirement_id').val(card_type);
	    if(card_type == 1)
	    {
	    	$('.packing_address_sec').hide();
	    }
	    else {

	    	$('.packing_address_sec').show();
	    }
}

$('#myForm').on('click', function () {
	if($("input[type='radio'].form-check-input").is(':checked')) {
	    var card_type = $("input[type='radio'].form-check-input:checked").val();
	    $('#packing_requirement_id').val(card_type);
	    if(card_type == 1)
	    {
	    	$('.packing_address_sec').hide();
	    }
	    else {

	    	$('.packing_address_sec').show();
	    }
	}
});



</script>
@endsection
