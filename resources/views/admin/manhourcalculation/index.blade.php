@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Floors
@endsection

@section('content')
@include('admin.components.message')	

<div class="content-wrapper container-xxl p-0">
        <div class="content-header row"></div>
        <div class="content-body">
          <section id="dashboard-ecommerce">
            <div class="">
                                <div class="btn-group pull-right">
                                    @if($privileges['Add']=='true') 
                                    <a href="{{URL::to('admin/manhourcalculation/create')}}" class="btn btn-info"><i class="fa fa-edit"></i>Add Man Hour Calculation</a>
                                        @endif
                                </div>
              
                                <div class="table-bar2">
                                    <table id="example" class="table table-striped" style="width:100%">
                                    <thead class="thead">
                                            <tr>
                                                <th>Men Hours</th>
                                                <th>Men</th>
                                                <th>Stairs Men</th>
                                                <th>Elevator Men</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($manhourcalculation as $charge)
                                    <tr>
                                        <td>
                                            {{$charge->men_hours}}
                                        </td>  
                                        <td>
                                            {{$charge->men}}
                                        </td> 
                                        <td>
                                            {{$charge->stairs_men}}
                                        </td>
                                        <td>
                                            {{$charge->elevator_men}}
                                        </td>                    
                                        <td>
                                            {{$charge->status}}
                                        </td>                                       
                                        <td width="20%">
                                            <div >
                                                <div style="float:left;padding:0 10px 10px 0;">
                                                 @if($privileges['Edit']=='true')
                                                {{ link_to_route('manhourcalculation.edit','Edit',array($charge->id), array('class' => 'btn btn-info')) }}
                                                @endif 
                                                </div>
                                                <div style="float:left;padding-right:10px;">
                                                   @if($privileges['Delete']=='true')
                                                    {{ Form::open(array('onsubmit' => 'return confirm("Are you sure you want to delete?")','method' => 'DELETE', 'route' => array('manhourcalculation.destroy', $charge->id))) }}
                                                    <button type="submit" class="btn btn-danger btn-xs pull-right" style="font-size: 11px;padding: 11px 12px;">Delete</button>
                                                    {{ Form::close() }}
                                                   @endif
                                                </div>
                                            </div>
                                        </td>
                                         </tr>
                                    @endforeach
                                        </tbody>
                                    </table>
                                    {!! $manhourcalculation->render() !!}
                                </div>
                                
   
                
            </div>
          </section>
        </div>  
      </div>

@endsection