@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Estimate Range
@endsection
@section('module')
Estimate Range
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return UserEditValidate();','method' => 'PUT', 'route' => array('manhourcalculation.update',$charge->id),'files'=>true)) }}
<div class="card mb-4">
   <div class="card-body after-loading-icon">
     <div class="container-fluid">
      	<div class="col-md-6">
			<div class="col-md-6">
            {{ Form::ahText('men_hours','Men Hours :',$charge->men_hours,array())  }}				
						{{ Form::ahNumber('men','Men :',$charge->men,array()) }}
						{{ Form::ahNumber('stairs_men','Stairs Men :',$charge->stairs_men,array()) }}
						{{ Form::ahNumber('elevator_men','Inventory :',$charge->elevator_men,array()) }}
		        {{ Form::ahSelect('is_active','Status :',$charge->is_active,array('1' => 'Active', '0' => 'Inactive')) }}
                </br>
		    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('manhourcalculation.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	</div>
  </div>
</div>
@endsection