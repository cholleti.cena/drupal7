@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Fuel Charges
@endsection
@section('module')
Fuel Charges
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return UserEditValidate();','method' => 'PUT', 'route' => array('floors.update',$charge->id),'files'=>true)) }}
<div class="card mb-4">
   <div class="card-body after-loading-icon">
     <div class="container-fluid">
      	<div class="col-md-6">
			<div class="col-md-6">
            {{ Form::ahNumber('floor','Floor :',$charge->floor,array())  }}				
						{{ Form::ahNumber('extra_time_Room','Extra Time / Room (Minutes) :',$charge->extra_time_Room,array()) }}
						{{ Form::ahNumber('stairs_os_items','Stairs; O/S Items :',$charge->stairs_os_items,array()) }}
						{{ Form::ahNumber('stairs_inventory','Stairs; Inventory :',$charge->stairs_inventory,array()) }}
						{{ Form::ahNumber('stairs_boxes','Stairs; Boxes :',$charge->stairs_boxes,array()) }}
						{{ Form::ahNumber('elevator_inventory','Elevator; Inventory :',$charge->elevator_inventory,array()) }}
						{{ Form::ahNumber('elevator_boxes','Elevator; Boxes :',$charge->elevator_boxes,array()) }}
		        {{ Form::ahSelect('is_active','Status :',$charge->is_active,array('1' => 'Active', '0' => 'Inactive')) }}
                </br>
		    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('floors.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	</div>
  </div>
</div>
@endsection