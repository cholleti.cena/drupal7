@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Floors
@endsection

@section('content')
@include('admin.components.message')	

<div class="content-wrapper container-xxl p-0">
        <div class="content-header row"></div>
        <div class="content-body">
          <section id="dashboard-ecommerce">
            <div class="">
                                <div class="btn-group pull-right">
                                    @if($privileges['Add']=='true') 
                                    <a href="{{URL::to('admin/floors/create')}}" class="btn btn-info"><i class="fa fa-edit"></i>Add Floor</a>
                                        @endif
                                </div>
              
                                <div class="table-bar2">
                                    <table id="example" class="table table-striped" style="width:100%">
                                    <thead class="thead">
                                            <tr>
                                                <th>Floor</th>
                                                <th>Extra Time / Room (Minutes)</th>
                                                <th>Stairs; O/S Items</th>
                                                <th>Stairs; Inventory</th>
                                                <th>Stairs; Boxes</th>
                                                <th>Elevator; Inventory</th>
                                                <th>Elevator; Boxes</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($floors as $charge)
                                    <tr>
                                        <td>
                                            {{$charge->floor}}
                                        </td>  
                                        <td>
                                            {{$charge->extra_time_Room}}
                                        </td> 
                                        <td>
                                            {{$charge->stairs_os_items}}
                                        </td> 
                                        <td>
                                            {{$charge->stairs_inventory}}
                                        </td> 
                                        <td>
                                            {{$charge->stairs_boxes}}
                                        </td> 
                                        <td>
                                            {{$charge->elevator_inventory}}
                                        </td> 
                                        <td>
                                            {{$charge->elevator_boxes}}
                                        </td>                            
                                        <td>
                                            {{$charge->status}}
                                        </td>                                       
                                        <td width="20%">
                                            <div >
                                                <div style="float:left;padding:0 10px 10px 0;">
                                                 @if($privileges['Edit']=='true')
                                                {{ link_to_route('floors.edit','Edit',array($charge->id), array('class' => 'btn btn-info')) }}
                                                @endif 
                                                </div>
                                                <div style="float:left;padding-right:10px;">
                                                   @if($privileges['Delete']=='true')
                                                    {{ Form::open(array('onsubmit' => 'return confirm("Are you sure you want to delete?")','method' => 'DELETE', 'route' => array('floors.destroy', $charge->id))) }}
                                                    <button type="submit" class="btn btn-danger btn-xs pull-right" style="font-size: 11px;padding: 11px 12px;">Delete</button>
                                                    {{ Form::close() }}
                                                   @endif
                                                </div>
                                            </div>
                                        </td>
                                         </tr>
                                    @endforeach
                                        </tbody>
                                    </table>
                                    {!! $floors->render() !!}
                                </div>
                                
   
                
            </div>
          </section>
        </div>  
      </div>

@endsection