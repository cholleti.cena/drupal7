@extends('admin.layouts.thememaster')
@section('title')
{{env('APP_NAME')}}-Packing Materials
@endsection
@section('module')
Packing Materials
@endsection

@section('content')
@include('admin.components.message')
{{Form::component('ahText', 'admin.components.form.text', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahSelect', 'admin.components.form.select', ['name', 'labeltext'=>null, 'value' => null,'valuearray' => [], 'attributes' => []])}}
{{Form::component('ahNumber', 'admin.components.form.number', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}
{{Form::component('ahTextarea', 'admin.components.form.textarea', ['name', 'labeltext'=>null, 'value' => null, 'attributes' => []])}}

{{ Form::open(array('onsubmit' => 'return CategoryValidate();','route' => 'packingmaterials.store','files'=>true)) }}

<div class="card mb-4">
   <div class="card-body after-loading-icon">
     <div class="container-fluid">
      	<div class="col-md-6">
        
				{{ Form::ahText('name','Name :','',array('maxlength' => '100'))  }}				
				{{ Form::ahNumber('price','Price :','',array()) }}
				{{ Form::ahNumber('default_quantity','Default Quantity :','',array('min'=>'0','maxlength' => '11','max'=>'99999999999')) }}
		    	{{ Form::ahSelect('is_active','Status :','1',array('1' => 'Active', '0' => 'Inactive')) }}
	    </div>
	    <div class="form-group">
		    <div class="panel-footer">
		        <div class="col-md-6 col-md-offset-3">
		            {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
		            {{ link_to_route('packingmaterials.index','Cancel',null, array('class' => 'btn btn-danger')) }}
		        </div>
		    </div>
	    </div>
	</div>
  </div>
</div>
@endsection