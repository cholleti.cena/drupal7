<link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/css/admin.css">
<div class="sidebar">
  <ul>
    <!-- <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i><span>Dashboard</span></a></li> -->
    <li><a href="/courses"><i class="fa fa-file-video-o" aria-hidden="true"></i><span>Courses</span></a></li>
    <li><a href="/jobs"><i class="fa fa-briefcase" aria-hidden="true"></i><span>Jobs</span></a></li>
    <li><a href="/faqs"><i class="fa fa-question-circle-o" aria-hidden="true"></i><span>Faq</span></a></li>
    </ul>
</div>