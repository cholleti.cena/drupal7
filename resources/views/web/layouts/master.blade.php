<!DOCTYPE html>
<html lang="en" >
<head>
    
    <title>@yield('title')</title>

    <link rel="apple-touch-icon" href="{{env('SITE_URL')}}/theme/web/app-assets/images/logo/logo.png">
    <meta name="csrf-token" content="{{ csrf_token() }}" /> 
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="32x32" href="{{env('SITE_URL')}}/theme/app-assets/images/ico/favicon.ico">  
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/vendors/css/charts/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/vendors/css/extensions/toastr.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/css/pages/dashboard-ecommerce.css">
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/css/plugins/charts/chart-apex.css">
    <link rel="stylesheet" type="text/css" href="{{env('SITE_URL')}}/theme/web/app-assets/css/plugins/extensions/ext-component-toastr.css">
    <!-- END: Page CSS-->
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    ::-webkit-scrollbar {
  width: 10px;
}
  ::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
  ::-webkit-scrollbar-thumb {
  background: #888; 
    border-radius:10px;
}
::-webkit-scrollbar-thumb:hover {
  
  background: #555; 
} 

    .popup-form__submit:hover {
  background-color: #121c2480;
}
.intro {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100vh;
}

button {
  border: none;
  outline: none;
  background-color: #277cbb;
  padding: 14px 35px;
  border-radius:20px;
  font-size: 18px;
  color: white;
  font-weight: 700;
  cursor: pointer;
  transition: background-color 0.3s ease-in-out;
}
.popup-wrapper {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(2, 2, 2, 0.685);
  z-index: 9998;
  opacity: 0;
  pointer-events: none;
  padding: 10px 20px;
  transition: opacity 0.3s ease-in-out;
}

.popup-wrapper.active {
  opacity: 1;
  pointer-events: all;
}

.popup {
  padding: 22px 25px;
  max-width: 780px;
  width: 100%;
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: #e4f5ff;
  border-radius: 15px;
}

.popup-form {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
}

.close-btn {
  position: absolute;
  top: 0;
  right: 5px;
  background: none;
  border: none;
  padding: 0;
  border: 0;
  width: 30px;
  height: 55px;
  outline: none;
  cursor: pointer;
  display: inline-block;
  transition: transform 0.3s, opacity 0.3s;
  -webkit-transition: transform 0.3s, opacity 0.3s;
  -moz-transition: transform 0.3s, opacity 0.3s;
  -ms-transition: transform 0.3s, opacity 0.3s;
  -o-transition: transform 0.3s, opacity 0.3s;
}

.close-btn:before,
.close-btn:after {
  content: "";
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  width: 2px;
  height: 30px;
  background-color: #1f1f1f;
  border-radius: 1.5px;
  transform: translateY(-50%);
}

.close-btn:before {
  transform: translateY(-50%) rotate(45deg);
}

.close-btn:after {
  transform: translateY(-50%) rotate(-45deg);
}

.close-btn:hover {
  transform: scale(1.1);
}

.close-btn:active {
  opacity: 0.8;
  transform: scale(0.9);
}

.popup-form__input {
  margin-bottom: 20px;
  padding: 12px 20px;
  width: 80%;
}
#registration{

height:480px;
overflow-x: hidden;
overflow-y: auto;
padding-right:15px;
}
body{
  font-family: arial;
  color: #000;
}

h1,h2,h3,h4,h5,h6{
  font-family: arial;
  color: #000;
}



</style>
</head>
<body>

@yield('content')

  
<script>
    let popup = document.querySelector(".popup-wrapper");
let popupForm = document.querySelector(".popup-form");
let popupBtn = document.querySelector(".popup-open");
let popupClose = document.querySelector(".close-btn");

popupBtn.addEventListener("click", (e) => {
  e.preventDefault;
  showPopup();
});

popupClose.addEventListener("click", (e) => {
  e.preventDefault;
  removePopup();
});

popup.addEventListener("click", (e) => {
  let target = e.target;
  if (target.classList.contains("popup-wrapper")) {
    removePopup();
  } else return;
});

function showPopup() {
  popup.classList.add("active");
  bodyScroll();
}

function removePopup() {
  popup.classList.remove("active");
  bodyScroll();
}

function bodyScroll() {
  document.body.classList.toggle("no-scroll");
}
</script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
  <!-- JavaScript for validations only -->
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
</body>
</html>
