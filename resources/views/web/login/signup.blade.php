@extends('web.layouts.master')
@section('title')
Sign Up | {{env('APP_NAME')}}
@endsection
@section('content')
<?php 
if(isset($_GET['type']))
{
	$type = $_GET['type'];
}
else
{
	$type = "";
}
 ?>
<div class="limiter">
	<div class="container-login100">
	    <div class="wrap-login100">
		   <div class="login100-pic js-tilt" data-tilt>
			   <img src="{{env('SITE_URL')}}/theme/web/img/login.png" alt="IMG">
			</div>
			<form class="login100-form validate-form" id="formSignUp">
				<span class="login100-form-title">
					Signup
				</span>
				<input type="hidden" name="is_registered" value="1">
				<input type="hidden" name="type" id="type" value="{{$type}}">
		          <!-- input -->
		            <div class="wrap-input100 validate-input" >
		            <input class="input100" type="text" name="first_name" id="first_name" placeholder="First Name" required>
		            <span class="focus-input100"></span>
		            <span class="symbol-input100">
		              <i class="fa fa-user" aria-hidden="true"></i>
		            </span>
		          </div>
		           <!-- input -->

		            <!-- input -->
		           <div class="wrap-input100 validate-input" >
		             <input class="input100" type="text" name="last_name" id="last_name" placeholder="Last Name" required>
		             <span class="focus-input100"></span>
		             <span class="symbol-input100">
		              <i class="fa fa-user" aria-hidden="true"></i>
		             </span>
          			</div>
           			<!-- input -->

					<div class="wrap-input100 validate-input" >
						<input class="input100" type="text" name="email" id="email" placeholder="Email" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<!-- <div class="wrap-input100 validate-input" >
						<input class="input100" type="password" name="password" id="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div> -->

		            <!-- input -->
		            <div class="wrap-input100 validate-input" >
		            <input class="input100" type="text" name="phone" id="phone" placeholder="Mobile Number" required>
		            <span class="focus-input100"></span>
		            <span class="symbol-input100">
		              <i class="fa fa-phone" aria-hidden="true"></i>
		            </span>
		          </div>
		           <!-- input -->

					<div class="">
						<button class="login100-form-btn">Sign up</button>
					</div>
					<div class="text-center mt-4 ">
						<a class="txt2" href="/login">Already registered User? Log in
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>

<script type="text/javascript">
    var URL = '<?php echo env('SITE_URL');?>';

    $(document).ready(function(){
    $("#formSignUp").submit(function(event){
        event.preventDefault(); //prevent default action 
        var post_url = URL+'/api/signUp'; 
        var form_data = $(this).serialize(); 
        	$.post( post_url, form_data, function( response ) 
        	{
	          if(response.status_code == 200)
	          {
	          	var type = $('#type').val();
	          	localStorage.setItem("user_data", JSON.stringify(response));
	          	if(type !== "")
	          	{
	          		var redirecturl = URL+'/reset-password?authentication='+response.data.reset_hash+'&type='+type;
	          	}
	          	else
	          	{
	          		var redirecturl = URL+'/reset-password?authentication='+response.data.reset_hash;
	          	}
	          	window.location.href = redirecturl;
	          	$('#formLogin').trigger("reset");
	            alert(response.message);
	          }
	          else
	          {
	            alert(response.message);
	            return false;            
	          }
	        });
    	});
    });
</script>
@endsection