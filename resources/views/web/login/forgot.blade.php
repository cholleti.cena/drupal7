@extends('web.layouts.master')
@section('title')
Forgot | {{env('APP_NAME')}}
@endsection
@section('content')

<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="{{env('SITE_URL')}}/theme/web/img/login.png" alt="IMG">
				</div>

				<form class="login100-form validate-form" id="formLogin">
					<span class="login100-form-title">
						 Forgot Password
					</span>
					<input type="hidden" name="action" value="sendotp">
					<div class="wrap-input100 validate-input" >
						<input class="input100" type="text" name="email" id="email" placeholder="Email" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="">
						<button class="login100-form-btn">
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<script type="text/javascript">
    var URL = '<?php echo env('SITE_URL');?>';

    $(document).ready(function(){
    $("#formLogin").submit(function(event){
        event.preventDefault(); //prevent default action 
        var post_url = URL+'/api/verify'; 
        var form_data = $(this).serialize(); 
        	$.post( post_url, form_data, function( response ) 
        	{
	          if(response.status_code == 200)
	          {
	          	var email = $('#email').val();
	          	var redirecturl = URL+'/reset-password?authentication='+response.data.reset_hash;
	          	window.location.href = redirecturl;
	          	$('#formLogin').trigger("reset");
	            alert(response.message);
	          }
	          else
	          {
	            alert(response.message);
	            return false;            
	          }
	        });
    	});
    });
</script>
@endsection