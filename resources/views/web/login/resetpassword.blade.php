@extends('web.layouts.master')
@section('title')
Set Your Password | {{env('APP_NAME')}}
@endsection
@section('content')

<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="{{env('SITE_URL')}}/theme/web/img/login.png" alt="IMG">
				</div>

				<form class="login100-form validate-form" id="formLogin">
					<span class="login100-form-title">
						 Set Your Password
					</span>
					<input type="hidden" name="action" value="verify">
					<input type="hidden" name="email" value="{{$email}}">
					<div class="wrap-input100 validate-input" >
						<input class="input100" type="text" name="otp" id="otp" placeholder="Code" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100 validate-input" >
						<input class="input100" type="password" name="newpassword" id="newpassword" placeholder="New Password" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100 validate-input" >
						<input class="input100" type="password" name="crmpassword" id="crmpassword" placeholder="Confirm Password" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					<div class="">
						<button class="login100-form-btn">
							Submit
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

<script type="text/javascript">
    var URL = '<?php echo env('SITE_URL');?>';

    $(document).ready(function(){
    $("#formLogin").submit(function(event){
        event.preventDefault(); //prevent default action 

        var newpassword = $('#newpassword').val();
        var crmpassword = $('#crmpassword').val();

        if(newpassword !== crmpassword)
        {
        	alert('Your new password and confirmation password do not match.');
        	return false;
        }

        var post_url = URL+'/api/verify'; 
        var form_data = $(this).serialize(); 
        	$.post( post_url, form_data, function( response ) 
        	{
	          if(response.status_code == 200)
	          {
	          	var type = $('#type').val();
	          	localStorage.setItem("user_data", JSON.stringify(response));
	          	var redirecturl = URL+'/authentication?access_token='+response.data.reset_hash;
	          	window.location.href = redirecturl;
	          	$('#formLogin').trigger("reset");
	            alert(response.message);
	          }
	          else
	          {
	            alert(response.message);
	            return false;            
	          }
	        });
    	});
    });
</script>
@endsection