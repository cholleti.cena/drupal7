@extends('web.layouts.master')
@section('title')
Login | {{env('APP_NAME')}}
@endsection
@section('content')
<?php 
if(isset($_GET['type']))
{
	$type = $_GET['type'];
}
else
{
	$type = "";
}
 ?>
<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="{{env('SITE_URL')}}/theme/web/img/login.png" alt="IMG">
				</div>

				<form class="login100-form validate-form" id="formLogin">
					<span class="login100-form-title">
						 Login
					</span>
					<input type="hidden" name="is_registered" value="1">
					<input type="hidden" name="type" id="type" value="{{$type}}">
					<div class="wrap-input100 validate-input" >
						<input class="input100" type="text" name="username" id="username" placeholder="Email" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" >
						<input class="input100" type="password" name="password" id="password" placeholder="Password" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>

					<div class="">
						<button class="login100-form-btn">
							Login
						</button>
					</div>

					<div class="text-center mt-4 ">
						<span class="txt1">
							Forgot
						</span>
						<a class="txt2" href="/forgot">
							Email / Password?
						</a>
					</div>

					<div class="text-center mt-4 ">
						@if($type !== '')
						<a class="txt2" href="/sign-up?type={{$type}}">
							Create your Account
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
						@else
						<a class="txt2" href="/sign-up">
							Create your Account
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
						@endif
					</div>
				</form>
			</div>
		</div>
	</div>

<script type="text/javascript">
    var URL = '<?php echo env('SITE_URL');?>';

    $(document).ready(function(){
    $("#formLogin").submit(function(event){
        event.preventDefault(); //prevent default action 
        var post_url = URL+'/api/login'; 
        var form_data = $(this).serialize(); 
        	$.post( post_url, form_data, function( response ) 
        	{
	          if(response.status_code == 200)
	          {
	          	var type = $('#type').val();
	          	localStorage.setItem("user_data", JSON.stringify(response));
	          	if(type !== "")
	          	{
	          		var redirecturl = URL+'/authentication?access_token='+response.data.reset_hash+'&type='+type;
	          	}
	          	else
	          	{
	          		var redirecturl = URL+'/authentication?access_token='+response.data.reset_hash;
	          	}
	          	window.location.href = redirecturl;
	          	$('#formLogin').trigger("reset");
	            alert(response.message);
	          }
	          else
	          {
	            alert(response.message);
	            return false;            
	          }
	        });
    	});
    });
</script>
@endsection