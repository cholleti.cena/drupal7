<div class="row">
      <div class="col-sm-6">
        <div class="mb-1">
          <label for="field1">First Name*</label>
          <input type="text" name="first_name" class="form-control" id="first_name">
        </div>                          
      </div>

      <div class="col-sm-6">
        <div class="mb-1">
          <label for="field2">Last Name*</label>
          <input type="text" name="last_name" class="form-control" id="last_name">
        </div>
      </div>

     <!-- <div class="col-sm-6">
          <label for="field3">Phone Type*</label>
            <select onchange="" class="form-control" id="phone_type" name="phone_type">
              <option value="0">Select</option>
               @foreach($metadata['phonetypes'] as $phonetype)
                 <option value="{{$phonetype['id']}}">{{$phonetype['name']}}</option>
              @endforeach
            </select>
      </div> -->

      <div class="col-sm-6">
        <div class="mb-1">
          <label for="field2">Phone Number*</label>
          <input type="number" name="phone" class="form-control" id="phone">
        </div>
      </div>

      <div class="col-sm-6">
        <div class="mb-1">
          <label for="field2">Alter Phone Number*</label>
          <input type="number" name="alter_phone" class="form-control" id="alter_phone">
        </div>
      </div>

      <div class="col-sm-6">
        <div class="mb-1">
          <label for="field3">E-mail*</label>
          <input type="email" name="email" class="form-control" id="email">
        </div>
      </div>

      <div class="col-sm-6">
        <label for="field3">Referred by*</label>
        <select onchange="" class="form-control" id="referred_by" name="referred_by">
          <option value="0">Select</option>
           @foreach($metadata['referrals'] as $referral)
             <option value="{{$referral['id']}}">{{$referral['name']}}</option>
          @endforeach
        </select>
      </div>

      <h4 class="mt-3">Moving Information</h4>

      <div class="col-sm-6 mb-2">
        <label for="field1">Approximate Move Date*</label>
        <input type="date" name="moving_information[approximate_move_date]" class="form-control" id="approximate_move_date">
      </div>

       <div class="col-sm-6">
          <label for="field3">Type of Service*</label>
          <select class="form-control" name="moving_information[type_of_service]" id="type_of_service">
            <option value="0">Select</option>
            @foreach($metadata['servicetypes'] as $servicetype)
             <option value="{{$servicetype['id']}}">{{$servicetype['name']}}</option>
            @endforeach
          </select>
      </div>
     
      <div class="col-sm-12 mb-2" >
        <label for="field2">Origin Address or Zip Code*</label>
        <input type="text" placeholder="Search for Place" name="moving_information[origin_address]" class="form-control" id="pacinput">
        <div id="map" style="display:none;"></div>
        <div id="infowindow-content">
          <span id="place-name" class="title"></span><br />
          <span id="place-address"></span>
        </div>
      </div>

      <input type="hidden" name="moving_information[origin_latitude]" class="form-control" id="origin_latitude" value="17.4875">
      <input type="hidden" name="moving_information[origin_longitude]" class="form-control" id="origin_longitude" value="78.3953">

      <div class="col-sm-6 mb-2">
        <label for="field3">Origin Type*</label>
          <select class="form-control" name="moving_information[origin_property_type_id]" id="origin_property_type_id">
              <option value="0">Select Type</option>
              @foreach($metadata['properties'] as $property)
              <option value="{{$property['id']}}">{{$property['name']}}</option>
              @endforeach
            </select>
      </div>

      <div class="col-sm-6 mb-2">
          <label for="field3">Origin Floor*</label>
            <input type="text" class="form-control input" name="moving_information[origin_floor][]" id="origin_floor"  value="0" />  
      </div>

      

      <div class="col-sm-6 mb-2">
        <label for="field3">Destination Floor*</label>
          <select class="form-control" name="moving_information[destination_property_type_id]" id="destination_property_type_id">
              <option value="0">Select Type</option>
              @foreach($metadata['properties'] as $property)
              <option value="{{$property['id']}}">{{$property['name']}}</option>
              @endforeach
            </select>
      </div>

      <div class="col-sm-6 mb-2">
          <label for="field3">Destination Floor*</label>
          <input type="text" class="form-control input" name="moving_information[destination_floor][]"  value="0" id="destination_floor"/>  
      </div>

      <input type="hidden" name="moving_information[destination_latitude]" class="form-control" id="destination_latitude" value="17.4875">
      <input type="hidden" name="moving_information[destination_longitude]" class="form-control" id="destination_longitude" value="78.3953">

      <div class="col-sm-12 mb-2">
        <label for="field2">Destination Address or Zip Code*</label>
        <input type="text" name="moving_information[destination_address]" class="form-control" id="paccinput">
        <div id="mapp" style="display:none;"></div>
          <div id="infowindoww-content">
            <span id="placee-name" class="title"></span><br />
            <span id="placee-address"></span>
          </div>
      </div>
</div>
<div class="col-xs-12 col-md-12 form-actions" >
<nav>
  <div class="nav nav-pills nav-fill col-md-3" id="nav-tab" role="tablist">
	<a class="nav-link  btn btn-primary" id="step2-tab" data-bs-toggle="tab" href="#step2"> Next →</a>
  </div>
</nav>


</div>

<style>
  /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
#map {
  height: 100%;
}

/* Optional: Makes the sample page fill the window. */
html,
body {
  height: 100%;
  margin: 0;
  padding: 0;
}

#description {
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
}

#infowindow-content .title {
  font-weight: bold;
}

#infowindoww-content .title {
  font-weight: bold;
}

#infowindow-content {
  display: none;
}

#infowindoww-content {
  display: none;
}

#map #infowindow-content {
  display: inline;
}

#mapp #infowindow-content {
  display: inline;
}

.pac-card {
  background-color: #fff;
  border: 0;
  border-radius: 2px;
  box-shadow: 0 1px 4px -1px rgba(0, 0, 0, 0.3);
  margin: 10px;
  padding: 0 0.5em;
  font: 400 18px Roboto, Arial, sans-serif;
  overflow: hidden;
  font-family: Roboto;
  padding: 0;
}

#pac-container {
  padding-bottom: 12px;
  margin-right: 12px;
}

.pac-controls {
  display: inline-block;
  padding: 5px 11px;
}

.pac-controls label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
}

#pacinput:focus {
  border-color: #4d90fe;
}

#paccinput:focus {
  border-color: #4d90fe;
}

#title {
  color: #fff;
  background-color: #4d90fe;
  font-size: 25px;
  font-weight: 500;
  padding: 6px 12px;
}

#floating-panel {
  position: absolute;
  top: 10px;
  left: 25%;
  z-index: 5;
  background-color: #fff;
  padding: 5px;
  border: 1px solid #999;
  text-align: center;
  font-family: "Roboto", "sans-serif";
  line-height: 30px;
  padding-left: 10px;
}

#mappp {
  height: 100%;
}

.popup-wrapper {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(2, 2, 2, 0.685);
    /* z-index: 9998; */
    opacity: 0;
    pointer-events: none;
    padding: 10px 20px;
    transition: opacity 0.3s ease-in-out;
}
  </style>
<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfUl7G2CIfkJdCRwakYUQeen2o5cCzcVE&callback=initMap&libraries=places&v=weekly"
      async
    >
</script>
<script type="text/javascript">
  
  function initMap() {
  const map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 40.749933, lng: -73.98633 },
    zoom: 13,
    mapTypeControl: false,
  });
  
  const input = document.getElementById("pacinput");
  const biasInputElement = document.getElementById("use-location-bias");
  const strictBoundsInputElement = document.getElementById("use-strict-bounds");
  const options = {
    fields: ["formatted_address", "geometry", "name"],
    strictBounds: false,
    types: ["establishment"],
  };

  const autocomplete = new google.maps.places.Autocomplete(input, options);
  autocomplete.bindTo("bounds", map);

  const infowindow = new google.maps.InfoWindow();
  const infowindowContent = document.getElementById("infowindow-content");
  infowindow.setContent(infowindowContent);
  const marker = new google.maps.Marker({
    map,
    anchorPoint: new google.maps.Point(0, -29),
  });
  autocomplete.addListener("place_changed", () => {
    infowindow.close();
    marker.setVisible(false);
    const place = autocomplete.getPlace();
    if (!place.geometry || !place.geometry.location) {
      window.alert("No details available for input: '" + place.name + "'");
      return;
    }
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);
    }
    //console.log(place.geometry.location.lat()+' - '+place.geometry.location.lng());
    $('#origin_latitude').val(place.geometry.location.lat());
    $('#origin_longitude').val(place.geometry.location.lng());
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);
    infowindowContent.children["place-name"].textContent = place.name;
    infowindowContent.children["place-address"].textContent =
      place.formatted_address;
    infowindow.open(map, marker);
  });

//desti
  const mapp = new google.maps.Map(document.getElementById("mapp"), {
    center: { lat: 40.749933, lng: -73.98633 },
    zoom: 13,
    mapTypeControl: false,
  });
  
  const inputp = document.getElementById("paccinput");
  const biasInputElementp = document.getElementById("use-location-bias");
  const strictBoundsInputElementp = document.getElementById("use-strict-bounds");
  const optionss = {
    fields: ["formatted_address", "geometry", "name"],
    strictBounds: false,
    types: ["establishment"],
  };

  const autocompletee = new google.maps.places.Autocomplete(inputp, options);
  autocompletee.bindTo("bounds", mapp);

  const infowindoww = new google.maps.InfoWindow();
  const infowindowwContent = document.getElementById("infowindoww-content");
  infowindoww.setContent(infowindowwContent);
  const markerr = new google.maps.Marker({
    mapp,
    anchorPoint: new google.maps.Point(0, -29),
  });
  autocompletee.addListener("place_changed", () => {
    infowindoww.close();
    markerr.setVisible(false);
    const placee = autocompletee.getPlace();
    if (!placee.geometry || !placee.geometry.location) {
      window.alert("No details available for input: '" + placee.name + "'");
      return;
    }
    if (placee.geometry.viewport) {
      mapp.fitBounds(placee.geometry.viewport);
    } else {
      mapp.setCenter(placee.geometry.location);
      mapp.setZoom(17);
    }
    //console.log(placee.geometry.location.lat()+' - '+placee.geometry.location.lng());
    $('#destination_latitude').val(placee.geometry.location.lat());
    $('#destination_longitude').val(placee.geometry.location.lng());
    markerr.setPosition(placee.geometry.location);
    markerr.setVisible(true);
    infowindowwContent.children["placee-name"].textContent = placee.name;
    infowindowwContent.children["placee-address"].textContent =
      placee.formatted_address;
    infowindow.open(mapp, marker);
  });
}
</script>


