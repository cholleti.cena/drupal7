<!DOCTYPE html>
<html>
<head>
	<title>Payment Response</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<h1 align="center">Payment {{$request['txStatus']}}</h1>
	<?php 
		if($request['txStatus'] === "SUCCESS")
		{
			header( "refresh:2; url=/my-courses",  true,  301  ); 
        	exit();
        }
        else
        {
        	header( "refresh:2; url=/checkout",  true,  301  ); 
        	exit();
        }
	 ?>	
</body>
</html>



