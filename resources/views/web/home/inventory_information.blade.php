<style>
	.blue-color {
    background-color: #d1eef9;
    color: #000;
}
.btn:not(.btn-sm):not(.btn-lg) {
    line-height: 1.44;
}
.btn-group-sm>.btn, .btn-sm {
    margin-bottom: 5px;
}
.btn-group-sm>.btn, .btn-sm {
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
}
.well {
    border: 0;
    padding: 20px;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
}
.well {
    min-height: 20px;
    padding: 19px;
    margin-bottom: 20px;
    background-color: #f5f5f5;
    border: 1px solid #e3e3e3;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgb(0 0 0 / 5%);
    box-shadow: inset 0 1px 1px rgb(0 0 0 / 5%);
}

btn>.btn:hover {
    z-index: 2;
}


.btn-group-sm>.btn, .btn-sm {
    margin-bottom: 5px;
}
.btn {
    outline: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
}
.btn-group-sm>.btn, .btn-sm {
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
}
.btn-default {
    color: #333;
    background-color: #fff;
    border-color: #ccc;
}
.btn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
.table-scrollable.table-scrollable-borderless {
    border: 0;
}
table {
    background-color: #fff;
}
table {
    border-spacing: 0;
    border-collapse: collapse;
}
.input-group-btn:first-child>.btn, .input-group-btn:first-child>.btn-group {
    margin-right: -1px;
}
.input-group-btn>.btn:active, .input-group-btn>.btn:focus, .input-group-btn>.btn:hover {
    z-index: 2;
}
btn:first-child>.btn, .input-group-btn:first-child>.btn-group>.btn, .input-group-btn:first-child>.dropdown-toggle, .input-group-btn:last-child>.btn-group:not(:last-child)>.btn, .input-group-btn:last-child>.btn:not(:last-child):not(.dropdown-toggle) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
}
.btn-warning, .btn-warning:hover {
    background-color: #1aace3;
    border-color: #1aace3;
}
.table-scrollable.table-scrollable-borderless {
    border: 0;
}
.table-scrollable {
    width: 100%;
    overflow-x: auto;
    overflow-y: hidden;
    border: 1px solid #e7ecf1;
    margin: 10px 0 !important;
}
.table-scrollable>.table {
    width: 100% !important;
    margin: 0 !important;
    margin-bottom: 0;
    background-color: #fff;
}
.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
}
table {
    border-collapse: separate;
    text-indent: initial;
    white-space: normal;
    line-height: normal;
    font-weight: normal;
    font-size: medium;
    font-style: normal;
    color: -internal-quirk-inherit;
    text-align: start;
    border-spacing: 2px;
    font-variant: normal;
}
.container.monitor {
    max-width: 990px;
    font-family: 'Roboto', sans-serif;
    border: 1px solid #929142%8d;
    border-left: 5px solid #1aace3;
}
.form-control-new {
    display: block;
    width: 40%;
    padding: 0.571rem 1rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.45;
    color: #6e6b7b;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #d8d6de;
    appearance: none;
    border-radius: 0.357rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}
	</style>

<div class="form">
<h5>* Please note inventory is required to provide a full estimate.</h5>

<div class="">
<div class="col-xs-12">
<hr>
<div class="row">
<div class="form-group form-group-md col-md-6">
<label class="control-label" for="square_footage">Total Square Footage:</label>
<input class="form-control allow-numbers" value="" oninput="this.setCustomValidity(&#39;&#39;);" required="required" type="text" name="moving_information[square_footage]" id="square_footage" />
</div>
<div class="form-group form-group-md col-md-6">
<label class="control-label" for="estimated_boxes">Total Estimated Boxes/Totes:</label>
<input class="form-control allow-numbers"  required="required" type="text" name="moving_information[estimated_boxes]" id="estimated_boxes" />
</div>
</div>
<div class="row">
<div class="form-group form-group-md col-md-6">
<label class="control-label" for="move_size_id">Move Size:</label>
<select class="form-control" name="moving_information[move_size_id]" id="move_size_id">
	<option value="">Select Type</option>
	
	 @foreach($metadata['movesizes'] as $movesize)
                 <option value="{{$movesize['id']}}">{{$movesize['name']}}</option>
              @endforeach

</select>
</div>
</div>
</div>
<div class="row">
<div class="col-xs-12">
<h3 class="page-header block">Select Items</h3>
<div class="well">
<div class="btn-group1 btn-group-sm"  >
@foreach($metadata['categories'] as $category)
<button name="button" type="button" onclick="find_inventory('{{$category['id']}}','{{$category['name']}}')" class="btn btn-default blue-color">{{$category['name']}}</button>
@endforeach

</div>
</div>

<div class="row">
<div class="col-xs-12">
<div class="input-group">
<span class="input-group-btn">
<button name="button" type="button" class="btn btn-default btn-sm" style="background: none;"> ◄ </button>
</span>
<span class="input-group-btn">
<button name="button" type="button" class="btn btn-default btn-sm" style="background: none;"> ► </button>
</span>
<input type="text" name="search_item" id="search_item" value="" onkeyup="search_inventory_items();" placeholder="Search" class="form-control input-sm" />
</div>
</div>
<div class="col-md-6 col-sm-6 col-xs-12">
<h4 class="margin-top-5"><span id="inventory_category"></span>
<small class="text-warning hidden-sm hidden-md hidden-lg"><br>Choose the items below</small>
</h4>
</div>
</div>
<div class="table-responsive1 table-scrollable table-scrollable-borderless">
<table id="estimate_itemized_inventory"  class="table-condensed" style="width: 100%;">
<tbody>
<!--<tr style="border-top: 1px solid #ddd;">
<td colspan="2"><a class="btn btn-sm font-grey-gallery" href="javascript:void(0)">No data to display</a></td>
</tr>-->
</tbody>
</table>
</div>
</div>
<div class="row">
<div class="col-xs-12">
<h3 class="page-header block">Inventory Details</h3>
<div >
<div class="table-responsive1 table-scrollable table-scrollable-borderless">
<table class="table table-stripped table-condensed" id="selected_inventory_items">
<thead>
<tr >
<td colspan="3">
<span style="float: right;margin-top: 10px;">
<h6><b>Cu Ft: <span id="cubicfoot"></span>&emsp;
Weight
: <span id="weight"></span></b></h6></span>
</td>
</tr>
<tr>
<th style="width: 40%;">ITEM</th>
<th style="width: 20%;">FLOOR</th>
<th style="width: 40%;">QTY</th>
</tr>
</thead>
<tbody>
<!--<tr>
<td>No Inventory Items Selected</td>
</tr>-->
</tbody>
</table>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-xs-12" style="float: right;">
<h3 class="page-header block">Materials Details</h3>
<div class="table-responsive1 table-scrollable table-scrollable-borderless">
<table class="table table-stripped table-condensed">
<thead>
<tr>
<th style="width: 50%;">ITEM</th>
<th  style="width: 50%;">QTY</th>
</tr>
</thead>
<tbody>
<tr>
<table id="selected_packing_material">
<tbody>
<!--<tr>
<td>No Material Items Selected</td>
</tr>-->
</tbody>
</table>
</tr>
</tbody>
<hr>
<tfoot>
<tr>
<td colspan="2">
<button name="button" type="button" onclick="add_packing_material()" class="btn btn-xs btn-success">Add a Packing Material</button>
</td>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
<div class="col-xs-12 col-md-12 form-actions" >
<!--<input type="submit" name="commit" value="Next →" class="btn btn-primary" id="estimate_btn" data-disable-with="Next →" />-->
<nav>
  <div class="nav nav-pills nav-fill col-md-3" id="nav-tab" role="tablist">
	<a class="nav-link btn btn-primary" id="step3-tab" data-bs-toggle="tab" href="#step3"> Next →</a>
  </div>
</nav>


</div>
</div>
</div>
<script>
var data = 
        <?php 
          $rows = array();
            foreach($metadata['categories'] as $casereferences){
                $Companyname = $casereferences['id'];
                $CaseRefence = $casereferences['items'];
                $CaseRefence_count = count($CaseRefence);
                for ($i=0; $i < $CaseRefence_count; $i++) { 
                  $rows[$Companyname][$i]= $CaseRefence[$i];
                }
                  
            }
           // $rows['base'] = array(array('name' => 'Please choose from above'));
            $d = json_encode($rows,JSON_PARTIAL_OUTPUT_ON_ERROR);
           echo $d;
        ?>;
        
        var materialsdata = 
        <?php 
            $mtd = json_encode($metadata['materials'],JSON_PARTIAL_OUTPUT_ON_ERROR);
           echo $mtd;
        ?>;
   
   
 $("#cubicfoot").text("0");	
 $("#weight").text("0");	
    function find_inventory(id,name) {
     
            $('#inventory_category').text(name);
            $(".previous_page").addClass("btn");
            $(".previous_page").text(" ◄ ");
            $(".next_page").addClass("btn");
            $(".next_page").text(" ► ");
            $("#extra_inventory_category_div").remove();
            $("#show_inventory_category_div").parent().find('.next_page').addClass("remove");
            $("#show_inventory_category_div").parent().find('.previous_page').addClass("remove");
            vals = data[id];
            
             $.each(vals,function(i,val){
			console.log(val);
              var op ='<tr style="border-top: 1px solid #ddd;" ><td colspan="2"><a class="btn btn-sm font-grey-gallery"  ><span  onclick="selected_inventory_item(\'' + val.id + '\',\'' + val.name + '\',\'' + val.cubic_foot + '\',\'' + val.weight + '\',\'' + val.price + '\')">'+val.name+'<input type="hidden" name="moving_information[category_id][]" value="' + val.id + '"></span></a></td></tr>';
             $('#estimate_itemized_inventory').append(op);
         });
        
    }
    increment = 1;
aRowCount=1;
arr=0;
    function selected_inventory_item(id,name,cubic_foot,weight,price) {	
		
		$("#cubicfoot").text(cubic_foot);	
		$("#weight").text(weight);	
		
            $("#extra_selected_inventory_div").hide();
            $(".previous_page").addClass("btn");
            $(".previous_page").text(" ◄ ");
            $(".next_page").addClass("btn");
            $(".next_page").text(" ► ");
            
             var item_name=name;
             var item_id=id;
           
             var op ='<tr id="row_'+aRowCount+'"><td width="40%" class="width-auto">'+item_name+'<input type="hidden" name="moving_information[inventory_items]['+arr+'][item_id]" class="form-control" id="item_id'+aRowCount+'" value="'+item_id+'"><input type="hidden" name="moving_information[inventory_items]['+arr+'][cubic_foot]" class="form-control" id="cubic_foot'+aRowCount+'" value="'+cubic_foot+'"><input type="hidden" name="moving_information[inventory_items]['+arr+'][weight]" class="form-control" id="weight'+aRowCount+'" value="'+weight+'"><input type="hidden" name="moving_information[inventory_items]['+arr+'][price]" class="form-control" id="price'+aRowCount+'" value="'+price+'"></td><td width="20%"><select id="floor_'+aRowCount+'" name="moving_information[inventory_items]['+arr+'][floor]" class="form-control-new input-sm" onchange="update_quantity('+aRowCount+')"><option selected="selected" value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option>></select></td><td width="40%"><div class="input-group input-small"><span class="input-group-btn"><button name="button" type="button" class="btn btn-warning btn-sm" onclick="subtract_quantity('+aRowCount+');">-</button> </span><input type="text" name="moving_information[inventory_items]['+arr+'][quantity]" id="quantity_label_'+aRowCount+'" value="'+increment+'" class="form-control-new input-sm allow-only-numbers" onchange="update_quantity('+item_id+')"><span class="input-group-btn"><button name="button" type="button" class="btn btn-warning btn-sm" onclick="add_quantity('+aRowCount+');">+</button>        </span><span class="input-group-btn"><button name="button" type="button" class="btn btn-danger btn-sm" id="'+aRowCount+'" onclick="delete_itemized_inventory('+aRowCount+');">X</button>&nbsp; </span></div></td></tr>';
        $('#selected_inventory_items').append(op);
             aRowCount++;
             arr++;
            
       
    }
  
    function delete_itemized_inventory(id) {
        if (confirm("Are you sure?")) {
          
               $('#row_'+id).remove();
                $("#extra_selected_inventory_div").hide();
                $(".previous_page").addClass("btn");
                $(".previous_page").text(" ◄ ");
                $(".next_page").addClass("btn");
                $(".next_page").text(" ► ");
        }
    }

    function subtract_quantity(id) {
        var value = $("#quantity_label_" + id).val();
        var floor_value = $("#floor_" + id).val();
        if (value > 1) {
            var quantity_value = parseInt(value) - 1;
            $("#quantity_label_" + id).val(quantity_value);
          
        }
    }

    function add_quantity(id) {
        var value = $("#quantity_label_" + id).val();
        var floor_value = $("#floor_" + id).val();
        var quantity_value = parseInt(value) + 1;
        $("#quantity_label_" + id).val(quantity_value);
       
    }

    function update_quantity(id) {
        var value = $("#quantity_label_" + id).val();
        var floor_value = $("#floor_" + id).val();
        
    }

   

    var xhr = null;
    function search_inventory_items(estimate_id) {
        var term = $("#search_item").val();
      /* if there is a previous ajax request, then we abort it and then set xhr to null */
        if (xhr != null) {
            xhr.abort();
            xhr = null;
        }
       /* xhr = $.get("/estimate/search_inventory_items/", {
            estimate_id: estimate_id,
            search_term: term,
            iframe: true
        }).done(function (data) {
        });*/
    }

    /*---------------------selected_packing_material table start-----------------------------*/
	metrialincrement = 1;
	metrialRowCount=1;
	marr=0;
    function add_packing_material() {		
		 var op ='<tr id="matrow_'+metrialRowCount+'"><td width="10%"><select name="moving_information[materials]['+marr+'][material_id]"   id="packing_material_'+metrialRowCount+'" onchange="update_material('+metrialRowCount+')" class="form-control">';
		 $.each(materialsdata,function(i,val){ 
		   op = op+'<option value="'+val.id+'">'+val.name+'</option>';		    
	      });		 
		 op=op+'</select> </td>  <td width="10%"><div class="input-group input-small"><span class="input-group-btn">  <button name="button" type="button" class="btn btn-warning btn-sm" onclick="subtract_material_quantity('+metrialRowCount+');">-</button></span> <input type="text" name="moving_information[materials]['+marr+'][quantity]" id="material_label_'+metrialRowCount+'" value="'+metrialincrement+'" class="form-control-new input-sm allow-only-numbers" onchange="update_material_quantity('+metrialRowCount+')">  <span class="input-group-btn"> <button name="button" type="button" class="btn btn-warning btn-sm" onclick="add_material_quantity('+metrialRowCount+');">+</button></span> <span class="input-group-btn"> <button name="button" type="button" class="btn btn-danger btn-sm" id="'+metrialRowCount+'" onclick="delete_packing_material('+metrialRowCount+');">X</button> </span></div></td></tr>';
        $('#selected_packing_material').append(op);
         metrialRowCount++;
         marr++;
    }

    function add_material_quantity(id) {
        var value = $("#material_label_" + id).val();
        var material_value = parseInt(value) + 1;
        $("#material_label_" + id).val(material_value);
       
    }

    function subtract_material_quantity(id) {
        var value = $("#material_label_" + id).val();
        var material_value = (parseInt(value) - 1) <= 1 ? 1 : parseInt(value) - 1;
        $("#material_label_" + id).val(material_value);
        
    }

    function update_material(id) {
        var value = $("#packing_material_" + id).val();
        var material_value = (parseInt(value) == 0) ? 1 : parseInt(value);
        
    }

    function update_material_quantity(id) {
        var value = $("#material_label_" + id).val();
        
    }

    function delete_packing_material(id) {
        if (confirm("Are you sure?")) {
               $('#matrow_'+id).remove();
        }
    }
    
   // function InventoryValidate(){
   /* if($('#first_name').val()=="")
    {
        alert('Please enter first name');
        $('#first_name').focus();
        return false;
    }

    if($('#last_name').val()=="")
    {
        alert('Please enter last name');
        $('#last_name').focus();
        return false;
    }

    // if($('#phone_type').val()=="0")
    // {
    //     alert('Please select phone type');
    //     $('#phone_type').focus();
    //     return false;
    // }

    if($('#phone').val()=="")
    {
        alert('Please enter phone number');
        $('#phone').focus();
        return false;
    }

    if($('#email').val()=="")
    {
        alert('Please enter email');
        $('#email').focus();
        return false;
    }

    if($('#referred_by').val()=="0")
    {
        alert('Please select referred by');
        $('#referred_by').focus();
        return false;
    }*/

//}
    /*---------------------selected_packing_material table end-----------------------------*/
</script>
</div>
