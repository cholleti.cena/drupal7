@extends('web.layouts.master')
@section('title')
Home | {{env('APP_NAME')}}
@endsection
@section('content')

<section class="intro">
    <button class="popup-open">Thanks for submitting your information. We will get back to you soon </button>
</section>
<script>
         setTimeout(function(){
            window.location.href = '/';
         }, 5000);
      </script>
@endsection