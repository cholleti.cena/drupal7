<div class="row">
      <div class="col-sm-12 mb-2">
        <div class="mb-1">
          @foreach($metadata['services'] as $service)
          <input type="checkbox" id="additional_services{{$service['id']}}" name="moving_information[additional_services][]" value="{{$service['id']}}">
          <label for="additional_services{{$service['id']}}">{{$service['name']}}</label><br>
          @endforeach
        </div>                          
      </div>

      <div class="col-sm-12 mb-2">
        <div class="mb-1">
          <label for="field5">Please mention any special items or conditions we should be aware of</label>
          <textarea id="w3review" name="w3review" rows="4" cols="50"></textarea>
        </div>
      </div>
</div>