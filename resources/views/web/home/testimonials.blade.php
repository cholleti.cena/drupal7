<!-- slider -->
<div class="container mt-5 mb-5">
	<div class="row">
		<div class="col-sm-12">
			<h3 class="text-center head_m2 mt-5 mb-5">Our <span>learners</span></h3>
  <!-- Swiper -->
  <div class="slider_m1">
  <div class="swiper-container">
    <div class="swiper-wrapper">
    @foreach($testimonials as $testimonial)
      <div class="swiper-slide">
      	  <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <p><img class=" img-fluid" src="{{$testimonial->image}}" alt="card image"></p>
                                    <h4 class="card-title">{{$testimonial->name}}</h4>
                                    <p class="card-text"><?php echo strlen($testimonial->message) > 100 ? substr($testimonial->message,0,100)."...Read More" : $testimonial->message;?></p>
                                <!--     <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a> -->
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title">{{$testimonial->name}}</h4>
                                    <p class="card-text">{{$testimonial->message}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
      </div>
      @endforeach
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
  </div>
</div>
<!-- slider -->
</div>
</div>
</div>