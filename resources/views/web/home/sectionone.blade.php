<div style="position: relative;overflow: hidden;">
      <div class="abst_m1" style=" "></div>
      <div class="abst_m2" style=" "></div>
      <!-- <img src="{{env('SITE_URL')}}/theme/web/img/tablet.png" class="abst_m3" > -->
       <img src="{{env('SITE_URL')}}/theme/web/img/search-job.png" class="abst_m4" >
      
<div class="container animatedParent " data-sequence='1000' >
       
           <!-- set -->
            <div class="row mt-5 mb-5">
                <?php 
                $urls = array("/about-us");
                if (!in_array($_SERVER['REQUEST_URI'], $urls)) {?>
            	 <div class="col-sm-6 align-self-center animated fadeInLeft " data-id='1'>
            	 	 <h3 class=" head_m2 mt-5 mb-3">About <span>Us.</span></h3>
            	 	 <p class="content_m1">
                        <?php 
                            if (strlen($default->about_us) <=291) {
                              echo $default->about_us;
                            } else {
                              echo substr($default->about_us, 0, 291) . '...';
                            }
                        ?>
                        <a href="/about-us" style="text-decoration:none">Read More</a></p>
            	 </div>
                 <div class="col-sm-6  animated fadeInRight " data-id='2' id="">
                    <img src="{{env('SITE_URL')}}/theme/web/img/10132.png" class="img-fluid">
                 </div>
                <?php } else {?>
                    <div class="col-sm-12 align-self-center animated fadeInLeft " data-id='3' id="">
                     <h3 class=" head_m2 mt-5 mb-3">About <span>Us.</span></h3>
                     <p class="content_m1">
                        <?php 
                              echo $default->about_us;
                        ?>
                      </p>
                 </div>
                <?php } ?>
            </div>
            <!-- set -->

             <!-- set -->
            <div class="row  mt-5 mb-5">
            	 <div class="col-sm-6  animated fadeInLeft " data-id='4' id="">
                <img src="{{$default->who_we_are_image}}" class="img-fluid">
            	 </div>
            	 <div class="col-sm-6 align-self-center  animated fadeInRight " data-id='5' id="">
            	 	 <h3 class=" head_m2 mt-5 mb-3">Who <span>we are</span></h3>
            	 	 <p class="content_m1">{!!$default->who_we_are!!}</p>
            	 	
            	 </div>
            	
            </div>
            <!-- set -->
  </div>
</div>

<div style="position: relative;overflow: hidden;" >
      <div class="abst_m1" style=" "></div>
      <div class="abst_m2" style=" "></div>
      <img src="{{env('SITE_URL')}}/theme/web/img/tablet.png" class="abst_m3" >
       <img src="{{env('SITE_URL')}}/theme/web/img/search-job.png" class="abst_m4" >
<div class="container animatedParent " data-sequence='1000'>
           
           <!-- set -->
            <div class="row mt-5 mb-5">
            	 <div class="col-sm-6 align-self-center animated fadeInLeft " data-id='5' id="">
            	 	 <h3 class=" head_m2 mt-5 mb-3"> <span>Vision</span></h3>
            	 	 <p class="content_m1">{!!$default->vision!!}</p>
            	 </div>
            	 <div class="col-sm-6 animated fadeInRight " data-id='6' id="">
            	 	<img src="{{$default->vision_image}}" class="img-fluid">
            	 </div>
            </div>
            <!-- set -->

             <!-- set -->
            <div class="row  mt-5 mb-5">
            	 <div class="col-sm-6 animated fadeInLeft " data-id='7' id="">
            	 	<img src="{{$default->mission_image}}" class="img-fluid">
            	 </div>
            	 <div class="col-sm-6 align-self-center animated fadeInRight " data-id='8' id="">
            	 	 <h3 class=" head_m2 mt-5 mb-3"><span>Mission</span></h3>
            	 	 <p class="content_m1">{!!$default->mission!!}</p>
            	 	
            	 </div>
            	
            </div>
            <!-- set -->
  </div>
</div>

<div style="position: relative;overflow: hidden;">
      <div class="abst_m1" style=" "></div>
      <div class="abst_m2" style=" "></div>
      <img src="{{env('SITE_URL')}}/theme/web/img/tablet.png" class="abst_m3" >
       <img src="{{env('SITE_URL')}}/theme/web/img/corevalue.png" class="abst_m4" >

<div class="container animatedParent " data-sequence='1000'>
           
           <!-- set -->
            <div class="row mt-5 mb-5">
            	 <div class="col-sm-6 align-self-center animated fadeInLeft " data-id='9'>
            	 	 <h3 class=" head_m2 mt-5 mb-3"> <span>Objective</span></h3>
            	 	 <p class="content_m1">{!!$default->objective!!}</p>
            	 </div>
            	 <div class="col-sm-6 animated fadeInRight " data-id='10' id="">
            	 	<img src="{{$default->objective_image}}" class="img-fluid">
            	 </div>
            </div>
            <!-- set -->

             <!-- set -->
            <div class="row  mt-5 mb-5">
            	 <div class="col-sm-6  animated fadeInLeft" data-id='10'>
            	 	<img src="{{$default->our_value_image}}" class="img-fluid">
            	 </div>
            	 <div class="col-sm-6 align-self-center  animated fadeInRight" data-id='11'>
            	 	 <h3 class=" head_m2 mt-5 mb-3">Our <span>Value</span></h3>
            	 	 <p class="content_m1">{!!$default->our_value!!}</p>
            	 	
            	 </div>
            	
            </div>
            <!-- set -->
  </div>
</div>
  
  <style type="text/css">
      .animation {
          position: relative;
          animation: mymove 5s infinite;
        }
    #div1 {animation-timing-function: linear;}

    @keyframes mymove {
      from {left: 0px;}
      to {left: 50px;}
    }
  </style>