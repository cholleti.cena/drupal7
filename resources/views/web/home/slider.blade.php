<section class="intro">
    <button class="popup-open"> Request Estimate </button>
    <div class="popup-wrapper">
      <div class="popup">
        <button class="close-btn"></button>
          <div class="row justify-content-center">
              <div class="col-md-12 col-lg-10">
                <form method="POST" action="{{URL::to('postLead')}}" accept-charset="UTF-8" onsubmit="return EstimateValidate();" id="registration">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <nav>
                  <div class="nav nav-pills nav-fill" id="nav-tab" role="tablist">
                    <a class="nav-link active" id="step1-tab" data-bs-toggle="tab" href="#step1"> Contact Move Info</a>
                    <a class="nav-link" id="step2-tab" data-bs-toggle="tab" href="#step2"> Inventory</a>
                    <a class="nav-link" id="step3-tab" data-bs-toggle="tab" href="#step3"> Additional Services</a>
                  </div>
                </nav>

                <div class="tab-content py-4">
                    <div class="tab-pane fade show active" id="step1">
                      <h4>Contact Information</h4>
                      @include('web.home.contact_information')
                    </div>

                    <div class="tab-pane fade" id="step2">
                      <h4>Inventory Information</h4>
                      @include('web.home.inventory_information')
                    </div>

                    <div class="tab-pane fade" id="step3">
                      <h4>Additional Services</h4>
                      @include('web.home.services_information')
                      {{ Form::submit('Get Estimate', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
                {!! Form::close() !!}
              </div>
         </div>
      </div>
    </div>
</section>


<script type="text/javascript">

function EstimateValidate()
{
    if($('#first_name').val()=="")
    {
        alert('Please enter first name');
        $('#first_name').focus();
        return false;
    }

    if($('#last_name').val()=="")
    {
        alert('Please enter last name');
        $('#last_name').focus();
        return false;
    }

    // if($('#phone_type').val()=="0")
    // {
    //     alert('Please select phone type');
    //     $('#phone_type').focus();
    //     return false;
    // }

    if($('#phone').val()=="")
    {
        alert('Please enter phone number');
        $('#phone').focus();
        return false;
    }

    if($('#email').val()=="")
    {
        alert('Please enter email');
        $('#email').focus();
        return false;
    }

    if($('#referred_by').val()=="0")
    {
        alert('Please select referred by');
        $('#referred_by').focus();
        return false;
    }

    if($('#approximate_move_date').val()=="")
    {
        alert('Please select move date');
        $('#approximate_move_date').focus();
        return false;
    }

    if($('#type_of_service').val()=="0")
    {
        alert('Please select type of service');
        $('#type_of_service').focus();
        return false;
    }

    if($('#origin_address').val()=="")
    {
        alert('Please enter origin address');
        $('#origin_address').focus();
        return false;
    }

    if($('#destination_property_type_id').val()=="0")
    {
        alert('Please select property');
        $('#destination_property_type_id').focus();
        return false;
    }

    if($('#origin_floor').val()=="0")
    {
        alert('Please select floor');
        $('#origin_floor').focus();
        return false;
    }

    if($('#destination_address').val()=="")
    {
        alert('Please enter destination address');
        $('#destination_address').focus();
        return false;
    }

    if($('#destination_property_type_id').val()=="0")
    {
        alert('Please select property');
        $('#destination_property_type_id').focus();
        return false;
    }

    if($('#destination_floor').val()=="0")
    {
        alert('Please select floor');
        $('#destination_floor').focus();
        return false;
    }

    if($('#square_footage').val()=="")
    {
        alert('Please enter square footage');
        $('#square_footage').focus();
        return false;
    }

    if($('#estimated_boxes').val()=="")
    {
        alert('Please enter estimated boxes');
        $('#estimated_boxes').focus();
        return false;
    }

    if($('#move_size_id').val()=="0")
    {
        alert('Please select move size');
        $('#move_size_id').focus();
        return false;
    }
}
</script>