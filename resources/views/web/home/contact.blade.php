@extends('web.layouts.master')
@section('title')
Contact Us | {{env('APP_NAME')}}
@endsection
@section('content')

<div class="bg-light">
  <div class="container py-5">
    <div class="row  align-items-center py-5">
      <div class="col-lg-6">
        <h1 class="display-4">Contact Us</h1>
        <p class="lead text-muted mb-0"></p>
      
      </div>
      <div class="col-lg-6 d-none d-lg-block"><img src="{{env('SITE_URL')}}/theme/web/img/contact-us.png" alt="" class="img-fluid"></div>
    </div>

<div class="row">
	
    	<div class="col-sm-6">
    	<iframe width="100%" height="420px;" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJaY32Qm3KWTkRuOnKfoIVZws&key=AIzaSyAf64FepFyUGZd3WFWhZzisswVx2K37RFY" allowfullscreen></iframe>
    	</div>

    	<div class="col-sm-6">
    		<form id="formContact">
	<input type="hidden" name="action" value="contact_us">
<!-- input -->
	<div class="wrap-input100 validate-input" >
						<input class="input100" type="text" name="name" id="name" placeholder="Name" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>
<!-- input -->

	
<!-- input -->
	<div class="wrap-input100 validate-input" >
						<input class="input100" type="text" name="email" id="email" placeholder="Email" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>
<!-- input -->


<!-- input -->
	<div class="wrap-input100 validate-input" >
						<input class="input100" type="text" name="phone" id="phone" placeholder="Mobile Number" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-phone" aria-hidden="true"></i>
						</span>
					</div>
<!-- input -->

<!-- input -->
	<div class="wrap-input100 validate-input" >
						 <textarea class="input100" placeholder="Enter your message here" name="message" id="message"  rows="4" style="height: auto;    padding-top: 23px;"></textarea>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-comment" aria-hidden="true"></i>
						</span>
					</div>
<!-- input -->


<button class="login100-form-btn">Submit</button>

</form>
    	</div>
   </div>
   	<div class="row">
        <!-- Boxes de Acoes -->
    	<div class="col-xs-12 col-sm-6 col-lg-4">
			<div class="box gride_m4">							
				<div class="icon">
					<div class="image"><i class="fa fa-envelope" aria-hidden="true"></i></div>
					<div class="info">
						<h3 class="title">MAIL & WEBSITE</h3>
						<p>
							<i class="fa fa-envelope" aria-hidden="true"></i> &nbsp gondhiyahardik6610@gmail.com
							<br>
							<br>
							<i class="fa fa-globe" aria-hidden="true"></i> &nbsp www.hardikgondhiya.com
						</p>
					
					</div>
				</div>
				<div class="space"></div>
			</div> 
		</div>
			
        <div class="col-xs-12 col-sm-6 col-lg-4">
			<div class="box gride_m4">							
				<div class="icon">
					<div class="image"><i class="fa fa-mobile" aria-hidden="true"></i></div>
					<div class="info">
						<h3 class="title">CONTACT</h3>
    					<p>
							<i class="fa fa-mobile" aria-hidden="true"></i> &nbsp (+91)-9624XXXXX
							<br>
							<br>
							<i class="fa fa-mobile" aria-hidden="true"></i> &nbsp  (+91)-756706XXXX
						</p>
					</div>
				</div>
				<div class="space"></div>
			</div> 
		</div>
			
        <div class="col-xs-12 col-sm-6 col-lg-4">
			<div class="box gride_m4">							
				<div class="icon">
					<div class="image"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
					<div class="info">
						<h3 class="title">ADDRESS</h3>
    					<p>
							 <i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp 15/3 Junction Plot 
							 "Shree Krishna Krupa", Rajkot - 360001.
						</p>
					</div>
				</div>
				<div class="space"></div>
			</div> 
		</div>		    
		<!-- /Boxes de Acoes -->
		
		<!--My Portfolio  dont Copy this -->
	    
	</div>


  </div>
</div>

<script type="text/javascript">
    var URL = '<?php echo env('SITE_URL');?>';

    $(document).ready(function(){
    $("#formContact").submit(function(event){
        event.preventDefault(); //prevent default action 
        var post_url = URL+'/api/verify'; 
        var form_data = $(this).serialize(); 
        	$.post( post_url, form_data, function( response ) 
        	{
	          if(response.status_code == 200)
	          {
	          	alert(response.message);
	          	$('#formContact').trigger("reset");
	          	return true;
	          }
	          else
	          {
	            alert(response.message);
	            return false;            
	          }
	        });
    	});
    });
</script>
@endsection