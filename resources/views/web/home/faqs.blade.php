@extends('web.layouts.master')
@section('title')
FAQS | {{env('APP_NAME')}}
@endsection
@section('content')

@include('web.layouts.sidemenu')

<style type="text/css">
  body{
        background: #eeeeee!important;
  }
  .header{
        background: linear-gradient(
256deg
, rgb(7 59 139) 0%, rgb(255 255 255) 64%) !important;
  }
  .navbar-light .navbar-nav .nav-link {
    color: #fff !important;
}
</style>


<div class="admin-wrap">
    <div class="container-fluid">
       <div class="row">
          <div class="col-sm-12 mb-3"><h3 class="head_m4">Frequently Asked Questions</h3></div>

   @foreach($getfaqs as $key => $faq)
		<div class="col-md-12">

			<div class="accordion accordion_m1" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapseOne">
         {{$faq->question}}
        </button>
      </h2>
    </div>

    <div id="collapse{{$key}}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
      {{$faq->answer}}
      </div>
    </div>
  </div>
</div>
			<!-- <figure class="">
				<figcaption class="info-wrap">
						<h5 class=" head_m2 mt-5 mb-3"><span></span></h5>
						<p class="desc">{{$faq->answer}}</p>
				</figcaption>
				 
			</figure> -->
		</div>
		@endforeach  
          </div>
      </div>
  </div>



@endsection