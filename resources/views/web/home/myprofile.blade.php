@extends('web.layouts.master')
@section('title')
My Profile | {{env('APP_NAME')}}
@endsection
@section('content')

@include('web.layouts.sidemenu')

<div class="admin-wrap">
    <div class="container-fluid">
       <div class="row">
          <div class="col-sm-12 mb-3"><h3 class="head_m4">My Profile</h3></div>
            <div class="col-sm-6">
              <div class="gride_m3">
                <div class="row">
                    <div class="col-sm-12"><h5 class="head_m5">Personal Info</h5></div>
                    <input type="hidden" name="user_id" id="user_id" value="{{$userdetails['id']}}">
                    <!-- input -->
                    <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">First Name</label>
                          <input type="text" class="form-control" name="first_name" id="first_name" value="{{$userdetails['first_name']}}" placeholder="Enter">
                         </div>
                    </div>
                    <!-- input -->
                    <!-- input -->
                    <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">Last Name</label>
                          <input type="text" class="form-control" name="last_name" id="last_name"  value="{{$userdetails['last_name']}}" placeholder="Enter">
                         </div>
                    </div>
                    <!-- input -->
                    <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">Email</label>
                          <input type="text" class="form-control" name="email" id="email" value="{{$userdetails['email']}}" placeholder="Enter">
                         </div>
                    </div>
                    <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">Phone</label>
                          <input type="text" class="form-control" name="phone" id="phone" value="{{$userdetails['phone']}}"  placeholder="Enter">
                         </div>
                    </div>
                    <div class="col-sm-6 text-right">
                    </div>
                    <div class="col-sm-6 text-right">
                        <button type="button" class="btn btn-success bor_1 mr-2" onclick="updateProfile()">Update</button>
                    </div>                
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="gride_m3">
                <div class="row">
                    <div class="col-sm-12"><h5 class="head_m5">Profile Pic</h5></div>
                    <input type="hidden" name="user_id" id="user_id" value="{{$userdetails['id']}}">
                    <!-- input -->
                    <div class="col-sm-6">
                          <div id="msg"></div>
                          <form method="post" id="image-form">
                            <input type="file" name="img[]" class="file" accept="image/*">
                            <div class="input-group my-3">
                              <input type="text" class="form-control" disabled placeholder="Upload File" id="file">
                              <div class="input-group-append">
                                <button type="button" class="browse btn btn-primary">Browse...</button>
                              </div>
                            </div>
                          </form>
                    </div>
                    <div class="col-sm-6">
                        <?php 
                            $logo_path = '';
                            $no_image=env('NO_PROFILE_IMAGE');
                            if(!empty($userdetails['profile_pic']))
                            {
                                $logo_path = $userdetails['profile_pic'];
                            }
                            else
                            {
                                $logo_path = $no_image;
                            }
                        ?>
                      <img src="{{$logo_path}}" id="preview" class="imgthum">
                    </div>
                    <!-- input -->
                    <!-- input -->            
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="gride_m3">
                <div class="row">
                    <div class="col-sm-12"><h5 class="head_m5">Basic Info</h5></div>
                    <input type="hidden" name="user_id" id="user_id" value="{{$userdetails['id']}}">
                    <!-- input -->
                    <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">Gender</label>
                            <select class="form-control" name="gender" id="gender">
                                <option value="0"></option>
                                <?php foreach ($general['configtypes'] as $key => $configtype) 
                                { 
                                    if($configtype->category_name === "Gender")
                                    {
                                     foreach ($configtype->configlables as $key => $gender) {
                                    ?>

                                    <option value="{{$gender->id}}" @if($userdetails['basic_details']['gender'] == $gender->id) selected="selected" @endif>{{$gender->name}}</option>
                               <?php  }} } ?>
                            </select>
                         </div>
                    </div>
                    <!-- input -->
                    <!-- input -->
                    <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">Date of birth</label>
                            <input type="date" class="form-control" name="dob" id="dob" value="{{$userdetails['basic_details']['dob']}}"  placeholder="Enter">
                         </div>
                    </div>
                    <!-- input -->
                    <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">Marital Status</label>
                            <select class="form-control" name="marital_status" id="marital_status">
                                <option value="0"></option>
                                <?php foreach ($general['configtypes'] as $key => $configtype) 
                                { 
                                    if($configtype->category_name === "Marital status")
                                    {
                                     foreach ($configtype->configlables as $key => $marital) {
                                    ?>

                                    <option value="{{$marital->id}}" @if($userdetails['basic_details']['marital_status'] == $marital->id) selected="selected" @endif>{{$marital->name}}</option>
                               <?php  }} } ?>
                            </select>
                         </div>
                    </div>
                     <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">Nationality</label>
                            <select class="form-control" name="nationality_id" id="nationality_id">
                                <option value="0"></option>
                                <?php foreach ($general['configtypes'] as $key => $configtype) 
                                { 
                                    if($configtype->category_name === "Nationality")
                                    {
                                     foreach ($configtype->configlables as $key => $nationality) {
                                    ?>

                                <option value="{{$nationality->id}}" @if($userdetails['basic_details']['nationality_id'] == $nationality->id) selected="selected" @endif>{{$nationality->name}}</option>
                               <?php  }} } ?>
                            </select>
                         </div>
                    </div>
                    <div class="col-sm-12">
                         <div class="form-group input_m2">
                          <label for="">Address</label>
                          <textarea class="form-control" name="address" id="address" placeholder="Enter" rows="4">{{$userdetails['basic_details']['address']}}</textarea>
                         </div>
                    </div>
                    <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">City</label>
                            <select class="form-control" name="city_id" id="city_id">
                                <option value="0"></option>
                                <?php foreach ($general['cities'] as $key => $city) 
                                { ?>
                                    <option value="{{$city->id}}" 
                                        @if($userdetails['basic_details']['city_id'] == $city->id) selected="selected" @endif>{{$city->city}}</option>
                               <?php  } ?>
                            </select>
                         </div>
                    </div>
                     <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">State</label>
                            <select class="form-control" name="state_id" id="state_id">
                                <option value="0"></option>
                                <?php foreach ($general['states'] as $key => $state) 
                                { ?>
                                    <option value="{{$state->id}}" @if($userdetails['basic_details']['state_id'] == $state->id) selected="selected" @endif>{{$state->state}}</option>
                               <?php  } ?>
                            </select>
                         </div>
                    </div>
                     <div class="col-sm-12">
                         <div class="form-group input_m2">
                          <label for="">Pin Code</label>
                            <input type="text" class="form-control" name="zipcode" id="zipcode" value="{{$userdetails['basic_details']['zipcode']}}" placeholder="Enter">
                         </div>
                    </div>
                    <div class="col-sm-6 text-right">
                    </div>
                    <div class="col-sm-6 text-right">
                        <button type="button" class="btn btn-success bor_1 mr-2" onclick="updateBasicInfo()">Update</button>
                    </div>                
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="gride_m3">
                <div class="row">
                    <div class="col-sm-12"><h5 class="head_m5">Education Info</h5></div>
                    <input type="hidden" name="user_id" id="user_id" value="{{$userdetails['id']}}">
                    <?php foreach ($general['educations'] as $key => $eds) 
                    { ?>
                    <div class="col-sm-12"><h5 class="head_m5">{{$eds->education_name}}</h5></div>
                    <input type="hidden" name="education_id[]" id="education_id_{{$eds->id}}" value="{{$eds->id}}">
                    <!-- input -->
                    <?php 
                      $edds = array(5,4,3);
                      if (in_array($eds->id, $edds)) {?>
                    <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">Department</label>
                            <select class="form-control" name="course_id[]" id="course_id_{{$eds->id}}">
                                <option value="0"></option>                        
                            </select>
                         </div>
                    </div>
                    <?php } else {?>
                        <input type="hidden" name="course_id[]" id="course_id_{{$eds->id}}" value="0">
                    <?php } ?>
                    <!-- input -->
                    <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">Passedout Year</label>
                            <select class="form-control" name="passing_year[]" id="passing_year_{{$eds->id}}">
                                <option value="0"></option>
                                <?php foreach ($general['years'] as $key => $year) 
                                { ?>
                                    <option value="{{$year->id}}">{{$year->year}}</option>
                               <?php  } ?>
                            </select>
                         </div>
                    </div>

                    <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">Register Number</label>
                          <input type="text" class="form-control" name="register_number[]" id="register_number_{{$eds->id}}" value=""  placeholder="Enter">
                         </div>
                    </div>
                    <div class="col-sm-6">
                         <div class="form-group input_m2">
                          <label for="">Marks Percentage</label>
                          <input type="text" class="form-control" name="percentage[]" id="percentage_{{$eds->id}}" value=""  placeholder="Enter">
                         </div>
                    </div>                    
                  <?php } ?> 
                  <div class="col-sm-6 text-right">
                    </div>
                    <div class="col-sm-6 text-right">
                        <button type="button" class="btn btn-success bor_1 mr-2" onclick="updateEducation()">Update</button>
                    </div>               
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .file {
  visibility: hidden;
  position: absolute;
}
.imgthum{
    padding: .25rem;
    background-color: #fff;
    border: 1px solid #dee2e6;
    border-radius: .25rem;
    max-width: 100%;
    height: 193px;
}
</style>

<script type="text/javascript">
function updateProfile()
{
    var user_id = $('#user_id').val();
    var first_name = $('#first_name').val();
    var last_name = $('#last_name').val();
    var email = $('#email').val();
    var phone = $('#phone').val();

    var URL = '<?php echo env('SITE_URL');?>';
    var user_data = JSON.parse(localStorage.getItem('user_data'));
    //console.log(user_data.data.access_token);
    if(user_id == 0 || user_data == null)
    {
        alert('Please login first');
        var redirecturl = URL+'/login';
        window.location.href = redirecturl;
    }
    else
    {
        $.ajax({
            type: "POST",
            url: URL+"/api/updateProfile",
            headers: {"Authorization": 'Bearer '+user_data.data.access_token , 'Accept': 'application/json'},
            data:{
                  type:'personal',
                  first_name:first_name,
                  last_name:last_name,
                  email:email,
                  phone:phone
            },
            success: function(response)
            {           
               console.log( "Data Saved: " + response);
               if(response.status_code == 200)
               {
                    alert(response.message);                    
               }
               else
               {
                    alert(response.message);
               }
               var redirecturl = URL+'/my-profile';
               window.location.href = redirecturl;
            }
        });
    }
}

$(document).on("click", ".browse", function() {
  var file = $(this).parents().find(".file");
  file.trigger("click");
});
$('input[type="file"]').change(function(e) {
  var fileName = e.target.files[0].name;
  $("#file").val(fileName);
  var URL = '<?php echo env('SITE_URL');?>';
  var user_data = JSON.parse(localStorage.getItem('user_data'));
  var reader = new FileReader();
  reader.onload = function(e) {
    // get loaded data and render thumbnail.
    document.getElementById("preview").src = e.target.result;
    $.ajax({
            type: "POST",
            url: URL+"/api/updateProfile",
            headers: {"Authorization": 'Bearer '+user_data.data.access_token , 'Accept': 'application/json'},
            data:{
                  type:'profile_pic',
                  profile_pic:e.target.result
            },
            success: function(response)
            {           
               console.log( "Data Saved: " + response);
               if(response.status_code == 200)
               {
                    alert(response.message);                    
               }
               else
               {
                    alert(response.message);
               }
               var redirecturl = URL+'/my-profile';
               window.location.href = redirecturl;
            }
    });
  };
  // read the image file as a data URL.
  reader.readAsDataURL(this.files[0]);
});

function updateBasicInfo() 
{
    var user_id = $('#user_id').val();
    var gender = $('#gender').val();
    var dob = $('#dob').val();
    var marital_status = $('#marital_status').val();
    var nationality_id = $('#nationality_id').val();
    var address = $('#address').val();
    var city_id = $('#city_id').val();
    var state_id = $('#state_id').val();
    var zipcode = $('#zipcode').val();

    var URL = '<?php echo env('SITE_URL');?>';
    var user_data = JSON.parse(localStorage.getItem('user_data'));
    //console.log(user_data.data.access_token);
    if(user_id == 0)
    {
        alert('Please login first');
    }
    else
    {
        $.ajax({
            type: "POST",
            url: URL+"/api/updateProfile",
            headers: {"Authorization": 'Bearer '+user_data.data.access_token , 'Accept': 'application/json'},
            data:{
                  type:'basic',
                  gender:gender,
                  dob:dob,
                  marital_status:marital_status,
                  nationality_id:nationality_id,
                  address:address,
                  city_id:city_id,
                  state_id:state_id,
                  zipcode:zipcode
            },
            success: function(response)
            {           
               console.log( "Data Saved: " + response);
               if(response.status_code == 200)
               {
                    alert(response.message);                    
               }
               else
               {
                    alert(response.message);
               }
               var redirecturl = URL+'/my-profile';
               window.location.href = redirecturl;
            }
        });
    }
}

// var mynewJson = JSON.parse(myJson);
// $('#state_id').on('change', function(){
//   console.log($('#state_id').val());
//   var mynewJson = JSON.stringify(JSON.parse(myJson),null,2);
//   for(var i = 0; i < mynewJson.length; i++)
//   {
//     console.log(mynewJson[i].cities);
//     if(mynewJson[i].cities.state_id == $(this).val())
//     {
//        $('#model').html('<option value="000">Cities</option>');
//        $.each(myJson[i].cities, function (index, value) {
//           $("#model").append('<option value="'+value.id+'">'+value.city+'</option>');
//       });
//     }
//   }
// });

var myJson = '<?php echo $general['educations'];?>';
var departments = '<?php echo $userdetails['education_details'];?>';
var mynewJson = JSON.parse(myJson);
var mydepartments = JSON.parse(departments);

for(var i = 0; i < mynewJson.length; i++)
{
 for(var d = 0; d < mydepartments.length; d++)
 {  
   if(mydepartments[d].course_id !== null)
   {
    $('#course_id_'+mydepartments[d].education_id.id+' option[value="'+mydepartments[d].course_id.id+'"]').attr("selected", "selected");
    }
   $('#passing_year_'+mydepartments[d].education_id.id+' option[value="'+mydepartments[d].passing_year.id+'"]').attr("selected", "selected");
   $('#register_number_'+mydepartments[d].education_id.id).val(mydepartments[d].register_number);
   $('#percentage_'+mydepartments[d].education_id.id).val(mydepartments[d].percentage);
 }
 for(var j = 0; j < mynewJson[i].courses.length; j++)
 {
    if(mynewJson[i].courses[j].education_id == mynewJson[i].id)
    {
         $.each(mynewJson[i].courses, function (index, value) {
              $("#course_id_"+mynewJson[i].id).append('<option value="'+value.id+'">'+value.name+'</option>');
      });
    }
 }
}

function updateEducation()
{
    var user_id = $('#user_id').val();
    var educations = [];
    for(var i = 0; i < mynewJson.length; i++)
    {
        educations.push(
            {
                education_id: $('#education_id_'+mynewJson[i].id).val(),
                course_id: $('#course_id_'+mynewJson[i].id).val(),
                passing_year : $('#passing_year_'+mynewJson[i].id).val(),
                register_number : $('#register_number_'+mynewJson[i].id).val(),
                percentage : $('#percentage_'+mynewJson[i].id).val(),
            }
        );
    }
    
    var URL = '<?php echo env('SITE_URL');?>';
    var user_data = JSON.parse(localStorage.getItem('user_data'));
    //console.log(user_data.data.access_token);
    if(user_id == 0)
    {
        alert('Please login first');
    }
    else
    {
        $.ajax({
            type: "POST",
            url: URL+"/api/updateProfile",
            headers: {"Authorization": 'Bearer '+user_data.data.access_token , 'Accept': 'application/json'},
            data:{
                  type:'educations',
                  educations:educations
            },
            success: function(response)
            {           
               console.log( "Data Saved: " + response);
               if(response.status_code == 200)
               {
                    alert(response.message);                    
               }
               else
               {
                    alert(response.message);
               }
               var redirecturl = URL+'/my-profile';
               window.location.href = redirecturl;
            }
        });
    }
}
</script>
@endsection