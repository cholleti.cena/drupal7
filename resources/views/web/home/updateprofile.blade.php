@extends('web.layouts.master')
@section('title')
My Profile | {{env('APP_NAME')}}
@endsection
@section('content')

@include('web.layouts.sidemenu')
<!-- <style type="text/css">
	body{
		background: #f6f6f6;
	}
</style> -->
<div class="admin-wrap ">	
	
		 
            
            <div class="container-fluid">
        <div class="row">
        	<div class="col-sm-12 mb-3"><h3 class="head_m4">My Profile</h3></div>
         <div class="col-sm-12">
         	<div class="gride_m3">
         		<div class="row">
         			<div class="col-sm-12"><h5 class="head_m5">Personal Info</h5></div>
         			
         			<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">Your Name</label>
                          <input type="text" class="form-control" placeholder="Enter">
                         </div>
         			</div>
         			<!-- input -->
         			<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">Last name</label>
                          <input type="text" class="form-control" placeholder="Enter">
                         </div>
         			</div>
         			<!-- input -->

         			<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">Email</label>
                          <input type="text" class="form-control" placeholder="Enter">
                         </div>
         			</div>
         			<!-- input -->
         			<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">Phone Number</label>
                          <input type="text" class="form-control" placeholder="Enter">
                         </div>
         			</div>
         			<!-- input -->
         			<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">Select Degree</label>
                            <select class="form-control">
                            	<option>Select</option>
                            	<option>Select</option>
                            	<option>Select</option>
                            </select>
                         </div>
         			</div>
         			<!-- input -->
         				<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">Select Department</label>
                            <select class="form-control">
                            	<option>Select</option>
                            	<option>Select</option>
                            	<option>Select</option>
                            </select>
                         </div>
         			</div>
         			<!-- input -->

         				<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">Select Passedout Year</label>
                            <select class="form-control">
                            	<option>Select</option>
                            	<option>Select</option>
                            	<option>Select</option>
                            </select>
                         </div>
         			</div>
         			<!-- input -->

         			<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">Register Number</label>
                           <input type="text" class="form-control" placeholder="Enter">
                         </div>
         			</div>
         			<!-- input -->

         			<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">College Name</label>
                           <input type="text" class="form-control" placeholder="Enter">
                         </div>
         			</div>
         			<!-- input -->

         			<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">CGPA</label>
                           <input type="text" class="form-control" placeholder="Enter">
                         </div>
         			</div>
         			<!-- input -->

         			<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">Profile Image</label>
                           <input type="file" class="form-control" placeholder="Enter">
                         </div>
         			</div>
         			<!-- input -->

         			<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">Resume</label>
                           <input type="file" class="form-control" placeholder="Enter">
                         </div>
         			</div>
         			<!-- input -->

         			<!-- input -->
         			<div class="col-sm-12">
         				 <div class="form-group input_m2">
                          <label for="">Self intro</label>
                           <textarea class="form-control" placeholder="Tell us something about you."></textarea>
                         </div>
         			</div>
         			<!-- input -->
         		</div>
         	</div>
       </div>

        <div class="col-sm-12">
         	<div class="gride_m3">
         		<div class="row">
         			<div class="col-sm-12"><h5 class="head_m5">Other Details</h5></div>
         			
         			<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">Current City</label>
                          <input type="text" class="form-control" placeholder="Enter">
                         </div>
         			</div>
         			<!-- input -->
         		
         			<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">Gender</label>
                            <select class="form-control">
                            	<option>Select</option>
                            	<option>Select</option>
                            	<option>Select</option>
                            </select>
                         </div>
         			</div>
         			<!-- input -->
         				<!-- input -->
         			<div class="col-sm-4">
         				 <div class="form-group input_m2">
                          <label for="">No. of Arrears</label>
                          <input type="text" class="form-control" placeholder="Enter">
                         </div>
         			</div>
         			<!-- input -->

         		</div>
         	</div>
       </div>

       <div class="col-sm-12 mt-5 mb-5 text-center">
       	<button type="button" class="btn btn-success bor_1 mr-2">Submit</button>
         <button type="button" class="btn btn-outline-success bor_1">Go to Profile</button>

       </div>
    </div>
        </div>
   </div>
</div> 
@endsection