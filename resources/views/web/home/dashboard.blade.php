@extends('web.layouts.master')
@section('title')
Dashboard | {{env('APP_NAME')}}
@endsection
@section('content')

@include('web.layouts.sidemenu')


<style type="text/css">
  body{
        background: #eeeeee!important;
  }
  .header{
        background: linear-gradient(
256deg
, rgb(7 59 139) 0%, rgb(255 255 255) 64%) !important;
  }
  .navbar-light .navbar-nav .nav-link {
    color: #fff !important;
}


</style>

<div class="admin-wrap">
    <div class="container-fluid">
       <div class="row">
          <div class="col-sm-12 mb-3"><h3 class="head_m4">Dashboard</h3></div>

      <div class="offset-sm-9 col-sm-3 mt-1 mb-3">
         <form class="form-inline search_m1">
        <i class="fa fa-search" aria-hidden="true"></i>
        <input class="form-control form-control-sm ml-3 w-75 live-search-box" type="text" placeholder="Search" aria-label="Search">
      </form>
       </div>
</div>

<div class="row">

            <div class="col-sm-6">
              <div class="gride_m3">
                <div class="row">
                    <div class="col-sm-12"><h5 class="head_m5">Portfolio links</h5></div>
                    
                    <div class="col-sm-6">
                          <h6>NIL</h6>
                    </div>
                 </div>
              </div>
            </div>

                        <div class="col-sm-6">
              <div class="gride_m3">
                <div class="row">
                    <div class="col-sm-12"><h5 class="head_m5">Profile Views</h5></div>
                    
                    <div class="col-sm-6">
                          <h6>NIL</h6>
                    </div>
                 </div>
              </div>
            </div>

             <div class="col-sm-6">
              <div class="gride_m3">
                <div class="row">
                    <div class="col-sm-12"><h5 class="head_m5">Profile Views</h5></div>
                    
                    <div class="col-sm-12">
                           <table class="w-100">
                             <tbody>
                               <tr>
                                 <td><h6>Total no. of Profile Views:  </h6></td>
                                 <td style="width: 40%">25</td>
                               </tr>
                               <tr>
                                 <td><h6>No. of Profile Views by Companies: </h6></td>
                                 <td style="width: 40%">25</td>
                               </tr>
                                <tr>
                                 <td><h6>No. of Profile Views in Last month: </h6></td>
                                 <td style="width: 40%">25</td>
                               </tr>
                             </tbody>
                           </table>
                    </div>
                 </div>
              </div>
            </div>

             <div class="col-sm-6">
              <div class="gride_m3">
                <div class="row">
                    <div class="col-sm-12"><h5 class="head_m5">Hackathons & Competitions</h5></div>
                    <div class="col-sm-6">
                          <h6>NIL</h6>
                    </div>
                 </div>
              </div>
            </div>

             <div class="col-sm-6">
              <div class="gride_m3">
                <div class="row">
                    <div class="col-sm-12"><h5 class="head_m5">Work Experience
</h5></div>
                    <div class="col-sm-6">
                          <h6>NIL</h6>
                    </div>
                 </div>
              </div>
            </div>


            <div class="col-sm-6">
              <div class="gride_m3">
                <div class="row">
                    <div class="col-sm-12"><h5 class="head_m5">Awards & Achievements</h5></div>
                    <div class="col-sm-6">
                          <h6>NIL</h6>
                    </div>
                 </div>
              </div>
            </div>


             <div class="col-sm-6">
              <div class="gride_m3">
                <div class="row">
                    <div class="col-sm-12"><h5 class="head_m5">Self Projects</h5></div>
                    <div class="col-sm-6">
                          <h6>NIL</h6>
                    </div>
                 </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="gride_m3">
                <div class="row">
                    <div class="col-sm-12"><h5 class="head_m5">Academics</h5></div>
                     <div class="col-sm-6">
                          <h6>NIL</h6>
                    </div>
                 </div>
              </div>
            </div>

             <div class="col-sm-6">
              <div class="gride_m3">
                <div class="row">
                    <div class="col-sm-12"><h5 class="head_m5">Other Details</h5></div>
                    
                    <div class="col-sm-12">
                           <table class="w-100">
                             <tbody>
                               <tr>
                                 <td><h6>Current City   :  </h6></td>
                                 <td style="width: 40%">25</td>
                               </tr>
                               <tr>
                                 <td><h6>Gender : </h6></td>
                                 <td style="width: 40%">25</td>
                               </tr>
                                <tr>
                                 <td><h6>Number of Arrears  : </h6></td>
                                 <td style="width: 40%">25</td>
                               </tr>
                             </tbody>
                           </table>
                    </div>
                 </div>
              </div>
            </div>

  <div class="col-sm-6">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button>
  </div>
</div>
</div>
</div>

<!-- <div class="container">	
	<div class="row">
		 <div class="col-sm-6 mt-5 mb-5">
            <h3 class=" head_m2"><span>Dashboard</span></h3>
         </div>
         <div class="col-sm-6 mt-5 mb-5">
       </div>
    </div>
        
   </div> -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="exampleModalLabel">Hi Ravi</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="tab_m3">
 <ul class="nav nav-tabs" id="myTab" role="tablist" style="display: none;">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
  </li>
</ul>

<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
    <div class="text-center">
    <p>Complete Your Profile to make it visible to the recruiters</p>
    <h4>Tell us a bit about yourself</h4>
</div>
<!-- input -->
    <div class="text-left mb-3">
  <label class="mr-3">
    <input type="radio" class="option-input radio" name="example" checked />
    Male
  </label>
  <label>
    <input type="radio" class="option-input radio" name="example" />
    Female
  </label>

</div>
<!-- input -->

<!-- input -->
<div class="wrap-input100 validate-input">
            <input class="input100" type="text" name="username" id="username" placeholder="Enter Country">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-map-marker" aria-hidden="true"></i>
            </span>
          </div>
<!-- input -->

<!-- input -->
<div class="wrap-input100 validate-input">
            <input class="input100" type="text" name="username" id="username" placeholder="Enter State">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-map-marker" aria-hidden="true"></i>
            </span>
          </div>
<!-- input -->

  
    <div class="input-group mb-3 group-end">
      <a class="btn btn_m3 btnNext">Next</a>
    </div>
    <!--/. form element wrap -->

  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
     
       <div class="text-center">
    <p>Complete Your Profile to make it visible to the recruiters</p>
    <h4>Tell us a bit about yourself</h4>
</div>
<!-- input -->
    <div class="text-left mb-3">
  <label class="mr-3">
    <input type="radio" class="option-input radio" name="example1" checked />
    College Student
  </label>
  <label>
    <input type="radio" class="option-input radio" name="example1" />
    School Student
  </label>
    <label>
    <input type="radio" class="option-input radio" name="example1" />
    College Graduate
  </label>

</div>
<!-- input -->

    <div class="input-group mb-3 group-end">
      <a class="btn btn_m2  btnPrevious mr-3">Previous</a>
      <a class="btn btn_m3  btnNext">Next</a>
    </div>
    <!--/. form element wrap -->

  </div>
  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
   
    <div class="text-center">
    <p>Complete Your Profile to make it visible to the recruiters</p>
    <h4>Tell us a bit about yourself</h4>
</div>


<!-- input -->
<div class="wrap-input100 validate-input mt-3">
            <input class="input100" type="text" name="username" id="username" placeholder="Enter College">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-building-o" aria-hidden="true"></i>
            </span>
          </div>
<!-- input -->

<!-- input -->
<div class="drp_m5 mt-3 mb-4">
   <i class="fa fa-graduation-cap" aria-hidden="true"></i>
            <select>
              <option>Select Degree</option>
              <option>Select Degree</option>
              <option>Select Degree</option>
           </select>
          </div>
<!-- input -->


<!-- input -->
<div class="wrap-input100 validate-input">
            <input class="input100" type="text" name="username" id="username" placeholder="Enter State">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-map-marker" aria-hidden="true"></i>
            </span>
          </div>
<!-- input -->

    <div class="input-group mb-3 group-end">
      <a class="btn btn_m2 btnPrevious ">Previous</a>
      <a class="btn btn_m3 btnSubmit ml-3">Submit</a>
    </div>
    <!--/. form element wrap -->
 </div>
</div>
  </div>

      </div>
     
    </div>
  </div>
</div>



 

<script type="text/javascript">
  $(document).ready(function() {
  $('.btnNext').click(function() {
    $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
  });

  $('.btnPrevious').click(function() {
    $('.nav-tabs .active').parent().prev('li').find('a').trigger('click');
  });
});

</script>

@endsection