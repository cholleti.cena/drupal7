<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicle_status')->truncate();

        DB::table('vehicle_status')->insert([
            'name' => "In-Service",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('vehicle_status')->insert([
            'name' => "Out of Service",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('vehicle_status')->insert([
            'name' => "Not Active",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('vehicle_status')->insert([
            'name' => "Sold",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
    }
}
