<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('module')->truncate();

        DB::table('module')->insert([
            'id' => 1,
        	'module_name' => "Admin Users",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 2,
            'module_name' => "Banners",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 3,
            'module_name' => "Categories",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 4,
            'module_name' => "Faqs",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 5,
            'module_name' => "Testimonials",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 6,
            'module_name' => "Roles",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 7,
            'module_name' => "Leads",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 8,
            'module_name' => "Schedules",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 9,
            'module_name' => "Categories",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 10,
            'module_name' => "Packing Materials",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 11,
            'module_name' => "Rooms",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 12,
            'module_name' => "Inventory Items",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 13,
            'module_name' => "Additional Charges",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 14,
            'module_name' => "Follow Up Management",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 15,
            'module_name' => "Leads Follow Up",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 16,
            'module_name' => "Travel Time",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 17,
            'module_name' => "Fuel Charges",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 18,
            'module_name' => "Square Footage",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 19,
            'module_name' => "Floors",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 20,
            'module_name' => "Estimate Range",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 21,
            'module_name' => "Distance From Truck",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 22,
            'module_name' => "Man Hour Calculation",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 23,
            'module_name' => "Pay roll",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('module')->insert([
            'id' => 24,
            'module_name' => "Facilities",
            'created_at' => now(),
            'updated_at' => now(),
         ]);
    }
}
