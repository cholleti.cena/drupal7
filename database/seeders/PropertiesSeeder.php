<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropertiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('properties')->truncate();

        DB::table('properties')->insert([
            'name' => "Apartment",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('properties')->insert([
            'name' => "Condo",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('properties')->insert([
            'name' => "Dock Height",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('properties')->insert([
            'name' => "House",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('properties')->insert([
            'name' => "Nursing Home",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('properties')->insert([
            'name' => "Office Building",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('properties')->insert([
            'name' => "Senior Community",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('properties')->insert([
            'name' => "Storage",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('properties')->insert([
            'name' => "Town House",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
        DB::table('properties')->insert([
            'name' => "Warehouse",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
    }
}
