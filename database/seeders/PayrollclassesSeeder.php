<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PayrollclassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pay_roll_classes')->truncate();

        DB::table('pay_roll_classes')->insert([
            'name' => "Moving Or Packing",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
        
        DB::table('pay_roll_classes')->insert([
            'name' => "Misc Hours",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
        

        DB::table('pay_roll_classes')->insert([
            'name' => "Tip",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
        

        DB::table('pay_roll_classes')->insert([
            'name' => "Bonus",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
        

        DB::table('pay_roll_classes')->insert([
            'name' => "Deduction",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pay_roll_classes')->insert([
            'name' => "PerDiem",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
        
    }
}
