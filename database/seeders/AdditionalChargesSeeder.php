<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdditionalChargesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('additional_charges')->truncate();

        DB::table('additional_charges')->insert([
            'name' => "2 movers for two hours",
            'price' => "300",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('additional_charges')->insert([
            'name' => "Additional Mover",
            'price' => "0.0",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('additional_charges')->insert([
            'name' => "Cancellation Fee (Within 48 hr)",
            'price' => "100.0",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('additional_charges')->insert([
            'name' => "CC Processing Fee",
            'price' => "0.0",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('additional_charges')->insert([
            'name' => "Claim",
            'price' => "0.0",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('additional_charges')->insert([
            'name' => "Exercise Equipment",
            'price' => "300",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        
    }
}
