<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VisiblePayrollFieldsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('visible_payroll_fields')->truncate();

        DB::table('visible_payroll_fields')->insert([
            'payroll_pdf_text' => "Select all",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('visible_payroll_fields')->insert([
            'payroll_pdf_text' => "Description",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('visible_payroll_fields')->insert([
            'payroll_pdf_text' => "Travel",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('visible_payroll_fields')->insert([
            'payroll_pdf_text' => "Commission",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('visible_payroll_fields')->insert([
            'payroll_pdf_text' => "Bonus",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('visible_payroll_fields')->insert([
            'payroll_pdf_text' => "Per-Diem",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('visible_payroll_fields')->insert([
            'payroll_pdf_text' => "Service-fee",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
    }
}
