<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PackingServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packing_services')->truncate();

        DB::table('packing_services')->insert([
            'name' => "MOVING SERVICES",
            'price' => "3200",
            'pricing_elements' => json_encode(array(1,2,9,11,12,13,14)),
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('packing_services')->insert([
            'name' => "TRAVELING SERVICES",
            'price' => "0",
            'pricing_elements' => json_encode(array(1,2,3,6,7,8,9,10,11,12,13,14)),
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('packing_services')->insert([
            'name' => "TRUCK AND EQUIPMENT FEE",
            'price' => "0",
            'pricing_elements' => json_encode(array(1,2,3,4,5,6,7,8,9,10,11,12,13,14)),
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
    }
}
