<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PricingElementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pricing_elments')->truncate();

        DB::table('pricing_elments')->insert([
            'id' => 1,
            'name' => "TRUCKS",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 2,
            'name' => "CREW SIZE",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 3,
            'name' => "HOURLY RATE",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 4,
            'name' => "MIN HOURS",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 5,
            'name' => "MAX HOURS",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 6,
            'name' => "EST HOURS",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 7,
            'name' => "MOVING HOURS",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 8,
            'name' => "TRAVEL TIME",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 9,
            'name' => "TRAVEL FEE",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 10,
            'name' => "MOVING FEE",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 11,
            'name' => "PACKING MATERIALS",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 12,
            'name' => "PACKING CREW SIZE",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 13,
            'name' => "PACKING HOURS",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 14,
            'name' => "PACKING FEE",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 15,
            'name' => "FUEL SURCHARGE",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pricing_elments')->insert([
            'id' => 16,
            'name' => "STORAGE FEE",
            'price' => 0,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
        
    }
}
