<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);        
        $this->call(DefaultsSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(ItemsSeeder::class);
        $this->call(MaterialsSeeder::class);
        $this->call(MoveSizesSeeder::class);
        $this->call(PhoneTypesSeeder::class);
        $this->call(PropertiesSeeder::class);
        $this->call(ReferralsSeeder::class);
        $this->call(ServicesSeeder::class);
        $this->call(ServiceTypesSeeder::class);
        $this->call(MoveStatusSeeder::class);
        $this->call(ModuleSeeder::class);
        $this->call(PackingRequirementsSeeder::class);
        $this->call(StorageRequirementsSeeder::class);
        $this->call(PackingMaterialSeeder::class);
        $this->call(PackingQuotesSeeder::class);
        $this->call(PackingServicesSeeder::class);
        $this->call(PricingElementsSeeder::class);
        $this->call(AdditionalChargesSeeder::class);
        $this->call(PayrollclassesSeeder::class);
        $this->call(VisiblePayrollFieldsSeeder::class);        
        $this->call(PayRollMilesSeeder::class);      
        $this->call(OwnerShipsSeeder::class);      
        $this->call(VehicleStatusSeeder::class);
    }
}
