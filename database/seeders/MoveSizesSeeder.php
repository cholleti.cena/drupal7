<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MoveSizesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('move_sizes')->truncate();

        DB::table('move_sizes')->insert([
            'name' => "Studio",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('move_sizes')->insert([
            'name' => "1 Bedroom",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('move_sizes')->insert([
            'name' => "2 Bedroom",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('move_sizes')->insert([
            'name' => "3 Bedroom",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('move_sizes')->insert([
            'name' => "4 Bedroom",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('move_sizes')->insert([
            'name' => "5+ Bedroom",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('move_sizes')->insert([
            'name' => "Office",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
    }
}
