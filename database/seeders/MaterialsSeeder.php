<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaterialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materials')->truncate();

        DB::table('materials')->insert([
            'name' => "Snow Blower",
            'description' => 'Snow Blower',
            'price' => 2000,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('materials')->insert([
            'name' => "Armoire/Ward",
            'description' => 'Armoire/Ward',
            'price' => 1000,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
    }
}
