<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role')->truncate();

        DB::table('role')->insert([
        	'name' => "Super Admin",
        	'role_type' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('role')->insert([
            'name' => "Admin",
            'role_type' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('role')->insert([
            'name' => "Operator",
            'role_type' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('role')->insert([
            'name' => "Movers",
            'role_type' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
    }
}
