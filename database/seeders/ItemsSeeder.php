<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->truncate();

        DB::table('items')->insert([
            'name' => "Snow Blower",
            'category_id' => 1,
            'sulg' => "snow-blower",
            'description' => 'Snow Blower',
            'price' => 2000,
            'weight' => 140,
            'cubic_foot' => 15,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('items')->insert([
            'name' => "Armoire/Ward",
            'category_id' => 2,
            'sulg' => "armoire-ward",
            'description' => 'Armoire/Ward',
            'price' => 1000,
            'weight' => 490,
            'cubic_foot' => 70,
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
    }
}
