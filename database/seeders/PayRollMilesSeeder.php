<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PayRollMilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pay_roll_miles')->truncate();

        DB::table('pay_roll_miles')->insert([
            'name' => "Miles",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('pay_roll_miles')->insert([
            'name' => "Long Distance",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        

        
    }
}
