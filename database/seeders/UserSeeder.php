<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_users')->truncate();

        DB::table('admin_users')->insert([
        	'name' => "Super Admin",
        	'email' => 'admin@gmail.com',
            'password' => bcrypt('admin@123'),
            'role_id' => 1,
            'status' => 1,
            'created_by' => 1,
            'modified_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
    }
}
