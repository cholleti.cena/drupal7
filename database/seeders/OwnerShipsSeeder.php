<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OwnerShipsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('owner_ships')->truncate();

        DB::table('owner_ships')->insert([
            'name' => "Company owned",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('owner_ships')->insert([
            'name' => "Employee owned",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('owner_ships')->insert([
            'name' => "Leased",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('owner_ships')->insert([
            'name' => "Rental",
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);        
    }
}
