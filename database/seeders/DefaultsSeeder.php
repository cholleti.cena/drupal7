<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DefaultsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('defaults')->truncate();

        DB::table('defaults')->insert([
        	'allow_create_logs' => 1,
        	'allow_edit_logs' => 1,
        	'allow_delete_logs' => 1,
        	'log_max_days' => 30,
            'version' => '1.0.0'
         ]);
    }
}
