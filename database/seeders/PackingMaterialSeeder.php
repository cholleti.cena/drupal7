<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PackingMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packing_materials')->truncate();

        DB::table('packing_materials')->insert([
            'name' => "PACKING MATERIALS",
            'price' => "25.99",
            "default_quantity" => 1,
            'pricing_elements' => json_encode(array(11)),
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('packing_materials')->insert([
            'name' => "PACKING SERVICES",
            'price' => "0",
            "default_quantity" => 1,
            'pricing_elements' => json_encode(array(12,13)),
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('packing_materials')->insert([
            'name' => "STORAGE SERVICES",
            'price' => "25.99",
            "default_quantity" => 1,
            'pricing_elements' => json_encode(array(16)),
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
         ]);
    }
}
