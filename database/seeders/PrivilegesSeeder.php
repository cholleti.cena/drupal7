<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PrivilegesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('privileges')->truncate();

        DB::table('privileges')->insert([
        	'id' => 2,
            'privilege' => "Add",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('privileges')->insert([
            'id' => 3,
        	'privilege' => "Edit",
            'created_at' => now(),
            'updated_at' => now(),
         ]);

        DB::table('privileges')->insert([
            'id' => 4,
        	'privilege' => "Delete",
            'created_at' => now(),
            'updated_at' => now(),
         ]);
    }
}
