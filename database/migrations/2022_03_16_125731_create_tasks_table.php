<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('task_id')->nullable();
            $table->Integer('task_type')->default(0);
            $table->string('assigned_user_by')->nullable();
            $table->string('assigned_user_to')->nullable();
            $table->string('client')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('priority')->nullable();
            $table->TinyInteger('status')->default(0);
            $table->LongText('comments')->nullable();
            $table->string('when_date')->nullable();
            $table->Integer('send_emails')->default(0);
            $table->TinyInteger('active_yn')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
