<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name',255)->nullable();
            $table->string('middle_name',255)->nullable();
            $table->string('last_name',255)->nullable();
            $table->string('email',255)->nullable();
            $table->string('gender',255)->nullable();
            $table->string('dob',255)->nullable();
            $table->string('phone_type',255)->nullable();
            $table->string('phone',15)->nullable();
            $table->string('alter_phone',15)->nullable();
            $table->string('profile_pic',255)->nullable();
            $table->Integer('reset_code')->nullable(); 
            $table->LongText('reset_hash',255)->nullable();
            $table->string('referral_code',255)->nullable();
            $table->string('referred_by',255)->nullable();  
            $table->string('refferal_link',255)->nullable();    
            $table->TinyInteger('is_phone_verify')->default(0);  
            $table->TinyInteger('is_email_verified')->default(0); 
            $table->string('phone_verified_at',255)->nullable();  
            $table->string('email_verified_at',255)->nullable();
            $table->string('provider',255)->nullable();  
            $table->string('last_login',255)->nullable();  
            $table->TinyInteger('is_accept_tc')->default(0);
            $table->TinyInteger('privacy_policy')->default(0);
            $table->TinyInteger('is_registered')->default(0);
            $table->TinyInteger('is_approve')->default(0);
            $table->TinyInteger('is_active')->default(0);
            $table->string('app_version')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
