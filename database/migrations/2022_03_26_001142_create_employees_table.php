<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('emp_no', 50)->nullable();
            $table->string('first_name', 150)->nullable();
            $table->string('middle_name', 150)->nullable();
            $table->string('last_name', 150)->nullable();
            $table->string('nick_name', 150)->nullable();
            $table->string('user_name', 150)->nullable();
            $table->string('password', 150)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('two_factor_authentication', 255)->nullable();
            $table->string('chat_prmsn', 50)->nullable();
            $table->string('incoming_call_prmsn', 50)->nullable();
            $table->string('customer_chat_prmsn', 50)->nullable();
            $table->string('chat_admin_prmsn', 50)->nullable();
            $table->string('call_center_contact_prmsn', 50)->nullable();
            $table->string('billing_user_mgmt_prmsn', 50)->nullable();
            $table->string('prop_app_user_prmsn', 50)->nullable();
            $table->string('foreman_prmsn', 50)->nullable();
            $table->string('groups_access', 100)->nullable();
            $table->string('assign_crew_mem', 100)->nullable();
            $table->LongText('profile_pic')->nullable();
            $table->LongText('signature_pic')->nullable();
            $table->string('designation', 75)->nullable();
            $table->string('business_phone_number', 50)->nullable();
            $table->string('time_zone', 50)->nullable();
            $table->string('ssn', 20)->nullable();
            $table->string('driver_license')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('gender', 20)->nullable();
            $table->string('relationship', 50)->nullable();
            $table->LongText('get_to_know')->nullable();
            $table->LongText('biography')->nullable();
            $table->string('employee_status', 150)->nullable();
            $table->string('employee_type', 150)->nullable();
            $table->date('hire_date')->nullable();
            $table->date('termination_date')->nullable();
            $table->string('hourly_rate', 100)->nullable();
            $table->string('commission_rate', 100)->nullable();
            $table->date('join_date');
            $table->date('end_date');
            $table->string('client_id', 20)->nullable();
            $table->TinyInteger('is_active')->default(1);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
