<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesEmailIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_email_ids', function (Blueprint $table) {
            $table->id();
            $table->string('emp_id', 50)->nullable();
            $table->string('email_id', 255)->nullable();
            $table->string('email_type', 255)->nullable();
            $table->TinyInteger('is_active')->default(1);
            $table->string('created_by', 50)->nullable();
            $table->string('updated_by', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_email_ids');
    }
}
