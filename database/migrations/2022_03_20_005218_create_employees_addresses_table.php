<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_addresses', function (Blueprint $table) {
            $table->id();
            $table->string('emp_id', 50)->nullable();
            $table->string('street', 255)->nullable();
            $table->string('suite', 255)->nullable();
            $table->string('country', 200)->nullable();
            $table->string('state', 200)->nullable();
            $table->string('city', 200)->nullable();
            $table->string('zip', 20)->nullable();
            $table->TinyInteger('is_active')->default(1);
            $table->string('landmark', 255)->nullable();
            $table->string('created_by', 50)->nullable();
            $table->string('updated_by', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_addresses');
    }
}
