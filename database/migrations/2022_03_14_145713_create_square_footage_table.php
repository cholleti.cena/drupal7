<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSquareFootageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('square_footage', function (Blueprint $table) {
            $table->id();
            $table->Integer('min_range')->default(0);
            $table->Integer('max_range')->default(0);
            $table->decimal('minutes_per_room',11,2)->default(0);
            $table->Integer('cubic_feet_per_room')->default(0);
            $table->Integer('weight_per_room')->default(0);
            $table->Integer('people')->default(0);
            $table->Integer('trucks')->default(0);
            $table->decimal('price',11,2)->default(0);
            $table->TinyInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('square_footage');
    }
}
