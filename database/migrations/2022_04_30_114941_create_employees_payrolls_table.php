<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesPayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_payrolls', function (Blueprint $table) {
            $table->id();
            $table->Integer('emp_id')->default(0);
            $table->string('class_select')->nullable();
            $table->string('company_branch')->nullable();
            $table->date('entry_date')->nullable();
            $table->string('class_name')->nullable();
            $table->string('customer')->nullable();
            $table->decimal('hourly_rate', 12, 2)->nullable();
            $table->string('hours')->nullable();
            $table->string('travel')->nullable();
            $table->string('commission')->nullable();
            $table->Integer('amount')->default(0);
            $table->string('description')->nullable();
            $table->Integer('is_active')->default(1);
            $table->string('updated_by')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_payrolls');
    }
}
