<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInventoryToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->LongText('message')->nullable()->after('cubic_foot');
            $table->Integer('moving_time')->nullable()->after('message');
            $table->TinyInteger('over_size')->default(0)->after('moving_time');
            $table->TinyInteger('taxable')->default(0)->after('over_size');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->LongText('message')->nullable()->after('cubic_foot');
            $table->Integer('moving_time')->nullable()->after('message');
            $table->TinyInteger('over_size')->default(0)->after('moving_time');
            $table->TinyInteger('taxable')->default(0)->after('over_size');
        });
    }
}
