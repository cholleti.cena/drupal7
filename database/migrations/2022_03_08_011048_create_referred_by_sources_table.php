<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferredBySourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referred_by_sources', function (Blueprint $table) {
            $table->id();
            $table->string('name')->default('null');
            $table->decimal('cost', 11,2)->default(0.00);
            $table->string('created_by')->default('null');
            $table->string('updated_by')->default('null');
            $table->Integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referred_by_sources');
    }
}
