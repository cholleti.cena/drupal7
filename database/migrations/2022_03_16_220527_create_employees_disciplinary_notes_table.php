<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesDisciplinaryNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_disciplinary_notes', function (Blueprint $table) {
            $table->id();
            $table->string('emp_id', 50)->nullable();
            $table->string('date', 50)->nullable();
            $table->string('rep', 100)->nullable();
            $table->string('customer_id', 255)->nullable();
            $table->string('customer_name', 255)->nullable();
            $table->string('estimate_id', 255)->nullable();
            $table->string('category', 200)->nullable();
            $table->text('issues')->nullable();
            $table->text('comments')->nullable();
            $table->text('note')->nullable();
            $table->TinyInteger('is_active')->default(1);
            $table->string('created_by', 50)->nullable();
            $table->string('updated_by', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_disciplinary_notes');
    }
}
