<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstimateMileageTravelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimate_mileage_travel', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->LongText('description')->nullable();
            $table->string('image')->nullable();
            $table->string('time_details')->nullable();
            $table->TinyInteger('is_time_details')->default(0);
            $table->TinyInteger('is_checked')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimate_mileage_travel');
    }
}
