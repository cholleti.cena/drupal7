<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManHourCalculationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('man_hour_calculation', function (Blueprint $table) {
            $table->id();
            $table->string('men_hours')->nullable();
            $table->Integer('men')->default(0);
            $table->Integer('stairs_men')->default(0);
            $table->Integer('elevator_men')->default(0);
            $table->TinyInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('man_hour_calculation');
    }
}
