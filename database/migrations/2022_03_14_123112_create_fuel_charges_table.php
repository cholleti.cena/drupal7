<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuelChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel_charges', function (Blueprint $table) {
            $table->id();
            $table->Integer('min_range')->default(0);
            $table->Integer('max_range')->default(0);
            $table->decimal('min_price',11,2)->default(0);
            $table->decimal('price_per_mile',11,2)->default(0);
            $table->TinyInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fuel_charges');
    }
}
