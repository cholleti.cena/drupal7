<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravelTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_time', function (Blueprint $table) {
            $table->id();
            $table->Integer('min_mile_range')->default(0);
            $table->Integer('max_mile_range')->default(0);
            $table->decimal('min_travel_time',11,2)->default(0);
            $table->TinyInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_time');
    }
}
