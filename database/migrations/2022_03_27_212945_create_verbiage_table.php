<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVerbiageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verbiage', function (Blueprint $table) {
            $table->id();
            $table->string('signature_text')->nullable();
            $table->string('signature_name')->nullable();
            $table->tinyinteger('required')->default(0);
            $table->tinyinteger('hide')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verbiage');
    }
}
