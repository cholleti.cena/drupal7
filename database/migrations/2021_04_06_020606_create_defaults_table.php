<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDefaultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('defaults', function (Blueprint $table) {
            $table->id();
            $table->TinyInteger('allow_create_logs')->default(0);
            $table->TinyInteger('allow_edit_logs')->default(0);
            $table->TinyInteger('allow_delete_logs')->default(0);
            $table->Integer('log_max_days')->default(0);
            $table->string('version',255)->nullable();
            $table->Integer('created_by')->nullable();
            $table->Integer('modified_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('defaults');
    }
}
