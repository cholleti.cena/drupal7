<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFloorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('floors', function (Blueprint $table) {
            $table->id();
            $table->Integer('floor')->default(0);
            $table->decimal('extra_time_Room',11,2)->default(0);
            $table->decimal('stairs_os_items',11,2)->default(0);
            $table->decimal('stairs_inventory',11,2)->default(0);
            $table->decimal('stairs_boxes',11,2)->default(0);
            $table->decimal('elevator_inventory',11,2)->default(0);
            $table->decimal('elevator_boxes',11,2)->default(0);
            $table->TinyInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('floors');
    }
}
