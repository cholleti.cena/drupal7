<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacilityUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facility_units', function (Blueprint $table) {
            $table->id();
            $table->Integer('facility_id')->default(0);
            $table->string('unit_name')->nullable();
            $table->decimal('width',11,2)->default(0);
            $table->decimal('height',11,2)->default(0);
            $table->decimal('depth',11,2)->default(0);
            $table->string('location')->nullable();
            $table->decimal('price',11,2)->default(0);
            $table->Integer('quantity')->default(0);
            $table->decimal('cuft',11,2)->default(0);
            $table->Integer('total_occupied')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facility_units');
    }
}
