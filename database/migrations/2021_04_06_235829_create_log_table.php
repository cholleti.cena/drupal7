<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log', function (Blueprint $table) {
            $table->id();
            $table->Integer('module_id')->nullable();
            $table->string('created_on',255)->nullable();
            $table->Integer('user_id')->nullable();
            $table->string('action',255)->nullable();
            $table->Integer('category')->nullable();
            $table->LongText('description')->nullable();
            $table->Integer('log_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log');
    }
}
