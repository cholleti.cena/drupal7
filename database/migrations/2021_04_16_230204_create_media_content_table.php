<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_content', function (Blueprint $table) {
            $table->id();
            $table->string('media_name',255)->nullable();
            $table->string('media_path',255)->nullable();
            $table->string('media_extension',255)->nullable();
            $table->Integer('created_by')->nullable();
            $table->Integer('modified_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_content');
    }
}
