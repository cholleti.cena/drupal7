<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesProspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_prospects', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('emp_id')->nullable();
            $table->string('referred_by')->nullable();
            $table->string('rep')->nullable();
            $table->string('type')->nullable();
            $table->Integer('is_active')->default(1);
            $table->string('lc')->nullable();
            $table->string('nc')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_prospects');
    }
}
