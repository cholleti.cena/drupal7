<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstimateHourlyServiceQuotedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimate_hourly_service_quoted', function (Blueprint $table) {
            $table->id();
            $table->TinyInteger('is_moving_services')->default(0);
            $table->TinyInteger('is_traveling_services')->default(0);
            $table->TinyInteger('is_packing_materials')->default(0);
            $table->TinyInteger('is_packing_services')->default(0);
            $table->TinyInteger('is_storage_services')->default(0);
            $table->TinyInteger('is_fuel_surcharge')->default(0);
            $table->TinyInteger('is_discount')->default(0);
            $table->LongText('hide_fields_from_estimate')->nullable();
            $table->TinyInteger('is_show_total_est_price')->default(0);
            $table->TinyInteger('is_show_hourly_notes')->default(0);
            $table->LongText('hourly_price_text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimate_hourly_service_quoted');
    }
}
