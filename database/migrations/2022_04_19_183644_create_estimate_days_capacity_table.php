<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstimateDaysCapacityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimate_days_capacity', function (Blueprint $table) {
            $table->id();
            $table->Integer('number_of_jobs')->nullable();
            $table->Integer('number_of_men')->nullable();
            $table->Integer('number_of_trucks')->nullable();
            $table->string('recurring_days')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimate_days_capacity');
    }
}
