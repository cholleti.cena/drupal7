<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_labels', function (Blueprint $table) {
            $table->id();
            $table->Integer('configtype_id')->nullable();
            $table->String('name')->nullable();
            $table->LongText('description')->nullable();
            $table->TinyInteger('is_active')->default(1);
            $table->Integer('created_by')->nullable();
            $table->Integer('modified_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_labels');
    }
}
