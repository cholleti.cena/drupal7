<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesClientReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_client_reviews', function (Blueprint $table) {
            $table->id();
            $table->string('emp_id', 50)->nullable();
            $table->string('client_id', 50)->nullable();
            $table->string('customer_id', 100)->nullable();
            $table->string('customer_name', 255)->nullable();
            $table->string('estimate_id', 255)->nullable();
            $table->string('crew_member_id', 200)->nullable();
            $table->string('rating_number', 10)->nullable();
            $table->text('comments')->nullable();
            $table->date('dates');
            $table->TinyInteger('is_active')->default(1);
            $table->string('created_by', 50)->nullable();
            $table->string('updated_by', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_client_reviews');
    }
}
