<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistanceFromTruckTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distance_from_truck', function (Blueprint $table) {
            $table->id();
            $table->string('distance_from_truck')->nullable();
            $table->decimal('room',11,2)->default(0);
            $table->decimal('o_s_items',11,2)->default(0);
            $table->decimal('inventory',11,2)->default(0);
            $table->decimal('boxes',11,2)->default(0);
            $table->TinyInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distance_from_truck');
    }
}
