<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoltextTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boltext', function (Blueprint $table) {
            $table->id();
            $table->string('bol')->nullable();
            $table->tinyinteger('subtotal')->default('1');
            $table->tinyinteger('tip')->default('1');
            $table->decimal('cc_processing_fee')->default('4.0');
            $table->tinyinteger('cc_fee')->default('1');
            $table->tinyinteger('shipment_text')->default('0');
            $table->tinyinteger('storage_access')->default('1');
            $table->tinyinteger('notice')->default('1');
            $table->integer('days_in_advance')->default('0');
            $table->decimal('article_per_pound_amount')->default('0.6');
            $table->string('overtime_rate')->nullable();
            $table->string('overtime_charge_after')->nullable();
            $table->tinyinteger('disable_for_foreman')->default('0');
            $table->tinyinteger('crew_review_popup')->default('1');
            $table->string('rating_default_text')->nullable();
            $table->tinyinteger('show_tip_crew_popup')->default('1');
            $table->string('tip_for_crew_default')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boltext');
    }
}
