<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFleetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleets', function (Blueprint $table) {
            $table->id();
            $table->string('nickname')->nullable();
            $table->integer('year')->nullable();
            $table->string('make')->nullable();
            $table->string('model')->nullable();
            $table->string('type')->nullable();
            $table->string('dimensions_l')->nullable();
            $table->string('dimensions_w')->nullable();
            $table->string('dimensions_h')->nullable();
            $table->string('cuft_capacity')->nullable();
            $table->integer('VIN')->nullable();
            $table->integer('tag_number')->nullable();
            $table->string('tag_country')->nullable();
            $table->string('tag_state')->nullable();
            $table->string('expires')->nullable();
            $table->string('insurance_company')->nullable();
            $table->integer('policy_number')->nullable();
            $table->decimal('hourly_rate')->nullable();
            $table->integer('vehicle_status')->default(0);
            $table->integer('owner_ship')->default(0);
            $table->integer('is_schedulable')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fleets');
    }
}
