<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('approximate_move_date')->nullable();
            $table->Integer('type_of_service')->default(0);
            $table->LongText('origin_address')->nullable();
            $table->decimal('origin_latitude', 11,2)->default(0.00);
            $table->decimal('origin_longitude', 11,2)->default(0.00);
            $table->Integer('origin_property_type_id')->nullable();
            $table->LongText('origin_floor')->nullable();
            $table->TinyInteger('origin_elevator')->default(0);
            $table->LongText('destination_address')->nullable();
            $table->decimal('destination_latitude', 11,2)->default(0.00);
            $table->decimal('destination_longitude', 11,2)->default(0.00);
            $table->Integer('destination_property_type_id')->nullable();
            $table->LongText('destination_floor')->nullable();
            $table->TinyInteger('destination_elevator')->default(0);
            $table->Integer('move_size_id')->default(0);
            $table->Integer('square_footage')->default(0);
            $table->Integer('estimated_boxes')->default(0);
            $table->string('total_weight')->nullable();
            $table->string('total_cubic_foot')->nullable();
            $table->Integer('lead_status')->default(1);
            $table->Integer('packing_requirement_id')->default(0);
            $table->LongText('comments')->nullable();
            $table->TinyInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
