<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_events', function (Blueprint $table) {
            $table->id();
            $table->string('emp_id')->nullable();
            $table->string('client_id')->nullable();
            $table->date('event_date')->nullable();
            $table->string('rep')->nullable();
            $table->string('source')->nullable();
            $table->string('person')->nullable();
            $table->string('estimate_id')->nullable();
            $table->LongText('comments')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->Integer('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_events');
    }
}
