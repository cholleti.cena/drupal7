<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaborPriceEstimateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labor_price_estimate', function (Blueprint $table) {
            $table->id();
            $table->Integer('people')->default(0);
            $table->decimal('price_per_hour',11,2)->default(0);
            $table->decimal('base_price',11,2)->default(0);
            $table->decimal('pack_per_hour',11,2)->default(0);
            $table->LongText('days')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labor_price_estimate');
    }
}
