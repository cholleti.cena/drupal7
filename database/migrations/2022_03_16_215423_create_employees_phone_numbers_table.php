<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesPhoneNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_phone_numbers', function (Blueprint $table) {
            $table->id();
            $table->string('emp_id', 50)->nullable();
            $table->string('phone_type', 50)->nullable();
            $table->string('country_code', 10)->nullable();
            $table->string('phone_number', 20)->nullable();
            $table->TinyInteger('is_active')->default(1);
            $table->string('created_by', 50)->nullable();
            $table->string('updated_by', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_phone_numbers');
    }
}
