<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolemoduleprivilegesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rolemoduleprivileges', function (Blueprint $table) {
            $table->id();
            $table->Integer('role_id')->default(0);
            $table->Integer('module_id')->default(0);
            $table->Integer('privilege_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rolemoduleprivileges');
    }
}
