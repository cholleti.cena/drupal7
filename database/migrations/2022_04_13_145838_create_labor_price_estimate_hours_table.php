<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaborPriceEstimateHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labor_price_estimate_hours', function (Blueprint $table) {
            $table->id();
            $table->Integer('labor_price_estimate_id')->default(0);
            $table->decimal('minimum_hours',11,2)->default(0);
            $table->TinyInteger('is_charge_after_minimum_hours')->default(0);
            $table->TinyInteger('minimum_type')->default(0);
            $table->TinyInteger('inventory_type')->default(0);
            $table->TinyInteger('inventory_type_for_iframe')->default(0);
            $table->TinyInteger('hourly_rate_type')->default(0);
            $table->TinyInteger('is_iframe_auto_calculate')->default(0);
            $table->TinyInteger('is_sort_inventory_vertically')->default(0);
            $table->TinyInteger('is_default_type')->default(0);
            $table->Integer('default_type')->default(0);
            $table->TinyInteger('is_default_floors')->default(0);
            $table->Integer('default_floors')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labor_price_estimate_hours');
    }
}
