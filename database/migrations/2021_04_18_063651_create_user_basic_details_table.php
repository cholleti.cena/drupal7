<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBasicDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_basic_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');            
            $table->TinyInteger('gender')->default(0);
            $table->string('dob',255)->nullable();
            $table->TinyInteger('marital_status')->default(0);
            $table->TinyInteger('nationality_id')->default(0);
            $table->LongText('address',255)->nullable();
            $table->Integer('city_id')->default(0);
            $table->Integer('state_id')->default(0);
            $table->Integer('zipcode')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_basic_details');
    }
}
