<!DOCTYPE html>
<html lang="en">
<head>
  <title>XML To Database</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<style>
.affected-row {
	background: #d3dcd3;
	padding: 10px;
	margin-bottom: 20px;
	border: #bdd6bd 1px solid;
	border-radius: 2px;
    color: #6e716e;
	width:20%;
}
.error-message {
    background: #eac0c0;
    padding: 10px;
    margin-bottom: 20px;
    border: #dab2b2 1px solid;
    border-radius: 2px;
    color: #5d5b5b;
}
th{
	 background-color: #beefb0;
}
</style>
<body>


<?php
$conn = mysqli_connect("localhost", "root", "", "xmldata");

$affectedRow = 0;

$xmldata = simplexml_load_file("BES980UK-en-flix.xml") or die("Error: Cannot create object");
$data = array();
$data = $xmldata->features;

foreach ($data->feature as $row) {
    $title = $row->title;
    $description = $row->description;
	foreach($row->image as $data){
		$image = xml_attribute($data, 'url');
		
	}
    
    $sql = "INSERT INTO test(title,description,image) VALUES ('" . $title . "','" . $description . "','" .  $image. "')";
    
    $result = mysqli_query($conn, $sql);
    
    if (! empty($result)) {
        $affectedRow ++;
    } else {
        $error_message = mysqli_error($conn) . "\n";
    }
}
if ($affectedRow > 0) {
    $message = $affectedRow . " records inserted";
} else {
    $message = "No records inserted";
}

function xml_attribute($object, $attribute)
{
    if(isset($object[$attribute]))
        return (string) $object[$attribute];
}


   $select_sql = "SELECT * FROM test";
   $select_result = mysqli_query($conn, $select_sql);
?>

​
<div class="container">
  <h2>XML File Data</h2>
   <div class="affected-row"><?php  echo $message; ?></div>
	<?php if (! empty($error_message)) { ?>
	<div class="error-message"><?php echo nl2br($error_message); ?></div>
	<?php } ?>            
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Title</th>
        <th>Description</th>
        <th>Image</th>
      </tr>
    </thead>
    <tbody>
      <?php
		while($rows=$select_result->fetch_assoc())
		{
	 ?>
            <tr>
                <td><?php echo $rows['title'];?></td>
                <td><?php echo $rows['description'];?></td>
                <td><?php echo $rows['image'];?></td>
            </tr>
            <?php
                }
             ?>
      
    </tbody>
  </table>
</div>
​
</body>
</html>

​
