-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 17, 2022 at 07:00 PM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `moveit`
--

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(15) NOT NULL,
  `task_type` varchar(50) NOT NULL,
  `assigned_user_by` varchar(150) DEFAULT NULL,
  `assigned_user_to` varchar(150) DEFAULT NULL,
  `client` varchar(100) DEFAULT NULL,
  `contact_person` varchar(150) DEFAULT NULL,
  `priority` varchar(40) DEFAULT NULL,
  `status` int(5) DEFAULT NULL,
  `comments` text,
  `when_date` datetime NOT NULL,
  `send_emails` int(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `active_yn` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `task_id`, `task_type`, `assigned_user_by`, `assigned_user_to`, `client`, `contact_person`, `priority`, `status`, `comments`, `when_date`, `send_emails`, `created_at`, `updated_at`, `active_yn`) VALUES
(1, '1644517766', '1', '7574', 'Carol Blanco', NULL, NULL, 'Low', 0, 'asdasd', '2022-02-18 00:00:00', 0, '2022-02-10 23:59:26', '2022-02-17 23:38:39', 1),
(2, '1644517811', '1', '7574', 'Chuck Squillante', NULL, NULL, 'Low', 3, 'asdasd', '2022-02-17 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(3, '1644518035', '3', '7590', 'Simon Du', NULL, NULL, 'High', 2, 'test', '2022-02-18 00:00:00', 0, '2022-02-11 00:03:55', '2022-02-11 00:03:55', 1),
(4, '1644680045', '1', '7574', 'Bernd Biedermanns', NULL, NULL, 'Low', 1, 'asdasd', '2022-02-17 00:00:00', 0, '2022-02-10 23:59:26', '2022-02-12 21:04:05', 1),
(5, '1644527815', '1', '7574', 'Bryn Samuel', NULL, NULL, 'Low', 3, 'asdasd', '2022-02-17 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(6, '1644537814', '1', '7574', 'Maurice Martini', NULL, NULL, 'Low', 1, 'asdasd', '2022-02-17 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(7, '1644541511', '1', '7574', 'Heather Reynolds', NULL, NULL, 'Low', 1, 'asdasd', '2022-02-17 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(8, '1644519811', '1', '7574', 'Lindsay Shanley', NULL, NULL, 'Low', 3, 'asdasd', '2022-02-17 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(9, '1644517811', '1', '7574', 'John Kelley', NULL, NULL, 'Low', 1, 'asdasd', '2022-02-17 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(10, '1644517811', '1', '7574', 'Fran Murphy', NULL, NULL, 'Low', 1, 'asdasd', '2022-02-17 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(11, '1644517811', '1', '7574', 'James Bednarek', NULL, NULL, 'Low', 1, 'asdasd', '2022-02-17 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(12, '1644518035', '3', '7590', 'Williams', NULL, NULL, 'High', 2, 'test', '2022-02-18 00:00:00', 0, '2022-02-11 00:03:55', '2022-02-11 00:03:55', 1),
(13, '1644513801', '1', '7574', 'Marva Serotkin', NULL, NULL, 'Low', 0, 'Mark	Williamson', '2022-02-18 00:00:00', 0, '2022-02-10 23:59:26', '2022-02-17 23:31:33', 1),
(14, '1644527802', '1', '7574', 'John Roch', NULL, NULL, 'Low', 3, 'Trexler	Topping', '2022-02-01 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(15, '1644537803', '1', '7574', 'Peter Rapoza', NULL, NULL, 'Low', 1, 'asdasd', '2022-02-02 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(16, '1644541804', '1', '7574', 'Deborah Garlet', NULL, NULL, 'Low', 1, 'asdasd', '2022-02-03 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(17, '1644519805', '1', '7574', 'Mark Hatton', NULL, NULL, 'Low', 3, 'asdasd', '2022-02-04 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(18, '1644517806', '1', '7574', 'Theresa Cherry', NULL, NULL, 'Low', 6, 'asdasd', '2022-02-05 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(19, '1644517807', '1', '7574', 'Don Sweeney', NULL, NULL, 'Low', 7, 'asdasd', '2022-02-06 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(20, '1644517808', '1', '7574', 'Keith Robinson', NULL, NULL, 'Low', 4, 'asdasd', '2022-02-07 00:00:00', 0, '2022-02-11 00:00:12', '2022-02-11 00:00:12', 1),
(21, '1644518809', '3', '7590', 'Mark Rodrigues', NULL, NULL, 'High', 2, 'test', '2022-02-08 00:00:00', 0, '2022-02-11 00:03:55', '2022-02-11 00:03:55', 1),
(22, '1644518810', '3', '7590', 'Michael Parent', NULL, NULL, 'High', 2, 'test', '2022-02-09 00:00:00', 0, '2022-02-11 00:03:55', '2022-02-11 00:03:55', 1),
(23, '1644518811', '3', '7590', 'Gary Klencheski', NULL, NULL, 'High', 2, 'test', '2022-02-09 00:00:00', 0, '2022-02-11 00:03:55', '2022-02-11 00:03:55', 1),
(24, '1644518812', '3', '7590', 'Matthew Jarman', NULL, NULL, 'High', 2, 'test', '2022-02-10 00:00:00', 0, '2022-02-11 00:03:55', '2022-02-11 00:03:55', 1),
(25, '1644518813', '3', '7590', 'Mark Rodrigues', NULL, NULL, 'High', 2, 'test', '2022-02-10 00:00:00', 0, '2022-02-11 00:03:55', '2022-02-11 00:03:55', 1),
(26, '1644518814', '3', '7590', 'Michael Parent', NULL, NULL, 'High', 2, 'test', '2022-02-11 00:00:00', 0, '2022-02-11 00:03:55', '2022-02-11 00:03:55', 1),
(27, '1644518815', '3', '7590', 'Gary Klencheski', NULL, NULL, 'High', 2, 'test', '2022-02-11 00:00:00', 0, '2022-02-11 00:03:55', '2022-02-11 00:03:55', 1),
(28, '1644518816', '3', '7590', 'Matthew Jarman', NULL, NULL, 'High', 2, 'test', '2022-02-11 00:00:00', 0, '2022-02-11 00:03:55', '2022-02-11 00:03:55', 1),
(29, '1644686280', '1', '7574', NULL, NULL, NULL, 'Low', 1, NULL, '2022-02-18 00:00:00', 0, '2022-02-12 22:48:00', '2022-02-12 22:48:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `task_status`
--

DROP TABLE IF EXISTS `task_status`;
CREATE TABLE IF NOT EXISTS `task_status` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `task_status`
--

INSERT INTO `task_status` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Not Started', 1, '2022-01-18 09:03:15', '2022-01-18 09:03:15'),
(2, 'In Progress', 1, '2022-01-18 09:03:15', '2022-01-18 09:03:15'),
(3, 'Completed', 1, '2022-01-18 09:03:15', '2022-01-18 09:03:15'),
(4, 'Waiting', 1, '2022-01-18 09:03:15', '2022-01-18 09:03:15'),
(5, 'Deferred', 1, '2022-02-12 21:01:57', '2022-02-12 21:01:57'),
(6, 'Pass Due', 1, '2022-02-12 21:22:49', '2022-02-12 21:22:49'),
(7, 'Survey', 1, '2022-02-12 21:22:49', '2022-02-12 21:22:49');

-- --------------------------------------------------------

--
-- Table structure for table `task_types`
--

DROP TABLE IF EXISTS `task_types`;
CREATE TABLE IF NOT EXISTS `task_types` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `task_types`
--

INSERT INTO `task_types` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Call Back', 1, '2022-01-18 09:03:15', '2022-01-18 09:03:15'),
(2, 'Send Email', 1, '2022-01-18 09:03:15', '2022-01-18 09:03:15'),
(3, 'Send Fax', 1, '2022-01-18 09:03:15', '2022-01-18 09:03:15'),
(4, 'Meeting', 1, '2022-01-18 09:03:15', '2022-01-18 09:03:15');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
