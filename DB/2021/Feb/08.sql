-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.17-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table moveit.admin_users
CREATE TABLE IF NOT EXISTS `admin_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobileno` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secret` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.admin_users: ~0 rows (approximately)
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` (`id`, `name`, `email`, `password`, `mobileno`, `reset_hash`, `secret`, `role_id`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
	(1, 'Super Admin', 'admin@gmail.com', '$2y$10$l3ezDjYznWxi0DPtk4f2WecvLrMpCSWkK6bVFaMkwMvAiIEeYvJ2S', NULL, 'b268e6f988d72654165895a1e47373bca2b5a699933687388f', NULL, 1, 1, 1, 1, '2022-01-31 09:31:41', '2022-02-06 10:44:19');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;

-- Dumping structure for table moveit.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.categories: ~10 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Basement', 1, '2022-01-31 09:31:47', '2022-01-31 09:31:47'),
	(2, 'Bedroom', 1, '2022-01-31 09:31:47', '2022-01-31 09:31:47'),
	(3, 'Electronics/Appliances', 1, '2022-01-31 09:31:47', '2022-01-31 09:31:47'),
	(4, 'Exercise', 1, '2022-01-31 09:31:48', '2022-01-31 09:31:48'),
	(5, 'Garage & Outdoors', 1, '2022-01-31 09:31:48', '2022-01-31 09:31:48'),
	(6, 'Industrial', 1, '2022-01-31 09:31:49', '2022-01-31 09:31:49'),
	(7, 'Kitchen & Dining', 1, '2022-01-31 09:31:50', '2022-01-31 09:31:50'),
	(8, 'Living & Family', 1, '2022-01-31 09:31:50', '2022-01-31 09:31:50'),
	(9, 'Misc/Other', 1, '2022-01-31 09:31:50', '2022-01-31 09:31:50'),
	(10, 'Office', 1, '2022-01-31 09:31:51', '2022-01-31 09:31:51');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table moveit.config_labels
CREATE TABLE IF NOT EXISTS `config_labels` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `configtype_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.config_labels: ~0 rows (approximately)
/*!40000 ALTER TABLE `config_labels` DISABLE KEYS */;
/*!40000 ALTER TABLE `config_labels` ENABLE KEYS */;

-- Dumping structure for table moveit.config_types
CREATE TABLE IF NOT EXISTS `config_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.config_types: ~0 rows (approximately)
/*!40000 ALTER TABLE `config_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `config_types` ENABLE KEYS */;

-- Dumping structure for table moveit.defaults
CREATE TABLE IF NOT EXISTS `defaults` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `allow_create_logs` tinyint(4) NOT NULL DEFAULT 0,
  `allow_edit_logs` tinyint(4) NOT NULL DEFAULT 0,
  `allow_delete_logs` tinyint(4) NOT NULL DEFAULT 0,
  `log_max_days` int(11) NOT NULL DEFAULT 0,
  `version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.defaults: ~0 rows (approximately)
/*!40000 ALTER TABLE `defaults` DISABLE KEYS */;
INSERT INTO `defaults` (`id`, `allow_create_logs`, `allow_edit_logs`, `allow_delete_logs`, `log_max_days`, `version`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 30, '1.0.0', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `defaults` ENABLE KEYS */;

-- Dumping structure for table moveit.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table moveit.faqs
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.faqs: ~0 rows (approximately)
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;

-- Dumping structure for table moveit.home_banners
CREATE TABLE IF NOT EXISTS `home_banners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `header_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hear_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.home_banners: ~0 rows (approximately)
/*!40000 ALTER TABLE `home_banners` DISABLE KEYS */;
/*!40000 ALTER TABLE `home_banners` ENABLE KEYS */;

-- Dumping structure for table moveit.items
CREATE TABLE IF NOT EXISTS `items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `sulg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cubic_foot` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.items: ~2 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`id`, `name`, `category_id`, `sulg`, `description`, `price`, `weight`, `cubic_foot`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Snow Blower', 1, 'snow-blower', 'Snow Blower', 2000.00, '140', '15', 1, '2022-01-31 09:31:55', '2022-01-31 09:31:55'),
	(2, 'Armoire/Ward', 2, 'armoire-ward', 'Armoire/Ward', 1000.00, '490', '70', 1, '2022-01-31 09:31:55', '2022-01-31 09:31:55');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Dumping structure for table moveit.items_images
CREATE TABLE IF NOT EXISTS `items_images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) unsigned NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.items_images: ~0 rows (approximately)
/*!40000 ALTER TABLE `items_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `items_images` ENABLE KEYS */;

-- Dumping structure for table moveit.leads
CREATE TABLE IF NOT EXISTS `leads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `approximate_move_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_of_service` int(11) NOT NULL DEFAULT 0,
  `origin_address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `origin_latitude` decimal(11,2) NOT NULL DEFAULT 0.00,
  `origin_longitude` decimal(11,2) NOT NULL DEFAULT 0.00,
  `origin_elevator` tinyint(4) NOT NULL DEFAULT 0,
  `origin_property_type_id` int(11) DEFAULT NULL,
  `origin_floor` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination_address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination_latitude` decimal(11,2) NOT NULL DEFAULT 0.00,
  `destination_longitude` decimal(11,2) NOT NULL DEFAULT 0.00,
  `destination_elevator` tinyint(4) NOT NULL DEFAULT 0,
  `destination_property_type_id` int(11) DEFAULT NULL,
  `destination_floor` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `move_size_id` int(11) NOT NULL DEFAULT 0,
  `square_footage` int(11) NOT NULL DEFAULT 0,
  `estimated_boxes` int(11) NOT NULL DEFAULT 0,
  `total_weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_cubic_foot` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lead_status` int(11) NOT NULL DEFAULT 1,
  `packing_requirement_id` int(11) NOT NULL DEFAULT 1,
  `comments` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.leads: ~11 rows (approximately)
/*!40000 ALTER TABLE `leads` DISABLE KEYS */;
INSERT INTO `leads` (`id`, `user_id`, `approximate_move_date`, `type_of_service`, `origin_address`, `origin_latitude`, `origin_longitude`, `origin_elevator`, `origin_property_type_id`, `origin_floor`, `destination_address`, `destination_latitude`, `destination_longitude`, `destination_elevator`, `destination_property_type_id`, `destination_floor`, `move_size_id`, `square_footage`, `estimated_boxes`, `total_weight`, `total_cubic_foot`, `lead_status`, `packing_requirement_id`, `comments`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 1, '01-01-2022', 1, 'Newington, New Hampshire 03801', 43.10, 70.83, 0, 1, '[1]', 'Newburyport, Massachusetts 01951', 42.78, 70.85, 1, 1, '[1,2]', 1, 1500, 10, '160', '18', 1, 2, 'test', 1, '2022-01-18 18:44:53', '2022-01-18 18:44:53'),
	(2, 1, '01-01-2022', 2, 'Hyderabad, Telangana 500072', 17.48, 78.42, 0, 1, '[1]', 'Hyderabad, Telangana 500072', 17.48, 78.42, 0, 1, '[1,3]', 1, 1500, 10, '160', '18', 1, 1, 'test', 1, '2022-01-18 18:45:53', '2022-01-18 18:45:53'),
	(3, 1, '01-01-2022', 3, 'Hyderabad, Telangana 500072', 17.48, 78.42, 0, 1, '[1]', 'Hyderabad, Telangana 500072', 17.48, 78.42, 0, 1, '[1]', 1, 1500, 10, '160', '18', 1, 1, 'test', 1, '2022-01-20 09:29:27', '2022-01-20 09:29:27'),
	(4, 2, '2022-02-03', 2, 'test', 0.00, 0.00, 0, 1, '["10"]', 'test', 0.00, 0.00, 0, 4, '["5"]', 2, 10000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 20:45:45', '2022-02-03 20:45:45'),
	(5, 3, '2022-02-03', 2, 'test', 0.00, 0.00, 0, 1, '["7"]', 'test', 0.00, 0.00, 0, 4, '["9"]', 2, 10000, 100, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 20:58:27', '2022-02-03 20:58:27'),
	(6, 4, '2022-02-03', 2, 'test', 0.00, 0.00, 0, 3, '["8"]', 'test', 0.00, 0.00, 0, 5, '["9"]', 2, 1000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 21:10:35', '2022-02-03 21:10:35'),
	(7, 4, '2022-02-03', 2, 'test', 0.00, 0.00, 0, 3, '["8"]', 'test', 0.00, 0.00, 0, 5, '["9"]', 2, 1000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 21:11:13', '2022-02-03 21:11:13'),
	(8, 4, '2022-02-03', 2, 'test', 0.00, 0.00, 0, 3, '["8"]', 'test', 0.00, 0.00, 0, 5, '["9"]', 2, 1000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 21:12:09', '2022-02-03 21:12:09'),
	(9, 4, '2022-02-03', 2, 'test', 0.00, 0.00, 0, 3, '["8"]', 'test', 0.00, 0.00, 0, 5, '["9"]', 2, 1000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 21:13:24', '2022-02-03 21:13:24'),
	(10, 5, '2022-02-03', 2, 'test', 0.00, 0.00, 0, 3, '["4"]', 'test', 0.00, 0.00, 0, 3, '["9"]', 3, 10000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 21:18:54', '2022-02-03 21:18:54'),
	(11, 6, '2022-02-03', 2, 'test', 0.00, 0.00, 0, 1, '["9"]', 'test', 0.00, 0.00, 0, 7, '["10"]', 2, 1000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 21:32:24', '2022-02-03 21:32:24');
/*!40000 ALTER TABLE `leads` ENABLE KEYS */;

-- Dumping structure for table moveit.leads_items
CREATE TABLE IF NOT EXISTS `leads_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `lead_id` bigint(20) unsigned NOT NULL,
  `item_id` bigint(20) unsigned NOT NULL,
  `floor` int(11) NOT NULL DEFAULT 0,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cubic_foot` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.leads_items: ~6 rows (approximately)
/*!40000 ALTER TABLE `leads_items` DISABLE KEYS */;
INSERT INTO `leads_items` (`id`, `user_id`, `lead_id`, `item_id`, `floor`, `quantity`, `price`, `weight`, `cubic_foot`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 2, 1, 10.00, '50', '20', '2022-01-18 18:44:54', '2022-01-18 18:44:54'),
	(2, 1, 1, 2, 1, 1, 10.00, '60', '50', '2022-01-18 18:44:54', '2022-01-18 18:44:54'),
	(3, 1, 2, 1, 1, 1, 10.00, '50', '20', '2022-01-18 18:45:53', '2022-01-18 18:45:53'),
	(4, 1, 2, 2, 1, 1, 10.00, '60', '50', '2022-01-18 18:45:54', '2022-01-18 18:45:54'),
	(5, 1, 3, 1, 1, 1, 10.00, '50', '20', '2022-01-20 09:29:27', '2022-01-20 09:29:27'),
	(6, 1, 3, 2, 1, 1, 10.00, '60', '50', '2022-01-20 09:29:27', '2022-01-20 09:29:27');
/*!40000 ALTER TABLE `leads_items` ENABLE KEYS */;

-- Dumping structure for table moveit.leads_materials
CREATE TABLE IF NOT EXISTS `leads_materials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `lead_id` bigint(20) unsigned NOT NULL,
  `material_id` bigint(20) unsigned NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `pack_quantity` int(11) NOT NULL DEFAULT 1,
  `unpack_quantity` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.leads_materials: ~2 rows (approximately)
/*!40000 ALTER TABLE `leads_materials` DISABLE KEYS */;
INSERT INTO `leads_materials` (`id`, `user_id`, `lead_id`, `material_id`, `quantity`, `price`, `pack_quantity`, `unpack_quantity`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 1, 89.59, 1, 1, '2022-01-18 18:44:54', '2022-01-18 18:44:54'),
	(2, 1, 2, 1, 1, 9000.00, 1, 1, '2022-01-18 18:45:54', '2022-01-18 18:45:54'),
	(3, 1, 3, 1, 1, 2000.00, 1, 1, '2022-01-20 09:29:27', '2022-01-20 09:29:27');
/*!40000 ALTER TABLE `leads_materials` ENABLE KEYS */;

-- Dumping structure for table moveit.leads_services
CREATE TABLE IF NOT EXISTS `leads_services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `lead_id` bigint(20) unsigned NOT NULL,
  `service_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.leads_services: ~14 rows (approximately)
/*!40000 ALTER TABLE `leads_services` DISABLE KEYS */;
INSERT INTO `leads_services` (`id`, `user_id`, `lead_id`, `service_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, '2022-01-18 18:44:55', '2022-01-18 18:44:55'),
	(2, 1, 1, 2, '2022-01-18 18:44:55', '2022-01-18 18:44:55'),
	(3, 1, 2, 1, '2022-01-18 18:45:55', '2022-01-18 18:45:55'),
	(4, 1, 2, 2, '2022-01-18 18:45:55', '2022-01-18 18:45:55'),
	(5, 1, 3, 1, '2022-01-20 09:29:27', '2022-01-20 09:29:27'),
	(6, 1, 3, 2, '2022-01-20 09:29:27', '2022-01-20 09:29:27'),
	(7, 4, 8, 1, '2022-02-03 21:12:09', '2022-02-03 21:12:09'),
	(8, 4, 8, 2, '2022-02-03 21:12:09', '2022-02-03 21:12:09'),
	(9, 4, 9, 1, '2022-02-03 21:13:24', '2022-02-03 21:13:24'),
	(10, 4, 9, 2, '2022-02-03 21:13:24', '2022-02-03 21:13:24'),
	(11, 5, 10, 1, '2022-02-03 21:18:54', '2022-02-03 21:18:54'),
	(12, 5, 10, 2, '2022-02-03 21:18:54', '2022-02-03 21:18:54'),
	(13, 6, 11, 1, '2022-02-03 21:32:24', '2022-02-03 21:32:24'),
	(14, 6, 11, 2, '2022-02-03 21:32:24', '2022-02-03 21:32:24');
/*!40000 ALTER TABLE `leads_services` ENABLE KEYS */;

-- Dumping structure for table moveit.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT NULL,
  `created_on` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `log_type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.log: ~0 rows (approximately)
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` (`id`, `module_id`, `created_on`, `user_id`, `action`, `category`, `description`, `log_type`, `created_at`, `updated_at`) VALUES
	(1, 1, '2022-01-18 16:45:25', 1, 'update', 1, 'User Super Admin is updated', 1, '2022-01-18 16:45:25', '2022-01-18 16:45:25');
/*!40000 ALTER TABLE `log` ENABLE KEYS */;

-- Dumping structure for table moveit.materials
CREATE TABLE IF NOT EXISTS `materials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.materials: ~2 rows (approximately)
/*!40000 ALTER TABLE `materials` DISABLE KEYS */;
INSERT INTO `materials` (`id`, `name`, `description`, `price`, `image`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Snow Blower', 'Snow Blower', 2000.00, NULL, 1, '2022-01-31 09:31:59', '2022-01-31 09:31:59'),
	(2, 'Armoire/Ward', 'Armoire/Ward', 1000.00, NULL, 1, '2022-01-31 09:31:59', '2022-01-31 09:31:59');
/*!40000 ALTER TABLE `materials` ENABLE KEYS */;

-- Dumping structure for table moveit.media_content
CREATE TABLE IF NOT EXISTS `media_content` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `media_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_extension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.media_content: ~0 rows (approximately)
/*!40000 ALTER TABLE `media_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_content` ENABLE KEYS */;

-- Dumping structure for table moveit.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.migrations: ~37 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(6, '2016_06_01_000004_create_oauth_clients_table', 1),
	(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
	(8, '2019_08_19_000000_create_failed_jobs_table', 1),
	(9, '2021_04_06_015221_create_admin_users_table', 1),
	(10, '2021_04_06_020606_create_defaults_table', 1),
	(11, '2021_04_06_021009_create_faqs_table', 1),
	(12, '2021_04_06_021216_create_home_banners_table', 1),
	(13, '2021_04_06_231416_create_role_table', 1),
	(14, '2021_04_06_234547_create_rolemoduleprivileges_table', 1),
	(15, '2021_04_06_234731_create_privileges_table', 1),
	(16, '2021_04_06_235732_create_module_table', 1),
	(17, '2021_04_06_235829_create_log_table', 1),
	(18, '2021_04_08_220617_create_testimonials_table', 1),
	(19, '2021_04_16_230204_create_media_content_table', 1),
	(20, '2021_04_18_063651_create_user_basic_details_table', 1),
	(21, '2021_04_30_222106_create_config_types_table', 1),
	(22, '2021_04_30_222222_create_config_labels_table', 1),
	(23, '2022_01_15_132647_create_services_table', 1),
	(24, '2022_01_15_141012_create_categories_table', 1),
	(25, '2022_01_15_141355_create_items_table', 1),
	(26, '2022_01_15_141802_create_items_images_table', 1),
	(27, '2022_01_15_142144_create_materials_table', 1),
	(28, '2022_01_15_142323_create_move_sizes_table', 1),
	(29, '2022_01_15_142415_create_phone_types_table', 1),
	(30, '2022_01_15_142505_create_properties_table', 1),
	(31, '2022_01_15_142608_create_referrals_table', 1),
	(32, '2022_01_15_142702_create_service_types_table', 1),
	(33, '2022_01_15_142821_create_leads_table', 1),
	(34, '2022_01_15_143821_create_leads_items_table', 1),
	(35, '2022_01_15_144232_create_leads_materials_table', 1),
	(36, '2022_01_15_144419_create_leads_services_table', 1),
	(37, '2022_01_15_205041_create_move_status_table', 1),
	(38, '2022_01_26_133300_create_packing_requirements_table', 2),
	(39, '2022_01_26_160727_create_storage_requirements_table', 3),
	(40, '2022_01_29_074845_create_packing_services_table', 4),
	(41, '2022_01_29_075206_create_packing_quotes_table', 4),
	(42, '2022_01_29_075353_create_packing_materials_table', 4),
	(43, '2022_01_29_080509_create_pricing_elements_table', 5);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table moveit.module
CREATE TABLE IF NOT EXISTS `module` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.module: ~8 rows (approximately)
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` (`id`, `module_name`, `created_at`, `updated_at`) VALUES
	(1, 'Admin Users', '2022-01-31 09:32:41', '2022-01-31 09:32:41'),
	(2, 'Banners', '2022-01-31 09:32:41', '2022-01-31 09:32:41'),
	(3, 'Categories', '2022-01-31 09:32:42', '2022-01-31 09:32:42'),
	(4, 'Faqs', '2022-01-31 09:32:43', '2022-01-31 09:32:43'),
	(5, 'Testimonials', '2022-01-31 09:32:43', '2022-01-31 09:32:43'),
	(6, 'Roles', '2022-01-31 09:32:44', '2022-01-31 09:32:44'),
	(7, 'Leads', '2022-01-31 09:32:44', '2022-01-31 09:32:44'),
	(8, 'Schedules', '2022-01-31 09:32:45', '2022-01-31 09:32:45');
/*!40000 ALTER TABLE `module` ENABLE KEYS */;

-- Dumping structure for table moveit.move_sizes
CREATE TABLE IF NOT EXISTS `move_sizes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.move_sizes: ~7 rows (approximately)
/*!40000 ALTER TABLE `move_sizes` DISABLE KEYS */;
INSERT INTO `move_sizes` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Studio', 1, '2022-01-31 09:32:03', '2022-01-31 09:32:03'),
	(2, '1 Bedroom', 1, '2022-01-31 09:32:04', '2022-01-31 09:32:04'),
	(3, '2 Bedroom', 1, '2022-01-31 09:32:04', '2022-01-31 09:32:04'),
	(4, '3 Bedroom', 1, '2022-01-31 09:32:04', '2022-01-31 09:32:04'),
	(5, '4 Bedroom', 1, '2022-01-31 09:32:05', '2022-01-31 09:32:05'),
	(6, '5+ Bedroom', 1, '2022-01-31 09:32:05', '2022-01-31 09:32:05'),
	(7, 'Office', 1, '2022-01-31 09:32:06', '2022-01-31 09:32:06');
/*!40000 ALTER TABLE `move_sizes` ENABLE KEYS */;

-- Dumping structure for table moveit.move_status
CREATE TABLE IF NOT EXISTS `move_status` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.move_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `move_status` DISABLE KEYS */;
INSERT INTO `move_status` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'New Lead', 1, '2022-01-31 09:32:38', '2022-01-31 09:32:38');
/*!40000 ALTER TABLE `move_status` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_access_tokens: ~3 rows (approximately)
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
	('00e42831ea83bf0f7bdb16a80f2048a09adc3dd555e8a8ac08dc0c0a6956b8b9ba13cf7e0a69e828', 1, 1, 'AppName', '[]', 0, '2022-01-19 11:54:04', '2022-01-19 11:54:04', '2023-01-19 11:54:04'),
	('680af915bc021e4da4b74ef114e1179cd2cf2cde6ad208bf395fcf5ef705183ad49ad5e34b9ebc6f', 1, 1, 'AppName', '[]', 0, '2022-02-08 17:02:53', '2022-02-08 17:02:53', '2023-02-08 17:02:53'),
	('b31ade19535ec60e1f9011ce4cd18244d025632b1c3e72a352a0970d3c7165ec5a8f80a67dde7c69', 1, 1, 'AppName', '[]', 0, '2022-02-02 16:17:56', '2022-02-02 16:17:56', '2023-02-02 16:17:56'),
	('be0f72b3cad42deda0fc9bdbb664be6e8015bd4f8fde857e2f48326f764d5d263aacbc907ab66bda', 1, 1, 'AppName', '[]', 0, '2022-01-23 19:44:59', '2022-01-23 19:44:59', '2023-01-23 19:44:59');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_auth_codes: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_clients: ~2 rows (approximately)
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'MoveIt Personal Access Client', 'Cy4r8dELeuEqjHpAaTZHUgro5vMaB0pUjf3K4O8m', NULL, 'http://localhost', 1, 0, 0, '2022-01-19 11:54:00', '2022-01-19 11:54:00'),
	(2, NULL, 'MoveIt Password Grant Client', 'QoZ75k9DVTr6KNpjWgmjxP5nDPc6gkDYsr0iHmGb', 'users', 'http://localhost', 0, 1, 0, '2022-01-19 11:54:00', '2022-01-19 11:54:00');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_personal_access_clients: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
	(1, 1, '2022-01-19 11:54:00', '2022-01-19 11:54:00');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_refresh_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Dumping structure for table moveit.packing_materials
CREATE TABLE IF NOT EXISTS `packing_materials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `pricing_elements` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.packing_materials: ~3 rows (approximately)
/*!40000 ALTER TABLE `packing_materials` DISABLE KEYS */;
INSERT INTO `packing_materials` (`id`, `name`, `price`, `pricing_elements`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'PACKING MATERIALS', 25.99, '[11]', 1, '2022-01-31 09:32:58', '2022-01-31 09:32:58'),
	(2, 'PACKING SERVICES', 0.00, '[12,13]', 1, '2022-01-31 09:32:58', '2022-01-31 09:32:58'),
	(3, 'STORAGE SERVICES', 25.99, '[16]', 1, '2022-01-31 09:32:58', '2022-01-31 09:32:58');
/*!40000 ALTER TABLE `packing_materials` ENABLE KEYS */;

-- Dumping structure for table moveit.packing_quotes
CREATE TABLE IF NOT EXISTS `packing_quotes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `pricing_elements` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.packing_quotes: ~3 rows (approximately)
/*!40000 ALTER TABLE `packing_quotes` DISABLE KEYS */;
INSERT INTO `packing_quotes` (`id`, `name`, `price`, `pricing_elements`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'ESTIMATED QUOTE', 0.00, '[1,2,9,11,12,13,14]', 1, '2022-01-31 09:33:04', '2022-01-31 09:33:04'),
	(2, 'HOURLY ESTIMATE', 0.00, '[1,2,3,6,7,8,9,10,11,12,13,14]', 1, '2022-01-31 09:33:04', '2022-01-31 09:33:04'),
	(3, 'NOT TO EXCEED', 0.00, '[1,2,3,4,5,6,7,8,9,10,11,12,13,14]', 1, '2022-01-31 09:33:04', '2022-01-31 09:33:04');
/*!40000 ALTER TABLE `packing_quotes` ENABLE KEYS */;

-- Dumping structure for table moveit.packing_requirements
CREATE TABLE IF NOT EXISTS `packing_requirements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.packing_requirements: ~3 rows (approximately)
/*!40000 ALTER TABLE `packing_requirements` DISABLE KEYS */;
INSERT INTO `packing_requirements` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'No Packing Required', 1, '2022-01-31 09:32:48', '2022-01-31 09:32:48'),
	(2, 'Flat Pack/Unpack', 1, '2022-01-31 09:32:49', '2022-01-31 09:32:49'),
	(3, 'Hourly Pack/Unpack', 1, '2022-01-31 09:32:50', '2022-01-31 09:32:50');
/*!40000 ALTER TABLE `packing_requirements` ENABLE KEYS */;

-- Dumping structure for table moveit.packing_services
CREATE TABLE IF NOT EXISTS `packing_services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `pricing_elements` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.packing_services: ~3 rows (approximately)
/*!40000 ALTER TABLE `packing_services` DISABLE KEYS */;
INSERT INTO `packing_services` (`id`, `name`, `price`, `pricing_elements`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'MOVING SERVICES', 3200.00, '[1,2,9,11,12,13,14]', 1, '2022-01-31 09:33:09', '2022-01-31 09:33:09'),
	(2, 'TRAVELING SERVICES', 0.00, '[1,2,3,6,7,8,9,10,11,12,13,14]', 1, '2022-01-31 09:33:09', '2022-01-31 09:33:09'),
	(3, 'TRUCK AND EQUIPMENT FEE', 0.00, '[1,2,3,4,5,6,7,8,9,10,11,12,13,14]', 1, '2022-01-31 09:33:10', '2022-01-31 09:33:10');
/*!40000 ALTER TABLE `packing_services` ENABLE KEYS */;

-- Dumping structure for table moveit.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table moveit.phone_types
CREATE TABLE IF NOT EXISTS `phone_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.phone_types: ~4 rows (approximately)
/*!40000 ALTER TABLE `phone_types` DISABLE KEYS */;
INSERT INTO `phone_types` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Mobile Phone', 1, '2022-01-31 09:32:12', '2022-01-31 09:32:12'),
	(2, 'Business phone', 1, '2022-01-31 09:32:12', '2022-01-31 09:32:12'),
	(3, 'Home phone', 1, '2022-01-31 09:32:13', '2022-01-31 09:32:13'),
	(4, 'Fax', 1, '2022-01-31 09:32:13', '2022-01-31 09:32:13');
/*!40000 ALTER TABLE `phone_types` ENABLE KEYS */;

-- Dumping structure for table moveit.pricing_elments
CREATE TABLE IF NOT EXISTS `pricing_elments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.pricing_elments: ~16 rows (approximately)
/*!40000 ALTER TABLE `pricing_elments` DISABLE KEYS */;
INSERT INTO `pricing_elments` (`id`, `name`, `price`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'TRUCKS', 0.00, 1, '2022-01-31 09:33:14', '2022-01-31 09:33:14'),
	(2, 'CREW SIZE', 0.00, 1, '2022-01-31 09:33:15', '2022-01-31 09:33:15'),
	(3, 'HOURLY RATE', 0.00, 1, '2022-01-31 09:33:15', '2022-01-31 09:33:15'),
	(4, 'MIN HOURS', 0.00, 1, '2022-01-31 09:33:16', '2022-01-31 09:33:16'),
	(5, 'MAX HOURS', 0.00, 1, '2022-01-31 09:33:16', '2022-01-31 09:33:16'),
	(6, 'EST HOURS', 0.00, 1, '2022-01-31 09:33:17', '2022-01-31 09:33:17'),
	(7, 'MOVING HOURS', 0.00, 1, '2022-01-31 09:33:17', '2022-01-31 09:33:17'),
	(8, 'TRAVEL TIME', 0.00, 1, '2022-01-31 09:33:17', '2022-01-31 09:33:17'),
	(9, 'TRAVEL FEE', 0.00, 1, '2022-01-31 09:33:17', '2022-01-31 09:33:17'),
	(10, 'MOVING FEE', 0.00, 1, '2022-01-31 09:33:17', '2022-01-31 09:33:17'),
	(11, 'PACKING MATERIALS', 0.00, 1, '2022-01-31 09:33:18', '2022-01-31 09:33:18'),
	(12, 'PACKING CREW SIZE', 0.00, 1, '2022-01-31 09:33:18', '2022-01-31 09:33:18'),
	(13, 'PACKING HOURS', 0.00, 1, '2022-01-31 09:33:18', '2022-01-31 09:33:18'),
	(14, 'PACKING FEE', 0.00, 1, '2022-01-31 09:33:19', '2022-01-31 09:33:19'),
	(15, 'FUEL SURCHARGE', 0.00, 1, '2022-01-31 09:33:19', '2022-01-31 09:33:19'),
	(16, 'STORAGE FEE', 0.00, 1, '2022-01-31 09:33:19', '2022-01-31 09:33:19');
/*!40000 ALTER TABLE `pricing_elments` ENABLE KEYS */;

-- Dumping structure for table moveit.privileges
CREATE TABLE IF NOT EXISTS `privileges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `privilege` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.privileges: ~4 rows (approximately)
/*!40000 ALTER TABLE `privileges` DISABLE KEYS */;
INSERT INTO `privileges` (`id`, `privilege`, `created_at`, `updated_at`) VALUES
	(1, 'View', '2022-01-18 16:42:12', '2022-01-18 16:42:12'),
	(2, 'Add', '2022-01-18 16:42:12', '2022-01-18 16:42:12'),
	(3, 'Edit', '2022-01-18 16:42:12', '2022-01-18 16:42:12'),
	(4, 'Delete', '2022-01-18 16:42:12', '2022-01-18 16:42:12');
/*!40000 ALTER TABLE `privileges` ENABLE KEYS */;

-- Dumping structure for table moveit.properties
CREATE TABLE IF NOT EXISTS `properties` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.properties: ~10 rows (approximately)
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
INSERT INTO `properties` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Apartment', 1, '2022-01-31 09:32:18', '2022-01-31 09:32:18'),
	(2, 'Condo', 1, '2022-01-31 09:32:19', '2022-01-31 09:32:19'),
	(3, 'Dock Height', 1, '2022-01-31 09:32:19', '2022-01-31 09:32:19'),
	(4, 'House', 1, '2022-01-31 09:32:20', '2022-01-31 09:32:20'),
	(5, 'Nursing Home', 1, '2022-01-31 09:32:20', '2022-01-31 09:32:20'),
	(6, 'Office Building', 1, '2022-01-31 09:32:20', '2022-01-31 09:32:20'),
	(7, 'Senior Community', 1, '2022-01-31 09:32:20', '2022-01-31 09:32:20'),
	(8, 'Storage', 1, '2022-01-31 09:32:21', '2022-01-31 09:32:21'),
	(9, 'Town House', 1, '2022-01-31 09:32:21', '2022-01-31 09:32:21'),
	(10, 'Warehouse', 1, '2022-01-31 09:32:21', '2022-01-31 09:32:21');
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;

-- Dumping structure for table moveit.referrals
CREATE TABLE IF NOT EXISTS `referrals` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.referrals: ~4 rows (approximately)
/*!40000 ALTER TABLE `referrals` DISABLE KEYS */;
INSERT INTO `referrals` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'A family member', 1, '2022-01-31 09:32:24', '2022-01-31 09:32:24'),
	(2, 'Facebook', 1, '2022-01-31 09:32:24', '2022-01-31 09:32:24'),
	(3, 'Friend', 1, '2022-01-31 09:32:24', '2022-01-31 09:32:24'),
	(4, 'Google Ad', 1, '2022-01-31 09:32:24', '2022-01-31 09:32:24');
/*!40000 ALTER TABLE `referrals` ENABLE KEYS */;

-- Dumping structure for table moveit.role
CREATE TABLE IF NOT EXISTS `role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_type` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.role: ~4 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `name`, `role_type`, `created_at`, `updated_at`) VALUES
	(1, 'Super Admin', 1, '2022-01-31 09:31:36', '2022-01-31 09:31:36'),
	(2, 'Admin', 1, '2022-01-31 09:31:36', '2022-01-31 09:31:36'),
	(3, 'Operator', 1, '2022-01-31 09:31:37', '2022-01-31 09:31:37');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dumping structure for table moveit.rolemoduleprivileges
CREATE TABLE IF NOT EXISTS `rolemoduleprivileges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT 0,
  `module_id` int(11) NOT NULL DEFAULT 0,
  `privilege_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.rolemoduleprivileges: ~6 rows (approximately)
/*!40000 ALTER TABLE `rolemoduleprivileges` DISABLE KEYS */;
INSERT INTO `rolemoduleprivileges` (`id`, `role_id`, `module_id`, `privilege_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, NULL, NULL),
	(2, 1, 1, 2, NULL, NULL),
	(3, 1, 1, 3, NULL, NULL),
	(4, 1, 1, 4, NULL, NULL),
	(5, 1, 7, 1, NULL, NULL),
	(6, 1, 7, 3, NULL, NULL);
/*!40000 ALTER TABLE `rolemoduleprivileges` ENABLE KEYS */;

-- Dumping structure for table moveit.services
CREATE TABLE IF NOT EXISTS `services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.services: ~2 rows (approximately)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'NEED PACKING MATERIALS', 1, '2022-01-31 09:32:27', '2022-01-31 09:32:27'),
	(2, 'NEED PACKING SERVICES', 1, '2022-01-31 09:32:27', '2022-01-31 09:32:27');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

-- Dumping structure for table moveit.service_types
CREATE TABLE IF NOT EXISTS `service_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.service_types: ~4 rows (approximately)
/*!40000 ALTER TABLE `service_types` DISABLE KEYS */;
INSERT INTO `service_types` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Residential Move', 1, '2022-01-31 09:32:31', '2022-01-31 09:32:31'),
	(2, 'Commercial Move', 1, '2022-01-31 09:32:31', '2022-01-31 09:32:31'),
	(3, 'Storage', 1, '2022-01-31 09:32:32', '2022-01-31 09:32:32'),
	(4, 'Packing Only', 1, '2022-01-31 09:32:32', '2022-01-31 09:32:32');
/*!40000 ALTER TABLE `service_types` ENABLE KEYS */;

-- Dumping structure for table moveit.storage_requirements
CREATE TABLE IF NOT EXISTS `storage_requirements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.storage_requirements: ~3 rows (approximately)
/*!40000 ALTER TABLE `storage_requirements` DISABLE KEYS */;
INSERT INTO `storage_requirements` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Warehouse Handling', 1, '2022-01-31 09:32:54', '2022-01-31 09:32:54'),
	(2, 'First Month Storage', 1, '2022-01-31 09:32:55', '2022-01-31 09:32:55'),
	(3, 'Misc Storage Fee', 1, '2022-01-31 09:32:55', '2022-01-31 09:32:55');
/*!40000 ALTER TABLE `storage_requirements` ENABLE KEYS */;

-- Dumping structure for table moveit.testimonials
CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.testimonials: ~0 rows (approximately)
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;

-- Dumping structure for table moveit.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_code` int(11) DEFAULT NULL,
  `reset_hash` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referred_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refferal_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_phone_verify` tinyint(4) NOT NULL DEFAULT 0,
  `is_email_verified` tinyint(4) NOT NULL DEFAULT 0,
  `phone_verified_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_accept_tc` tinyint(4) NOT NULL DEFAULT 0,
  `privacy_policy` tinyint(4) NOT NULL DEFAULT 0,
  `is_registered` tinyint(4) NOT NULL DEFAULT 0,
  `is_approve` tinyint(4) NOT NULL DEFAULT 0,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `app_version` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.users: ~5 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `gender`, `dob`, `phone_type`, `phone`, `profile_pic`, `reset_code`, `reset_hash`, `referral_code`, `referred_by`, `refferal_link`, `is_phone_verify`, `is_email_verified`, `phone_verified_at`, `email_verified_at`, `provider`, `last_login`, `is_accept_tc`, `privacy_policy`, `is_registered`, `is_approve`, `is_active`, `app_version`, `created_at`, `updated_at`) VALUES
	(1, 'Raviteja', NULL, 'Ch', 'ch.raviteja2@gmail.com', NULL, NULL, '1', '7189698624', NULL, 360652, 'eae8a518a7cabe4147819a974a578220fe2779c9ad08c50540', '404339', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-01-18 18:33:42', '2022-01-18 18:33:42'),
	(2, 'test', NULL, 'test', 'ravi@gmail.com', NULL, NULL, '2', '976766767676', NULL, 302126, 'a5bb4a942442992aca9d073e3d83724568d6a63bf1ee7554f4', 'F44CA8', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-03 20:45:45', '2022-02-03 20:45:45'),
	(3, 'test', NULL, 'test', 'ravi@gmail.com', NULL, NULL, '2', '98643634534', NULL, 372267, 'f3aa3652e8e2ab95b2e92d03cbd6b134a8a53b3fef8885d2bf', 'FBC4DD', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-03 20:58:26', '2022-02-03 20:58:26'),
	(4, 'test', NULL, 'test', 'rvi@gmail.com', NULL, NULL, '2', '97776567657', NULL, 188653, 'b6a3552683c7116a556b5120ebe4f9dd9ec780af6aa32333cb', 'AC6BD0', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-03 21:10:34', '2022-02-03 21:10:34'),
	(5, 'test', NULL, 'test', 'ts@gmail.com', NULL, NULL, '2', '978567567657', NULL, 178089, '5ccc27a89b4816b59c29e86b640a5ee4c3be136223cf190b38', '497879', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-03 21:18:54', '2022-02-03 21:18:54'),
	(6, 'test', NULL, 'test', 'ravi@gmail.com', NULL, NULL, '2', '98837773783', NULL, 299930, '2e3827814937af4d14e47d99fa026b9317038b8c4a882de13b', '94844B', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-03 21:32:23', '2022-02-03 21:32:23');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table moveit.user_basic_details
CREATE TABLE IF NOT EXISTS `user_basic_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT 0,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` tinyint(4) NOT NULL DEFAULT 0,
  `nationality_id` tinyint(4) NOT NULL DEFAULT 0,
  `address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(11) NOT NULL DEFAULT 0,
  `state_id` int(11) NOT NULL DEFAULT 0,
  `zipcode` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.user_basic_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_basic_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_basic_details` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
