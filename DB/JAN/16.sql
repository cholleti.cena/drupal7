-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.17-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table moveit.admin_users
CREATE TABLE IF NOT EXISTS `admin_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobileno` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secret` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.admin_users: ~0 rows (approximately)
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` (`id`, `name`, `email`, `password`, `mobileno`, `reset_hash`, `secret`, `role_id`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
	(1, 'Super Admin', 'admin@gmail.com', '$2y$10$K4JDEpSyMdfg29m5wElOmuGDzdzT.K4qaklGM0eTEGF9bb5pH06/a', NULL, NULL, NULL, 1, 1, 1, 1, '2022-01-15 22:10:01', '2022-01-15 22:10:01');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;

-- Dumping structure for table moveit.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.categories: ~10 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Basement', 1, '2022-01-15 22:10:03', '2022-01-15 22:10:03'),
	(2, 'Bedroom', 1, '2022-01-15 22:10:03', '2022-01-15 22:10:03'),
	(3, 'Electronics/Appliances', 1, '2022-01-15 22:10:03', '2022-01-15 22:10:03'),
	(4, 'Exercise', 1, '2022-01-15 22:10:03', '2022-01-15 22:10:03'),
	(5, 'Garage & Outdoors', 1, '2022-01-15 22:10:03', '2022-01-15 22:10:03'),
	(6, 'Industrial', 1, '2022-01-15 22:10:03', '2022-01-15 22:10:03'),
	(7, 'Kitchen & Dining', 1, '2022-01-15 22:10:03', '2022-01-15 22:10:03'),
	(8, 'Living & Family', 1, '2022-01-15 22:10:03', '2022-01-15 22:10:03'),
	(9, 'Misc/Other', 1, '2022-01-15 22:10:03', '2022-01-15 22:10:03'),
	(10, 'Office', 1, '2022-01-15 22:10:03', '2022-01-15 22:10:03');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table moveit.channel
CREATE TABLE IF NOT EXISTS `channel` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `channel_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `is_approve` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.channel: ~0 rows (approximately)
/*!40000 ALTER TABLE `channel` DISABLE KEYS */;
/*!40000 ALTER TABLE `channel` ENABLE KEYS */;

-- Dumping structure for table moveit.defaults
CREATE TABLE IF NOT EXISTS `defaults` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `allow_create_logs` tinyint(4) NOT NULL DEFAULT 0,
  `allow_edit_logs` tinyint(4) NOT NULL DEFAULT 0,
  `allow_delete_logs` tinyint(4) NOT NULL DEFAULT 0,
  `log_max_days` int(11) NOT NULL DEFAULT 0,
  `version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.defaults: ~0 rows (approximately)
/*!40000 ALTER TABLE `defaults` DISABLE KEYS */;
INSERT INTO `defaults` (`id`, `allow_create_logs`, `allow_edit_logs`, `allow_delete_logs`, `log_max_days`, `version`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 30, '1.0.0', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `defaults` ENABLE KEYS */;

-- Dumping structure for table moveit.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table moveit.gallery
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_category` int(11) DEFAULT NULL,
  `gallery` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.gallery: ~0 rows (approximately)
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;

-- Dumping structure for table moveit.gallery_categories
CREATE TABLE IF NOT EXISTS `gallery_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.gallery_categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `gallery_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery_categories` ENABLE KEYS */;

-- Dumping structure for table moveit.gallery_images
CREATE TABLE IF NOT EXISTS `gallery_images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.gallery_images: ~0 rows (approximately)
/*!40000 ALTER TABLE `gallery_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery_images` ENABLE KEYS */;

-- Dumping structure for table moveit.items
CREATE TABLE IF NOT EXISTS `items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `sulg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cubic_foot` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.items: ~2 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`id`, `name`, `category_id`, `sulg`, `description`, `price`, `weight`, `cubic_foot`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Snow Blower', 1, 'snow-blower', 'Snow Blower', 2000.00, '140', '15', 1, '2022-01-15 22:10:04', '2022-01-15 22:10:04'),
	(2, 'Armoire/Ward', 1, 'armoire-ward', 'Armoire/Ward', 1000.00, '490', '70', 1, '2022-01-15 22:10:04', '2022-01-15 22:10:04');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Dumping structure for table moveit.items_images
CREATE TABLE IF NOT EXISTS `items_images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) unsigned NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.items_images: ~0 rows (approximately)
/*!40000 ALTER TABLE `items_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `items_images` ENABLE KEYS */;

-- Dumping structure for table moveit.leads
CREATE TABLE IF NOT EXISTS `leads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `approximate_move_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_of_service` int(11) NOT NULL DEFAULT 0,
  `origin_address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `origin_latitude` decimal(11,2) NOT NULL DEFAULT 0.00,
  `origin_longitude` decimal(11,2) NOT NULL DEFAULT 0.00,
  `origin_property_type_id` int(11) DEFAULT NULL,
  `origin_floor` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination_address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination_latitude` decimal(11,2) NOT NULL DEFAULT 0.00,
  `destination_longitude` decimal(11,2) NOT NULL DEFAULT 0.00,
  `destination_property_type_id` int(11) DEFAULT NULL,
  `destination_floor` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `move_size_id` int(11) NOT NULL DEFAULT 0,
  `square_footage` int(11) NOT NULL DEFAULT 0,
  `estimated_boxes` int(11) NOT NULL DEFAULT 0,
  `total_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_cubic_foot` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `lead_status` tinyint(4) NOT NULL DEFAULT 1,
  `comments` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.leads: ~3 rows (approximately)
/*!40000 ALTER TABLE `leads` DISABLE KEYS */;
INSERT INTO `leads` (`id`, `user_id`, `approximate_move_date`, `type_of_service`, `origin_address`, `origin_latitude`, `origin_longitude`, `origin_property_type_id`, `origin_floor`, `destination_address`, `destination_latitude`, `destination_longitude`, `destination_property_type_id`, `destination_floor`, `move_size_id`, `square_footage`, `estimated_boxes`, `total_weight`, `total_cubic_foot`, `is_active`, `lead_status`, `comments`, `created_at`, `updated_at`) VALUES
	(1, 1, '01-01-2022', 1, 'Hyderabad, Telangana 500072', 17.48, 78.42, 1, '[1]', 'Hyderabad, Telangana 500072', 17.48, 78.42, 1, '[1]', 1, 1500, 10, '160', '18', 1, 1, NULL, '2022-01-15 20:02:24', '2022-01-15 20:02:24'),
	(2, 1, '01-01-2022', 1, 'Hyderabad, Telangana 500072', 17.48, 78.42, 1, '[1]', 'Hyderabad, Telangana 500072', 17.48, 78.42, 1, '[1]', 1, 1500, 10, '160', '18', 1, 1, NULL, '2022-01-15 20:14:55', '2022-01-15 20:14:55'),
	(3, 1, '01-01-2022', 1, 'Hyderabad, Telangana 500072', 17.48, 78.42, 1, '[1]', 'Hyderabad, Telangana 500072', 17.48, 78.42, 1, '[1]', 1, 1500, 10, '160', '18', 1, 1, NULL, '2022-01-15 20:15:22', '2022-01-15 20:15:22'),
	(4, 1, '01-01-2022', 1, 'Hyderabad, Telangana 500072', 17.48, 78.42, 1, '[1]', 'Hyderabad, Telangana 500072', 17.48, 78.42, 1, '[1]', 1, 1500, 10, '160', '18', 1, 1, 'test', '2022-01-15 23:06:38', '2022-01-15 23:06:38'),
	(5, 1, '01-01-2022', 1, 'Hyderabad, Telangana 500072', 17.48, 78.42, 1, '[1]', 'Hyderabad, Telangana 500072', 17.48, 78.42, 1, '[1]', 1, 1500, 10, '160', '18', 1, 1, 'test', '2022-01-16 11:52:10', '2022-01-16 11:52:10');
/*!40000 ALTER TABLE `leads` ENABLE KEYS */;

-- Dumping structure for table moveit.leads_items
CREATE TABLE IF NOT EXISTS `leads_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `lead_id` bigint(20) unsigned NOT NULL,
  `item_id` bigint(20) unsigned NOT NULL,
  `floor` int(11) NOT NULL DEFAULT 0,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cubic_foot` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.leads_items: ~8 rows (approximately)
/*!40000 ALTER TABLE `leads_items` DISABLE KEYS */;
INSERT INTO `leads_items` (`id`, `user_id`, `lead_id`, `item_id`, `floor`, `quantity`, `price`, `weight`, `cubic_foot`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 1, 1, 10.00, '50', '20', '2022-01-15 20:02:24', '2022-01-15 20:02:24'),
	(2, 1, 1, 2, 1, 1, 10.00, '60', '50', '2022-01-15 20:02:24', '2022-01-15 20:02:24'),
	(3, 1, 3, 1, 1, 1, 10.00, '50', '20', '2022-01-15 20:15:22', '2022-01-15 20:15:22'),
	(4, 1, 3, 2, 1, 1, 10.00, '60', '50', '2022-01-15 20:15:22', '2022-01-15 20:15:22'),
	(5, 1, 4, 1, 1, 1, 10.00, '50', '20', '2022-01-15 23:06:38', '2022-01-15 23:06:38'),
	(6, 1, 4, 2, 1, 1, 10.00, '60', '50', '2022-01-15 23:06:38', '2022-01-15 23:06:38'),
	(7, 1, 5, 1, 1, 1, 10.00, '50', '20', '2022-01-16 11:52:10', '2022-01-16 11:52:10'),
	(8, 1, 5, 2, 1, 1, 10.00, '60', '50', '2022-01-16 11:52:11', '2022-01-16 11:52:11');
/*!40000 ALTER TABLE `leads_items` ENABLE KEYS */;

-- Dumping structure for table moveit.leads_materials
CREATE TABLE IF NOT EXISTS `leads_materials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `lead_id` bigint(20) unsigned NOT NULL,
  `material_id` bigint(20) unsigned NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.leads_materials: ~3 rows (approximately)
/*!40000 ALTER TABLE `leads_materials` DISABLE KEYS */;
INSERT INTO `leads_materials` (`id`, `user_id`, `lead_id`, `material_id`, `quantity`, `price`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 1, 10.00, '2022-01-15 20:02:24', '2022-01-15 20:02:24'),
	(2, 1, 3, 1, 1, 10.00, '2022-01-15 20:15:23', '2022-01-15 20:15:23'),
	(3, 1, 4, 1, 1, 10.00, '2022-01-15 23:06:38', '2022-01-15 23:06:38'),
	(4, 1, 5, 1, 1, 10.00, '2022-01-16 11:52:11', '2022-01-16 11:52:11');
/*!40000 ALTER TABLE `leads_materials` ENABLE KEYS */;

-- Dumping structure for table moveit.leads_services
CREATE TABLE IF NOT EXISTS `leads_services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `lead_id` bigint(20) unsigned NOT NULL,
  `service_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.leads_services: ~8 rows (approximately)
/*!40000 ALTER TABLE `leads_services` DISABLE KEYS */;
INSERT INTO `leads_services` (`id`, `user_id`, `lead_id`, `service_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, '2022-01-15 20:02:24', '2022-01-15 20:02:24'),
	(2, 1, 1, 2, '2022-01-15 20:02:24', '2022-01-15 20:02:24'),
	(3, 1, 3, 1, '2022-01-15 20:15:24', '2022-01-15 20:15:24'),
	(4, 1, 3, 2, '2022-01-15 20:15:24', '2022-01-15 20:15:24'),
	(5, 1, 4, 1, '2022-01-15 23:06:38', '2022-01-15 23:06:38'),
	(6, 1, 4, 2, '2022-01-15 23:06:39', '2022-01-15 23:06:39'),
	(7, 1, 5, 1, '2022-01-16 11:52:11', '2022-01-16 11:52:11'),
	(8, 1, 5, 2, '2022-01-16 11:52:12', '2022-01-16 11:52:12');
/*!40000 ALTER TABLE `leads_services` ENABLE KEYS */;

-- Dumping structure for table moveit.materials
CREATE TABLE IF NOT EXISTS `materials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.materials: ~2 rows (approximately)
/*!40000 ALTER TABLE `materials` DISABLE KEYS */;
INSERT INTO `materials` (`id`, `name`, `description`, `price`, `image`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Snow Blower', 'Snow Blower', 2000.00, NULL, 1, '2022-01-15 22:10:04', '2022-01-15 22:10:04'),
	(2, 'Armoire/Ward', 'Armoire/Ward', 1000.00, NULL, 1, '2022-01-15 22:10:04', '2022-01-15 22:10:04');
/*!40000 ALTER TABLE `materials` ENABLE KEYS */;

-- Dumping structure for table moveit.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.migrations: ~33 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(6, '2016_06_01_000004_create_oauth_clients_table', 1),
	(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
	(8, '2019_08_19_000000_create_failed_jobs_table', 1),
	(9, '2021_04_06_015221_create_admin_users_table', 1),
	(10, '2021_04_06_020606_create_defaults_table', 1),
	(11, '2021_04_06_231416_create_role_table', 1),
	(12, '2021_04_20_154612_create_news_table', 1),
	(13, '2021_04_28_072402_create_news_categories_table', 1),
	(14, '2021_05_03_114609_create_news_types_table', 1),
	(15, '2021_05_03_130413_create_gallery_table', 1),
	(16, '2021_05_04_073206_create_gallery_images_table', 1),
	(17, '2021_05_13_082908_create_gallery_categories_table', 1),
	(18, '2021_10_26_233937_create_channel_table', 1),
	(19, '2021_12_01_150057_create_news_likes_table', 1),
	(20, '2022_01_15_132413_create_users_referrals_table', 2),
	(21, '2022_01_15_132647_create_services_table', 3),
	(22, '2022_01_15_141012_create_categories_table', 3),
	(23, '2022_01_15_141355_create_items_table', 4),
	(24, '2022_01_15_141802_create_items_images_table', 4),
	(25, '2022_01_15_142144_create_materials_table', 4),
	(26, '2022_01_15_142323_create_move_sizes_table', 4),
	(27, '2022_01_15_142415_create_phone_types_table', 4),
	(28, '2022_01_15_142505_create_properties_table', 4),
	(29, '2022_01_15_142608_create_referrals_table', 4),
	(30, '2022_01_15_142702_create_service_types_table', 4),
	(31, '2022_01_15_142821_create_leads_table', 4),
	(32, '2022_01_15_143821_create_leads_items_table', 4),
	(33, '2022_01_15_144232_create_leads_materials_table', 4),
	(34, '2022_01_15_144419_create_leads_services_table', 4),
	(35, '2022_01_15_205041_create_move_status_table', 5);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table moveit.move_sizes
CREATE TABLE IF NOT EXISTS `move_sizes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.move_sizes: ~7 rows (approximately)
/*!40000 ALTER TABLE `move_sizes` DISABLE KEYS */;
INSERT INTO `move_sizes` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Studio', 1, '2022-01-15 22:10:06', '2022-01-15 22:10:06'),
	(2, '1 Bedroom', 1, '2022-01-15 22:10:06', '2022-01-15 22:10:06'),
	(3, '2 Bedroom', 1, '2022-01-15 22:10:06', '2022-01-15 22:10:06'),
	(4, '3 Bedroom', 1, '2022-01-15 22:10:06', '2022-01-15 22:10:06'),
	(5, '4 Bedroom', 1, '2022-01-15 22:10:06', '2022-01-15 22:10:06'),
	(6, '5+ Bedroom', 1, '2022-01-15 22:10:06', '2022-01-15 22:10:06'),
	(7, 'Office', 1, '2022-01-15 22:10:06', '2022-01-15 22:10:06');
/*!40000 ALTER TABLE `move_sizes` ENABLE KEYS */;

-- Dumping structure for table moveit.move_status
CREATE TABLE IF NOT EXISTS `move_status` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.move_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `move_status` DISABLE KEYS */;
INSERT INTO `move_status` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'New Lead', 1, '2022-01-15 22:10:11', '2022-01-15 22:10:11');
/*!40000 ALTER TABLE `move_status` ENABLE KEYS */;

-- Dumping structure for table moveit.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `channel_id` bigint(20) unsigned NOT NULL,
  `news_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_type` int(11) NOT NULL DEFAULT 0,
  `news_type` int(11) NOT NULL DEFAULT 0,
  `publish_start_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish_end_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `sub_category_id` int(11) NOT NULL DEFAULT 0,
  `likes` int(11) NOT NULL DEFAULT 0,
  `shares` int(11) NOT NULL DEFAULT 0,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_link` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `is_publish` tinyint(4) NOT NULL DEFAULT 0,
  `is_approve` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.news: ~0 rows (approximately)
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
/*!40000 ALTER TABLE `news` ENABLE KEYS */;

-- Dumping structure for table moveit.news_categories
CREATE TABLE IF NOT EXISTS `news_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `modified_by` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.news_categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `news_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_categories` ENABLE KEYS */;

-- Dumping structure for table moveit.news_likes
CREATE TABLE IF NOT EXISTS `news_likes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.news_likes: ~0 rows (approximately)
/*!40000 ALTER TABLE `news_likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_likes` ENABLE KEYS */;

-- Dumping structure for table moveit.news_types
CREATE TABLE IF NOT EXISTS `news_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.news_types: ~0 rows (approximately)
/*!40000 ALTER TABLE `news_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_types` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_access_tokens: ~4 rows (approximately)
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
	('43fd27515a8660f38dbe5ebe5c52143b0edaedcb99af62bbc7c8949970bcecb1b4e037d73e7b3441', 1, 1, 'AppName', '[]', 0, '2022-01-16 11:51:57', '2022-01-16 11:51:57', '2023-01-16 11:51:57'),
	('7d085bbc2a1525e744cd902353df0bfea6e69f46af11575834a3cd5eab6a89106e9fa51971f1395a', 1, 1, 'AppName', '[]', 0, '2022-01-15 18:43:36', '2022-01-15 18:43:36', '2023-01-15 18:43:36'),
	('932c0d85e1bdc5c243cf34fd0d16d61ce203acce34dc67def60147b8f4a1dc69a4b694805f1c3a1a', 1, 1, 'AppName', '[]', 0, '2022-01-15 18:42:10', '2022-01-15 18:42:10', '2023-01-15 18:42:10'),
	('ff8ab80d9d6e4f8606eba91c8ce3ccbcd88d53e4add2422a5661b47768bc2b211ddd019fa38e38e3', 1, 1, 'AppName', '[]', 0, '2022-01-15 18:41:24', '2022-01-15 18:41:24', '2023-01-15 18:41:24');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_auth_codes: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_clients: ~2 rows (approximately)
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Laravel Personal Access Client', 'u7KP9MY8cL0OnIO8iMr4rcHzTdWooxnBiu6p9LoD', NULL, 'http://localhost', 1, 0, 0, '2022-01-15 18:40:56', '2022-01-15 18:40:56'),
	(2, NULL, 'Laravel Password Grant Client', 'fWDdXVSMkQuzOXxjAb8Fhrtnh04ojubqPIg8RJCh', 'users', 'http://localhost', 0, 1, 0, '2022-01-15 18:40:58', '2022-01-15 18:40:58');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_personal_access_clients: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
	(1, 1, '2022-01-15 18:40:57', '2022-01-15 18:40:57');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_refresh_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Dumping structure for table moveit.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table moveit.phone_types
CREATE TABLE IF NOT EXISTS `phone_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.phone_types: ~4 rows (approximately)
/*!40000 ALTER TABLE `phone_types` DISABLE KEYS */;
INSERT INTO `phone_types` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Mobile Phone', 1, '2022-01-15 22:10:06', '2022-01-15 22:10:06'),
	(2, 'Business phone', 1, '2022-01-15 22:10:06', '2022-01-15 22:10:06'),
	(3, 'Home phone', 1, '2022-01-15 22:10:07', '2022-01-15 22:10:07'),
	(4, 'Fax', 1, '2022-01-15 22:10:07', '2022-01-15 22:10:07');
/*!40000 ALTER TABLE `phone_types` ENABLE KEYS */;

-- Dumping structure for table moveit.properties
CREATE TABLE IF NOT EXISTS `properties` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.properties: ~10 rows (approximately)
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
INSERT INTO `properties` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Apartment', 1, '2022-01-15 22:10:07', '2022-01-15 22:10:07'),
	(2, 'Condo', 1, '2022-01-15 22:10:07', '2022-01-15 22:10:07'),
	(3, 'Dock Height', 1, '2022-01-15 22:10:07', '2022-01-15 22:10:07'),
	(4, 'House', 1, '2022-01-15 22:10:07', '2022-01-15 22:10:07'),
	(5, 'Nursing Home', 1, '2022-01-15 22:10:07', '2022-01-15 22:10:07'),
	(6, 'Office Building', 1, '2022-01-15 22:10:07', '2022-01-15 22:10:07'),
	(7, 'Senior Community', 1, '2022-01-15 22:10:07', '2022-01-15 22:10:07'),
	(8, 'Storage', 1, '2022-01-15 22:10:08', '2022-01-15 22:10:08'),
	(9, 'Town House', 1, '2022-01-15 22:10:08', '2022-01-15 22:10:08'),
	(10, 'Warehouse', 1, '2022-01-15 22:10:08', '2022-01-15 22:10:08');
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;

-- Dumping structure for table moveit.referrals
CREATE TABLE IF NOT EXISTS `referrals` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.referrals: ~4 rows (approximately)
/*!40000 ALTER TABLE `referrals` DISABLE KEYS */;
INSERT INTO `referrals` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'A family member', 1, '2022-01-15 22:10:08', '2022-01-15 22:10:08'),
	(2, 'Facebook', 1, '2022-01-15 22:10:08', '2022-01-15 22:10:08'),
	(3, 'Friend', 1, '2022-01-15 22:10:08', '2022-01-15 22:10:08'),
	(4, 'Google Ad', 1, '2022-01-15 22:10:08', '2022-01-15 22:10:08');
/*!40000 ALTER TABLE `referrals` ENABLE KEYS */;

-- Dumping structure for table moveit.role
CREATE TABLE IF NOT EXISTS `role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_type` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.role: ~2 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `name`, `role_type`, `created_at`, `updated_at`) VALUES
	(1, 'Super Admin', 1, '2022-01-15 22:10:00', '2022-01-15 22:10:00'),
	(2, 'Admin', 1, '2022-01-15 22:10:00', '2022-01-15 22:10:00'),
	(3, 'Operator', 1, '2022-01-15 22:10:00', '2022-01-15 22:10:00');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dumping structure for table moveit.services
CREATE TABLE IF NOT EXISTS `services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.services: ~2 rows (approximately)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'NEED PACKING MATERIALS', 1, '2022-01-15 22:10:09', '2022-01-15 22:10:09'),
	(2, 'NEED PACKING SERVICES', 1, '2022-01-15 22:10:09', '2022-01-15 22:10:09');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

-- Dumping structure for table moveit.service_types
CREATE TABLE IF NOT EXISTS `service_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.service_types: ~4 rows (approximately)
/*!40000 ALTER TABLE `service_types` DISABLE KEYS */;
INSERT INTO `service_types` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Residential Move', 1, '2022-01-15 22:10:10', '2022-01-15 22:10:10'),
	(2, 'Commercial Move', 1, '2022-01-15 22:10:10', '2022-01-15 22:10:10'),
	(3, 'Storage', 1, '2022-01-15 22:10:10', '2022-01-15 22:10:10'),
	(4, 'Packing Only', 1, '2022-01-15 22:10:10', '2022-01-15 22:10:10');
/*!40000 ALTER TABLE `service_types` ENABLE KEYS */;

-- Dumping structure for table moveit.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_code` int(11) DEFAULT NULL,
  `reset_hash` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referred_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refferal_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_phone_verify` tinyint(4) NOT NULL DEFAULT 0,
  `is_email_verified` tinyint(4) NOT NULL DEFAULT 0,
  `phone_verified_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_accept_tc` tinyint(4) NOT NULL DEFAULT 0,
  `privacy_policy` tinyint(4) NOT NULL DEFAULT 0,
  `is_registered` tinyint(4) NOT NULL DEFAULT 0,
  `is_approve` tinyint(4) NOT NULL DEFAULT 0,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `app_version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `gender`, `dob`, `phone_type`, `phone`, `profile_pic`, `reset_code`, `reset_hash`, `referral_code`, `referred_by`, `refferal_link`, `is_phone_verify`, `is_email_verified`, `phone_verified_at`, `email_verified_at`, `provider`, `last_login`, `is_accept_tc`, `privacy_policy`, `is_registered`, `is_approve`, `is_active`, `app_version`, `created_at`, `updated_at`) VALUES
	(1, 'Raviteja', NULL, 'Ch', 'ch.raviteja2@gmail.com', NULL, NULL, '1', '7189698624', NULL, 856664, '1696b4394b649d0f0bcbdd591be8b13726d7253cef08a28d8b', 'B26542', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-01-15 20:02:24', '2022-01-15 20:02:24');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table moveit.users_referrals
CREATE TABLE IF NOT EXISTS `users_referrals` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `referred_by` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.users_referrals: ~0 rows (approximately)
/*!40000 ALTER TABLE `users_referrals` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_referrals` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
