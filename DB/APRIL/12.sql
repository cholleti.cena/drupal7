-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.17-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table moveit.addendums
CREATE TABLE IF NOT EXISTS `addendums` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.addendums: ~0 rows (approximately)
/*!40000 ALTER TABLE `addendums` DISABLE KEYS */;
/*!40000 ALTER TABLE `addendums` ENABLE KEYS */;

-- Dumping structure for table moveit.additional_charges
CREATE TABLE IF NOT EXISTS `additional_charges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.additional_charges: ~6 rows (approximately)
/*!40000 ALTER TABLE `additional_charges` DISABLE KEYS */;
INSERT INTO `additional_charges` (`id`, `name`, `price`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, '2 movers for two hours', 300.00, 1, '2022-04-10 17:00:55', '2022-04-10 17:00:55'),
	(2, 'Additional Mover', 0.00, 1, '2022-04-10 17:00:55', '2022-04-10 17:00:55'),
	(3, 'Cancellation Fee (Within 48 hr)', 100.00, 1, '2022-04-10 17:00:55', '2022-04-10 17:00:55'),
	(4, 'CC Processing Fee', 0.00, 1, '2022-04-10 17:00:56', '2022-04-10 17:00:56'),
	(5, 'Claim', 0.00, 1, '2022-04-10 17:00:56', '2022-04-10 17:00:56'),
	(6, 'Exercise Equipment', 300.00, 1, '2022-04-10 17:00:56', '2022-04-10 17:00:56');
/*!40000 ALTER TABLE `additional_charges` ENABLE KEYS */;

-- Dumping structure for table moveit.admin_users
CREATE TABLE IF NOT EXISTS `admin_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobileno` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secret` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.admin_users: ~0 rows (approximately)
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` (`id`, `name`, `email`, `password`, `mobileno`, `reset_hash`, `secret`, `role_id`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
	(1, 'Super Admin', 'admin@gmail.com', '$2y$10$/U3mk/NHxtH/mJwBkItM9ey4r6IuW05FdCaa7WA8oMfV84BTsjWAu', NULL, 'a00b971c8ec7d8eb081fe7bb2733a000020356e3096558b467', NULL, 1, 1, 1, 1, '2022-04-10 16:59:40', '2022-04-11 11:00:26');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;

-- Dumping structure for table moveit.boltext
CREATE TABLE IF NOT EXISTS `boltext` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bol` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtotal` tinyint(4) NOT NULL DEFAULT 1,
  `tip` tinyint(4) NOT NULL DEFAULT 1,
  `cc_processing_fee` double(8,2) NOT NULL DEFAULT 4.00,
  `cc_fee` tinyint(4) NOT NULL DEFAULT 1,
  `shipment_text` tinyint(4) NOT NULL DEFAULT 0,
  `storage_access` tinyint(4) NOT NULL DEFAULT 1,
  `notice` tinyint(4) NOT NULL DEFAULT 1,
  `days_in_advance` int(11) NOT NULL DEFAULT 0,
  `article_per_pound_amount` double(8,2) NOT NULL DEFAULT 0.60,
  `overtime_rate` int(11) DEFAULT NULL,
  `overtime_charge_after` int(11) DEFAULT NULL,
  `disable_for_foreman` tinyint(4) NOT NULL DEFAULT 0,
  `crew_review_popup` tinyint(4) NOT NULL DEFAULT 1,
  `rating_default_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_tip_crew_popup` tinyint(4) NOT NULL DEFAULT 1,
  `tip_for_crew_default` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.boltext: ~0 rows (approximately)
/*!40000 ALTER TABLE `boltext` DISABLE KEYS */;
INSERT INTO `boltext` (`id`, `bol`, `subtotal`, `tip`, `cc_processing_fee`, `cc_fee`, `shipment_text`, `storage_access`, `notice`, `days_in_advance`, `article_per_pound_amount`, `overtime_rate`, `overtime_charge_after`, `disable_for_foreman`, `crew_review_popup`, `rating_default_text`, `show_tip_crew_popup`, `tip_for_crew_default`, `created_at`, `updated_at`) VALUES
	(1, 'Texas BOL', 1, 1, 4.40, 1, 1, 1, 1, 4, 0.40, NULL, NULL, 0, 1, NULL, 1, NULL, NULL, '2022-03-29 18:41:47');
/*!40000 ALTER TABLE `boltext` ENABLE KEYS */;

-- Dumping structure for table moveit.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.categories: ~11 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Basement', 1, '2022-04-10 16:59:46', '2022-04-10 16:59:46'),
	(2, 'Bedroom', 1, '2022-04-10 16:59:46', '2022-04-10 16:59:46'),
	(3, 'Electronics/Appliances', 1, '2022-04-10 16:59:46', '2022-04-10 16:59:46'),
	(4, 'Exercise', 1, '2022-04-10 16:59:47', '2022-04-10 16:59:47'),
	(5, 'Garage & Outdoors', 1, '2022-04-10 16:59:47', '2022-04-10 16:59:47'),
	(6, 'Industrial', 1, '2022-04-10 16:59:47', '2022-04-10 16:59:47'),
	(7, 'Kitchen & Dining', 1, '2022-04-10 16:59:48', '2022-04-10 16:59:48'),
	(8, 'Living & Family', 1, '2022-04-10 16:59:48', '2022-04-10 16:59:48'),
	(9, 'Misc/Other', 1, '2022-04-10 16:59:48', '2022-04-10 16:59:48'),
	(10, 'Office', 1, '2022-04-10 16:59:49', '2022-04-10 16:59:49'),
	(11, 'Categories', 1, '2022-04-10 16:59:49', '2022-04-10 16:59:49');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table moveit.client_tags
CREATE TABLE IF NOT EXISTS `client_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.client_tags: ~0 rows (approximately)
/*!40000 ALTER TABLE `client_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_tags` ENABLE KEYS */;

-- Dumping structure for table moveit.config_labels
CREATE TABLE IF NOT EXISTS `config_labels` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `configtype_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.config_labels: ~0 rows (approximately)
/*!40000 ALTER TABLE `config_labels` DISABLE KEYS */;
/*!40000 ALTER TABLE `config_labels` ENABLE KEYS */;

-- Dumping structure for table moveit.config_types
CREATE TABLE IF NOT EXISTS `config_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.config_types: ~0 rows (approximately)
/*!40000 ALTER TABLE `config_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `config_types` ENABLE KEYS */;

-- Dumping structure for table moveit.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.countries: ~0 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `code`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'IN', 'IN', '1', '2022-04-10', '2022-04-10 13:19:26', '2022-04-10 13:19:27');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table moveit.defaults
CREATE TABLE IF NOT EXISTS `defaults` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `allow_create_logs` tinyint(4) NOT NULL DEFAULT 0,
  `allow_edit_logs` tinyint(4) NOT NULL DEFAULT 0,
  `allow_delete_logs` tinyint(4) NOT NULL DEFAULT 0,
  `log_max_days` int(11) NOT NULL DEFAULT 0,
  `version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.defaults: ~0 rows (approximately)
/*!40000 ALTER TABLE `defaults` DISABLE KEYS */;
INSERT INTO `defaults` (`id`, `allow_create_logs`, `allow_edit_logs`, `allow_delete_logs`, `log_max_days`, `version`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 30, '1.0.0', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `defaults` ENABLE KEYS */;

-- Dumping structure for table moveit.distance_from_truck
CREATE TABLE IF NOT EXISTS `distance_from_truck` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `distance_from_truck` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room` decimal(11,2) NOT NULL DEFAULT 0.00,
  `o_s_items` decimal(11,2) NOT NULL DEFAULT 0.00,
  `inventory` decimal(11,2) NOT NULL DEFAULT 0.00,
  `boxes` decimal(11,2) NOT NULL DEFAULT 0.00,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.distance_from_truck: ~0 rows (approximately)
/*!40000 ALTER TABLE `distance_from_truck` DISABLE KEYS */;
/*!40000 ALTER TABLE `distance_from_truck` ENABLE KEYS */;

-- Dumping structure for table moveit.employees
CREATE TABLE IF NOT EXISTS `employees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `emp_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nick_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_authentication` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chat_prmsn` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incoming_call_prmsn` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_chat_prmsn` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chat_admin_prmsn` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `call_center_contact_prmsn` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_user_mgmt_prmsn` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prop_app_user_prmsn` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foreman_prmsn` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `groups_access` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assign_crew_mem` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_pic` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_phone_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_zone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ssn` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driver_license` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relationship` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `get_to_know` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biography` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_status` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_type` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hire_date` date DEFAULT NULL,
  `termination_date` date DEFAULT NULL,
  `hourly_rate` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commission_rate` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `join_date` date NOT NULL,
  `end_date` date NOT NULL,
  `client_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.employees: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Dumping structure for table moveit.employees_addresses
CREATE TABLE IF NOT EXISTS `employees_addresses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suite` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `landmark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.employees_addresses: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees_addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees_addresses` ENABLE KEYS */;

-- Dumping structure for table moveit.employees_client_reviews
CREATE TABLE IF NOT EXISTS `employees_client_reviews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estimate_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `crew_member_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating_number` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dates` date NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.employees_client_reviews: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees_client_reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees_client_reviews` ENABLE KEYS */;

-- Dumping structure for table moveit.employees_disciplinary_notes
CREATE TABLE IF NOT EXISTS `employees_disciplinary_notes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rep` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estimate_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issues` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.employees_disciplinary_notes: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees_disciplinary_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees_disciplinary_notes` ENABLE KEYS */;

-- Dumping structure for table moveit.employees_email_ids
CREATE TABLE IF NOT EXISTS `employees_email_ids` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.employees_email_ids: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees_email_ids` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees_email_ids` ENABLE KEYS */;

-- Dumping structure for table moveit.employees_phone_numbers
CREATE TABLE IF NOT EXISTS `employees_phone_numbers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.employees_phone_numbers: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees_phone_numbers` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees_phone_numbers` ENABLE KEYS */;

-- Dumping structure for table moveit.estimate_range
CREATE TABLE IF NOT EXISTS `estimate_range` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `estimate_range_from` int(11) NOT NULL DEFAULT 0,
  `estimate_range_to` int(11) NOT NULL DEFAULT 0,
  `total_estimate_price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.estimate_range: ~0 rows (approximately)
/*!40000 ALTER TABLE `estimate_range` DISABLE KEYS */;
/*!40000 ALTER TABLE `estimate_range` ENABLE KEYS */;

-- Dumping structure for table moveit.expertise_types
CREATE TABLE IF NOT EXISTS `expertise_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.expertise_types: ~0 rows (approximately)
/*!40000 ALTER TABLE `expertise_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `expertise_types` ENABLE KEYS */;

-- Dumping structure for table moveit.facilities
CREATE TABLE IF NOT EXISTS `facilities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_id` int(11) NOT NULL DEFAULT 0,
  `zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `late_fee` decimal(11,2) DEFAULT NULL,
  `default_terms` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_cost` decimal(11,2) DEFAULT NULL,
  `terms` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_storage` tinyint(4) NOT NULL DEFAULT 0,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.facilities: ~0 rows (approximately)
/*!40000 ALTER TABLE `facilities` DISABLE KEYS */;
INSERT INTO `facilities` (`id`, `name`, `street`, `state_id`, `zip`, `phone`, `fax`, `email`, `late_fee`, `default_terms`, `minimum_cost`, `terms`, `default_storage`, `is_active`, `created_at`, `updated_at`) VALUES
	(2, 'test', 'test', 3932, '24324', '243243243', 'adawe', 'were@gmail.com', 4234.00, 'werewr', 3432.00, 'erwer', 1, 1, '2022-03-22 13:03:54', '2022-03-22 13:03:54');
/*!40000 ALTER TABLE `facilities` ENABLE KEYS */;

-- Dumping structure for table moveit.facility_units
CREATE TABLE IF NOT EXISTS `facility_units` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `facility_id` int(11) NOT NULL DEFAULT 0,
  `unit_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` decimal(11,2) DEFAULT 0.00,
  `height` decimal(11,2) DEFAULT 0.00,
  `depth` decimal(11,2) DEFAULT 0.00,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) DEFAULT 0.00,
  `quantity` int(11) DEFAULT 0,
  `cuft` decimal(11,2) NOT NULL DEFAULT 0.00,
  `total_occupied` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.facility_units: ~0 rows (approximately)
/*!40000 ALTER TABLE `facility_units` DISABLE KEYS */;
INSERT INTO `facility_units` (`id`, `facility_id`, `unit_name`, `width`, `height`, `depth`, `location`, `price`, `quantity`, `cuft`, `total_occupied`, `created_at`, `updated_at`) VALUES
	(1, 2, '10x10', 10.00, 10.00, 10.00, 'testdasf', 1.00, 1, 1000.00, 3, '2022-03-22 15:09:23', '2022-03-22 15:26:44');
/*!40000 ALTER TABLE `facility_units` ENABLE KEYS */;

-- Dumping structure for table moveit.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table moveit.faqs
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.faqs: ~0 rows (approximately)
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;

-- Dumping structure for table moveit.fleets
CREATE TABLE IF NOT EXISTS `fleets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `make` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dimensions_l` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dimensions_w` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dimensions_h` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cuft_capacity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VIN` int(11) DEFAULT NULL,
  `tag_number` int(11) DEFAULT NULL,
  `tag_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expires` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insurance_company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_number` int(11) DEFAULT NULL,
  `hourly_rate` decimal(8,2) DEFAULT NULL,
  `vehicle_status` int(11) DEFAULT NULL,
  `owner_ship` int(11) DEFAULT NULL,
  `is_schedulable` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.fleets: ~0 rows (approximately)
/*!40000 ALTER TABLE `fleets` DISABLE KEYS */;
INSERT INTO `fleets` (`id`, `nickname`, `year`, `make`, `model`, `type`, `dimensions_l`, `dimensions_w`, `dimensions_h`, `cuft_capacity`, `VIN`, `tag_number`, `tag_country`, `tag_state`, `expires`, `insurance_company`, `policy_number`, `hourly_rate`, `vehicle_status`, `owner_ship`, `is_schedulable`, `created_at`, `updated_at`) VALUES
	(1, 'test', 3, 'erwe', '3234', '1', '34', '3', '3', '4', 34, 42, '1', NULL, '2022-04-23', '34', 3432, 34.00, 2, 2, 0, '2022-04-10 16:32:19', '2022-04-11 12:26:46');
/*!40000 ALTER TABLE `fleets` ENABLE KEYS */;

-- Dumping structure for table moveit.fleettypes
CREATE TABLE IF NOT EXISTS `fleettypes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.fleettypes: ~0 rows (approximately)
/*!40000 ALTER TABLE `fleettypes` DISABLE KEYS */;
INSERT INTO `fleettypes` (`id`, `name`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Box Truck', '1', '1', 1, '2022-04-10 13:08:14', '2022-04-10 13:08:14');
/*!40000 ALTER TABLE `fleettypes` ENABLE KEYS */;

-- Dumping structure for table moveit.floors
CREATE TABLE IF NOT EXISTS `floors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `floor` int(11) NOT NULL DEFAULT 0,
  `extra_time_Room` decimal(11,2) NOT NULL DEFAULT 0.00,
  `stairs_os_items` decimal(11,2) NOT NULL DEFAULT 0.00,
  `stairs_inventory` decimal(11,2) NOT NULL DEFAULT 0.00,
  `stairs_boxes` decimal(11,2) NOT NULL DEFAULT 0.00,
  `elevator_inventory` decimal(11,2) NOT NULL DEFAULT 0.00,
  `elevator_boxes` decimal(11,2) NOT NULL DEFAULT 0.00,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.floors: ~0 rows (approximately)
/*!40000 ALTER TABLE `floors` DISABLE KEYS */;
/*!40000 ALTER TABLE `floors` ENABLE KEYS */;

-- Dumping structure for table moveit.follow_up_automation
CREATE TABLE IF NOT EXISTS `follow_up_automation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `days_until_move` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `follow_up_hours` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.follow_up_automation: ~0 rows (approximately)
/*!40000 ALTER TABLE `follow_up_automation` DISABLE KEYS */;
/*!40000 ALTER TABLE `follow_up_automation` ENABLE KEYS */;

-- Dumping structure for table moveit.fuel_charges
CREATE TABLE IF NOT EXISTS `fuel_charges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `min_range` int(11) NOT NULL DEFAULT 0,
  `max_range` int(11) NOT NULL DEFAULT 0,
  `min_price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `price_per_mile` decimal(11,2) NOT NULL DEFAULT 0.00,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.fuel_charges: ~0 rows (approximately)
/*!40000 ALTER TABLE `fuel_charges` DISABLE KEYS */;
/*!40000 ALTER TABLE `fuel_charges` ENABLE KEYS */;

-- Dumping structure for table moveit.home_banners
CREATE TABLE IF NOT EXISTS `home_banners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `header_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hear_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.home_banners: ~0 rows (approximately)
/*!40000 ALTER TABLE `home_banners` DISABLE KEYS */;
/*!40000 ALTER TABLE `home_banners` ENABLE KEYS */;

-- Dumping structure for table moveit.invoicev2_maxprice
CREATE TABLE IF NOT EXISTS `invoicev2_maxprice` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `heading` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.invoicev2_maxprice: ~2 rows (approximately)
/*!40000 ALTER TABLE `invoicev2_maxprice` DISABLE KEYS */;
INSERT INTO `invoicev2_maxprice` (`id`, `heading`, `content`, `created_at`, `updated_at`) VALUES
	(1, 'INVOICE NOTE V2	', 'GRATUITIES ARE APPRECIATED BUT NOT REQUIRED!', NULL, NULL),
	(2, 'Maximum Price', 'GRATUITIES ARE APPRECIATED BUT NOT REQUIRED! teesdfsdf', NULL, '2022-04-01 17:27:37');
/*!40000 ALTER TABLE `invoicev2_maxprice` ENABLE KEYS */;

-- Dumping structure for table moveit.invoice_notes
CREATE TABLE IF NOT EXISTS `invoice_notes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `notice_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.invoice_notes: ~2 rows (approximately)
/*!40000 ALTER TABLE `invoice_notes` DISABLE KEYS */;
INSERT INTO `invoice_notes` (`id`, `notice_content`, `created_at`, `updated_at`) VALUES
	(1, 'GRATUITIES ARE APPRECIATED BUT NOT REQUIRED!', NULL, NULL),
	(2, '3 HOUR AND 30 MINUTE MINIMUM (3.5 HOURS OF WORK TIME)', NULL, NULL);
/*!40000 ALTER TABLE `invoice_notes` ENABLE KEYS */;

-- Dumping structure for table moveit.items
CREATE TABLE IF NOT EXISTS `items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `sulg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cubic_foot` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `moving_time` int(11) DEFAULT NULL,
  `over_size` tinyint(4) DEFAULT 0,
  `taxable` int(11) DEFAULT 0,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.items: ~2 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`id`, `name`, `category_id`, `sulg`, `description`, `price`, `weight`, `cubic_foot`, `message`, `moving_time`, `over_size`, `taxable`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Snow Blower', 1, 'snow-blower', 'Snow Blower', 2000.00, '140', '15', NULL, NULL, 0, 0, 1, '2022-04-10 16:59:51', '2022-04-10 16:59:51'),
	(2, 'Armoire/Ward', 2, 'armoire-ward', 'Armoire/Ward', 1000.00, '490', '70', NULL, NULL, 0, 0, 1, '2022-04-10 16:59:51', '2022-04-10 16:59:51');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Dumping structure for table moveit.items_images
CREATE TABLE IF NOT EXISTS `items_images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) unsigned NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.items_images: ~0 rows (approximately)
/*!40000 ALTER TABLE `items_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `items_images` ENABLE KEYS */;

-- Dumping structure for table moveit.leads
CREATE TABLE IF NOT EXISTS `leads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `approximate_move_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_of_service` int(11) NOT NULL DEFAULT 0,
  `origin_address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `origin_latitude` decimal(11,4) NOT NULL DEFAULT 0.0000,
  `origin_longitude` decimal(11,4) NOT NULL DEFAULT 0.0000,
  `origin_elevator` tinyint(4) NOT NULL DEFAULT 0,
  `origin_property_type_id` int(11) DEFAULT NULL,
  `origin_floor` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination_address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination_latitude` decimal(11,4) NOT NULL DEFAULT 0.0000,
  `destination_longitude` decimal(11,4) NOT NULL DEFAULT 0.0000,
  `destination_elevator` tinyint(4) NOT NULL DEFAULT 0,
  `destination_property_type_id` int(11) DEFAULT NULL,
  `destination_floor` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `move_size_id` int(11) NOT NULL DEFAULT 0,
  `square_footage` int(11) NOT NULL DEFAULT 0,
  `estimated_boxes` int(11) NOT NULL DEFAULT 0,
  `total_weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_cubic_foot` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lead_status` int(11) NOT NULL DEFAULT 1,
  `packing_requirement_id` int(11) NOT NULL DEFAULT 1,
  `comments` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.leads: ~13 rows (approximately)
/*!40000 ALTER TABLE `leads` DISABLE KEYS */;
INSERT INTO `leads` (`id`, `user_id`, `approximate_move_date`, `type_of_service`, `origin_address`, `origin_latitude`, `origin_longitude`, `origin_elevator`, `origin_property_type_id`, `origin_floor`, `destination_address`, `destination_latitude`, `destination_longitude`, `destination_elevator`, `destination_property_type_id`, `destination_floor`, `move_size_id`, `square_footage`, `estimated_boxes`, `total_weight`, `total_cubic_foot`, `lead_status`, `packing_requirement_id`, `comments`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 1, '01-01-2022', 1, 'Newington, New Hampshire 03801', 43.1000, 70.8300, 0, 1, '[1]', 'Newburyport, Massachusetts 01951', 42.7800, 70.8500, 1, 1, '[1,2]', 1, 1500, 10, '160', '18', 1, 2, 'test', 1, '2022-01-18 18:44:53', '2022-01-18 18:44:53'),
	(2, 1, '01-01-2022', 2, 'Hyderabad, Telangana 500072', 17.4800, 78.4200, 0, 1, '[1]', 'Hyderabad, Telangana 500072', 17.4800, 78.4200, 0, 1, '[1,3]', 1, 1500, 10, '160', '18', 1, 1, 'test', 1, '2022-01-18 18:45:53', '2022-01-18 18:45:53'),
	(3, 1, '01-01-2022', 3, 'Hyderabad, Telangana 500072', 17.4800, 78.4200, 0, 1, '[1]', 'Hyderabad, Telangana 500072', 17.4800, 78.4200, 0, 1, '[1]', 1, 1500, 10, '160', '18', 1, 1, 'test', 1, '2022-01-20 09:29:27', '2022-01-20 09:29:27'),
	(4, 2, '2022-02-03', 2, 'test', 0.0000, 0.0000, 0, 1, '["10"]', 'test', 0.0000, 0.0000, 0, 4, '["5"]', 2, 10000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 20:45:45', '2022-02-03 20:45:45'),
	(5, 3, '2022-02-03', 2, 'test', 0.0000, 0.0000, 0, 1, '["7"]', 'test', 0.0000, 0.0000, 0, 4, '["9"]', 2, 10000, 100, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 20:58:27', '2022-02-03 20:58:27'),
	(6, 4, '2022-02-03', 2, 'test', 0.0000, 0.0000, 0, 3, '["8"]', 'test', 0.0000, 0.0000, 0, 5, '["9"]', 2, 1000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 21:10:35', '2022-02-03 21:10:35'),
	(7, 4, '2022-02-03', 2, 'test', 0.0000, 0.0000, 0, 3, '["8"]', 'test', 0.0000, 0.0000, 0, 5, '["9"]', 2, 1000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 21:11:13', '2022-02-03 21:11:13'),
	(8, 4, '2022-02-03', 2, 'test', 0.0000, 0.0000, 0, 3, '["8"]', 'test', 0.0000, 0.0000, 0, 5, '["9"]', 2, 1000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 21:12:09', '2022-02-03 21:12:09'),
	(9, 4, '2022-02-03', 2, 'test', 0.0000, 0.0000, 0, 3, '["8"]', 'test', 0.0000, 0.0000, 0, 5, '["9"]', 2, 1000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 21:13:24', '2022-02-03 21:13:24'),
	(10, 5, '2022-02-03', 2, 'test', 0.0000, 0.0000, 0, 3, '["4"]', 'test', 0.0000, 0.0000, 0, 3, '["9"]', 3, 10000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-03 21:18:54', '2022-02-03 21:18:54'),
	(11, 1, '01-01-2022', 1, 'Hyderabad, Telangana 500072', 37.7700, -122.4500, 0, 1, '[1]', 'Hyderabad, Telangana 500072', 37.7700, -122.5100, 0, 1, '[1]', 1, 1500, 10, '160', '18', 1, 1, 'test', 1, '2022-02-03 21:32:24', '2022-02-19 16:39:51'),
	(12, 7, '2022-02-18', 2, 'califorinia', 0.0000, 0.0000, 0, 1, '["3"]', 'test', 0.0000, 0.0000, 0, 6, '["4"]', 2, 10000, 10, NULL, NULL, 1, 1, NULL, 1, '2022-02-18 11:51:20', '2022-02-18 11:51:20'),
	(13, 9, '2022-02-23', 2, 'Hyatt Place New York City / Times Square, West 39th Street, New York, NY, USA', 40.7600, -73.9900, 0, 1, '["1"]', 'Times Square, Manhattan, NY, USA', 40.7600, -73.9900, 0, 3, '["2"]', 2, 10000, 212, NULL, NULL, 1, 1, NULL, 1, '2022-02-23 16:48:26', '2022-02-23 16:48:26'),
	(14, 10, '2022-02-24', 4, 'TESSA, Amsterdam Avenue, New York, NY, USA', 40.7814, -73.9792, 0, 1, '["1"]', 'Hyatt Place New York / Chelsea, West 24th Street, New York, NY, USA', 40.7441, -73.9941, 0, 3, '["2"]', 2, 10000, 122, NULL, NULL, 1, 1, NULL, 1, '2022-02-23 16:56:20', '2022-02-23 16:56:20');
/*!40000 ALTER TABLE `leads` ENABLE KEYS */;

-- Dumping structure for table moveit.leads_follow_up
CREATE TABLE IF NOT EXISTS `leads_follow_up` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `days_until_move` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `follow_up_hours` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.leads_follow_up: ~0 rows (approximately)
/*!40000 ALTER TABLE `leads_follow_up` DISABLE KEYS */;
/*!40000 ALTER TABLE `leads_follow_up` ENABLE KEYS */;

-- Dumping structure for table moveit.leads_items
CREATE TABLE IF NOT EXISTS `leads_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `lead_id` bigint(20) unsigned NOT NULL,
  `item_id` bigint(20) unsigned NOT NULL,
  `floor` int(11) NOT NULL DEFAULT 0,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cubic_foot` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.leads_items: ~8 rows (approximately)
/*!40000 ALTER TABLE `leads_items` DISABLE KEYS */;
INSERT INTO `leads_items` (`id`, `user_id`, `lead_id`, `item_id`, `floor`, `quantity`, `price`, `weight`, `cubic_foot`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 2, 1, 10.00, '50', '20', '2022-01-18 18:44:54', '2022-01-18 18:44:54'),
	(2, 1, 1, 2, 1, 1, 10.00, '60', '50', '2022-01-18 18:44:54', '2022-01-18 18:44:54'),
	(3, 1, 2, 1, 1, 1, 10.00, '50', '20', '2022-01-18 18:45:53', '2022-01-18 18:45:53'),
	(4, 1, 2, 2, 1, 1, 10.00, '60', '50', '2022-01-18 18:45:54', '2022-01-18 18:45:54'),
	(5, 1, 3, 1, 1, 1, 10.00, '50', '20', '2022-01-20 09:29:27', '2022-01-20 09:29:27'),
	(6, 1, 3, 2, 1, 1, 10.00, '60', '50', '2022-01-20 09:29:27', '2022-01-20 09:29:27'),
	(7, 1, 11, 1, 1, 1, 10.00, '50', '20', '2022-02-19 16:39:51', '2022-02-19 16:39:51'),
	(8, 1, 11, 2, 1, 1, 10.00, '60', '50', '2022-02-19 16:39:51', '2022-02-19 16:39:51');
/*!40000 ALTER TABLE `leads_items` ENABLE KEYS */;

-- Dumping structure for table moveit.leads_materials
CREATE TABLE IF NOT EXISTS `leads_materials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `lead_id` bigint(20) unsigned NOT NULL,
  `material_id` bigint(20) unsigned NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `pack_quantity` int(11) NOT NULL DEFAULT 1,
  `unpack_quantity` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.leads_materials: ~2 rows (approximately)
/*!40000 ALTER TABLE `leads_materials` DISABLE KEYS */;
INSERT INTO `leads_materials` (`id`, `user_id`, `lead_id`, `material_id`, `quantity`, `price`, `pack_quantity`, `unpack_quantity`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 1, 89.59, 1, 1, '2022-01-18 18:44:54', '2022-01-18 18:44:54'),
	(2, 1, 2, 1, 1, 9000.00, 1, 1, '2022-01-18 18:45:54', '2022-01-18 18:45:54'),
	(3, 1, 3, 1, 1, 2000.00, 1, 1, '2022-01-20 09:29:27', '2022-01-20 09:29:27'),
	(4, 1, 11, 1, 1, 10.00, 1, 1, '2022-02-19 16:39:51', '2022-02-19 16:39:51');
/*!40000 ALTER TABLE `leads_materials` ENABLE KEYS */;

-- Dumping structure for table moveit.leads_services
CREATE TABLE IF NOT EXISTS `leads_services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `lead_id` bigint(20) unsigned NOT NULL,
  `service_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.leads_services: ~21 rows (approximately)
/*!40000 ALTER TABLE `leads_services` DISABLE KEYS */;
INSERT INTO `leads_services` (`id`, `user_id`, `lead_id`, `service_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, '2022-01-18 18:44:55', '2022-01-18 18:44:55'),
	(2, 1, 1, 2, '2022-01-18 18:44:55', '2022-01-18 18:44:55'),
	(3, 1, 2, 1, '2022-01-18 18:45:55', '2022-01-18 18:45:55'),
	(4, 1, 2, 2, '2022-01-18 18:45:55', '2022-01-18 18:45:55'),
	(5, 1, 3, 1, '2022-01-20 09:29:27', '2022-01-20 09:29:27'),
	(6, 1, 3, 2, '2022-01-20 09:29:27', '2022-01-20 09:29:27'),
	(7, 4, 8, 1, '2022-02-03 21:12:09', '2022-02-03 21:12:09'),
	(8, 4, 8, 2, '2022-02-03 21:12:09', '2022-02-03 21:12:09'),
	(9, 4, 9, 1, '2022-02-03 21:13:24', '2022-02-03 21:13:24'),
	(10, 4, 9, 2, '2022-02-03 21:13:24', '2022-02-03 21:13:24'),
	(11, 5, 10, 1, '2022-02-03 21:18:54', '2022-02-03 21:18:54'),
	(12, 5, 10, 2, '2022-02-03 21:18:54', '2022-02-03 21:18:54'),
	(13, 6, 11, 1, '2022-02-03 21:32:24', '2022-02-03 21:32:24'),
	(14, 6, 11, 2, '2022-02-03 21:32:24', '2022-02-03 21:32:24'),
	(15, 7, 12, 1, '2022-02-18 11:51:20', '2022-02-18 11:51:20'),
	(16, 7, 12, 2, '2022-02-18 11:51:20', '2022-02-18 11:51:20'),
	(17, 1, 11, 1, '2022-02-19 16:39:52', '2022-02-19 16:39:52'),
	(18, 1, 11, 2, '2022-02-19 16:39:52', '2022-02-19 16:39:52'),
	(19, 9, 13, 1, '2022-02-23 16:48:26', '2022-02-23 16:48:26'),
	(20, 9, 13, 2, '2022-02-23 16:48:26', '2022-02-23 16:48:26'),
	(21, 10, 14, 1, '2022-02-23 16:56:20', '2022-02-23 16:56:20'),
	(22, 10, 14, 2, '2022-02-23 16:56:20', '2022-02-23 16:56:20');
/*!40000 ALTER TABLE `leads_services` ENABLE KEYS */;

-- Dumping structure for table moveit.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT NULL,
  `created_on` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `log_type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.log: ~67 rows (approximately)
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` (`id`, `module_id`, `created_on`, `user_id`, `action`, `category`, `description`, `log_type`, `created_at`, `updated_at`) VALUES
	(1, 1, '2022-01-18 16:45:25', 1, 'update', 1, 'User Super Admin is updated', 1, '2022-01-18 16:45:25', '2022-01-18 16:45:25'),
	(2, 3, '2022-02-11 13:09:11', 1, 'create', 1, 'Category  Created Successfully!', 1, '2022-02-11 13:09:11', '2022-02-11 13:09:11'),
	(3, 3, '2022-02-11 14:40:44', 1, 'update', 1, 'Category test123 Updated Successfully!', 1, '2022-02-11 14:40:44', '2022-02-11 14:40:44'),
	(4, 3, '2022-02-11 14:40:52', 1, 'delete', 1, 'Category test123 Deleted Successfully!', 1, '2022-02-11 14:40:52', '2022-02-11 14:40:52'),
	(5, 9, '2022-02-11 23:29:45', 1, 'update', 1, 'Category test test Updated Successfully!', 1, '2022-02-11 23:29:45', '2022-02-11 23:29:45'),
	(6, 9, '2022-02-11 23:51:06', 1, 'create', 1, 'Category  Created Successfully!', 1, '2022-02-11 23:51:06', '2022-02-11 23:51:06'),
	(7, 9, '2022-02-19 18:43:03', 1, 'create', 1, 'Category  Created Successfully!', 1, '2022-02-19 18:43:03', '2022-02-19 18:43:03'),
	(8, 10, '2022-02-19 18:45:46', 1, 'create', 1, 'Packing Materials test is created', 1, '2022-02-19 18:45:46', '2022-02-19 18:45:46'),
	(9, 10, '2022-02-19 18:52:36', 1, 'update', 1, 'Packing Materials test is updated', 1, '2022-02-19 18:52:36', '2022-02-19 18:52:36'),
	(10, 10, '2022-03-02 15:52:10', 1, 'update', 1, 'Packing Materials PACKING MATERIALS is updated', 1, '2022-03-02 15:52:10', '2022-03-02 15:52:10'),
	(11, 11, '2022-03-02 16:30:46', 1, 'create', 1, 'Room test Created Successfully!', 1, '2022-03-02 16:30:46', '2022-03-02 16:30:46'),
	(12, 11, '2022-03-02 16:36:12', 1, 'update', 1, 'Room test Updated Successfully!', 1, '2022-03-02 16:36:12', '2022-03-02 16:36:12'),
	(13, 11, '2022-03-02 16:36:26', 1, 'delete', 1, 'Room test Deleted Successfully!', 1, '2022-03-02 16:36:27', '2022-03-02 16:36:27'),
	(14, 12, '2022-03-04 16:27:05', 1, 'create', 1, 'Inventory Item test Created Successfully!', 1, '2022-03-04 16:27:05', '2022-03-04 16:27:05'),
	(15, 12, '2022-03-04 16:40:17', 1, 'create', 1, 'Inventory Item testrwer Created Successfully!', 1, '2022-03-04 16:40:18', '2022-03-04 16:40:18'),
	(16, 12, '2022-03-04 18:24:41', 1, 'update', 1, 'Inventory Item testrwer Updated Successfully!', 1, '2022-03-04 18:24:41', '2022-03-04 18:24:41'),
	(17, 12, '2022-03-04 18:25:20', 1, 'delete', 1, 'Inventory Item testrwer Deleted Successfully!', 1, '2022-03-04 18:25:20', '2022-03-04 18:25:20'),
	(18, 12, '2022-03-06 10:09:20', 1, 'create', 1, 'Inventory Item test Created Successfully!', 1, '2022-03-06 10:09:21', '2022-03-06 10:09:21'),
	(19, 12, '2022-03-06 10:10:02', 1, 'update', 1, 'Inventory Item test Updated Successfully!', 1, '2022-03-06 10:10:02', '2022-03-06 10:10:02'),
	(20, 13, '2022-03-08 18:47:08', 1, 'create', 1, 'Room test Created Successfully!', 1, '2022-03-08 18:47:08', '2022-03-08 18:47:08'),
	(21, 13, '2022-03-08 18:51:36', 1, 'update', 1, 'Room test Updated Successfully!', 1, '2022-03-08 18:51:36', '2022-03-08 18:51:36'),
	(22, 13, '2022-03-08 18:51:53', 1, 'delete', 1, 'Additional Charges test Deleted Successfully!', 1, '2022-03-08 18:51:53', '2022-03-08 18:51:53'),
	(23, 14, '2022-03-09 16:00:50', 1, 'create', 1, 'Follow Up Automation 0-5 Created Successfully!', 1, '2022-03-09 16:00:50', '2022-03-09 16:00:50'),
	(24, 14, '2022-03-09 17:01:31', 1, 'update', 1, 'Follow Up Automation 0-5 Updated Successfully!', 1, '2022-03-09 17:01:31', '2022-03-09 17:01:31'),
	(25, 14, '2022-03-09 17:01:36', 1, 'delete', 1, 'Follow Up Automation 0-5 Deleted Successfully!', 1, '2022-03-09 17:01:36', '2022-03-09 17:01:36'),
	(26, 16, '2022-03-11 16:14:38', 1, 'create', 1, 'Travel Time - Min 0Max1000 Created Successfully!', 1, '2022-03-11 16:14:39', '2022-03-11 16:14:39'),
	(27, 16, '2022-03-11 16:24:27', 1, 'update', 1, 'Travel Time - Min 0Max1000 Updated Successfully!', 1, '2022-03-11 16:24:27', '2022-03-11 16:24:27'),
	(28, 16, '2022-03-11 16:25:23', 1, 'update', 1, 'Travel Time - Min 0 - Max1000 Updated Successfully!', 1, '2022-03-11 16:25:23', '2022-03-11 16:25:23'),
	(29, 16, '2022-03-11 16:25:49', 1, 'delete', 1, 'Travel Time - Min 0Max1000 Deleted Successfully!', 1, '2022-03-11 16:25:49', '2022-03-11 16:25:49'),
	(30, 17, '2022-03-14 13:06:37', 1, 'create', 1, 'Travel Time - Min 4 - Max 4 Created Successfully!', 1, '2022-03-14 13:06:37', '2022-03-14 13:06:37'),
	(31, 17, '2022-03-14 13:14:47', 1, 'update', 1, 'Fuel Charges - Min 41 - Max 42 Updated Successfully!', 1, '2022-03-14 13:14:47', '2022-03-14 13:14:47'),
	(32, 17, '2022-03-14 13:15:12', 1, 'delete', 1, 'Fuel Charges - Min  - Max  Deleted Successfully!', 1, '2022-03-14 13:15:12', '2022-03-14 13:15:12'),
	(33, 18, '2022-03-14 15:20:38', 1, 'create', 1, 'Fuel Charges - Min 3 - Max 2 Created Successfully!', 1, '2022-03-14 15:20:38', '2022-03-14 15:20:38'),
	(34, 18, '2022-03-14 15:24:22', 1, 'update', 1, 'Square Footage - Min 31 - Max 21 Updated Successfully!', 1, '2022-03-14 15:24:22', '2022-03-14 15:24:22'),
	(35, 18, '2022-03-14 15:25:10', 1, 'delete', 1, 'Square Footage - Min 31 - Max 21 Deleted Successfully!', 1, '2022-03-14 15:25:11', '2022-03-14 15:25:11'),
	(36, 19, '2022-03-16 15:12:23', 1, 'create', 1, 'Floor 1 Created Successfully!', 1, '2022-03-16 15:12:24', '2022-03-16 15:12:24'),
	(37, 19, '2022-03-16 15:16:26', 1, 'update', 1, 'Floor 1 Updated Successfully!', 1, '2022-03-16 15:16:26', '2022-03-16 15:16:26'),
	(38, 19, '2022-03-16 15:16:30', 1, 'delete', 1, 'Floor 1 Deleted Successfully!', 1, '2022-03-16 15:16:31', '2022-03-16 15:16:31'),
	(39, 20, '2022-03-16 15:52:15', 1, 'create', 1, 'Estimate Range From 0 To 1 Created Successfully!', 1, '2022-03-16 15:52:15', '2022-03-16 15:52:15'),
	(40, 20, '2022-03-16 15:58:17', 1, 'update', 1, 'Estimate Range From 0 To 1 Updated Successfully!', 1, '2022-03-16 15:58:17', '2022-03-16 15:58:17'),
	(41, 20, '2022-03-16 15:59:03', 1, 'delete', 1, 'Estimate Range From 0 To 1 Deleted Successfully!', 1, '2022-03-16 15:59:03', '2022-03-16 15:59:03'),
	(42, 20, '2022-03-16 18:19:28', 1, 'create', 1, 'Distance From Truck Less than 50 Feet Created Successfully!', 1, '2022-03-16 18:19:28', '2022-03-16 18:19:28'),
	(43, 21, '2022-03-16 18:27:14', 1, 'update', 1, 'Distance From Truck Less than 50 Feet Updated Successfully!', 1, '2022-03-16 18:27:14', '2022-03-16 18:27:14'),
	(44, 21, '2022-03-16 18:27:51', 1, 'delete', 1, 'Distance from truck Less than 50 Feet Deleted Successfully!', 1, '2022-03-16 18:27:51', '2022-03-16 18:27:51'),
	(45, 22, '2022-03-16 20:09:28', 1, 'create', 1, 'Man Hour Calculation 0-7 Created Successfully!', 1, '2022-03-16 20:09:28', '2022-03-16 20:09:28'),
	(46, 22, '2022-03-16 20:15:13', 1, 'update', 1, 'Man Hour Calculation 0-7 Updated Successfully!', 1, '2022-03-16 20:15:13', '2022-03-16 20:15:13'),
	(47, 22, '2022-03-16 20:17:00', 1, 'delete', 1, 'Man Hour Calculation 0-7 Deleted Successfully!', 1, '2022-03-16 20:17:00', '2022-03-16 20:17:00'),
	(48, 23, '2022-03-20 09:16:16', 1, 'create', 1, 'PayRolls test Created Successfully!', 1, '2022-03-20 09:16:17', '2022-03-20 09:16:17'),
	(49, 23, '2022-03-20 09:17:46', 1, 'create', 1, 'PayRolls test Created Successfully!', 1, '2022-03-20 09:17:46', '2022-03-20 09:17:46'),
	(50, 23, '2022-03-20 09:19:50', 1, 'create', 1, 'PayRolls test Created Successfully!', 1, '2022-03-20 09:19:50', '2022-03-20 09:19:50'),
	(51, 23, '2022-03-20 11:12:47', 1, 'update', 1, 'Pay Rolls testerwer Updated Successfully!', 1, '2022-03-20 11:12:47', '2022-03-20 11:12:47'),
	(52, 23, '2022-03-20 11:13:36', 1, 'update', 1, 'Pay Rolls testerwer Updated Successfully!', 1, '2022-03-20 11:13:36', '2022-03-20 11:13:36'),
	(53, 23, '2022-03-20 11:14:21', 1, 'delete', 1, 'Pay Rolls testerwer Deleted Successfully!', 1, '2022-03-20 11:14:22', '2022-03-20 11:14:22'),
	(54, 23, '2022-03-21 21:55:39', 1, 'create', 1, 'PayRolls  Created Successfully!', 1, '2022-03-21 21:55:39', '2022-03-21 21:55:39'),
	(55, 23, '2022-03-22 06:20:43', 1, 'create', 1, 'PayRolls test Created Successfully!', 1, '2022-03-22 06:20:44', '2022-03-22 06:20:44'),
	(56, 23, '2022-03-22 06:21:05', 1, 'update', 1, 'Pay Rolls test Updated Successfully!', 1, '2022-03-22 06:21:05', '2022-03-22 06:21:05'),
	(57, 24, '2022-03-22 11:30:43', 1, 'create', 1, 'Facilities test Created Successfully!', 1, '2022-03-22 11:30:43', '2022-03-22 11:30:43'),
	(58, 24, '2022-03-22 11:35:44', 1, 'create', 1, 'Facilities test Created Successfully!', 1, '2022-03-22 11:35:44', '2022-03-22 11:35:44'),
	(59, 24, '2022-03-22 12:55:58', 1, 'update', 1, 'Facilities test Updated Successfully!', 1, '2022-03-22 12:55:58', '2022-03-22 12:55:58'),
	(60, 24, '2022-03-22 12:56:07', 1, 'update', 1, 'Facilities testsadsad Updated Successfully!', 1, '2022-03-22 12:56:07', '2022-03-22 12:56:07'),
	(61, 24, '2022-03-22 12:56:57', 1, 'delete', 1, 'Facilities testsadsad Deleted Successfully!', 1, '2022-03-22 12:56:57', '2022-03-22 12:56:57'),
	(62, 24, '2022-03-22 13:03:54', 1, 'create', 1, 'Facilities test Created Successfully!', 1, '2022-03-22 13:03:54', '2022-03-22 13:03:54'),
	(63, 24, '2022-03-22 14:51:41', 1, 'create', 1, 'Units test Created Successfully!', 1, '2022-03-22 14:51:41', '2022-03-22 14:51:41'),
	(64, 24, '2022-03-22 14:53:57', 1, 'create', 1, 'Units tssdf Created Successfully!', 1, '2022-03-22 14:53:57', '2022-03-22 14:53:57'),
	(65, 24, '2022-03-22 14:56:57', 1, 'create', 1, 'Units test Created Successfully!', 1, '2022-03-22 14:56:57', '2022-03-22 14:56:57'),
	(66, 24, '2022-03-22 15:09:23', 1, 'create', 1, 'Units 10x10 Created Successfully!', 1, '2022-03-22 15:09:23', '2022-03-22 15:09:23'),
	(67, 24, '2022-03-22 15:21:46', 1, 'update', 1, 'Units 10x10 Updated Successfully!', 1, '2022-03-22 15:21:46', '2022-03-22 15:21:46'),
	(68, 24, '2022-03-22 15:26:44', 1, 'update', 1, 'Units 10x10 Updated Successfully!', 1, '2022-03-22 15:26:44', '2022-03-22 15:26:44');
/*!40000 ALTER TABLE `log` ENABLE KEYS */;

-- Dumping structure for table moveit.man_hour_calculation
CREATE TABLE IF NOT EXISTS `man_hour_calculation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `men_hours` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `men` int(11) NOT NULL DEFAULT 0,
  `stairs_men` int(11) NOT NULL DEFAULT 0,
  `elevator_men` int(11) NOT NULL DEFAULT 0,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.man_hour_calculation: ~0 rows (approximately)
/*!40000 ALTER TABLE `man_hour_calculation` DISABLE KEYS */;
/*!40000 ALTER TABLE `man_hour_calculation` ENABLE KEYS */;

-- Dumping structure for table moveit.materials
CREATE TABLE IF NOT EXISTS `materials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.materials: ~2 rows (approximately)
/*!40000 ALTER TABLE `materials` DISABLE KEYS */;
INSERT INTO `materials` (`id`, `name`, `description`, `price`, `image`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Snow Blower', 'Snow Blower', 2000.00, NULL, 1, '2022-04-10 16:59:54', '2022-04-10 16:59:54'),
	(2, 'Armoire/Ward', 'Armoire/Ward', 1000.00, NULL, 1, '2022-04-10 16:59:54', '2022-04-10 16:59:54');
/*!40000 ALTER TABLE `materials` ENABLE KEYS */;

-- Dumping structure for table moveit.media_content
CREATE TABLE IF NOT EXISTS `media_content` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `media_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_extension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.media_content: ~0 rows (approximately)
/*!40000 ALTER TABLE `media_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_content` ENABLE KEYS */;

-- Dumping structure for table moveit.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.migrations: ~87 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(6, '2016_06_01_000004_create_oauth_clients_table', 1),
	(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
	(8, '2019_08_19_000000_create_failed_jobs_table', 1),
	(9, '2021_04_06_015221_create_admin_users_table', 1),
	(10, '2021_04_06_020606_create_defaults_table', 1),
	(11, '2021_04_06_021009_create_faqs_table', 1),
	(12, '2021_04_06_021216_create_home_banners_table', 1),
	(13, '2021_04_06_231416_create_role_table', 1),
	(14, '2021_04_06_234547_create_rolemoduleprivileges_table', 1),
	(15, '2021_04_06_234731_create_privileges_table', 1),
	(16, '2021_04_06_235732_create_module_table', 1),
	(17, '2021_04_06_235829_create_log_table', 1),
	(18, '2021_04_08_220617_create_testimonials_table', 1),
	(19, '2021_04_16_230204_create_media_content_table', 1),
	(20, '2021_04_18_063651_create_user_basic_details_table', 1),
	(21, '2021_04_30_222106_create_config_types_table', 1),
	(22, '2021_04_30_222222_create_config_labels_table', 1),
	(23, '2022_01_15_132647_create_services_table', 1),
	(24, '2022_01_15_141012_create_categories_table', 1),
	(25, '2022_01_15_141355_create_items_table', 1),
	(26, '2022_01_15_141802_create_items_images_table', 1),
	(27, '2022_01_15_142144_create_materials_table', 1),
	(28, '2022_01_15_142323_create_move_sizes_table', 1),
	(29, '2022_01_15_142415_create_phone_types_table', 1),
	(30, '2022_01_15_142505_create_properties_table', 1),
	(31, '2022_01_15_142608_create_referrals_table', 1),
	(32, '2022_01_15_142702_create_service_types_table', 1),
	(33, '2022_01_15_142821_create_leads_table', 1),
	(34, '2022_01_15_143821_create_leads_items_table', 1),
	(35, '2022_01_15_144232_create_leads_materials_table', 1),
	(36, '2022_01_15_144419_create_leads_services_table', 1),
	(37, '2022_01_15_205041_create_move_status_table', 1),
	(38, '2022_01_26_133300_create_packing_requirements_table', 2),
	(39, '2022_01_26_160727_create_storage_requirements_table', 3),
	(40, '2022_01_29_074845_create_packing_services_table', 4),
	(41, '2022_01_29_075206_create_packing_quotes_table', 4),
	(42, '2022_01_29_075353_create_packing_materials_table', 4),
	(43, '2022_01_29_080509_create_pricing_elements_table', 5),
	(44, '2022_03_04_163116_add_inventory_to_items_table', 6),
	(45, '2022_03_08_163911_create_additional_charges_table', 7),
	(46, '2022_03_08_004820_create_prospect_interest_statuses_table', 8),
	(47, '2022_03_08_011048_create_referred_by_sources_table', 8),
	(48, '2022_03_08_012627_create_expertise_types_table', 8),
	(49, '2022_03_08_014120_create_client_tags_table', 8),
	(50, '2022_03_08_232412_create_fleettypes_table', 8),
	(51, '2022_03_09_152559_create_follow_up_automation_table', 9),
	(52, '2022_03_09_175808_create_leads_follow_up_table', 10),
	(53, '2022_03_10_161352_create_travel_time_table', 11),
	(54, '2022_03_14_123112_create_fuel_charges_table', 12),
	(55, '2022_03_14_145713_create_square_footage_table', 13),
	(56, '2022_03_16_124141_create_floors_table', 14),
	(57, '2022_03_16_125731_create_tasks_table', 15),
	(58, '2022_03_16_130319_create_task_status_table', 15),
	(59, '2022_03_16_130425_create_task_types_table', 15),
	(60, '2022_03_16_152441_create_estimate_range_table', 15),
	(61, '2022_03_16_160604_create_distance_from_truck_table', 16),
	(62, '2022_03_16_160637_create_man_hour_calculation_table', 16),
	(63, '2022_03_20_073719_create_pay_roll_classes_table', 17),
	(64, '2022_03_20_075838_create_pay_rolls_table', 18),
	(65, '2022_03_20_141010_create_pay_roll_configurations_table', 19),
	(66, '2022_03_20_143600_create_visible_payroll_fields_table', 19),
	(67, '2022_03_20_150715_create_pay_roll_miles_table', 20),
	(68, '2022_03_22_063615_create_states_table', 21),
	(69, '2022_03_22_063900_create_facilities_table', 21),
	(70, '2022_03_22_131339_create_units_table', 22),
	(71, '2022_03_22_131904_create_facility_units_table', 23),
	(72, '2022_03_27_141251_create_addendums_table', 24),
	(73, '2022_03_27_212945_create_verbiage_table', 24),
	(74, '2022_03_28_162724_create_boltext_table', 24),
	(75, '2022_03_30_161406_create_terms_and_conditions_table', 25),
	(76, '2022_04_01_095422_create_notice_and_policies_table', 25),
	(77, '2022_04_01_110102_create_invoice_notes_table', 25),
	(78, '2022_04_01_113648_create_invoicev2_maxprice_table', 25),
	(79, '2022_04_04_171116_create_fleets_table', 26),
	(80, '2022_04_07_185124_create_years_table', 27),
	(81, '2022_03_16_215423_create_employees_phone_numbers_table', 28),
	(82, '2022_03_16_220527_create_employees_disciplinary_notes_table', 28),
	(83, '2022_03_19_124513_create_states_table', 29),
	(84, '2022_03_19_124549_create_countries_table', 29),
	(85, '2022_03_19_125810_create_timezones_table', 29),
	(86, '2022_03_20_005218_create_employees_addresses_table', 29),
	(87, '2022_03_23_215550_create_employees_email_ids_table', 29),
	(88, '2022_03_25_215946_create_employees_client_reviews_table', 29),
	(89, '2022_03_26_001142_create_employees_table', 29),
	(90, '2022_04_10_164751_create_vehicle_status_table', 30),
	(91, '2022_04_10_164912_create_owner_ships_table', 30);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table moveit.module
CREATE TABLE IF NOT EXISTS `module` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.module: ~24 rows (approximately)
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` (`id`, `module_name`, `created_at`, `updated_at`) VALUES
	(1, 'Admin Users', '2022-04-10 17:00:25', '2022-04-10 17:00:25'),
	(2, 'Banners', '2022-04-10 17:00:25', '2022-04-10 17:00:25'),
	(3, 'Categories', '2022-04-10 17:00:25', '2022-04-10 17:00:25'),
	(4, 'Faqs', '2022-04-10 17:00:25', '2022-04-10 17:00:25'),
	(5, 'Testimonials', '2022-04-10 17:00:26', '2022-04-10 17:00:26'),
	(6, 'Roles', '2022-04-10 17:00:26', '2022-04-10 17:00:26'),
	(7, 'Leads', '2022-04-10 17:00:26', '2022-04-10 17:00:26'),
	(8, 'Schedules', '2022-04-10 17:00:26', '2022-04-10 17:00:26'),
	(9, 'Categories', '2022-04-10 17:00:26', '2022-04-10 17:00:26'),
	(10, 'Packing Materials', '2022-04-10 17:00:27', '2022-04-10 17:00:27'),
	(11, 'Rooms', '2022-04-10 17:00:27', '2022-04-10 17:00:27'),
	(12, 'Inventory Items', '2022-04-10 17:00:27', '2022-04-10 17:00:27'),
	(13, 'Additional Charges', '2022-04-10 17:00:27', '2022-04-10 17:00:27'),
	(14, 'Follow Up Management', '2022-04-10 17:00:27', '2022-04-10 17:00:27'),
	(15, 'Leads Follow Up', '2022-04-10 17:00:27', '2022-04-10 17:00:27'),
	(16, 'Travel Time', '2022-04-10 17:00:27', '2022-04-10 17:00:27'),
	(17, 'Fuel Charges', '2022-04-10 17:00:28', '2022-04-10 17:00:28'),
	(18, 'Square Footage', '2022-04-10 17:00:28', '2022-04-10 17:00:28'),
	(19, 'Floors', '2022-04-10 17:00:29', '2022-04-10 17:00:29'),
	(20, 'Estimate Range', '2022-04-10 17:00:29', '2022-04-10 17:00:29'),
	(21, 'Distance From Truck', '2022-04-10 17:00:29', '2022-04-10 17:00:29'),
	(22, 'Man Hour Calculation', '2022-04-10 17:00:29', '2022-04-10 17:00:29'),
	(23, 'Pay roll', '2022-04-10 17:00:29', '2022-04-10 17:00:29'),
	(24, 'Facilities', '2022-04-10 17:00:30', '2022-04-10 17:00:30');
/*!40000 ALTER TABLE `module` ENABLE KEYS */;

-- Dumping structure for table moveit.move_sizes
CREATE TABLE IF NOT EXISTS `move_sizes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.move_sizes: ~7 rows (approximately)
/*!40000 ALTER TABLE `move_sizes` DISABLE KEYS */;
INSERT INTO `move_sizes` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Studio', 1, '2022-04-10 16:59:58', '2022-04-10 16:59:58'),
	(2, '1 Bedroom', 1, '2022-04-10 16:59:58', '2022-04-10 16:59:58'),
	(3, '2 Bedroom', 1, '2022-04-10 16:59:59', '2022-04-10 16:59:59'),
	(4, '3 Bedroom', 1, '2022-04-10 16:59:59', '2022-04-10 16:59:59'),
	(5, '4 Bedroom', 1, '2022-04-10 17:00:00', '2022-04-10 17:00:00'),
	(6, '5+ Bedroom', 1, '2022-04-10 17:00:00', '2022-04-10 17:00:00'),
	(7, 'Office', 1, '2022-04-10 17:00:01', '2022-04-10 17:00:01');
/*!40000 ALTER TABLE `move_sizes` ENABLE KEYS */;

-- Dumping structure for table moveit.move_status
CREATE TABLE IF NOT EXISTS `move_status` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.move_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `move_status` DISABLE KEYS */;
INSERT INTO `move_status` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'New Lead', 1, '2022-04-10 17:00:22', '2022-04-10 17:00:22');
/*!40000 ALTER TABLE `move_status` ENABLE KEYS */;

-- Dumping structure for table moveit.notice_and_policies
CREATE TABLE IF NOT EXISTS `notice_and_policies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `heading` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.notice_and_policies: ~11 rows (approximately)
/*!40000 ALTER TABLE `notice_and_policies` DISABLE KEYS */;
INSERT INTO `notice_and_policies` (`id`, `heading`, `content`, `created_at`, `updated_at`) VALUES
	(1, 'IMPORTANT NOTICE', 'CUSTOMER ACCEPTS ANY AND ALL LIABILITY OF WATER DAMAGE. BIG HILL MOVERS WILL NOT DISCONNECT OR RECONNECT ANY WATER LINES. Unless otherwise specified, customer expressly releases the shipment to a declared value of $0.60 per pound per article, and shall continue during and all services rendered. Carriers maximum liability for loss and damage shall be either the lump sum value declared by the customer below, or $0.60 per pound per article. There is a $250.00 deductible to the customer under the terms of this valuation. Rates for increase valuation are available upon request. The company is not liable for damage to items packed by others or for items which require packing for safe handling.ie mirrors, marble, pictures, glass tops, lamps, or lamp shades, box springs, and mattresses, and all other unpacked fragile items, as well as all breakables packed in a carton by customer. Furthermore, all functions of electrical and mechanical appliances to include, but not limited to, computers, refrigerators, televisions, stereos, washing machines, dryers, etc. Unless there is visible outward damage. In no case is the carrier liable due to the inherent vice nature of any article being moved. I also agree to all Terms and Conditions on the back of this contract.', NULL, '2022-04-01 15:06:39'),
	(2, 'DIFFICULT ACCESS EXCEPTIONS', 'I understand that the movers in attempting to get a piece of furniture into an area that said piece has difficulty entering or exiting from is not insured. This said piece or pieces and the property damage that may incur is not insured in this event. I hereby knowingly accept all responsibility to any and all damages that may incur to existing property such as walls, bannisters, doorways, light fixtures, fans, sprinklers, etc.,and all floor coverings,i.e. rugs, linoleum, wooden floors, laminate,tile, etc. In this instance, any kind of damage to the furniture or property is not covered or the responsibility of the mover.', NULL, '2022-04-01 15:06:51'),
	(3, 'TERMS', 'PAYMENT IS REQUIRED PRIOR TO UNLOADING OF GOODS FROM OUR VEHICLES . LOCAL MOVES WITHIN MASSACHUSETTS MAY PAY CASH, CASHIERS CHECK, A VALID PERSONAL CHECK FROM A LOCAL BANK SHOWING UPON ITS FACE THE NAME AND ADDRESS OF THE SHIPPER OR AUTHORIZED REPRESENTIVE, OR VIA A CREDIT CARD . If PAYMENT IS MADE VIA CREDIT OR DEBIT CARD A 5% CHARGE MAY BE ADDED. Should collection procedures be necessary to collect this bill, the customer agrees to pay and all costs of collection including reasonable attorney fees. A $35.00 service charge will be assessed on all checks returned by the bank as unpaid.', NULL, '2022-04-01 15:07:10'),
	(4, 'DELIVERY ACKNOWLEDGEMENT', 'ACKNOWLEDGMENT IS MADE OF PERFORMANCE OF SERVICES OUTLINED ABOVE IN A SATISFACTORY MANNER, AND ALL GOODS RECEIVED SATISFACTORY EXCEPT AS NOTED ABOVE.', NULL, '2022-04-01 15:07:24'),
	(5, 'SHIPMENT TEXT', NULL, NULL, NULL),
	(6, 'NOTICE', 'The customer signing this contract must insert in the space above in his own handwriting his declaration of the actual value of the shipment , otherwise the shipment will be deemed released to a maximum value equal to $0.60 per pound per article.', NULL, '2022-04-01 15:07:36'),
	(7, 'STORAGE ACCOUNTS ACCESS', NULL, NULL, NULL),
	(8, 'DEFAULT VALUATION AMOUNT', '$0.60 per pound per article', NULL, NULL),
	(9, 'GUARANTEED PRICE TEXT FOR INVOICE', 'This ESTIMATED PRICE is based on the info you provided, and that your locations are packed and ready to move. If additional packing, additional items, or variables happen the price will be adjusted accordingly. PLEASE KEEP IN MIND THIS IS AN ESTIMATE.', NULL, NULL),
	(10, 'TITLE FOR GUARANTEED QUOTE', 'CONTRACT FOR SERVICE - BOL', NULL, NULL),
	(11, 'TITLE FOR HOURLY QUOTE', 'CONTRACT FOR SERVICE - BOL', NULL, '2022-04-01 15:07:58');
/*!40000 ALTER TABLE `notice_and_policies` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_access_tokens: ~2 rows (approximately)
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
	('00e42831ea83bf0f7bdb16a80f2048a09adc3dd555e8a8ac08dc0c0a6956b8b9ba13cf7e0a69e828', 1, 1, 'AppName', '[]', 0, '2022-01-19 11:54:04', '2022-01-19 11:54:04', '2023-01-19 11:54:04'),
	('680af915bc021e4da4b74ef114e1179cd2cf2cde6ad208bf395fcf5ef705183ad49ad5e34b9ebc6f', 1, 1, 'AppName', '[]', 0, '2022-02-08 17:02:53', '2022-02-08 17:02:53', '2023-02-08 17:02:53'),
	('b31ade19535ec60e1f9011ce4cd18244d025632b1c3e72a352a0970d3c7165ec5a8f80a67dde7c69', 1, 1, 'AppName', '[]', 0, '2022-02-02 16:17:56', '2022-02-02 16:17:56', '2023-02-02 16:17:56'),
	('be0f72b3cad42deda0fc9bdbb664be6e8015bd4f8fde857e2f48326f764d5d263aacbc907ab66bda', 1, 1, 'AppName', '[]', 0, '2022-01-23 19:44:59', '2022-01-23 19:44:59', '2023-01-23 19:44:59'),
	('da82217c81d3d54fe3dda5a67426ab5821ed8156c9f17c24374744d66e42cd8e66c73b8bfe50c27b', 1, 1, 'AppName', '[]', 0, '2022-02-11 15:06:00', '2022-02-11 15:06:00', '2023-02-11 15:06:00');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_auth_codes: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_clients: ~2 rows (approximately)
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'MoveIt Personal Access Client', 'Cy4r8dELeuEqjHpAaTZHUgro5vMaB0pUjf3K4O8m', NULL, 'http://localhost', 1, 0, 0, '2022-01-19 11:54:00', '2022-01-19 11:54:00'),
	(2, NULL, 'MoveIt Password Grant Client', 'QoZ75k9DVTr6KNpjWgmjxP5nDPc6gkDYsr0iHmGb', 'users', 'http://localhost', 0, 1, 0, '2022-01-19 11:54:00', '2022-01-19 11:54:00');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_personal_access_clients: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
	(1, 1, '2022-01-19 11:54:00', '2022-01-19 11:54:00');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Dumping structure for table moveit.oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.oauth_refresh_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Dumping structure for table moveit.owner_ships
CREATE TABLE IF NOT EXISTS `owner_ships` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.owner_ships: ~4 rows (approximately)
/*!40000 ALTER TABLE `owner_ships` DISABLE KEYS */;
INSERT INTO `owner_ships` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Company owned', 1, '2022-04-10 17:01:13', '2022-04-10 17:01:13'),
	(2, 'Employee owned', 1, '2022-04-10 17:01:14', '2022-04-10 17:01:14'),
	(3, 'Leased', 1, '2022-04-10 17:01:14', '2022-04-10 17:01:14'),
	(4, 'Rental', 1, '2022-04-10 17:01:14', '2022-04-10 17:01:14');
/*!40000 ALTER TABLE `owner_ships` ENABLE KEYS */;

-- Dumping structure for table moveit.packing_materials
CREATE TABLE IF NOT EXISTS `packing_materials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `default_quantity` int(11) NOT NULL DEFAULT 0,
  `pricing_elements` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.packing_materials: ~3 rows (approximately)
/*!40000 ALTER TABLE `packing_materials` DISABLE KEYS */;
INSERT INTO `packing_materials` (`id`, `name`, `price`, `default_quantity`, `pricing_elements`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'PACKING MATERIALS', 25.99, 1, '[11]', 1, '2022-04-10 17:00:38', '2022-04-10 17:00:38'),
	(2, 'PACKING SERVICES', 0.00, 1, '[12,13]', 1, '2022-04-10 17:00:38', '2022-04-10 17:00:38'),
	(3, 'STORAGE SERVICES', 25.99, 1, '[16]', 1, '2022-04-10 17:00:38', '2022-04-10 17:00:38');
/*!40000 ALTER TABLE `packing_materials` ENABLE KEYS */;

-- Dumping structure for table moveit.packing_quotes
CREATE TABLE IF NOT EXISTS `packing_quotes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `pricing_elements` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.packing_quotes: ~3 rows (approximately)
/*!40000 ALTER TABLE `packing_quotes` DISABLE KEYS */;
INSERT INTO `packing_quotes` (`id`, `name`, `price`, `pricing_elements`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'ESTIMATED QUOTE', 0.00, '[1,2,9,11,12,13,14]', 1, '2022-04-10 17:00:41', '2022-04-10 17:00:41'),
	(2, 'HOURLY ESTIMATE', 0.00, '[1,2,3,6,7,8,9,10,11,12,13,14]', 1, '2022-04-10 17:00:42', '2022-04-10 17:00:42'),
	(3, 'NOT TO EXCEED', 0.00, '[1,2,3,4,5,6,7,8,9,10,11,12,13,14]', 1, '2022-04-10 17:00:42', '2022-04-10 17:00:42');
/*!40000 ALTER TABLE `packing_quotes` ENABLE KEYS */;

-- Dumping structure for table moveit.packing_requirements
CREATE TABLE IF NOT EXISTS `packing_requirements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.packing_requirements: ~3 rows (approximately)
/*!40000 ALTER TABLE `packing_requirements` DISABLE KEYS */;
INSERT INTO `packing_requirements` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'No Packing Required', 1, '2022-04-10 17:00:32', '2022-04-10 17:00:32'),
	(2, 'Flat Pack/Unpack', 1, '2022-04-10 17:00:32', '2022-04-10 17:00:32'),
	(3, 'Hourly Pack/Unpack', 1, '2022-04-10 17:00:32', '2022-04-10 17:00:32');
/*!40000 ALTER TABLE `packing_requirements` ENABLE KEYS */;

-- Dumping structure for table moveit.packing_services
CREATE TABLE IF NOT EXISTS `packing_services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `pricing_elements` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.packing_services: ~3 rows (approximately)
/*!40000 ALTER TABLE `packing_services` DISABLE KEYS */;
INSERT INTO `packing_services` (`id`, `name`, `price`, `pricing_elements`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'MOVING SERVICES', 3200.00, '[1,2,9,11,12,13,14]', 1, '2022-04-10 17:00:45', '2022-04-10 17:00:45'),
	(2, 'TRAVELING SERVICES', 0.00, '[1,2,3,6,7,8,9,10,11,12,13,14]', 1, '2022-04-10 17:00:45', '2022-04-10 17:00:45'),
	(3, 'TRUCK AND EQUIPMENT FEE', 0.00, '[1,2,3,4,5,6,7,8,9,10,11,12,13,14]', 1, '2022-04-10 17:00:46', '2022-04-10 17:00:46');
/*!40000 ALTER TABLE `packing_services` ENABLE KEYS */;

-- Dumping structure for table moveit.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table moveit.pay_rolls
CREATE TABLE IF NOT EXISTS `pay_rolls` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pay_roll_class_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.pay_rolls: ~2 rows (approximately)
/*!40000 ALTER TABLE `pay_rolls` DISABLE KEYS */;
INSERT INTO `pay_rolls` (`id`, `pay_roll_class_id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(2, 0, NULL, 1, '2022-03-21 21:55:39', '2022-03-21 21:55:39'),
	(3, 4, 'test', 1, '2022-03-22 06:20:43', '2022-03-22 06:21:05');
/*!40000 ALTER TABLE `pay_rolls` ENABLE KEYS */;

-- Dumping structure for table moveit.pay_roll_classes
CREATE TABLE IF NOT EXISTS `pay_roll_classes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.pay_roll_classes: ~6 rows (approximately)
/*!40000 ALTER TABLE `pay_roll_classes` DISABLE KEYS */;
INSERT INTO `pay_roll_classes` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Moving Or Packing', 1, '2022-04-10 17:01:00', '2022-04-10 17:01:00'),
	(2, 'Misc Hours', 1, '2022-04-10 17:01:00', '2022-04-10 17:01:00'),
	(3, 'Tip', 1, '2022-04-10 17:01:00', '2022-04-10 17:01:00'),
	(4, 'Bonus', 1, '2022-04-10 17:01:00', '2022-04-10 17:01:00'),
	(5, 'Deduction', 1, '2022-04-10 17:01:01', '2022-04-10 17:01:01'),
	(6, 'PerDiem', 1, '2022-04-10 17:01:01', '2022-04-10 17:01:01');
/*!40000 ALTER TABLE `pay_roll_classes` ENABLE KEYS */;

-- Dumping structure for table moveit.pay_roll_configurations
CREATE TABLE IF NOT EXISTS `pay_roll_configurations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `payroll_pdf_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payroll_mile_text` int(11) NOT NULL DEFAULT 0,
  `visible_payroll_fields` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.pay_roll_configurations: ~0 rows (approximately)
/*!40000 ALTER TABLE `pay_roll_configurations` DISABLE KEYS */;
INSERT INTO `pay_roll_configurations` (`id`, `payroll_pdf_text`, `payroll_mile_text`, `visible_payroll_fields`, `created_at`, `updated_at`) VALUES
	(1, 'test', 2, '["2","3"]', '2022-03-21 22:08:02', '2022-03-21 22:08:02');
/*!40000 ALTER TABLE `pay_roll_configurations` ENABLE KEYS */;

-- Dumping structure for table moveit.pay_roll_miles
CREATE TABLE IF NOT EXISTS `pay_roll_miles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.pay_roll_miles: ~2 rows (approximately)
/*!40000 ALTER TABLE `pay_roll_miles` DISABLE KEYS */;
INSERT INTO `pay_roll_miles` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Miles', 1, '2022-04-10 17:01:09', '2022-04-10 17:01:09'),
	(2, 'Long Distance', 1, '2022-04-10 17:01:09', '2022-04-10 17:01:09');
/*!40000 ALTER TABLE `pay_roll_miles` ENABLE KEYS */;

-- Dumping structure for table moveit.phone_types
CREATE TABLE IF NOT EXISTS `phone_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.phone_types: ~4 rows (approximately)
/*!40000 ALTER TABLE `phone_types` DISABLE KEYS */;
INSERT INTO `phone_types` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Mobile Phone', 1, '2022-04-10 17:00:03', '2022-04-10 17:00:03'),
	(2, 'Business phone', 1, '2022-04-10 17:00:03', '2022-04-10 17:00:03'),
	(3, 'Home phone', 1, '2022-04-10 17:00:04', '2022-04-10 17:00:04'),
	(4, 'Fax', 1, '2022-04-10 17:00:04', '2022-04-10 17:00:04');
/*!40000 ALTER TABLE `phone_types` ENABLE KEYS */;

-- Dumping structure for table moveit.pricing_elments
CREATE TABLE IF NOT EXISTS `pricing_elments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.pricing_elments: ~16 rows (approximately)
/*!40000 ALTER TABLE `pricing_elments` DISABLE KEYS */;
INSERT INTO `pricing_elments` (`id`, `name`, `price`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'TRUCKS', 0.00, 1, '2022-04-10 17:00:48', '2022-04-10 17:00:48'),
	(2, 'CREW SIZE', 0.00, 1, '2022-04-10 17:00:48', '2022-04-10 17:00:48'),
	(3, 'HOURLY RATE', 0.00, 1, '2022-04-10 17:00:48', '2022-04-10 17:00:48'),
	(4, 'MIN HOURS', 0.00, 1, '2022-04-10 17:00:49', '2022-04-10 17:00:49'),
	(5, 'MAX HOURS', 0.00, 1, '2022-04-10 17:00:49', '2022-04-10 17:00:49'),
	(6, 'EST HOURS', 0.00, 1, '2022-04-10 17:00:49', '2022-04-10 17:00:49'),
	(7, 'MOVING HOURS', 0.00, 1, '2022-04-10 17:00:49', '2022-04-10 17:00:49'),
	(8, 'TRAVEL TIME', 0.00, 1, '2022-04-10 17:00:49', '2022-04-10 17:00:49'),
	(9, 'TRAVEL FEE', 0.00, 1, '2022-04-10 17:00:49', '2022-04-10 17:00:49'),
	(10, 'MOVING FEE', 0.00, 1, '2022-04-10 17:00:50', '2022-04-10 17:00:50'),
	(11, 'PACKING MATERIALS', 0.00, 1, '2022-04-10 17:00:50', '2022-04-10 17:00:50'),
	(12, 'PACKING CREW SIZE', 0.00, 1, '2022-04-10 17:00:50', '2022-04-10 17:00:50'),
	(13, 'PACKING HOURS', 0.00, 1, '2022-04-10 17:00:50', '2022-04-10 17:00:50'),
	(14, 'PACKING FEE', 0.00, 1, '2022-04-10 17:00:51', '2022-04-10 17:00:51'),
	(15, 'FUEL SURCHARGE', 0.00, 1, '2022-04-10 17:00:51', '2022-04-10 17:00:51'),
	(16, 'STORAGE FEE', 0.00, 1, '2022-04-10 17:00:51', '2022-04-10 17:00:51');
/*!40000 ALTER TABLE `pricing_elments` ENABLE KEYS */;

-- Dumping structure for table moveit.privileges
CREATE TABLE IF NOT EXISTS `privileges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `privilege` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.privileges: ~4 rows (approximately)
/*!40000 ALTER TABLE `privileges` DISABLE KEYS */;
INSERT INTO `privileges` (`id`, `privilege`, `created_at`, `updated_at`) VALUES
	(1, 'View', '2022-01-18 16:42:12', '2022-01-18 16:42:12'),
	(2, 'Add', '2022-01-18 16:42:12', '2022-01-18 16:42:12'),
	(3, 'Edit', '2022-01-18 16:42:12', '2022-01-18 16:42:12'),
	(4, 'Delete', '2022-01-18 16:42:12', '2022-01-18 16:42:12');
/*!40000 ALTER TABLE `privileges` ENABLE KEYS */;

-- Dumping structure for table moveit.properties
CREATE TABLE IF NOT EXISTS `properties` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.properties: ~10 rows (approximately)
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
INSERT INTO `properties` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Apartment', 1, '2022-04-10 17:00:07', '2022-04-10 17:00:07'),
	(2, 'Condo', 1, '2022-04-10 17:00:07', '2022-04-10 17:00:07'),
	(3, 'Dock Height', 1, '2022-04-10 17:00:08', '2022-04-10 17:00:08'),
	(4, 'House', 1, '2022-04-10 17:00:08', '2022-04-10 17:00:08'),
	(5, 'Nursing Home', 1, '2022-04-10 17:00:09', '2022-04-10 17:00:09'),
	(6, 'Office Building', 1, '2022-04-10 17:00:09', '2022-04-10 17:00:09'),
	(7, 'Senior Community', 1, '2022-04-10 17:00:09', '2022-04-10 17:00:09'),
	(8, 'Storage', 1, '2022-04-10 17:00:09', '2022-04-10 17:00:09'),
	(9, 'Town House', 1, '2022-04-10 17:00:09', '2022-04-10 17:00:09'),
	(10, 'Warehouse', 1, '2022-04-10 17:00:10', '2022-04-10 17:00:10');
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;

-- Dumping structure for table moveit.prospect_interest_statuses
CREATE TABLE IF NOT EXISTS `prospect_interest_statuses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.prospect_interest_statuses: ~2 rows (approximately)
/*!40000 ALTER TABLE `prospect_interest_statuses` DISABLE KEYS */;
INSERT INTO `prospect_interest_statuses` (`id`, `name`, `created_by`, `updated_by`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'test', '1', '1', 0, '2022-03-09 12:46:15', '2022-03-09 12:46:15'),
	(2, 'testtt', '1', '1', 1, '2022-03-09 12:50:12', '2022-03-09 12:50:12');
/*!40000 ALTER TABLE `prospect_interest_statuses` ENABLE KEYS */;

-- Dumping structure for table moveit.referrals
CREATE TABLE IF NOT EXISTS `referrals` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.referrals: ~4 rows (approximately)
/*!40000 ALTER TABLE `referrals` DISABLE KEYS */;
INSERT INTO `referrals` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'A family member', 1, '2022-04-10 17:00:13', '2022-04-10 17:00:13'),
	(2, 'Facebook', 1, '2022-04-10 17:00:13', '2022-04-10 17:00:13'),
	(3, 'Friend', 1, '2022-04-10 17:00:13', '2022-04-10 17:00:13'),
	(4, 'Google Ad', 1, '2022-04-10 17:00:14', '2022-04-10 17:00:14');
/*!40000 ALTER TABLE `referrals` ENABLE KEYS */;

-- Dumping structure for table moveit.referred_by_sources
CREATE TABLE IF NOT EXISTS `referred_by_sources` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `cost` decimal(11,2) NOT NULL DEFAULT 0.00,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.referred_by_sources: ~0 rows (approximately)
/*!40000 ALTER TABLE `referred_by_sources` DISABLE KEYS */;
/*!40000 ALTER TABLE `referred_by_sources` ENABLE KEYS */;

-- Dumping structure for table moveit.role
CREATE TABLE IF NOT EXISTS `role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_type` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.role: ~4 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `name`, `role_type`, `created_at`, `updated_at`) VALUES
	(1, 'Super Admin', 1, '2022-04-10 16:59:36', '2022-04-10 16:59:36'),
	(2, 'Admin', 1, '2022-04-10 16:59:36', '2022-04-10 16:59:36'),
	(3, 'Operator', 1, '2022-04-10 16:59:36', '2022-04-10 16:59:36'),
	(4, 'Movers', 1, '2022-04-10 16:59:37', '2022-04-10 16:59:37');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dumping structure for table moveit.rolemoduleprivileges
CREATE TABLE IF NOT EXISTS `rolemoduleprivileges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT 0,
  `module_id` int(11) NOT NULL DEFAULT 0,
  `privilege_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.rolemoduleprivileges: ~58 rows (approximately)
/*!40000 ALTER TABLE `rolemoduleprivileges` DISABLE KEYS */;
INSERT INTO `rolemoduleprivileges` (`id`, `role_id`, `module_id`, `privilege_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, NULL, NULL),
	(2, 1, 1, 2, NULL, NULL),
	(3, 1, 1, 3, NULL, NULL),
	(4, 1, 1, 4, NULL, NULL),
	(5, 1, 7, 1, NULL, NULL),
	(6, 1, 7, 3, NULL, NULL),
	(7, 1, 9, 1, NULL, NULL),
	(8, 1, 9, 2, NULL, NULL),
	(9, 1, 9, 3, NULL, NULL),
	(10, 1, 9, 4, NULL, NULL),
	(11, 1, 10, 1, NULL, NULL),
	(12, 1, 10, 2, NULL, NULL),
	(13, 1, 10, 3, NULL, NULL),
	(14, 1, 10, 4, NULL, NULL),
	(15, 1, 11, 1, NULL, NULL),
	(16, 1, 11, 2, NULL, NULL),
	(17, 1, 11, 3, NULL, NULL),
	(18, 1, 11, 4, NULL, NULL),
	(19, 1, 12, 2, NULL, NULL),
	(20, 1, 12, 3, NULL, NULL),
	(21, 1, 12, 4, NULL, NULL),
	(22, 1, 13, 2, NULL, NULL),
	(23, 1, 13, 3, NULL, NULL),
	(24, 1, 13, 4, NULL, NULL),
	(25, 1, 14, 2, NULL, NULL),
	(26, 1, 14, 3, NULL, NULL),
	(27, 1, 14, 4, NULL, NULL),
	(28, 1, 15, 2, NULL, NULL),
	(29, 1, 15, 3, NULL, NULL),
	(30, 1, 15, 4, NULL, NULL),
	(31, 1, 16, 2, NULL, NULL),
	(32, 1, 16, 3, NULL, NULL),
	(33, 1, 16, 4, NULL, NULL),
	(34, 1, 17, 2, NULL, NULL),
	(35, 1, 17, 3, NULL, NULL),
	(36, 1, 17, 4, NULL, NULL),
	(37, 1, 18, 2, NULL, NULL),
	(38, 1, 18, 3, NULL, NULL),
	(39, 1, 18, 4, NULL, NULL),
	(40, 1, 19, 2, NULL, NULL),
	(41, 1, 19, 3, NULL, NULL),
	(42, 1, 19, 4, NULL, NULL),
	(43, 1, 20, 2, NULL, NULL),
	(44, 1, 20, 3, NULL, NULL),
	(45, 1, 20, 4, NULL, NULL),
	(46, 1, 21, 2, NULL, NULL),
	(47, 1, 21, 3, NULL, NULL),
	(48, 1, 21, 4, NULL, NULL),
	(49, 1, 21, 4, NULL, NULL),
	(50, 1, 22, 2, NULL, NULL),
	(51, 1, 22, 3, NULL, NULL),
	(52, 1, 22, 4, NULL, NULL),
	(53, 1, 23, 2, NULL, NULL),
	(54, 1, 23, 3, NULL, NULL),
	(55, 1, 23, 4, NULL, NULL),
	(56, 1, 24, 2, NULL, NULL),
	(57, 1, 24, 3, NULL, NULL),
	(58, 1, 24, 4, NULL, NULL);
/*!40000 ALTER TABLE `rolemoduleprivileges` ENABLE KEYS */;

-- Dumping structure for table moveit.services
CREATE TABLE IF NOT EXISTS `services` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.services: ~2 rows (approximately)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'NEED PACKING MATERIALS', 1, '2022-04-10 17:00:16', '2022-04-10 17:00:16'),
	(2, 'NEED PACKING SERVICES', 1, '2022-04-10 17:00:17', '2022-04-10 17:00:17');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

-- Dumping structure for table moveit.service_types
CREATE TABLE IF NOT EXISTS `service_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.service_types: ~4 rows (approximately)
/*!40000 ALTER TABLE `service_types` DISABLE KEYS */;
INSERT INTO `service_types` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Residential Move', 1, '2022-04-10 17:00:19', '2022-04-10 17:00:19'),
	(2, 'Commercial Move', 1, '2022-04-10 17:00:19', '2022-04-10 17:00:19'),
	(3, 'Storage', 1, '2022-04-10 17:00:20', '2022-04-10 17:00:20'),
	(4, 'Packing Only', 1, '2022-04-10 17:00:20', '2022-04-10 17:00:20');
/*!40000 ALTER TABLE `service_types` ENABLE KEYS */;

-- Dumping structure for table moveit.square_footage
CREATE TABLE IF NOT EXISTS `square_footage` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `min_range` int(11) NOT NULL DEFAULT 0,
  `max_range` int(11) NOT NULL DEFAULT 0,
  `minutes_per_room` decimal(11,2) NOT NULL DEFAULT 0.00,
  `cubic_feet_per_room` int(11) NOT NULL DEFAULT 0,
  `weight_per_room` int(11) NOT NULL DEFAULT 0,
  `people` int(11) NOT NULL DEFAULT 0,
  `trucks` int(11) NOT NULL DEFAULT 0,
  `price` decimal(11,2) NOT NULL DEFAULT 0.00,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.square_footage: ~0 rows (approximately)
/*!40000 ALTER TABLE `square_footage` DISABLE KEYS */;
/*!40000 ALTER TABLE `square_footage` ENABLE KEYS */;

-- Dumping structure for table moveit.states
CREATE TABLE IF NOT EXISTS `states` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.states: ~0 rows (approximately)
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT INTO `states` (`id`, `name`, `country_id`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'AP', '1', '1', '2022-04-10', '2022-04-10 13:19:47', '2022-04-10 13:19:47');
/*!40000 ALTER TABLE `states` ENABLE KEYS */;

-- Dumping structure for table moveit.storage_requirements
CREATE TABLE IF NOT EXISTS `storage_requirements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.storage_requirements: ~3 rows (approximately)
/*!40000 ALTER TABLE `storage_requirements` DISABLE KEYS */;
INSERT INTO `storage_requirements` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Warehouse Handling', 1, '2022-04-10 17:00:35', '2022-04-10 17:00:35'),
	(2, 'First Month Storage', 1, '2022-04-10 17:00:35', '2022-04-10 17:00:35'),
	(3, 'Misc Storage Fee', 1, '2022-04-10 17:00:35', '2022-04-10 17:00:35');
/*!40000 ALTER TABLE `storage_requirements` ENABLE KEYS */;

-- Dumping structure for table moveit.tasks
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `task_type` int(11) NOT NULL DEFAULT 0,
  `assigned_user_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assigned_user_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `comments` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `when_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_emails` int(11) NOT NULL DEFAULT 0,
  `active_yn` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.tasks: ~0 rows (approximately)
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;

-- Dumping structure for table moveit.task_status
CREATE TABLE IF NOT EXISTS `task_status` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.task_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `task_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `task_status` ENABLE KEYS */;

-- Dumping structure for table moveit.task_types
CREATE TABLE IF NOT EXISTS `task_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.task_types: ~0 rows (approximately)
/*!40000 ALTER TABLE `task_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `task_types` ENABLE KEYS */;

-- Dumping structure for table moveit.terms_and_conditions
CREATE TABLE IF NOT EXISTS `terms_and_conditions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `heading` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.terms_and_conditions: ~2 rows (approximately)
/*!40000 ALTER TABLE `terms_and_conditions` DISABLE KEYS */;
INSERT INTO `terms_and_conditions` (`id`, `heading`, `content`, `created_at`, `updated_at`) VALUES
	(2, 'testdsdf', 'testsdfsdf', '2022-03-31 13:07:40', '2022-04-01 17:26:54'),
	(3, 'qwerty', 'asdfgf', '2022-03-31 14:51:15', '2022-04-01 15:45:14');
/*!40000 ALTER TABLE `terms_and_conditions` ENABLE KEYS */;

-- Dumping structure for table moveit.testimonials
CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.testimonials: ~0 rows (approximately)
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;

-- Dumping structure for table moveit.timezones
CREATE TABLE IF NOT EXISTS `timezones` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.timezones: ~0 rows (approximately)
/*!40000 ALTER TABLE `timezones` DISABLE KEYS */;
/*!40000 ALTER TABLE `timezones` ENABLE KEYS */;

-- Dumping structure for table moveit.travel_time
CREATE TABLE IF NOT EXISTS `travel_time` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `min_mile_range` int(11) NOT NULL DEFAULT 0,
  `max_mile_range` int(11) NOT NULL DEFAULT 0,
  `min_travel_time` decimal(11,2) NOT NULL DEFAULT 0.00,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.travel_time: ~0 rows (approximately)
/*!40000 ALTER TABLE `travel_time` DISABLE KEYS */;
/*!40000 ALTER TABLE `travel_time` ENABLE KEYS */;

-- Dumping structure for table moveit.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alter_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_code` int(11) DEFAULT NULL,
  `reset_hash` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referred_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refferal_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_phone_verify` tinyint(4) NOT NULL DEFAULT 0,
  `is_email_verified` tinyint(4) NOT NULL DEFAULT 0,
  `phone_verified_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_accept_tc` tinyint(4) NOT NULL DEFAULT 0,
  `privacy_policy` tinyint(4) NOT NULL DEFAULT 0,
  `is_registered` tinyint(4) NOT NULL DEFAULT 0,
  `is_approve` tinyint(4) NOT NULL DEFAULT 0,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `app_version` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.users: ~9 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `gender`, `dob`, `phone_type`, `phone`, `alter_phone`, `profile_pic`, `reset_code`, `reset_hash`, `referral_code`, `referred_by`, `refferal_link`, `is_phone_verify`, `is_email_verified`, `phone_verified_at`, `email_verified_at`, `provider`, `last_login`, `is_accept_tc`, `privacy_policy`, `is_registered`, `is_approve`, `is_active`, `app_version`, `created_at`, `updated_at`) VALUES
	(1, 'Raviteja', NULL, 'Ch', 'ch.raviteja2@gmail.com', NULL, NULL, '1', '7189698624', '', NULL, 360652, 'eae8a518a7cabe4147819a974a578220fe2779c9ad08c50540', '404339', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-01-18 18:33:42', '2022-01-18 18:33:42'),
	(2, 'test', NULL, 'test', 'ravi@gmail.com', NULL, NULL, '2', '976766767676', '', NULL, 302126, 'a5bb4a942442992aca9d073e3d83724568d6a63bf1ee7554f4', 'F44CA8', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-03 20:45:45', '2022-02-03 20:45:45'),
	(3, 'test', NULL, 'test', 'ravi@gmail.com', NULL, NULL, '2', '98643634534', '', NULL, 372267, 'f3aa3652e8e2ab95b2e92d03cbd6b134a8a53b3fef8885d2bf', 'FBC4DD', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-03 20:58:26', '2022-02-03 20:58:26'),
	(4, 'test', NULL, 'test', 'rvi@gmail.com', NULL, NULL, '2', '97776567657', '', NULL, 188653, 'b6a3552683c7116a556b5120ebe4f9dd9ec780af6aa32333cb', 'AC6BD0', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-03 21:10:34', '2022-02-03 21:10:34'),
	(5, 'test', NULL, 'test', 'ts@gmail.com', NULL, NULL, '2', '978567567657', '', NULL, 178089, '5ccc27a89b4816b59c29e86b640a5ee4c3be136223cf190b38', '497879', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-03 21:18:54', '2022-02-03 21:18:54'),
	(6, 'test', NULL, 'test', 'ravi@gmail.com', NULL, NULL, '2', '98837773783', '', NULL, 299930, '2e3827814937af4d14e47d99fa026b9317038b8c4a882de13b', '94844B', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-03 21:32:23', '2022-02-03 21:32:23'),
	(7, 'raviteja', NULL, 'ch', 'ch@gmail.com', NULL, NULL, NULL, '7989879899', '6382929289', NULL, 302276, 'd81347d7f2f8daaddeef4ae6cb2827bc40a7893f1c20ba9c7f', 'A54F8C', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-18 11:51:20', '2022-02-18 11:51:20'),
	(8, 'test', NULL, 'test', 'te@gmail.com', NULL, NULL, NULL, '675463634', '4123123', NULL, 332063, 'a44b33d0df0510f5741cf33509b8992066e9c68d0137c9e688', '76DC99', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-23 16:46:47', '2022-02-23 16:46:47'),
	(9, 'test', NULL, 'test', 'test@gmail.com', NULL, NULL, NULL, '24234234', '342423432', NULL, 181784, '7f24246cb23c9cffcdcff787e118e34021badf5783f1eb6aee', '6BDEFE', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-23 16:48:26', '2022-02-23 16:48:26'),
	(10, 'testww', NULL, 'wqwe', 'test@gmail.com', NULL, NULL, NULL, '324324234', '234324324', NULL, 346732, '2015b8a8680545010e63f8892ff379dfae5d0c23acfcbba171', '2D833C', '2', NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '1.0.0', '2022-02-23 16:56:20', '2022-02-23 16:56:20');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table moveit.user_basic_details
CREATE TABLE IF NOT EXISTS `user_basic_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT 0,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` tinyint(4) NOT NULL DEFAULT 0,
  `nationality_id` tinyint(4) NOT NULL DEFAULT 0,
  `address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(11) NOT NULL DEFAULT 0,
  `state_id` int(11) NOT NULL DEFAULT 0,
  `zipcode` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.user_basic_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_basic_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_basic_details` ENABLE KEYS */;

-- Dumping structure for table moveit.vehicle_status
CREATE TABLE IF NOT EXISTS `vehicle_status` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.vehicle_status: ~4 rows (approximately)
/*!40000 ALTER TABLE `vehicle_status` DISABLE KEYS */;
INSERT INTO `vehicle_status` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'In-Service', 1, '2022-04-10 17:01:17', '2022-04-10 17:01:17'),
	(2, 'Out of Service', 1, '2022-04-10 17:01:18', '2022-04-10 17:01:18'),
	(3, 'Not Active', 1, '2022-04-10 17:01:18', '2022-04-10 17:01:18'),
	(4, 'Sold', 1, '2022-04-10 17:01:19', '2022-04-10 17:01:19');
/*!40000 ALTER TABLE `vehicle_status` ENABLE KEYS */;

-- Dumping structure for table moveit.verbiage
CREATE TABLE IF NOT EXISTS `verbiage` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `signature_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `required` tinyint(4) NOT NULL DEFAULT 0,
  `hide` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.verbiage: ~0 rows (approximately)
/*!40000 ALTER TABLE `verbiage` DISABLE KEYS */;
/*!40000 ALTER TABLE `verbiage` ENABLE KEYS */;

-- Dumping structure for table moveit.visible_payroll_fields
CREATE TABLE IF NOT EXISTS `visible_payroll_fields` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `payroll_pdf_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.visible_payroll_fields: ~7 rows (approximately)
/*!40000 ALTER TABLE `visible_payroll_fields` DISABLE KEYS */;
INSERT INTO `visible_payroll_fields` (`id`, `payroll_pdf_text`, `is_active`, `created_at`, `updated_at`) VALUES
	(1, 'Select all', 1, '2022-04-10 17:01:03', '2022-04-10 17:01:03'),
	(2, 'Description', 1, '2022-04-10 17:01:04', '2022-04-10 17:01:04'),
	(3, 'Travel', 1, '2022-04-10 17:01:04', '2022-04-10 17:01:04'),
	(4, 'Commission', 1, '2022-04-10 17:01:05', '2022-04-10 17:01:05'),
	(5, 'Bonus', 1, '2022-04-10 17:01:05', '2022-04-10 17:01:05'),
	(6, 'Per-Diem', 1, '2022-04-10 17:01:06', '2022-04-10 17:01:06'),
	(7, 'Service-fee', 1, '2022-04-10 17:01:06', '2022-04-10 17:01:06');
/*!40000 ALTER TABLE `visible_payroll_fields` ENABLE KEYS */;

-- Dumping structure for table moveit.years
CREATE TABLE IF NOT EXISTS `years` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table moveit.years: ~4 rows (approximately)
/*!40000 ALTER TABLE `years` DISABLE KEYS */;
INSERT INTO `years` (`id`, `year`, `created_at`, `updated_at`) VALUES
	(1, 1930, '2022-04-10 08:30:15', '2022-04-10 08:30:16'),
	(2, 1931, '2022-04-10 08:30:15', '2022-04-10 08:30:16'),
	(3, 1932, '2022-04-10 08:30:15', '2022-04-10 08:30:16'),
	(4, 1933, '2022-04-10 08:30:15', '2022-04-10 08:30:16'),
	(5, 1934, '2022-04-10 08:30:15', '2022-04-10 08:30:16');
/*!40000 ALTER TABLE `years` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
